--filepath=O:\Datafeed\Prices\FTPFailure\
--filenameprefix=
--filename=FTPFailure
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Prices\missing_ftp\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--#1
SELECT distinct site as FTPServer,localfolder as LocalFolder,
remotefolder as RemoteFolder,acttime as ActTime FROM prices.fileupload
WHERE prices.fileupload.transfer like '%fail%'
and prices.fileupload.acttime >= concat(SUBDATE(curdate(), INTERVAL 1 DAY),' 09:00:00') 
and prices.fileupload.acttime < concat(curdate(),' 09:00:00')
and localfolder like '%P04%'
order by prices.fileupload.acttime