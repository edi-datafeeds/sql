--filepath=O:\Datafeed\Prices\Bonds_Unmatched\
--filenameprefix=
--filename=Bond_Unmatched
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
select distinct
MIC, 
LocalCode,
Isin, 
Currency,
MktCloseDate, 
Issuername,
SectyCD, 
SecurityDesc, 
Sedol,
UsCode, 
PrimaryExchgCD,
ExchgCD, 
Comment 
from prices.liveprices
where providerid in(select id from prices.markets where marketcode like '%FI')
and secid = 0
and 
(
isin not in(select isin FROM prices.bondstatic)
OR 
localcode not in(select commoncode FROM prices.bondstatic)
)
and sectycd <> 'WAR'
order by MIC,LocalCode,Isin