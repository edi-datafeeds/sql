--filepath=O:\Datafeed\Prices\PDES\
--filenameprefix=
--filename=PDES_Missing
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
select
prices.markets.mic as MIC,
prices.markets.description as MarketName,
notifications.notificationlog.mktclosedate as MktCloseDate,
case when(right(prices.markets.marketcode,3)='_IX') then 'Index'
     when(right(prices.markets.marketcode,3)='_FI') then 'Bond'
     else 'Stock' end as FileType,
'Missing' as Message
from notifications.notificationlog
left outer join prices.markets on prices.markets.id = notifications.notificationlog.providerid
left outer join prices.marketloadlog on prices.marketloadlog.providerid = notifications.notificationlog.providerid
                                        and prices.marketloadlog.mktclosedate = notifications.notificationlog.mktclosedate
where notifications.notificationlog.type = 'DEL'
and prices.markets.name = 'PDes'
and prices.marketloadlog.providerid is null
and notifications.notificationlog.mktclosedate >= SUBDATE(CURDATE(),INTERVAL 10 DAY)
and notifications.notificationlog.mktclosedate <= SUBDATE(CURDATE(),INTERVAL 4 DAY)
order by notifications.notificationlog.mktclosedate