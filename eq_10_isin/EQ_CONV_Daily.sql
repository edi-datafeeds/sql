--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--CONV
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_CONV.Changed,
'select  ConvNotes As Notes FROM CONV WHERE ConvID = '+ cast(v_EQ_CONV.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'125'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN v_EQ_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_CONV.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_CONV.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_CONV.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_CONV.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_CONV.FromDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_CONV.FromDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/* CASE WHEN v_EQ_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END, */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_CONV.EventID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_EQ_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN v_EQ_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_CONV.ToDate <>'' AND v_EQ_CONV.FromDate <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , v_EQ_CONV.ToDate,112)
     WHEN v_EQ_CONV.FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_CONV.FromDate,112)+'/UKWN'
     WHEN v_EQ_CONV.ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_CONV.ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_EQ_CONV.Price <>''
     THEN ':90B::EXER//ACTU/'+v_EQ_CONV.CurenCD+
          +substring(v_EQ_CONV.Price,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     END,
CASE WHEN v_EQ_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v_EQ_CONV.RatioNew <>'' AND v_EQ_CONV.RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_EQ_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(v_EQ_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
/*Currently no suitable paydate
CASE WHEN v_EQ_CONV.Paydate <>'' 
             AND v_EQ_CONV.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_CONV.Paydate,112)
     WHEN v_EQ_CONV.paydate <>'' 
             AND v_EQ_CONV.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_CONV.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_CONV
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (ToDate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and Actflag <> ''
order by caref2 desc, caref1, caref3