--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--REDO
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_REDO.Changed,
'select  CurrdNotes As Notes FROM CURRD WHERE CurrdID = '+ cast(v_EQ_REDO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'162'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_REDO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_REDO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_REDO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_REDO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_REDO.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_REDO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + v_EQ_REDO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_REDO.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_REDO.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB1,
':16R:FIA',
CASE WHEN v_EQ_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + v_EQ_REDO.MCD
     END,
':11A::DENO//'+v_EQ_REDO.NewCurenCD,
':16S:FIA',
/*CASE WHEN v_EQ_REDO.oldparvalue  <>'' and v_EQ_REDO.newparvalue <>''
     and cast(v_EQ_REDO.oldparvalue as decimal(11,5))>0 and cast(v_EQ_REDO.newparvalue as decimal(11,5))>0
     THEN ':92D::NEWO//1,/'+cast(cast(v_EQ_REDO.oldparvalue as decimal(11,5))/cast(v_EQ_REDO.newparvalue as decimal(11,5)) as varchar(30))
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,*/
CASE WHEN v_EQ_REDO.newparvalue <>''
            AND v_EQ_REDO.oldparvalue <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_EQ_REDO.newparvalue as char (15)))+
                    '/'+rtrim(cast(v_EQ_REDO.oldparvalue as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_REDO
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Effectivedate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and NewCurenCD <>''
order by caref2 desc, caref1, caref3