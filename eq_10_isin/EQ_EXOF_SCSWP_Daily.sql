--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--EXOF
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_EXOF_SCSWP.Changed,
'select  ScswpNotes As Notes FROM SCSWP WHERE RdID = '+ cast(v_EQ_EXOF_SCSWP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'164'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_EXOF_SCSWP.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_EXOF_SCSWP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_EXOF_SCSWP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_EXOF_SCSWP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_EXOF_SCSWP.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_EXOF_SCSWP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_EXOF_SCSWP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_EXOF_SCSWP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':69J::PWAL//UKWN'
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN v_EQ_EXOF_SCSWP.newratio <>''
          AND v_EQ_EXOF_SCSWP.oldratio <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_EQ_EXOF_SCSWP.newratio as char (15)))+
                    '/'+rtrim(cast(v_EQ_EXOF_SCSWP.oldratio as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN v_EQ_EXOF_SCSWP.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_EXOF_SCSWP.Paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_EXOF_SCSWP
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3