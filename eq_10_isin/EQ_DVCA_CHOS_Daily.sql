--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--DVCA
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_DVOP.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(v_EQ_DVOP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE
     WHEN v_EQ_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVCA',
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_DVOP.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_DVOP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_DVOP.MCD is not null
     THEN ':94B::PLIS//EXCH/' + v_EQ_DVOP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_DVOP.Exdate is not null 
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_EQ_DVOP.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v_EQ_DVOP.Recdate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN v_EQ_DVOP.DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN v_EQ_DVOP.DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN v_EQ_DVOP.DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN v_EQ_DVOP.Frequency='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN v_EQ_DVOP.Frequency='SMA'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='TRM'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='QTR'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='MNT'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='WKL'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='DLY'
     THEN ':22F::DIVI//REGR'
     ELSE ':22F::DIVI//INTE'
     END,
/* START In case of Franking */
/* END In case of Franking */     
/* RDNotes populated by rendering programme */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_DVOP.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* CASH OPTION START_1 */
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
     THEN ':13A::CAON//001'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 <> 'C'
     THEN ''
     WHEN v_EQ_DVOP.CurenCD_1 <> '' AND v_EQ_DVOP.CurenCD_1 <> ''
     THEN ':11A::OPTN//' + v_EQ_DVOP.CurenCD_1
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_1 = 'C' and (v_EQ_DVOP.DefaultOpt_1='T' 
         or (DefaultOpt_2='F' and DefaultOpt_3='F' and DefaultOpt_4='F' and DefaultOpt_5='F' and DefaultOpt_6='F'))
     THEN ':17B::DFLT//Y'
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
            AND  v_EQ_DVOP.Netdividend_1 <> ''
     THEN ':92F::NETT//'+CurenCD_1+ltrim(cast(v_EQ_DVOP.Netdividend_1 as char(15)))
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
            AND  v_EQ_DVOP.Netdividend_1 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
            AND v_EQ_DVOP.Grossdividend_1 <> ''
     THEN ':92F::GRSS//'+CurenCD_1+ltrim(cast(v_EQ_DVOP.Grossdividend_1 as char(15)))
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
            AND v_EQ_DVOP.Grossdividend_1 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
     THEN ':16R:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
           AND v_EQ_DVOP.Paydate is not null 
              AND v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DVOP.paydate,112)
     WHEN Divtype_1 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
     THEN ':16S:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D' OR v_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_1 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION START_2 */
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
     THEN ':13A::CAON//002'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 <> 'C'
     THEN ''
     WHEN v_EQ_DVOP.CurenCD_2 <> '' AND v_EQ_DVOP.CurenCD_2 <> ''
     THEN ':11A::OPTN//' + v_EQ_DVOP.CurenCD_2
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_2 = 'C' and v_EQ_DVOP.DefaultOpt_2='T' 
     THEN ':17B::DFLT//Y'
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
            AND  v_EQ_DVOP.Netdividend_2 <> ''
     THEN ':92F::NETT//'+CurenCD_2+ltrim(cast(v_EQ_DVOP.Netdividend_2 as char(15)))
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
            AND  v_EQ_DVOP.Netdividend_2 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
            AND v_EQ_DVOP.Grossdividend_2 <> ''
     THEN ':92F::GRSS//'+CurenCD_2+ltrim(cast(v_EQ_DVOP.Grossdividend_2 as char(15)))
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
            AND v_EQ_DVOP.Grossdividend_2 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
     THEN ':16R:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
           AND v_EQ_DVOP.Paydate is not null 
              AND v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DVOP.paydate,112)
     WHEN Divtype_2 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
     THEN ':16S:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_2 = 'D' OR v_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_2 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_2 */
/* CASH OPTION START_3 */
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
     THEN ':13A::CAON//003'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 <> 'C'
     THEN ''
     WHEN v_EQ_DVOP.CurenCD_3 <> '' AND v_EQ_DVOP.CurenCD_3 <> ''
     THEN ':11A::OPTN//' + v_EQ_DVOP.CurenCD_3
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_3 = 'C' and v_EQ_DVOP.DefaultOpt_3='T' 
     THEN ':17B::DFLT//Y'
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
            AND  v_EQ_DVOP.Netdividend_3 <> ''
     THEN ':92F::NETT//'+CurenCD_3+ltrim(cast(v_EQ_DVOP.Netdividend_3 as char(15)))
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
            AND  v_EQ_DVOP.Netdividend_3 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
            AND v_EQ_DVOP.Grossdividend_3 <> ''
     THEN ':92F::GRSS//'+CurenCD_3+ltrim(cast(v_EQ_DVOP.Grossdividend_3 as char(15)))
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
            AND v_EQ_DVOP.Grossdividend_3 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
     THEN ':16R:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
           AND v_EQ_DVOP.Paydate is not null 
              AND v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DVOP.paydate,112)
     WHEN Divtype_3 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
     THEN ':16S:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_3 = 'D' OR v_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_3 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_3 */
/* CASH OPTION START_4 */
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
     THEN ':13A::CAON//004'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 <> 'C'
     THEN ''
     WHEN v_EQ_DVOP.CurenCD_4 <> '' AND v_EQ_DVOP.CurenCD_4 <> ''
     THEN ':11A::OPTN//' + v_EQ_DVOP.CurenCD_4
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_4 = 'C' and v_EQ_DVOP.DefaultOpt_4='T' 
     THEN ':17B::DFLT//Y'
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
            AND  v_EQ_DVOP.Netdividend_4 <> ''
     THEN ':92F::NETT//'+CurenCD_4+ltrim(cast(v_EQ_DVOP.Netdividend_4 as char(15)))
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
            AND  v_EQ_DVOP.Netdividend_4 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
            AND v_EQ_DVOP.Grossdividend_4 <> ''
     THEN ':92F::GRSS//'+CurenCD_4+ltrim(cast(v_EQ_DVOP.Grossdividend_4 as char(15)))
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
            AND v_EQ_DVOP.Grossdividend_4 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
     THEN ':16R:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
           AND v_EQ_DVOP.Paydate is not null 
              AND v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DVOP.paydate,112)
     WHEN Divtype_4 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
     THEN ':16S:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_4 = 'D' OR v_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_4 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_4 */
/* CASH OPTION START_5 */
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
     THEN ':13A::CAON//005'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 <> 'C'
     THEN ''
     WHEN v_EQ_DVOP.CurenCD_5 <> '' AND v_EQ_DVOP.CurenCD_5 <> ''
     THEN ':11A::OPTN//' + v_EQ_DVOP.CurenCD_5
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_5 = 'C' and v_EQ_DVOP.DefaultOpt_5='T' 
     THEN ':17B::DFLT//Y'
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
            AND  v_EQ_DVOP.Netdividend_5 <> ''
     THEN ':92F::NETT//'+CurenCD_5+ltrim(cast(v_EQ_DVOP.Netdividend_5 as char(15)))
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
            AND  v_EQ_DVOP.Netdividend_5 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
            AND v_EQ_DVOP.Grossdividend_5 <> ''
     THEN ':92F::GRSS//'+CurenCD_5+ltrim(cast(v_EQ_DVOP.Grossdividend_5 as char(15)))
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
            AND v_EQ_DVOP.Grossdividend_5 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
     THEN ':16R:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
           AND v_EQ_DVOP.Paydate is not null 
              AND v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DVOP.paydate,112)
     WHEN Divtype_5 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
     THEN ':16S:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_5 = 'D' OR v_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_5 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_5 */
/* CASH OPTION START_6 */
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
     THEN ':13A::CAON//006'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 <> 'C'
     THEN ''
     WHEN v_EQ_DVOP.CurenCD_6 <> '' AND v_EQ_DVOP.CurenCD_6 <> ''
     THEN ':11A::OPTN//' + v_EQ_DVOP.CurenCD_6
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_6 = 'C' and v_EQ_DVOP.DefaultOpt_6='T' 
     THEN ':17B::DFLT//Y'
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
            AND  v_EQ_DVOP.Netdividend_6 <> ''
     THEN ':92F::NETT//'+CurenCD_6+ltrim(cast(v_EQ_DVOP.Netdividend_6 as char(15)))
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
            AND  v_EQ_DVOP.Netdividend_6 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
            AND v_EQ_DVOP.Grossdividend_6 <> ''
     THEN ':92F::GRSS//'+CurenCD_6+ltrim(cast(v_EQ_DVOP.Grossdividend_6 as char(15)))
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
            AND v_EQ_DVOP.Grossdividend_6 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
     THEN ':16R:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
           AND v_EQ_DVOP.Paydate is not null 
              AND v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DVOP.paydate,112)
     WHEN Divtype_6 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
     THEN ':16S:CASHMOVE'
     ELSE '' 
     END,
CASE WHEN v_EQ_DVOP.Actflag_6 = 'D' OR v_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN v_EQ_DVOP.Divtype_6 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_6 */
'' as Notes,
'-}$'
From v_EQ_DVOP
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and actflag <> 'C'
and actflag_1 <> 'D'
and actflag_1 <> 'C'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
/* make sure exclusively cash types */
and (DIVTYPE_1 = 'C' or DIVTYPE_1 = '')
and (DIVTYPE_2 = 'C' or DIVTYPE_2 = '')
and (DIVTYPE_3 = 'C' or DIVTYPE_3 = '')
and (DIVTYPE_4 = 'C' or DIVTYPE_4 = '')
and (DIVTYPE_5 = 'C' or DIVTYPE_5 = '')
and (DIVTYPE_6 = 'C' or DIVTYPE_6 = '')
/* and at least 2 are cash types */
and ((divtype_1='C' and divtype_2='C') or (divtype_1='C' and divtype_3='C')
or   (divtype_1='C' and divtype_4='C') or (divtype_1='C' and divtype_5='C')
or   (divtype_1='C' and divtype_6='C') or (divtype_2='C' and divtype_3='C')
or   (divtype_2='C' and divtype_4='C') or (divtype_2='C' and divtype_5='C'))
/* and that one corresponding pair at least are undeleted */
and ((actflag_1<>'D' and actflag_2<>'D') or (actflag_1<>'D' and actflag_3<>'D')
or   (actflag_1<>'D' and actflag_4<>'D') or (actflag_1<>'D' and actflag_5<>'D')
or   (actflag_1<>'D' and actflag_6<>'D') or (actflag_2<>'D' and actflag_3<>'D')
or   (actflag_2<>'D' and actflag_4<>'D') or (actflag_2<>'D' and actflag_5<>'D'))
order by caref2 desc, caref1, caref3