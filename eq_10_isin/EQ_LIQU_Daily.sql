--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--LIQU
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=LIQ
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
'select distinct * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + rtrim(cast(v_EQ_LIQU.EventID as char(10))) + ' and sEvent = '+ char(39) + 'LIQ'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
v_EQ_LIQU.Changed,
'select  LiquidationTerms As Notes FROM LIQ WHERE LIQID = '+ cast(v_EQ_LIQU.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'113'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_LIQU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_LIQU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_LIQU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_LIQU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_LIQU.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_LIQU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_LIQU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_LIQU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_LIQU.RdDate <>'' AND v_EQ_LIQU.RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_LIQU.RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_LIQU
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and actflag <> 'C'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (RdDate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3