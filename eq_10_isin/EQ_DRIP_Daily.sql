--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--DRIP
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_DVOP.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( v_EQ_DVOP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'101'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN  v_EQ_DVOP.Actflag_1 = 'D'
     THEN ':23G:CANC'
     WHEN  v_EQ_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN  v_EQ_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN  v_EQ_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  v_EQ_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DRIP',
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_DVOP.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_DVOP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN  v_EQ_DVOP.MCD <>''
     THEN ':94B::PLIS//EXCH/' +  v_EQ_DVOP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN  v_EQ_DVOP.Exdate <>'' AND  v_EQ_DVOP.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar ,  v_EQ_DVOP.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN  v_EQ_DVOP.Recdate <>'' AND  v_EQ_DVOP.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar ,  v_EQ_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN v_EQ_DVOP.DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN v_EQ_DVOP.DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN v_EQ_DVOP.DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN v_EQ_DVOP.Frequency='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN v_EQ_DVOP.Frequency='SMA'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='TRM'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='QTR'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='MNT'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='WKL'
     THEN ':22F::DIVI//REGR'
     WHEN v_EQ_DVOP.Frequency='DLY'
     THEN ':22F::DIVI//REGR'
     ELSE ':22F::DIVI//INTE'
     END,
/* START In case of Franking */
/* END In case of Franking */
/* RDNotes populated by rendering programme */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast( v_EQ_DVOP.RdID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN  v_EQ_DVOP.CurenCD_1 <> ''
     THEN ':11A::OPTN//' +  v_EQ_DVOP.CurenCD_1
     END,
':17B::DFLT//Y',
CASE WHEN  v_EQ_DVOP.DripLastDate <>'' AND  v_EQ_DVOP.DripLastDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar ,  v_EQ_DVOP.DripLastDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN  v_EQ_DVOP.Netdividend_1 <> ''
                 AND  v_EQ_DVOP.Netdividend_1 IS NOT NULL
     THEN ':92F::NETT//'+ v_EQ_DVOP.CurenCD_1+ltrim(cast( v_EQ_DVOP.Netdividend_1 as char(15)))
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN  v_EQ_DVOP.Grossdividend_1 <> ''
             AND  v_EQ_DVOP.Grossdividend_1 IS NOT NULL
     THEN ':92F::GRSS//'+ v_EQ_DVOP.CurenCD_1+ltrim(cast( v_EQ_DVOP.Grossdividend_1 as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  v_EQ_DVOP.Paydate <>'' 
             AND  v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  v_EQ_DVOP.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SECU',
':17B::DFLT//N',
CASE WHEN  v_EQ_DVOP.Fractions_1 = 'C'
     THEN ':22F::DISF//CINL'
     WHEN  v_EQ_DVOP.Fractions_1 = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN  v_EQ_DVOP.Fractions_1 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN  v_EQ_DVOP.Fractions_1 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN  v_EQ_DVOP.Fractions_1 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN  v_EQ_DVOP.Fractions_1 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN  v_EQ_DVOP.DripReinvPrice <>'' 
             AND  v_EQ_DVOP.DripReinvPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+ v_EQ_DVOP.CurenCD_1+ltrim(cast( v_EQ_DVOP.DripReinvPrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB1,
CASE WHEN  v_EQ_DVOP.DripPaydate <>'' AND  v_EQ_DVOP.DripPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  v_EQ_DVOP.DripPaydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_DVOP
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and actflag <> 'C'
and actflag_1 <> 'D'
and actflag_1 <> 'C'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and Actflag_1 <> ''
and Divtype_1 = 'C'
and DripcntryCD <> ''
and substring(Curencd_1,1,2)=Dripcntrycd
and (CalcListdate<=Exdate or (Exdate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=AnnounceDate))
order by caref2 desc, caref1, caref3