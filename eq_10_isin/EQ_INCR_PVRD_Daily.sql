--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--INCR
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_INCR_DECR_PVRD.Changed,
'select  PVRDNotes As Notes FROM PVRD WHERE PVRDID = '+ cast(v_EQ_INCR_DECR_PVRD.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'154'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_INCR_DECR_PVRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_INCR_DECR_PVRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_INCR_DECR_PVRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_INCR_DECR_PVRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INCR',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_INCR_DECR_PVRD.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_INCR_DECR_PVRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_INCR_DECR_PVRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_INCR_DECR_PVRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_INCR_DECR_PVRD.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_INCR_DECR_PVRD.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/*CASE WHEN v_EQ_INCR_DECR_PVRD.OldParValue <> '0' or v_EQ_INCR_DECR_PVRD.NewParValue <> ''
     THEN ':70E::TXNR//'
     ELSE '' END,
CASE WHEN v_EQ_INCR_DECR_PVRD.OldParValue <> '0'
     THEN 'Old Par Value ' +v_EQ_INCR_DECR_PVRD.OldParValue
     ELSE ''
     END, 
CASE WHEN v_EQ_INCR_DECR_PVRD.NewParValue <> '0'
     THEN 'New Par Value ' +v_EQ_INCR_DECR_PVRD.NewParValue
     ELSE ''
     END, */
':16S:CADETL',
'' as Notes,
'-}$'
From v_EQ_INCR_DECR_PVRD
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (EffectiveDate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and OldParValue<>'0'
and NewParValue<>'0'
and cast(rtrim(OldParValue) as decimal(14,5))<
      cast(rtrim(NewParValue) as decimal(14,5))
order by caref2 desc, caref1, caref3