--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--BIDS
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=BB
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
'select distinct V10s_MPAY.*, BB.Startdate as OpenDate, BB.Enddate as CloseDate from V10s_MPAY'
+ ' inner join BB on v10S_MPAY.eventid = BB.BbID'
+ ' where v10S_MPAY.actflag<>'+char(39)+'D'+char(39)+' and eventid = ' 
+ rtrim(cast(v_EQ_BIDS.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'BB'+ char(39)
+ ' And Paytype <> ' +char(39) +char(39) 
+ ' Order By OptionID, SerialID' as MPAYlink,
v_EQ_BIDS.Changed,
'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(v_EQ_BIDS.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'115'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_BIDS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_BIDS.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN v_EQ_BIDS.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_BIDS.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS',
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_BIDS.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_BIDS.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_BIDS.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_BIDS.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_BIDS.StartDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_BIDS.StartDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/* CASE WHEN v_EQ_BIDS.EndDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_BIDS.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_BIDS.EndDate <>''
       AND v_EQ_BIDS.StartDate <>'' 
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_BIDS.StartDate,112)+'/'
          +CONVERT ( varchar , v_EQ_BIDS.EndDate,112)
     WHEN v_EQ_BIDS.StartDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_BIDS.StartDate,112)+'/UKWN'
     WHEN v_EQ_BIDS.EndDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_BIDS.EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END, */
CASE WHEN v_EQ_BIDS.MaxQlyQty <> ''
     THEN ':36B::QTSO//'+substring(v_EQ_BIDS.MaxQlyQty,1,15) + ','
     END,
':16S:CADETL',
/* ':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_EQ_BIDS.CurenCD <> '' 
     THEN ':11A::OPTN//' + v_EQ_BIDS.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v_EQ_BIDS.paydate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_BIDS.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN v_EQ_BIDS.MaxPrice <> ''
     THEN ':90B::OFFR//ACTU/'+v_EQ_BIDS.CurenCD+
          +substring(v_EQ_BIDS.MaxPrice,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
'' as Notes, 
'-}$' */
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_BIDS
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Enddate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and Actflag <> ''
order by caref2 desc, caref1, caref3
