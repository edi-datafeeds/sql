--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--TEND
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=TKOVR
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
'select distinct V10s_MPAY.*, TKOVR.Opendate, TKOVR.CloseDate from V10s_MPAY'
+ ' inner join TKOVR on v10S_MPAY.eventid = TKOVR.TkovrID'
+ ' where v10S_MPAY.actflag<>'+char(39)+'D'+char(39)+' and eventid = ' 
+ rtrim(cast(v_EQ_TEND_TKOVR.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) 
+ ' Order By OptionID, SerialID' as MPAYlink,
v_EQ_TEND_TKOVR.Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(v_EQ_TEND_TKOVR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'131'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_TEND_TKOVR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_TEND_TKOVR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_TEND_TKOVR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_TEND_TKOVR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
CASE WHEN v_EQ_TEND_TKOVR.CmAcqdate <> ''
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_TEND_TKOVR.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_TEND_TKOVR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_TEND_TKOVR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_TEND_TKOVR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_TEND_TKOVR.Opendate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_TEND_TKOVR.opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN v_EQ_TEND_TKOVR.CmAcqdate <> ''
     THEN ':98A::WUCO//'+CONVERT ( varchar , v_EQ_TEND_TKOVR.CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
/* CASE WHEN v_EQ_TEND_TKOVR.CloseDate <> ''
       AND v_EQ_TEND_TKOVR.OpenDate <> ''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_TEND_TKOVR.OpenDate,112)+'/'
          +CONVERT ( varchar , v_EQ_TEND_TKOVR.CloseDate,112)
     WHEN v_EQ_TEND_TKOVR.OpenDate <> ''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_TEND_TKOVR.OpenDate,112)+'/UKWN'
     WHEN v_EQ_TEND_TKOVR.CloseDate <> ''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_TEND_TKOVR.CloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_EQ_TEND_TKOVR.CloseDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_TEND_TKOVR.CloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END, */
CASE WHEN v_EQ_TEND_TKOVR.UnconditionalDate <>'' and v_EQ_TEND_TKOVR.UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_TEND_TKOVR
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and actflag <> 'C'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (CmAcqdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
*/
    )     
order by caref2 desc, caref1, caref3