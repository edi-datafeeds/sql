--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--RHDI
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_RHTS_RTS.Changed,
'select  RtsNotes As Notes FROM RTS WHERE RDID = '+ cast(v_EQ_RHTS_RTS.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'160'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_RHTS_RTS.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_RHTS_RTS.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_RHTS_RTS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_RHTS_RTS.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHDI',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
/*
Removed 2008/11/12, but left in-case it's required later
'' as link568,
':16R:LINK',
':20C::RELA//117'+v_EQ_RHTS_RTS.EXCHGID+cast(v_EQ_RHTS_RTS.Seqnum as char(1))+ cast(v_EQ_RHTS_RTS.EventID as char(16)) as CAref,
':16S:LINK',
*/
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_RHTS_RTS.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_RHTS_RTS.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_RHTS_RTS.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_RHTS_RTS.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_RHTS_RTS.Exdate <>'' AND v_EQ_RHTS_RTS.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_EQ_RHTS_RTS.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v_EQ_RHTS_RTS.Recdate <>'' AND v_EQ_RHTS_RTS.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_RHTS_RTS.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
/* RDNotes populated by rendering programme */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_RHTS_RTS.RdID as char(16)) as Notes, */ 
':22F::RHDI//EXRI',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_EQ_RHTS_RTS.Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_RHTS_RTS.Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_RHTS_RTS.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_RHTS_RTS.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_RHTS_RTS.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_RHTS_RTS.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
':17B::DFLT//Y',
CASE WHEN v_EQ_RHTS_RTS.EndTrade <> '' 
       AND v_EQ_RHTS_RTS.StartTrade <> '' 
     THEN ':69A::REVO//'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.StartTrade,112) + '/'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.EndTrade,112)
     WHEN v_EQ_RHTS_RTS.StartTrade <> '' 
     THEN ':69C::REVO//'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.StartTrade,112) + '/UKWN'
     WHEN v_EQ_RHTS_RTS.EndTrade <> '' 
     THEN ':69E::REVO//UKWN/'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.EndTrade,112)
     ELSE ':69J::REVO//UKWN'
     END,
CASE WHEN v_EQ_RHTS_RTS.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_EQ_RHTS_RTS.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN traTFB1<>''
     THEN ':35B:' + traTFB1
     END,
CASE WHEN v_EQ_RHTS_RTS.EndTrade <> '' 
       AND v_EQ_RHTS_RTS.StartTrade <> '' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.StartTrade,112) + '/'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.EndTrade,112)
     WHEN v_EQ_RHTS_RTS.StartTrade <> '' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.StartTrade,112) + '//UKWN'
     WHEN v_EQ_RHTS_RTS.EndTrade <> '' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , v_EQ_RHTS_RTS.EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
CASE WHEN v_EQ_RHTS_RTS.RatioNew <>''
          AND v_EQ_RHTS_RTS.RatioOld <>''
     THEN ':92D::ADEX//'+ltrim(cast(v_EQ_RHTS_RTS.RatioNew as char (15)))+
                    '/'+ltrim(cast(v_EQ_RHTS_RTS.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN v_EQ_RHTS_RTS.Paydate <>'' 
               AND v_EQ_RHTS_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_RHTS_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_RHTS_RTS
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3