--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--RHTS
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 20
v_EQ_RHTS_ENT.Changed,
'select  ENTNotes As Notes FROM ENT WHERE RDID = '+ cast(v_EQ_RHTS_ENT.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'116'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_RHTS_ENT.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_RHTS_ENT.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_RHTS_ENT.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_RHTS_ENT.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHTS',
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_RHTS_ENT.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_RHTS_ENT.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_RHTS_ENT.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_RHTS_ENT.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
/* CASE WHEN v_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END, 
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <>''
AND v_EQ_RHTS_ENT.EndSubscription IS NOT NULL
       AND v_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.StartSubscription,112)+'/'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     WHEN v_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.StartSubscription,112)+'/UKWN'
     WHEN v_EQ_RHTS_ENT.EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END, */
/* RDNotes populated by rendering programme */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_RHTS_ENT.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN v_EQ_RHTS_ENT.CurenCD <> '' 
     THEN ':11A::OPTN//' + v_EQ_RHTS_ENT.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <>''
AND v_EQ_RHTS_ENT.EndSubscription IS NOT NULL
       AND v_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.StartSubscription,112)+'/'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     WHEN v_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.StartSubscription,112)+'/UKWN'
     WHEN v_EQ_RHTS_ENT.EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.EntIssuePrice <> ''
     THEN ':90B::PRPP//ACTU/'+CurenCD+ltrim(cast(v_EQ_RHTS_ENT.EntIssuePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN v_EQ_RHTS_ENT.RatioNew <>''
          AND v_EQ_RHTS_ENT.RatioOld <>''
     THEN ':92D::NWRT//'+ltrim(cast(v_EQ_RHTS_ENT.RatioNew as char (15)))+
                    '/'+ltrim(cast(v_EQ_RHTS_ENT.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
CASE WHEN v_EQ_RHTS_ENT.Paydate <> '' and v_EQ_RHTS_ENT.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_RHTS_ENT.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_RHTS_ENT
WHERE 
mainTFB1<>''
and exchgcd='gblse'
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3