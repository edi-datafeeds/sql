--filepath=o:\AUTO\ftp_dir\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_NET
--fileheadertext=EDI_FTP_PATHS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\639incr\
--fieldheaders=y
--filetidy=N
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1

select distinct servb.userdiraccess.LoginID, servb.userdiraccess.Dir,servb.uservirtualpath.VirtualPath
from servb.userdiraccess
join servb.uservirtualpath on servb.userdiraccess.LoginID = servb.uservirtualpath.LoginID
and servb.uservirtualpath.PhysicalPath = servb.userdiraccess.Dir
or servb.userdiraccess.LoginID != servb.uservirtualpath.LoginID
join servb.user on servb.userdiraccess.LoginID = servb.user.LoginID
where servb.user.Enabled != 0
order by servb.userdiraccess.LoginID,  servb.userdiraccess.Dir asc;