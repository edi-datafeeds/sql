--filepath=o:\Datafeed\Youness\
--filenameprefix=NET_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_FTP_PATHS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\639incr\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1

select distinct servb.userdiraccess.LoginID, servb.userdiraccess.Dir,servb.uservirtualpath.VirtualPath
from servb.userdiraccess
join servb.uservirtualpath on servb.userdiraccess.LoginID = servb.uservirtualpath.LoginID
and servb.uservirtualpath.PhysicalPath = servb.userdiraccess.Dir
or servb.userdiraccess.LoginID != servb.uservirtualpath.LoginID
order by servb.userdiraccess.LoginID,  servb.userdiraccess.Dir asc;