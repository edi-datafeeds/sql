--filepath=o:\AUTO\ftp_dir\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_COM
--fileheadertext=EDI_FTP_PATHS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\639incr\
--fieldheaders=y
--filetidy=N
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1

select distinct servubkup.userdiraccess.LoginID, servubkup.userdiraccess.Dir,servubkup.uservirtualpath.VirtualPath
from servubkup.userdiraccess
join servubkup.uservirtualpath on servubkup.userdiraccess.LoginID = servubkup.uservirtualpath.LoginID
and servubkup.uservirtualpath.PhysicalPath = servubkup.userdiraccess.Dir
or servubkup.userdiraccess.LoginID != servubkup.uservirtualpath.LoginID
join servubkup.user on servubkup.userdiraccess.LoginID = servubkup.user.LoginID
where servubkup.user.Enabled != 0
order by servubkup.userdiraccess.LoginID,  servubkup.userdiraccess.Dir asc;