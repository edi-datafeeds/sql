--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.703
--suffix=
--fileheadertext=EDI_UT_703_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\ut\703\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT * 
FROM v54f_701_Dividend_extended
WHERE CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
ORDER BY EventID desc
