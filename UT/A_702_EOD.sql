--filepath=o:\datafeed\UT\702\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.702
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\702\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n




--# 1
USE WCA
SELECT * 
FROM v54f_701_Dividend

WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 1)
ORDER BY EventID
