--filepath=o:\Datafeed\Warrant\609\
--filenameprefix=
--filename=yyyymmdd_WAREX
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_WAREX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\warrant\609\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select * from v51f_609_warrant_exercise
where changed >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
and (primaryexchgcd = exchgcd or exchgcd is null)
