--filepath=o:\Datafeed\Warrant\WARfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_CWCHG
--fileheadertext=EDI_WAR_CWCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('CWCHG')asTablename,
cwchg.Actflag,
cwchg.Announcedate,
cwchg.Acttime,
cwchg.SecID,
cwchg.NotificationDate,
cwchg.EventType as RelatedEvent,
cwchg.OldCallPut,
cwchg.NewCallPut,
cwchg.OldUnSecID,
cwchg.NewUnSecID,
cwchg.OldExpirationDate,
cwchg.NewExpirationDate,
cwchg.OldCurenCD,
cwchg.NewCurenCD,
cwchg.OldStrikePrice,
cwchg.NewStrikePrice,
cwchg.OldUnRatio,
cwchg.NewUnRatio,
cwchg.OldWarrantRatio,
cwchg.NewWarrantRatio,
cwchg.OldExerciseStyle,
cwchg.NewExerciseStyle,
cwchg.OldExerciseIn,
cwchg.NewExerciseIn,
cwchg.CwchgID,
cwchg.OldCWType,
cwchg.NewCWType,
cwchg.OldKOPrice,
cwchg.NewKOPrice,
cwchg.Notes
FROM cwchg
inner join cowar on cwchg.secid = cowar.secid
WHERE cwchg.acttime >= (select max(acttime) from tbl_Opslog where seq = 3)
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''
