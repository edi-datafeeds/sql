--filepath=o:\Datafeed\Warrant\WARfeed\Refdata2\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_WARTM
--fileheadertext=EDI_WAR_WARTM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\Refdata2\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('WARTM') as Tablename,
WARTM.Actflag,
WARTM.Announcedate,
WARTM.Acttime,
WARTM.SecID,
WARTM.IssueDate,
WARTM.ExpirationDate,
WARTM.RedemptionDate,
WARTM.ExerciseStyle,
WARTM.WartmNotes as Notes
FROM WARTM
WHERE WARTM.acttime >= (select max(acttime) from tbl_Opslog where seq = 3)
