--filepath=o:\Datafeed\Warrant\WARfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_COWAR
--fileheadertext=EDI_WAR_COWAR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('COWAR')asTablename,
cowar.Actflag,
cowar.Announcedate,
cowar.Acttime,
cowar.SecID,
cowar.UnSecID,
cowar.CallPut,
cowar.ExpirationDate,
cowar.CurenCD,
cowar.StrikePrice,
cowar.UnRatio,
cowar.WarrantRatio,
cowar.ExerciseStyle,
cowar.ExerciseIn,
cowar.IssueDate,
cowar.OpenEnded,
cowar.CWType,
cowar.VarStrikePrice,
cowar.KOPrice,
cowar.Notes
FROM cowar
WHERE cowar.acttime >= (select max(acttime) from tbl_Opslog where seq = 3)
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''
