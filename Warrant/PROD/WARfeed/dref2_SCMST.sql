--filepath=o:\Datafeed\Warrant\WARfeed\Refdata2\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_WAR_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\Refdata2\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.Regs144a,
scmst.SharesOutstanding
FROM wartm
inner join scmst on wartm.secid = scmst.secid
WHERE scmst.acttime >= (select max(acttime) from tbl_Opslog where seq = 3)

union

SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.Regs144a,
scmst.SharesOutstanding
FROM warex
inner join scmst on warex.exerSecid = scmst.secid
WHERE scmst.acttime >= (select max(acttime) from tbl_Opslog where seq = 3)
order by sectycd, isin 
