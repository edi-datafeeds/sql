--filepath=o:\Datafeed\Warrant\WARfeed\Refdata2\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_WAR_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\Refdata2\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
exchg.CntryCD,
exchg.MIC
FROM wartm
inner join scexh on wartm.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
SCEXH.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

union

SELECT
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
exchg.CntryCD,
exchg.MIC
FROM warex
inner join scexh on warex.exerSecid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
SCEXH.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
