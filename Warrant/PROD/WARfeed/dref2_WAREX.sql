--filepath=o:\Datafeed\Warrant\WARfeed\Refdata2\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_WAREX
--fileheadertext=EDI_WAR_WAREX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\Refdata2\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('WAREX') as Tablename,
WAREX.Actflag,
WAREX.Announcedate,
WAREX.Acttime,
WAREX.WarexID,
WAREX.SecID,
WAREX.Fromdate,
WAREX.Todate,
WAREX.Rationew,
WAREX.Ratioold,
WAREX.CurenCD,
WAREX.StrikePrice,
WAREX.PricePerShare,
WAREX.ExerSecID
FROM WAREX
WHERE WAREX.acttime >= (select max(acttime) from tbl_Opslog where seq = 3)
and WAREX.Rationew<>''
and WAREX.Ratioold<>''
