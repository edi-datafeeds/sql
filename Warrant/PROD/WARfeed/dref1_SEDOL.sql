--filepath=o:\Datafeed\Warrant\WARfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_WAR_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use wca
select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM cowar
inner join sedol on cowar.secid = sedol.secid
where 
SEDOL.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''

union

select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM cowar
inner join sedol on cowar.unSecID = sedol.secid
where 
SEDOL.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''
