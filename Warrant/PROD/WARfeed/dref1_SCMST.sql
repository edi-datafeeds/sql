--filepath=o:\Datafeed\Warrant\WARfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_WAR_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.Regs144a,
scmst.SharesOutstanding
FROM cowar
inner join scmst on cowar.secid = scmst.secid
WHERE scmst.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and scmst.secid is not null
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''

union

SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.Regs144a,
scmst.SharesOutstanding
FROM cowar
inner join scmst on cowar.unSecID = scmst.secid
WHERE scmst.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and scmst.secid is not null
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''

order by sectycd, isin 