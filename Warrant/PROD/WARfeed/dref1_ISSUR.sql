--filepath=o:\Datafeed\Warrant\WARfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_WAR_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\WARfeed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp
FROM cowar
inner join scmst on cowar.Secid = scmst.secid
INNER JOIN issur ON SCMST.IssID = issur.IssID
where 
ISSUR.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''

union

SELECT
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp
FROM cowar
inner join scmst on cowar.unSecid = scmst.secid
INNER JOIN issur ON SCMST.IssID = issur.IssID
where 
ISSUR.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
and cowar.UnRatio <> ''
and cowar.WarrantRatio <> ''
