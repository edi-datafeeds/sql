--filepath=O:\Datafeed\FEDFUNDS\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.funds
--suffix=_FED
--fileheadertext=EDI_FEDFUNDS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\FedFunds\
--fieldheaders=y
--filetidy=Y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1

select fedfunds.funds.feeddate as Date,fedfunds.funds.daily as Daily,fedfunds.funds.low as Low,fedfunds.funds.high as High,fedfunds.funds.deviation as 'Standard Deviation',fedfunds.funds.target as 'Target Rate',fedfunds.lookups.description as Notes
from fedfunds.funds
left outer join fedfunds.lookups on fedfunds.funds.dateflag = fedfunds.lookups.code
where fedfunds.funds.feeddate=(select max(fedfunds.funds.feeddate) from fedfunds.funds limit 1) 