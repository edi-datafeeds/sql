--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('CTCHG') as TableName,
CTCHG.Actflag,
CTCHG.announcedate as Created,
CTCHG.Acttime as Changed, 
CTCHG.CtChgID,
CTCHG.SecID,
SCMST.ISIN,
CTCHG.EffectiveDate,
CTCHG.OldResultantRatio,
CTCHG.NewResultantRatio,
CTCHG.OldSecurityRatio,
CTCHG.NewSecurityRatio,
CTCHG.OldCurrency,
CTCHG.NewCurrency,
CTCHG.OldConversionPrice,
CTCHG.NewConversionPrice,
CTCHG.ResSectyCD,
CTCHG.OldResSecID,
CTCHG.NewResSecID,
CTCHG.EventType,
CTCHG.RelEventID,
CTCHG.OldFromDate,
CTCHG.NewFromDate,
CTCHG.OldTodate,
CTCHG.NewToDate,
CTCHG.ConvtID,
CTCHG.OldFXRate,
CTCHG.NewFXRate,
CTCHG.OldPriceAsPercent,
CTCHG.NewPriceAsPercent,
CTCHG.Notes
FROM CTCHG
INNER JOIN BOND ON CTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
and CTCHG.announcedate between '2012/11/01' and '2012/11/30'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
