--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=EDI_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('LSTAT') as TableName,
LSTAT.Actflag,
LSTAT.announcedate as Created,
LSTAT.Acttime as Changed,
LSTAT.LstatID,
SCMST.SecID,
SCMST.ISIN,
LSTAT.ExchgCD,
LSTAT.NotificationDate,
LSTAT.EffectiveDate,
LSTAT.LStatStatus,
LSTAT.EventType,
LSTAT.Reason
FROM LSTAT
INNER JOIN BOND ON LSTAT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
and LSTAT.announcedate between '2012/11/01' and '2012/11/30'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
