--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('SFUND') as TableName,
SFUND.Actflag,
SFUND.AnnounceDate as Created,
SFUND.Acttime as Changed,
SFUND.SfundID,
SFUND.SecID,
SCMST.ISIN,
SFUND.SfundDate,
SFUND.CurenCD as Sinking_Currency,
SFUND.Amount,
SFUND.AmountasPercent,
SFUND.SfundNotes as Notes
FROM SFUND
INNER JOIN BOND ON SFUND.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
SFUND.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and SFUND.actflag<> 'D'

