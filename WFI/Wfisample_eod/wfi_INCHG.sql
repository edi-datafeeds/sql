--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT
upper('INCHG') as TableName,
INCHG.Actflag,
INCHG.AnnounceDate as Created,
INCHG.Acttime as Changed,
INCHG.InChgID,
BOND.SecID,
SCMST.ISIN,
INCHG.IssID,
INCHG.InChgDate,
INCHG.OldCntryCD,
INCHG.NewCntryCD,
INCHG.EventType 
FROM INCHG
INNER JOIN SCMST ON INCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
INCHG.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (bond.issuedate<=inchg.inchgdate
     or bond.issuedate is null or inchg.inchgdate is null
    )
and eventtype<>'CLEAN'
and eventtype<>'CORR'
