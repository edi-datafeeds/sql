--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.CpoptID,
SCMST.SecID,
SCMST.ISIN,
CPOPT.CallPut, 
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.CPType,
CPOPT.PriceAsPercent,
CPOPT.InWholePart,
CPOPT.FormulaBasedPrice,
CPOPT.Notes as Notes 
FROM CPOPT
INNER JOIN BOND ON CPOPT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
CPOPT.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
