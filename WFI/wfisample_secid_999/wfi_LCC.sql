--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LCC
--fileheadertext=EDI_LCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('LCC') as TableName,
LCC.Actflag,
LCC.AnnounceDate as Created,
LCC.Acttime as Changed,
LCC.LccID,
LCC.SecID,
SCMST.ISIN,
LCC.ExchgCD,
LCC.EffectiveDate,
LCC.OldLocalCode,
LCC.NewLocalCode,
LCC.EventType,
LCC.RelEventID
FROM LCC
INNER JOIN BOND ON LCC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
--and LCC.acttime> '2004/01/01'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
