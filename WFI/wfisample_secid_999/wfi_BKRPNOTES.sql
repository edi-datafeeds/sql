--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
select
upper('BKRPNOTES') as TableName,
BKRP.Actflag,
BKRP.BkrpID,
BKRP.BkrpNotes as Notes
FROM BKRP
where
BKRP.Issid in (select wca.dbo.scmst.issid from client.dbo.pfsecid
inner join wca.dbo.scmst on client.dbo.pfsecid.code = wca.dbo.scmst.secid
                                        and client.dbo.pfsecid.accid=999 and client.dbo.pfsecid.actflag<>'D')
--and BKRP.acttime> '2004/01/01'
