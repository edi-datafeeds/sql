--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SELRT
--fileheadertext=EDI_SELRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('SELRT') as TableName,
SELRT.Actflag,
SELRT.AnnounceDate as Created,
SELRT.Acttime as Changed,
SELRT.SelrtID,
SELRT.SecID,
SCMST.ISIN,
SELRT.CntryCD,
SELRT.Restriction
FROM SELRT
INNER JOIN BOND ON SELRT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
--and SELRT.acttime> '2004/01/01'
