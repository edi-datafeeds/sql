--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('BOND') as Tablename,
BOND.Actflag,
BOND.AnnounceDate as Created,
BOND.Acttime as Changed,
BOND.SecID,
SCMST.ISIN,
BOND.BondType,
BOND.DebtMarket,
BOND.CurenCD as DebtCurrency,
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as NominalValue,
BOND.IssueDate,
BOND.IssueCurrency,
case when bond.IssuePrice<>'' then cast(bond.Issueprice as decimal(18,4))
     when rtrim(bond.largeparvalue)='' then null
     when rtrim(BOND.PriceAsPercent)='' then null
     else cast(bond.largeparvalue as decimal(18,0)) * cast(wca.dbo.BOND.PriceAsPercent as decimal(18,4))/100 
     end as IssuePrice,
BOND.IssueAmount,
BOND.IssueAmountDate,
BOND.OutstandingAmount,
BOND.OutstandingAmountDate,
BOND.InterestBasis,
BOND.InterestRate,
BOND.InterestAccrualConvention,
BOND.InterestPaymentFrequency,
BOND.IntCommencementDate as InterestCommencementDate,
BOND.FirstCouponDate,
BOND.InterestPayDate1,
BOND.InterestPayDate2,
BOND.InterestPayDate3,
BOND.InterestPayDate4,
BOND.DomesticTaxRate,
BOND.NonResidentTaxRate,
BOND.FRNType,
BOND.FRNIndexBenchmark,
BOND.Markup as FrnMargin,
BOND.MinimumInterestRate as FrnMinInterestRate,
BOND.MaximumInterestRate as FrnMaxInterestRate,
BOND.Rounding as FrnRounding,
BOND.Series,
BOND.Class,
BOND.OnTap,
BOND.MaximumTapAmount,
BOND.TapExpiryDate,
BOND.Guaranteed,
BOND.SecuredBy,
BOND.SecurityCharge,
BOND.Subordinate,
BOND.SeniorJunior,
BOND.WarrantAttached,
BOND.MaturityStructure,
BOND.Perpetual,
BOND.MaturityDate,
BOND.MaturityExtendible,
BOND.Callable,
BOND.Puttable,
BOND.Denomination1,
BOND.Denomination2,
BOND.Denomination3,
BOND.Denomination4,
BOND.Denomination5,
BOND.Denomination6,
BOND.Denomination7,
BOND.MinimumDenomination,
BOND.DenominationMultiple,
BOND.Strip,
BOND.StripInterestNumber, 
BOND.Bondsrc,
BOND.MaturityBenchmark,
BOND.ConventionMethod,
BOND.FrnIntAdjFreq as FrnInterestAdjFreq,
BOND.IntBusDayConv as InterestBusDayConv,
BOND.InterestCurrency,
BOND.MatBusDayConv as MaturityBusDayConv,
BOND.MaturityCurrency, 
BOND.TaxRules,
BOND.VarIntPayDate as VarInterestPaydate,
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
case when bond.matprice<>'' then cast(bond.matprice as decimal(18,4))
     when rtrim(bond.largeparvalue)='' then null
     when rtrim(BOND.MatPriceAsPercent)='' then null
     else cast(bond.largeparvalue as decimal(18,0)) * cast(wca.dbo.BOND.MatPriceAsPercent as decimal(18,4))/100 
     end as MaturityPrice,
BOND.MatPriceAsPercent as MaturityPriceAsPercent,
BOND.SinkingFund,
BOND.GoverningLaw,
BOND.Municipal,
BOND.PrivatePlacement,
BOND.Syndicated,
BOND.Tier,
BOND.UppLow,
BOND.Collateral,
BOND.CoverPool,
BOND.PikPay,
BOND.Notes,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
ISSUR.Shortname,
ISSUR.LegalName,
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN ISSUR.Isstype='GOV' THEN 'G' WHEN ISSUR.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype
SCMST.IssID,
SCMST.SectyCD,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.X as CommonCode,
SCMST.Holding,
SCMST.StructCD,
SCMST.RegS144A
FROM BOND
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = scmst.issid
where
BOND.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
--and bond.acttime> '2004/01/01'