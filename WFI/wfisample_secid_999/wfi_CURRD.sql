--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('CURRD') as TableName,
CURRD.Actflag,
CURRD.AnnounceDate as Created,
CURRD.Acttime as Changed,
CURRD.CurrdID,
CURRD.SecID,
SCMST.ISIN,
CURRD.EffectiveDate,
CURRD.OldCurenCD,
CURRD.NewCurenCD,
CURRD.OldParValue,
CURRD.NewParValue,
CURRD.EventType,
CURRD.CurRdNotes as Notes
FROM CURRD
INNER JOIN BOND ON CURRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
and CURRD.actflag<>'D'
--and CURRD.acttime> '2004/01/01'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
