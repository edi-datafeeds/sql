--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=EDI_INTBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('INTBC') as TableName,
INTBC.Actflag,
INTBC.AnnounceDate as Created,
INTBC.Acttime as Changed,
INTBC.IntbcID,
INTBC.SecID,
SCMST.ISIN,
INTBC.EffectiveDate,
INTBC.OldIntBasis,
INTBC.NewIntBasis,
INTBC.OldIntBDC,
INTBC.NewIntBDC
FROM INTBC
INNER JOIN BOND ON INTBC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
--and INTBC.acttime> '2004/01/01'
