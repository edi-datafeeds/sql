--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=EDI_INTBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('INTBC') as TableName,
INTBC.Actflag,
INTBC.AnnounceDate as Created,
INTBC.Acttime as Changed,
INTBC.IntbcID,
INTBC.SecID,
SCMST.ISIN,
INTBC.EffectiveDate,
INTBC.OldIntBasis,
INTBC.NewIntBasis,
INTBC.OldIntBDC,
INTBC.NewIntBDC
FROM INTBC
INNER JOIN BOND ON INTBC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
INTBC.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)