--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BDC
--fileheadertext=EDI_BDC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BDC') as TableName,
BDC.Actflag,
BDC.AnnounceDate as Created,
BDC.Acttime as Changed,
BDC.BdcID,
BDC.SecID,
SCMST.ISIN,
BDC.BDCAppliedTo,
BDC.CntrID,
BDC.Notes
FROM BDC
INNER JOIN BOND ON BDC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BDC.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)