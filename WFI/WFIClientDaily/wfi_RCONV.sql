--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_RCONV
--fileheadertext=EDI_RCONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('RCONV') as TableName,
RCONV.Actflag,
RCONV.AnnounceDate as Created,
RCONV.Acttime as Changed,
RCONV.RconvID,
RCONV.SecID,
SCMST.ISIN,
RCONV.EffectiveDate,
RCONV.OldInterestAccrualConvention,
RCONV.NewInterestAccrualConvention,
RCONV.OldConvMethod,
RCONV.NewConvMethod,
RCONV.Eventtype,
RCONV.Notes
FROM RCONV
INNER JOIN BOND ON RCONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
RCONV.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'