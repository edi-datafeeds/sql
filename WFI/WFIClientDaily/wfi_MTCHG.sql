--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('MTCHG') as TableName,
MTCHG.Actflag,
MTCHG.AnnounceDate as Created,
MTCHG.Acttime as Changed,
MTCHG.MtChgID,
MTCHG.SecID,
SCMST.ISIN,
MTCHG.NotificationDate,
MTCHG.OldMaturityDate,
MTCHG.NewMaturityDate,
MTCHG.Reason,
MTCHG.EventType,
MTCHG.OldMaturityBenchmark,
MTCHG.NewMaturityBenchmark,
MTCHG.Notes as Notes
FROM MTCHG
INNER JOIN BOND ON MTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
MTCHG.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'