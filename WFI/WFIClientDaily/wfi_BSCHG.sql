--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BSCHG
--fileheadertext=EDI_BSCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select
upper('BSCHG') as TableName,
BSCHG.Actflag,
BSCHG.AnnounceDate as Created,
BSCHG.Acttime as Changed,
BSCHG.BschgID,
BSCHG.SecID,
SCMST.ISIN,
BSCHG.NotificationDate,
BSCHG.OldBondType,
BSCHG.NewBondType,
BSCHG.OldCurenCD,
BSCHG.NewCurenCD,
BSCHG.OldPIU,
BSCHG.NewPIU,
BSCHG.OldInterestBasis,
BSCHG.NewInterestBasis,
BSCHG.Eventtype,
BSCHG.OldInterestCurrency,
BSCHG.NewInterestCurrency,
BSCHG.OldMaturityCurrency,
BSCHG.NewMaturityCurrency,
BSCHG.OldIntBusDayConv,
BSCHG.NewIntBusDayConv,
BSCHG.OldMatBusDayConv,
BSCHG.NewMatBusDayConv,
BSCHG.BschgNotes as Notes
FROM BSCHG
INNER JOIN BOND ON BSCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BSCHG.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'