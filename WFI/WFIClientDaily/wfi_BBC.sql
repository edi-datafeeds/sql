--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
select distinct
upper('BBC') as tablename,
bbc.actflag,
bbc.announcedate,
bbc.acttime,
bbc.bbcid,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
bbc.bbgcompid,
bbc.bbgcomptk
from bbc
inner join scmst on bbc.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
BBC.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
