--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=EDI_CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CRDRT') as TableName,
CRDRT.Actflag,
CRDRT.AnnounceDate as Created,
CRDRT.Acttime as Changed, 
CRDRT.SecID,
CRDRT.RatingAgency,
CRDRT.RatingDate,
CRDRT.Rating,
CRDRT.Direction,
CRDRT.WatchList,
CRDRT.WatchListReason
FROM CRDRT
INNER JOIN BOND ON CRDRT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
CRDRT.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)

