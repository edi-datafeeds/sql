--filepath=O:\Datafeed\Debt\WFIClientDaily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIClientDaily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('WKNCH') as TableName,
WKNCH.Actflag,
WKNCH.AnnounceDate as Created,
WKNCH.Acttime as Changed,
WKNCH.SecID,
WKNCH.EffectiveDate,
WKNCH.WknChID,
WKNCH.Eventtype,
WKNCH.RelEventID,
WKNCH.OldWKN,
WKNCH.NewWKN
FROM WKNCH
INNER JOIN BOND ON WKNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
WKNCH.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)

