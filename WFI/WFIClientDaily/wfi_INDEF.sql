--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INDEF
--fileheadertext=EDI_INDEF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'INDEF' as TableName,
INDEF.Actflag,
INDEF.AnnounceDate as Created,
INDEF.Acttime as Changed,
INDEF.IndefID,
INDEF.SecID,
SCMST.Isin,
INDEF.DefaultType,
INDEF.DefaultDate,
INDEF.Notes
FROM INDEF
INNER JOIN BOND ON INDEF.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
INDEF.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)