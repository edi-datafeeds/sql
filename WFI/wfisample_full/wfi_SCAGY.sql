--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('SCAGY') as TableName,
SCAGY.Actflag,
SCAGY.AnnounceDate as Created,
SCAGY.Acttime as Changed,
SCAGY.ScagyID,
SCAGY.SecID,
SCMST.ISIN,
SCAGY.Relationship,
SCAGY.AgncyID,
SCAGY.GuaranteeType,
SCAGY.SpStartDate,
SCAGY.SpEndDate,
SCAGY.Notes
FROM SCAGY
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
SCAGY.actflag<>'D'
and scmst.statusflag<>'I'