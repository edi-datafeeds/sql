--filepath=o:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\iods\
--FIELDHEADERS=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#1 
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.CpoptID,
SCMST.SecID,
SCMST.ISIN,
CPOPT.CallPut, 
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.CPType,
CPOPT.PriceAsPercent,
CPOPT.InWholePart,
CPOPT.FormulaBasedPrice,
CPOPN.Notes as Notes 
FROM CPOPT
INNER JOIN SCMST ON CPOPT.SecID = SCMST.SecID
LEFT OUTER JOIN CPOPN ON CPOPT.SECID = CPOPN.SECID AND CPOPT.CALLPUT = CPOPN.CALLPUT
where
cpopt.acttime>(select max(wca.dbo.tbl_opslog.acttime)-0.1 from wca.dbo.tbl_opslog where seq = 3)
