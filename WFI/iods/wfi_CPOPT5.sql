--filepath=o:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPT05
--fileheadertext=EDI_CPOPT05_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\iods\
--FIELDHEADERS=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--#5
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.CpoptID,
SCMST.SecID,
SCMST.ISIN,
CPOPT.CallPut, 
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.CPType,
CPOPT.PriceAsPercent,
CPOPT.InWholePart,
CPOPT.FormulaBasedPrice,
CPOPT.Notes as Notes 
FROM CPOPT
INNER JOIN SCMST ON cpopt.SecID = SCMST.SecID
where
cpopt.actflag<>'D'
and cpoptid>=2767713 and cpoptid < 3319406
