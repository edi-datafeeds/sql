--filepath=o:\datafeed\wfi\iods\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\iods\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('SDCHG') as TableName,
SDCHG.Actflag,
SDCHG.AnnounceDate as Created, 
SDCHG.Acttime as Changed,
SDCHG.SdChgID,
SDCHG.SecID,
SCMST.ISIN,
SDCHG.EffectiveDate,
SDCHG.EventType,
SDCHG.OldSedol,
SDCHG.NewSedol,
SDCHG.CntryCD as OldCntryCD,
SDCHG.NewCntryCD,
SDCHG.RcntryCD as OldRcntryCD,
SDCHG.NewRcntryCD
FROM SDCHG
INNER JOIN BOND ON SDCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
sdchg.acttime>(select max(wca.dbo.tbl_opslog.acttime)-0.1 from wca.dbo.tbl_opslog where seq = 3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'
