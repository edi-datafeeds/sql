--filepath=o:\datafeed\wfi\iods\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\iods\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate as Created,
SEDOL.Acttime as Changed, 
SEDOL.SedolId,
SEDOL.SecID,
SCMST.ISIN,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD 
FROM SEDOL
INNER JOIN BOND ON SEDOL.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
sedol.acttime>(select max(wca.dbo.tbl_opslog.acttime)-0.1 from wca.dbo.tbl_opslog where seq = 3)
