--filepath=o:\datafeed\wfi\iods\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ICC
--fileheadertext=EDI_ICC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\iods\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('ICC') as TableName,
ICC.Actflag,
ICC.AnnounceDate as Created,
ICC.Acttime as Changed,
ICC.IccID,
ICC.SecID,
SCMST.ISIN,
ICC.EffectiveDate,
ICC.OldISIN,
ICC.NewISIN,
ICC.OldUSCode,
ICC.NewUSCode,
ICC.OldVALOREN,
ICC.NewVALOREN,
ICC.EventType,
ICC.RelEventID,
ICC.OldCommonCode,
ICC.NewCommonCode
FROM ICC
INNER JOIN BOND ON ICC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
icc.acttime>(select max(wca.dbo.tbl_opslog.acttime)-0.1 from wca.dbo.tbl_opslog where seq = 3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'
