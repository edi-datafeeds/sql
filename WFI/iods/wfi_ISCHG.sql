--filepath=o:\datafeed\wfi\iods\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\iods\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('ISCHG') as TableName,
ISCHG.Actflag,
ISCHG.AnnounceDate as Created,
ISCHG.Acttime as Changed,
ISCHG.IschgID,
BOND.SecID,
SCMST.ISIN,
ISCHG.IssID,
ISCHG.NameChangeDate,
ISCHG.IssOldName,
ISCHG.IssNewName,
ISCHG.EventType,
ISCHG.LegalName,
ISCHG.MeetingDateFlag, 
ISCHG.IsChgNotes as Notes
FROM ISCHG
INNER JOIN SCMST ON ISCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
ischg.acttime>(select max(wca.dbo.tbl_opslog.acttime)-0.1 from wca.dbo.tbl_opslog where seq = 3)
and (bond.issuedate<=ischg.namechangedate
     or bond.issuedate is null or ischg.namechangedate is null
    )
and eventtype<>'CLEAN'
and eventtype<>'CORR'
