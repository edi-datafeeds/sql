use wca
select
scmst.SecID,
portfolio.dbo.fisin.code as ISIN,
case when cpct.ct = 1  and (futfrom.fromdate = futfrom.todate or futfrom.todate is null) then 'EU'
     when futto.fromdate<futfrom.fromdate and futto.fromdate <> futto.todate and futto.todate is not null then 'US'
     when futfrom.fromdate <> futfrom.todate and futfrom.todate is not null then 'US'
     when cpct.ct > 1 then 'BM'
     else null
     end as Call_Frequency,
case when cpct.ct = 1  and (futfrom.fromdate = futfrom.todate or futfrom.todate is null) then futfrom.Fromdate
     when futto.fromdate<futfrom.fromdate and futto.fromdate <> futto.todate and futto.todate is not null then null
     when futfrom.fromdate <> futfrom.todate and futfrom.todate is not null then null
     when cpct.ct > 1 then futfrom.Fromdate
     else null
     end as Next_Call_Date,
case when cpct.ct = 1  and (futfrom.fromdate = futfrom.todate or futfrom.todate is null) then null
     when futto.fromdate<futfrom.fromdate and futto.fromdate <> futto.todate and futto.todate is not null then futto.fromdate
     when futfrom.fromdate <> futfrom.todate and futfrom.todate is not null then futfrom.fromdate
     else null
     end as From_Date,
case when cpct.ct = 1  and (futfrom.fromdate = futfrom.todate or futfrom.todate is null) then null
     when futto.fromdate<futfrom.fromdate and futto.fromdate <> futto.todate and futto.todate is not null then futto.Todate
     when futfrom.fromdate <> futfrom.todate and futfrom.todate is not null then futfrom.Todate
     else null
     end as To_Date,
case when koct.ct = 1  and (futfrom.fromdate = KOfutfrom.todate or KOfutfrom.todate is null) then 'EU'
     when KOfutto.fromdate<futfrom.fromdate and KOfutto.fromdate <> KOfutto.todate and KOfutto.todate is not null then 'US'
     when KOfutfrom.fromdate <> KOfutfrom.todate and KOfutfrom.todate is not null then 'US'
     when koct.ct > 1 then 'BM'
     else null
     end as KO_Call_Frequency,
case when koct.ct = 1  and (futfrom.fromdate = KOfutfrom.todate or KOfutfrom.todate is null) then KOfutfrom.Fromdate
     when KOfutto.fromdate<futfrom.fromdate and KOfutto.fromdate <> KOfutto.todate and KOfutto.todate is not null then null
     when KOfutfrom.fromdate <> KOfutfrom.todate and KOfutfrom.todate is not null then null
     when koct.ct > 1 then KOfutfrom.Fromdate
     else null
     end as KO_Next_Call_Date,
case when koct.ct = 1  and (futfrom.fromdate = KOfutfrom.todate or KOfutfrom.todate is null) then null
     when KOfutto.fromdate<futfrom.fromdate and KOfutto.fromdate <> KOfutto.todate and KOfutto.todate is not null then KOfutto.fromdate
     when KOfutfrom.fromdate <> KOfutfrom.todate and KOfutfrom.todate is not null then KOfutfrom.fromdate
     else null
     end as KO_From_Date,
case when koct.ct = 1  and (futfrom.fromdate = KOfutfrom.todate or KOfutfrom.todate is null) then null
     when KOfutto.fromdate<futfrom.fromdate and KOfutto.fromdate <> KOfutto.todate and KOfutto.todate is not null then KOfutto.Todate
     when KOfutfrom.fromdate <> KOfutfrom.todate and KOfutfrom.todate is not null then KOfutfrom.Todate
     else null
     end as KO_To_Date,
from portfolio.dbo.fisin
left outer join scmst on portfolio.dbo.fisin.code = Isin
left outer join bond on scmst.secid = bond.secid
left outer join portfolio.dbo.cpopt_ct as cpct on scmst.secid = cpct.secid
left outer join portfolio.dbo.cpopt_ko_ct as koct on scmst.secid = koct.secid
left outer join cpopt_FUTURE as futfrom on cpct.secid = futfrom.secid
left outer join cpopt_FUTURE_todate as futto on cpct.secid = futto.secid
left outer join cpopt on futfrom.cpoptid = cpopt.cpoptid
left outer join cpopt_FUTURE as kofutfrom on koct.secid = kofutfrom.secid
left outer join cpopt_FUTURE_todate as kofutto on koct.secid = kofutto.secid
where 
portfolio.dbo.fisin.accid  = 185
and portfolio.dbo.fisin.Actflag<>'D'
and scmst.secid is not null
and bond.callable<>'N'
order by portfolio.dbo.fisin.code
