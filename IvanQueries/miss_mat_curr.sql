use wca
select scmst.secid, scmst.isin, bond.maturitydate, bond.perpetual
from scmst
inner join bond on scmst.secid = bond.secid
inner join sedol on bond.secid = sedol.secid
where
scmst.isin in (select col001 from portfolio.dbo.matcodes)
or scmst.uscode in (select col001 from portfolio.dbo.matcodes)
or sedol.sedol in (select col001 from portfolio.dbo.matcodes)

use wca
select scmst.secid, scmst.isin, bond.curencd, bond.issuecurrency
from scmst
inner join bond on scmst.secid = bond.secid
inner join sedol on bond.secid = sedol.secid
where
scmst.isin in (select col001 from portfolio.dbo.currcodes)
or scmst.uscode in (select col001 from portfolio.dbo.currcodes)
or sedol.sedol in (select col001 from portfolio.dbo.currcodes)
