--use wca
--select * from sedol
--where sedol is null
--order by acttime

update sedol
set sedol =''
where sedol is null


--use wca
--select * from scmst
--where statusflag is null
--order by acttime

update scmst
set statusflag =''
where statusflag is null

update scmst
set statusreason =''
where statusreason is null

update scmst
set voting =''
where voting is null

update scmst
set uscode =''
where uscode is null

update scmst
set isin =''
where isin is null

update scmst
set valoren =''
where valoren is null

update scmst
set x =''
where x is null

update scmst
set cfi =''
where cfi is null

update scmst
set holding =''
where holding is null

update scmst
set wkn =''
where wkn is null

update scmst
set regs144a =''
where regs144a is null

update scmst
set structcd =''
where structcd is null



--use wca
--select * from scexh
--where rtscd is null
--order by acttime

update scexh
set liststatus =''
where liststatus is null

update scexh
set tradestatus =''
where tradestatus is null

update scexh
set localcode =''
where localcode is null

update scexh
set junklocalcode =''
where junklocalcode is null

update scexh
set mintrdgqty =''
where mintrdgqty is null

update scexh
set rtscd =''
where rtscd is null

update scexh
set bltick =''
where bltick is null