USE [CorporateActions]
GO

/****** Object:  StoredProcedure [dbo].[usp_MergeAllData]    Script Date: 05/23/2013 15:19:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_MergeAllData]
AS
SET NOCOUNT ON;
/*
Allows for the fake MERGE of data from AllDataWorking into AllData.  This is necessary because the source table is all VARCHAR(255), and we want to put the data in clean.
Always run on only a single day of data.

EXEC dbo.usp_MergeAllData

Auth:  RLewis
Date:  2/12/2013
*/
BEGIN 
	BEGIN TRY
			/* Count variables */
			DECLARE 
				@DelCount INT,
				@InsCount INT,
				@Message VARCHAR(2000)
	
			/* Check for existing data, and delete. 
			IF EXISTS(SELECT 1 FROM dbo.AllDataWorking a 
			WHERE EXISTS(
				SELECT 1 FROM dbo.AllData b
				WHERE CAST(EventCD AS VARCHAR(10)) = b.EventCD
				AND CAST(EventID AS VARCHAR(10)) = b.EventID
				AND CAST(OptionID AS VARCHAR(10)) = b.OptionID
				AND CAST(SerialID AS VARCHAR(10)) = b.SerialID
				AND CAST(ScexhID AS VARCHAR(10)) = b.ScexhID)
			)
			BEGIN
				DELETE b
				FROM dbo.AllDataWorking a JOIN dbo.AllData b
				  ON CAST(a.EventCD AS VARCHAR(10)) = b.EventCD
				  AND CAST(a.EventID AS VARCHAR(10)) = b.EventID
				  AND CAST(a.OptionID AS VARCHAR(10)) = b.OptionID
				  AND CAST(a.SerialID AS VARCHAR(10)) = b.SerialID
				  AND CAST(a.ScexhID AS VARCHAR(10)) = b.ScexhID
			
				  SET @DelCount = @@ROWCOUNT
			END   */
 
 
 			/* Identify duplicates from working dataset. */
			DECLARE @AllDataDupes TABLE (
				EventCD varchar(10),EventID int,OptionID int,SerialID int,ScexhID int,Actflag char(1),Changed datetime,Created date,SecID int,IssID int,ISIN char(12),
				USCode char(9),IssuerName varchar(70),CntryofIncorp char(2),SectyCD char(3),SecurityDesc varchar(70),ParValue decimal(14, 5),PVCurrency char(3),StatusFlag char(1),
				PrimaryExchgCD char(6),ExchgCntry char(2),ExchgCD char(6),MIC char(4),MICSeg char(4),LocalCode varchar(50),ListStatus char(1),Date1Type varchar(20),Date1 date,Date2Type varchar(20),
				Date2 date,Date3Type varchar(20),Date3 date,Date4Type varchar(20),Date4 date,Date5Type varchar(20),Date5 date,Date6Type varchar(20),Date6 date,Date7Type varchar(20),Date7 date,
				Date8Type varchar(20),Date8 date,Date9Type varchar(20),Date9 date,Date10Type varchar(20),Date10 date,Date11Type varchar(20),Date11 date,Date12Type varchar(20),Date12 date,
				Paytype char(1),[Priority] char(1),DefaultOpt char(10),OutTurnSecID char(1),OutTurnISIN char(12),RatioOld decimal(15, 7),RatioNew decimal(15, 7),Fractions char(1),Currency char(3),
				Rate1Type varchar(20),Rate1 decimal(18, 9),Rate2Type varchar(20),Rate2 decimal(18, 9),Field1Name varchar(20),Field1 varchar(255),Field2Name varchar(20),Field2 varchar(255),
				Field3Name varchar(20),Field3 varchar(255),Field4Name varchar(20),Field4 varchar(255),Field5Name varchar(20),Field5 varchar(255),Field6Name varchar(20),Field6 varchar(255),
				Field7Name varchar(20),Field7 varchar(255),Field8Name varchar(20),Field8 varchar(255),Field9Name varchar(20),Field9 varchar(255),Field10Name varchar(20),Field10 varchar(255),
				Field11Name varchar(20),Field11 varchar(255),Field12Name varchar(20),Field12 varchar(255),Field13Name varchar(20),Field13 varchar(255),Field14Name varchar(20),Field14 varchar(255),
				Field15Name varchar(20),Field15 varchar(255),Field16Name varchar(20),Field16 varchar(255),Field17Name varchar(20),Field17 varchar(255),Field18Name varchar(20),Field18 varchar(255),			
				Field19Name varchar(20),Field19 varchar(255),Field20Name varchar(20),Field20 varchar(255),Field21Name varchar(20),Field21 varchar(255),Field22Name varchar(20),Field22 varchar(255),
				RowNumber INT)
			INSERT @AllDataDupes (
				EventCD,EventID,OptionID,SerialID,ScexhID,Actflag,Changed,Created,SecID,IssID,ISIN,USCode,IssuerName,CntryofIncorp,SectyCD,SecurityDesc,ParValue,PVCurrency,StatusFlag,
				PrimaryExchgCD,ExchgCntry,ExchgCD,MIC,MICSeg,LocalCode,ListStatus,Date1Type,Date1,Date2Type,Date2,Date3Type,Date3,Date4Type,Date4,Date5Type,Date5,Date6Type,Date6,Date7Type,
				Date7,Date8Type,Date8,Date9Type,Date9,Date10Type,Date10,Date11Type,Date11,Date12Type,Date12,Paytype,[Priority],DefaultOpt,OutTurnSecID,OutTurnISIN,RatioOld,RatioNew,
				Fractions,Currency,Rate1Type,Rate1,Rate2Type,Rate2,Field1Name,Field1,Field2Name,Field2,Field3Name,Field3,Field4Name,Field4,Field5Name,Field5,Field6Name,Field6,Field7Name,Field7,
				Field8Name,Field8,Field9Name,Field9,Field10Name,Field10,Field11Name,Field11,Field12Name,Field12,Field13Name,Field13,Field14Name,Field14,Field15Name,Field15,Field16Name,Field16,
				Field17Name,Field17,Field18Name,Field18,Field19Name,Field19,Field20Name,Field20,Field21Name,Field21,Field22Name,Field22,
				RowNumber 	)
			SELECT -- column alias's not necessary. only in place to help w/diagnostics, if needed.
				CAST(a.EventCD AS VARCHAR(10)) [EventCD],
				CAST(a.EventID AS INT) [EventID],
				CAST(a.OptionID AS INT) [OptionID],
				CAST(a.SerialID AS INT) [SerialID],
				CAST(a.ScexhID AS INT) [ScexhID],
				CAST(Actflag AS CHAR(1)) [ActFlag],
				CASE WHEN ISDATE(Changed) = 1 THEN CONVERT(DATE,Changed) ELSE NULL END [Changed],
				CASE WHEN ISDATE(Created) = 1 THEN CONVERT(DATE,Created) ELSE NULL END [Created],
				CAST(SecID AS INT) [SecID],
				CAST(IssID AS INT) [IssID],
				CAST(ISIN AS CHAR(12)) [ISIN],
				CAST(USCode AS CHAR(9)) [USCode],
				CAST(IssuerName AS VARCHAR(70)) [IssuerName],
				CAST(CntryofIncorp AS CHAR(2)) [CntryOfIncorp],
				CAST(SectyCD AS CHAR(3)) [SectyCD],
				CAST(SecurityDesc AS VARCHAR(70)) [SecurityDesc],
				CASE WHEN ParValue = '' THEN 0 ELSE CAST(ParValue AS DECIMAL(14,5)) END [ParValue],
				CAST(PVCurrency AS CHAR(3)) [PVCurrency],
				CAST(StatusFlag AS CHAR(1)) [StatusFlag],
				CAST(PrimaryExchgCD AS CHAR(6))[PrimaryExchCD],
				CAST(ExchgCntry AS CHAR(2))[ExchCntry],
				CAST(ExchgCD AS CHAR(6))[ExchCD],
				CAST(MIC AS CHAR(4))[MIC],
				CAST(MICSeg AS CHAR(4))[MICSeg],
				CAST(LocalCode AS VARCHAR(50))[LocalCode],
				CAST(ListStatus AS CHAR(1))[ListStatus],
				CAST(Date1Type AS VARCHAR(20))[DateType],
				CASE WHEN ISDATE(Date1) = 1 THEN CONVERT(DATE,Date1) ELSE NULL END [Date1],
				CAST(Date2Type AS VARCHAR(20))[Date2Type],
--				CONVERT(VARCHAR(10),Date2,121)[Date2],
				CASE WHEN ISDATE(Date2) = 1 THEN CONVERT(DATE,Date2) ELSE NULL END [Date2],
				CAST(Date3Type AS VARCHAR(20))[Date3Type],
				CASE WHEN ISDATE(Date3) = 1 THEN CONVERT(DATE,Date3) ELSE NULL END [Date3],
				CAST(Date4Type AS VARCHAR(20))[Date4Type],
				CASE WHEN ISDATE(Date4) = 1 THEN CONVERT(DATE,Date4) ELSE NULL END [Date4],
				CAST(Date5Type AS VARCHAR(20))[Date5Type],
				CASE WHEN ISDATE(Date5) = 1 THEN CONVERT(DATE,Date5) ELSE NULL END [Date5],
				CAST(Date6Type AS VARCHAR(20))[Date6Type],
				CASE WHEN ISDATE(Date6) = 1 THEN CONVERT(DATE,Date6) ELSE NULL END [Date6],
				CAST(Date7Type AS VARCHAR(20))[Date7Type],
				CASE WHEN ISDATE(Date7) = 1 THEN CONVERT(DATE,Date7) ELSE NULL END [Date7],
				CAST(Date8Type AS VARCHAR(20))[Date8Type],
				CASE WHEN ISDATE(Date8) = 1 THEN CONVERT(DATE,Date8) ELSE NULL END [Date8],
				CAST(Date9Type AS VARCHAR(20))[Date9Type],
				CASE WHEN ISDATE(Date9) = 1 THEN CONVERT(DATE,Date9) ELSE NULL END [Date9],
				CAST(Date10Type AS VARCHAR(20))[Date10Type],
				CASE WHEN ISDATE(Date10) = 1 THEN CONVERT(DATE,Date10) ELSE NULL END [Date10],
				CAST(Date11Type AS VARCHAR(20))[Date11Type],
				CASE WHEN ISDATE(Date11) = 1 THEN CONVERT(DATE,Date11) ELSE NULL END [Date11],
				CAST(Date12Type AS VARCHAR(20))[Date12Type],
				CASE WHEN ISDATE(Date12) = 1 THEN CONVERT(DATE,Date12) ELSE NULL END [Date12],
				CAST(Paytype AS CHAR(1))[PayType],
				CAST([Priority] AS CHAR(10))[Priority],
				CASE WHEN DefaultOpt = '' THEN ' - ' ELSE CAST(DefaultOpt AS CHAR(1)) END [DefaultOpt],
				CAST(OutTurnSecID AS CHAR(1)) [OutTurnSecID],
				CAST(OutTurnIsin AS CHAR(12)) [OutTurnISIN],
				CASE WHEN RatioOld = '' THEN 0 ELSE CAST(RatioOld AS DECIMAL(15,7)) END [RatioOld],
				CASE WHEN RatioNew = '' THEN 0 ELSE CAST(RatioNew AS DECIMAL(15,7)) END [RatioNew],
				CAST(Fractions AS CHAR(1))[Fractions],
				CAST(Currency AS CHAR(3))[Currency],
				CAST(Rate1Type AS VARCHAR(20))[Rate1Type],
				CASE WHEN Rate1 = '' THEN 0 ELSE CAST(Rate1 AS DECIMAL(18,9))END [Rate1],
				CAST(Rate2Type AS VARCHAR(20))[Rate2Type],
				CASE WHEN Rate2 = '' THEN 0 ELSE CAST(Rate2 AS DECIMAL(18,9))END [Rate2],
				CAST(Field1Name AS VARCHAR(20))[FieldName],
				CAST(Field1 AS VARCHAR(255))[Field1],
				CAST(Field2Name AS VARCHAR(20))[Field2Name],
				CAST(Field2 AS VARCHAR(255))[Field2],
				CAST(Field3Name AS VARCHAR(20))[Field3Name],
				CAST(Field3 AS VARCHAR(255))[Field3],
				CAST(Field4Name AS VARCHAR(20))[Field4Name],
				CAST(Field4 AS VARCHAR(255))[Field4],
				CAST(Field5Name AS VARCHAR(20))[Field5Name],
				CAST(Field5 AS VARCHAR(255))[Field5],
				CAST(Field6Name AS VARCHAR(20))[Field6Name],
				CAST(Field6 AS VARCHAR(255))[Field6],
				CAST(Field7Name AS VARCHAR(20))[Field7Name],
				CAST(Field7 AS VARCHAR(255))[Field7],
				CAST(Field8Name AS VARCHAR(20))[Field8Name],
				CAST(Field8 AS VARCHAR(255))[Field8],
				CAST(Field9Name AS VARCHAR(20))[Field9Name],
				CAST(Field9 AS VARCHAR(255))[Field9],
				CAST(Field10Name AS VARCHAR(20))[Field10Name],
				CAST(Field10 AS VARCHAR(255))[Field10],
				CAST(Field11Name AS VARCHAR(20))[Field11Name],
				CAST(Field11 AS VARCHAR(255))[Field11],
				CAST(Field12Name AS VARCHAR(20))[Field12Name],
				CAST(Field12 AS VARCHAR(255))[Field12],
				CAST(Field13Name AS VARCHAR(20))[Field13Name],
				CAST(Field13 AS VARCHAR(255))[Field13],
				CAST(Field14Name AS VARCHAR(20))[Field14Name],
				CAST(Field14 AS VARCHAR(255))[Field14],
				CAST(Field15Name AS VARCHAR(20))[Field15Name],
				CAST(Field15 AS VARCHAR(255))[Field15],
				CAST(Field16Name AS VARCHAR(20))[Field16Name],
				CAST(Field16 AS VARCHAR(255))[Field16],
				CAST(Field17Name AS VARCHAR(20))[Fiedl17Name],
				CAST(Field17 AS VARCHAR(255))[Field17],
				CAST(Field18Name AS VARCHAR(20))[Fiedl18Name],
				CAST(Field18 AS VARCHAR(255))[Field18],
				CAST(Field19Name AS VARCHAR(20))[Field19Name],
				CAST(Field19 AS VARCHAR(255))[Field19],
				CAST(Field20Name AS VARCHAR(20))[Field20Name],
				CAST(Field20 AS VARCHAR(255))[Field20],
				CAST(Field21Name AS VARCHAR(20))[Field21Name],
				CAST(Field21 AS VARCHAR(255))[Field21],
				CAST(Field22Name AS VARCHAR(20))[Field22Name],
				CAST(Field22 AS VARCHAR(255))[Field22],
				ROW_NUMBER() OVER (PARTITION BY EventCD,EventID,OptionID,SerialID,ScexhID ORDER BY EventID,EventCD,OptionID,SerialID,ScexhID DESC) rn
			FROM
				dbo.AllDataWorking a

			/* Now write the data w/out the duplicates. */
			INSERT dbo.AllData (
				 EventCD,EventID,OptionID,SerialID,ScexhID,Actflag,Changed,Created,SecID,IssID,ISIN,USCode,IssuerName,CntryofIncorp,SectyCD,SecurityDesc,
				 ParValue,PVCurrency,StatusFlag,PrimaryExchgCD,ExchgCntry,ExchgCD,MIC,MICSeg,LocalCode,ListStatus,Date1Type,Date1,Date2Type,Date2,Date3Type,Date3,
				 Date4Type,Date4,Date5Type,Date5,Date6Type,Date6,Date7Type,Date7,Date8Type,Date8,Date9Type,Date9,Date10Type,Date10,Date11Type,Date11,Date12Type,Date12,
				 Paytype,[Priority],DefaultOpt,OutTurnSecID,OutTurnISIN,RatioOld,RatioNew,Fractions,Currency,Rate1Type,Rate1,Rate2Type,Rate2,Field1Name,Field1,
				 Field2Name,Field2,Field3Name,Field3,Field4Name,Field4,Field5Name,Field5,Field6Name,Field6,Field7Name,Field7,Field8Name,Field8,Field9Name,Field9,
				 Field10Name,Field10,Field11Name,Field11,Field12Name,Field12,Field13Name,Field13,Field14Name,Field14,Field15Name,Field15,Field16Name,Field16,
				 Field17Name,Field17,Field18Name,Field18,Field19Name,Field19,Field20Name,Field20,Field21Name,Field21,Field22Name,Field22
			  )
			SELECT 
				EventCD,EventID,OptionID,SerialID,ScexhID,Actflag,Changed,Created,SecID,IssID,ISIN,USCode,IssuerName,CntryofIncorp,SectyCD,SecurityDesc,ParValue,PVCurrency,StatusFlag,
				PrimaryExchgCD,ExchgCntry,ExchgCD,MIC,MICSeg,LocalCode,ListStatus,Date1Type,Date1,Date2Type,Date2,Date3Type,Date3,Date4Type,Date4,Date5Type,Date5,Date6Type,Date6,Date7Type,
				Date7,Date8Type,Date8,Date9Type,Date9,Date10Type,Date10,Date11Type,Date11,Date12Type,Date12,Paytype,[Priority],DefaultOpt,OutTurnSecID,OutTurnISIN,RatioOld,RatioNew,
				Fractions,Currency,Rate1Type,Rate1,Rate2Type,Rate2,Field1Name,Field1,Field2Name,Field2,Field3Name,Field3,Field4Name,Field4,Field5Name,Field5,Field6Name,Field6,Field7Name,Field7,
				Field8Name,Field8,Field9Name,Field9,Field10Name,Field10,Field11Name,Field11,Field12Name,Field12,Field13Name,Field13,Field14Name,Field14,Field15Name,Field15,Field16Name,Field16,
				Field17Name,Field17,Field18Name,Field18,Field19Name,Field19,Field20Name,Field20,Field21Name,Field21,Field22Name,Field22
			FROM
				@AllDataDupes
			WHERE
				RowNumber < 2
			
			SET @InsCount = @@ROWCOUNT
			SET @Message = CONVERT(VARCHAR(10),@DelCount) + ' existing AllData records deleted, ' + CONVERT(VARCHAR(10),@InsCount) + ' new AllData insertions.'
			SELECT @Message
		
	END TRY
	BEGIN CATCH
		EXEC dbo.usp_WriteErrorLog;
	END CATCH
END

SET NOCOUNT OFF;



GO


