use wca
select distinct scmst.secid, scmst.isin, issur.issuername, issur.cntryofincorp, scmst.securitydesc, scmst.primaryexchgcd, scexh.exchgcd, v54f_620_dividend.exdate
from scmst
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
left outer join continent as exchCont on substring(exchgcd,1,2) = exchCont.cntrycd
left outer join continent as incoCont on cntryofincorp = incoCont.cntrycd
left outer join v54f_620_dividend on scmst.secid =v54f_620_dividend.secid
where scmst.sectycd='ETF'
and (exchcont.continent = 'Europe'
or incocont.continent = 'Europe')
and statusflag<>'I'
--and (v54f_620_dividend.primaryex = 'Yes' or v54f_620_dividend.primaryex = '' or v54f_620_dividend.primaryex is null)

order by scmst.secid, exdate desc
