use wca
select distinct
upper('SACHG') as TableName,
SACHG.Actflag,
SACHG.AnnounceDate,
SACHG.Acttime,
SACHG.SachgID,
SACHG.ScagyID,
SACHG.EffectiveDate,
SACHG.OldRelationship,
SACHG.NewRelationship,
SACHG.OldAgncyID,
SACHG.NewAgncyID,
SACHG.OldSpStartDate,
SACHG.NewSpStartDate,
SACHG.OldSpEndDate,
SACHG.NewSpEndDate,
SACHG.OldGuaranteeType,
SACHG.NewGuaranteeType
FROM SACHG
INNER JOIN SCAGY ON SACHG.ScagyID = SACHG.ScagyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.xissid.issid from portfolio.dbo.xissid)
and SACHG.actflag<>'D'
and scmst.actflag<>'D'
and bond.actflag<>'D'
and (scmst.statusflag<>'I' or scmst.statusflag is null)
and BOND.MaturityDate>getdate()-2

use wca
select distinct
upper('SACHG') as TableName,
SACHG.Actflag,
SACHG.AnnounceDate,
SACHG.Acttime,
SACHG.SachgID,
SACHG.ScagyID,
SACHG.EffectiveDate,
SACHG.OldRelationship,
SACHG.NewRelationship,
SACHG.OldAgncyID,
SACHG.NewAgncyID,
SACHG.OldSpStartDate,
SACHG.NewSpStartDate,
SACHG.OldSpEndDate,
SACHG.NewSpEndDate,
SACHG.OldGuaranteeType,
SACHG.NewGuaranteeType
FROM SACHG
INNER JOIN SCAGY ON SACHG.ScagyID = SACHG.ScagyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
scagy.scagyid=35775


SCMST.issid in (select portfolio.dbo.xissid.issid from portfolio.dbo.xissid)
and SACHG.actflag<>'D'
and scmst.actflag<>'D'
and bond.actflag<>'D'
and (scmst.statusflag<>'I' or scmst.statusflag is null)
and BOND.MaturityDate>getdate()-2
and scagyid=35775


use wca
select distinct
upper('SCAGY') as TableName,
SCAGY.Actflag,
SCAGY.AnnounceDate,
SCAGY.Acttime,
SCAGY.ScagyID,
SCAGY.SecID,
SCAGY.AgncyID,
SCAGY.GuaranteeType,
SCAGY.SpStartDate,
SCAGY.SpEndDate
FROM SCAGY
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.xissid.issid from portfolio.dbo.xissid)
--and SCAGY.actflag<>'D'
--and scmst.actflag<>'D'
--and bond.actflag<>'D'
--and (scmst.statusflag<>'I' or scmst.statusflag is null)
--and BOND.MaturityDate>getdate()-2
and scagyid=35775


