use wca
select distinct col001, sectycd, bond.secid, issuername from portfolio.dbo.pwc_isin
left outer join scmst on portfolio.dbo.pwc_isin.col001 = isin
left outer join bond on scmst.secid = bond.secid
left outer join issur on scmst.issid = issur.issid
left outer join scexh on bond.secid = scexh.secid
where
bond.secid is not null
and scmst.statusflag<>'I'
and col001 not in (select code from portfolio.dbo.fisin where accid = 186 and actflag<>'D')

use portfolio
select distinct col001 from pwc_isin
left outer join fisin on col001 = code and accid = 186
left outer join wca.dbo.scmst on portfolio.dbo.pwc_isin.col001 = isin
where 
code is null
and wca.dbo.scmst.statusflag<>'I'
and wca.dbo.scmst.secid is not null