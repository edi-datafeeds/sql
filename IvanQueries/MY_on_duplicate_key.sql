INSERT INTO histrate (basecur, finalcur, rate, ratedate, acttime)

SELECT basecur, finalcur, rate, feeddate, acttime

FROM liverate

WHERE

basecur='CAD' AND finalcur='USD'

ON DUPLICATE KEY UPDATE

acttime=liverate.acttime, rate=liverate.rate, ratedate=liverate.feeddate;