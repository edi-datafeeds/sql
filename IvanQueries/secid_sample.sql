use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM AGM
INNER JOIN SCMST ON AGM.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
substring(isin,1,2)='XS'
and AGM.BondSecID is null
order by agmid desc


use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM BKRP
INNER JOIN SCMST ON BKRP.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
substring(isin,1,2)='XS'
order by bkrpid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM CONV
INNER JOIN BOND ON CONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
Convtype<>'TENDER'
and substring(isin,1,2)='XS'
order by convid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM CONV
INNER JOIN BOND ON CONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
Convtype='TENDER'
and substring(isin,1,2)='XS'
order by convid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
redemtype<>'TENDER'
and substring(isin,1,2)='XS'
order by redemid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
redemtype='TENDER'
and substring(isin,1,2)='XS'
order by redemid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM COSNT
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
substring(isin,1,2)='XS'
order by cosnt.rdid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM CURRD
INNER JOIN BOND ON CURRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
eventtype<>'CLEAN'
and eventtype<>'CORR'
and substring(isin,1,2)='XS'
order by currdid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM ICC
INNER JOIN BOND ON ICC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
eventtype<>'CLEAN'
and eventtype<>'CORR'
and substring(isin,1,2)='XS'
order by iccid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM LCC
INNER JOIN BOND ON LCC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
eventtype<>'CLEAN'
and eventtype<>'CORR'
and substring(isin,1,2)='XS'
order by lccid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM SCCHG
INNER JOIN BOND ON SCCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
eventtype<>'CLEAN'
and eventtype<>'CORR'
and substring(isin,1,2)='XS'
order by scchgid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM ISCHG
INNER JOIN SCMST ON ISCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(bond.issuedate<=ischg.namechangedate
     or bond.issuedate is null or ischg.namechangedate is null)
and substring(isin,1,2)='XS'
order by ischgid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM LAWST
INNER JOIN SCMST ON LAWST.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
substring(isin,1,2)='XS'
order by lawstid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
from [INT]
INNER JOIN RD ON INT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID
where
substring(isin,1,2)='XS'
order by rd.rdid desc


use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM LIQ
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
substring(isin,1,2)='XS'
order by liqid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM PVRD
INNER JOIN BOND ON PVRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
eventtype<>'CLEAN'
and eventtype<>'CORR'
and substring(isin,1,2)='XS'
order by pvrdid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM RCONV
INNER JOIN BOND ON RCONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
substring(isin,1,2)='XS'
order by rconvid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM RDNOM
INNER JOIN BOND ON RDNOM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
substring(isin,1,2)='XS'
order by rdnomid desc

use wca
insert into portfolio.dbo.wfisample
SELECT top 5
BOND.SecID
FROM SFUND
INNER JOIN BOND ON SFUND.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
substring(isin,1,2)='XS'
order by sfundid desc
