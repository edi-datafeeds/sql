



use prices
drop table sungard

use prices
SELECT
clientport.*,
histprices.mic as EdiPriceMarket,
histprices.[close] as EdiClose,
currency as EdiCurrency,
smf4.dbo.security.opol as SmfMarket,
smf4.dbo.security.StatusFlag,
exchgcd,
smf4.dbo.miclist.cntrycd as MarketCountry
into sungard
FROM clientport
left outer join smf4.dbo.security on clientport.sedol = smf4.dbo.security.sedol
                        and clientport.sedol is not null 
                        and clientport.sedol <> ''
left outer join smf4.dbo.miclist on smf4.dbo.security.opol = smf4.dbo.miclist.mic
                    and smf4.dbo.security.opol is not null
left outer join histprices on clientport.isin = histprices.isin
                        and clientport.isin is not null
                        and clientport.isin <> ''
                        and smf4.dbo.miclist.cntrycd = substring(exchgcd,1,2)

                        and smf4.dbo.miclist.mic is not null
                        and mktclosedate='2008/11/26'
                        and ((smf4.dbo.security.opol = histprices.mic) or 
                             (smf4.dbo.security.opol = 'XLON' and histprices.mic='SEAQ') or
                             (smf4.dbo.security.opol = 'XNCM' and histprices.mic='XNAS') or
                             (smf4.dbo.security.opol = 'XNGS' and histprices.mic='XNAS') or
                             (smf4.dbo.security.opol = 'XNMS' and histprices.mic='XNAS'))
union

SELECT
clientport.*,
histprices.mic as EdiPriceMarket,
histprices.[close] as EdiClose,
currency as EdiCurrency,
smf4.dbo.security.opol as SmfMarket,
smf4.dbo.security.StatusFlag,
exchgcd,
smf4.dbo.miclist.cntrycd as MarketCountry
FROM clientport
left outer join smf4.dbo.security on clientport.sedol = smf4.dbo.security.sedol
                        and clientport.sedol is not null 
                        and clientport.sedol <> ''
left outer join smf4.dbo.miclist on smf4.dbo.security.opol = smf4.dbo.miclist.mic
                    and smf4.dbo.security.opol is not null
left outer join histprices on clientport.sedol = histprices.sedol
                        and clientport.sedol is not null
                        and clientport.sedol <> ''
                        and smf4.dbo.miclist.cntrycd = substring(exchgcd,1,2)
                        and smf4.dbo.miclist.mic is not null
                        and mktclosedate='2008/11/26'
                        and ((smf4.dbo.security.opol = histprices.mic) or (smf4.dbo.security.opol = 'XLON' and histprices.mic='SEAQ'))


--select * from histprices
--where isin = 'BRALLLCDAM10'


drop table sunid

use prices
select id 
into sunid
from sungard
group by id
having count(id) >  1



select sungard.* from sungard
left outer join sunid on sungard.id = sunid.id
where
(sungard.EdiPriceMarket is not null
or sunid.id is null)



select distinct sungard.id 
into matched_id
from sungard
where 
sungard.EdiPriceMarket is not null


use prices
select * from histprices
--where sedol = 'B1P8G85'
--where isin = 'XS0284605036'

where sedol ='0182704'

where isin = 'GB0006486152'



select * from sungard
where sedol is null

use smf4
select sedol, opol, isin from security
--where isin = 'XS0284605036'

where sedol = '2027342'


use prices
SELECT distinct
clientport.*,
histprices.mic as EdiPriceMarket,
histprices.[close] as EdiClose,
currency as EdiCurrency,
' ' as SmfMarket,
' ' as StatusFlag,
exchgcd,
' ' as MarketCountry
--into sungard1
FROM clientport
left outer join histprices on clientport.isin = histprices.isin
                        and clientport.isin is not null
                        and clientport.isin <> ''
                        and mktclosedate='2008/11/26'
where
clientport.id not in (select id from matched_id)
and histprices.mic is not null

use wca
select cntrycd from continent
where continent = 'Africa'

use prices
select * from histprices
where
--isin='SE0000337842'
mic='xsto'



use smf4
select * from security
where
opol = 'XWBO'
sedol = 'b0742y2'

use smf4
select * from miclist
where mic='xmad'

use prices
update histprices
set mic='ROCO'
where mic='OTC'

select * from histprices
where mic = 'OTC'
where substring(isin,1,2) = 'TW'
