USE [client]
GO
/****** Object:  Table [dbo].[SEME_eq_11_isin]    Script Date: 12/06/2011 21:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEME_Citibank_i](
	[CORP] [nchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
	[SEME] [bigint] NOT NULL,
	[PREV] [bigint] NULL,
	[Acttime] [datetime] NULL,
 CONSTRAINT [PK_SEME_Citibank_i] PRIMARY KEY CLUSTERED 
(
	[CORP] ASC,
	[SEME] ASC
) ON [PRIMARY]
) ON [PRIMARY]


insert into SEME_Citibank_i select * from seme_sample where seme=1