Query For fixed rate bonds

1. interest basis equals 'FXD'
2. dated date is populated
3. maturity date is populated
4. bond type is populated
5. first coupon is populated
6. interest payment frequency is populated
7. interest rate is populated (00.000 is considered a blank value)
8a. Callable is = blank
8b  Callable is = 'N'
8c. Callable is = 'Y' and where Call Type = 'O' and at least one row where Call From and Call Price are populated

use wca
select distinct bond.secid from bond
inner join cpopt on bond.secid = cpopt.secid
where 
cpopt.callput = 'C'
and cpopt.calltype = 'O'
and cpopt.callprice
and interestBasis = 'FXD'
and issuedate is not null
and maturitydate is not null
and bondtype is not null
and interestpaymentfrequency <> ''
and firstcoupondate is not null
--and issueprice is null 
--and parvalue is null
and (interestrate is not null or substring(interestrate,1,5)='0.000')
--and callable = ''

select distinct price from cpopt
where price is null
