use wca
delete from scmst
where
secid in
(select scmst.secid from scmst
inner join cowar on scmst.secid = cowar.secid
where sectycd = 'CW'
and expirationdate<getdate()-90)

use wca
delete from issur
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from agm
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from scmst
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from scexh
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from sedol
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from bond
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from exdt
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from rd
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from cpopt
where actflag = 'D'
and acttime<getdate()-31

use wca
delete from cowar
where actflag = 'D'
and acttime<getdate()-31


use wca
delete from SCEXH
where
SCEXH.secid not in
(select scmst.secid from scmst)

use wca
delete from SCCHG
where
SCCHG.secid not in
(select scmst.secid from scmst)

use wca
delete from SCACT
where
SCACT.secid not in
(select scmst.secid from scmst)


use wca
delete from PVRD
where
PVRD.secid not in
(select scmst.secid from scmst)


use wca
delete from NLIST
where
NLIST.scexhid not in
(select scexh.scexhid from scexh)

use wca
delete from LTCHG
where
LTCHG.secid not in
(select scmst.secid from scmst)

use wca
delete from LSTAT
where
LSTAT.secid not in
(select scmst.secid from scmst)

use wca
delete from LCC
where
LCC.secid not in
(select scmst.secid from scmst)


use wca
delete from ICC
where
ICC.secid not in
(select scmst.secid from scmst)

use wca
delete from CWCHG
where
CWCHG.secid not in
(select scmst.secid from scmst)

use wca
delete from SDCHG
where
SDCHG.secid not in
(select scmst.secid from scmst)

use wca
delete from CURRD
where
CURRD.secid not in
(select scmst.secid from scmst)

use wca
delete from AGCHG
where
AGCHG.secid not in
(select scmst.secid from scmst
inner join AGCHG on scmst.secid=AGCHG.secid)

use wca
delete from SEDOL
where
SEDOL.secid not in
(select scmst.secid from scmst)

use wca
delete from SHOCH
where
SHOCH.secid not in
(select scmst.secid from scmst)

delete from cowar
where
cowar.secid not in
(select cowar.secid from cowar
inner join scmst on cowar.secid=scmst.secid)

delete from agm
where
agm.issid not in
(select agm.issid from agm
inner join issur on agm.issid=issur.issid)
