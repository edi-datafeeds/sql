use wca
select distinct 
cntrg.secid,
scmst.isin,
issur.issuername,
scmst.securitydesc,
RegistrarName,Add1,Add2,Add3,Add4,Add5,City,agncy.CntryCD,agncy.WebSite,Contact1,Tel1,
Fax1,email1,Contact2,Tel2,Fax2,email2
from agncy
left outer join cntrg on agncy.agncyid = cntrg.regid
left outer join scmst on cntrg.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
WHERE
scmst.secid is not null
and scmst.statusflag<>'I' 
and scmst.actflag<>'D'
and agncy.actflag<>'D'
and cntrg.actflag<>'D'
order by RegistrarName desc

