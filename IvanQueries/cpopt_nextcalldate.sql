print " GENERATING portfolio.dbo.CPOPT_Count, please wait..."
go


use portfolio
if exists (select * from sysobjects where name = 'CPOPT_single')
 drop table CPOPT_single
use wca
select
cpopt.secid,
case when Todate is not null and fromdate <> todate then 'US' 
     else 'EU'
     end as Call_Frequency,
case when Todate is not null and fromdate <> todate then fromdate end as From_Date,
case when Todate is not null and fromdate <> todate then todate end as To_Date,
case when Todate is null or fromdate = todate then fromdate end as Next_Call_Date
into portfolio.dbo.CPOPT_single
from portfolio.dbo.cpopt_ct
inner join cpopt on portfolio.dbo.CPOPT_Ct.SecID = cpopt.secid
where
cptype<>'KO'
and portfolio.dbo.CPOPT_Ct.Ct = 1
and cpopt.actflag<>'D'

go

print ""
go

print " INDEXING portfolio.dbo.CPOPT_Count ,please  wait ....."
GO 
use portfolio
ALTER TABLE CPOPT_single ALTER COLUMN secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_single] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_single] PRIMARY KEY ([secid]) ON [PRIMARY]
GO 
