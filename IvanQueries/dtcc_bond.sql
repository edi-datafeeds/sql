use wca
select isin, issuername, securitydesc, bondtype, maturitydate, interestrate
from
scmst
left outer join bond on scmst.secid = bond.secid
left outer join issur on scmst.issid = issur.issid
where
bond.secid is not null
and (statusflag<> 'I' or statusflag is null)
and isin <>''

