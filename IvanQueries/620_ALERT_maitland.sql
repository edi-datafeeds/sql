--# 1
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Liquidationdate>=getdate()-1 and Liquidationdate<getdate()+1)
order by changed


--# 2
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed


--# 3
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed


--# 4
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed



--# 5
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed



--# 6
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and ((Closedate>=getdate()-1 and Closedate<getdate()+1)
or (CmAcqdate>=getdate()-1 and CmAcqdate<getdate()+1)
or (TkovrPaydate>=getdate()-1 and TkovrPaydate<getdate()+1))
order by changed



--# 7
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed




--# 8
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed



--# 9
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed


--# 10
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed



--# 11
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed



--# 12
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed


--# 13
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed



--# 14
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed



--# 15
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed

--# 16
use wca
SELECT * 
FROM v54f_620_Bonus_Rights 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed



--# 17
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed



--# 18
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (NotificationDate>=getdate()-1 and NotificationDate<getdate()+1)
order by changed



--# 19
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (NameChangeDate>=getdate()-1 and NameChangeDate<getdate()+1)
order by changed



--# 20
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (DateOfChange>=getdate()-1 and DateOfChange<getdate()+1)
order by changed


--# 21
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed



--# 22
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed




--# 23
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and sedol in (select col001 from portfolio.dbo.maitland)
and (Exdate>=getdate()-1 and Exdate<getdate()+1)
order by changed




--# 24
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed



--# 25
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed

--# 26
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
(ListStatus <> 'D' or ListStatus is null)
and sedol in (select col001 from portfolio.dbo.maitland)
and (Redemptiondate>=getdate()-1 and Redemptiondate<getdate()+1)
order by changed


--# 27
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
(ListStatus <> 'D' or ListStatus is null)
and sedol in (select col001 from portfolio.dbo.maitland)
and (Effectivedate>=getdate()-1 and Effectivedate<getdate()+1)
order by changed

