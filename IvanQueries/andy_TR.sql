use wca
select distinct exchg.exchgcd, parvalue, isin, issuername, securitydesc, mic from exchg
inner join scexh on exchg.exchgcd = scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where 
mic = 'XIST'
and statusflag<>'I'
order by parvalue, mic, issuername

