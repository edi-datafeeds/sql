
use wca
select redem.secid, statusflag, partfinal,redemdate, indef.defaultdate, oindef.defaultdate from redem
inner join bond on redem.secid = bond.secid
inner join scmst on redem.secid = scmst.secid
inner join indef on redem.secid = indef.secid and 'intDEFA' = indef.defaulttype
inner join indef as oindef on redem.secid = oindef.secid and 'outofDEFA' = oindef.defaulttype
where redemtype='CALL'
and partfinal='F'
--and statusflag='D'
order by redem.secid desc, redem.acttime desc

select distinct defaulttype from indef