use wca
select top 10
case when col001 is not null then 'Yes' else 'No' end as price_found,
scmst.secid,
scmst.isin,
issur.issuername,
scmst.securitydesc,
issur.cntryofincorp,
bond.bondtype,
bond.InterestBasis,
bond.SecurityCharge
from scmst
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join portfolio.dbo.markit_bprice on bond.secid = col001
where 
scmst.actflag<>'D'
and bond.actflag<>'D'
and (maturitydate>getdate()-1 or maturitydate is null)
and statusflag<>'I'
order by price_found desc, isin
--and substring(isin,1,2) ='IT'
