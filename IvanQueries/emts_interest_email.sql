--filepath=o:\Datafeed\Debt\xsecid\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INTALERT
--fileheadertext=EDI_DEBT_INTALERT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Debt\xsecid\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use wca
select
upper('INTALERT') as TableName,
getdate()+45 as Pay_Date,
case when intpy.bcurencd<>'' and intpy.bcurencd is not null then intpy.bcurencd else bond.curencd end as Currency,
case when intpy.intrate<>'' then intrate else bond.interestrate end as Interest_Rate,
bond.interestpaymentfrequency as Frequency,
bond.interestaccrualconvention as Accrual_Convention,
bond.frnindexbenchmark as FRN_Index_Benchmark,
bond.markup as FRN_Margin,
Recdate as Record_Date,
case when intpy.rescindinterest is null or intpy.rescindinterest='F' then 'No' else 'Yes' end as Payment_Cancelled
from bond
inner join scmst on bond.secid = scmst.secid
left outer join RD ON bond.secid = rd.secid
LEFT OUTER JOIN SCEXH ON BOND.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
left outer join int on rd.rdid = int.rdid
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID

where
bond.secid in (select portfolio.dbo.xsecid.secid from portfolio.dbo.xsecid)
and bond.actflag<>'D'
and scmst.actflag<>'D'
and (
paydate = convert(varchar(4),year(getdate()+45))+'/'+convert(varchar(2),month(getdate()+45))+'/'+
convert(varchar(2),day(getdate()+45))

or convert(varchar(4),year(getdate()))+substring(Interestpaydate1,3,2)+substring(Interestpaydate1,1,2)=
 convert(varchar(30),getdate()+45, 112)
or
convert(varchar(4),year(getdate()))+substring(Interestpaydate2,3,2)+substring(Interestpaydate2,1,2)=
 convert(varchar(30),getdate()+45, 112)
or
convert(varchar(4),year(getdate()))+substring(Interestpaydate3,3,2)+substring(Interestpaydate3,1,2)=
 convert(varchar(30),getdate()+45, 112)
or
convert(varchar(4),year(getdate()))+substring(Interestpaydate4,3,2)+substring(Interestpaydate4,1,2)=
 convert(varchar(30),getdate()+45, 112)
or
firstcoupondate=convert(varchar(30),getdate()+45, 112)
)

and ((intpy.intrate<>'' and intpy.intrate is not null) or bond.interestrate<>'')
and ((intpy.bcurencd <>'' and intpy.bcurencd is not null) or bond.curencd<>'')
and (bond.maturitydate>getdate()+45 or (bond.maturitydate is null and perpetual<>''))
and (bond.firstcoupondate<=convert(varchar(30),getdate()+45, 112) or bond.firstcoupondate is null)
and (bond.intcommencementdate<=convert(varchar(30),getdate()+45, 112) or bond.intcommencementdate is null)
and (bond.issuedate<=convert(varchar(30),getdate()+45, 112) or bond.issuedate is null)

