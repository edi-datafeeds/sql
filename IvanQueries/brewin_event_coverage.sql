use portfolio
select
brewin.*,
case when wca.dbo.sectygrp.secgrpid= 1 or wca.dbo.sectygrp.secgrpid= 2 then 
''+char(96)+'x'+char(96)+'x'+char(96)+'x'+char(96)+'x'+char(96)+'x'+
char(96)+''+char(96)+''+char(96)+''+char(96)+''+char(96)+''
     when wca.dbo.sectygrp.secgrpid= 4 then
'?'+char(96)+''+char(96)+''+char(96)+''+char(96)+''+char(96)+''+
char(96)+'x'+char(96)+''+char(96)+'x'+char(96)+''+char(96)+'x'
else
''+char(96)+''+char(96)+''+char(96)+''+char(96)+''+char(96)+''+
char(96)+''+char(96)+''+char(96)+''+char(96)+''+char(96)+''
     end as fred,
case when wca.dbo.sectygrp.secgrpid= 1 or wca.dbo.sectygrp.secgrpid= 2 then 'EQUITY'
     when wca.dbo.sectygrp.secgrpid= 4 then 'DEBT'
     when wca.dbo.sectygrp.secgrpid= 3 then 'WCA - WARRANT'
     when wca.dbo.sectygrp.secgrpid= 5 then 'WCA - OPEN ENDED FUND'
     when wca.dbo.sectygrp.secgrpid= 6 then 'WCA - UNIT TRUST'
     when wca.dbo.sectygrp.secgrpid= 7 then 'WCA - MUTUAL FUND'
     when smf4.dbo.security.statusflag = 'D' then 'Defunct SEDOL'
     when smf4.dbo.security.securityID is null then 'Non-Existent SEDOL'
     end as EDIComment,
wca.dbo.scmst.sectycd
 from brewin

left outer join wca.dbo.sedol on col001 = wca.dbo.sedol.sedol
left outer join wca.dbo.scmst on wca.dbo.sedol.secid = wca.dbo.scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join smf4.dbo.security on col001 = smf4.dbo.security.sedol

order by EDIComment

