use wca
select col001, 
sectycd,
case when statusflag ='' then 'Active' else 'Inactive' end as Status,
case when icc.secid is not null then 'Yes' else 'No' end as FoundOldWcaIsin
from portfolio.dbo.lomfi
left outer join scmst on col001 = scmst.isin
left outer join icc on col001 = icc.oldisin
where scmst.secid is not null
or icc.secid is not null
order by sectycd


use wca
select col001, sedol, sectype, smf4.dbo.security.statusflag, opol
from portfolio.dbo.lomfi
left outer join scmst on col001 = scmst.isin
left outer join icc on col001 = icc.oldisin
left outer join smf4.dbo.security on col001 = smf4.dbo.security.isin
where scmst.secid is null
and icc.secid is null
order by col001