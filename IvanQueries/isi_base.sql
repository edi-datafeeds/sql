select isibase.mic, isibase.secid, scmst.isin, isibase.cntrycd, issur.issuername, 
scmst.securitydesc, scmst.sectycd, scmst.issid,
case when scmst.statusflag<>'I' then 'Active' else 'Inactive' end as globalscmststatus,
case when scexh.liststatus is null then 'U' when scexh.liststatus='N' or scexh.liststatus='' then 'L' else scexh.liststatus end as wcaliststatus,
isibase.linkflag
from isibase
inner join scmst on isibase.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join exchg on isibase.mic = exchg.mic
left outer join scexh on scmst.secid = scexh.secid and exchg.exchgcd = scexh.exchgcd
where scexh.liststatus is not null
and scexh.liststatus <>'D'
and scexh.liststatus <>'S'
and linkflag = 'N'
and statusflag<>'I'
and sectycd='EQS'

order by  desc



use prices
select
id
into dupid2
from srf_isi
where
--voting = 'v'
--and
secid<>0
and secid in (select secid from srf_isi
group by secid
having count(secid) > 3)
order by secid

use prices
select secid
from srf_isi
where secid =0

use prices
select distinct mic, secid, cntrycd, 'Y' as Linkflag
into wca.dbo.linked
from srf_isi7

inner join dupid2 on srf_isi.id = dupid2.id
order by secid, mic


select distinct mic 
into pmic
from histprices

use wca
select distinct exchg.mic, scmst.secid, exchg.cntrycd, 'N' as Linkflag
into isiwcaonly
from scmst
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join linked on exchg.mic = linked.mic
            and scmst.secid = linked.secid
where exchg.mic in (select mic from prices.dbo.pmic)
and wca.dbo.sectygrp.secgrpid<3
and linked.secid is null

select

select * from linked


select * 
into isibase
from linked
union
select * from isiwcaonly


select isibase.mic, isibase.secid, scmst.isin, isibase.cntrycd, issur.issuername, 
scmst.securitydesc, scmst.sectycd, scmst.issid,
case when scmst.statusflag<>'I' then 'Active' else 'Inactive' end as globalscmststatus,
case when scexh.liststatus is null then 'U' when scexh.liststatus='N' or scexh.liststatus='' then 'L' else scexh.liststatus end as wcaliststatus,
isibase.linkflag
from isibase
inner join scmst on isibase.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join exchg on isibase.mic = exchg.mic
left outer join scexh on scmst.secid = scexh.secid and exchg.exchgcd = scexh.exchgcd
where scexh.liststatus is not null
and scexh.liststatus <>'D'
and scexh.liststatus <>'S'
and linkflag = 'N'
and statusflag<>'I'
and sectycd='EQS'

order by  desc



select distinct isibase.secid
from isibase
inner join scmst on isibase.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join exchg on isibase.mic = exchg.mic
left outer join scexh on scmst.secid = scexh.secid and exchg.exchgcd = scexh.exchgcd
where scexh.liststatus is not null
and scexh.liststatus <>'D'
and scexh.liststatus <>'S'
and linkflag = 'N'
and statusflag<>'I'
and sectycd='EQS'

                                        


select * from isibase
where secid not in (select secid from scmst)