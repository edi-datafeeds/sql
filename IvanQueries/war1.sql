use wca
select distinct wartm.secid, wartm.wtexersecid, warex.exersecid from wartm
left outer join warex on wartm.secid = warex.secid
where expirationdate>getdate()-1
and exersecid is null
and wartm.wtexersecid is null
and warex.secid is null


select * from warex
where exersecid is null

use wca
select distinct 
wartm.secid,
wartm.wtexersecid,
warex.exersecid
from portfolio.dbo.rm800
inner join scmst on portfolio.dbo.rm800.isin = scmst.isin
inner join wartm on scmst.secid=wartm.secid
left outer join warex on wartm.secid = warex.secid
where warex.exersecid is null
and expirationdate>getdate()-1


use smf4
select * from security
where cregcode = 'XX'
