use wca
select distinct
isin as ISIN,
uscode as Cusip,
case when sedol.actflag='D' or sedol.secid is null then '' else sedol end as Sedol,
cntryofincorp as Domicile,
securitydesc as IssueName,
issuername as Issuername,
Regs144A,
case when exchg.actflag is null then '' else mic end as mic
from scmst
left outer join issur on scmst.issid = issur.issid
left outer join sedol on scmst.secid = sedol.secid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
scmst.actflag<>'D'
and scmst.sectycd = 'BND'
