use wca
select top 1
SCMST.SecID,
SEDOL.SEDOL as takeoutlater,
intpy.bCurenCD,
BOND.InterestAccrualConvention,
scmst.parvalue,
intpy.bParvalue,
int.InterestFromDate,
int.InterestToDate,
intpy.AnlCoupRate,
intpy.GrossInterest,
intpy.NetInterest,
intpy.IntRate,
BOND.InterestPaymentFrequency,
int.days
FROM
INT
left outer JOIN RD ON RD.RdID = INT.RdID
left outer join SCMST ON RD.SecID = SCMST.SecID
left outer join BOND ON SCMST.SecID = BOND.SecID
left outer join SEDOL ON SCMST.SecID = SEDOL.SecID
                        and sedol.cntrycd = 'GB'
left outer JOIN INTPY ON INT.RdID = INTPY.RDID
where
int.InterestToDate<'2007/01/21'
and bond.actflag<>'D'
and scmst.actflag<>'D'
and int.actflag<>'D'
and intpy.actflag<>'D'
and rescindinterest='F'
and sedol = '0903671'
order by int.InterestToDate desc


