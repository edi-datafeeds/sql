select * from scmst
where isin = 'US91912E1055'
or isin = 'FR0010304774'

US91912E1055 from January 2011

USE WCA
SELECT *  
FROM v54f_620_Dividend

WHERE CHANGED > '2011/01/01'
and (secid=72988)

Security Type - BND
IssuerName
SecurityDesc

CntryOfIncorp - CA
or
CA listing
or
CA ISIN prefix


select
sectycd,
issuername,
securitydesc,
cntryofincorp,
isin,
exchgcd
from scmst
inner join issur on scmst.issid = issur.issid
left outer join scexh on scmst.secid = scexh.secid
where
(substring(isin,1,2)='CA' or cntryofincorp='CA' or substring(exchgcd,1,2)='CA')
and scmst.actflag<>'D'
order by sectycd, issuername