--filepath=o:\upload\acc\155\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend 
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\155\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA2
SELECT * 
FROM t610_Company_Meeting 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 2
USE WCA2
SELECT *
FROM t610_Call
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 3
USE WCA2
SELECT * 
FROM t610_Liquidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA2
SELECT *
FROM t610_Certificate_Exchange
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 5
USE WCA2
SELECT * 
FROM t610_International_Code_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 6
USE WCA2
SELECT * 
FROM t610_Preference_Conversion
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 7
use WCA2
SELECT * 
FROM t610_Preference_Redemption
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 8
use WCA2
SELECT * 
FROM t610_Security_Reclassification
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 9
use WCA2
SELECT * 
FROM t610_Lot_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 10
use WCA2
SELECT * 
FROM t610_Sedol_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 11
use WCA2
SELECT * 
FROM t610_Buy_Back
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 12
use WCA2
SELECT * 
FROM t610_Capital_Reduction
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 13
use WCA2
SELECT * 
FROM t610_Takeover
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 14
use WCA2
SELECT * 
FROM t610_Arrangement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 15
use WCA2
SELECT * 
FROM t610_Bonus
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 16
use WCA2
SELECT * 
FROM t610_Bonus_Rights
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 17
use WCA2
SELECT * 
FROM t610_Consolidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 18
use WCA2
SELECT * 
FROM t610_Demerger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 19
use WCA2
SELECT * 
FROM t610_Distribution
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 20
use WCA2
SELECT * 
FROM t610_Divestment
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 21
use WCA2
SELECT * 
FROM t610_Entitlement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 22
use WCA2
SELECT * 
FROM t610_Merger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 23
use WCA2
SELECT * 
FROM t610_Preferential_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 24
use WCA2
SELECT * 
FROM t610_Purchase_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 25
use WCA2
SELECT * 
FROM t610_Rights 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 26
use WCA2
SELECT * 
FROM t610_Security_Swap 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 27
use WCA2
SELECT *
FROM t610_Subdivision
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 28
use WCA2
SELECT *
FROM t610_Bankruptcy 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 29
use WCA2
SELECT *
FROM t610_Financial_Year_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 30
use WCA2
SELECT *
FROM t610_Incorporation_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 31
use WCA2
SELECT *
FROM t610_Issuer_Name_change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 32
use WCA2
SELECT *
FROM t610_Lawsuit
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 33
use WCA2
SELECT *
FROM t610_Security_Description_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 34
use WCA2
SELECT *
FROM t610_Assimilation
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 35
use WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 36
use WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 37
use WCA2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 38
use WCA2
SELECT * 
FROM t610_Announcement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 39
use WCA2
SELECT * 
FROM t610_Parvalue_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 40
USE WCA2
SELECT * 
FROM t610_Currency_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 41
USE WCA2
SELECT * 
FROM t610_Return_of_Capital 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol


--# 42
USE WCA2
SELECT * 
FROM t610_Dividend
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 43
USE WCA2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol

--# 44
USE WCA2
SELECT * 
FROM t610_Franking
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='U'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='U'))
ORDER BY EventID, ExchgCD, Sedol


--# 51
USE WCA
SELECT * 
FROM v50f_610_Company_Meeting 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 52
USE WCA
SELECT *
FROM v53f_610_Call
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 53
USE WCA
SELECT * 
FROM v50f_610_Liquidation
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 54
USE WCA
SELECT *
FROM v51f_610_Certificate_Exchange
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 55
USE WCA
SELECT * 
FROM v51f_610_International_Code_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 56
USE WCA
SELECT * 
FROM v51f_610_Preference_Conversion
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 57
use wca
SELECT * 
FROM v51f_610_Preference_Redemption
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 58
use wca
SELECT * 
FROM v51f_610_Security_Reclassification
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 59
use wca
SELECT * 
FROM v52f_610_Lot_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 60
use wca
SELECT * 
FROM v52f_610_Sedol_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 61
use wca
SELECT * 
FROM v53f_610_Buy_Back
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 62
use wca
SELECT * 
FROM v53f_610_Capital_Reduction
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 63
use wca
SELECT * 
FROM v53f_610_Takeover
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 64
use wca
SELECT * 
FROM v54f_610_Arrangement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 65
use wca
SELECT * 
FROM v54f_610_Bonus
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 66
use wca
SELECT * 
FROM v54f_610_Bonus_Rights
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 67
use wca
SELECT * 
FROM v54f_610_Consolidation
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 68
use wca
SELECT * 
FROM v54f_610_Demerger
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 69
use wca
SELECT * 
FROM v54f_610_Distribution
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 70
use wca
SELECT * 
FROM v54f_610_Divestment
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 71
use wca
SELECT * 
FROM v54f_610_Entitlement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 72
use wca
SELECT * 
FROM v54f_610_Merger
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 73
use wca
SELECT * 
FROM v54f_610_Preferential_Offer
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 74
use wca
SELECT * 
FROM v54f_610_Purchase_Offer
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 75
use wca
SELECT * 
FROM v54f_610_Rights 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 76
use wca
SELECT * 
FROM v54f_610_Security_Swap 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 77
use wca
SELECT *
FROM v54f_610_Subdivision
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 78
use wca
SELECT *
FROM v50f_610_Bankruptcy 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 79
use wca
SELECT *
FROM v50f_610_Financial_Year_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 80
use wca
SELECT *
FROM v50f_610_Incorporation_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 81
use wca
SELECT *
FROM v50f_610_Issuer_Name_change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 82
use wca
SELECT *
FROM v50f_610_Lawsuit
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 83
use wca
SELECT *
FROM v51f_610_Security_Description_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 84
use wca
SELECT *
FROM v52f_610_Assimilation
WHERE (CHANGED > getdate()-183)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 85
use WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE (CHANGED > getdate()+1)

--# 86
use WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE (CHANGED > getdate()+1)

--# 87
use WCA2
SELECT *
FROM t610_New_Listing
WHERE (CHANGED > getdate()+1)

--# 88
use wca
SELECT * 
FROM v50f_610_Announcement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 89
use wca
SELECT * 
FROM v51f_610_Parvalue_Redenomination 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 90
USE WCA
SELECT * 
FROM v51f_610_Currency_Redenomination 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol

--# 91
USE WCA
SELECT * 
FROM v53f_610_Return_of_Capital 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol


--# 92
USE WCA
SELECT * 
FROM v54f_610_Dividend
WHERE 
(sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
and (CHANGED > getdate()-183)
AND  (PAYDATE > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
ORDER BY EventID, ExchgCD, Sedol

--# 93
USE WCA
SELECT * 
FROM v54f_610_Dividend_Reinvestment_Plan
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
 or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
and (CHANGED > getdate()-183)
AND  (PAYDATE > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
ORDER BY EventID, ExchgCD, Sedol

--# 94
USE WCA
SELECT * 
FROM v54f_610_Franking
WHERE (CHANGED > getdate()-183)
AND  (PAYDATE > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=155 and actflag='I'))
     or (uscode in (select code from portfolio.dbo.fuscode where accid=155 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
