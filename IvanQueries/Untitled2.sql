--FilePath=O:\Datafeed\15022\Euroclear\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DVSE
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=off
--ArchivePath=n:\15022\Euroclear\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime


--# 1

use wca
select top 50   
v54f_ISO_D.Changed,
/*'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(v54f_ISO_D.EventID as char(16)) as ChkNotes, */
'' as ISOHDR,
':16R:GENL',
'101'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_D.Actflag_1 = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_D.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_D.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_D.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_D.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVSE',
':22F::CAMV//MAND',
':98A::PREP//'+CONVERT ( varchar , Changed,112) as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v54f_ISO_D.Sidname +' '+ v54f_ISO_D.Sid,
substring(v54f_ISO_D.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_D.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_D.Sedol <> '' and v54f_ISO_D.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_D.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_D.Localcode <> ''
     THEN '/TS/' + v54f_ISO_D.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_D.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_D.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_D.Exdate <>'' AND v54f_ISO_D.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_D.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v54f_ISO_D.Recdate <>'' AND v54f_ISO_D.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v54f_ISO_D.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v54f_ISO_D.Taxrate_1 <> ''
     THEN ':92K::TAXR//UKWN' 
     ELSE ':92K::TAXR//UKWN' 
     END as COMMASUB,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v54f_ISO_D.RdID as char(16)) as Notes, */*/ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v54f_ISO_D.Fractions_1 = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN v54f_ISO_D.Fractions_1 = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN v54f_ISO_D.Fractions_1 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v54f_ISO_D.Fractions_1 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v54f_ISO_D.Fractions_1 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v54f_ISO_D.Fractions_1 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_D.Ressid_1 <> ''
     THEN ':35B:' + v54f_ISO_D.Ressidname_1 +' '+ v54f_ISO_D.Ressid_1
     WHEN (v54f_ISO_D.Ressecid_1 = '')
     THEN ':35B:' + v54f_ISO_D.Sidname +' '+ v54f_ISO_D.Sid
     WHEN v54f_ISO_D.SecID = v54f_ISO_D.Ressecid_1
     THEN ':35B:' + v54f_ISO_D.Sidname +' '+ v54f_ISO_D.Sid
     END,
CASE WHEN v54f_ISO_D.RatioNew_1 <>''
            AND v54f_ISO_D.RatioNew_1 IS NOT NULL
          AND v54f_ISO_D.RatioOld_1 <>''
            AND v54f_ISO_D.RatioOld_1 IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(v54f_ISO_D.RatioNew_1 as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_D.RatioOld_1 as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN v54f_ISO_D.Paydate <>'' 
               AND v54f_ISO_D.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_D.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v54f_ISO_D
WHERE 
SID <> ''
and Actflag <> ''
and DIVType_1 = 'B'
AND OpID_2 = ''
AND OpID_3 = ''
AND OpID_4 = ''
AND OpID_5 = ''
AND OpID_6 = ''
order by caref1, caref2 desc, caref3


use finhols2
select * from pubhol_exch_map


select * 
from pubhol_holidays

where fin_code = 'UK_LSE'
order by hol_date

update 
set notes=notes+'Early May
from pubhol_holidays

where fin_code = 'UK_LSE'
order by hol_date