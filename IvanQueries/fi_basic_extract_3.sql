use wca
select
scmst.secid,
scmst.isin,
issur.issuername, 
scmst.securitydesc
from scmst
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
where 
scmst.actflag<>'D'
and bond.actflag<>'D'
and (maturitydate>getdate()-1 or maturitydate is null)
and statusflag<>'I'
and substring(isin,1,2) ='IT'

