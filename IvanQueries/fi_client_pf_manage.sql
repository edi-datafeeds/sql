-- report 1 - missings
use client
select code, pfisin.acttime from pfisin
left outer join wca.dbo.scmst on code =isin
where accid = 212
and pfisin.actflag <> 'D'
and isin is null
order by pfisin.acttime desc

-- report 2 - non fixed income or missing bond record
use client
select code, pfisin.acttime, sectycd, bondtype from pfisin
left outer join wca.dbo.scmst on code =isin
left outer join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid
where accid = 212
and pfisin.actflag <> 'D'
and isin is not null
and bondtype is null
order by pfisin.acttime desc

-- report 3 - Missing offering doc
use client
select code, pfisin.acttime, bondsrc, sectycd, bondtype from pfisin
left outer join wca.dbo.scmst on code =isin
left outer join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid
where accid = 212
and pfisin.actflag <> 'D'
and isin is not null
and bondtype is not null
and bondsrc<>'OC'
and bondsrc<>'PR'
and bondsrc<>'PS'
and statusflag<>'I'
order by pfisin.acttime, bondsrc desc