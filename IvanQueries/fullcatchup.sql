
use wca
insert into full_lstat Select * from lstat
where
lstatid not in
(select lstatid from wca.dbo.full_lstat)

use wca
insert into full_nlist Select * from nlist
where
scexhid not in
(select scexhid from wca.dbo.full_nlist)

use wca
insert into full_rd Select * from rd
where
rdid not in
(select rdid from wca.dbo.full_rd)

use wca
insert into full_div Select * from div
where
divid not in
(select divid from wca.dbo.full_div)

use wca
insert into full_agm Select * from agm
where
agmid not in
(select agmid from wca.dbo.full_agm)

use wca
insert into full_ischg Select * from ischg
where
ischgid not in
(select ischgid from wca.dbo.full_ischg)

use wca
insert into full_sdchg Select * from sdchg
where
sdchgid not in
(select sdchgid from wca.dbo.full_sdchg)

use wca
insert into full_scchg Select * from scchg
where
scchgid not in
(select scchgid from wca.dbo.full_scchg)

use wca
insert into full_ann Select * from ann
where
annid not in
(select annid from wca.dbo.full_ann)

use wca
insert into full_lcc Select * from lcc
where
lccid not in
(select lccid from wca.dbo.full_lcc)

use wca
insert into full_icc Select * from icc
where
iccid not in
(select iccid from wca.dbo.full_icc)


*****************************

use wca
insert into wca.dbo.lawst Select * from wcahist.dbo.lawst
where
wcahist.dbo.lawst.lawstid not in
(select lawstid from wca.dbo.lawst)

use wca
insert into wca.dbo.bkrp Select * from wcahist.dbo.bkrp
where
wcahist.dbo.bkrp.bkrpid not in
(select bkrpid from wca.dbo.bkrp)

use wca
insert into wca.dbo.liq Select * from wcahist.dbo.liq
where
wcahist.dbo.liq.liqid not in
(select liqid from wca.dbo.liq)


use wca
insert into wca.dbo.prf Select * from wcahist.dbo.prf
where
wcahist.dbo.prf.rdid not in
(select rdid from wca.dbo.prf)

use wca
insert into wca.dbo.call Select * from wcahist.dbo.call
where
wcahist.dbo.call.callid not in
(select callid from wca.dbo.call)

use wca
insert into wca.dbo.assm Select * from wcahist.dbo.assm
where
wcahist.dbo.assm.assmid not in
(select assmid from wca.dbo.assm)

use wca
insert into wca.dbo.drip Select * from wcahist.dbo.drip
where
wcahist.dbo.drip.divid not in
(select divid from wca.dbo.drip)

use wca
insert into wca.dbo.frank Select * from wcahist.dbo.frank
where
wcahist.dbo.frank.divid not in
(select divid from wca.dbo.frank)

use wca
Select * from wcahist.dbo.full_divpy
where
wcahist.dbo.full_divpy.divid not in
(select divid from wca.dbo.divpy)


select count(*) from wca.dbo.divpy
where actflag<>'D'
and acttime>'2010/01/01'
select count(*) from wcahist.dbo.full_divpy
where actflag<>'D'
and acttime>'2010/01/01'


use wca
insert into full_divpy
Select * from wca.dbo.divpy
where
divid not in
(select divid from wca.dbo.full_divpy)


use wca
Select * 
into tempexdt
from wca.dbo.exdt
where
rdid not in
(select rdid from wca.dbo.full_exdt)

insert into full_exdt select * from tempexdt