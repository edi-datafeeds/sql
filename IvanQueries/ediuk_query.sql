use smf4
drop table fred

select distinct code 
into fred
from portfolio.dbo.xcode
left outer join security on code = isin
inner join edicadetail on security.securityid = edicadetail.securityid
inner join edicaoption on security.securityid = edicaoption.securityid
where
--security.securityid is null
(opol= 'xlon' or opol='XPLU')
and (edicadetail.catype = 'DO'
or edicadetail.catype = 'D2'
or edicadetail.catype = 'DV'
or edicadetail.catype = 'SV'
or edicaoption.optiontype = 'DO'
or edicaoption.optiontype = 'D2'
or edicaoption.optiontype = 'DV'
or edicaoption.optiontype = 'SV')

select code from portfolio.dbo.xcode
where code not in (select code from smf4.dbo.fred)