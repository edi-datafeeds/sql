use wca
select isin from scmst
inner join bond on scmst.secid = bond.secid
inner join issur on scmst.issid = issur.issid
where
(maturitydate is null or maturitydate>getdate()-1)
and cntryofincorp='AN'
and sectycd = 'BND'
and statusflag<>'I'
and bond.actflag<>'D'