use portfolio
select distinct
rmg_eq_srf.EdiSecid,
wca.dbo.rd.announcedate,
wca.dbo.rd.rdid,
wca.dbo.rdprt.EventType
into rmg_corpcts2
from rmg_eq_srf
inner join wca.dbo.rd on rmg_eq_srf.EdiSecid = wca.dbo.rd.secid
left outer join wca.dbo.rdprt on wca.dbo.rd.rdid = wca.dbo.rdprt.rdid
where 
wca.dbo.rd.announcedate > '2005/10/01' 


use portfolio
SELECT 
rmg_corpcts2.EdiSecid, Count(rmg_corpcts2.rdid)as cts
into rmg_corps2
FROM rmg_corpcts2
GROUP BY rmg_corpcts2.EdiSecid
--order by rmg_eq_srf.EdiSecid

select * from rmg_corps2

select 
rmg_corps2.cts as CaCounts,
rmg_eq_srf.*
into rmg_eq_srf_cacounts
from rmg_eq_srf
left outer join rmg_corps2 on rmg_eq_srf.edisecid = rmg_corps2.edisecid
