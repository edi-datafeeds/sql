-- delete table ahead
use smf4
if exists (select * from sysobjects 
where name = 'tempUKCAflat')
 drop table tempUKCAflat

-- generate data for new increment
use wca
select *
from v54f_618_UKCAflatdiv
where changed > '2007/01/04'
(select acttime from tbl_opslog
where feeddate = '2007/01/04' and seq = 3)

use smf4
select * from tempukcaflat
where changed>getdate()-1

--delete all unconfirmed existing records
use smf4
delete from edicaoption
where exists 
(select
tempUKCAflat.eventid
from tempUKCAflat
where edicaoption.CADID = tempUKCAflat.eventid
and edicaoption.Payrate='U'
and edicaoption.distsedol='WCAD')

use smf4
delete from edicadetail
where exists 
(select
tempUKCAflat.eventid as CADID
from tempUKCAflat
where edicadetail.CADID = tempUKCAflat.eventid
and edicadetail.Confirmation='U'
and edicadetail.SsnYear='WCAD')


-- edicadetail insert where key not found
insert into edicadetail
(Actflag, Actdate, CADID, SecurityID, AcceptDate, Closedate,
PayDate, Confirmation, credate, ssnyear, CAtype)
select
tempUKCAflat.Actflag,
getdate(),
tempUKCAflat.eventid,
security.securityID,
tempUKCAflat.OptElectionDate,
tempUKCAflat.RecDate as Closedate,
--tempUKCAflat.ExDate,
tempUKCAflat.PayDate,
--'' as BusYrTo,
'U' as Confirmation,
tempUKCAflat.Created,
'WCAD' as ssnyear,
case when divtype = 'C' and divtype_2 is null
      and divtype_3 is null and divtype_4 is null
     then 'DD'
     when divtype = 'C' and (divtype_2 = 'C'
       or divtype_3 = 'C' or divtype_4 = 'C')
     then 'D2'
     when divtype = 'C' and (divtype_2 = 'S'
       or divtype_3 = 'S' or divtype_4 = 'S')
     then 'DO'
     when divtype = 'S' and divtype_2 is null
      and divtype_3 is null and divtype_4 is null
     then 'SD'
     else '' end as CAType
from tempUKCAflat
inner join security on tempUKCAflat.sedol = security.sedol
where
not exists
(select edicadetail.securityID from edicadetail
where edicadetail.securityID = security.securityID
and edicadetail.CADID = tempUKCAflat.eventid)

use smf4
select * from edicaoption
where payrate = 'U'
and distsedol = 'WCAD'

-- EDIcaoption insert where key not found

use smf4
insert into smf4.dbo.edicaoption
(Actflag, Actdate, CADID, SecurityID, CashDistCallRate,
Numerat, Denomin, OverseasTaxrate, FracEnt, OptionCurr,
NetOfTaxInd, credate, SecCodeOfDist, distsedol, payrate, Comment)
select 
tempUKCAflat.Actflag,
getdate(),
tempUKCAflat.eventid,
security.securityID,
tempUKCAflat.GrossDividend,
tempUKCAflat.RatioNew,
tempUKCAflat.RatioOld,
tempUKCAflat.Taxrate,
tempUKCAflat.Fractions,
tempUKCAflat.CurenCD,
'G' as NetOfTaxInd,
tempUKCAflat.Created,
tempUKCAflat.resisin,
'WCAD' as distsedol,
'U' as payrate,
tempUKCAflat.DivPeriodCD
from tempUKCAflat
inner join smf4.dbo.security on tempUKCAflat.sedol = security.sedol
where
not exists
(select edicaoption.cadid from edicaoption
where edicaoption.securityID = security.securityID
and edicaoption.CADID = tempUKCAflat.eventid)


-- shoch to EDICA system
use wca
insert into smf4.dbo.edicadetail
(Actflag, Actdate, Credate, CADID, SecurityID, Ostaxrate, CAType, SsnYear, Confirmation)
select
scmst.actflag,
scmst.acttime,
scmst.announcedate,
scmst.secid,
smf4.dbo.security.SecurityID,
scmst.sharesoutstanding,
'SH' as Catype,
'WCAS' as ssnyear,
'C' as Confirmation
from scmst
inner join sedol on scmst.secid = sedol.secid
               and sedol.cntrycd = 'GB'
inner join smf4.dbo.security on sedol.sedol = smf4.dbo.security.sedol
where scmst.actflag<>'D'
and sharesoutstanding <> ''
and sharesoutstanding is not null
and primaryexchgCD = 'GBLSE'

use smf4
select top 200 * from edicadetail
order by headerdate desc
where distsedol = 'WCAD'

use smf4
select * from edicadetail
where ssnyear = 'WCAD'