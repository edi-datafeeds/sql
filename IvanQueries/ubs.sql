use wca
select sectycd,col001 from portfolio.dbo.ubs
left outer join scmst on portfolio.dbo.ubs.col001 = isin
where isin is not null
order by sectycd

bond.secid as SecID,
scmst.isin as ISIN,
scmst.uscode as UsCode,
sedol.sedol as SEDOL,
issuer.issuername as Issuer,
issuer.cntryofincorp as Incorporation_Country,
scmst.parvalue as Original_Par_value,
exchg.exchgname as Exchange,
scexh.Localcode as Symbol,

use smf4
select * from 