use wca
select * 
into portfolio.dbo.unftse
from portfolio.dbo.ftse
left outer join sedol on col001 =sedol
where sedol.secid is null

use smf4
select portfolio.dbo.unftse.col001, issuername, longdesc, statusflag
from portfolio.dbo.unftse
left outer join security on col001 = security.sedol
left outer join issuer on security.issuerid = issuer.issuerid
order by statusflag, issuername

where sdchg.secid is not null

select * from portfolio.dbo.unftse