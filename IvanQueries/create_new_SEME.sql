USE [client]
GO
/****** Object:  Table [dbo].[SEME_eq_11_isin]    Script Date: 12/06/2011 21:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEME_nasdaq](
	[CORP] [nchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
	[SEME] [bigint] NOT NULL,
	[PREV] [bigint] NULL,
	[Acttime] [datetime] NULL,
	[MTflag] [smallint] NOT NULL,
 CONSTRAINT [PK_SEME_nasdaq] PRIMARY KEY CLUSTERED 
(
	[CORP] ASC,
        [MTflag]
) ON [PRIMARY]
) ON [PRIMARY]
