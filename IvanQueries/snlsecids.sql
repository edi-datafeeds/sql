use wca
select scmst.secid
into portfolio.dbo.snl_secid
from scmst
inner join bond on scmst.secid = bond.secid
where
scmst.issid in (select issid from portfolio.dbo.snl_issid)
and scmst.secid not in (select col001 from portfolio.dbo.snlscmst1)
--and scmst.acttime<'2011/11/08 16:00:00'
union

select scmst.secid
from scmst
inner join bond on scmst.secid = bond.secid
where
scmst.issid in (select issid from portfolio.dbo.snl_issid)
and bond.secid not in (select col001 from portfolio.dbo.snlbond1)
--and (scmst.acttime<'2011/11/08 16:00:00' and bond.acttime<'2011/11/08 16:00:00')
