use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate as Created,
SCMST.Acttime as Changed,
SCMST.SecID,
SCMST.ISIN,
SCMST.IssID,
SCMST.SecurityDesc,
case when SCMST.Statusflag<>'I' or SCMST.Statusflag is null then 'A' ELSE 'I' end as StatusFlag,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.X as CommonCode,
SCMST.Holding,
SCMST.StructCD,
SCMST.RegS144A
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
OR substring(scmst.isin,1,2) = 'CA' and BOND.secid not in (select secid from SCEXH))
and scmst.acttime> '2001/01/01'
and (isin = 'CA077906NB72'
or isin = 'CA077906NS08'
or isin = 'CA077906MZ59'
or isin = 'CA077906MT99'
or isin = 'CA077906MR34'
or isin = 'CA077906MD48'
or isin = 'CA077906NL54'
or isin = 'CA077906NC55'
or isin = 'CA077906NA99'
or isin = 'CA077906MY84'
or isin = 'CA077906MW29'
or isin = 'CA077906MC64'
or isin = 'CA077906NM38'
or isin = 'CA077906MX02'
or isin = 'CA077906LX11'
or isin = 'CA077906NG69'
or isin = 'CA077906LY93'
or isin = 'CA077906MU62'
or isin = 'CA077906NP68'
or isin = 'CA077906NF86'
or isin = 'CA077906MM47'
or isin = 'CA077906MF95'
or isin = 'CA74815ZN657'
or isin = 'CA077906NQ42'
or isin = 'CA077906NJ09'
or isin = 'CA077906MJ18'
or isin = 'CA077906ME21'
or isin = 'CA077906NR25'
or isin = 'CA077906LZ68'
or isin = 'CA077906MA09'
or isin = 'CA077906MG78'
or isin = 'CA077906MN20'
or isin = 'CA077906MP77'
or isin = 'CA077906MV46'
or isin = 'CA42205VAA40'
or isin = 'CA42205VAB23'
or isin = 'CA44889ZAX48'
or isin = 'CA683234VF27'
or isin = 'CA683234WZ71'
or isin = 'CA683234YF99'
or isin = 'CA683234YH55'
or isin = 'CA683234ZV31'
or isin = 'CA72819MK911'
or isin = 'CA76691ZAC01'
or isin = 'CA76691ZAE66'
or isin = 'CA76691ZAF32'
or isin = 'CA76691ZAG15'
or isin = 'CA76691ZAH97'
or isin = 'CA780085RH17'
or isin = 'CA780085TH98'
or isin = 'CA780085TR70'
or isin = 'CA780085TR70'
or isin = 'CA780085TS53'
or isin = 'CA780085UQ78'
or isin = 'CA780085VC73'
or isin = 'CA861920A307'
or isin = 'CA861920A489'
or isin = 'CA861920A554')


select * from scmst
where
isin = 'CA42205VAA40'
or isin = 'CA42205VAB23'

select * from scexh
where secid = 415442

select * from bond
where secid = 415442

