USE WOL
update tbl_wol_sec set fld_isin = fld_sedol
where fld_isin is null or fld_isin = '' or fld_isin = 'NO ISIN'
go

use wol
update tbl_wol_sec set fld_iss_sec = fld_iss_sec + ' ('+fld_sedol+')'
where fld_iss_sec not like '%'+fld_sedol+'%'
and substring(fld_isin,5,7)<>fld_sedol
and fld_sec_ssn_date>'2007/04/01'
go
