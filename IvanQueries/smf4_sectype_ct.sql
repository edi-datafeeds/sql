use smf4
SELECT security.sectype,
Count(security.securityid) AS sectype_ct
into sectyct_defunct
from security
where 
statusflag='D'
and actdate<getdate()-180
GROUP BY security.sectype

use smf4
select sectype, [name] as SectypeName, sectype_ct, typegroup
from sectyct
left outer join sectype on sectyct.sectype = sectype.code

use smf4
select sectype, [name] as SectypeName, sectype_ct, typegroup
from sectyct_defunct
left outer join sectype on sectyct_defunct.sectype = sectype.code
