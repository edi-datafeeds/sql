use wca
select distinct
 
wca.dbo.scmst.Acttime as Scmst_ActTime,
wca.dbo.scmst.Actflag as Scmst_ActFlag,
wca.dbo.scmst.SecID as Scmst_SecID,
wca.dbo.scmst.ParentSecID as Scmst_ParentSecID,
wca.dbo.scmst.IssID as Scmst_IssID,
wca.dbo.scmst.SectyCD as Scmst_SecTyCD,
wca.dbo.scmst.SecurityDesc as Scmst_SecurityDesc,
wca.dbo.scmst.Statusflag as Scmst_StatusFlag,
wca.dbo.scmst.StatusReason as Scmst_StatusReason,
wca.dbo.scmst.PrimaryExchgCD as Scmst_PrimaryExchgCD,
wca.dbo.scmst.CurenCD as Scmst_CurenCD,
wca.dbo.scmst.ParValue as Scmst_ParValue,
wca.dbo.scmst.IssuePrice as Scmst_IssuePrice,
wca.dbo.scmst.PaidUpValue as Scmst_PaidUpValue,
wca.dbo.scmst.Voting as Scmst_Voting,
wca.dbo.scmst.FYENPPDate as Scmst_FYENPPDate,
wca.dbo.scmst.USCode as Scmst_USCode,
--portfolio.dbo.p04.Isin as Prices_ISIN,
wca.dbo.scmst.ISIN as Scmst_ISIN,
--portfolio.dbo.p04.sedol as Prices_SEDOL,
wca.dbo.sedol.sedol as WCA_SEDOL,
wca.dbo.scmst.SharesOutstanding as Scmst_SharesOutstanding,
wca.dbo.scmst.WKN as Scmst_WKN,
wca.dbo.scmst.REGS144A as Scmst_REGS144A,
wca.dbo.scmst.SourceDate as Scmst_SourceDate,
--portfolio.dbo.p04.LocalCode as Prices_LocalCode,
--portfolio.dbo.p04.Currency as Prices_TradingCurrency,
--portfolio.dbo.p04.issuername as Prices_IssuerName,
--portfolio.dbo.p04.USCode as Prices_USCode,
--portfolio.dbo.p04.PrimaryExchgCD as Prices_PrimaryExchangeCode,
--portfolio.dbo.p04.ExchgCD as Prices_ExchangeCode,
wca.dbo.scexh.Acttime as Exchange_ActTime,
wca.dbo.scexh.Actflag as Exchange_ActFlag,
wca.dbo.scexh.ExchgCD as Exchange_ExchangeCode,
wca.dbo.scexh.ListStatus as Exchange_ListStatus,
wca.dbo.scexh.Lot as Exchange_LotSize,
wca.dbo.scexh.MinTrdgQty as Exchange_MinTradingQuantity,
wca.dbo.scexh.TradeStatus as Exchange_TradeStatus,
wca.dbo.scexh.LocalCode as Exchange_LocalCode,
wca.dbo.scexh.ScExhID as Exchange_ScExhID,
wca.dbo.issur.Acttime as Issur_Acttime,
wca.dbo.issur.Actflag as Issur_Actflag,
wca.dbo.issur.IssID as Issur_IssID,
wca.dbo.issur.IssuerName as Issur_IssuerName,
wca.dbo.issur.IndusID as Issur_IndusID,
wca.dbo.issur.CntryofIncorp as Issur_CntryOfIncorp,
wca.dbo.issur.FinancialYearEnd as Issur_FinancialYearEnd
 
from wca.dbo.scmst
 
left outer join wca.dbo.issur on wca.dbo.scmst.issid=wca.dbo.issur.issid
left outer join wca.dbo.scexh on wca.dbo.scmst.secid=wca.dbo.scexh.secid
left outer join portfolio.dbo.p04 on wca.dbo.scmst.secid=portfolio.dbo.p04.secid
LEFT OUTER JOIN wca.dbo.exchg ON wca.dbo.scexh.ExchgCD = wca.dbo.exchg.ExchgCD
LEFT OUTER JOIN wca.dbo.sedol ON wca.dbo.scmst.SecID = wca.dbo.sedol.SecID
                      AND wca.dbo.exchg.cntryCD = wca.dbo.Sedol.CntryCD
                         
where 
wca.dbo.scmst.Acttime > '2007-09-30'
and portfolio.dbo.p04.secid is null
and wca.dbo.scexh.ListStatus <> 'D'
and (wca.dbo.scmst.statusflag<> 'I' or wca.dbo.scmst.statusflag is null)
order by scmst.sectycd
 
 
 
 
 

 
 