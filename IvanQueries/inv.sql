use wca
select col001, newuscode, eventtype
from portfolio.dbo.invpart2
left outer join scmst on col001 = uscode
left outer join icc on col001 = icc.olduscode
where
scmst.secid is null
and icc.secid is not null
order by eventtype, col001

use wca
select col001
from portfolio.dbo.invpart2
left outer join scmst on col001 = uscode
left outer join icc on col001 = icc.olduscode
where
scmst.secid is null
and icc.secid is null
order by eventtype, col001
