use wca
select
sd.oldratio, sd.newratio,
consd.oldratio, consd.newratio,
rts.ratioold, rts.rationew,
ent.ratioold, ent.rationew,
bon.ratioold, bon.rationew,
prf.ratioold, prf.rationew,
--dist.ratioold, dist.rationew,
--dmrgr.ratioold, dmrgr.rationew,
exdt.eventtype, scmst.sharesoutstanding, shoch.newsos, shochseq1.effectivedate, shoch.announcedate, exdt.exdate, unsecid, exdt.exchgcd
from portfolio.dbo.otcmarkets
inner join scmst on portfolio.dbo.otcmarkets.unsecid = scmst.secid
left outer join rd on unsecid = rd.secid
left outer join exdt on rd.rdid = exdt.rdid and primaryexchgcd = exdt.exchgcd
left outer join shochseq1 on unsecid = shochseq1.secid and 1 = shochseq1.seqnum
left outer join shoch on shochseq1.shochid = shoch.shochid
left outer join consd on rd.rdid = consd.rdid
left outer join sd on rd.rdid = sd.rdid
left outer join rts on rd.rdid = rts.rdid
left outer join ent on rd.rdid = ent.rdid
left outer join bon on rd.rdid = bon.rdid
left outer join dist on rd.rdid = dist.rdid
left outer join dmrgr on rd.rdid = dmrgr.rdid
left outer join prf on rd.rdid = prf.rdid
where
exdt.exchgcd is not null
and exdate>getdate()-4000
and exdate<getdate()
and eventtype<>'DIV'
and eventtype<>'PO'
and eventtype<>'ODDLT'
and shoch.effectivedate<exdate
order by exdate

select * from scmst
where
secid = 67062
