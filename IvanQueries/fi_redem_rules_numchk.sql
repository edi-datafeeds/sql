-- MCAL
use wca
select distinct redem.secid, redemid
from redem
inner join bond on redem.secid = bond.secid
where
mandoptflag = 'M'
and partfinal = 'F'
and not (redemtype='MAT' or maturitydate=redemdate)
and redemdate is not null
and redemdate <> '1800/01/01'
and redemtype <> 'PUT'

union

select redem.secid, redemid
from redem
inner join bond on redem.secid = bond.secid
where
redemtype = 'PUT'
and not (maturitydate=redemdate or maturitydate is null)
and redemdate is not null
and redemdate <> '1800/01/01'

union

select redem.secid, redemid
from redem
inner join bond on redem.secid = bond.secid
where
mandoptflag = 'M'
and partfinal = 'F'
and redemdate is not null
and redemdate <> '1800/01/01'
and (redemtype='MAT' or maturitydate=redemdate)

union

select redem.secid, redemid
from redem
inner join bond on redem.secid = bond.secid
where
partfinal = 'P'
and redemdate is not null
and redemdate <> '1800/01/01'
and not (redemtype='MAT' or maturitydate=redemdate)
and poolfactor=''
and redemtype <> 'PUT'

union

select redem.secid, redemid
from redem
inner join bond on redem.secid = bond.secid
where
--mandoptflag = 'M'
partfinal = 'P'
and redemdate is not null
and redemdate <> '1800/01/01'
and not (redemtype='MAT' or maturitydate=redemdate)
and poolfactor<>''
and redemtype <> 'PUT'

