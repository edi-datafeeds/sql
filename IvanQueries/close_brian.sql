use smf4
select issuername,
col001 as sedol,
longdesc, 
cinccode as CntryofIncorp,
cregcode as CntryofReg,
Statusflag as TradingStatus,
opol as PlaceofListing,
miclist.cntrycd as CntryofListing,
sectype.typegroup as SecurityGroup,
sectype

from portfolio.dbo.bsedol
left outer join security on substring(col001,1,6) = substring(sedol,1,6)
left outer join issuer on security.issuerid = issuer.issuerid
left outer join miclist on security.opol = miclist.mic
                    and security.opol is not null
left outer join sectype on sectype = code
order by securitygroup, sectype