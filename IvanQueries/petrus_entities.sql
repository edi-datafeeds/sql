use wca
select distinct
scmst.Issid,
Issuername,
CntryofIncorp,
exchg.cntrycd as ExchangeCountry,
PrimaryExchgcd,
localcode as PrimaryLocalcode,
liststatus
from scmst
inner join scexh on scmst.primaryexchgcd = scexh.exchgcd and scmst.secid=scexh.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
where
(exchg.cntrycd = 'GB' or exchg.cntrycd = 'US')
and Primaryexchgcd<>''
and localcode<>''
and exchg.actflag<>'D'
and scmst.actflag<>'D'
and scexh.actflag<>'D'
and primaryexchgcd<>'GBBND'
and  primaryexchgcd<>'USBND'
order by primaryexchgcd, issuername