
select smf4.dbo.sectype.typegroup, smf4.dbo.sectype.[name], hsbc0918.* from hsbc0918
left outer join wca.dbo.sedol on hsbc0918.col001 = wca.dbo.sedol.sedol
left outer join smf4.dbo.security on hsbc0918.col001 = smf4.dbo.security.sedol
left outer join smf4.dbo.sectype on smf4.dbo.security.sectype = smf4.dbo.sectype.code
where
wca.dbo.sedol.secid is not null
order by smf4.dbo.sectype.typegroup, smf4.dbo.sectype.[name]

use portfolio
select smf4.dbo.sectype.typegroup, smf4.dbo.sectype.[name], hsbc0918.*, smf4.dbo.security.* from hsbc0918
left outer join wca.dbo.sedol on hsbc0918.col001 = wca.dbo.sedol.sedol
left outer join hsbcold on hsbc0918.col001 = hsbcold.col001
left outer join smf4.dbo.security on hsbc0918.col001 = smf4.dbo.security.sedol
left outer join smf4.dbo.sectype on smf4.dbo.security.sectype = smf4.dbo.sectype.code
where wca.dbo.sedol.secid is null
and hsbcold.col001 is null
and smf4.dbo.security.statusflag <> 'D'
and smf4.dbo.security.securityid is not null
order by smf4.dbo.sectype.typegroup, smf4.dbo.sectype.[name]

use wca
select * from scmst
where isin = 'GB0000109792'


