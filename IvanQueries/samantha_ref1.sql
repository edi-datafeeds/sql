use wca
select col001, sedol, sectype, smf4.dbo.security.statusflag, opol
from portfolio.dbo.advent
left outer join scmst on col001 = scmst.isin
left outer join smf4.dbo.security on col001 = smf4.dbo.security.isin
where secid is null
order by col001
