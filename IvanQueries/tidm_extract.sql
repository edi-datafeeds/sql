--filepath=o:\Datafeed\Smf\122\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.122
--suffix=
--fileheadertext=EDI_SMF_122_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Smf\122\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT distinct
smf4.dbo.security.Sedol,
smf4.dbo.security.Isin,
smf4.dbo.security.OPOL,
smf4.dbo.issuer.Issuername,
smf4.dbo.security.Longdesc,
smf4.dbo.market.Tidm as Tidisplay
FROM security
INNER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
left outer join market on security.securityid = market.securityid
            and security.opol = market.mic
            and (security.opol = 'XLON')
where
security.Actflag <> 'D'
and security.statusflag<>'D'
and security.confirmation='C'
and tidm<>''
and tidm is not null
