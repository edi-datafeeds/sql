use wca
select distinct
scmst.isin,
scmst.secid,
CONVERT ( varchar , lstat.effectivedate,111) as effectivedate,
scexh.exchgcd
from scmst
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join lstat on scexh.secid = lstat.secid and scexh.exchgcd = lstat.exchgcd
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
where
statusflag='I'
and lstatstatus = 'D'
and isin <>''
and (lstat.effectivedate >'2008/03/31' or (lstat.secid is null and scmst.acttime>'2008/03/31'))
and scmst.actflag<>'D'
and scexh.actflag<>'D'
and scmst.sectycd <> 'UNT'
and scmst.sectycd <> 'CDI'
and scmst.sectycd <> 'TRT'
and scmst.sectycd <> 'DRT'
and sectygrp.secgrpid<3
and (exchg.cntrycd = 'AT'
or exchg.cntrycd = 'BE'
or exchg.cntrycd = 'BG'
or exchg.cntrycd = 'CH'
or exchg.cntrycd = 'CY'
or exchg.cntrycd = 'CZ'
or exchg.cntrycd = 'DE'
or exchg.cntrycd = 'DK'
or exchg.cntrycd = 'EE'
or exchg.cntrycd = 'ES'
or exchg.cntrycd = 'FI'
or exchg.cntrycd = 'FR'
or exchg.cntrycd = 'GB'
or exchg.cntrycd = 'GR'
or exchg.cntrycd = 'HU'
or exchg.cntrycd = 'IE'
or exchg.cntrycd = 'IS'
or exchg.cntrycd = 'IT'
or exchg.cntrycd = 'LT'
or exchg.cntrycd = 'LU'
or exchg.cntrycd = 'LV'
or exchg.cntrycd = 'MT'
or exchg.cntrycd = 'NL'
or exchg.cntrycd = 'NO'
or exchg.cntrycd = 'PL'
or exchg.cntrycd = 'PT'
or exchg.cntrycd = 'RO'
or exchg.cntrycd = 'SE'
or exchg.cntrycd = 'SI'
or exchg.cntrycd = 'SK')
order by isin, scexh.exchgcd


--select * from lstat
--where secid = 115665
