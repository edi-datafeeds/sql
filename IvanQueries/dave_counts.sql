use wca
SELECT COUNT(*)
FROM bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
((cntryofincorp = 'CA' and substring(isin,1,2)='CA')
or (cntryofincorp = 'DE' and substring(isin,1,2)='DE')
or (cntryofincorp = 'FR' and substring(isin,1,2)='FR')
or (cntryofincorp = 'IT' and substring(isin,1,2)='IT')
or (cntryofincorp = 'JP' and substring(isin,1,2)='JP')
or (cntryofincorp = 'GB' and substring(isin,1,2)='GB')
or (cntryofincorp = 'US' and substring(isin,1,2)='US'))
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate())
and bond.actflag<>'D'

