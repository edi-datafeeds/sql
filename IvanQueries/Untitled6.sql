use wca
select 'v54f_620_dividend' as viewname, caref,isin, secid, eventid
from v54f_620_dividend

where caref in (select caref from portfolio.dbo.xcaref)

union

select 'v50f_620_company_meeting' as viewname, caref,isin, secid, eventid
from v50f_620_company_meeting
where caref in (select caref from portfolio.dbo.xcaref)
--order by v50f_620_company_meeting

--use portfolio
--insert into xcaref
--select
--'604010047145914' as caref

--use portfolio
--delete xcaref

select * from v54f_620_franking
where changed between '2009/03/27' and '2009/03/28'