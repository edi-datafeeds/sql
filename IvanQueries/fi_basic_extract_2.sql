
use wca
select
'cntrycd',
'issuername', 
'securitydesc',
'mic',
'exchgcd',
'secid',
'isin',
'uscode',
'commoncode',
'Localcode'
from scmst

use wca
select
exchg.cntrycd,
issur.issuername, 
scmst.securitydesc,
exchg.mic,
scexh.exchgcd,
scmst.secid,
scmst.isin,
scmst.uscode,
scmst.x as commoncode,
scexh.Localcode,
case when bond.Bondsrc is null or bond.Bondsrc='' or bond.Bondsrc='PP' or bond.Bondsrc='SR' then '' else 'Yes' end as Backup_Document
from scmst
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
scexh.actflag<>'D'
and scmst.actflag<>'D'
and bond.actflag<>'D'
and scexh.secid is not null
and scmst.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus<>'D'
and (bond.maturitydate>getdate()-1 or bond.maturitydate is null)

union

select
exchg.cntrycd,
issur.issuername, 
scmst.securitydesc,
exchg.mic,
scexh.exchgcd,
scmst.secid,
scmst.isin,
scmst.uscode,
scmst.x as commoncode,
scexh.Localcode,
case when bond.Bondsrc is null or bond.Bondsrc='' or bond.Bondsrc='PP' or bond.Bondsrc='SR' then '' else 'Yes' end as Backup_Document
from scmst
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
scmst.actflag<>'D'
and bond.actflag<>'D'
and scexh.secid is null
and scmst.actflag<>'D'
and scmst.statusflag<>'I'
and (bond.maturitydate>getdate()-1 or bond.maturitydate is null)


--select distinct bondsrc from bond