

use wca
select top 50 
shoch.acttime,
shoch.actflag,
scmst.isin, 
scmst.uscode, 
issur.issuername, 
oldsos, newsos, 
shoch.effectivedate
from scmst
inner join shochseq on scmst.secid = shochseq.secid
inner join issur on scmst.issid = issur.issid
inner join shoch on shochseq.shochid = shoch.shochid
where scmst.actflag<>'d'
and (statusflag<>'I' or statusflag is null)
and newsos<>''
and seqnum=1
and sectycd = 'ETF'
order by shoch.effectivedate desc
