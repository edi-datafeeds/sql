use smf4
SELECT security.sectype, Count(security.sectype) AS Ct
FROM smf4.dbo.security
where
unitofqcurrcode=''
and security.actflag<>'D'
and actdate>'2013/03/01 12:00:00'
and (sectype='AA'
or sectype='AB'
or sectype='AC'
or sectype='AD'
or sectype='AE'
or sectype='AJ'
or sectype='AK'
or sectype='AL'
or sectype='AM'
or sectype='AO'
or sectype='AP'
or sectype='CB'
or sectype='CC'
or sectype='CG'
or sectype='DC'
or sectype='SP'
or sectype='UW'
or sectype='CA')
GROUP BY security.sectype