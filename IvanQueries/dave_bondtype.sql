use wca
select
issuername,
securitydesc,
bond.secid,
isin,
sectycd,
exchgcd,
bondtype
from bond
inner join scmst on bond.secid = scmst.secid
inner join scexh on bond.secid = scexh.secid
inner join issur on scmst.issid = issur.issid
where
statusflag<>'I'
and bond.actflag<>'D'

