USE portfolio
GO
CREATE PROCEDURE sp_cpopt_flagging

AS

DECLARE MyCursor CURSOR LOCAL FORWARD_ONLY FOR
  SELECT secid,cpoptid FROM cpopt_temp 
  ORDER BY secid,fromdate desc

OPEN MyCursor
DECLARE @Ssecid int
DECLARE @Scpoptid int
DECLARE @Tsecid int
DECLARE @Tcpoptid int
FETCH NEXT FROM MyCursor INTO @Tsecid, @Tcpoptid
WHILE @@FETCH_STATUS = 0
BEGIN   
   IF @Ssecid <> @Tsecid
   BEGIN 
      UPDATE cpopt_temp SET cpopt_temp.Actflag= 'F'
      WHERE cpopt_temp.cpoptid = @Tcpoptid
   END
   SET @Ssecid =  @Tsecid
   SET  @Scpoptid = @Tcpoptid
   FETCH NEXT FROM MyCursor INTO @Tsecid, @Tcpoptid
END
CLOSE MyCursor
DEALLOCATE MyCursor


exec sp_cpopt_flagging


SELECT actflag,secid,cpoptid, fromdate FROM cpopt_temp 
ORDER BY secid,fromdate desc
