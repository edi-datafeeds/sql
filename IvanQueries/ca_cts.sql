
use wca
select 
cntrycd,
count(cntrycd) as [2008]
into ct08
from rd
left outer join scexh on rd.secid = scexh.secid
left outer join cntry on substring(scexh.exchgcd,1,2) = cntry.cntrycd
where year(rd.announcedate) = '2008'
group by cntrycd


select
country,
ct01.[2001],
ct02.[2002],
ct03.[2003],
ct04.[2004],
ct05.[2005],
ct06.[2006],
ct07.[2007],
ct08.[2008],
cttot.Total
from cttot
left outer join ct01 on cttot.cntrycd = ct01.cntrycd
left outer join ct02 on cttot.cntrycd = ct02.cntrycd
left outer join ct03 on cttot.cntrycd = ct03.cntrycd
left outer join ct04 on cttot.cntrycd = ct04.cntrycd
left outer join ct05 on cttot.cntrycd = ct05.cntrycd
left outer join ct06 on cttot.cntrycd = ct06.cntrycd
left outer join ct07 on cttot.cntrycd = ct07.cntrycd
left outer join ct08 on cttot.cntrycd = ct08.cntrycd
left outer join cntry on cttot.cntrycd = cntry.cntrycd


