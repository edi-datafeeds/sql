use wca
select code, shoch.newsos, shoch.effectivedate, shoch.announcedate from portfolio.dbo.xcode
left outer join wca.dbo.scmst on portfolio.dbo.xcode.code = wca.dbo.scmst.isin
left outer join shochseq on scmst.secid = shochseq.secid
               and 1 = seqnum
left outer join shoch on shochseq.shochid = shoch.shochid
