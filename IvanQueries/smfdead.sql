use smf4
select distinct issuer.issuername, security.longdesc, security.sedol, security.isin, wca.dbo.scmst.isin as EDIIsin from security
left outer join sectype on sectype = code
left outer join issuer on security.issuerid = issuer.issuerid
left outer join wca.dbo.scmst on security.isin = wca.dbo.scmst.isin
                and security.isin <> ''
                and security.isin is not null
left outer join wca.dbo.SEDOL on security.SEDOL = wca.dbo.SEDOL.SEDOL
where security.statusflag='D'
and typegroup = 'debt'
and closedate>getdate()-1
and (wca.dbo.scmst.statusflag<>'I' OR wca.dbo.scmst.SECID IS NULL)
and (wca.dbo.SEDOL.DEFUNCT<>'t' OR wca.dbo.SEDOL.SECID IS NULL)
