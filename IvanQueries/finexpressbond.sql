use wca
select col001,
issuername,
indusid,
securitydesc,
maturitydate,
bond.curencd,
ratingagency,
ratingdate,
rating
from portfolio.dbo.feisin
left outer join scmst on col001 = isin
left outer join issur on scmst.issid = issur.issid
left outer join bond on scmst.secid = bond.secid
left outer join crdrt on scmst.secid = crdrt.secid

use wca
select col001,
issuername,
indusid,
securitydesc,
maturitydate,
bond.curencd,
ratingagency,
ratingdate,
rating
from portfolio.dbo.fesedol
left outer join sedol on col001 = sedol
left outer join scmst on sedol.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join bond on scmst.secid = bond.secid
left outer join crdrt on scmst.secid = crdrt.secid

