use portfolio

select ct, 
case when b1.outstandingamount='' then b2.outstandingamount else b1.outstandingamount end as outstandingamount, 
case when b1.outstandingamount='' then b2.outstandingamountdate else b1.outstandingamountdate end as outstandingamountdate,
case when b1.issueamount='' then b2.issueamount else b1.issueamount end as issueamount, 
case when b1.issueamount='' then b2.issueamountdate else b1.issueamountdate end as issueamountdate
from sapbo
left outer join wca.dbo.scmst as s1 on sapbo.isin= s1.isin
left outer join wca.dbo.bond as b1 on s1.secid = b1.secid
left outer join wca.dbo.scmst as s2 on sapbo.cusip= s2.uscode
left outer join wca.dbo.bond as b2 on s2.secid = b2.secid
order by ct

delete pushsap

insert into pushsap
select secid from wca.dbo.scmst
where
isin ='AU0000XQLQK1'
or isin = 'XS0454984765'
or isin = 'JP1201091939'
or isin = 'TRT060814T18'
or isin = 'XS0431157584'
