use wca
select 
col001, 
sedol,
sectype,
sectycd,
sedol,
[name],
typegroup,
wca.dbo.scmst.statusflag as wca_statusflag,
smf4.dbo.security.statusflag as smf_statusflag,
opol
from portfolio.dbo.markit
left outer join scmst on col001 = scmst.isin
left outer join smf4.dbo.security on col001 = smf4.dbo.security.isin
left outer join smf4.dbo.sectype on smf4.dbo.security.sectype = smf4.dbo.sectype.code
where secid is not null
-- and securityid is null
--  order by typegroup, smf_statusflag, [name]
order by wca_statusflag, sectycd
