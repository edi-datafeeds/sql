use wol
select distinct 
ajf.id as AjfID,
sitlist.sitname,
sec.sedol,
cab.issuername,
ajf.factor,
ajf.closing,
ajf.iteration
from cab
inner join sec on cab.cabepoch = sec.cabepoch
                 and cab.cabedition = sec.cabedition
inner join dir on cab.cabepoch = dir.cabepoch
                 and cab.cabedition = dir.cabedition
inner join ajf on sec.sedol = ajf.sedol
                 and dir.eventdate = ajf.exdate
left outer join sitlist on cab.sitcd = sitlist.sitcd

