use wca
SELECT exchg.exchgname, min(rd.announcedate) as firstdate, max(rd.announcedate) as lastdate
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join rd on scmst.secid = rd.rdid
where
scexh.actflag<>'D'
GROUP BY exchg.exchgname order by exchg.exchgname
