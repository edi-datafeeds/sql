use wca
select 
portfolio.dbo.citidivs.security, 
portfolio.dbo.citidivs.security_description, 
v54f_610_dividend.exdate,
v54f_610_dividend.recdate,
v54f_610_dividend.paydate,
v54f_610_dividend.grossdividend,
v54f_610_dividend.exchgcd,
v54f_610_dividend.primaryex,
v54f_610_dividend.created
from portfolio.dbo.citidivs
left outer join v54f_610_dividend on portfolio.dbo.citidivs.security = v54f_610_dividend.isin
where paydate > '2006/04/01'
and action<> 'Deleted'
and v54f_610_dividend.grossdividend is not null
and v54f_610_dividend.grossdividend <> ''






