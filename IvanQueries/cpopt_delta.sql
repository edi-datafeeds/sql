use portfolio
delete from cpopt_temp
go

select * from portfolio.dbo.cpopt_temp

use wca
insert into portfolio.dbo.cpopt_temp
select cpopt.* from portfolio.dbo.fisin
inner join scmst on portfolio.dbo.fisin.code = scmst.isin
inner join cpopt on scmst.secid = cpopt.secid
where
portfolio.dbo.fisin.actflag='I'
and accid = 185

select * from scmst
where secid=175536

use portfolio
select * from fisin
where
code = 'USU37818AA62'



use portfolio
delete from cpopt_future
go

--select min(secid) from cpopt where

use portfolio
declare @mycount as integer
set @mycount=(select min(secid) from cpopt_temp)

WHILE @mycount<(select max(secid)+1 from cpopt_temp)
BEGIN

use portfolio

insert into cpopt_future

select top 1
cpopt_temp.cpoptid,
cpopt_temp.secid,
Fromdate,
todate
from cpopt_temp
inner join wca.dbo.bond on cpopt_temp.secid = wca.dbo.bond.secid
where
wca.dbo.bond.secid = @mycount
and cpopt_temp.actflag<>'D'
and cpopt_temp.fromdate>getdate()-1
order by cpopt_temp.secid, cpopt_temp.fromdate

set @mycount = @mycount+1

END

go

--select * from portfolio.dbo.cpopt_future


use portfolio
delete from cpopt_past
go

use wca
declare @mycount as integer
set @mycount=130000

WHILE @mycount<(select max(secid) from cpopt)
BEGIN

use wca

insert into portfolio.dbo.cpopt_future_todate

select top 1
cpopt.cpoptid,
cpopt.secid,
Fromdate,
todate
from cpopt
inner join bond on cpopt.secid = bond.secid

where
bond.secid = @mycount
and cpopt.actflag<>'D'
and cpopt.todate>getdate()-1
order by cpopt.secid, cpopt.todate

set @mycount = @mycount+1

END

go

--select * from portfolio.dbo.cpopt_future_todate
