
use portfolio
if exists (select * from sysobjects where name = 'CPOPT_ct')
 drop table CPOPT_ct
use wca
select cpopt.secid, count(secid) as ct
into portfolio.dbo.CPOPT_ct
from cpopt
where
cpopt.actflag <> 'D'
and cptype<>'KO'
GROUP BY cpopt.secid
order by count(secid)
go

print ""
go

print " INDEXING portfolio.dbo.CPOPT_ct ,please  wait ....."
GO 
use portfolio
ALTER TABLE CPOPT_ct ALTER COLUMN  secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_ct] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_ct] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 
