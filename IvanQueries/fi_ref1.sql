use wca
select
portfolio.dbo.prospect.col001 as isin,
scmst.sectycd,
scmst.statusflag
from portfolio.dbo.prospect
left outer join scmst on portfolio.dbo.prospect.col001 = scmst.isin
where 
scmst.actflag<>'D' or scmst.secid is null
order by sectycd, portfolio.dbo.prospect.isin

