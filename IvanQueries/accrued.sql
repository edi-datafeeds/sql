use wca
select
SCMST.SecID,
SCMST.ISIN,
SEDOL.SEDOL,
intpy.bCurenCD,
BOND.InterestRate,
BOND.InterestPaymentFrequency,
BOND.InterestAccrualConvention,
bond.parvalue,
intpy.bParvalue,
BOND.Denomination1,
BOND.InterestBasis,
int.InterestFromDate,
int.InterestToDate,
intpy.AnlCoupRate,
intpy.GrossInterest,
intpy.NetInterest,
intpy.DomesticTaxRate,
intpy.IntRate
FROM
INT
left outer JOIN RD ON RD.RdID = INT.RdID
left outer join SCMST ON RD.SecID = SCMST.SecID
left outer join BOND ON SCMST.SecID = BOND.SecID
left outer join SEDOL ON SCMST.SecID = SEDOL.SecID
                        and sedol.cntrycd = 'GB'
left outer JOIN INTPY ON INT.RdID = INTPY.RDID
where
intpy.InterestToDate>'2007/01/01'
and scmst.sectycd = 'BND'
and bond.actflag<>'D'
and scmst.actflag<>'D'
and int.actflag<>'D'
and intpy.actflag<>'D'
and rescindinterest='F'
--and interestbasis <> 'FR'
and grossinterest<>''
--and netinterest<>''
and sedol.cntrycd = 'GB'
and sedol = '0903671'
--and substring(denomination1,1,3)<>substring(bparvalue,1,3)

