use wca
select distinct isin
into portfolio.dbo.indus1
from portfolio.dbo.mkrep
inner join issur on portfolio.dbo.mkrep.secid = issid
inner join scmst on issur.issid = scmst.issid
where indusid is not null
and scmst.statusflag<>'I'
and scmst.actflag<>'D'
and scmst.isin<>''
and scmst.isin is not null


use smf4
select distinct issuer.*
from portfolio.dbo.indus1
inner join security on portfolio.dbo.indus1.isin = security.isin
inner join issuer on security.issuerid = issuer.issuerid
