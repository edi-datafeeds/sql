use portfolio
select
case  when p04.secid is not null then 'EdiSecID MATCH'
      when p2.localcode <> '' then 'TICKER MATCH'
      when p3.Isin <> ''  then 'ISIN MATCH'
      when p4.Uscode <> ''  then 'USCODE MATCH'
      when p5.sedol <> ''  then 'SEDOL MATCH'
else 'NO MATCH' End as PriceLink,
rmg_eq_srf_cacounts.*
--into rmg_eq_srf_all
from rmg_eq_srf_cacounts
left outer join p04 on rmg_eq_srf_cacounts.edisecid = p04.secid
                                   and rmg_eq_srf_cacounts.mic = p04.mic
left outer join p04 as p2 on rmg_eq_srf_cacounts.ticker = p2.localcode
                                   and rmg_eq_srf_cacounts.mic = p2.mic
left outer join p04 as p3 on rmg_eq_srf_cacounts.isin = p3.isin
                                   and rmg_eq_srf_cacounts.mic = p3.mic
left outer join p04 as p4 on rmg_eq_srf_cacounts.cusip = p4.uscode
                                   and rmg_eq_srf_cacounts.mic = p4.mic
left outer join p04 as p5 on rmg_eq_srf_cacounts.sedol = p5.sedol
                                   and rmg_eq_srf_cacounts.mic = p5.mic
where
p04.id is null
and p2.id is null
and p3.id is null
and p4.id is null
and p5.id is null


select top 1 * from rmg_eq_srf_all

use