-- summary query
use wol
select top 5
'Situation - '+sitlist.sitname,
'Date - '+rtrim(cast(cab.displaydate as varchar(20))),
'Bulletin ID - '+cab.displayid+'/'+rtrim(cast(cab.cabedition as varchar(20))),
'http://www.exchange-data.net/php/cab/full_view.php?cab_id='+rtrim(cast(cab.cabepoch as varchar(20)))+'&cab_count='+rtrim(cast(cab.cabedition as varchar(20)))+'&PSecID='++rtrim(cast(cab.psecid as varchar(20)))+'&n=6011'
from cab
left outer join sitlist on cab.SitCD = sitlist.SitCD
where 
cab.cabstatus='SENT'
and displaydate<getdate()-0.5
order by cab.cabepoch desc

-- cab query
use wol
select top 5
'Web link to bulletin and reated documents -',
'http://www.exchange-data.net/php/cab/full_view.php?cab_id='+rtrim(cast(cab.cabepoch as varchar(20)))+'&cab_count='+rtrim(cast(cab.cabedition as varchar(20)))+'&PSecID='++rtrim(cast(cab.psecid as varchar(20)))+'&n=6011',
'Bulletin ID - '+cab.displayid+'/'+rtrim(cast(cab.cabedition as varchar(20))),
'Situation - '+sitlist.sitname,
'Date - '+rtrim(cast(cab.displaydate as varchar(20))),
'Valid ISA Status names -',
'Q =Qualifying, NQ =Not qualifying, NR =Not relevant,',
'FIR =Further info required, NLQ =No longer qualifying',
'',
'TISA Notes below the line',
'',
'____________________________________________________________________'
from cab
left outer join sitlist on cab.SitCD = sitlist.SitCD
where 
cab.cabstatus='SENT'
and displaydate<getdate()-0.5
order by cab.cabepoch desc


--sec query
use wol
select top 5
sec.isin+' - '+sec.title,
'Markets - '+smf4.dbo.market.mic,
case when TISA.secid is null then 'Current ISA status - N' else 'Current ISA status - '+TISA.Tisaflag end
from cab
left outer join sec on cab.cabepoch = sec.cabepoch
                   and cab.cabedition = sec.cabedition
left outer join smf4.dbo.market on sec.secid = smf4.dbo.market.securityid
left outer join tisa on sec.secid = tisa.secid
where 
cab.cabstatus='SENT'
and displaydate<getdate()-0.5
order by cab.cabepoch desc
