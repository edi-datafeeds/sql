use wca
SELECT exchg.exchgname, Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
where
scexh.actflag<>'D'
and scexh.liststatus<>'D'
and scmst.sectycd<>'BND'
GROUP BY exchg.exchgname order by exchg.exchgname

