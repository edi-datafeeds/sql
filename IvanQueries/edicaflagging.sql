--FilePath=o:\datafeed\smf\125\
--FileName=YYYYMMDD
--FileExtension=.125

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=DD/MM/YYYY
--DataTimeFormat=
--ForceTime

--# 1
use smf4

SELECT 
Issuer.Issuername,
Security.Longdesc,
Security.Isin,
EDICAOption.OptionCurr,
Case when EDICADetail.TaxRateInd = 'N' then rtrim(cast(EDICAOption.CashDistCallRate as varchar(16))) else '' end as NetRate,
Case when EDICADetail.TaxRateInd = 'G' OR EDICADetail.TaxRateInd = '' OR EDICADetail.TaxRateInd IS NULL then rtrim(cast(EDICAOption.CashDistCallRate as varchar(16))) else '' end as GrossRate,
EDICADetail.Exdate,
EDICADetail.Paydate,
EDICAOption.Comment
FROM EDICADetail
INNER JOIN Security ON Security.SecurityID = EDICADetail.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
LEFT OUTER JOIN SECTYPE ON Security.Sectype = SECTYPE.Code
INNER JOIN EDICAOption ON (EDICADetail.Securityid = EDICAOption.SecurityId) 
 AND (EDICADetail.CADID = EDICAOption.CADID)
Where 
(EDICADetail.CAtype ='DD'
or EDICADetail.CAtype ='D2')
and EDICAdetail.Actflag<>'D'
and EDICAoption.Actflag<>'D'
and paydate>'2007/04/06'
and paydate<'2008/04/06'
and upper(sectype.Typegroup)='EQUITY'
and paydate is not null
and EDICAOption.CashDistCallRate <> ''
and EDICAOption.CashDistCallRate is not null
and EDICAOption.OptionCurr is not null
and EDICAOption.OptionCurr <> ''
ORDER BY issuername

use wca
select * from bond
where secid = 64484



use smf4
select unitofqcurrcode from security
where
-- unitofqcurrcode is not null
--and 
statusflag<>'D'

use smf4
update edicaoption
set edicaoption.actflag = 'D'
--select edicaoption.*
from edicadetail
inner join edicaoption on edicadetail.securityid = edicaoption.securityid
         and edicadetail.cadid = edicaoption.cadid
inner join security on edicadetail.securityid = security.securityid
inner join issuer on security.issuerid = issuer.issuerid
where
issuer.cinccode = 'GB'
and edicadetail.ssnyear='WCAD'

use smf4
select *
from edicaoption
inner join security on edicaoption.securityid = security.securityid
inner join issuer on security.issuerid = issuer.issuerid
where
issuer.cinccode = 'GB'
and edicaoption.distsedol='WCAD'
