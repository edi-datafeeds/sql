use wca
select scmst.SecID, scmst.ISIN, PrimaryExchgCD, localcode as PrimaryLocalcode
FROM scmst
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
                        and scexh.exchgcd = primaryexchgcd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid
WHERE 
scmst.actflag<>'D'
and (wca.dbo.scmst.structcd <> 'CEF' or  wca.dbo.scmst.structcd is null)
and scmst.sectycd<>'ETF'
and scmst.sectycd<>'TRT'
and scmst.sectycd<>'DRT'
and sectygrp.secgrpid=1
and (exchg.cntrycd = 'AT'
or exchg.cntrycd = 'BE'
or exchg.cntrycd = 'BG'
or exchg.cntrycd = 'CH'
or exchg.cntrycd = 'CY'
or exchg.cntrycd = 'CZ'
or exchg.cntrycd = 'DE'
or exchg.cntrycd = 'DK'
or exchg.cntrycd = 'EE'
or exchg.cntrycd = 'ES'
or exchg.cntrycd = 'FI'
or exchg.cntrycd = 'FR'
or exchg.cntrycd = 'GB'
or exchg.cntrycd = 'GR'
or exchg.cntrycd = 'HU'
or exchg.cntrycd = 'IE'
or exchg.cntrycd = 'IS'
or exchg.cntrycd = 'IT'
or exchg.cntrycd = 'LT'
or exchg.cntrycd = 'LU'
or exchg.cntrycd = 'LV'
or exchg.cntrycd = 'MT'
or exchg.cntrycd = 'NL'
or exchg.cntrycd = 'NO'
or exchg.cntrycd = 'PL'
or exchg.cntrycd = 'PT'
or exchg.cntrycd = 'RO'
or exchg.cntrycd = 'SE'
or exchg.cntrycd = 'SI'
or exchg.cntrycd = 'SK')
