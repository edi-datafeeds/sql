use portfolio
select col001 from dba
left outer join prices.dbo.liveprices on col001 = isin
where isin is not null



use portfolio
select secid, sectycd
into wmtype
from dbb
left outer join wca.dbo.scmst on col001 = wca.dbo.scmst.isin
where wca.dbo.scmst.isin is not null
union
select secid, sectycd from dba
left outer join wca.dbo.scmst on col001 = wca.dbo.scmst.isin
where wca.dbo.scmst.isin is not null


select sectycd, count(sectycd)
from wmtype
group by sectycd


select * from notwca
where col001 not in
(select col001 from dba
left outer join smf4.dbo.security on col001 = smf4.dbo.security.isin
where smf4.dbo.security.isin is not null
union
select col001 from dbb
left outer join smf4.dbo.security on col001 = smf4.dbo.security.isin
where smf4.dbo.security.isin is not null)