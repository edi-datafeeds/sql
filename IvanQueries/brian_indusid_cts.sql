use wca
drop table exchgcdct
drop table exchgcdct2
drop table exchgcdct3
drop table exchgcdct4
 
use wca
SELECT scexh.exchgcd,
Count(distinct issur.issid) AS exchgcd_ct
into exchgcdct
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
where issur.actflag <> 'D'
and exchg.actflag <> 'D'
and sectycd= 'EQS'
GROUP BY scexh.exchgcd
 
use wca
SELECT scexh.exchgcd,
Count(distinct issur.issid) AS exchgcd_ct
into exchgcdct2
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
where issur.actflag <> 'D'
and exchg.actflag <> 'D'
and sectycd= 'EQS'
and indusid is not null
and indusid<>0
GROUP BY scexh.exchgcd



use wca
SELECT scexh.exchgcd,
Count(distinct issur.issid) AS exchgcd_ct
into exchgcdct3
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
where issur.actflag <> 'D'
and exchg.actflag <> 'D'
and sectycd= 'EQS'
and indusid is not null
and indusid<>0
and primaryexchgcd = scexh.exchgcd
GROUP BY scexh.exchgcd

use wca
SELECT scexh.exchgcd,
Count(distinct issur.issid) AS exchgcd_ct
into exchgcdct4
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
where issur.actflag <> 'D'
and exchg.actflag <> 'D'
and sectycd= 'EQS'
and primaryexchgcd = scexh.exchgcd
and scexh.liststatus<>'D'
GROUP BY scexh.exchgcd

select
exchgcdct.exchgcd,
exchgname,
exchgcdct.exchgcd_ct as total_listed,
exchgcdct2.exchgcd_ct as with_indusid,
exchgcdct4.exchgcd_ct as primary_listed,
exchgcdct3.exchgcd_ct as primary_with_indusid
from exchgcdct
left outer join exchg on exchgcdct.exchgcd = exchg.exchgcd
left outer join exchgcdct2 on exchgcdct.exchgcd = exchgcdct2.exchgcd
left outer join exchgcdct3 on exchgcdct.exchgcd = exchgcdct3.exchgcd
left outer join exchgcdct4 on exchgcdct.exchgcd = exchgcdct4.exchgcd
order by exchgcdct.exchgcd

