-- create flag table
use wca
if exists (select * from sysobjects where name = 'cflagtemp')
 drop table cflagtemp
select bond.secid, bond.callable
into cflagtemp
from bond
where bond.actflag<>'D'

--tidy flags
update cflagtemp
set cflagtemp.callable='X'
from cflagtemp
left outer join cpopt on  cflagtemp.secid = cpopt.secid
where cpopt.secid is not null
and cpopt.actflag<>'D'
and cpopt.callput='C'
and (cflagtemp.callable='' or cflagtemp.callable='N')

--set specified callables
use wca
update cflagtemp
set cflagtemp.callable='C'
from cflagtemp
left outer join cpopt on  cflagtemp.secid = cpopt.secid
left outer join bond on  cflagtemp.secid = bond.secid
where cpopt.secid is not null
and cpopt.actflag<>'D'
and cpopt.callput='C'
and cpopt.mandatoryoptional='O'
and (cpopt.price<>'' or cpopt.cppriceaspercent<>'')
and cpopt.callput='C'


use wca
if exists (select * from sysobjects where name = 'fx_C')
 drop table fx_C
SELECT bond.bondtype, Count(bond.secid) AS FX_C
into fx_C
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = 'C'
and bond.interestbasis = 'FXD'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> ''
and firstcoupondate is not null
and (interestrate is not null or substring(interestrate,1,5)='0.000')
GROUP BY bond.bondtype order by bond.bondtype

use wca
if exists (select * from sysobjects where name = 'fx_N')
 drop table fx_N
SELECT bond.bondtype, Count(bond.secid) AS FX_N
into fx_N
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = 'N'
and bond.interestbasis = 'FXD'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> ''
and firstcoupondate is not null
and (interestrate is not null or substring(interestrate,1,5)='0.000')
GROUP BY bond.bondtype order by bond.bondtype

use wca
if exists (select * from sysobjects where name = 'fx_')
 drop table fx_
SELECT bond.bondtype, Count(bond.secid) AS FX_
into fx_
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = ''
and bond.interestbasis = 'FXD'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> ''
and firstcoupondate is not null
and (interestrate is not null or substring(interestrate,1,5)='0.000')
GROUP BY bond.bondtype order by bond.bondtype




use wca
if exists (select * from sysobjects where name = 'fr_C')
 drop table fr_C
SELECT bond.bondtype, Count(bond.secid) AS FR_C
into fr_C
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = 'C'
and bond.interestbasis = 'FR'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> ''
and firstcoupondate is not null
and frntype <> ''
and frnindexbenchmark <> ''
GROUP BY bond.bondtype order by bond.bondtype


use wca
if exists (select * from sysobjects where name = 'fr_N')
 drop table fr_N
SELECT bond.bondtype, Count(bond.secid) AS FR_N
into fr_N
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = ''
and bond.interestbasis = 'FR'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> ''
and firstcoupondate is not null
and frntype <> ''
and frnindexbenchmark <> ''
GROUP BY bond.bondtype order by bond.bondtype


use wca
if exists (select * from sysobjects where name = 'fr_')
 drop table fr_
SELECT bond.bondtype, Count(bond.secid) AS FR_
into fr_
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = ''
and bond.interestbasis = 'FR'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> ''
and firstcoupondate is not null
and frntype <> ''
and frnindexbenchmark <> ''
GROUP BY bond.bondtype order by bond.bondtype



use wca
if exists (select * from sysobjects where name = 'zc_not_')
 drop table zc_not_
SELECT bond.bondtype, Count(bond.secid) AS ZC_NOT_
into zc_not_
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = ''
and bond.interestbasis = 'ZC'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> 'ITM'
and issueprice = ''
and parvalue = ''
GROUP BY bond.bondtype order by bond.bondtype


use wca
if exists (select * from sysobjects where name = 'zc_not_N')
 drop table zc_
SELECT bond.bondtype, Count(bond.secid) AS ZC_NOT_N
into zc_not_N
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = 'N'
and bond.interestbasis = 'ZC'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> 'ITM'
and issueprice = ''
and parvalue = ''
GROUP BY bond.bondtype order by bond.bondtype

use wca
if exists (select * from sysobjects where name = 'zc_')
 drop table zc_
SELECT bond.bondtype, Count(bond.secid) AS ZC_
into zc_
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = ''
and bond.interestbasis = 'ZC'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> 'ITM'
and issueprice <> ''
and parvalue <> ''
GROUP BY bond.bondtype order by bond.bondtype

use wca
if exists (select * from sysobjects where name = 'zc_N')
 drop table zc_N
SELECT bond.bondtype, Count(bond.secid) AS ZC_N
into zc_N
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = 'N'
and bond.interestbasis = 'ZC'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> 'ITM'
and issueprice <> ''
and parvalue <> ''
GROUP BY bond.bondtype order by bond.bondtype


use wca
if exists (select * from sysobjects where name = 'zc_C')
 drop table zc_
SELECT bond.bondtype, Count(bond.secid) AS ZC_C
into zc_C
FROM cflagtemp
inner join bond on cflagtemp.secid = bond.secid
where
bond.bondtype<>''
and cflagtemp.callable = 'C'
and bond.interestbasis = 'ZC'
and issuedate is not null
and maturitydate is not null
and interestpaymentfrequency <> 'ITM'
and issueprice <> ''
and parvalue <> ''
GROUP BY bond.bondtype order by bond.bondtype



--use wca
--SELECT bond.bondtype, Count(bond.secid) AS CountOf
--FROM bond
--inner join scmst on BOND.secid = scmst.secid
--inner join sandp0314 on scmst.isin = sandp0314.isincode
--where bond.actflag <> 'D'
--GROUP BY bond.bondtype order by bond.bondtype



select fx_N.bondtype, 
zc_C.zc_C, zc_N.zc_N, zc_.zc_, 
zc_not_N.zc_not_n,zc_not_.zc_not_,
fx_C.fx_C, fx_N.fx_N, fx_.fx_, 
fr_C.fr_C, fr_N.fr_N, fr_.fr_  

from fx_N
left outer join fx_ on fx_N.bondtype=fx_.bondtype
left outer join fx_C on fx_N.bondtype=fx_C.bondtype
left outer join fr_ on fx_N.bondtype=fr_.bondtype
left outer join fr_N on fx_N.bondtype=fr_n.bondtype
left outer join fr_C on fx_N.bondtype=fr_C.bondtype
left outer join zc_ on fx_N.bondtype=zc_.bondtype
left outer join zc_N on fx_N.bondtype=zc_n.bondtype
left outer join zc_C on fx_N.bondtype=zc_C.bondtype
left outer join zc_not_ on fx_N.bondtype=zc_not_.bondtype
left outer join zc_not_N on fx_N.bondtype=zc_not_n.bondtype
