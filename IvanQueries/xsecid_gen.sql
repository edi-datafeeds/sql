 drop table xsecid

use wca
select bond.secid
into portfolio.dbo.xsecid
from issur
inner join scmst on issur.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where 
(cntryofincorp='PE'
or cntryofincorp='BR'
or cntryofincorp='CR'
or cntryofincorp='GT'
or cntryofincorp='NG'
or cntryofincorp='KE'
or cntryofincorp='AR'
or cntryofincorp='CO')
and maturitydate between getdate() and getdate()+1500
and bond.actflag<>'D'
and scmst.actflag<>'D'
and bond.announcedate>'2007/01/01'

use portfolio
ALTER TABLE xsecid ALTER COLUMN  secid int NOT NULL

ALTER TABLE [DBO].[xsecid] WITH NOCHECK ADD 
 CONSTRAINT [pk2_xsecid] PRIMARY KEY ([secid])  ON [PRIMARY]

