use wca
select 
col001 as isin,
scmst.sectycd,
sedol.sedol as SEDOL,
issur.issuername as Issuer,
issur.cntryofincorp,
exchg.exchgname as Exchange,
scexh.Localcode,
scmst.sharesoutstanding
from portfolio.dbo.eastcap
left outer join scmst on portfolio.dbo.eastcap.col001 = isin
left outer join issur on scmst.issid = issur.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scexh.secid = sedol.secid
                       and exchg.cntrycd = sedol.cntrycd

--where isin is not null
order by sectycd




