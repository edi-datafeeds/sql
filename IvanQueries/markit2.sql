use wca
select distinct
portfolio.dbo.London.F1,
case 
     when v54f_620_dividend.secid is not null then 'Yes' 
     when isindiv.secid is not null then 'Yes' 
     when uscodediv.secid is not null then 'Yes' 
     when scmst.secid is not null and (scmst.statusflag <> 'I' or scmst.statusflag is null) then 'No'
     when scmstisin.secid is not null and (scmstisin.statusflag <> 'I' or scmstisin.statusflag is null) then 'No'
     when scmstuscode.secid is not null and (scmstuscode.statusflag <> 'I' or scmstuscode.statusflag is null) then 'No'
     when sdchg.secid is not null then 'Def' 
     when icc.secid is not null then 'Def' 
     when iccuscode.secid is not null then 'Def' 
     when scmst.statusflag = 'I' then 'Def' 
     when scmstisin.statusflag = 'I' then 'Def' 
     when scmstuscode.statusflag = 'I' then 'Def' 
     else '' end as result
into mitLondon3
from
portfolio.dbo.London
left outer join sedol on portfolio.dbo.London.sedol = sedol.sedol
                     and portfolio.dbo.London.sedol is not null
left outer join sdchg on portfolio.dbo.London.sedol = sdchg.oldsedol
left outer join scmst on sedol.secid = scmst.secid
left outer join v54f_620_dividend on scmst.secid = v54f_620_dividend.secid
--left outer join smf4.dbo.security on portfolio.dbo.London.sedol = smf4.dbo.security.sedol
left outer join scmst as scmstisin on portfolio.dbo.London.isin = scmstisin.isin
                           and portfolio.dbo.London.isin is not null
left outer join icc on portfolio.dbo.London.isin = icc.oldisin
                     and portfolio.dbo.London.isin is not null
left outer join icc as iccuscode on substring(portfolio.dbo.London.cusip,1,9) = iccuscode.olduscode
                     and portfolio.dbo.London.cusip is not null
left outer join v54f_620_dividend as isindiv on scmstisin.secid = isindiv.secid
left outer join scmst as scmstuscode on substring(portfolio.dbo.London.cusip,1,9) = scmstuscode.uscode
                           and portfolio.dbo.London.cusip is not null
left outer join v54f_620_dividend as uscodediv on scmstuscode.secid = uscodediv.secid


select count(*) from mitLondon3
where result = 'Yes'
order by f1


where result = 'Yes'

SELECT     f1
FROM         mitLondon
GROUP BY F1
HAVING COUNT(F1) > 1



use wca
select distinct
portfolio.dbo.London.F1,
--scmst.secid,
--sdchg.secid as sdchgsecid,
--case when scmst.statusflag = '' then 'A' ELSE scmst.statusflag end as statusflag,
--smf4.dbo.security.statusflag,
--v54f_620_dividend.secid as divsecid,
case 
     when v54f_620_dividend.secid is not null then 'Yes' 
     when isindiv.secid is not null then 'Yes' 
     when scmst.secid is not null and (scmst.statusflag <> 'I' or scmst.statusflag is null) then 'No'
     when scmstisin.secid is not null and (scmstisin.statusflag <> 'I' or scmstisin.statusflag is null) then 'No'
     when scmstuscode.secid is not null and (scmstuscode.statusflag <> 'I' or scmstuscode.statusflag is null) then 'No'
     when sdchg.secid is not null then 'Def' 
     when icc.secid is not null then 'Def' 
     when iccuscode.secid is not null then 'Def' 
     when scmst.statusflag = 'I' then 'Def' 
     when scmstisin.statusflag = 'I' then 'Def' 
     when scmstuscode.statusflag = 'I' then 'Def' 
     else '' end as result
--into mitLondon
from
portfolio.dbo.London
left outer join sedol on portfolio.dbo.London.sedol = sedol.sedol
                     and portfolio.dbo.London.sedol is not null
left outer join sdchg on portfolio.dbo.London.sedol = sdchg.oldsedol
left outer join scmst on sedol.secid = scmst.secid
left outer join v54f_620_dividend on scmst.secid = v54f_620_dividend.secid
--left outer join smf4.dbo.security on portfolio.dbo.London.sedol = smf4.dbo.security.sedol
left outer join scmst as scmstisin on portfolio.dbo.London.isin = scmstisin.isin
                           and portfolio.dbo.London.isin is not null
left outer join icc on portfolio.dbo.London.isin = icc.oldisin
                     and portfolio.dbo.London.isin is not null
left outer join icc as iccuscode on substring(portfolio.dbo.London.cusip,1,9) = iccuscode.olduscode
                     and portfolio.dbo.London.cusip is not null
left outer join v54f_620_dividend as isindiv on scmstisin.secid = isindiv.secid
left outer join scmst as scmstuscode on substring(portfolio.dbo.London.cusip,1,9) = scmstuscode.uscode
                           and portfolio.dbo.London.cusip is not null
left outer join v54f_620_dividend as uscodediv on scmstuscode.secid = uscodediv.secid


select * from mitLondon4
order by f1

SELECT     f1
FROM         mitLondon4
GROUP BY F1
HAVING COUNT(F1) > 1


