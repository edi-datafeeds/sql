use wca
select distinct
bond.SecID,
issuername,
securitydesc,
isin,
RegistrarName,
agncy.CntryCD as Agency_Country,
scagy.Relationship
from
scmst
inner join bond on scmst.secid = bond.secid
inner join issur on scmst.issid = issur.issid
inner join scagy on bond.secid = scagy.secid
inner join agncy on scagy.agncyid = agncy.agncyid
where maturitydate>getdate()
and scmst.statusflag<>'I'
and (relationship = 'BM'
or relationship = 'CAL'
or relationship = 'MGR'
or relationship = 'PA')



