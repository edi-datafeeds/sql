use wca
select distinct
issur.issid,
issuername,
indusid,
website
from issur
left outer join isscn on issur.issid = isscn.issid
inner join scmst on issur.issid = scmst.issid
inner join scexh on scmst.secid = scexh.secid
where substring(exchgcd,1,2) = 'IE'
and liststatus<>'D'
