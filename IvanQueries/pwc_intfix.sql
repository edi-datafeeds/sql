use wca
delete from infixpwc
go


use wca
declare @mycount as integer
set @mycount=-cast(getdate()-'2008/09/01' as int)

WHILE @mycount<30
BEGIN

use wca

insert into infixpwc

select
upper('INTALERT') as Event_Name,
bond.secid,
scmst.isin,
getdate()+@mycount as Payable_Date,
IssueDate as Issue_Date,
MaturityDate as Maturity_Date,
IntCommencementdate as Interest_Commencement_Date,
FirstCouponDate as First_Coupon_Date,
case when bond.ParValue<>'' then bond.ParValue else scmst.Parvalue end as Par_Value,
case when bond.interestcurrency <>'' then bond.interestcurrency else bond.curencd end as Currency,
bond.interestrate as Interest_Rate,
bond.interestpaymentfrequency as Frequency,
bond.interestaccrualconvention as Accrual_Convention,
bond.InterestBasis as Interest_Basis,
bond.intbusdayconv as Interest_Bus_Day_Convention
from bond
inner join scmst on bond.secid = scmst.secid

where
bond.secid in (select portfolio.dbo.pwc_secid.secid from portfolio.dbo.pwc_secid)
and bond.actflag<>'D'
and scmst.actflag<>'D'
and (
convert(varchar(4),year(getdate()))+substring(Interestpaydate1,3,2)+substring(Interestpaydate1,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
convert(varchar(4),year(getdate()))+substring(Interestpaydate2,3,2)+substring(Interestpaydate2,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
convert(varchar(4),year(getdate()))+substring(Interestpaydate3,3,2)+substring(Interestpaydate3,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
convert(varchar(4),year(getdate()))+substring(Interestpaydate4,3,2)+substring(Interestpaydate4,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
firstcoupondate=convert(varchar(30),getdate()+@mycount, 112)
)

and bond.interestrate<>''
and bond.curencd<>''
and (bond.maturitydate>getdate()+@mycount or (bond.maturitydate is null and perpetual<>''))
and (bond.firstcoupondate<=convert(varchar(30),getdate()+@mycount, 112) or bond.firstcoupondate is null)
and (bond.intcommencementdate<=convert(varchar(30),getdate()+@mycount, 112) or bond.intcommencementdate is null)
and (bond.issuedate<=convert(varchar(30),getdate()+@mycount, 112) or bond.issuedate is null)

   set @mycount = @mycount+1
END

go

/*
select * from infixpwc
--where interestcurrency<>currency
order by payable_date
select getdate()-407
*/

--delete from infixpwc
--where pay_date < '2008/09/01'

--select scmst.parvalue from infixpwc
--inner join scmst on infixpwc.secid = scmst.secid
--where infixpwc.parvalue = ''


use wca
select * from icc
inner join portfolio.dbo.wfiport on portfolio.dbo.wfiport.secid = icc.secid



use portfolio
select * from fisin
where code = 'INE338I01019'

use portfolio
update fisin
set actflag = 'U'
where accid = 186