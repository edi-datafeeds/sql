--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_WFIPORT_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select
'INTPY' as TableName,
CASE WHEN INT.Actflag = 'D' or INT.actflag='C' or intpy.actflag is null THEN INT.Actflag ELSE INTPY.Actflag END as Actflag,
INT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > INT.Acttime) and (RD.Acttime > EXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > INT.Acttime) THEN EXDT.Acttime ELSE INT.Acttime END as [Changed],
INT.RdID,
SCMST.SecID,
SCMST.ISIN,
INTPY.BCurenCD as DebtCurrency,
INTPY.BParValue as NominalValue,
SCEXH.ExchgCD,
RD.Recdate,
EXDT.Exdate,
EXDT.Paydate,
INT.InterestFromDate,
INT.InterestToDate,
INT.Days,
case when indef.defaulttype<>'' then 'F' else '' end as InterestDefault,
INTPY.CurenCD as Interest_Currency,
INTPY.IntRate as Interest_Rate,
INTPY.GrossInterest,
INTPY.NetInterest,
INTPY.DomesticTaxRate,
INTPY.NonResidentTaxRate,
INTPY.RescindInterest,
INTPY.AgencyFees,
INTPY.CouponNo,
null as CouponID,
INTPY.DefaultOpt,
INTPY.OptElectionDate,
INTPY.AnlCoupRate,
[INT].IntNotes as Notes
from [INT]
INNER JOIN RD ON INT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON BOND.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID 
     AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
     AND EXDT.Actflag<>'D'
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID
left outer join indef on scmst.secid = indef.secid and exdt.paydate = indef.defaultdate and 'INTDEFA' = indef.defaulttype
where
(
BOND.secid in (select portfolio.dbo.wfiport.secid from portfolio.dbo.wfiport)
and (INT.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or EXDT.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or INTPY.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and scexh.liststatus<>'D'
)
OR
(
BOND.secid in (select portfolio.dbo.wfihist.secid from portfolio.dbo.wfihist)
and (paydate>='2004/01/01' or (paydate is null and INT.AnnounceDate>='2004/01/01'))
and INT.actflag<>'D'
and INTPY.actflag<>'D'
)
