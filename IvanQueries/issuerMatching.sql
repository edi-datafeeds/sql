update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = null



use wca
select company, issuername from portfolio.dbo.[foreign]
left outer join issur on substring(company,1,12) = substring(issuername,1,12)


update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join issur on replace(company,' ','') = replace(issuername,' ','')
where issur.issid is not null
and portfolio.dbo.[foreign].issid is null

update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join issur on replace(replace(company,'.',''),' ','')
                       = replace(replace(issuername,'.',''),' ','')
where issur.issid is not null
and portfolio.dbo.[foreign].issid is null

update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join issur on replace(replace(replace(company,'.',''),',',''),' ','') 
                       = replace(replace(replace(issuername,'.',''),',',''),' ','')
where issur.issid is not null
and portfolio.dbo.[foreign].issid is null

update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join issur on replace(replace(replace(replace(company,'.',''),',',''),' plc',' '),' ','')
                       = replace(replace(replace(replace(issuername,'.',''),',',''),' plc',' '),' ','')
where issur.issid is not null
and portfolio.dbo.[foreign].issid is null

update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(company,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'incorporated',' '),' inc ',' '),' ','') 
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'incorporated',' '),' inc ',' '),' ','')
where issur.issid is not null
and portfolio.dbo.[foreign].issid is null

update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(company,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),' ','') 
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),' ','') 
where issur.issid is not null
and portfolio.dbo.[foreign].issid is null



update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join scexh on symbol = localcode and exchgcd='USPINK'
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
where
portfolio.dbo.[foreign].issid is null
and issuername is not null




update portfolio.dbo.[foreign]
set portfolio.dbo.[foreign].issid = issur.issid, portfolio.dbo.[foreign].ediissuer = issur.issuername
from portfolio.dbo.[foreign]
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(company,'.',' '),',',' '),'limited',' '),'holding',' '),' ltd ',' '),'PT ',' '),' and ',' '),' corp ',' '),' co ',' '),'&',' '),'ASA',' '),'SA',' '),'(the)',' '),' ','')
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holding',' '),' ltd ',' '),' hldg ',' '),' and ',' '),' corp ',' '),' co ',' '),'&',' '),' PT',' '),'TBK',' '),' ','') 
where issur.issid is not null
and portfolio.dbo.[foreign].issid is null



select * from portfolio.dbo.[foreign]
where portfolio.dbo.[foreign].issid is null