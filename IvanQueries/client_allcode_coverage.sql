use wca
select portfolio.dbo.cefd1.*, 
sectycd,
case when statusflag ='' then 'Active' when statusflag is null then null else 'Inactive' end as Status,
case when icc.secid is not null then 'Yes' when scmst.secid is null then null else 'No' end as FoundOldWcaCode
from portfolio.dbo.cefd1
left outer join scmst on security = scmst.isin
left outer join icc on security = icc.oldisin
where 
--(scmst.secid is not null
--or icc.secid is not null)
--and 
type = 'isin'

union

select portfolio.dbo.cefd1.*, 
sectycd,
case when statusflag ='' then 'Active' when statusflag is null then null else 'Inactive' end as Status,
case when icc.secid is not null then 'Yes' when scmst.secid is null then null else 'No' end as FoundOldWcaCode
from portfolio.dbo.cefd1
left outer join scmst on security = scmst.uscode
left outer join icc on security = icc.olduscode
where 
--(scmst.secid is not null
--or icc.secid is not null)
--and 
type = 'cusip'

union

select portfolio.dbo.cefd1.*, 
sectycd,
case when statusflag ='' then 'Active' when statusflag is null then null else 'Inactive' end as Status,
case when sdchg.secid is not null then 'Yes' when scmst.secid is null then null else 'No' end as FoundOldWcaCode
from portfolio.dbo.cefd1
left outer join sedol on security=sedol
left outer join scmst on sedol.secid = scmst.secid
left outer join sdchg on  portfolio.dbo.cefd1.security = sdchg.oldsedol
where 
(scmst.secid is not null
or sdchg.secid is not null)
and 
type = 'sedol'

union

select portfolio.dbo.cefd1.*, 
sectype as sectycd, 
smf4.dbo.security.statusflag as Status,
null as FoundOldWcacode
from portfolio.dbo.cefd1
left outer join sedol on security=sedol
left outer join scmst on sedol.secid = scmst.secid
left outer join sdchg on security = sdchg.oldsedol
left outer join smf4.dbo.security on security = smf4.dbo.security.sedol
where 
(scmst.secid is null and sdchg.secid is null)
and type = 'sedol'
order by sectycd, type, foundoldwcacode
