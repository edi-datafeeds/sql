USE WCA
select
statusflag,
BOND.SecID,
ISIN,
IssuerName,
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN ISSUR.Isstype='GOV' THEN 'G' WHEN ISSUR.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype,
SecurityDesc,
maturitydate,
SectyCD,
ExchgCD,
BondSrc
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
where
(substring(EXCHGCD,1,2)='JP')
and scmst.actflag<>'D'
and BOND.actflag<>'D'
order by DEBTTYPE, sectycd, issuername