delete portfolio.dbo.mkrep

use wca
insert into portfolio.dbo.mkrep
select distinct top 50 secid from cpopt
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 50 secid from redem
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 20 secid from conv
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 50 secid from frnfx
where secid not in (select secid from portfolio.dbo.mkrep)
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 50 secid from bond
where secid not in (select secid from portfolio.dbo.mkrep)
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 20 secid from bschg
where secid not in (select secid from portfolio.dbo.mkrep)
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 20 secid from rdnom
where secid not in (select secid from portfolio.dbo.mkrep)
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 20 secid from irchg
where secid not in (select secid from portfolio.dbo.mkrep)
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 20 secid from ifchg
where secid not in (select secid from portfolio.dbo.mkrep)
order by secid desc

use wca
insert into portfolio.dbo.mkrep
select distinct top 20 secid from bochg
where secid not in (select secid from portfolio.dbo.mkrep)
order by secid desc
