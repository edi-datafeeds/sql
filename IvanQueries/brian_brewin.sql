use wca
select distinct col001,
Issuername,
CntryofIncorp,
case when statusflag = 'I' then 'Inactive' else 'Active' end as SecStatus,
Securitydesc,
rcntrycd as RegCountry,
SectyCD
from portfolio.dbo.brewinsedol
left outer join sedol on col001 = sedol.sedol
left outer join scmst on sedol.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join smf4.dbo.security on col001 = smf4.dbo.security.sedol
where sedol.sedol is not null

 and oldsedol is not null
 and smf4.dbo.security.sedol is null



use wca
select * from portfolio.dbo.brewinsedol
left outer join sedol on col001 = sedol
left outer join sdchg on col001 = oldsedol
where sedol is null and oldsedol is null



use wca
select distinct col001,
smf4.dbo.security.shortdesc,
statusflag,
cregcode as RegCountry,
smf4.dbo.sectype.name,
smf4.dbo.sectype.Typegroup
from portfolio.dbo.brewinsedol
left outer join sedol on col001 = sedol.sedol
left outer join smf4.dbo.security on col001 = smf4.dbo.security.sedol
left outer join smf4.dbo.sectype on smf4.dbo.security.sectype = smf4.dbo.sectype.code
where sedol.sedol is null
 and smf4.dbo.security.sedol is not null

