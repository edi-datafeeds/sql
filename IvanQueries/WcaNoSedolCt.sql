use wca
SELECT cntry.country, Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on exchg.cntrycd = cntry.cntrycd
left outer join sedol on exchg.cntrycd = sedol.rcntrycd
                       and scexh.secid = sedol.secid
where
sedol.secid is null
and scexh.actflag<>'D'
and scexh.liststatus<>'D'
and sectygrp.secgrpid<3
GROUP BY cntry.country order by cntry.country
