--filepath=o:\Datafeed\smf\113\
--filenameprefix=
--filename=YYYYMMDD
--filenamealt=
--fileextension=.113
--suffix=
--fileheaderTEXT=EDI_SMF_113_
--fileheaderdate=YYYYMMDD
--datadateformat=yyyy-mm-dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\DIS\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefootertext=EDI_ENDOFFILE


--# 1
use smf4

SELECT
Security.SecurityID,
Dailyconf.tempActflag AS Actflag,
Issuer.Issuername,
Issuer.Cinccode,
udr.dbo.udrnew.DRtype as typeofDR,
Security.Cficode,
substring(Security.OPOL,2,3) as Infosource,
substring(Security.sectype,1,1) as Typeflag,
Security.Isin,
Security.IssuerID,
Security.Tidisplay,
Security.Longdesc,
Security.Statusflag,
Security.Sedol,
Security.Unitofq,
Security.UnitofqCurrcode as Currcode,
Security.Formflag
FROM Dailyconf
INNER JOIN Security ON Security.SecurityID = Dailyconf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
LEFT OUTER JOIN Udr.dbo.Udrnew ON Security.Sedol = udr.dbo.udrnew.drSedol
where Security.confirmation = 'C'
ORDER BY Security.Sedol





























print ""
go
print "STEP-1 delete table ahead"
go
print ""
go
use smf4
if exists (select * from sysobjects 
where name = 'tempUKCAflat')
 drop table tempUKCAflat
go

print ""
go
print "STEP-2 Generate next WCA UK Dividend batch"
go
print ""
go
use wca
select distinct *
--into smf4.dbo.tempUKCAflat
from v54f_701_dividend
where PAYDATE >= '2006/04/06'


use wca
select distinct eventid
from v54f_701_dividend
where PAYDATE >= '2005/04/06'


go

print ""
go
print "STEP-3 delete all unconfirmed EDICAOPTION records"
go
print ""
go
use smf4
delete from edicaoption
where exists 
(select
tempUKCAflat.eventid
from tempUKCAflat
where edicaoption.CADID = tempUKCAflat.eventid
and edicaoption.Payrate = 'U'
and edicaoption.distsedol='WUT')
go



print ""
go
print "STEP-4 delete all unconfirmed EDICADETAIL records"
go
print ""
go
use smf4
delete from edicadetail
where exists 
(select
tempUKCAflat.eventid as CADID
from tempUKCAflat
where edicadetail.CADID = tempUKCAflat.eventid
and edicadetail.Confirmation='U'
and edicadetail.SsnYear='WUT')
go

print ""
go
print "STEP-5 insert EDICADETAIL records where key not found"
go
print ""
go
use smf4
insert into edicadetail
(Actflag, Actdate, CADID, SecurityID, Closedate,
Exdate, PayDate, UKTaxrate, TaxRateInd,
Confirmation, credate, ssnyear, CAtype)
select
substring(tempUKCAflat.Action,1,1) as Actflag,
changed,
tempUKCAflat.eventid,
security.securityID,
--tempUKCAflat.OptElectionDate,
tempUKCAflat.RecDate as Closedate,
tempUKCAflat.ExDate,
tempUKCAflat.PayDate,
--'' as BusYrTo,
tempUKCAflat.Taxrate,
'G' as TaxRateInd,
'U' as Confirmation,
tempUKCAflat.Created,
'WUT' as ssnyear,
'DT' as CAtype
from tempUKCAflat
inner join security on tempUKCAflat.sedol = security.sedol
where
not exists
(select edicadetail.securityID from edicadetail
where edicadetail.securityID = security.securityID
and edicadetail.CADID = tempUKCAflat.eventid)

go

print ""
go
print "STEP-6 insert EDICAOPTON records where key not found"
go
print ""
go
use smf4
insert into smf4.dbo.edicaoption
(Actflag, Actdate, CADID, SecurityID, CashDistCallRate,
OptionCurr, credate, distsedol, numerat, denomin, payrate)
select 
substring(tempUKCAflat.Action,1,1) as Actflag,
changed,
tempUKCAflat.eventid,
security.securityID,
tempUKCAflat.GrossDividend,
tempUKCAflat.CurenCD,
tempUKCAflat.Created,
'WUT' as distsedol,
tempUKCAflat.Group2GrossDiv as numerat,
tempUKCAflat.Equalisation as denomin,
'U' as payrate
from tempUKCAflat
inner join smf4.dbo.security on tempUKCAflat.sedol = security.sedol
where
not exists
(select edicaoption.cadid from edicaoption
where edicaoption.securityID = security.securityID
and edicaoption.CADID = tempUKCAflat.eventid)
go



