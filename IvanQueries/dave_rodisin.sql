use wca
select distinct
col001,
IssuerName,
SecurityDesc,
IndusID,
bond.curencd as DebtCurrency,
MaturityDate,
interestbasis as InterestType,
InterestRate,
InterestPaymentFrequency,
InterestAccrualConvention,
Callable,
Puttable,
case when cpopt.secid is null then '' else 'Yes' end as Schedule
from portfolio.dbo.rodisin
left outer join scmst on col001 = isin
left outer join issur on scmst.issid = issur.issid
left outer join bond on scmst.secid = bond.secid
left outer join cpopt on bond.secid = cpopt.secid
order by issuername

