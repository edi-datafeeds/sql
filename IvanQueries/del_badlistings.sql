use wca
select scexh.*
into otc_scexh
from scexh
left outer join ipo on scexh.secid = ipo.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where ipo.secid is not null and exchg.cntrycd='US' and scexh.listdate is null and ipo.ipostatus <> 'history'

delete scexh
where
scexh.scexhid in (select scexhid from otc_scexh)

