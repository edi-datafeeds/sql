use portfolio
select col001, sectycd, statusflag from innovest
left outer join wca.dbo.scmst on col001 = uscode
where secid is not null
order by statusflag, sectycd, col001

use portfolio
select col001, isin from innovest
left outer join wca.dbo.scmst on col001 = uscode
where secid is not null
order by col001