--filepath=o:\datafeed\debt\report\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_FI_REPORT_pwc548
--fileheadertext=EDI_FI_PF_REPORT_pwc548
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldsepaator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
select
'ISIN IS NOT FOUND IN WCA'

--# 
use wca
select col001 as Portfolio_ISIN
from portfolio.dbo.pwc548
left outer join scmst on col001  = isin
where
scmst.secid is null

--# 
select
'ISIN IS AN OLD ISIN IN WCA'
--# 
use wca
select 
* from portfolio.dbo.pwc548
left outer join scmst on col001  = isin
left outer join icc on scmst.secid = icc.oldisin
where
isin is null and oldisin is not null

--# 
select
'NO INTEREST PAYMENT OR DIVIDEND INFORMATION OF ANY KIND'
--# 
use wca
select distinct col001 as Portfolio_ISIN, issuedate, maturitydate, 
issuername, securitydesc from portfolio.dbo.pwc548
inner join scmst on col001  = isin
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join rd on scmst.secid = rd.secid
left outer join int on rd.rdid = int.rdid
left outer join DIV on rd.rdid = div.rdid
left outer join DIVPY on div.divid = divpy.divid
where
interestpaydate1='' and int.rdid is null
and DIV.rdid is null


--# 
select
'PREFERRED AND/OR PREFERENCE DIVIDEND INFORMATION FOUND'
--# 
use wca
select distinct col001 as Portfolio_ISIN,
Frequency, Marker, rd.recdate, divpy.curencd as Currency, divpy.grossdividend, divpy.netdividend, divpy.ratioold, divpy.rationew,
issuedate, maturitydate, 
issuername, securitydesc from portfolio.dbo.pwc548
inner join scmst on col001  = isin
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join rd on scmst.secid = rd.secid
inner join DIV on rd.rdid = div.rdid
left outer join DIVPY on div.divid = divpy.divid
where recdate>'2008/09/01'


--# 
select
'ISIN IS NOT A FIXED INCOME ASSET - ANY EXISTING DIVIDENDS ARE JOINED'
--# 
use wca
select distinct col001 as Portfolio_ISIN,
Frequency, Marker, rd.recdate, divpy.curencd as Currency, divpy.grossdividend, divpy.netdividend, divpy.ratioold, divpy.rationew,
scmst.sectycd, issuername, securitydesc from portfolio.dbo.pwc548
inner join scmst on col001  = isin
inner join issur on scmst.issid = issur.issid
left outer join bond on scmst.secid = bond.secid
left outer join rd on scmst.secid = rd.secid
left outer join DIV on rd.rdid = div.rdid
left outer join DIVPY on div.divid = divpy.divid
where
scmst.sectycd <> 'BND'
and recdate>'2008/09/01'

--# 
select
'INACTIVE ISIN LIST - NOTE THAT THESE ARE NOT EXCLUDED FROM INTEREST REPORTS'
--# 
use wca
select col001 as Portfolio_ISIN, maturitydate, issuername, securitydesc from portfolio.dbo.pwc548
inner join scmst on col001  = isin
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
where
bond.maturitydate<getdate()-1
or scmst.statusflag='I'


--# 
select
'MARKET INTEREST ANNOUNCEMENTS BUT NO FIXED INTEREST DATES FOUND'
--# 
use wca
select col001 as Portfolio_ISIN, issuedate, maturitydate, 
InterestFromdate,
InterestTodate,
Days,
InterestDefault,
issuername, securitydesc from portfolio.dbo.pwc548
inner join scmst on col001  = isin
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
inner join rd on scmst.secid = rd.secid
inner join int on rd.rdid = int.rdid
where
interestpaydate1=''

--# 
select
'BOTH FIXED INTEREST DATES AND MARKET ANNOUNCEMENTS FOUND'
--# 
use wca
select DISTINCT col001 as Portfolio_ISIN, issuedate, maturitydate, 
interestpaydate1, 
interestpaydate2, 
interestpaydate3, 
interestpaydate4,
InterestFromdate,
InterestTodate,
issuername, securitydesc from portfolio.dbo.pwc548
inner join scmst on col001  = isin
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
inner join rd on scmst.secid = rd.secid
inner join int on rd.rdid = int.rdid
where
interestpaydate1<>''


--# 
select
'FIXED INTEREST DATES AND NO MARKET ANNOUNCEMENTS'
--# 
use wca
select distinct col001 as Portfolio_ISIN, issuedate, maturitydate, 
interestpaydate1, 
interestpaydate2, 
interestpaydate3, 
interestpaydate4, 
issuername, securitydesc from portfolio.dbo.pwc548
inner join scmst on col001  = isin
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join rd on scmst.secid = rd.secid
left outer join int on rd.rdid = int.rdid
where
interestpaydate1<>'' and int.rdid is null


select * from portfolio.dbo.pwc548

