use portfolio
select 
cty, Country, [Proper Name], Sedol, msci1.ISIN, [Old Nos], wca.dbo.scmst.sharesoutstanding as [New Nos], Events
from msci1
left outer join wca.dbo.scmst on msci1.ISIN = wca.dbo.scmst.isin
order by id


use portfolio
select 
--cty, Country, [Proper Name], Sedol, msci1.ISIN, wca.dbo.shoch.oldsos as [Old Nos], 
-- case when wca.dbo.shoch.newsos is not null then wca.dbo.shoch.newsos else wca.dbo.scmst.sharesoutstanding end as [New Nos], Events,
case when wca.dbo.scmst.secid is not null then 'found' else '' end as found
from msci1
left outer join wca.dbo.scmst on msci1.ISIN = wca.dbo.scmst.isin
left outer join wca.dbo.shochseq on wca.dbo.scmst.secid = wca.dbo.shochseq.secid
                           and seqnum = 1
left outer join wca.dbo.shoch on wca.dbo.shochseq.shochid = wca.dbo.shoch.shochid
order by id

