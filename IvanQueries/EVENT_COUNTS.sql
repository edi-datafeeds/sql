SELECT 'Announcement', COUNT(distinct EVENTID) FROM v50f_620_Announcement where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Bankruptcy', COUNT(distinct EVENTID) FROM v50f_620_Bankruptcy  where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Class_Action', COUNT(distinct EVENTID) FROM v50f_620_Class_Action where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Company_Meeting', COUNT(distinct EVENTID) FROM v50f_620_Company_Meeting  where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Financial_Year_Change', COUNT(distinct EVENTID) FROM v50f_620_Financial_Year_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Incorporation_Change', COUNT(distinct EVENTID) FROM v50f_620_Incorporation_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Issuer_Name_change', COUNT(distinct EVENTID) FROM v50f_620_Issuer_Name_change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Liquidation', COUNT(distinct EVENTID) FROM v50f_620_Liquidation where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Certificate_Exchange', COUNT(distinct EVENTID) FROM v51f_620_Certificate_Exchange where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Conversion_Terms', COUNT(distinct EVENTID) FROM v51f_620_Conversion_Terms where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Conversion_Terms_Change', COUNT(distinct EVENTID) FROM v51f_620_Conversion_Terms_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Currency_Redenomination', COUNT(distinct EVENTID) FROM v51f_620_Currency_Redenomination  where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'International_Code_Change', COUNT(distinct EVENTID) FROM v51f_620_International_Code_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Parvalue_Redenomination', COUNT(distinct EVENTID) FROM v51f_620_Parvalue_Redenomination  where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Redemption_Terms', COUNT(distinct EVENTID) FROM v51f_620_Redemption_Terms where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Security_Description_Change', COUNT(distinct EVENTID) FROM v51f_620_Security_Description_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Security_Reclassification', COUNT(distinct EVENTID) FROM v51f_620_Security_Reclassification where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Assimilation', COUNT(distinct EVENTID) FROM v52f_620_Assimilation where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Listing_Status_Change', COUNT(distinct EVENTID) FROM v52f_620_Listing_Status_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Local_Code_Change', COUNT(distinct EVENTID) FROM v52f_620_Local_Code_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Lot_Change', COUNT(distinct EVENTID) FROM v52f_620_Lot_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'New_Listing', COUNT(distinct EVENTID) FROM v52f_620_New_Listing where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Sedol_Change', COUNT(distinct EVENTID) FROM v52f_620_Sedol_Change where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Buy_Back', COUNT(distinct EVENTID) FROM v53f_620_Buy_Back where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Call', COUNT(distinct EVENTID) FROM v53f_620_Call where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Capital_Reduction', COUNT(distinct EVENTID) FROM v53f_620_Capital_Reduction where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Return_of_Capital', COUNT(distinct EVENTID) FROM v53f_620_Return_of_Capital  where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Takeover', COUNT(distinct EVENTID) FROM v53f_620_Takeover where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Arrangement', COUNT(distinct EVENTID) FROM v54f_620_Arrangement where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Bonus', COUNT(distinct EVENTID) FROM v54f_620_Bonus where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Bonus_Rights', COUNT(distinct EVENTID) FROM v54f_620_Bonus_Rights where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Consolidation', COUNT(distinct EVENTID) FROM v54f_620_Consolidation where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Demerger', COUNT(distinct EVENTID) FROM v54f_620_Demerger where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Distribution', COUNT(distinct EVENTID) FROM v54f_620_Distribution where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Divestment', COUNT(distinct EVENTID) FROM v54f_620_Divestment where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Dividend', COUNT(distinct EVENTID) FROM v54f_620_Dividend where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Dividend_Reinvestment_Plan', COUNT(distinct EVENTID) FROM v54f_620_Dividend_Reinvestment_Plan where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Entitlement', COUNT(distinct EVENTID) FROM v54f_620_Entitlement where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Franking', COUNT(distinct EVENTID) FROM v54f_620_Franking where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Merger', COUNT(distinct EVENTID) FROM v54f_620_Merger where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Preferential_Offer', COUNT(distinct EVENTID) FROM v54f_620_Preferential_Offer where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Purchase_Offer', COUNT(distinct EVENTID) FROM v54f_620_Purchase_Offer where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Rights', COUNT(distinct EVENTID) FROM v54f_620_Rights  where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Security_Swap', COUNT(distinct EVENTID) FROM v54f_620_Security_Swap  where created BETWEEN '2008/10/01' AND '2009/10/01'
union
SELECT 'Subdivision', COUNT(distinct EVENTID) FROM v54f_620_Subdivision where created BETWEEN '2008/10/01' AND '2009/10/01'
