use wca
select 
bond.secid, isin, issuername, securitydesc, bondsrc
from portfolio.dbo.xissid
inner join scmst on portfolio.dbo.xissid.issid = scmst.issid
inner join issur on portfolio.dbo.xissid.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join scagy on scmst.secid = scagy.secid
where 
scagy.secid is null 
and bond.actflag<>'D'
and (maturitydate>getdate()-1 or maturitydate is null)
and scmst.actflag<>'D'
and (scmst.statusflag<>'I' or scmst.statusflag is null)

--and bond.acttime>'2008/08/16'



