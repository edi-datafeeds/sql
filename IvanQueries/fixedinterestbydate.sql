use wca
select 
'INTFIX' as sEvent,
v20c_920_BOND.IssID,
v20c_920_BOND.SecID,
v20c_920_BOND.CntryofIncorp,
v20c_920_BOND.IssuerName,
v20c_920_BOND.SecurityDesc,
v20c_920_BOND.ParValue,
v20c_920_BOND.CurenCD,
v20c_920_BOND.ISIN,
v20c_920_BOND.USCode,
v20c_920_BOND.CommonCode,
v20c_920_BOND.SecurityStatus,
v20c_920_BOND.PrimaryExchgCD,
Sedol.defunct,
Sedol.Sedol,
Sedol.CntryCD,
Sedol.RCntryCD,
v20c_920_BOND.BondType,
v20c_920_BOND.NominalValue,
v20c_920_BOND.MaturityDate,
v20c_920_BOND.MaturityExtendible,
v20c_920_BOND.IssueCurrency,
v20c_920_BOND.IssueDate,
v20c_920_BOND.IntPaydate1,
v20c_920_BOND.IntPaydate2,
v20c_920_BOND.IntPaydate3,
v20c_920_BOND.IntPaydate4,
v20c_920_BOND.IntBasis,
v20c_920_BOND.IntRate,
v20c_920_BOND.IntAccrualConvention,
v20c_920_BOND.IntPaymentFrequency,
v20c_920_BOND.IntCommencementDate,
v20c_920_BOND.Perpetual,
v20c_920_BOND.IntBusDayConv,
v20c_920_BOND.ConventionMethod,
v20c_920_BOND.FrnType,
v20c_920_BOND.FrnIndexBenchmark,
v20c_920_BOND.FrnMargin,
v20c_920_BOND.FrnMinIntRate,
v20c_920_BOND.FrnMaxIntRate,
v20c_920_BOND.FrnRounding,
--v20c_920_BOND.NewFrnIntAdjFreq,
v20c_920_BOND.InterestCurrency,
getdate()+28 as paydate

from v20c_920_BOND
left outer join sedol on v20c_920_BOND.SecID = Sedol.SecID
                           and sedol.defunct = 'F'
where 
intrate<>''
and (
convert(varchar(4),year(getdate()))+substring(Intpaydate1,3,2)+substring(Intpaydate1,1,2)=
 convert(varchar(30),getdate()+28, 112)
or
convert(varchar(4),year(getdate()))+substring(Intpaydate2,3,2)+substring(Intpaydate2,1,2)=
 convert(varchar(30),getdate()+28, 112)
or
convert(varchar(4),year(getdate()))+substring(Intpaydate3,3,2)+substring(Intpaydate3,1,2)=
 convert(varchar(30),getdate()+28, 112)
or
convert(varchar(4),year(getdate()))+substring(Intpaydate4,3,2)+substring(Intpaydate4,1,2)=
 convert(varchar(30),getdate()+28, 112)
)



select top 10 *
from v57f_901_interest_payment
where isin= 'GB0001335420'
order by interestfromdate desc

select  *
from v57f_901_interest_payment
where isin= 'GB0001335420'
order by interestfromdate

select  *
from v57f_901_interest_payment
where interesttodate-interestfromdate<40
and exchgcd='gblse'
order by secid desc



select
v20c_920_BOND.*
--case when convert(varchar(4),year(getdate()))+substring(Intpaydate1,3,2)+substring(Intpaydate1,1,2)=
-- convert(varchar(30),getdate()+28, 112)
--     then convert(varchar(4),year(getdate()))+substring(Intpaydate1,3,2)+substring(Intpaydate1,1,2)
--convert(varchar(4),year(getdate()))+substring(Intpaydate1,3,2)+substring(Intpaydate1,1,2)
from v20c_920_BOND
where
Intpaydate1 <>''
and intrate<>''
and (
convert(varchar(4),year(getdate()))+substring(Intpaydate1,3,2)+substring(Intpaydate1,1,2)=
 convert(varchar(30),getdate()+28, 112)
or
convert(varchar(4),year(getdate()))+substring(Intpaydate2,3,2)+substring(Intpaydate2,1,2)=
 convert(varchar(30),getdate()+28, 112)
or
convert(varchar(4),year(getdate()))+substring(Intpaydate3,3,2)+substring(Intpaydate3,1,2)=
 convert(varchar(30),getdate()+28, 112)
or
convert(varchar(4),year(getdate()))+substring(Intpaydate4,3,2)+substring(Intpaydate4,1,2)=
 convert(varchar(30),getdate()+28, 112)
)




select
interestpaymentfrequency from bond
where
interestbasis = 'FXD'
and actflag <>'D'
and maturitydate>getdate()
and interestpaymentfrequency=''
and interestpaydate1=''
and interestpaydate2=''
and interestpaydate3=''
and interestpaydate4=''
order by interestpaymentfrequency


