use wca
select
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
DIVPY.CurenCD,
DIVPY.GrossDividend,
DIVPY.NetDividend
from div
INNER JOIN RD ON RD.RdID = DIV.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN SEDOL ON RD.SecID = SEDOL.SecID
INNER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN DIVPY ON DIV.DivID = DIVPY.DivID
where
sedol = 'B16KPT4'
and ('2008-04-04 00:00' >= exdt.exdate or '2008-04-04 00:00' >= pexdt.exdate)
and ('2008-04-04 00:00' <= exdt.paydate or '2008-04-04 00:00' <= pexdt.paydate)


use wca

select netDividend, grossDividend, curenCD, taxRate, exDate, payDate from v54f_620_dividend

where

'2008-04-04 00:00' >= exdate

and '2008-04-04 00:00' <= paydate

and divtype = 'C'

and curencd = 'GBP'

and netdividend != ''

and netdividend is not null

and exchgcd = 'gblse'

and sedol = 'B16KPT4'

