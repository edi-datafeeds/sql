use wca
select col001, 'active in wca' as mess
from portfolio.dbo.cusipfii
left outer join scmst on col001 = uscode
where secid is not null
and (statusflag<>'i' or statusflag is null)

union

select col001, 'inactive in wca' as mess
from portfolio.dbo.cusipfii
left outer join scmst on col001 = uscode
left outer join icc on col001 = oldisin
where scmst.secid is null
and icc.secid is null

union

select col001, 'oldcode in wca' as mess
from portfolio.dbo.cusipfii
left outer join icc on col001 = olduscode
where secid is not null

order by mess, col001
