-- MCAL
use wca
select redem.secid, redemtype, poolfactor, maturitydate, redemdate, mandoptflag, partfinal, redemprice from redem
inner join bond on redem.secid = bond.secid
where
redemtype <> 'PUT'
and mandoptflag = 'M'
and partfinal = 'F'
and not (redemtype='MAT' or maturitydate=redemdate)
and redemdate is not null
and redemdate <> '1800/01/01'
order by redemtype

-- BPUT
use wca
select redem.secid, redemtype, poolfactor, maturitydate, redemdate, mandoptflag, partfinal, redemprice from redem
inner join bond on redem.secid = bond.secid
where
redemtype = 'PUT'
and not (maturitydate=redemdate or maturitydate is null)
and redemdate is not null
and redemdate <> '1800/01/01'
order by redemtype


-- REDM
use wca
select redem.secid, redemtype, poolfactor, maturitydate, redemdate, mandoptflag, partfinal, redemprice 
from redem
inner join bond on redem.secid = bond.secid
where
mandoptflag = 'M'
and partfinal = 'F'
and redemdate is not null
and redemdate <> '1800/01/01'
and (redemtype='MAT' or maturitydate=redemdate)


-- PCAL
use wca
select redem.secid, redemtype, poolfactor, maturitydate, redemdate, mandoptflag, partfinal, redemprice 
from redem
inner join bond on redem.secid = bond.secid
where
partfinal = 'P'
and redemdate is not null
and redemdate <> '1800/01/01'
and not (redemtype='MAT' or maturitydate=redemdate)
and poolfactor=''
and redemtype <> 'PUT'
order by redemtype


-- PRED
use wca
select redem.secid, redemtype, poolfactor, maturitydate, redemdate, mandoptflag, partfinal, redemprice 
from redem
inner join bond on redem.secid = bond.secid
where
--mandoptflag = 'M'
partfinal = 'P'
and redemdate is not null
and redemdate <> '1800/01/01'
and not (redemtype='MAT' or maturitydate=redemdate)
and poolfactor<>''
and redemtype <> 'PUT'
order by redemtype


select distinct redemtype 
from redem


select * From v57f_FI_REDM