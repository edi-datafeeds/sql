use wca
select distinct
bond.secid,
scmst.issid,
issur.isstype,
sectycd,
statusflag,
maturitydate,
isin,
issuername,
securitydesc,
cntryofincorp,
exchgcd
from bond
inner join scmst on bond.secid = scmst.secid
inner join scexh on bond.secid = scexh.secid
inner join issur on scmst.issid = issur.issid
where
scmst.issid not in (select issid from scmst where sectycd = 'EQS' and statusflag<>'I')
and substring(exchgcd,3,3)<>'BND'
and issur.isstype<>'GOV'
and maturitydate>getdate()-1
and statusflag<>'I'
order by exchgcd, statusflag, maturitydate