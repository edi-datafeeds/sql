delete scmst
from scmst
left outer join cowar on scmst.secid=cowar.secid
where
sectycd='CW'
and cowar.secid is null

use wca
delete scmst
from scmst
left outer join cowar on scmst.secid=cowar.secid
where
sectycd='CW'
and cowar.expirationdate<getdate()-90
and cowar.secid is not null

delete cowar
from cowar
left outer join scmst on cowar.secid = scmst.secid
where
scmst.secid is null
and expirationdate<getdate()-90


delete bond
from bond
left outer join scmst on bond.secid = scmst.secid
where
scmst.secid is null

delete scexh
from scexh
left outer join scmst on scexh.secid = scmst.secid
where
scmst.secid is null

delete cpopt
from cpopt
left outer join scmst on cpopt.secid = scmst.secid
where
scmst.secid is null

delete sedol
from sedol
left outer join scmst on sedol.secid = scmst.secid
where
scmst.secid is null

