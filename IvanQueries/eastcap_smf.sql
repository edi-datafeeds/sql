use smf4
select 
col001 as isin,
security.sedol,
issuer.issuername,
security.longdesc,
security.statusflag
from portfolio.dbo.eastcap
left outer join wca.dbo.scmst on portfolio.dbo.eastcap.col001 = wca.dbo.scmst.isin
left outer join security on portfolio.dbo.eastcap.col001 = security.isin
left outer join issuer on security.issuerid = issuer.issuerid

where
wca.dbo.scmst.isin is null
order by sedol

