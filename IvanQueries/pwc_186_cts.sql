use portfolio
select SECID
into pwcsecid
from fisin
INNER JOIN wca.dbo.scmst on code=isin
where
accid=186
and fisin.actflag<>'D'
and statusflag<>'I'

union
select SECID from fuscode
INNER JOIN wca.dbo.scmst on code=uscode
where
accid=186
and fuscode.actflag<>'D'
and statusflag<>'I'
union
select wca.dbo.sedol.SECID from fsedol
INNER JOIN wca.dbo.sedol on code=sedol
INNER JOIN wca.dbo.scmst on wca.dbo.sedol.secid=wca.dbo.scmst.secid
where
accid=186
and fsedol.actflag<>'D'
and statusflag<>'I'

use wca
SELECT curencd, Count(curencd) AS Ct
FROM portfolio.dbo.pwcsecid
inner join bond on portfolio.dbo.pwcsecid.secid=bond.secid
GROUP BY curencd
ORDER BY CT desc

use wca
SELECT cntryofincorp, Count(bond.secid) AS Ct
FROM portfolio.dbo.pwcsecid
inner join bond on portfolio.dbo.pwcsecid.secid=bond.secid
inner join scmst on bond.secid=scmst.secid
inner join issur on scmst.issid=issur.issid
GROUP BY cntryofincorp
ORDER BY CT desc
