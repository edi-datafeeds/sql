use portfolio
if exists (select * from sysobjects where name = 'id_citi_old')
   drop table id_citi_old
GO

use wca
select secid
into portfolio.dbo.id_citi_old
from portfolio.dbo.fsedol as c
inner join sedol on code = sedol
where
accid = 190 and c.actflag='U'
union
select secid
from portfolio.dbo.fisin as c
inner join scmst on code = isin
where
accid = 190 and c.actflag='U'
union
select secid
from portfolio.dbo.fuscode as c
inner join scmst on code = uscode
where
accid = 190 and c.actflag='U'
GO

USE portfolio
ALTER TABLE id_citi_old ALTER COLUMN secid int NOT NULL 
GO 

USE portfolio
ALTER TABLE [DBO].[id_citi_old] WITH NOCHECK ADD 
       CONSTRAINT [pk_old_secid] PRIMARY KEY ([secid])  ON [PRIMARY]
GO

use portfolio
if exists (select * from sysobjects where name = 'id_citi_new')
   drop table id_citi_new
GO
 
use wca
select secid
into portfolio.dbo.id_citi_new
from portfolio.dbo.fsedol as c
inner join sedol on code = sedol
where
accid = 190 and c.actflag='I'
union
select secid
from portfolio.dbo.fisin as c
inner join scmst on code = isin
where
accid = 190 and c.actflag='I'
union
select secid
from portfolio.dbo.fuscode as c
inner join scmst on code = uscode
where
accid = 190 and c.actflag='I'
GO

USE portfolio
ALTER TABLE id_citi_new ALTER COLUMN secid int NOT NULL 
GO 

USE portfolio
ALTER TABLE [DBO].[id_citi_new] WITH NOCHECK ADD 
       CONSTRAINT [pk_new_secid] PRIMARY KEY ([secid])  ON [PRIMARY]
GO