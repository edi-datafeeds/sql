use wca
select
isin as ISIN,
Issuername as CompanyName,
SecurityDesc as SecurityTitle,
ParValue as NominalValue,
PVCurrency as Currency,
MIC as Market,
v54f_650_DVOP.Frequency as Payment_Type,
case when divtype_1='C' and actflag_1<>'D' and (defaultopt_1 = 'T' or curencd_1='GBP') then PaymentAmount_1
     when divtype_2='C' and actflag_2<>'D' and (defaultopt_2 = 'T' or curencd_2='GBP') then PaymentAmount_2
     when divtype_3='C' and actflag_3<>'D' and (defaultopt_3 = 'T' or curencd_3='GBP') then PaymentAmount_3
     when divtype_4='C' and actflag_4<>'D' and (defaultopt_4 = 'T' or curencd_4='GBP') then PaymentAmount_4
     when divtype_5='C' and actflag_5<>'D' and (defaultopt_5 = 'T' or curencd_5='GBP') then PaymentAmount_5
     when divtype_6='C' and actflag_6<>'D' and (defaultopt_6 = 'T' or curencd_6='GBP') then PaymentAmount_6
     when divtype_1='C' and actflag_1<>'D' then PaymentAmount_1
     when divtype_2='C' and actflag_2<>'D' then PaymentAmount_2
     when divtype_3='C' and actflag_3<>'D' then PaymentAmount_3
     when divtype_4='C' and actflag_4<>'D' then PaymentAmount_4
     when divtype_5='C' and actflag_5<>'D' then PaymentAmount_5
     when divtype_6='C' and actflag_6<>'D' then PaymentAmount_6
     end as PaymentAmount,
case when divtype_1='C' and actflag_1<>'D' and (defaultopt_1 = 'T' or curencd_1='GBP') then curencd_1
     when divtype_2='C' and actflag_2<>'D' and (defaultopt_2 = 'T' or curencd_2='GBP') then curencd_2
     when divtype_3='C' and actflag_3<>'D' and (defaultopt_3 = 'T' or curencd_3='GBP') then curencd_3
     when divtype_4='C' and actflag_4<>'D' and (defaultopt_4 = 'T' or curencd_4='GBP') then curencd_4
     when divtype_5='C' and actflag_5<>'D' and (defaultopt_5 = 'T' or curencd_5='GBP') then curencd_5
     when divtype_6='C' and actflag_6<>'D' and (defaultopt_6 = 'T' or curencd_6='GBP') then curencd_6
     when divtype_1='C' and actflag_1<>'D' then curencd_1
     when divtype_2='C' and actflag_2<>'D' then curencd_2
     when divtype_3='C' and actflag_3<>'D' then curencd_3
     when divtype_4='C' and actflag_4<>'D' then curencd_4
     when divtype_5='C' and actflag_5<>'D' then curencd_5
     when divtype_6='C' and actflag_6<>'D' then curencd_6
     end as PaymentCurrency,
case when divtype_1='C' and actflag_1<>'D' and (defaultopt_1 = 'T' or curencd_1='GBP') then netgrossmarker_1
     when divtype_2='C' and actflag_2<>'D' and (defaultopt_2 = 'T' or curencd_2='GBP') then netgrossmarker_2
     when divtype_3='C' and actflag_3<>'D' and (defaultopt_3 = 'T' or curencd_3='GBP') then netgrossmarker_3
     when divtype_4='C' and actflag_4<>'D' and (defaultopt_4 = 'T' or curencd_4='GBP') then netgrossmarker_4
     when divtype_5='C' and actflag_5<>'D' and (defaultopt_5 = 'T' or curencd_5='GBP') then netgrossmarker_5
     when divtype_6='C' and actflag_6<>'D' and (defaultopt_6 = 'T' or curencd_6='GBP') then netgrossmarker_6
     when divtype_1='C' and actflag_1<>'D' then netgrossmarker_1
     when divtype_2='C' and actflag_2<>'D' then netgrossmarker_2
     when divtype_3='C' and actflag_3<>'D' then netgrossmarker_3
     when divtype_4='C' and actflag_4<>'D' then netgrossmarker_4
     when divtype_5='C' and actflag_5<>'D' then netgrossmarker_5
     when divtype_6='C' and actflag_6<>'D' then netgrossmarker_6
     end as NetGrossMarker,
null as DeclarationDate,
Paydate as PaymentDate,
Exdate as ExDate,
case when divtype_1='S' and actflag_1<>'D' then 'Y'
     when divtype_2='S' and actflag_2<>'D' then 'Y'
     when divtype_3='S' and actflag_3<>'D' then 'Y'
     when divtype_4='S' and actflag_4<>'D' then 'Y'
     when divtype_5='S' and actflag_5<>'D' then 'Y'
     when divtype_6='S' and actflag_6<>'D' then 'Y'
     else ''
     end as ScripOptionMarker,
case when divtype_1='S' and actflag_1<>'D' then rationew_1
     when divtype_2='S' and actflag_2<>'D' then rationew_2
     when divtype_3='S' and actflag_3<>'D' then rationew_3
     when divtype_4='S' and actflag_4<>'D' then rationew_4
     when divtype_5='S' and actflag_5<>'D' then rationew_5
     when divtype_6='S' and actflag_6<>'D' then rationew_6
     else ''
     end as RatioNew,
case when divtype_1='S' and actflag_1<>'D' then ratioold_1
     when divtype_2='S' and actflag_2<>'D' then ratioold_2
     when divtype_3='S' and actflag_3<>'D' then ratioold_3
     when divtype_4='S' and actflag_4<>'D' then ratioold_4
     when divtype_5='S' and actflag_5<>'D' then ratioold_5
     when divtype_6='S' and actflag_6<>'D' then ratioold_6
     else ''
     end as RatioOld,
case when DRIPCntryCD<>'' then 'Y' else null end as DRIPmarker,
case when DRIPCntryCD<>'' then DripReinvPrice else null end as DRIPprice,
case when DRIPCntryCD<>'' then CNTRY.CurenCD else null end as DRIPCurrency,
case when StructCD='REIT' then 'Y' else null end as REITmarker,
DIV.DIVNotes as Notes
from v54f_650_DVOP
inner join div on eventid = divid
left outer join CNTRY on DRIPCntryCD=CNTRY.CntryCD
where
(mic='XDUB'
or mic='XLON'
or mic='XPLU')
and
((divtype_1='C' and actflag_1<>'D')
or (divtype_2='C' and actflag_2<>'D')
or (divtype_3='C' and actflag_3<>'D')
or (divtype_4='C' and actflag_4<>'D')
or (divtype_5='C' and actflag_5<>'D')
or (divtype_6='C' and actflag_6<>'D'))
and paydate>'2008/04/06'
and paydate<'2009/04/06'
