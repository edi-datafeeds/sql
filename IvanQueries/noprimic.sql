use wca
select icc.secid as newsecid, scmst2.isin as newIsin, scmst2.statusflag, col001 from portfolio.dbo.noprimic
left outer join scmst on portfolio.dbo.noprimic.col001 = isin
left outer join icc on col001 = oldisin
left outer join exchg on primaryexchgcd = exchgcd
left outer join scmst as scmst2 on icc.secid = scmst2.secid
where scmst.secid is null

and scmst.statusflag <> 'I'

and mic=''