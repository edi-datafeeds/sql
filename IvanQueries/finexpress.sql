use wca
select
issur.cntryofincorp,
issur.issuername, 
scmst.securitydesc,
scmst.isin,
bond.maturitydate,
case when FRNINDXBEN.Lookup is null then bond.FRNIndexBenchmark else FRNINDXBEN.Lookup end as FRN_Index_Benchmark,
bond.curencd as Current_Currency,
case when wfi.dbo.sys_debt_type.debt_type ='S' then 'Supranational' 
     when wfi.dbo.sys_debt_type.debt_type='G' then 'Government' 
     when wfi.dbo.sys_debt_type.debt_type='Y' then 'Government Agency' 
     else 'Corporate'
     end as Debt_Structure,
ratingagency,
rating
from portfolio.dbo.finexpress
left outer join scmst on col001 = isin
left outer join crdrt on scmst.secid = crdrt.secid
inner join issur on scmst.issid = issur.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.dbo.sys_lookup as STRUCTCD on SCMST.StructCD = STRUCTCD.Code
                      and 'STRUCTCD' = STRUCTCD.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNINDXBEN on BOND.FRNIndexBenchmark = FRNINDXBEN.Code
                      and 'FRNINDXBEN' = FRNINDXBEN.TypeGroup
left outer join wfi.dbo.sys_debt_type on scmst.issid = wfi.dbo.sys_debt_type.issid
where 
scmst.actflag<>'D'
and bond.actflag<>'D'
and scmst.actflag<>'D'
and scmst.statusflag<>'I'
