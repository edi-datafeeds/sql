
use smf4
if exists (select * from sysobjects 
where name = 'WCADtemp')
delete from WCADtemp


use wca
insert into smf4.dbo.WCADtemp
select * 
from v54f_620_dividend
where 
changed = (select max(acttime) from tbl_Opslog)
and exchgcd='GBLSE'
and optionid is not null
and optionid=1
and divtype = 'C'
and (sectycd = 'DR')

use wca
select * 
from v54f_620_dividend
where 
changed = (select max(acttime) from tbl_Opslog)
and exchgcd='GBLSE'
and optionid is not null
and optionid=1
and divtype = 'C'
and (sectycd = 'DR' or cntryofincorp<>'GB')

select * from smf4.dbo.WCADtemp

use smf4
delete from edicaoption
where exists 
(select
WCADtemp.eventid
from WCADtemp
where
edicaoption.CADID = WCADtemp.eventid
and edicaoption.distsedol='WCAD')



use smf4
delete from edicadetail
where exists 
(select
WCADtemp.eventid as CADID
from WCADtemp
where
edicadetail.CADID = WCADtemp.eventid
and edicadetail.SsnYear='WCAD')


use smf4
insert into edicadetail
(Actflag, Actdate, CADID, SecurityID, AcceptDate, Closedate,
ExDate, PayDate, OsTaxrate,
TaxRateInd, Confirmation, credate, ssnyear, CAtype)
select
case when WCADtemp.Actflag = 'D' then WCADtemp.Actflag
     else WCADtemp.Actflag end as ActFlag1,
changed,
WCADtemp.eventid,
security.securityID,
WCADtemp.OptElectionDate,
WCADtemp.RecDate as Closedate,
WCADtemp.ExDate,
WCADtemp.PayDate,
WCADtemp.Taxrate,
'G' as TaxRateInd,
'C' as Confirmation,
WCADtemp.Created,
'WCAD' as ssnyear,
'DD' as CAType
from WCADtemp
inner join security on WCADtemp.sedol = security.sedol
where
not exists
(select edicadetail.securityID from edicadetail
where edicadetail.securityID = security.securityID
and edicadetail.CADID = WCADtemp.eventid
and optionid = 1 
and divtype = 'C')




use smf4
insert into smf4.dbo.edicaoption
(Actflag, Actdate, CADID, SecurityID, CashDistCallRate,
 OptionCurr, credate, distsedol, payrate, Divtype, Comment)
select 
case when WCADtemp.Actflag = 'D' then WCADtemp.Actflag
     else WCADtemp.Actflag end as ActFlag1,
changed,
WCADtemp.eventid,
security.securityID,
WCADtemp.GrossDividend,
WCADtemp.CurenCD,
WCADtemp.Created,
'WCAD' as distsedol,
'C' as payrate,
'C' as Divtype,
case when WCADtemp.approxflag = 'T' then 'APPROXIMATE DIVIDEND' else '' end as Comment
from WCADtemp
inner join smf4.dbo.security on WCADtemp.sedol = security.sedol
where
not exists
(select edicaoption.cadid from edicaoption
where edicaoption.securityID = security.securityID
and edicaoption.CADID = WCADtemp.eventid
and optionid = 1 
and divtype = 'C')
