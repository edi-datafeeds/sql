select distinct issur.issid
into portfolio.dbo.lissid
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
liststatus<>'D'
and scexh.actflag<>'D'
and scmst.actflag<>'D'
and issur.actflag<>'D'
and scmst.sectycd<>'BND'

select distinct issur.issid
--into portfolio.dbo.lissid
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
issur.issid not in (select issid from portfolio.dbo.lissid)
and scexh.actflag<>'D'
and scmst.actflag<>'D'
and issur.actflag<>'D'
and scmst.sectycd<>'BND'

I need to give list of all delisted securities to Jonathan since 2004. 
They need Issuer name, ISIN, Local codes, Country of Incorp, Delisting date, 
Exchange code, Exchange name, reason for delisting.

select issuername, cntryofincorp,  isin, sectycd, scexh.exchgcd, localcode,  effectivedate, eventtype, reason, scexh.acttime
from scmst
inner join scexh on scmst.secid=scexh.secid
inner join issur on scmst.issid = issur.issid
left outer join lstat on scexh.exchgcd=lstat.exchgcd
                         and scexh.secid = lstat.secid
where
scmst.issid not in (select issid from portfolio.dbo.lissid)
and primaryexchgcd=scexh.exchgcd
and scexh.actflag<>'D'
and scmst.actflag<>'D'
--and issur.actflag<>'D'
and scmst.sectycd='EQS'
and lstatstatus is null
order by issuername


select issuername, cntryofincorp,  isin, sectycd, scexh.exchgcd, localcode,  effectivedate, eventtype, reason, scexh.acttime
from scmst
inner join scexh on scmst.secid=scexh.secid
inner join issur on scmst.issid = issur.issid
left outer join lstat on scexh.exchgcd=lstat.exchgcd
                         and scexh.secid = lstat.secid
where
scmst.issid not in (select issid from portfolio.dbo.lissid)
and primaryexchgcd=scexh.exchgcd
and scexh.actflag<>'D'
and scmst.actflag<>'D'
and issur.actflag<>'D'
and scmst.sectycd='EQS'
and lstatstatus= 'D'
order by issuername






select distinct issuername
from scmst
inner join scexh on scmst.secid=scexh.secid
inner join issur on scmst.issid = issur.issid
left outer join lstat on scexh.exchgcd=lstat.exchgcd
                         and scexh.secid = lstat.secid
where
scmst.issid not in (select issid from portfolio.dbo.lissid)
and primaryexchgcd=scexh.exchgcd
and scexh.actflag<>'D'
and scmst.actflag<>'D'
--and issur.actflag<>'D'
and scmst.sectycd='EQS'
and lstatstatus is null
order by issuername


select distinct issuername
from scmst
inner join scexh on scmst.secid=scexh.secid
inner join issur on scmst.issid = issur.issid
left outer join lstat on scexh.exchgcd=lstat.exchgcd
                         and scexh.secid = lstat.secid
where
scmst.issid not in (select issid from portfolio.dbo.lissid)
and primaryexchgcd=scexh.exchgcd
and scexh.actflag<>'D'
and scmst.actflag<>'D'
and issur.actflag<>'D'
and scmst.sectycd='EQS'
and lstatstatus= 'D'
order by issuername
