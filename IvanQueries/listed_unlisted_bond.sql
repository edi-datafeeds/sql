use wca
select distinct bond.secid
--into portfolio.dbo.listedbond
from
bond
inner join scexh on bond.secid = scexh.secid
inner join scmst on bond.secid = scmst.secid
where
liststatus<>'D'
and substring(scexh.exchgcd,3,3)<>'BND'
and statusflag<>'I'
and bond.actflag<>'D'

use wca
select distinct bond.secid
from
bond
inner join scmst on bond.secid = scmst.secid
where
statusflag<>'I'
and bond.secid not in (select secid from portfolio.dbo.listedbond)
and bond.actflag<>'D'
