use portfolio
delete xsecid

use wca
insert into portfolio.dbo.xsecid
select distinct secid
from scmst
inner join issur on scmst.issid = issur.issid
where (scmst.issid = 83902
or scmst.issid = 30419
or scmst.issid = 33931
or scmst.issid = 75774
or scmst.issid = 118277)
and issur.cntryofincorp = 'US'
and substring(scmst.isin,1,2) <> ''
and scmst.actflag<>'D'
and scmst.sectycd = 'BND'


use wca
insert into portfolio.dbo.xsecid
select distinct secid
from scmst
inner join issur on scmst.issid = issur.issid
where (scmst.issid = 49516
or scmst.issid = 41829
or scmst.issid = 41817
or scmst.issid = 42995
or scmst.issid = 43403)
and issur.cntryofincorp = 'CA'
and substring(scmst.isin,1,2) <> ''
and scmst.actflag<>'D'
and scmst.sectycd = 'BND'




