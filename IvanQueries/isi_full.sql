use wca
select top 50
exchg.cntrycd,
exchg.mic,
scmst.secid,
scmst.isin,
scexh.Localcode,
issur.issuername, 
scmst.issid,
scmst.securitydesc,
scexh.exchgcd,
scmst.sectycd,
scmst.primaryexchgcd,
case when scmst.statusflag<>'I' then 'A' else 'I' end as globalscmststatus,
case when scexh.liststatus is null then 'U' when scexh.liststatus='N' or scexh.liststatus='' then 'L' else scexh.liststatus end as wcaliststatus,
scmst.acttime as changed,
scmst.actflag as ScmstRecordStatus,
scexh.actflag as ScexhRecordStatus
from scmst
inner join issur on scmst.issid = issur.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
scmst.actflag<>'D'
and scexh.actflag<>'D'
