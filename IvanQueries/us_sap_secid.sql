use portfilio


select top 2000
bond.secid
into portfolio.dbo.xsecid
from bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where maturitydate>getdate()+1
--and bondtype = 'BD'
and cntryofincorp='US'
--and issuername like '%government%'
--and scmst.issid in (select portfolio.dbo.xissid.issid from portfolio.dbo.xissid)
order by maturitydate
