use wca
select
col001 as isin,
Uscode,
Issuername,
SecurityDesc,
MaturityDate,
Perpetual,
InterestBasis,
InterestRate,
FRNType,
FRNIndexBenchmark,
bond.CurenCD as DebtCurrency
from portfolio.dbo.rodisin
left outer join scmst on col001 = isin
left outer join issur on scmst.issid = issur.issid
left outer join bond on scmst.secid = bond.secid

