use wca
SELECT secty.securitydescriptor, Count(scmst.secid) AS Ct
FROM scmst
inner join secty on scmst.sectycd = secty.sectycd
where
scmst.actflag<>'D'
and (scmst.statusflag<>'I' or scmst.statusflag is null)
and substring(primaryexchgcd,1,2)<>'US'
and substring(primaryexchgcd,1,2)<>'CA'
GROUP BY secty.securitydescriptor order by secty.securitydescriptor


use wca
SELECT sectygrp.securitydescriptor, Count(scmst.secid) AS Ct
FROM scmst
inner join sectygrp on scmst.sectycd = sectygrp.sectycd
where
scmst.actflag<>'D'
and (scmst.statusflag<>'D' or scmst.statusflag is null)
GROUP BY sectygrp.securitydescriptor order by sectygrp.securitydescriptor


select * from sectygrp