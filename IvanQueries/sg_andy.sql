use wca
select distinct
portfolio.dbo.SG.Sedol,
exdate,
recdate,
'DIV' as Eventtype,
EventID
from portfolio.dbo.SG
left outer join v54f_620_dividend on portfolio.dbo.SG.sedol = v54f_620_dividend.sedol
 and ((changed>'2009/10/16' and changed<'2009/12/01')
 or (created>'2009/10/16' and created<'2009/12/01'))


use wca
select * from tbl_opslog