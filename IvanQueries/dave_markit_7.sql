use portfolio
select
'euro_corp' as MatchFile,
col001 as code,
wca.dbo.scmst.Sectycd
from portfolio.dbo.euro_corp
left outer join wca.dbo.scmst on col001 = isin
order by wca.dbo.scmst.Sectycd, col001

use portfolio
select 
'euro_sov' as MatchFile,
col001 as code,
wca.dbo.scmst.Sectycd
from portfolio.dbo.euro_sov
left outer join wca.dbo.scmst on col001 = isin
order by wca.dbo.scmst.Sectycd, col001

use portfolio
select 
'gilt' as MatchFile,
col001 as code,
wca.dbo.scmst.Sectycd
from portfolio.dbo.gilt
left outer join wca.dbo.scmst on col001 = isin
order by wca.dbo.scmst.Sectycd, col001

use portfolio
select 
'pfandbrief' as MatchFile,
col001 as code,
wca.dbo.scmst.Sectycd
from portfolio.dbo.pfandbrief
left outer join wca.dbo.scmst on col001 = isin
order by wca.dbo.scmst.Sectycd, col001

use portfolio
select 
'usd_bullet' as MatchFile,
col001 as code,
wca.dbo.scmst.Sectycd
from portfolio.dbo.usd_bullet
left outer join wca.dbo.scmst on col001 = isin
order by wca.dbo.scmst.Sectycd, col001

use portfolio
select 
'usd_trace' as MatchFile,
col001 as code,
wca.dbo.scmst.Sectycd
from portfolio.dbo.usd_trace
left outer join wca.dbo.scmst on col001 = uscode
order by wca.dbo.scmst.Sectycd, col001

use portfolio
select 
'gbp_corp' as MatchFile,
col001 as code,
wca.dbo.scmst.Sectycd
from portfolio.dbo.gbp_corp
left outer join wca.dbo.scmst on col001 = isin
order by wca.dbo.scmst.Sectycd, col001

