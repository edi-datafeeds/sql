use prices
select 
MIC,
LocalCode,
case when wca.dbo.scmst.Isin is not null and wca.dbo.scmst.Isin <> '' then wca.dbo.scmst.Isin ELSE SRF_isi.Isin end as Isin,
Currency,
case when wca.dbo.scmst.secid is not null then wca.dbo.scmst.secid ELSE SRF_isi.SecID end as SecID,
case when wca.dbo.issur.Issuername is not null then wca.dbo.issur.Issuername ELSE SRF_isi.Issuername end as Issuername,
case when wca.dbo.scmst.SectyCD is not null then wca.dbo.scmst.SectyCD ELSE SRF_isi.SectyCD end as SectyCD,
case when wca.dbo.scmst.SecurityDesc is not null then wca.dbo.scmst.SecurityDesc ELSE SRF_isi.SecurityDesc end as SecurityDesc,
--case when smf4.dbo.Security.sedol is not null and smf4.dbo.Security.sedol <> '' then smf4.dbo.Security.sedol ELSE SRF_isi.sedol end as Sedol,
SRF_isi.sedol,
case when wca.dbo.scmst.uscode is not null and wca.dbo.scmst.uscode <> '' then wca.dbo.scmst.uscode ELSE SRF_isi.uscode end as Uscode,
case when wca.dbo.scmst.PrimaryExchgCD is not null and wca.dbo.scmst.PrimaryExchgCD <> '' then wca.dbo.scmst.PrimaryExchgCD ELSE SRF_isi.PrimaryExchgCD end as PrimaryExchgCD,
Cntrycd,
ExchgCD,
[id]
into srf_isi4
from SRF_isi
left outer join wca.dbo.scmst on SRF_isi.isin = wca.dbo.scmst.isin
                and SRF_isi.isin <> ''
left outer join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid

update srf_isi
set cntrycd = substring(exchgcd,1,2)

select  secid from srf_isi
where sedol<>''

use smf4
update security
set cntrycd = miclist.cntrycd
from
security
inner join miclist on security.opol = miclist.mic

select count(secid)
from srf_isi1
where secid<>''

select * from srf_isi4
where cntrycd is null


use prices
select distinct
MIC,
LocalCode,
srf_isi4.Isin,
Currency,
SecID,
Issuername,
SectyCD,
SecurityDesc,
case when smf4.dbo.Security.sedol is not null and smf4.dbo.Security.sedol <> '' then smf4.dbo.Security.sedol ELSE SRF_isi4.sedol end as Sedol,
uscode,
PrimaryExchgCD,
ExchgCD
into srf_isi6
from SRF_isi4
left outer join smf4.dbo.security on srf_isi4.isin = smf4.dbo.security.isin
                and srf_isi4.isin<>''
                and srf_isi4.cntrycd = smf4.dbo.security.cntrycd
where


where mktclosedate>'2009/02/23'


use prices
select top 10 *
from SRF_isi
where mktclosedate>'2009/02/23'


use prices
drop table sungard

use prices
SELECT
clientport.*,
SRF_isi.mic as EdiPriceMarket,
SRF_isi.[close] as EdiClose,
currency as EdiCurrency,
smf4.dbo.security.opol as SmfMarket,
smf4.dbo.security.StatusFlag,
exchgcd,
smf4.dbo.miclist.cntrycd as MarketCountry
into sungard
FROM clientport
left outer join smf4.dbo.security on clientport.sedol = smf4.dbo.security.sedol
                        and clientport.sedol is not null 
                        and clientport.sedol <> ''
left outer join smf4.dbo.miclist on smf4.dbo.security.opol = smf4.dbo.miclist.mic
                    and smf4.dbo.security.opol is not null
left outer join SRF_isi on clientport.isin = SRF_isi.isin
                        and clientport.isin is not null
                        and clientport.isin <> ''
                        and smf4.dbo.miclist.cntrycd = substring(exchgcd,1,2)

                        and smf4.dbo.miclist.mic is not null
                        and mktclosedate='2008/11/26'
                        and ((smf4.dbo.security.opol = SRF_isi.mic) or 
                             (smf4.dbo.security.opol = 'XLON' and SRF_isi.mic='SEAQ') or
                             (smf4.dbo.security.opol = 'XNCM' and SRF_isi.mic='XNAS') or
                             (smf4.dbo.security.opol = 'XNGS' and SRF_isi.mic='XNAS') or
                             (smf4.dbo.security.opol = 'XNMS' and SRF_isi.mic='XNAS'))
union

SELECT
clientport.*,
SRF_isi.mic as EdiPriceMarket,
SRF_isi.[close] as EdiClose,
currency as EdiCurrency,
smf4.dbo.security.opol as SmfMarket,
smf4.dbo.security.StatusFlag,
exchgcd,
smf4.dbo.miclist.cntrycd as MarketCountry
FROM clientport
left outer join smf4.dbo.security on clientport.sedol = smf4.dbo.security.sedol
                        and clientport.sedol is not null 
                        and clientport.sedol <> ''
left outer join smf4.dbo.miclist on smf4.dbo.security.opol = smf4.dbo.miclist.mic
                    and smf4.dbo.security.opol is not null
left outer join SRF_isi on clientport.sedol = SRF_isi.sedol
                        and clientport.sedol is not null
                        and clientport.sedol <> ''
                        and smf4.dbo.miclist.cntrycd = substring(exchgcd,1,2)
                        and smf4.dbo.miclist.mic is not null
                        and mktclosedate='2008/11/26'
                        and ((smf4.dbo.security.opol = SRF_isi.mic) or (smf4.dbo.security.opol = 'XLON' and SRF_isi.mic='SEAQ'))


--select * from SRF_isi
--where isin = 'BRALLLCDAM10'


drop table sunid

use prices
select id 
into sunid
from sungard
group by id
having count(id) >  1



select sungard.* from sungard
left outer join sunid on sungard.id = sunid.id
where
(sungard.EdiPriceMarket is not null
or sunid.id is null)



select distinct sungard.id 
into matched_id
from sungard
where 
sungard.EdiPriceMarket is not null


use prices
select * from SRF_isi
--where sedol = 'B1P8G85'
--where isin = 'XS0284605036'

where sedol ='0182704'

where isin = 'GB0006486152'



select * from sungard
where sedol is null

use smf4
select sedol, opol, isin from security
--where isin = 'XS0284605036'

where sedol = '2027342'


use prices
SELECT distinct
clientport.*,
SRF_isi.mic as EdiPriceMarket,
SRF_isi.[close] as EdiClose,
currency as EdiCurrency,
' ' as SmfMarket,
' ' as StatusFlag,
exchgcd,
' ' as MarketCountry
--into sungard1
FROM clientport
left outer join SRF_isi on clientport.isin = SRF_isi.isin
                        and clientport.isin is not null
                        and clientport.isin <> ''
                        and mktclosedate='2008/11/26'
where
clientport.id not in (select id from matched_id)
and SRF_isi.mic is not null

use wca
select cntrycd from continent
where continent = 'Africa'

use prices
select * from SRF_isi
where
--isin='SE0000337842'
mic='xsto'



use smf4
select * from security
where
opol = 'XWBO'
sedol = 'b0742y2'

use smf4
select * from miclist
where mic='xmad'

use prices
update SRF_isi
set mic='ROCO'
where mic='OTC'

select * from SRF_isi
where mic = 'OTC'
where substring(isin,1,2) = 'TW'


se

select distinct srf_isi7.*, wca.dbo.scmst.issid from srf_isi7
left outer join wca.dbo.scmst on srf_isi7.secid = wca.dbo.scmst.secid 
order by cntrycd, srf_isi7.mic, srf_isi7.sectycd, srf_isi7.issuername, srf_isi7.localcode

select distinct srf_isi7.*, wca.dbo.scmst.issid from srf_isi7
left outer join wca.dbo.scmst on srf_isi7.secid = wca.dbo.scmst.secid 
order by cntrycd, srf_isi7.mic, srf_isi7.sectycd, srf_isi7.issuername, srf_isi7.localcode

select distinct mic, secid, tradingcurrency from srf_isi7
where secid <> 0

select distinct mic, secid, currency, localcode from srf_isi2
where secid <> 0

select distinct * from srf_isi2

select * 
into srf_isi2
from srf_isi

select * from srf_isi

select * from srf_isi7
left outer join wca.dbo.icc on srf_isi7.isin = wca.dbo.icc.oldisin
where srf_isi7.secid = 0
and isin <>''
and wca.dbo.icc.secid is not null


use prices
select 
substring(SRF_isi6.exchgcd,1,2) as CntryCD,
SRF_isi6.MIC,
SRF_isi6.LocalCode,
SRF_isi6.Isin,
SRF_isi6.TradingCurrency,
case when SRF_isi6.secid is not null then SRF_isi6.secid ELSE wca.dbo.icc.SecID end as SecID,
case when wca.dbo.issur.Issuername is not null then wca.dbo.issur.Issuername ELSE SRF_isi6.Issuername end as Issuername,
case when wca.dbo.scmst.SectyCD is not null then wca.dbo.scmst.SectyCD ELSE SRF_isi6.SectyCD end as SectyCD,
case when wca.dbo.scmst.SecurityDesc is not null then wca.dbo.scmst.SecurityDesc ELSE SRF_isi6.SecurityDesc end as SecurityDesc,
SRF_isi6.sedol,
case when wca.dbo.scmst.uscode is not null and wca.dbo.scmst.uscode <> '' then wca.dbo.scmst.uscode ELSE SRF_isi6.uscode end as Uscode,
case when wca.dbo.scmst.PrimaryExchgCD is not null and wca.dbo.scmst.PrimaryExchgCD <> '' then wca.dbo.scmst.PrimaryExchgCD ELSE SRF_isi6.PrimaryExchgCD end as PrimaryExchgCD,
SRF_isi6.ExchgCD
into srf_isi7
from SRF_isi6
left outer join wca.dbo.icc on srf_isi6.isin = wca.dbo.icc.oldisin
                 and srf_isi6.secid=0
                 and srf_isi6.isin<>''
left outer join wca.dbo.scmst on wca.dbo.icc.secid = wca.dbo.scmst.secid
left outer join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid



use wca
select * from sedolseq1
where seqnum>1


use wca
select distinct scmst.secid
into portfolio.dbo.ftsesecid
from portfolio.dbo.ftse15022
left outer join sedol on col001 = sedol
left outer join scmst on sedol.secid = scmst.secid
where
sedol.secid is not null
order by scmst.secid

use wca
select recdate, sedol, statusflag
from portfolio.dbo.isin66
left outer join scmst on portfolio.dbo.isin66.isin = scmst.isin
left outer join sedol on scmst.secid = sedol.secid
left outer join rd on scmst.secid = rd.secid
where
rd.acttime>'2008/10/01'

scmst.secid not in (select secid from portfolio.dbo.ftsesecid)

use smf4
select distinct isin
into portfolio.dbo.ftseISIN
from portfolio.dbo.ftse66
left outer join security on col001 = sedol
where
security.isin <>''

use wca 
select * from icc
where 
oldisin='CNE100000999'

use wca
select * from sdchg
where rcntrycd<>newrcntrycd
and eventtype<>'clean'
and eventtype<>'corr'

use wca
select * from portfolio.dbo.ftse15022
inner join sedolseq1 on col001 = sedol
where seqnum=1


use wca
select sectycd from sedol
inner join scmst on sedol.secid=scmst.secid
where cntrycd = 'ZZ'

use wca
select * from sedolseq2
where seqnum>1