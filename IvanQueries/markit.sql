use wca
select col001, scmst.secid, scmst.sectycd, issur.issuername, case when scmst.statusflag = '' then 'A' ELSE scmst.statusflag end as statusflag,
smf4.dbo.security.statusflag, smf4.dbo.security.shortdesc, smf4.dbo.sectype.name, smf4.dbo.sectype.typegroup, smf4.dbo.security.actflag
from
portfolio.dbo.markit
left outer join sedol on col001=sedol.sedol
left outer join scmst on sedol.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join v54f_620_dividend on scmst.secid = v54f_620_dividend.secid
left outer join smf4.dbo.security on col001 = smf4.dbo.security.sedol
left outer join smf4.dbo.sectype on smf4.dbo.security.sectype = code
where sedol.secid is not null
and sedol.defunct = 'F'
and col001 not in (select portfolio.dbo.mitdiv.col001 from portfolio.dbo.mitdiv)
and v54f_620_dividend.secid is null
and scmst.actflag <>'D'
and sedol.actflag <>'D'

order by scmst.statusflag, scmst.sectycd, issur.issuername

select * from scmst

select * from v51f_609_Warrant_expiry_alert
where isin = 'gb0030927478'

select * from sedol

select * from scexh
where secid = 90407


where sedol = '3092747'

use wca
select col001, statusflag, shortdesc, smf4.dbo.sectype.name, smf4.dbo.sectype.typegroup, smf4.dbo.security.actflag
from
portfolio.dbo.markit
left outer join sedol on col001=sedol.sedol
left outer join smf4.dbo.security on col001 = smf4.dbo.security.sedol
left outer join smf4.dbo.sectype on smf4.dbo.security.sectype = code
where sedol.secid is null
order by typegroup, statusflag, shortdesc
