use wca
select Isin, Issuername, SecurityDesc
from SCMST
inner join issur on scmst.issid = issur.issid
where
scmst.actflag<>'D'
and (scmst.statusflag <> 'I' OR scmst.statusflag is null)
and isin <> ''
order by issuername, isin