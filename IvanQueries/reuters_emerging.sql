use wca
insert into portfolio.dbo.xsecid

select bond.secid
from bond
inner join scmst on bond.secid = scmst.secid
WHERE
maturitydate > getdate()-1
and scmst.actflag<>'D'
and BOND.Actflag<>'D'
and (
substring(isin,1,2)='DZ'
or substring(isin,1,2)='AO'
or substring(isin,1,2)='AZ'
or substring(isin,1,2)='BA'
or substring(isin,1,2)='BW'
or substring(isin,1,2)='CM'
or substring(isin,1,2)='CI'
or substring(isin,1,2)='HR'
or substring(isin,1,2)='GM'
or substring(isin,1,2)='GE'
or substring(isin,1,2)='GH'
or substring(isin,1,2)='KZ'
or substring(isin,1,2)='KE'
or substring(isin,1,2)='KG'
or substring(isin,1,2)='LT'
or substring(isin,1,2)='MW'
or substring(isin,1,2)='MA'
or substring(isin,1,2)='NG'
or substring(isin,1,2)='SL'
or substring(isin,1,2)='SK'
or substring(isin,1,2)='SI'
or substring(isin,1,2)='SD'
or substring(isin,1,2)='TN'
or substring(isin,1,2)='TR'
or substring(isin,1,2)='UG'
or substring(isin,1,2)='UA'
or substring(isin,1,2)='UZ'
or substring(isin,1,2)='ZM')
