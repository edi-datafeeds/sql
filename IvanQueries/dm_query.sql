use wca
select issuername, securitydesc, exchgcd, x as commoncode, uscode, sedol
from bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join sedol on scmst.secid = sedol.secid
          and substring(scexh.exchgcd,1,2) = sedol.cntrycd
where
bond.actflag<>'D'
and scmst.isin = ''
and scmst.actflag<>'D'
and scmst.statusflag<>'I'
and issur.actflag<>'D'
and scexh.actflag<>'D'
and scexh.liststatus<>'D'
and (bond.maturitydate is null or bond.maturitydate>getdate())
