

use portfolio
drop table fiddupesedol

use wca
select 
scexh.secid,
scexh.exchgcd,
count(scexh.secid) as exchgsedolct
into portfolio.dbo.fiddupesedol
from scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scexh.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
--inner join scmst on scexh.secid = scmst.secid
--where count(scexh.secid)>1
group by scexh.secid, scexh.exchgcd
order by exchgsedolct desc


-- dupe sedol CA ct for RD in Last year
use wca
select 
portfolio.dbo.fidsecid.secid,
count(portfolio.dbo.fidsecid.secid) as rdct
into portfolio.dbo.fidca
from portfolio.dbo.fidsecid
left outer join rd on portfolio.dbo.fidsecid.secid = rd.secid
where
rd.acttime>'2009/12/20'
group by portfolio.dbo.fidsecid.secid
order by rdct


-- dupe sedol CA ct for RD NOT in Last year
use wca
select 
portfolio.dbo.fidsecid.secid,
count(portfolio.dbo.fidsecid.secid) as rdct
--into portfolio.dbo.fiddupesedol
from portfolio.dbo.fidsecid
left outer join rd on portfolio.dbo.fidsecid.secid = rd.secid
where
(rd.acttime<'2009/12/20' or rd.secid is null)
and portfolio.dbo.fidsecid.secid not in (select secid from portfolio.dbo.fidca)
group by portfolio.dbo.fidsecid.secid
order by rdct


use wca
select distinct portfolio.dbo.fiddupesedol.secid, sedol.sedol, portfolio.dbo.fiddupesedol.exchgcd, portfolio.dbo.fiddupesedol.exchgsedolct
from portfolio.dbo.fiddupesedol
inner join exchg on portfolio.dbo.fiddupesedol.exchgcd = exchg.exchgcd
inner join sedol on portfolio.dbo.fiddupesedol.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
inner join scmst on portfolio.dbo.fiddupesedol.secid = scmst.secid
where
exchgsedolct>1
and scmst.sectycd<>'BND'
and scmst.statusflag<>'I'
order by exchgsedolct desc


use portfolio
select distinct col001,  wca.dbo.shoch.newsos,  wca.dbo.shoch.effectivedate,  wca.dbo.shoch.announcedate from bvdsos
left outer join  wca.dbo.scmst on col001 = wca.dbo.scmst.secid
left outer join  wca.dbo.shoch on  wca.dbo.scmst.secid =  wca.dbo.shoch.secid
where sharesoutstanding <> ''
order by col001


