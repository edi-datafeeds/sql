use wca
SELECT exchg.exchgname, Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join sedol on exchg.cntrycd = sedol.cntrycd
                       and scexh.secid = sedol.secid
where
(sedol.secid is null or sedol.actflag='D')
and scexh.actflag<>'D'
and scexh.liststatus<>'D'
and sectygrp.secgrpid<4
GROUP BY exchg.exchgname order by exchg.exchgname
