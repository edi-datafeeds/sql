select code from portfolio.dbo.momi
left outer join scmst on code = isin
left outer join icc on code = oldisin
where scmst.secid is null
and icc.secid is null


select * from portfolio.dbo.momc
left outer join scmst on code = uscode
left outer join icc on code = olduscode
where scmst.secid is null
and icc.secid is null


use wca
select portfolio.dbo.moms.code from portfolio.dbo.moms
left outer join sedol on code = sedol
left outer join sdchg on code = oldsedol
where sedol.secid is null
and sdchg.secid is null


select isin, sectycd, primaryexchgcd, issuername, securitydesc from portfolio.dbo.momi
left outer join scmst on code = isin
left outer join issur on scmst.issid = issur.issid
where scmst.secid is not null
and scmst.actflag<>'D'
order by sectycd, primaryexchgcd, issuername


select uscode, sectycd, primaryexchgcd, issuername, securitydesc from portfolio.dbo.momc
left outer join scmst on code = uscode
left outer join issur on scmst.issid = issur.issid
where scmst.secid is not null
and scmst.actflag<>'D'
order by sectycd, primaryexchgcd, issuername


select * from portfolio.dbo.moms
left outer join sedol on code = sedol
left outer join sdchg on code = oldsedol
where sedol.secid is null
and sdchg.secid is null

select sedol, sectycd, primaryexchgcd, issuername, securitydesc from portfolio.dbo.moms
left outer join sedol on code = sedol
left outer join scmst on sedol.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
where scmst.secid is not null
and scmst.actflag<>'D'
order by sectycd, primaryexchgcd, issuername


use prices
select liveprices.* from portfolio.dbo.momi
left outer join liveprices on code = isin
left outer join wca.dbo.icc on code = oldisin
where liveprices.id is not null
order by MIC, isin

use prices
select code from portfolio.dbo.momc
left outer join liveprices on code = uscode
where liveprices.id is null

use prices
select code from portfolio.dbo.moms
left outer join liveprices on code = sedol
where liveprices.id is null
