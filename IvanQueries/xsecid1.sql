use portfolio
 drop table xsecid

use wca
select bond.secid
into portfolio.dbo.xsecid
from portfolio.dbo.sandp
inner join scmst on portfolio.dbo.sandp.col001= scmst.isin
inner join bond on scmst.secid = bond.secid
and (maturitydate > getdate() or maturitydate is null)
and bond.actflag<>'D'
and scmst.actflag<>'D'

use portfolio
ALTER TABLE xsecid ALTER COLUMN  secid int NOT NULL

ALTER TABLE [DBO].[xsecid] WITH NOCHECK ADD 
 CONSTRAINT [pk3_xsecid] PRIMARY KEY ([secid])  ON [PRIMARY]


use portfolio
select col001 from morleyfm
inner join smf4.dbo.security on col001 =sedol
where statusflag='D'



use smf4
select opol, statusflag from security
where
SEDOL = '4459040'
or SEDOL = '2963167'
or SEDOL = '2115054'
or SEDOL = '2740081'
or SEDOL = 'B013N08'
or SEDOL = 'B02YC25'
or SEDOL = 'B0393Q0'
or SEDOL = 'B04PX67'
or SEDOL = 'B02Q1D5'
or SEDOL = 'B0BZHN5'
or SEDOL = 'B0WFX69'
or SEDOL = 'B15S4F8'
or SEDOL = 'B0P4V14'
or SEDOL = 'B1GJYR2'
or SEDOL = 'B10S9K3'
or SEDOL = 'B1RRNL2'
or SEDOL = 'B1VG9N9'
or SEDOL = 'B1W8M35'
or SEDOL = 'B1XF5W5'
or SEDOL = 'B28QXT3'
or SEDOL = 'B2N8736'
or SEDOL = 'B0WHNZ4'
or SEDOL = 'B1W7SB2'
or SEDOL = 'B2NKYX1'
or SEDOL = 'B2PQJ10'
or SEDOL = 'B2PRT46'
or SEDOL = 'B2Q14V9'
or SEDOL = 'B05QW51'
or SEDOL = 'B1704V8'
or SEDOL = '2047685'
or SEDOL = 'B2Q2M74'



select distinct opol
from security
where actflag<>'D'
and statusflag<>'D'