use wca
select [entity name(conformed)], issuername from portfolio.dbo.cpl4
left outer join issur on [entity name(conformed)] = issuername

use wca
update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on [entity name(conformed)] = issuername
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null



update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace([entity name(conformed)],' ','') = replace(issuername,' ','')
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null

update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace([entity name(conformed)],'.',''),' ','')
                       = replace(replace(issuername,'.',''),' ','')
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null

update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace([entity name(conformed)],'.',''),',',''),' ','') 
                       = replace(replace(replace(issuername,'.',''),',',''),' ','')
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null

update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace(replace([entity name(conformed)],'.',''),',',''),' plc',' '),' ','')
                       = replace(replace(replace(replace(issuername,'.',''),',',''),' plc',' '),' ','')
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null

update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace([entity name(conformed)],'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'incorporated',' '),' inc ',' '),' ','') 
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'incorporated',' '),' inc ',' '),' ','')
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null

update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(replace([entity name(conformed)],'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),' ','') 
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),' ','') 
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null


update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace([entity name(conformed)],'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),'sicav',''),' ','')  
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),'sicav',''),' ','')  
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null


update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace([entity name(conformed)],'.',' '),',',' '),'limited',' '),'holding',' '),' ltd ',' '),'PT ',' '),' and ',' '),' corp ',' '),' co ',' '),'&',' '),'ASA',' '),char(34),' '),'(holdings)',' '),' ','')
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holding',' '),' ltd ',' '),' hldg ',' '),' and ',' '),' corp ',' '),' co ',' '),'&',' '),' PT',' '),char(34),' '),' ','') 
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null


--
update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace([entity name(conformed)],'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd',' '),' hldg',' '),'the ',' '),' corp',' '),' co',' '),'-',''),' ','')  
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd',' '),' hldg',' '),'the ',' '),' corp',' '),' co',' '),'-',''),' ','')  
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null
--
update portfolio.dbo.cpl4
set portfolio.dbo.cpl4.[EDI Issuer id] = issur.issid, portfolio.dbo.cpl4.[EDI Issuername] = issur.issuername
from portfolio.dbo.cpl4
left outer join issur on replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace([entity name(conformed)],'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),'the ',''),' ','')  
                         = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(issuername,'.',' '),',',' '),'limited',' '),'holdings',' '),' ltd ',' '),' hldg ',' '),'corporation',' '),' corp ',' '),' co ',' '),'the ',''),' ','')  
where issur.issid is not null
and portfolio.dbo.cpl4.[EDI Issuer id] is null



select * from portfolio.dbo.cpl4
where portfolio.dbo.cpl4.[EDI Issuer id] is null