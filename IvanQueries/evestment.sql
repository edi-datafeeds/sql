use wca
--select * from portfolio.dbo.xcode
select distinct
Issuername as [Company Name],
uscode as CUSIP,
code as SEDOL,
ISIN,
localcode as Ticker,
Issuername as [Issue Name],
'' as Price,
exchg.cntrycd as Country,
scmst.curencd as Currency,
exchgname as Market
from portfolio.dbo.xcode
left outer join sedol on sedol = code
left outer join scmst on sedol.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join exchg on sedol.cntrycd = exchg.cntrycd
left outer join scexh on exchg.exchgcd = scexh.exchgcd and scmst.secid = scexh.secid
