use wca
--select bond.secid, bond.actflag from bond
update bond
set bond.acttime='2011/02/28 15:00:00'
--t bond.actflag='D'
from bond
left outer join scmst on bond.secid = scmst.secid
where
bond.actflag<>'D' and scmst.actflag='D'

use wca
select bond.secid, bond.actflag
from bond
left outer join scmst on bond.secid = scmst.secid
where
scmst.secid is null

-- kill bond orphans
use wca
--select bond.secid, bond.actflag from bond
update bond
--set bond.acttime='2011/02/28 15:00:00'
set bond.actflag='D'
from bond
left outer join scmst on bond.secid = scmst.secid
where
scmst.secid is null

use wca
select cpopt.secid, cpopt.actflag
from cpopt
left outer join scmst on cpopt.secid = scmst.secid
where
scmst.secid is null
