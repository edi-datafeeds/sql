-- get keys for deletion
use wca
select eventid as divid, rdid
into divdel
from v54f_620_dividend
where
paydate < '2011/01/01' and paydate is not null

-- delete divpy
use wca
delete from divpy
where
divid in (select divid from divdel)
--order by acttime desc

-- delete div
use wca
delete from div
where
divid in (select divid from divdel)


-- delete exdt
use wca
delete from exdt
where
paydate < '2011/01/01' and paydate is not null
and eventtype='DIV'

-- delete lstat
use wca delete from lstat where acttime < '2011/01/01'
use wca delete from nlist where acttime < '2011/01/01'

use wca delete from agm where acttime < '2011/01/01'
use wca delete from ischg where acttime < '2011/01/01'
use wca delete from sdchg where acttime < '2011/01/01'
use wca delete from scchg where acttime < '2011/01/01'

use wca delete from ann where acttime < '2011/01/01'
use wca delete from lcc where acttime < '2011/01/01'
use wca delete from icc where acttime < '2011/01/01'

use wca delete from pvrd where acttime < '2011/01/01'

