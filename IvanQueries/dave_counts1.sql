use wca
select 'Municipal' as 'Type', count(bond.secid) as 'Count' from bond
inner join scmst on bond.secid = scmst.secid
where
municipal='Y'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
union
select 'Security Charge' as 'Type', count(bond.secid) as 'Count' from bond
inner join scmst on bond.secid = scmst.secid
where
(securitycharge='AMB' or securitycharge='MB')
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
union
select 'Maturity Structure' as 'Type', count(bond.secid) as 'Count' from bond
inner join scmst on bond.secid = scmst.secid
where
(MaturityStructure='C' or MaturityStructure='S' or MaturityStructure='V')
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
union
select 'Government' as 'Type', count(bond.secid) as 'Count' from bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
isstype='GOV'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
union
select 'Government Agency' as 'Type', count(bond.secid) as 'Count' from bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
isstype='GOVAGENCY'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
union
select 'Corporate' as 'Type', count(bond.secid) as 'Count' from bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
isstype=''
and cntryofincorp<>'AA'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
union
select 'Supranational' as 'Type', count(bond.secid) as 'Count' from bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
cntryofincorp='AA'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
