select distinct
wca.dbo.scmst.isin
from wca.dbo.issur
inner join wca.dbo.scmst on wca.dbo.issur.issid = wca.dbo.scmst.issid
inner join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid
left outer join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
where (wca.dbo.exchg.cntrycd = 'JP' or (wca.dbo.exchg.cntrycd is null and substring(wca.dbo.scmst.isin,1,2)='JP')
-- or wca.dbo.issur.cntryofincorp ='JP'
)
and wca.dbo.scmst.actflag <> 'd'
and wca.dbo.scmst.statusflag <> 'i'
order by sectycd, scexh.exchgcd
