use wca
select col001, scmst.isin, cowar.acttime, unratio, warrantratio from portfolio.dbo.rmsecid
inner join scmst on portfolio.dbo.rmsecid.col001 = scmst.secid
left outer join cowar on portfolio.dbo.rmsecid.col001 = cowar.secid
where statusflag <> 'I'
and sectycd = 'CW'
and (unratio is null or unratio='')

use wca
select col001, scmst.isin, warex.acttime, ratioold, rationew
--, unratio, warrantratio 
from portfolio.dbo.rmsecid
inner join scmst on portfolio.dbo.rmsecid.col001 = scmst.secid
left outer join warex on portfolio.dbo.rmsecid.col001 = warex.secid
where statusflag <> 'I'
and sectycd = 'war'
and ratioold is not null
and ratioold <> ''