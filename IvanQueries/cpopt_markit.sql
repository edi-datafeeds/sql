use wca
select
cpopt.secid,
cptype,
case when (select count(secid) from cpopt where cptype = 'EU' and cpopt.actflag <> 'D' GROUP BY cpopt.secid)>1 then 'BM' else 'EU' end as cptype,
markit_eu.ct>1 then 'BM' else 'EU' end as cptype
from cpopt
left outer join markit_eu on cpopt.secid = markit_eu.secid
where cptype = 'EU'
--and markit_eu.ct>1
and cpopt.actflag <> 'D'

use wca
select cpopt.secid, count(secid) as ct
into markit_eu
from cpopt
where cptype = 'EU'
and cpopt.actflag <> 'D'
GROUP BY cpopt.secid
order by count(secid)

use wca
select
cpopt.secid,
cptype,
case when markit_eu.ct>1 then 'BM' else 'EU' end as cptype
from cpopt
left outer join markit_eu on cpopt.secid = markit_eu.secid
where cptype = 'EU'
and markit_eu.ct>1
and cpopt.actflag <> 'D'


select * from markit_eu
where ct > 1
