use WCA
SELECT distinct
drtype
FROM scmst
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid

WHERE 
scmst.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and scmst.sectycd = 'DR'
and (exchg.cntrycd = 'AT'
or exchg.cntrycd = 'BE'
or exchg.cntrycd = 'BG'
or exchg.cntrycd = 'CH'
or exchg.cntrycd = 'CY'
or exchg.cntrycd = 'CZ'
or exchg.cntrycd = 'DE'
or exchg.cntrycd = 'DK'
or exchg.cntrycd = 'EE'
or exchg.cntrycd = 'ES'
or exchg.cntrycd = 'FI'
or exchg.cntrycd = 'FR'
or exchg.cntrycd = 'GB'
or exchg.cntrycd = 'GR'
or exchg.cntrycd = 'HU'
or exchg.cntrycd = 'IE'
or exchg.cntrycd = 'IS'
or exchg.cntrycd = 'IT'
or exchg.cntrycd = 'LT'
or exchg.cntrycd = 'LU'
or exchg.cntrycd = 'LV'
or exchg.cntrycd = 'MT'
or exchg.cntrycd = 'NL'
or exchg.cntrycd = 'NO'
or exchg.cntrycd = 'PL'
or exchg.cntrycd = 'PT'
or exchg.cntrycd = 'RO'
or exchg.cntrycd = 'SE'
or exchg.cntrycd = 'SI'
or exchg.cntrycd = 'SK')
