use wca
select localcode as ETF_Localcode
from scexh
inner join scmst on scexh.secid = scmst.secid
where
sectycd = 'ETF'
and scexh.liststatus<>'D'
and statusflag <> 'I'
and scexh.actflag<>'D'
and localcode<>''