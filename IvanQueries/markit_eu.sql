print " GENERATING portfolio.dbo.markit_eu, please wait..."
go

use portfolio
if exists (select * from sysobjects where name = 'markit_eu')
 drop table markit_eu
use wca
select cpopt.secid, count(secid) as ct
into portfolio.dbo.markit_eu
from cpopt
where cptype = 'EU'
and cpopt.actflag <> 'D'
GROUP BY cpopt.secid
order by count(secid)
go

print ""
go

print " INDEXING portfolio.dbo.markit_eu ,please  wait ....."
GO 
use portfolio
ALTER TABLE markit_eu ALTER COLUMN  secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[markit_eu] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_markit_eu] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 
