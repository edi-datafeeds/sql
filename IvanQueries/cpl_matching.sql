--update values
use wca
update portfolio.dbo.cpl4 
set [edi issuer id]=issur.IssID, [edi local code]=scexh.localcode,  [edi issuername]=issur.issuername
from portfolio.dbo.cpl4 as cpl
left outer join portfolio.dbo.cplmap on cpl.[primary listing exchange] = portfolio.dbo.cplmap.primaryexchange
left outer join scexh on portfolio.dbo.cplmap.exchgcd = scexh.exchgcd
                           and cpl.[primary listing ticker] = localcode
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
where
issur.issid is not null

select * from portfolio.dbo.cplmap.primaryexchange



use wca
select distinct
[cpl number],
cpl.[primary listing ticker],
[entity name(Conformed)],
issur.IssID,
scmst.SecID,
scmst.Isin,
Issuername,
CntryofIncorp,
SecurityDesc,
PrimaryExchgCD,
scexh.ExchgCD,
Localcode
from portfolio.dbo.cpl4 as cpl
left outer join portfolio.dbo.cplmap on cpl.[primary listing exchange] = portfolio.dbo.cplmap.primaryexchange
left outer join scexh on portfolio.dbo.cplmap.exchgcd = scexh.exchgcd
                           and cpl.[primary listing ticker] = localcode
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
where
issur.issid is not null
order by issuername

select * from portfolio.dbo.cpl4
where
[edi issuer id] is not null
and [edi issuername]<>[entity name(conformed)]

ORDER by [edi issuer id]




use wca
select [primary listing ticker], [entity name(conformed)], issuername, [primary listing exchange], scexh.exchgcd
from portfolio.dbo.cpl4 as cpl
left outer join portfolio.dbo.cplmap on cpl.[primary listing exchange] = portfolio.dbo.cplmap.primaryexchange
left outer join scexh on portfolio.dbo.cplmap.exchgcd = scexh.exchgcd
                           and cpl.[primary listing ticker] = localcode
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
where
issur.issid is not null


use wca
select * from scmst
where issid = 34122

select * from scexh
where secid = 37666