use portfolio
select * from digi_ut

select * from

use portfolio
select distinct digi_ut.isin --, digi_ut.paydate, digi_ut.exdate,  digi_ut.grp1amt, digi_ut.grp2amy as grp2amt
into ut_um_isin
from digi_ut
left outer join temp701
on digi_ut.isin = temp701.isin

where 
temp701.isin is null
and digi_ut.isin not in (select isin from smfisin)


select wca.dbo.issur.issuername,wca.dbo.scmst.securitydesc, ut_um_isin.isin, wca.dbo.scmst.statusflag, wca.dbo.scmst.sectycd  from ut_um_isin
left outer join wca.dbo.scmst on ut_um_isin.isin = wca.dbo.scmst.isin
left outer join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
--left outer join wca.dbo.rd on wca.dbo.scmst.secid = wca.dbo.rd.secid
where 
(wca.dbo.scmst.statusflag<>'I'
or wca.dbo.scmst.statusflag is null)
--and wca.dbo.rd.recdate > '2007/01/01'
order by wca.dbo.scmst.sectycd

use portfolio
select distinct isin 
into temp701
from wca.dbo.v54f_701_dividend
where 
(wca.dbo.v54f_701_dividend.paydate>'2008/04/06' 
and wca.dbo.v54f_701_dividend.paydate<='2009/04/05'
or wca.dbo.v54f_701_dividend.exdate>'2008/01/01')

and isin not in (select isin from smfisin)


--and digi_ut.isin not in (select isin from smfisin)


select * from smfisin

use portfolio
select digi_eq.isin, digi_eq.paydate, wca.dbo.v54f_620_dividend.isin from digi_eq
left outer join wca.dbo.v54f_620_dividend
on digi_eq.isin = wca.dbo.v54f_620_dividend.isin
and digi_eq.paydate = convert(varchar, wca.dbo.v54f_620_dividend.paydate,103)
where 
wca.dbo.v54f_620_dividend.isin is null
and len(digi_eq.isin)=12

select count(*) from digi_ut


-- Non-GB isins
select  distinct ut_um1.isin, smf4.dbo.security.statusflag, smf4.dbo.security.longdesc from ut_um1
left outer join wca.dbo.scmst on ut_um1.isin = wca.dbo.scmst.isin
left outer join wca.dbo.icc on ut_um1.isin = wca.dbo.icc.oldisin
left outer join smf4.dbo.security on ut_um1.isin = smf4.dbo.security.isin
where
wca.dbo.scmst.secid is null
and wca.dbo.icc.secid is null
and smf4.dbo.security.securityid is null

-- Genuine GB isins
select  distinct ut_um1.isin, smf4.dbo.security.statusflag, smf4.dbo.security.longdesc from ut_um1
left outer join wca.dbo.scmst on ut_um1.isin = wca.dbo.scmst.isin
left outer join wca.dbo.icc on ut_um1.isin = wca.dbo.icc.oldisin
left outer join smf4.dbo.security on ut_um1.isin = smf4.dbo.security.isin
where
wca.dbo.scmst.secid is null
and wca.dbo.icc.secid is null
and smf4.dbo.security.securityid is not null


select  distinct ut_um1.isin
from ut_um1
left outer join wca.dbo.scmst on ut_um1.isin = wca.dbo.scmst.isin
left outer join wca.dbo.icc on ut_um1.isin = wca.dbo.icc.oldisin
left outer join smf4.dbo.security on ut_um1.isin = smf4.dbo.security.isin
where
wca.dbo.scmst.secid is null
and wca.dbo.icc.secid is null
and smf4.dbo.security.securityid is null

-- Genuine GB isins
select  distinct ut_um1.isin
from ut_um1
left outer join wca.dbo.scmst on ut_um1.isin = wca.dbo.scmst.isin
left outer join wca.dbo.icc on ut_um1.isin = wca.dbo.icc.oldisin
left outer join smf4.dbo.security on ut_um1.isin = smf4.dbo.security.isin
where
wca.dbo.scmst.secid is null
and wca.dbo.icc.secid is null
and smf4.dbo.security.securityid is not null


-- matched via ICC
select  ut_um2.*  
into ut_um3
from ut_um2
left outer join wca.dbo.icc on ut_um2.isin = wca.dbo.icc.oldisin
left outer join wca.dbo.v54f_701_dividend
on wca.dbo.icc.secid = wca.dbo.v54f_701_dividend.secid
and ut_um2.paydate = convert(varchar, wca.dbo.v54f_701_dividend.paydate,103)
where
wca.dbo.v54f_701_dividend.isin is null

select ut_um3.* 
into ut_um4
from ut_um3
left outer join wca.dbo.scmst on ut_um3.isin = wca.dbo.scmst.isin
left outer join smf4.dbo.security on substring(ut_um3.isin,5,7) = smf4.dbo.security.sedol
where (grp1amt<>'0' or grp2amy<>'0')
and substring(paydate,7,4)<>'2006'
and not (substring(paydate,4,2)<'04' and substring(paydate,7,4)='2007')
and wca.dbo.scmst.statusflag<>'I'
and smf4.dbo.security.statusflag='T'

select * from ut_um4

select 

use portfolio
select distinct isin 
into ddigi_ut
from digi_ut
where isin is not null
and digi_ut.isin not in (select isin from smfisin)

select *
from ddigi_ut
left outer join wca.dbo.v54f_701_dividend
on ddigi_ut.isin = wca.dbo.v54f_701_dividend.isin
where 
wca.dbo.v54f_701_dividend.isin is not null
and wca.dbo.v54f_701_dividend.paydate>'2008/04/06'

select *
from wca.dbo.v54f_701_dividend
where 
--wca.dbo.v54f_701_dividend.isin is not null
--and 
wca.dbo.v54f_701_dividend.paydate>'2008/04/06'

select ddigi_ut.*, wca.dbo.scmst.statusflag, wca.dbo.icc.secid, wca.dbo.icc.newisin
from ddigi_ut
left outer join wca.dbo.v54f_701_dividend
      on ddigi_ut.isin = wca.dbo.v54f_701_dividend.isin
      and wca.dbo.v54f_701_dividend.paydate>'2008/04/06'
left outer join wca.dbo.scmst on ddigi_ut.isin = wca.dbo.scmst.isin
left outer join wca.dbo.icc on ddigi_ut.isin = wca.dbo.icc.oldisin
where 
wca.dbo.v54f_701_dividend.isin is null
and wca.dbo.scmst.statusflag<>'I'



use wca
select * from wca.dbo.v54f_701_dividend
where nildividend = 'F'