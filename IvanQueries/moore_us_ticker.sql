use wca
select distinct col001 as USTicker, issuername, securitydesc, isin, uscode from portfolio.dbo.us_ticker_list
left outer join scexh on col001=localcode
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
and substring(exchgcd,1,2)='US'
where
scmst.secid is  null
