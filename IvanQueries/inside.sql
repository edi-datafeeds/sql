use wca
select col001, scmst.statusflag, sedol.sedol, unitofqcurrcode, opol from portfolio.dbo.inside
left outer join scmst on col001 = uscode
left outer join sedol on scmst.secid = sedol.secid
left outer join smf4.dbo.security on sedol.sedol = smf4.dbo.security.sedol
where
scmst.secid is not null and sedol.secid is not null

