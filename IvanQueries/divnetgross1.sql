use smf4
select distinct
sedol as sedol1, paydate as paydate1, taxrateind, 
case when divtype = 'special' then 'S' else 'X' end as divtype
into wca.dbo.divnetgross1
from edicadetail
inner join security on edicadetail.securityid = security.securityid
where paydate>='2008/04/06' and paydate<'2009/04/07'
and paydate is not null
and (taxrateind='N' or taxrateind='G')
and edicadetail.actflag<>'D'

select * from 
