delete markit_secid

use portfolio
insert into markit_secid
select distinct secid
from gbp_corp
left outer join wca.dbo.scmst on gbp_corp.col001 = wca.dbo.scmst.isin
where secid is not null


delete markit_secid
where secid is null

use portfolio
select * from euro_sov