use wca
if exists (select * from sysobjects where name = 'SHOCHSEQ')
	drop table SHOCHSEQ

use wca
select distinct
div.announcedate as effectivedate,
scexh.secid,
1 as seqnum,
rd.rdid as shochid
into shochseq
from scexh
inner join rd on scexh.secid = rd.secid
inner join div on rd.rdid = div.rdid
where substring(exchgcd,1,2)='FR'
and scexh.actflag<>'D'
and div.actflag<>'D'
and rd.actflag<>'D'





select * from v54f_620_dividend
where rdid in (select eventid from shochseq where seqnum<4)
and excountry = 'FR'
and liststatus<>'D'

select * from shochseq



use wca
if exists (select * from sysobjects where name = 'SHOCHSEQ')
	drop table SHOCHSEQ
go

select 
SHOCH.secid,
v54f_620_dividend.eventid,
1 as seqnum,
v54f_620_dividend.caref
into SHOCHSEQ
from v54f_620_dividend
where v54f_620_dividend.actflag<>'D'
and excountry='FR'
go

