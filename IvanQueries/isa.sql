use smf4
select SecurityID, Actdate as Acttime, Actflag, ' ' as Isaflag
into isa
from security
where
statusflag<>'D'
and actflag<>'D'

use smf4
select count(securityid) from ISA
where isaflag = 'Y'


-- new records

select * from isa
left outer join previsa on isa.securityid = previsa.securityid
where previsa.securityid is null

-- newly defunct records
select * from previsa
left outer join isa on previsa.securityid = isa.securityid
where isa.securityid is null

select 
'ISA CHANGES' as Reason,
sedol,
isa.isaflag as NewISAflag,
previsa.isaflag as OldISAFlag,
issuername,
longdesc,
statusflag
from isa
inner join previsa on isa.securityid = previsa.securityid
left outer join security on isa.securityid = security.securityid
left outer join issuer on security.issuerid = issuer.issuerid
where
previsa.isaflag<>isa.isaflag

union

select 
'ISA ADDITIONS' as Reason,
sedol,
isa.isaflag as NewISAflag,
'' as OldISAFlag,
issuername,
longdesc,
statusflag
from isa
left outer join previsa on isa.securityid = previsa.securityid
left outer join security on isa.securityid = security.securityid
left outer join issuer on security.issuerid = issuer.issuerid
where
previsa.securityID is null

union

select 
'ISA DELETIONS' as Reason,
sedol,
'' as NewISAflag,
previsa.isaflag as OldISAFlag,
issuername,
longdesc,
statusflag
from previsa
left outer join isa on previsa.securityid = isa.securityid
left outer join security on previsa.securityid = security.securityid
left outer join issuer on security.issuerid = issuer.issuerid
where
isa.securityID is null


select * from security
where securityid=86279


