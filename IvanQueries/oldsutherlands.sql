use portfolio
if exists (select * from sysobjects where name = 'CPOPT_ct')
 drop table CPOPT_ct
use wca
select cpopt.secid, count(secid) as ct
into portfolio.dbo.CPOPT_ct
from cpopt
where
cpopt.actflag <> 'D'
and cptype<>'KO'
GROUP BY cpopt.secid
order by count(secid)
go


print " INDEXING portfolio.dbo.CPOPT_ct ,please  wait ....."
GO 
use portfolio
ALTER TABLE CPOPT_ct ALTER COLUMN  secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_ct] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_ct] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 


use portfolio
if exists (select * from sysobjects where name = 'CPOPT_EU_TODAY')
 drop table CPOPT_EU_TODAY
use wca
select
cpopt.secid,
fromdate as Next_Call_Date
into portfolio.dbo.CPOPT_EU_TODAY
from portfolio.dbo.cpopt_ct
inner join cpopt on portfolio.dbo.CPOPT_Ct.SecID = cpopt.secid
where
cptype<>'KO'
and cpopt.fromdate between getdate()-1 and getdate()
and (cpopt.fromdate = cpopt.todate or cpopt.todate is null)
and portfolio.dbo.CPOPT_Ct.Ct = 1
and cpopt.actflag<>'D'

go

use portfolio
ALTER TABLE CPOPT_EU_TODAY ALTER COLUMN secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_EU_TODAY] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_EU_TODAY] PRIMARY KEY ([secid]) ON [PRIMARY]
GO 


use portfolio
if exists (select * from sysobjects where name = 'CPOPT_EU_FUTURE')
 drop table CPOPT_EU_FUTURE
use wca
select
cpopt.secid,
fromdate as Next_Call_Date
into portfolio.dbo.CPOPT_EU_FUTURE
from portfolio.dbo.cpopt_ct
inner join cpopt on portfolio.dbo.CPOPT_Ct.SecID = cpopt.secid
where
cptype<>'KO'
and cpopt.fromdate>getdate()
and (cpopt.fromdate = cpopt.todate or cpopt.todate is null)
and portfolio.dbo.CPOPT_Ct.Ct = 1
and cpopt.actflag<>'D'

go

use portfolio
ALTER TABLE CPOPT_EU_FUTURE ALTER COLUMN secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_EU_FUTURE] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_EU_FUTURE] PRIMARY KEY ([secid]) ON [PRIMARY]
GO 





use portfolio
if exists (select * from sysobjects where name = 'CPOPT_KO_ct')
 drop table CPOPT_KO_ct
use wca
select cpopt.secid, count(secid) as ct
into portfolio.dbo.CPOPT_KO_ct
from cpopt
where
cpopt.actflag <> 'D'
and cptype='KO'
GROUP BY cpopt.secid
order by count(secid)
go


use portfolio
ALTER TABLE CPOPT_KO_ct ALTER COLUMN  secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_KO_ct] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_KO_ct] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 


use portfolio
if exists (select * from sysobjects where name = 'CPOPT_EUK_FUTURE')
 drop table CPOPT_EUK_FUTURE
use wca
select
cpopt.secid,
fromdate as Next_Call_Date
into portfolio.dbo.CPOPT_EUK_FUTURE
from portfolio.dbo.cpopt_KO_ct
inner join cpopt on portfolio.dbo.cpopt_KO_ct.SecID = cpopt.secid
where
cptype='KO'
and cpopt.fromdate>getdate()
and (cpopt.fromdate = cpopt.todate or cpopt.todate is null)
and portfolio.dbo.cpopt_KO_ct.Ct = 1
and cpopt.actflag<>'D'

go

use portfolio
ALTER TABLE CPOPT_EUK_FUTURE ALTER COLUMN secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_EUK_FUTURE] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_EUK_FUTURE] PRIMARY KEY ([secid]) ON [PRIMARY]
GO 

use portfolio
if exists (select * from sysobjects where name = 'CPOPT_BM_FUTURE')
 drop table CPOPT_BM_FUTURE
use wca
select distinct
cpopt.secid,
cpopt.cpoptid,
fromdate as Next_Call_Date
--into portfolio.dbo.CPOPT_BM_FUTURE
from portfolio.dbo.cpopt_ct
inner join cpopt on portfolio.dbo.cpopt_ct.SecID = cpopt.secid
where
cptype<>'KO'
and cpopt.fromdate>getdate()
and (cpopt.fromdate = cpopt.todate or cpopt.todate is null)
and portfolio.dbo.cpopt_ct.Ct > 1
and cpopt.actflag<>'D'
and cpopt.cpoptid exists in (select top 1 cpoptid from wca.dbo.cpopt where fromdate>getdate() order by fromdate)

go

use portfolio
ALTER TABLE CPOPT_BM_FUTURE ALTER COLUMN secid int NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_BM_FUTURE] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_BM_FUTURE] PRIMARY KEY ([secid]) ON [PRIMARY]
GO 

use portfolio
select distinct * from CPOPT_BM_FUTURE


