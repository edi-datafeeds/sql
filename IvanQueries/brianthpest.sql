use wca
select
col001,
scmst.sectycd,
grpname, 
case when statusflag = '' then 'A' else statusflag end as Statusflag,
primaryexchgcd
from portfolio.dbo.schroders
left outer join sedol on col001 = sedol
left outer join sdchg on col001 = oldsedol
left outer join scmst on sedol.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
where oldsedol is null
and sedol.secid is not null
order by scmst.sectycd, statusflag, primaryexchgCD, col001