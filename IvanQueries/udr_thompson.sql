use udr
select distinct 
issuername,
unscmst.isin as unISIN,
wca.dbo.scmst.isin as drISIN,
ratio
from udrnew
inner join wca.dbo.scexh on udrnew.secid = wca.dbo.scexh.secid
inner join wca.dbo.scmst on wca.dbo.scexh.secid = wca.dbo.scmst.secid
left outer join wca.dbo.scmst as unscmst on unsecid = unscmst.secid
where
(drtype = 'ADR'
or DRtype = 'GDR')
--and wca.dbo.scmst.isin <>''
and (substring(wca.dbo.scexh.exchgcd,1,2)='US'
or substring(wca.dbo.scexh.exchgcd,1,2)='CA')
order by issuername, wca.dbo.scmst.isin



use wca
select * from dprcp
where unsecid is null

select
issuername,
securitydesc,
isin
from scmst
inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
left outer join dprcp on scmst.secid = dprcp.secid
where
sectycd = 'DR'
and dprcp.secid is null
and statusflag <>'I'