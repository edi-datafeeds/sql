use wca
select distinct 
portfolio.dbo.bre2.*,
case when v54f_620_dividend.paydate is not null then 'x'
     when newsed.paydate is not null then 'x'
     when v54f_920_Interest_payment.paydate is not null then 'x'
     else '' end as iso,
case when v54f_620_dividend.paydate is not null then 'x'
     when v54f_920_Interest_payment.paydate is not null then 'x'
     when v54f_701_dividend.paydate is not null then 'x'
     else '' end as edi,
case when v54f_620_dividend.paydate is not null then 'x'
     when v54f_920_Interest_payment.paydate is not null then 'x'
     when v54f_701_dividend.paydate is not null then 'x'
     else '' end as web,
case when sdchg.newsedol is not null then 'matched on successor sedol' end as EDICOMMENT
from portfolio.dbo.bre2
left outer join v54f_620_dividend on portfolio.dbo.bre2.sedol = v54f_620_dividend.sedol
                    and portfolio.dbo.bre2.paydate = v54f_620_dividend.paydate
left outer join sdchg on portfolio.dbo.bre2.sedol = sdchg.oldsedol
left outer join v54f_620_dividend as newsed on sdchg.newsedol = newsed.sedol
                    and portfolio.dbo.bre2.paydate = newsed.paydate
left outer join v54f_701_dividend on portfolio.dbo.bre2.sedol = v54f_701_dividend.sedol
                      and portfolio.dbo.bre2.paydate = v54f_701_dividend.paydate
left outer join v54f_920_Interest_payment on portfolio.dbo.bre2.sedol = v54f_920_Interest_payment.sedol
                      and portfolio.dbo.bre2.paydate = v54f_920_Interest_payment.paydate
where 
v54f_620_dividend.paydate is null
and sdchg.newsedol is null
and v54f_701_dividend.paydate is null
and v54f_920_Interest_payment.paydate is null