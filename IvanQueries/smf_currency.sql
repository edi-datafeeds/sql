use smf4
select * from security
inner join security as sec2 on security.isin = sec2.isin
   and security.opol = sec2.opol
where
security.isin <>''
and security.opol<>''
and security.isin is not null
and security.securityid<>sec2.securityid
and security.cregcode=sec2.cregcode