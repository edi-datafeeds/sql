use WCA
SELECT
'Security Name' as Data_Change,
scmst.isin,
scmst.x,
SCCHG.SecOldName as Previous_Value,
SCCHG.SecNewName as Current_Value,
SCCHG.DateofChange as Effective_Date,
case when RELEVENT.Lookup is null then SCCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Related_Event,
case when SCCHG.Actflag='D' then 'DELETE' when SCCHG.Actflag='I' then 'INSERT' ELSE 'UPDATE' end as Record_Status,
SCCHG.Acttime as Changed
from SCCHG 
INNER JOIN SCMST ON SCCHG.SecID = SCMST.SecID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
left outer join wca3.dbo.tbl_LOOKUP as RELEVENT on SCCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
WHERE
(maturitydate > getdate()-1 or maturitydate is null)
and SCCHG.actflag<>'D'
and SecOldName <> SecNewName


select distinct scmst.secid, issuername, securitydesc from bond
INNER JOIN SCMST ON bond.SecID = SCMST.SecID
INNER JOIN issur ON scmst.issID = issur.issID
where (isin is null or isin='')
and (maturitydate>getdate()-1 or maturitydate is null)
and scmst.actflag<>'D'
and bond.actflag<>'D'
and (statusflag<>'I' or statusflag is null)



inner join scms