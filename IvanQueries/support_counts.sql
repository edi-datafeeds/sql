use wca
select * from v54f_620_subdivision
where
exdate is null
and changed>'2010/01/01'

use wca
select top 10 * from v54f_620_subdivision

use wca
select
excountry,
count(excountry) as count_2001
from v54f_620_subdivision
group by excountry
--order by  byexcountry desc

use wca
select
excountry,
count(excountry) as count_2001
into ctfs_null
from v54f_620_subdivision
where exdate is null
group by excountry


use wca
select
excountry,
count(excountry) as count_2001
into ctrs_null
from v54f_620_consolidation
where exdate is null
group by excountry

select
ctfs.excountry as Exchange_Country,
ctfs.count_2001 as listing_total_count,
case when ctfs_null.count_2001 is null then 0 else ctfs_null.count_2001 end as with_blank_exdate
from ctfs
left outer join ctfs_null on ctfs.excountry = ctfs_null.excountry

select
ctrs.excountry as Exchange_Country,
ctrs.count_2001 as listing_total_count,
case when ctrs_null.count_2001 is null then 0 else ctrs_null.count_2001 end as with_blank_exdate
from ctrs
left outer join ctrs_null on ctrs.excountry = ctrs_null.excountry

use wca
select * from scmst
where
isin = 'it0004610165'