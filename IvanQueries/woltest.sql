use wol
select 
cab.displaydate,
cab.cabepoch,
cab.cabedition,
cab.sitcd,
Isin,
Title,
Settlement,
EventCD,
Eventdate,
EventDesc
from dir
inner join cab on dir.cabepoch = cab.cabepoch
          and dir.cabedition = cab.cabedition
          and dir.psecid = cab.psecid
inner join sec on dir.cabepoch = sec.cabepoch
          and dir.cabedition = sec.cabedition
          and dir.psecid = sec.psecid
where eventdate is not null
