SELECT cntry.country,
Count(bond.secid) as muni_ct
into portfolio.dbo.ct1
from cntry
inner JOIN issur on cntry.cntryCD = issur.Cntryofincorp
inner join scmst on issur.issid = scmst.issid
INNER JOIN bond  ON scmst.secid =  bond.secid
where
municipal='Y'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
GROUP BY cntry.country order by cntry.country

SELECT cntry.country,
Count(bond.secid) as AMD_MB_ct
into portfolio.dbo.ct2
from cntry
inner JOIN issur on cntry.cntryCD = issur.Cntryofincorp
inner join scmst on issur.issid = scmst.issid
INNER JOIN bond  ON scmst.secid =  bond.secid
where
(securitycharge='AMB' or securitycharge='MB')
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
GROUP BY cntry.country order by cntry.country

SELECT cntry.country,
Count(bond.secid) as Convertible_ct
into portfolio.dbo.ct3
from cntry
inner JOIN issur on cntry.cntryCD = issur.Cntryofincorp
inner join scmst on issur.issid = scmst.issid
INNER JOIN bond  ON scmst.secid =  bond.secid
where
(MaturityStructure='C' or MaturityStructure='S' or MaturityStructure='V')
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
GROUP BY cntry.country order by cntry.country

SELECT cntry.country,
Count(bond.secid) as GOV_ct
into portfolio.dbo.ct4
from cntry
inner JOIN issur on cntry.cntryCD = issur.Cntryofincorp
inner join scmst on issur.issid = scmst.issid
INNER JOIN bond  ON scmst.secid =  bond.secid
where
isstype='GOV'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
GROUP BY cntry.country order by cntry.country

SELECT cntry.country,
Count(bond.secid) as GOVAGENCY_ct
into portfolio.dbo.ct5
from cntry
inner JOIN issur on cntry.cntryCD = issur.Cntryofincorp
inner join scmst on issur.issid = scmst.issid
INNER JOIN bond  ON scmst.secid =  bond.secid
where
isstype='GOVAGENCY'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
GROUP BY cntry.country order by cntry.country

SELECT cntry.country,
Count(bond.secid) as CORPORATE_ct
into portfolio.dbo.ct6
from cntry
inner JOIN issur on cntry.cntryCD = issur.Cntryofincorp
inner join scmst on issur.issid = scmst.issid
INNER JOIN bond  ON scmst.secid =  bond.secid
where
isstype=''
and cntryofincorp<>'AA'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
GROUP BY cntry.country order by cntry.country

SELECT cntry.country,
Count(bond.secid) as SUPRANAT_ct
into portfolio.dbo.ct7
from cntry
inner JOIN issur on cntry.cntryCD = issur.Cntryofincorp
inner join scmst on issur.issid = scmst.issid
INNER JOIN bond  ON scmst.secid =  bond.secid
where
cntryofincorp='AA'
and bond.actflag<>'D'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate()-1)
GROUP BY cntry.country order by cntry.country

use portfolio
select 
cntrylist.country,
muni_ct,
AMD_MB_ct,
Convertible_ct,
GOV_ct,
GOVAGENCY_ct,
CORPORATE_ct,
SUPRANAT_ct
from cntrylist
left outer join ct1 on cntrylist.country = ct1.country
left outer join ct2 on cntrylist.country = ct2.country
left outer join ct3 on cntrylist.country = ct3.country
left outer join ct4 on cntrylist.country = ct4.country
left outer join ct5 on cntrylist.country = ct5.country
left outer join ct6 on cntrylist.country = ct6.country
left outer join ct7 on cntrylist.country = ct7.country
where
muni_ct is not null
or AMD_MB_ct is not null
or Convertible_ct is not null
or GOV_ct is not null
or GOVAGENCY_ct is not null
or CORPORATE_ct is not null
or SUPRANAT_ct  is not null
