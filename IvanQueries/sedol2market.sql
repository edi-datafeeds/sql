use wca
select distinct scexh.exchgcd, exchg.exchgname from portfolio.dbo.pam
left outer join sedol on pricekey = sedol
left outer join scexh on sedol.secid = scexh.secid
                 and sedol.cntrycd = substring(scexh.exchgcd,1,2)
left outer join exchg on scexh.exchgcd = exchg.exchgcd
