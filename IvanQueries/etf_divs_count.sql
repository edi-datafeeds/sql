use wca
select * from scmst
where isin = 'dk0060083566'


use wca
select * from v54f_620_dividend
where
isin = 'FR0010407197'

isin in (select code from portfolio.dbo.xcode)

drop table portfolio.dbo.etfcts

use wca
select 
isin,
count(distinct eventid) as total_dividends
into portfolio.dbo.etfcts
from v54f_620_dividend
where
isin in (select code from portfolio.dbo.xcode)
group by isin


use portfolio
select distinct isin from portfolio.dbo.etfcts
where total_dividends>1


use wca
select top 1
isin,
recdate
into portfolio.dbo.etffirstdiv
from v54f_620_dividend
where
isin in (select code from portfolio.dbo.xcode)
order by isin, recdate

use portfolio
delete portfolio.dbo.etffirstdiv

select distinct * from portfolio.dbo.etffirstdiv

DECLARE MyCursor CURSOR LOCAL FORWARD_ONLY FOR
  SELECT isin, created, recdate FROM wca.dbo.v54f_620_dividend
where
isin in (select code from portfolio.dbo.xcode)
 order by isin, created

OPEN MyCursor
DECLARE @tisin char(12)
DECLARE @sisin char(12)
DECLARE @tcreated datetime
DECLARE @trecdate datetime
FETCH NEXT FROM MyCursor INTO @tisin, @tcreated, @trecdate
SET @tisin =  ''
WHILE @@FETCH_STATUS = 0
BEGIN
   IF @sisin <> @tisin
   BEGIN 
      insert into portfolio.dbo.etffirstdiv
      select isin, created, recdate from wca.dbo.v54f_620_dividend
      WHERE wca.dbo.v54f_620_dividend.isin = @tisin 
           and wca.dbo.v54f_620_dividend.created=@tcreated
           and wca.dbo.v54f_620_dividend.recdate=@trecdate
   END
   SET @sisin = @tisin
   FETCH NEXT FROM MyCursor INTO @tisin, @tcreated, @trecdate
END
CLOSE MyCursor
DEALLOCATE MyCursor
GO

use wca
select distinct isin, recdate from v54f_620_dividend
where
isin in (select code from portfolio.dbo.xcode)
 order by isin, recdate


select e



use portfolio
select distinct wca.dbo.scmst.secid, xcode.code as isin, total_dividends, created as first_div_created, recdate as first_div_recdate
from xcode
left outer join wca.dbo.scmst on code = wca.dbo.scmst.isin
left outer join etfcts on xcode.code = etfcts.isin
left outer join etffirstdiv on etfcts.isin = etffirstdiv.isin
order by total_dividends, created, secid, code