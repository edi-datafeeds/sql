--FilePath=o:\Datafeed\EDIUK\123\
--FileName=YYYYMMDD
--FileExtension=.123
--FileHeaderTEXT=EDI_SMF_123_YYYYMMDD

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use smf4
SELECT security.sedol, EDICAFlat.*, EDICADetail.*, EDICAOption.*
FROM EDICAFlat
INNER JOIN Security ON Security.SecurityID = EDICAFlat.SecurityID
INNER JOIN EDICADetail ON (EDICAFlat.Securityid = EDICADetail.SecurityId) AND (EDICAFlat.CadID = EDICADetail.CadID)
INNER JOIN EDICAOption ON (EDICAFlat.Securityid = EDICAOption.SecurityId) AND (EDICAFlat.CadID = EDICAOption.CadID) AND (EDICAFlat.CadoID = EDICAOption.CadoID)
where (EDICADetail.ssnyear<>'WCAD' or EDICADetail.ssnyear is null)
and (EDICADetail.CAType='DD'
or EDICADetail.CAType = 'DS'
or EDICADetail.CAType = 'DO'
or EDICADetail.CAType = 'D2')
ORDER BY Security.Sedol