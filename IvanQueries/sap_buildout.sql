select distinct bond.secid from bond
inner join scmst on bond.secid = scmst.secid
left outer join cpopt on bond.secid = cpopt.secid
where
statusflag<>'I'
and (bondsrc='DE' or bondsrc='OC' or bondsrc='PE' or bondsrc='PR' or bondsrc='PS')
and (callable='N' or (callable='Y' and cpopt.cpoptid is not null))
and (perpetual<>'' or maturitydate is not null)
and outstandingamount<>''

