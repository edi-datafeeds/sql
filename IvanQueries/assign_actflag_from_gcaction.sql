use wca
UPDATE AGCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE AGM set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE AGNCY set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE AGYDT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ANN set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ARR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ASSM set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BB set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BDC set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BKRP set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BOCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BON set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BOND set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BSCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BSKCC set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BSKWC set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BSKWT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE BWCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CALL set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CAPRD set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CNTR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CNTRG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CNTRY set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CONSD set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CONV set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CONVT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE COSNT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE COWAR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CPOPT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CRCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CRDRT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CTCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CTX set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CUREN set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CURRD set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE CWCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DEED set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DIST set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DIV set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DIVPY set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DMRGR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DPRCP set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DRCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DRIP set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DVPRD set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE DVST set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ENT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE EVENT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE EXCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE EXDT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE FRACTION set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE FRANK set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE FRNFX set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE FYChg set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ICC set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE IFCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE INCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE INDUS set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE INT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE INTBC set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE INTPY set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE IRCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ISCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ISSUR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE LAWST set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE LCC set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE LIQ set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE LOOKUP set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE LSTAT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE LTCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE MPAY set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE MRGR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE MTCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE NLIST set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ODDLT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ORDLK set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE PO set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE PRF set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE PRFTM set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE PVRD set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE RCAP set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE RCONV set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE RD set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE RDNOM set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE RDPRT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE REDEM set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE REDMT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE ROChG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE RTCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE RTS set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SACHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SCACT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SCAGY set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SCCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SCEXH set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SCMST set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SCSWP set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SD set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SDCHG   set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SECRC set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SECTY set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SEDOL set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SELRT set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SFUND set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SHOCH set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SS set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE SSLink set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE TKOVR set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE TRNCH set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE UMCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE UMPRG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE UMTRF set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE MF set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE MFCON set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE WAREX set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE WARTM set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE WHTAX set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE WTCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
UPDATE WXCHG set actflag=gcaction where gcaction <> 'A' and gcaction <> 'U'
