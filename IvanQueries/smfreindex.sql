use smf4
ALTER TABLE [DBO].[issuer] WITH NOCHECK ADD 
CONSTRAINT [pk_issuer1] PRIMARY KEY ([issuerid])  ON [PRIMARY]
GO
use smf4
ALTER TABLE [DBO].[security] WITH NOCHECK ADD 
CONSTRAINT [pk_security1] PRIMARY KEY ([securityid])  ON [PRIMARY]
GO
 CREATE  INDEX [IX_Security_01] ON [dbo].[security]([Sedol]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Security_02] ON [dbo].[security]([IssuerID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Security_03] ON [dbo].[security]([Isin], [CntryCD]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Security_04] ON [dbo].[security]([Isin], [Cregcode]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Security_05] ON [dbo].[security]([OPOL]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Security_06] ON [dbo].[security]([SecType]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Security_07] ON [dbo].[security]([Cregcode]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Security_08] ON [dbo].[security]([Actdate]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO


use smf4
ALTER TABLE [DBO].[market] WITH NOCHECK ADD 
CONSTRAINT [pk_market1] PRIMARY KEY ([marketid], [securityid])  ON [PRIMARY]
GO

 CREATE  INDEX [IX_Market_01] ON [dbo].[market]([TIDM]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Market_02] ON [dbo].[market]([SecurityID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_Market_03] ON [dbo].[market]([MIC]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO



ALTER TABLE [DBO].[EDICADetail] WITH NOCHECK ADD 
CONSTRAINT [PK_EDICADetail] PRIMARY KEY  CLUSTERED 
	(
		[CADID],
		[SecurityID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
 CREATE  INDEX [IX_EDICADetail] ON [dbo].[edicadetail]([Acceptdate]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO
 CREATE  INDEX [IX_EDICADetail_1] ON [dbo].[edicadetail]([Closedate]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO
 CREATE  INDEX [IX_EDICADetail_2] ON [dbo].[edicadetail]([CAType]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO
 CREATE  INDEX [IX_EDICADetail_3] ON [dbo].[edicadetail]([EffecDate]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO
 CREATE  INDEX [IX_EDICADetail_CADID] ON [dbo].[edicadetail]([CADID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO
 CREATE  INDEX [IX_EDICADetail_secid] ON [dbo].[edicadetail]([SecurityID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO


ALTER TABLE [DBO].[EDICAOption] WITH NOCHECK ADD 
CONSTRAINT [PK_edicaoption] PRIMARY KEY  CLUSTERED 
	(
		[CADOID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 

 CREATE  INDEX [IX_EDICAOption] ON [dbo].[edicaoption]([DivType]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_EDICAOption_1] ON [dbo].[edicaoption]([CashDistCallRate]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_EDICAOption_CADID] ON [dbo].[edicaoption]([CADID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [IX_edicaoption_2] ON [dbo].[edicaoption]([SecurityID], [CADID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

