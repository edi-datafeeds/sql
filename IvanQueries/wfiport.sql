use portfolio
delete wfiport
use portfolio
insert into WFIPORT
select secid
from fIsin
inner join wca.dbo.scmst on code = isin
where accid = 188 and fisin.actflag = 'U'
union
select secid
from fsedol
inner join wca.dbo.sedol on code = sedol
where accid = 188 and fsedol.actflag = 'U'
union
select secid
from fuscode
inner join wca.dbo.scmst on code = uscode
where accid = 188 and fuscode.actflag = 'U'

use portfolio
delete wfihist
use portfolio
insert into WFIHIST
select secid
from fIsin
inner join wca.dbo.scmst on code = isin
where accid = 188 and fisin.actflag = 'I'
union
select secid
from fsedol
inner join wca.dbo.sedol on code = sedol
where accid = 188 and fsedol.actflag = 'I'
union
select secid
from fuscode
inner join wca.dbo.scmst on code = uscode
where accid = 188 and fuscode.actflag = 'I'