use portfolio
delete wfiport
delete wfihist

insert into wfihist
select wca.dbo.bond.secid
from fisin
inner join wca.dbo.scmst on fisin.code = wca.dbo.scmst.isin
inner join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid
where
wca.dbo.bond.actflag<>'D'
and fisin.accid = 188 and fisin.actflag<>'D'
and wca.dbo.bond.secid not in (select secid from wfihist)

insert into wfihist
select wca.dbo.bond.secid
from fuscode
inner join wca.dbo.scmst on fuscode.code = wca.dbo.scmst.uscode
inner join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid
where
wca.dbo.bond.actflag<>'D'
and fuscode.accid = 188 and fuscode.actflag<>'D'
and wca.dbo.bond.secid not in (select secid from wfihist)


insert into wfihist
select wca.dbo.bond.secid
from fsedol
inner join wca.dbo.sedol on fsedol.code = wca.dbo.sedol.sedol
inner join wca.dbo.bond on wca.dbo.sedol.secid = wca.dbo.bond.secid
where
wca.dbo.bond.actflag<>'D'
and fsedol.accid = 188 and fsedol.actflag<>'D'
and wca.dbo.bond.secid not in (select secid from wfihist)
