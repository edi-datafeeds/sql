use wca
select distinct
issur.issid,
cntryofincorp,
issuername,
CntryCD as cntryofExchg,
primaryexchgcd,
scexh.ExchgCD,
Localcode,
MIC,
case when scexh.secid is not null or scmst.primaryexchgcd = scexh.exchgcd then 'Yes' else '' end as Exchg_is_Primary
from issur
inner join scmst on issur.issid = scmst.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where
(scmst.primaryexchgcd = scexh.exchgcd
or scmst.primaryexchgcd='' or scexh.secid is null)
and scexh.liststatus<>'D'
and scmst.statusflag<>'I'



