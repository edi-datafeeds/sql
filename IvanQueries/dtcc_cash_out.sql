use portfolio
select distinct wca.dbo.v54f_620_dividend.created, wca.dbo.v54f_620_dividend.changed, dtcc1.date as dtccdate, dtcc1.*, wca.dbo.v54f_620_dividend.recdate as EDI_recdate, wca.dbo.v54f_620_dividend.paydate as EDI_paydate  from dtcc1 
left outer join wca.dbo.v54f_620_dividend on isin# = isin and [record date] = recdate
where 
(wca.dbo.v54f_620_dividend.sedol = dtcc1.sedol or dtcc1.sedol is null or wca.dbo.v54f_620_dividend.sedol is null)
order by wca.dbo.v54f_620_dividend.created