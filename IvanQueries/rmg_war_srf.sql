use portfolio
select
case when wca.dbo.scmst.isin is not null then 'ISIN MATCH'
     when wca.dbo.icc.oldisin is not null then 'OLDISIN MATCH'
else 'NO MATCH' End as EdiMatch,
case when wca.dbo.scmst.secid is null then ' '
     when wca.dbo.scmst.statusflag is null or wca.dbo.scmst.statusflag='' then 'A'
 else wca.dbo.scmst.statusflag end as EdiStatus,
wca.dbo.scmst.sectycd as EDISectyCD,
wca.dbo.scmst.secid as EDISecID,
rmg_warrants.*
into rmg_war_srf1
from rmg_warrants
left outer join wca.dbo.scmst on rmg_warrants.isin = wca.dbo.scmst.isin
left outer join wca.dbo.icc on rmg_warrants.isin = wca.dbo.icc.oldisin
where 
wca.dbo.scmst.secid is not null

union

select
case  when wca.dbo.scmst.uscode is not null then 'USCODE MATCH'
      when wca.dbo.sedol.sedol is not null then 'SEDOL MATCH'
      when wca.dbo.sdchg.secid is not null then 'OLD SEDOL MATCH'
      when wca.dbo.scexh.localcode is not null then 'TICKER MATCH'
else 'NO MATCH' End as EdiMatch,
case when wca.dbo.scmst.secid is null then ' '
     when wca.dbo.scmst.statusflag is null or wca.dbo.scmst.statusflag='' then 'A'
 else wca.dbo.scmst.statusflag end as EdiStatus,
wca.dbo.scmst.sectycd as EDISectyCD,
case  when wca.dbo.scmst.uscode is not null then wca.dbo.scmst.secid
      when wca.dbo.sedol.sedol is not null then wca.dbo.sedol.secid
      when wca.dbo.sdchg.secid is not null then wca.dbo.sdchg.secid
      when wca.dbo.scexh.localcode is not null then wca.dbo.scexh.secid
else null End as EDISecid,
rmg_warnm.*
from rmg_warnm
left outer join wca.dbo.scmst on rmg_warnm.cusip = wca.dbo.scmst.uscode
left outer join wca.dbo.sedol on rmg_warnm.sedol = wca.dbo.sedol.sedol
left outer join wca.dbo.exchg on rmg_warnm.mic = wca.dbo.exchg.mic
left outer join wca.dbo.scexh on rmg_warnm.ticker = wca.dbo.scexh.localcode
                                   and wca.dbo.exchg.exchgcd = wca.dbo.scexh.exchgcd
left outer join wca.dbo.icc on rmg_warnm.cusip = wca.dbo.icc.olduscode
left outer join wca.dbo.sdchg on rmg_warnm.sedol = wca.dbo.sdchg.oldsedol
