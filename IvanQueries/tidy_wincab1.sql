use wol
delete tbl_wol_dir
from tbl_wol_dir
inner join tbl_wol_ssn on tbl_wol_dir.fld_dir_ssn_id = tbl_wol_ssn.fld_ssn_id
                          and tbl_wol_dir.fld_dir_ssn_count = tbl_wol_ssn.fld_ssn_count
where fld_dir_ssn_id>'9500/10'
and substring(fld_dir_ssn_id,6,2) = '10'
and fld_ssn_date<'2010/10/29'

use wol
delete tbl_wol_sec
where fld_sec_ssn_id>'9500/10'
and substring(fld_SEC_ssn_id,6,2) = '10'
and fld_sec_ssn_date<'2010/10/29'

use wol
delete tbl_wol_ssn
where fld_ssn_id>'9500/10'
and substring(fld_ssn_id,6,2) = '10'
and fld_ssn_date<'2010/10/29'