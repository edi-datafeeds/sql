use wca
select col001 as isin,
issuername,
securitydesc,
sectycd,
maturitydate,
seniorjunior
from portfolio.dbo.sd_isin
left outer join scmst on col001 = isin
left outer join bond on scmst.secid = bond.secid
left outer join issur on scmst.issid = issur.issid
order by sectycd, issuername, securitydesc