use priceshist;
ALTER TABLE `priceshist`.`histxber13` ADD INDEX `Index_isin`(`Isin`),
 ADD INDEX `Index_localcode`(`LocalCode`);
update priceshist.histxber13 as h
inner join wca.scmst as sm on h.isin = sm.isin
                                and (sm.secid <> '' or sm.secid is not null or sm.secid <> 0)
                                and h.isin <> ''
                                and h.rejoinflag is null
                                and h.rejoinsecid is null
                                and sm.actflag <>'d'
                                and sm.statusflag <> 'i'
set h.rejoinsecid=sm.secid,
rejoinflag =
          (
           case when (sm.SecID is not null and h.secid = 0) then '1'
           when (sm.SecID is not null and h.secid = sm.secid and h.secid <> 0) then '2'
           when (sm.SecID is not null and h.secid <> sm.secid and h.secid <> 0) then '3'
           end
          )
where (h.rejoinflag is null and h.rejoinsecid is null) and h.isin <> '';
update priceshist.histxber13 as h
inner join wca.scmst as sm on h.isin = sm.isin
                                and (sm.secid <> '' or sm.secid is not null or sm.secid <> 0)
                                and h.isin <> ''
                                and h.rejoinflag is null
                                and h.rejoinsecid is null
                                and sm.actflag <>'d'
                                and sm.statusflag = 'i'
set h.rejoinsecid=sm.secid,
rejoinflag =
          (
           case when (sm.SecID is not null and h.secid = 0) then '4'
           when (sm.SecID is not null and h.secid = sm.secid and h.secid <> 0) then '5'
           when (sm.SecID is not null and h.secid <> sm.secid and h.secid <> 0) then '6'
           end
          )
where (h.rejoinflag is null and h.rejoinsecid is null) and h.isin <> '';
update priceshist.histxber13 as h
inner join wca.scexh as exh on h.localcode = exh.localcode
                                and exh.liststatus <> 'd'
                                and exh.exchgcd = 'DEBSE'
                                and (exh.secid <> '' or exh.secid is not null or exh.secid <> 0)
                                and h.localcode <> ''
                                and h.rejoinflag is null
                                and h.rejoinsecid is null
set h.rejoinsecid=exh.secid,
rejoinflag =
          (
           case when (exh.SecID is not null and h.secid = 0) then '7'
                when (exh.SecID is not null and h.secid = exh.secid and h.secid <> 0) then '8'
                when (exh.SecID is not null and h.secid <> exh.secid and h.secid <> 0) then '9'
           end
          )
where (h.rejoinflag is null and h.rejoinsecid is null) and h.localcode <> '';
update priceshist.histxber13 as h
inner join wca.scexh as exh on h.localcode = exh.localcode
                                and exh.liststatus = 'd'
                                and exh.exchgcd = 'DEBSE'
                                and (exh.secid <> '' or exh.secid is not null or exh.secid <> 0)
                                and h.localcode <> ''
                                and h.rejoinflag is null
                                and h.rejoinsecid is null
set h.rejoinsecid=exh.secid,
rejoinflag =
          (
           case when (exh.SecID is not null and h.secid = 0) then 'a'
                when (exh.SecID is not null and h.secid = exh.secid and h.secid <> 0) then 'b'
                when (exh.SecID is not null and h.secid <> exh.secid and h.secid <> 0) then 'c'
           end
          )
where (h.rejoinflag is null and h.rejoinsecid is null) and h.localcode <> '';
update priceshist.histxber13 as h
inner join wca.icc as ic on h.isin = ic.oldisin
                             AND (h.rejoinflag is null and h.secid <> 0)
                             AND (h.isin <> '' and h.isin is not null)
                             AND ic.actflag <> 'd'
                             AND ic.eventtype <> 'clean'
set h.rejoinsecid = h.secid, rejoinflag = 'i'
where h.secid <> 0 and h.rejoinflag is null;
update priceshist.histxber13 as h
inner join wca.lcc as ic on h.localcode = ic.oldlocalcode
                             AND (h.rejoinflag is null and h.secid <> 0)
                             AND (h.localcode <> '' and h.localcode is not null)
                             AND ic.actflag <> 'd'
                             AND ic.eventtype <> 'clean'
                             AND ic.exchgcd = 'DEBSE'
set h.rejoinsecid = h.secid, rejoinflag = 'l'
where h.secid <> 0 AND (h.localcode <> '' and h.localcode is not null);
ALTER TABLE `priceshist`.`histxber13` ADD INDEX `dedupe`(`rejoinsecid`, `MktCloseDate`, `PriceDate` desc, `Currency` desc, `high` desc);
delete from dupe_id_ivan;
insert into dupe_id_ivan (id,secid,mktclosedate)
SELECT h.id,h.rejoinsecid,h.mktclosedate FROM priceshist.histxber13 as h
where h.rejoinsecid is not null
and h.id <> (SELECT subh.id FROM priceshist.histxber13 as subh
             where subh.mktclosedate = h.mktclosedate
                   and subh.rejoinsecid = h.rejoinsecid and subh.rejoinsecid is not null
                   order by subh.mktclosedate, subh.rejoinsecid,
                            subh.pricedate desc,subh.currency desc,subh.high desc limit 1);
select h.* from histxber13 as h
inner join dupe_id_ivan as d on h.id = d.id;
delete h.* from histxber13 as h
inner join dupe_id_ivan as d on h.id = d.id;
update histxber13 as h
inner join wca.scmst as sm on h.rejoinsecid = sm.secid and h.rejoinflag = '1'
                     and sm.actflag <>'d'
                     and sm.statusflag <> 'i'
left outer join wca.issur on sm.issid = wca.issur.issid
                      and wca.issur.issuername <> ''
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol,h.uscode = sm.uscode,h.primaryexchgcd = sm.primaryexchgcd, h.issuername = wca.issur.issuername,
h.sectycd = sm.sectycd, h.securitydesc = sm.securitydesc
where h.rejoinflag = '1';
update histxber13 as h
inner join wca.scmst as sm on h.rejoinsecid = sm.secid and h.rejoinflag = '3'
                     and sm.actflag <>'d'
                     and sm.statusflag <> 'i'
left outer join wca.issur on sm.issid = wca.issur.issid
                      and wca.issur.issuername <> ''
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol,h.uscode = sm.uscode,h.primaryexchgcd = sm.primaryexchgcd, h.issuername = wca.issur.issuername,
h.sectycd = sm.sectycd, h.securitydesc = sm.securitydesc
where h.rejoinflag = '3';
update histxber13 as h
inner join wca.scmst as sm on h.rejoinsecid = sm.secid and h.rejoinflag = '5'
                     and sm.actflag <>'d'
                     and sm.statusflag <> 'i'
left outer join wca.issur on sm.issid = wca.issur.issid
                      and wca.issur.issuername <> ''
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol,h.uscode = sm.uscode,h.primaryexchgcd = sm.primaryexchgcd, h.issuername = wca.issur.issuername,
h.sectycd = sm.sectycd, h.securitydesc = sm.securitydesc
where h.rejoinflag = '5';
update histxber13 as h
inner join wca.scmst as sm on h.rejoinsecid = sm.secid and h.rejoinflag = 'a'
                     and sm.actflag <>'d'
                     and sm.statusflag <> 'i'
left outer join wca.issur on sm.issid = wca.issur.issid
                      and wca.issur.issuername <> ''
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol,h.uscode = sm.uscode,h.primaryexchgcd = sm.primaryexchgcd, h.issuername = wca.issur.issuername,
h.sectycd = sm.sectycd, h.securitydesc = sm.securitydesc
where h.rejoinflag = 'a';
update histxber13 as h
inner join wca.scmst as sm on h.rejoinsecid = sm.secid and h.rejoinflag = '4'
                     and sm.actflag <>'d'
left outer join wca.issur on sm.issid = wca.issur.issid
                      and wca.issur.issuername <> ''
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol,h.uscode = sm.uscode,h.primaryexchgcd = sm.primaryexchgcd, h.issuername = wca.issur.issuername,
h.sectycd = sm.sectycd, h.securitydesc = sm.securitydesc
where h.rejoinflag = '4';
update histxber13 as h
inner join wca.scmst as sm on h.rejoinsecid = sm.secid and h.rejoinflag = '6'
                     and sm.actflag <>'d'
left outer join wca.issur on sm.issid = wca.issur.issid
                      and wca.issur.issuername <> ''
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol,h.uscode = sm.uscode,h.primaryexchgcd = sm.primaryexchgcd, h.issuername = wca.issur.issuername,
h.sectycd = sm.sectycd, h.securitydesc = sm.securitydesc
where h.rejoinflag = '6';
update histxber13 as h
inner join wca.scmst as sm on h.rejoinsecid = sm.secid and h.rejoinflag = 'i' and h.issuername = ''
                     and sm.actflag <>'d'
                     and sm.statusflag <> 'i'
left outer join wca.issur on sm.issid = wca.issur.issid
                      and wca.issur.issuername <> ''
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol,h.uscode = sm.uscode,h.primaryexchgcd = sm.primaryexchgcd, h.issuername = wca.issur.issuername,
h.sectycd = sm.sectycd, h.securitydesc = sm.securitydesc
where h.rejoinflag = 'i' and h.issuername = '';
update histxber13 as h
set h.sedol = '', h.uscode = '', h.primaryexchgcd = ''
where h.rejoinflag is null and (sectycd = 'eqs' or sectycd = 'prf');
update histxber13
set providerid = 205
where SectyCD = 'bnd' and providerid = 91;
update histxber13
set rejoinsecid = 0
where rejoinflag is null and (sectycd = 'eqs' or sectycd = 'prf');
ALTER TABLE `priceshist`.`histxber13` CHANGE COLUMN `SecID` `SecID_old` INT(11) DEFAULT NULL,
 CHANGE COLUMN `rejoinSecID` `SecID` INT(11) DEFAULT NULL,
 DROP INDEX `dedupe`,
 ADD INDEX `dedupe` USING BTREE(`SecID`, `MktCloseDate`, `PriceDate`, `Currency`, `High`);
update histxber13
set exchgcd = 'DEBSE'
where exchgcd <> 'DEBSE';
update histxber13 as h
inner join wca.scmst as sm on h.secid = sm.secid and h.rejoinflag is not null and h.sedol = ''
                     and sm.actflag <>'d'
                     and sm.statusflag <> 'i'
left outer join wca.sedol on sm.secid = wca.sedol.secid
                     and wca.sedol.cntrycd='DE'
                     and wca.sedol.curencd=h.currency
                     and wca.sedol.sedol <> ''
                     and wca.sedol.actflag <> 'D'
                     and wca.sedol.defunct <> 'T'
set h.sedol = wca.sedol.sedol
where (h.rejoinflag = 2 or h.rejoinflag = 5 or h.rejoinflag = 8 or h.rejoinflag = 9 or h.rejoinflag = 'c' or h.rejoinflag = 'b'); and h.sedol = '';
