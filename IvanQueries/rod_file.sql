use wca
SELECT issuername, securitydesc, isin, cntryofincorp, maturitydate
FROM bond
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
where
securitycharge='MB'
and statusflag<>'I'
and (maturitydate is null or maturitydate>getdate())
and bond.actflag<>'D'
