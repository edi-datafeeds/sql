use portfolio
select
case  when p04.secid is not null then 'EdiSecID MATCH'
      when p2.localcode <> '' then 'TICKER MATCH'
      when p3.Isin <> ''  then 'ISIN MATCH'
      when p5.sedol <> ''  then 'SEDOL MATCH'
else 'NO MATCH' End as PriceLink,
rmg_war_srf1.*
--into rmg_war_srf1_all
from rmg_war_srf1
left outer join p04 on rmg_war_srf1.edisecid = p04.secid
                                   and rmg_war_srf1.mic = p04.mic
left outer join p04 as p2 on rmg_war_srf1.ticker = p2.localcode
                                   and rmg_war_srf1.mic = p2.mic
left outer join p04 as p3 on rmg_war_srf1.isin = p3.isin
                                   and rmg_war_srf1.mic = p3.mic
left outer join p04 as p5 on rmg_war_srf1.sedol = p5.sedol
                                   and rmg_war_srf1.mic = p5.mic

order by rmg_war_srf1.id


select top 1 * from rmg_war_srf1

update rmg_war_srf1
set edisecid = 0
where edisecid is null

use