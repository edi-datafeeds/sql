--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_PRCHG
--fileheadertext=EDI_PRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
SELECT 
upper('PRCHG') as TableName,
prchg.Actflag,
prchg.AnnounceDate as Created,
prchg.Acttime as Changed,
prchg.PrchgID,
prchg.SecID,
prchg.EffectiveDate,
prchg.EventType,
prchg.OldExchgCD,
prchg.NewExchgCD,
prchg.Notes
FROM prchg
INNER JOIN bond ON prchg.SecID = bond.SecID
INNER JOIN scmst ON bond.SecID = scmst.SecID
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and prchg.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
