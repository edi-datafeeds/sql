--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select
upper('MPAY') as Tablename,
mpay.Actflag,
mpay.AnnounceDate,
mpay.Acttime,
mpay.Sevent as EventType,
mpay.EventId,
mpay.OptionId,
mpay.SerialId,
mpay.Sectycd as ResSectycd,
mpay.ResSecid,
resscmst.Isin as ResIsin,
resissur.IssuerName as ResIssuerName,
resscmst.SecurityDesc as ResSecurityDesc,
mpay.RatioNew,
mpay.RatioOld,
mpay.Fractions,
mpay.MinOfrQty,
mpay.MaxOfrQty,
mpay.MinQlyQty,
mpay.MaxQlyQty,
mpay.PayDate,
mpay.Curencd,
mpay.MinPrice,
mpay.MaxPrice,
mpay.TndrStrkPrice,
mpay.TndrStrkStep,
mpay.PayType,
mpay.DutchAuction,
mpay.DefaultOpt,
mpay.OptElectionDate
from mpay
inner join liq on 'liq' = mpay.sevent and liq.liqid = mpay.eventid
inner join scmst on liq.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join issur as resissur on resscmst.issid = resissur.issid
where
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and MPAY.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
