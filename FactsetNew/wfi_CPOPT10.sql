--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPT10
--fileheadertext=EDI_CPOPT_10_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CPOPT') as Tablename,
wca.cpopt.ActFlag,
wca.cpopt.AnnounceDate as Created,
wca.cpopt.Acttime as Changed,
wca.cpopt.CpoptId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.cpopt.CallPut, 
wca.cpopt.FromDate,
wca.cpopt.ToDate,
wca.cpopt.NoticeFrom,
wca.cpopt.NoticeTo,
wca.cpopt.Currency,
wca.cpopt.Price,
wca.cpopt.MandatoryOptional,
wca.cpopt.MinNoticeDays,
wca.cpopt.MaxNoticeDays,
wca.cpopt.CpType,
wca.cpopt.PriceAsPercent,
wca.cpopt.InWholePart,
wca.cpopt.FormulaBasedPrice,
wca.cpopn.Notes as Notes 
from wca.cpopt
inner join wca.bond on wca.cpopt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.cpopn on wca.cpopt.secid = wca.cpopn.secid and wca.cpopt.callput = wca.cpopn.callput
where
scmst.isin in (select client.pfisin.code from client.pfisin where accid = 212)


and wca.cpoptid>5464830 and wca.cpoptid < 5972563