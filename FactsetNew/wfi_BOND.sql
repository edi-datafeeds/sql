--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('BOND') as Tablename,
bond.ActFlag,
bond.AnnounceDate as Created,
bond.Acttime as Changed,
bond.SecId,
scmst.Isin,
bond.BondType,
bond.DebtMarket,
bond.Curencd as DebtCurrency,
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as NominalValue,
bond.IssueDate,
bond.IssueCurrency,
bond.IssuePrice,
bond.IssueAmount,
bond.IssueAmountDate,
bond.OutstandingAmount,
bond.OutstandingAmountDate,
bond.InterestBasis,
case when isnull(int.NilInt,'F') = 'T' then '0' else bond.interestRate end as interestRate,
bond.InterestAccrualConvention,
bond.InterestPaymentFrequency,
bond.IntcommenCementDate,
bond.FirstCouponDate,
bond.InterestPayDate1,
bond.InterestPayDate2,
bond.InterestPayDate3,
bond.InterestPayDate4,
bondx.DomesticTaxRate,
bondx.NonResidentTaxRate,
bond.FrnType,
bond.FrnIndexBenchmark,
bond.Markup as FrnMargin,
bond.MinimumInterestRate as FrnMinInterestRate,
bond.MaximumInterestRate as FrnmaxInterestRate,
bond.Rounding,
bondx.Series,
bondx.Class,
bondx.OnTap,
bondx.MaximumTapAmount,
bondx.TapExpiryDate,
bond.Guaranteed,
bond.SecuredBy,
bond.SecurityCharge,
bond.Subordinate,
bond.SeniorJunior,
bond.WarrantAttached,
bond.MaturityStructure,
bond.Perpetual,
bond.Maturitydate,
bond.MaturityExtendible,
bond.Callable,
bond.Puttable,
bondx.Denomination1,
bondx.Denomination2,
bondx.Denomination3,
bondx.Denomination4,
bondx.Denomination5,
bondx.Denomination6,
bondx.Denomination7,
bondx.MinimumDenomination,
bondx.DenominationMultiple,
bond.Strip,
bond.StripInterestNumber, 
bond.Bondsrc,
bond.MaturityBenchmark,
bond.ConventionMethod,
bond.FrnIntAdjFreq as FrnInterestAdjFreq,
bond.IntBusDayConv as InterestBusDayConv,
bond.InterestCurrency,
bond.MatBusDayConv as MaturityBusDayConv,
bond.MaturityCurrency, 
bondx.TaxRules,
bond.VarIntPayDate as VarInterestPayDate,
bond.PriceAsPercent,
bond.PayoutMode,
bond.Cumulative,
case when bond.MatPrice<>'' then cast(bond.MatPrice as decimal(18,4))
     when rtrim(bond.LargeParValue)='' then null
     when rtrim(bond.MatPriceAsPercent)='' then null
     else cast(bond.LargeParValue as decimal(18,0)) * cast(bond.MatPriceAsPercent as decimal(18,4))/100 
     end as MatPrice,
bond.MatPriceAsPercent,
bond.SinkingFund,
bondx.GovCity,
bondx.GovState,
bondx.GovCntry,
bondx.GovLawLkup as GovLaw,
bondx.GoverningLaw as GovLawNotes,
bond.Municipal,
bond.PrivatePlacement,
bond.Syndicated,
bond.Tier,
bond.UppLow,
bond.Collateral,
bond.CoverPool,
bond.PikPay,
bond.YielDatIssue,
bond.CoCoTrigger as Coco,
bond.Notes
from bond
inner join scmst on bond.secid = scmst.secid
left outer join bondx on bond.secid = bondx.secid
left outer join rd on bond.secid = rd.secid
left outer join int on rd.rdid=int.rdid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bond.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)))
AND (rd.rdid is null or rd.rdid =
(select top 1 subrd.rdid from rd as subrd
left outer join int as subint on subrd.rdid=subint.rdid
where subrd.secid=rd.secid
order by subint.rdid desc))