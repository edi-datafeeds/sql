--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('SCMST') as Tablename,
scmst.ActFlag,
scmst.AnnounceDate as Created,
scmst.Acttime as Changed,
scmst.SecId,
scmst.Isin,
scmst.ParentSecid,
scmst.IssId,
scmst.Sectycd,
scmst.SecurityDesc,
case when scmst.StatusFlag = '' then 'A' else scmst.StatusFlag end as StatusFlag,
scmst.PrimaryExchgcd,
scmst.Curencd,
scmst.ParValue,
scmst.UsCode,
scmst.X as CommonCode,
scmst.Holding,
scmst.Structcd,
scmst.Wkn,
scmst.Regs144a,
scmst.Cfi,
scmst.ScmstNotes as Notes
from scmst
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
  or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
  and scmst.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
