--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPN
--fileheadertext=EDI_CPOPN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
SELECT 
upper('CPOPN') as TableName,
cpopn.Actflag,
cpopn.AnnounceDate as Created,
cpopn.Acttime as Changed,
cpopn.CpopnID,
cpopn.SecID,
cpopn.CallPut, 
cpopn.Notes as Notes 
FROM cpopn
INNER JOIN bond ON cpopn.SecID = bond.SecID
INNER JOIN scmst ON bond.SecID = scmst.SecID
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and cpopn.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))