--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('FRNFX') as Tablename,
frnfx.ActFlag,
frnfx.AnnounceDate as Created,
frnfx.Acttime as Changed,
frnfx.FrnFxid,
frnfx.SecId,
scmst.Isin,
frnfx.EffectiveDate,
frnfx.OldFrnType,
frnfx.OldFrnIndexBenchmark,
frnfx.OldMarkup as OldFrnMargin,
frnfx.OldMinimumInterestRate,
frnfx.OldMaximumInterestRate,
frnfx.OldRounding,
frnfx.NewFrnType,
frnfx.NewFrnIndexBenchmark,
frnfx.NewMarkup as NewFrnMargin,
frnfx.NewMinimumInterestRate,
frnfx.NewMaximumInterestRate,
frnfx.NewRounding,
frnfx.EventType,
frnfx.OldFrnIntAdjFreq,
frnfx.NewFrnIntAdjFreq
from frnfx
inner join bond on frnfx.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and frnfx.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))