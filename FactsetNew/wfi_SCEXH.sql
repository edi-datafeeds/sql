--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('SCEXH') as Tablename,
scexh.ActFlag,
scexh.AnnounceDate as Created,
scexh.Acttime as Changed,
scexh.ScexhId,
scexh.SecId,
scmst.Isin,
scexh.Exchgcd,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
'' as TradeStatus,
scexh.LocaLcode,
SUBSTRING(SCEXH.JunkLocalcode,0,50) as JunkLocalcode,
scexh.ScexhNotes as Notes
from scexh
inner join bond on scexh.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and SCEXH.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))

