--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BOCHG
--fileheadertext=EDI_BOCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('BOCHG') as Tablename,
bochg.ActFlag,
bochg.AnnounceDate as Created,
bochg.Acttime as Changed,  
bochg.BochgId,
bochg.RelEventId,
bochg.SecId,
scmst.Isin,
bochg.EffectiveDate,
bochg.OldOutValue,
bochg.NewOutValue,
bochg.EventType,
bochg.OldOutDate,
bochg.NewOutDate,
bochg.BochgNotes as Notes
from bochg
inner join bond on bochg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bochg.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))