--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('scagy') as Tablename,
scagy.ActFlag,
scagy.AnnounceDate as Created,
scagy.Acttime as Changed,
scagy.ScagyId,
scagy.SecId,
scmst.Isin,
scagy.Relationship,
scagy.AgncyId,
scagy.GuaranteeType,
scagy.SpStartDate,
scagy.SpendDate,
scagy.Notes
from scagy
inner join bond on scagy.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and SCAGY.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
