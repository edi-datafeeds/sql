--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
'COSNT' as Tablename,
cosnt.Actflag,
cosnt.Announcedate as Created,
case when (rd.Acttime is not null) and (rd.Acttime > cosnt.Acttime) then rd.Acttime else cosnt.Acttime end as Changed,
cosnt.RdId,
scmst.SecId,
rd.RecDate,
scmst.Isin,
cosnt.ExpiryDate,
cosnt.ExpiryTime,
SUBSTRING(cosnt.TimeZone, 1,3) AS TimeZone,
cosnt.CollateralRelease,
cosnt.Currency,
cosnt.Fee,
cosnt.Notes
from cosnt
inner join rd on cosnt.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and cosnt.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))