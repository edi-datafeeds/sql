--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=EDI_LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('LTCHG') as Tablename,
ltchg.ActFlag,
ltchg.AnnounceDate as Created,
ltchg.Acttime as Changed,
ltchg.LtchgId,
scmst.SecId,
scmst.Isin,
ltchg.Exchgcd,
ltchg.EffectiveDate,
ltchg.OldLot,
ltchg.OldMintrdQty,
ltchg.NewLot,
ltchg.NewMintrdgQty
from ltchg
inner join bond on ltchg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and LTCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
