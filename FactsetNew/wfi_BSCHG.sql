--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BSCHG
--fileheadertext=EDI_BSCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select
upper('BSCHG') as Tablename,
bschg.ActFlag,
bschg.AnnounceDate as Created,
bschg.Acttime as Changed,
bschg.BschgId,
bschg.SecId,
scmst.Isin,
bschg.NotificationDate,
bschg.OldBondType,
bschg.NewBondType,
bschg.OldCurencd,
bschg.NewCurencd,
bschg.OldPIU,
bschg.NewPIU,
bschg.OldInterestBasis,
bschg.NewInterestBasis,
bschg.EventType,
bschg.OldInterestCurrency,
bschg.NewInterestCurrency,
bschg.OldMaturityCurrency,
bschg.NewMaturityCurrency,
bschg.OldIntBusDayConv,
bschg.NewIntBusDayConv,
bschg.OldMatBusDayConv,
bschg.NewMatBusDayConv,
bschg.BschgNotes as Notes
from bschg
inner join bond on bschg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bschg.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))