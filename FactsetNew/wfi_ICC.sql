--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ICC
--fileheadertext=EDI_ICC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('ICC') as Tablename,
icc.ActFlag,
icc.AnnounceDate as Created,
icc.Acttime as Changed,
icc.IccId,
icc.SecId,
scmst.Isin,
icc.EffectiveDate,
icc.OldIsin,
icc.NewIsin,
icc.OldUsCode,
icc.NewUsCode,
icc.OldValoren,
icc.NewValoren,
icc.EventType,
icc.RelEventId,
icc.OldCommonCode,
icc.NewCommonCode
from icc
-- inner outer join bond on icc.secid = bond.secid
inner join scmst on icc.secid = scmst.secid
where
ICC.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)