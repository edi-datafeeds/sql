--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('IRCHG') as Tablename,
irchg.ActFlag,
irchg.AnnounceDate as Created,
irchg.Acttime as Changed,
irchg.IrchgId,
irchg.SecId,
scmst.Isin,
irchg.EffectiveDate,
irchg.OldInterestRate,
irchg.NewInterestRate,
irchg.EventType,
irchg.Notes 
from irchg
inner join bond on irchg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and IRCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
