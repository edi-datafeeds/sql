--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SACHG
--fileheadertext=EDI_SACHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('SACHG') as Tablename,
sachg.ActFlag,
sachg.Announcedate as Created,
sachg.Acttime as Changed,
sachg.SachgId,
sachg.ScagyId,
bond.SecId,
scmst.Isin,
sachg.EffectiveDate,
sachg.OldRelationship,
sachg.NewRelationship,
sachg.OldAgncyId,
sachg.NewAgncyId,
sachg.OldSpStartDate,
sachg.NewSpStartDate,
sachg.OldSpendDate,
sachg.NewSpendDate,
sachg.OldGuaranteeType,
sachg.NewGuaranteeType
from sachg
inner join scagy on sachg.scagyid = scagy.scagyid
inner join bond on scagy.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and SACHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
