--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('SDCHG') as Tablename,
sdchg.ActFlag,
sdchg.AnnounceDate as Created, 
sdchg.Acttime as Changed,
sdchg.SdchgId,
sdchg.SecId,
scmst.Isin,
sdchg.Cntrycd as OldCntrycd,
sdchg.EffectiveDate,
sdchg.OldSedol,
sdchg.NewSedol,
sdchg.EventType,
sdchg.RCntrycd as OldrCntrycd,
sdchg.ReleventId,
sdchg.NewCntrycd,
sdchg.NewrCntrycd,
sdchg.SdchgNotes as Notes
from sdchg
inner join bond on sdchg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and SDCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
