--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('MTCHG') as Tablename,
mtchg.ActFlag,
mtchg.AnnounceDate as Created,
mtchg.Acttime as Changed,
mtchg.MtchgId,
mtchg.SecId,
scmst.Isin,
mtchg.NotificationDate,
mtchg.OldMaturityDate,
mtchg.NewMaturityDate,
mtchg.Reason,
mtchg.EventType,
mtchg.OldMaturityBenchmark,
mtchg.NewMaturityBenchmark,
mtchg.Notes as Notes
from mtchg
inner join bond on mtchg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and MTCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
