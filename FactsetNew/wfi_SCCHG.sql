--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('SCCHG') as Tablename,
scchg.ActFlag,
scchg.AnnounceDate as Created,
scchg.Acttime as Changed,
scchg.ScchgId,
scchg.SecId,
scmst.Isin,
scchg.DateOfChange,
scchg.SecOldName,
scchg.SecNewName,
scchg.EventType,
scchg.OldSectycd,
scchg.NewSectycd,
scchg.OldRegs144a,
scchg.NewRegs144a,
scchg.ScchgNotes as Notes
from scchg
inner join bond on scchg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and SCCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
