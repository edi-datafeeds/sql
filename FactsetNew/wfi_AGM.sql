--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('AGM') as Tablename,
agm.Actflag,
agm.Announcedate as Created,
agm.Acttime as Changed, 
agm.AgmId,
bond.SecId,
scmst.Isin,
agm.IssId,
agm.AgmDate,
agm.AgmEgm,
agm.AgmNo,
agm.FyeDate,
agm.AgmTime,
agm.Add1,
agm.Add2,
agm.Add3,
agm.Add4,
agm.Add5,
agm.Add6,
agm.City,
agm.Cntrycd,
agm.BondSecid
from agm
inner join scmst on agm.bondsecid = scmst.secid
inner join bond on agm.bondsecid = bond.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))

or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and AGM.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))