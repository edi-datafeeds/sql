--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CONV
--fileheadertext=EDI_CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
upper('CONV') as Tablename,
conv.ActFlag,
conv.Announcedate as Created,
case when (rd.Acttime is not null) and (rd.Acttime > conv.Acttime) then rd.Acttime else conv.Acttime end as Changed,
conv.ConvId,
scmst.SecId,
scmst.Isin,
rd.RecDate,
conv.FromDate,
conv.ToDate,
conv.RatioNew,
conv.RatioOld,
conv.Curencd as ConvCurrency,
conv.CurPair,
conv.Price,
conv.MandoptFlag,
conv.ResSecId,
conv.ResSectycd,
resscmst.Isin as ResIsin,
resissur.IssuerName as ResIssuerName,
resscmst.SecurityDesc as ResSecurityDesc,
conv.Fractions,
conv.FxRate,
conv.PartFinalFlag,
conv.ConvType,
conv.RdId,
conv.PriceAsPercent,
conv.AmountConverted,
conv.SettlementDate,
conv.ConvNotes as Notes
from conv
inner join bond on conv.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join rd on conv.rdid = rd.rdid
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join issur as resissur on resscmst.issid = resissur.issid
where
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and conv.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))