--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
SELECT distinct
upper('BBE') as Tablename,
bbe.ActFlag,
bbe.AnnounceDate,
bbe.Acttime,
bbe.BbeId,
bbe.SecId,
bbe.Exchgcd,
bbe.Curencd,
bbe.BbgexhId,
bbe.Bbgexhtk
from bbe
inner join bond on bbe.secid = bond.secid
inner join scmst on bbe.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bbe.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))