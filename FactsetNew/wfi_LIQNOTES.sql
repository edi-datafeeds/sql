--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select
upper('LIQNOTES') as Tablename,
liq.ActFlag,
liq.LiqId,
liq.IssId,
liq.LiquidationTerms
from liq
where
LIQ.Issid in (select wca.dbo.scmst.issid from client.dbo.pfisin
inner join wca.dbo.scmst on client.dbo.pfisin.code = wca.dbo.scmst.isin
where (client.dbo.pfisin.accid=212 and client.dbo.pfisin.actflag='I')
or (client.dbo.pfisin.accid=212 and client.dbo.pfisin.actflag='U'
and LIQ.acttime> (select max(feeddate) from tbl_Opslog where seq = 3 )))
