--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select distinct
upper('LIQ') as Tablename,
liq.ActFlag,
liq.AnnounceDate as Created,
liq.Acttime as Changed,
liq.LiqId,
bond.SecId,
scmst.Isin,
liq.IssId,
liq.Liquidator,
liq.LiqAdd1,
liq.LiqAdd2,
liq.LiqAdd3,
liq.LiqAdd4,
liq.LiqAdd5,
liq.LiqAdd6,
liq.LiqCity,
liq.LiqCntrycd,
liq.LiqTel,
liq.LiqFax,
liq.LiqEmail,
liq.RdDate
from liq
inner join scmst on liq.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and LIQ.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
