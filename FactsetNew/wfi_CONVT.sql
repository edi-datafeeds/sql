--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('CONVT') as Tablename,
convt.ActFlag,
convt.Announcedate as Created,
convt.Acttime as Changed,
convt.ConvtId,
scmst.SecId,
scmst.Isin,
convt.FromDate,
convt.ToDate,
convt.RatioNew,
convt.RatioOld,
convt.Curencd,
convt.CurPair,
convt.Price,
convt.MandoptFlag,
convt.ResSecId,
convt.Sectycd as ResSectycd,
resscmst.Isin as ResIsin,
resissur.Issuername as ResIssuername,
resscmst.SecurityDesc as ResSecurityDesc,
convt.Fractions,
convt.FxRate,
convt.PartFinalFlag,
convt.PriceAsPercent,
convt.ConvtNotes as Notes
from convt
inner join bond on convt.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as resscmst on convt.ressecid = resscmst.secid
left outer join issur as resissur on resscmst.issid = resissur.issid
where
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and convt.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))