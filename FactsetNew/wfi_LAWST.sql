--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LAWST
--fileheadertext=EDI_LAWST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--#
use wca
select distinct
upper('LAWST') as Tablename,
lawst.ActFlag,
lawst.AnnounceDate as Created,
lawst.Acttime as Changed,
lawst.LawstId,
bond.SecId,
scmst.Isin,
lawst.IssId,
lawst.EffectiveDate,
lawst.LaType,
lawst.RegDate
from lawst
inner join scmst on lawst.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and LAWST.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
