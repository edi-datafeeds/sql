--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select
upper('INCHG') as Tablename,
inchg.ActFlag,
inchg.AnnounceDate as Created,
inchg.Acttime as Changed,
inchg.InchgId,
bond.SecId,
scmst.Isin,
inchg.IssId,
inchg.InchgDate,
inchg.OldCntrycd,
inchg.NewCntrycd,
inchg.EventType 
from inchg
inner join scmst on inchg.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and INCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
and (bond.issuedate<=inchg.inchgdate
     or bond.issuedate is null or inchg.inchgdate is null
    )