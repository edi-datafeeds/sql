--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LOOKUP
--fileheadertext=EDI_LOOKUP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
SELECT
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
FROM lookupextra

union

SELECT
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM lookup



union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
dvprd.DvprdCD,
dvprd.Divperiod
from dvprd

union

SELECT
upper('I') as Actflag,
secty.Acttime,
upper('SECTYPE') as TypeGroup,
secty.SectyCD,
secty.SecurityDescriptor
from secty

union

SELECT
exchg.Actflag,
exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
exchg.ExchgCD as Code,
exchg.Exchgname as Lookup
from exchg

union

SELECT
exchg.Actflag,
exchg.Acttime,
upper('MICCODE') as TypeGroup,
exchg.MIC as Code,
exchg.Exchgname as Lookup
from exchg
where MIC <> '' and MIC is not null

union

SELECT
exchg.Actflag,
exchg.Acttime,
upper('MICMAP') as TypeGroup,
exchg.ExchgCD as Code,
exchg.MIC as Lookup
from exchg

union

SELECT
indus.Actflag,
indus.Acttime,
upper('INDUS') as TypeGroup,
cast(indus.IndusID as char(10)) as Code,
indus.IndusName as Lookup
from indus

union

SELECT
cntry.Actflag,
cntry.Acttime,
upper('CNTRY') as TypeGroup,
cntry.CntryCD as Code,
cntry.Country as Lookup
from cntry

union

SELECT
curen.Actflag,
curen.Acttime,
upper('CUREN') as TypeGroup,
curen.CurenCD as Code,
curen.Currency as Lookup
from curen

union

SELECT
event.Actflag,
event.Acttime,
upper('EVENT') as TypeGroup,
event.EventType as Code,
event.EventName as Lookup
from event

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2
and 1=2

order by TypeGroup, Code
