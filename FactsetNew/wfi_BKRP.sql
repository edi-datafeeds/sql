--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=EDI_BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select distinct
upper('BKRP') as Tablename,
bkrp.ActFlag,
bkrp.AnnounceDate as Created,
bkrp.Acttime as Changed,  
bkrp.BkrpId,
bond.SecId,
scmst.Isin,
bkrp.IssId,
bkrp.NotificationDate,
bkrp.FilingDate
from bkrp
inner join scmst on bkrp.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bkrp.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))