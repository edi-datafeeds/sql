--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select
upper('BKRPNOTES') as Tablename,
bkrp.ActFlag,
bkrp.BkrpId,
bkrp.IssId,
bkrp.BkrpNotes as Notes
from bkrp
where
BKRP.Issid in (select wca.dbo.scmst.issid from client.dbo.pfisin
inner join wca.dbo.scmst on client.dbo.pfisin.code = wca.dbo.scmst.isin
where (client.dbo.pfisin.accid=212 and client.dbo.pfisin.actflag='I')
or (client.dbo.pfisin.accid=212 and client.dbo.pfisin.actflag='U'
and bkrp.acttime> (select max(feeddate) from tbl_Opslog where seq = 3 )))
