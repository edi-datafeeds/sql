--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
upper('BBEC') as Tablename,
bbec.Actflag,
bbec.AnnounceDate,
bbec.Acttime,
bbec.BbecId,
bbec.SecId,
bbec.BbeId,
bbec.OldExchgcd,
bbec.OldCurencd,
bbec.EffectiveDate,
bbec.NewExchgcd,
bbec.NewCurencd,
bbec.OldBbgexhId,
bbec.NewBbgexhId,
bbec.OldBbgexhtk,
bbec.NewBbgexhtk,
bbec.RelEventId,
bbec.EventType,
bbec.Notes
from bbec
inner join bond on bbec.secid = bond.secid
inner join scmst on bbec.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bbec.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))