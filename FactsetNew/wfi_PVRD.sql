--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('PVRD') as Tablename,
pvrd.ActFlag,
pvrd.AnnounceDate as Created,
pvrd.Acttime as Changed,
pvrd.PvrdId,
pvrd.SecId,
scmst.Isin,
pvrd.EffectiveDate,
pvrd.Curencd,
pvrd.OldParValue,
pvrd.NewParValue,
pvrd.EventType,
pvrd.PvrdNotes as Notes
from pvrd
inner join bond on pvrd.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and PVRD.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
