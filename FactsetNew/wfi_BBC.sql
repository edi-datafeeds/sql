--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select distinct
upper('BBC') as Tablename,
bbc.ActFlag,
bbc.AnnounceDate,
bbc.Acttime,
bbc.BbcId,
bbc.SecId,
bbc.Cntrycd,
bbc.Curencd,
bbc.BbgcompId,
bbc.Bbgcomptk
from bbc
inner join bond on bbc.secid = bond.secid
inner join scmst on bbc.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bbc.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))