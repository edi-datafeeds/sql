--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select distinct
upper('ISSUR') as Tablename,
issur.ActFlag,
issur.AnnounceDate as Created,
issur.Acttime as Changed,
issur.IssId,
issur.IssuerName,
issur.IndusId,
issur.CntryOfIncorp,
issur.FinancialYearEnd,
issur.ShortName,
issur.LegalName,
issur.CntryOfDom,
issur.StateOfDom,
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN issur.Isstype='GOV' THEN 'G' WHEN issur.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype,
cast(issur.issurnotes as varchar(8000))
-- issur.IssurNotes
-- #case when issur.cntryofincorp='aa' then 's' when issur.isstype='gov' then 'g' when issur.isstype='govagency' then 'y' else 'c' end as debttype
from issur
inner join scmst on issur.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and ISSUR.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
