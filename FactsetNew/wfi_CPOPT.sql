--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('CPOPT') as Tablename,
cpopt.ActFlag,
cpopt.AnnounceDate as Created,
cpopt.Acttime as Changed,
cpopt.CpoptId,
scmst.SecId,
scmst.Isin,
cpopt.CallPut, 
cpopt.FromDate,
cpopt.ToDate,
cpopt.NoticeFrom,
cpopt.NoticeTo,
cpopt.Currency,
cpopt.Price,
cpopt.MandatoryOptional,
cpopt.MinNoticeDays,
cpopt.MaxNoticeDays,
cpopt.CpType,
cpopt.PriceAsPercent,
cpopt.InWholePart,
cpopt.FormulaBasedPrice
-- cpopn.Notes as Notes 
from cpopt
inner join bond on cpopt.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
-- left outer join cpopn on cpopt.secid = cpopn.secid and cpopt.callput = cpopn.callput
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and cpopt.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))