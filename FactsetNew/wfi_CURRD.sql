--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('CURRD') as Tablename,
currd.ActFlag,
currd.AnnounceDate as Created,
currd.Acttime as Changed,
currd.CurrdId,
currd.SecId,
scmst.Isin,
currd.EffectiveDate,
currd.OldCurencd,
currd.NewCurencd,
currd.OldParValue,
currd.NewParValue,
currd.EventType,
currd.CurrdNotes as Notes
from currd
inner join bond on currd.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and CURRD.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
and eventtype<>'CLEAN'
and eventtype<>'CORR'
