--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('IFCHG') as Tablename,
ifchg.ActFlag,
ifchg.AnnounceDate as Created,
ifchg.Acttime as Changed,
ifchg.IfchgId,
ifchg.SecId,
scmst.Isin,
ifchg.NotificationDate,
ifchg.OldintPayFrqncy,
ifchg.OldintPayDate1,
ifchg.OldintPayDate2,
ifchg.OldintPayDate3,
ifchg.OldintPayDate4,
ifchg.NewintPayFrqncy,
ifchg.NewintPayDate1,
ifchg.NewintPayDate2,
ifchg.NewintPayDate3,
ifchg.NewintPayDate4,
ifchg.EventType
from ifchg
inner join bond on ifchg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and ifchg.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
