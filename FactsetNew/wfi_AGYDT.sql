--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=EDI_AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select distinct
upper('AGYDT') as Tablename,
agydt.ActFlag,
agydt.Announcedate as Created,
agydt.Acttime as Changed,
agydt.AgydtId,
agydt.AgncyId,
agydt.EffectiveDate,
agydt.OldRegistrarName,
agydt.OldAdd1,
agydt.OldAdd2,
agydt.OldAdd3,
agydt.OldAdd4,
agydt.OldAdd5,
agydt.OldAdd6,
agydt.OldCity,
agydt.OldCntrycd,
agydt.OldWebsite,
agydt.OldContact1,
agydt.OldTel1,
agydt.OldFax1,
agydt.OldEmail1,
agydt.OldContact2,
agydt.OldTel2,
agydt.OldFax2,
agydt.OldEmail2,
agydt.OldState,
agydt.NewRegistrarName,
agydt.NewAdd1,
agydt.NewAdd2,
agydt.NewAdd3,
agydt.NewAdd4,
agydt.NewAdd5,
agydt.NewAdd6,
agydt.NewCity,
agydt.NewCntrycd,
agydt.NewWebsite,
agydt.NewContact1,
agydt.NewTel1,
agydt.NewFax1,
agydt.NewEmail1,
agydt.NewContact2,
agydt.NewTel2,
agydt.NewFax2,
agydt.NewEmail2,
agydt.NewState
from agydt
inner join scagy on agydt.agncyid = scagy.agncyid
inner join bond on scagy.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and agydt.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))