--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select distinct
'INTPY' as Tablename,
case when int.ActFlag = 'd' or int.ActFlag='c' or intpy.ActFlag is null then int.ActFlag else intpy.ActFlag end as ActFlag,
int.AnnounceDate as Created,
case when (rd.Acttime is not null) and (rd.Acttime > int.Acttime) and (rd.Acttime > exdt.Acttime) then rd.Acttime when (exdt.Acttime is not null) and (exdt.Acttime > int.Acttime) then exdt.Acttime else int.Acttime end as Changed,
scmst.SecId,
scmst.Isin,
intpy.BCurencd as DebtCurrency,
intpy.BParValue as NominalValue,
scexh.Exchgcd,
int.RdId,
case when intpy.optionid is null then '1' else intpy.optionid end as OptionID,
rd.RecDate,
exdt.ExDate,
exdt.PayDate,
int.InterestFromDate,
int.InterestToDate,
int.Days,
intpy.Curencd as Interest_Currency,
intpy.intrate as Interest_Rate,
intpy.GrossInterest,
intpy.NetInterest,
intpy.DomesticTaxRate,
intpy.NonResidentTaxRate,
intpy.RescindInterest,
intpy.AgencyFees,
intpy.CouponNo,
null as CouponId,
intpy.DefaultOpt,
intpy.OptElectionDate,
case when int.NilInt = 'T' then '0' else intpy.AnlCoupRate end as AnlCoupRate,
int.IndefPay
from [INT]
inner join rd on int.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join scexh on bond.secid = scexh.secid
left outer join exdt on rd.rdid = exdt.rdid 
     and scexh.exchgcd = exdt.exchgcd and 'int' = exdt.eventtype
     left outer join intpy on int.rdid = intpy.rdid
where
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(scmst.isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and (INTPY.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
or INT.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
or RD.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
or EXDT.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)))
