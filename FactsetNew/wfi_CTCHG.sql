--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('ctchg') as Tablename,
ctchg.ActFlag,
ctchg.AnnounceDate as Created,
ctchg.Acttime as Changed, 
ctchg.Ctchgid,
ctchg.SecId,
scmst.Isin,
ctchg.EffectiveDate,
ctchg.OldResultantRatio,
ctchg.NewResultantRatio,
ctchg.OldSecurityRatio,
ctchg.NewSecurityRatio,
ctchg.OldCurrency,
ctchg.NewCurrency,
ctchg.OldCurPair,
ctchg.NewCurPair,
ctchg.OldConversionPrice,
ctchg.NewConversionPrice,
ctchg.ResSectycd,
ctchg.OldResSecid,
ctchg.NewResSecid,
ctchg.EventType,
ctchg.ReleventId,
ctchg.OldFromDate,
ctchg.NewFromDate,
ctchg.OldToDate,
ctchg.NewToDate,
ctchg.ConvtId,
ctchg.OldFxRate,
ctchg.NewFxRate,
ctchg.OldPriceasPercent,
ctchg.NewPriceasPercent,
ctchg.Notes
from ctchg
inner join bond on ctchg.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and CTCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))

