--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('ISCHG') as Tablename,
ischg.ActFlag,
ischg.AnnounceDate as Created,
ischg.Acttime as Changed,
ischg.IschgId,
bond.SecId,
scmst.Isin,
ischg.IssId,
ischg.NameChangeDate,
ischg.IssOldName,
ischg.IssNewName,
ischg.EventType,
ischg.LegalName,
ischg.MeetingDateFlag, 
ischg.IschgNotes as Notes
from ischg
inner join scmst on ischg.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and ISCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
and (bond.issuedate<=ischg.namechangedate
     or bond.issuedate is null or ischg.namechangedate is null
    )

