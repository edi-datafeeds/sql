--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
upper('BBCC') as Tablename,
bbcc.ActFlag,
bbcc.AnnounceDate,
bbcc.Acttime,
bbcc.BbccId,
bbcc.SecId,
bbcc.BbcId,
bbcc.OldCntrycd,
bbcc.OldCurencd,
bbcc.EffectiveDate,
bbcc.NewCntrycd,
bbcc.NewCurencd,
bbcc.OldBbgcompId,
bbcc.NewBbgcompId,
bbcc.OldBbgcomptk,
bbcc.NewBbgcomptk,
bbcc.RelEventId,
bbcc.EventType,
bbcc.Notes
from bbcc
inner join bond on bbcc.secid = bond.secid
inner join scmst on bbcc.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and bbcc.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))