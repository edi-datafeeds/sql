--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_RCONV
--fileheadertext=EDI_RCONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('RCONV') as Tablename,
rconv.ActFlag,
rconv.AnnounceDate as Created,
rconv.Acttime as Changed,
rconv.RconvId,
rconv.SecId,
scmst.Isin,
rconv.EffectiveDate,
rconv.OldInterestAccrualConvention,
rconv.NewInterestAccrualConvention,
rconv.OldConvMethod,
rconv.NewConvMethod,
rconv.EventType,
rconv.Notes
from rconv
inner join bond on rconv.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and RCONV.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
