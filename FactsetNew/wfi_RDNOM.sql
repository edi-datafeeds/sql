--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('rdnom') as Tablename,
rdnom.ActFlag,
rdnom.AnnounceDate as Created,
rdnom.Acttime as Changed, 
rdnom.RdnomId,
rdnom.SecId,
scmst.Isin,
rdnom.EffectiveDate,
rdnom.OldDenomination1,
rdnom.OldDenomination2,
rdnom.OldDenomination3,
rdnom.OldDenomination4,
rdnom.OldDenomination5,
rdnom.OldDenomination6,
rdnom.OldDenomination7,
rdnom.OldMinimumDenomination,
rdnom.OldDenominationMultiple,
rdnom.NewDenomination1,
rdnom.NewDenomination2,
rdnom.NewDenomination3,
rdnom.NewDenomination4,
rdnom.NewDenomination5,
rdnom.NewDenomination6,
rdnom.NewDenomination7,
rdnom.NewMinimumDenomination,
rdnom.NewDenominationMultiple,
rdnom.Notes
from rdnom
inner join bond on rdnom.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and RDNOM.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
