--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select 
upper('REDMT') as Tablename,
redmt.ActFlag,
redmt.AnnounceDate as Created,
redmt.Acttime as Changed,
redmt.RedmtId,
scmst.SecId,
scmst.Isin,
redmt.RedemptionDate as RedemDate,
redmt.Curencd as RedemCurrency,
redmt.RedemptionPrice as RedemPrice,
redmt.MandOptFlag,
redmt.PartFinal,
redmt.RedemptionType as RedemType,
redmt.RedemptionAmount as RedemAmount,
redmt.RedemptionPremium as RedemPremium,
redmt.RedemInPercent,
redmt.PriceAsPercent,
redmt.PremiumAsPercent,
redmt.RedmtNotes as Notes
from redmt
inner join bond on redmt.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and redmt.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
