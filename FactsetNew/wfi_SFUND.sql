--filepath=O:\Datafeed\Debt\FactsetNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\FactsetNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select
upper('SFUND') as Tablename,
sfund.ActFlag,
sfund.AnnounceDate as Created,
sfund.Acttime as Changed,
sfund.SfundId,
sfund.SecId,
scmst.Isin,
sfund.SfundDate,
sfund.Curencd as Sinking_Currency,
sfund.Amount,
sfund.AmountAsPercent,
sfund.SfundNotes as Notes
from sfund
inner join bond on sfund.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I'))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and SFUND.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))

