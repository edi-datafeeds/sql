--fileheadertext=
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.641
--suffix=
--fileheadertext=EDI_BBG_641_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
select distinct
upper('BBC') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbcid,
main.secid,
main.cntrycd,
main.curencd,
main.bbgcompid,
main.bbgcomptk
from bbc as main
inner join scmst on main.secid = scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
main.actflag<>'D'
and scmst.actflag<>'D'

--# 2
use WCA
SELECT distinct
upper('BBE') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbeid,
main.secid,
main.exchgcd,
main.curencd,
main.bbgexhid,
main.bbgexhtk
from bbe as main
inner join bbc on main.secid = bbc.secid and substring(main.exchgcd,1,2) = bbc.cntrycd and main.curencd=bbc.curencd
inner join scmst on main.secid = scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
main.actflag<>'D'
and scmst.actflag<>'D'

--# 3
use WCA
SELECT
upper('BBCC') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbccid,
main.secid,
main.oldcntrycd,
main.oldcurencd,
main.effectivedate,
main.newcntrycd,
main.newcurencd,
main.oldbbgcompid,
main.newbbgcompid,
main.oldbbgcomptk,
main.newbbgcomptk,
main.releventid,
main.eventtype,
main.notes
FROM bbcc as main
inner join scmst on main.secid = scmst.secid
inner join bbc on main.bbccid = bbc.bbcid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where
scmst.actflag<>'D'
and main.actflag<>'D'

--# 4
use WCA
SELECT
upper('BBEC') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbecid,
main.secid,
main.bbeid,
main.oldexchgcd,
main.oldcurencd,
main.effectivedate,
main.newexchgcd,
main.newcurencd,
main.oldbbgexhid,
main.newbbgexhid,
main.oldbbgexhtk,
main.newbbgexhtk,
main.releventid,
main.eventtype,
main.notes
FROM bbec as main
inner join scmst on main.secid = scmst.secid
inner join bbe on main.bbeid = bbe.bbeid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
scmst.actflag<>'D'
and main.actflag<>'D'
