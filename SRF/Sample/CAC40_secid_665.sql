--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.665
--suffix=_CAC40_Secid_SampleData
--fileheadertext=EDI_STATIC_665_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\665\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT distinct
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp,
issur.CntryofDom
FROM issur
left outer join scmst on issur.issid = scmst.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE issur.actflag<>'D'
and scmst.secid in (select secid from icon.dbo.ixcon where ixnamid=11 and onindex='T')


--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.StructCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.NoParValue,
scmst.Voting,
scmst.Holding,
scmst.RegS144A,
scmst.VotePerSec,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM scmst
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join scexh on scmst.secid = scexh.secid
WHERE scmst.actflag<>'D'
and scmst.secid in (select secid from icon.dbo.ixcon where ixnamid=11 and onindex='T')


--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SedolId,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.CurenCD
FROM sedol
inner join scmst on sedol.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join scexh on sedol.secid = scexh.secid
WHERE sedol.actflag<>'D'
and scmst.secid in (select secid from icon.dbo.ixcon where ixnamid=11 and onindex='T')
and cntrycd in (select cntrycd from icon.dbo.ixnam where ixnamid=11)
and defunct='F'


--# 4
use WCA
SELECT distinct
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
substring(scexh.ExchgCD,1,2) as CntryCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.LocalCode,
scexh.MktsgID
FROM scexh
inner join scmst on scexh.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE scexh.actflag<>'D'
and scmst.secid in (select secid from icon.dbo.ixcon where ixnamid=11 and onindex='T')
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=11)
and 'D'<>scexh.liststatus



--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
WHERE exchg.actflag<>'D'
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=11)

