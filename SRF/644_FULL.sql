--filepath=o:\
--fileheadertext=
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.644
--suffix=
--fileheadertext=EDI_MKTSG_644_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select
mktsg.acttime as changed,
mktsg.actflag,
mktsg.mktsgid,
scexh.secid,
mktsg.exchgcd,
mktsegment,
mic
FROM mktsg
inner join scexh on mktsg.mktsgid = scexh.mktsgid
inner join scmst on scexh.secid = scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
where
mktsg.actflag <> 'd'