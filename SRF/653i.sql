--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog
--fileextension=.653
--suffix=
--fileheadertext=EDI_FLATSRF_653_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\653i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n

--# 1
declare @effective as datetime
set @effective=convert(varchar(4),year(getdate()))+'/'+convert(varchar(2),month(getdate()))+'/'+convert(varchar(2),day(getdate()))

use wca
select
exchg.cntrycd,
exchg.mic,
scmst.secid,
case when icc.secid is not null then icc.newisin else scmst.isin end as isin,
case when lcc.secid is not null then lcc.newlocalcode else scexh.localcode end as localcode,
case when ischg.issnewname<>'' and ischg.issnewname is not null then ischg.issnewname else issur.issuername end as issuername,
scmst.issid,
case when scchg.secnewname<>'' and scchg.secnewname is not null then scchg.secnewname else scmst.securitydesc end as securitydesc,
scexh.exchgcd,
case when scchg.newsectycd<>'' and scchg.newsectycd is not null then scchg.newsectycd else scmst.sectycd end as sectycd,
scmst.primaryexchgcd,
case when scmst.statusflag<>'I' then 'A' else 'I' end as globalscmststatus,
case when lstat.lstatstatus<>'' AND lstat.lstatstatus is not null then lstat.lstatstatus when scexh.liststatus is null then 'U' when scexh.liststatus='N' or scexh.liststatus='' then 'L' else scexh.liststatus end as wcaliststatus,
scmst.acttime as ScmstChanged,
scmst.actflag as ScmstRecordStatus,
scexh.actflag as ScexhRecordStatus
from scmst
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and sectygrp.secgrpid<3
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join icc on scmst.secid = icc.secid and @effective = icc.effectivedate
left outer join scchg on scmst.secid = scchg.secid and @effective = scchg.dateofchange
left outer join lcc on scmst.secid = lcc.secid and scexh.exchgcd = lcc.exchgcd and @effective = lcc.effectivedate
left outer join ischg on scmst.issid = ischg.issid and @effective = ischg.namechangedate
left outer join lstat on scmst.secid = lstat.secid and scexh.exchgcd = lstat.exchgcd and @effective = lstat.effectivedate
where 
(scmst.acttime >= (select max(acttime)-0.1 from wca.dbo.tbl_Opslog)
or issur.acttime >= (select max(acttime)-0.1 from wca.dbo.tbl_Opslog)
or scexh.acttime >= (select max(acttime)-0.1 from wca.dbo.tbl_Opslog))
