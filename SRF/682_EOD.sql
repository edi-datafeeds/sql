--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.682
--suffix=
--fileheadertext=EDI_682_SRF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\682\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select distinct
SCEXH.ScexhID,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.Sedol else '' end as Sedol,
SCEXH.Announcedate as ListingAdded,
SCEXH.Acttime as ListingChanged,
case when scexh.actflag is not null then scexh.actflag else scmst.actflag end as RecordStatus,
SCMST.SecID,
SCMST.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp,
ISSUR.IndusID,
case when scmst.statusflag='' then 'A' else scmst.statusflag end as IssueStatus,
SCMST.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as ParvalueCurrency,
SCMST.CFI,
SCMST.SectyCD,
SCMST.StructCD,
scmst.Isin,
scmst.Uscode,
SCMST.PrimaryExchgCD,
SCEXH.ExchgCD,
exchg.cntryCD as ListingCountry,
SCEXH.Localcode,
case when SCEXH.ListStatus='N' or SCEXH.ListStatus='' then 'L' else SCEXH.ListStatus end as ListStatus
FROM scexh
inner JOIN scmst ON scexh.SecID = scmst.SecID
left outer join ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join sedol on scexh.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
where
scmst.secid in (select code from client.dbo.pfsecid where accid = 997 and actflag<>'D')
and secgrpid<3
and scexh.actflag<>'D'
and scmst.actflag<>'D'
and issur.actflag<>'D'
and exchg.actflag<>'D'
--and (scmst.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3) 
--or scexh.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3) 
--or issur.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3)
--or sedol.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3))
