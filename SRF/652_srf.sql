--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.652
--suffix=
--fileheadertext=EDI_SRF_652_
--fileheaderdate=yymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\652\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select 
Levent,
EventID,
scexh.announcedate as Created,
scexh.acttime as Changed,
scexh.Actflag,
SCMST.IssID,
SCMST.SecID,
SCMST.SecurityDesc,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus
FROM scmst
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
inner join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd and 3>wca.dbo.sectygrp.secgrpid
where
issur.actflag<>'D'
and scmst.actflag<>'D'
and scexh.actflag<>'D'
