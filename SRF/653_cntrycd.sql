--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog
--fileextension=.653
--suffix=_US
--fileheadertext=EDI_FLATSRF_653_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\653i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
exchg.cntrycd,
exchg.mic,
scmst.secid,
scmst.isin,
scexh.localcode,
issur.issuername,
scmst.issid,
scmst.securitydesc,
scexh.exchgcd,
scmst.sectycd,
scmst.primaryexchgcd,
case when scmst.statusflag<>'I' then 'A' else 'I' end as globalscmststatus,
scexh.liststatus as wcaliststatus,
sedol.sedol,
sedol.curencd as TradingCurrency,
scmst.acttime as ScmstChanged,
scmst.actflag as ScmstRecordStatus,
scexh.actflag as ScexhRecordStatus
from scmst
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and sectygrp.secgrpid<3
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid and exchg.cntrycd=sedol.cntrycd and 'F'=sedol.defunct and 'D'<>sedol.actflag
where 
scmst.actflag <>'D'
and issur.actflag <>'D'
and scexh.actflag <>'D'
and exchg.cntrycd='US'
and scexh.liststatus<>'D'
