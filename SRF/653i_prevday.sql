--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.653
--suffix=
--fileheadertext=EDI_FLATSRF_653_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog where seq=3 order by acttime desc
--sevent=n
--shownulls=n

--# 1
use wca
select
exchg.cntrycd,
exchg.mic,
scmst.secid,
scmst.isin,
scexh.localcode,
issur.issuername,
scmst.issid,
scmst.securitydesc,
scexh.exchgcd,
scmst.sectycd,
scmst.primaryexchgcd,
case when scmst.statusflag<>'I' then 'A' else 'I' end as globalscmststatus,
case when scexh.liststatus is null then 'U' when scexh.liststatus='N' or scexh.liststatus='' then 'L' else scexh.liststatus end as wcaliststatus,
scmst.acttime as ScmstChanged,
scmst.actflag as ScmstRecordStatus,
scexh.actflag as ScexhRecordStatus
from scmst
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and sectygrp.secgrpid<3
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
where 
(scmst.acttime = (select max(acttime) from wca.dbo.tbl_Opslog where seq=3)
or issur.acttime = (select max(acttime) from wca.dbo.tbl_Opslog where seq=3)
or scexh.acttime = (select max(acttime) from wca.dbo.tbl_Opslog where seq=3))
