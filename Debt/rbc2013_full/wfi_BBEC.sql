--filepath=O:\datafeed\debt\rbcNewFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=2
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT
upper('BBEC') as tablename,
BBEC.actflag,
BBEC.announcedate,
BBEC.acttime,
BBEC.bbecid,
BBEC.secid,
BBEC.bbeid,
BBEC.oldexchgcd,
BBEC.oldcurencd,
BBEC.effectivedate,
BBEC.newexchgcd,
BBEC.newcurencd,
BBEC.oldbbgexhid,
BBEC.newbbgexhid,
BBEC.oldbbgexhtk,
BBEC.newbbgexhtk,
BBEC.releventid,
BBEC.eventtype,
BBEC.notes
FROM bbec as BBEC
inner join scmst on bbec.secid = scmst.secid
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and AGM.Actflag <>'d'
--and BBEC.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
