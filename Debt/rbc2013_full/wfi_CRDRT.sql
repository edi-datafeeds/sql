--filepath=O:\datafeed\debt\rbcNewFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=2
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=EDI_CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CRDRT') as TableName,
CRDRT.Actflag,
CRDRT.AnnounceDate as Created,
CRDRT.Acttime as Changed, 
CRDRT.SecID,
CRDRT.RatingAgency,
CRDRT.RatingDate,
CRDRT.Rating,
CRDRT.Direction,
CRDRT.WatchList,
CRDRT.WatchListReason
FROM CRDRT
INNER JOIN BOND ON CRDRT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and CRDRT.Actflag <>'d'
--and CRDRT.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
