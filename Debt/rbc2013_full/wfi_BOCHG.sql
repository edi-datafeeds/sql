--filepath=O:\datafeed\debt\rbcNewFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=2
--fileextension=.txt
--suffix=_BOCHG
--fileheadertext=EDI_BOCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BOCHG') as TableName,
BOCHG.Actflag,
BOCHG.AnnounceDate as Created,
BOCHG.Acttime as Changed,  
BOCHG.BochgID,
BOCHG.RelEventID,
BOCHG.SecID,
SCMST.ISIN,
BOCHG.EffectiveDate,
BOCHG.OldOutValue,
BOCHG.NewOutValue,
BOCHG.EventType,
BOCHG.OldOutDate,
BOCHG.NewOutDate,
BOCHG.BochgNotes as Notes
FROM BOCHG
INNER JOIN BOND ON BOCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and BOCHG.Actflag <>'d'
--and BOCHG.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
