--filepath=O:\datafeed\debt\rbcNewFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=2
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT distinct
upper('BBE') as tablename,
bbe.actflag,
bbe.announcedate,
bbe.acttime,
bbe.bbeid,
bbe.secid,
bbe.exchgcd,
bbe.curencd,
bbe.bbgexhid,
bbe.bbgexhtk
from bbe
inner join scmst on bbe.secid = scmst.secid
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and AGM.Actflag <>'d'
--and BBE.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
