--filepath=O:\datafeed\debt\rbcNewFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=2
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT
upper('BBCC') as tablename,
BBCC.actflag,
BBCC.announcedate,
BBCC.acttime,
BBCC.bbccid,
BBCC.secid,
BBCC.bbcid,
BBCC.oldcntrycd,
BBCC.oldcurencd,
BBCC.effectivedate,
BBCC.newcntrycd,
BBCC.newcurencd,
BBCC.oldbbgcompid,
BBCC.newbbgcompid,
BBCC.oldbbgcomptk,
BBCC.newbbgcomptk,
BBCC.releventid,
BBCC.eventtype,
BBCC.notes
FROM BBCC
inner join scmst on bbcc.secid = scmst.secid
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and AGM.Actflag <>'d'
--and BBCC.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
