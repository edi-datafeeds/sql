--filepath=O:\datafeed\debt\rbcNewFull\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=2
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate as Created,
ISSUR.Acttime as Changed,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
ISSUR.Shortname,
ISSUR.LegalName,
ISSUR.CntryofDom,
ISSUR.StateofDom,
cast(ISSUR.IssurNotes As varchar(8000)),
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN ISSUR.Isstype='GOV' THEN 'G' WHEN ISSUR.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and ISSUR.Actflag <>'d'
--and ISSUR.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
