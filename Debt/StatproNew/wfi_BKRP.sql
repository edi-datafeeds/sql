--filepath=O:\Datafeed\Debt\StatproNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=EDI_BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\StatproNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('BKRP') as Tablename,
wca.bkrp.ActFlag,
wca.bkrp.AnnounceDate as Created,
wca.bkrp.Acttime as Changed,  
wca.bkrp.BkrpId,
wca.bond.SecId,
wca.scmst.Isin,
wca.bkrp.IssId,
wca.bkrp.NotificationDate,
wca.bkrp.FilingDate
from wca.bkrp
inner join wca.scmst on wca.bkrp.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.bkrp.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)