--filepath=O:\Datafeed\Debt\StatproNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\StatproNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('ISCHG') as Tablename,
wca.ischg.ActFlag,
wca.ischg.AnnounceDate as Created,
wca.ischg.Acttime as Changed,
wca.ischg.IschgId,
wca.bond.SecId,
wca.scmst.Isin,
wca.ischg.IssId,
wca.ischg.NameChangeDate,
wca.ischg.IssOldName,
wca.ischg.IssNewName,
wca.ischg.EventType,
wca.ischg.LegalName,
wca.ischg.MeetingDateFlag, 
wca.ischg.IschgNotes as Notes
from wca.ischg
inner join wca.scmst on wca.ischg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(wca.bond.issuedate<=wca.ischg.namechangedate
     or wca.bond.issuedate is null or wca.ischg.namechangedate is null
    )
and wca.ischg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)


