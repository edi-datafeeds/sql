--filepath=O:\Datafeed\Debt\StatproNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\StatproNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('LIQ') as Tablename,
wca.liq.ActFlag,
wca.liq.AnnounceDate as Created,
wca.liq.Acttime as Changed,
wca.liq.LiqId,
wca.bond.SecId,
wca.scmst.Isin,
wca.liq.IssId,
wca.liq.Liquidator,
wca.liq.LiqAdd1,
wca.liq.LiqAdd2,
wca.liq.LiqAdd3,
wca.liq.LiqAdd4,
wca.liq.LiqAdd5,
wca.liq.LiqAdd6,
wca.liq.LiqCity,
wca.liq.LiqCntrycd,
wca.liq.LiqTel,
wca.liq.LiqFax,
wca.liq.LiqEmail,
wca.liq.RdDate
from wca.liq
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.liq.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)