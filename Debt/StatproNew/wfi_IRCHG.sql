--filepath=O:\Datafeed\Debt\StatproNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\StatproNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('IRCHG') as Tablename,
wca.irchg.ActFlag,
wca.irchg.AnnounceDate as Created,
wca.irchg.Acttime as Changed,
wca.irchg.IrchgId,
wca.irchg.SecId,
wca.scmst.Isin,
wca.irchg.EffectiveDate,
wca.irchg.OldInterestRate,
wca.irchg.NewInterestRate,
wca.irchg.EventType,
wca.irchg.Notes 
from wca.irchg
inner join wca.bond on wca.irchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.irchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)