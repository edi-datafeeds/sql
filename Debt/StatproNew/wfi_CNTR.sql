--filepath=O:\Datafeed\Debt\StatproNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_CNTR
--fileheadertext=EDI_CNTR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\StatproNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CNTR') as Tablename,
wca.cntr.ActFlag,
wca.cntr.Announcedate as Created,
wca.cntr.Acttime as Changed,
wca.cntr.CntrId,
wca.cntr.CentreName,
wca.cntr.Cntrycd
from wca.cntr
#where
#
#wca.cntr.actflag<>'d'
#wca.cntr.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where
