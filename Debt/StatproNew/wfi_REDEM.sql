--filepath=O:\Datafeed\Debt\StatproNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\StatproNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
'REDEM' as Tablename,
wca.redem.Actflag,
wca.redem.Announcedate as Created,
case when (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.redem.Acttime) then wca.rd.Acttime else wca.redem.Acttime end as Changed,
wca.redem.Redemid,
wca.scmst.Secid,
wca.scmst.Isin,
wca.rd.RecDate,
wca.redem.RedemDate,
wca.redem.Curencd as RedemCurrency,
wca.redem.RedemPrice,
wca.redem.MandOptFlag,
wca.redem.PartFinal,
wca.redem.RedemType,
wca.redem.AmountRedeemed,
wca.redem.RedemPremium,
wca.redem.RedemPercent,
-- redem.poolfactor,
CASE  WHEN  instr(redem.poolfactor,'.') < 5
                THEN  substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+9)
                ELSE substring(redem.poolfactor,1,instr(redem.poolfactor,'.')+8)
                END AS poolfactor,

wca.redem.PriceAsPercent,
wca.redem.PremiumAsPercent,
wca.redem.IndefPay,
wca.redem.TenderOpenDate,
wca.redem.TenderCloseDate,
wca.redem.RedemNotes as Notes
from wca.redem
inner join wca.bond on wca.redem.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.redem.rdid = wca.rd.rdid
where
wca.redem.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)