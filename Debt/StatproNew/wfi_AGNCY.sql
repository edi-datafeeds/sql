--filepath=O:\Datafeed\Debt\StatproNew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 2), '%Y%m%d')
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\StatproNew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('AGNCY') as Tablename,
wca.agncy.ActFlag,
wca.agncy.Announcedate as Created,
wca.agncy.Acttime as Changed,
wca.agncy.Agncyid,
wca.agncy.RegistrarName,
wca.agncy.Add1,
wca.agncy.Add2,
wca.agncy.Add3,
wca.agncy.Add4,
wca.agncy.Add5,
wca.agncy.Add6,
wca.agncy.City,
wca.agncy.Cntrycd,
wca.agncy.Website,
wca.agncy.Contact1,
wca.agncy.Tel1,
wca.agncy.Fax1,
wca.agncy.Email1,
wca.agncy.Contact2,
wca.agncy.Tel2,
wca.agncy.Fax2,
wca.agncy.Email2,
wca.agncy.Depository,
wca.agncy.State
from wca.agncy
inner join wca.scagy on wca.agncy.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.agncy.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)