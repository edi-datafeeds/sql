--filepath=o:\upload\acc\192\feednew\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=
--EDI_wfisample_INTBC_
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feednew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 

--# 
use wca
select
upper('INTBC') as TableName,
INTBC.Actflag,
INTBC.AnnounceDate as Created,
INTBC.Acttime as Changed,
INTBC.IntbcID,
INTBC.SecID,
SCMST.ISIN,
INTBC.EffectiveDate,
INTBC.OldIntBasis,
INTBC.NewIntBasis
FROM INTBC
INNER JOIN BOND ON INTBC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='U')
and INTBC.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='I')
or SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 192 and actflag='I')
