--filepath=o:\upload\acc\192\feednew\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_ICC
--fileheadertext=
--EDI_wfisample_ICC_
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feednew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 

--# 
use wca
select
upper('ICC') as TableName,
ICC.Actflag,
ICC.AnnounceDate as Created,
ICC.Acttime as Changed,
ICC.IccID,
ICC.SecID,
SCMST.ISIN,
ICC.EffectiveDate,
ICC.OldISIN,
ICC.NewISIN,
ICC.OldUSCode,
ICC.NewUSCode,
ICC.OldVALOREN,
ICC.NewVALOREN,
ICC.EventType,
ICC.RelEventID,
ICC.OldCommonCode,
ICC.NewCommonCode
FROM ICC
INNER JOIN BOND ON ICC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='U')
and ICC.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'
)
or SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='I')
or SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 192 and actflag='I')
