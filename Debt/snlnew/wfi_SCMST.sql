--filepath=o:\upload\acc\192\feednew\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=
--EDI_wfisample_SCMST_
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feednew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate as Created,
SCMST.Acttime as Changed,
SCMST.SecID,
SCMST.ISIN,
SCMST.IssID,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
case when len(SCMST.X)=9 then SCMST.X else '' end as CommonCode,
SCMST.Holding,
SCMST.StructCD,
SCMST.RegS144A
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='U')
and (bond.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
     or scmst.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)))
or SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='I')
or SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 192 and actflag='I')
