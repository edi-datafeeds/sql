--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_WFIPORT_188_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('TRNCH') as TableName,
TRNCH.Actflag,
TRNCH.AnnounceDate as Created,
TRNCH.Acttime as Changed,
TRNCH.TrnchNumber,
TRNCH.SecID,
SCMST.ISIN,
TRNCH.TrancheDate,
TRNCH.TrancheAmount,
TRNCH.ExpiryDate,
TRNCH.Notes as Notes
FROM TRNCH
INNER JOIN BOND ON TRNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(
TRNCH.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)