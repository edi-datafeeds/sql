--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_WFIPORT_188_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT DISTINCT
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate as Created,
ISSUR.Acttime as Changed,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
'' as HOAdd1,
'' as HOAdd2,
'' as HOAdd3,
'' as HOAdd4,
'' as HOAdd5,
'' as HOAdd6,
'' as HOCity,
'' as HOCntryCD,
'' as HOTel,
'' as HOFax,
'' as HOEmail,
'' as ROAdd1,
'' as ROAdd2,
'' as ROAdd3,
'' as ROAdd4,
'' as ROAdd5,
'' as ROAdd6,
'' as ROCity,
'' as ROCntryCD,
'' as ROTel,
'' as ROFax,
'' as ROEmail,
'' as website,
'' as Chairman,
'' as MD,
'' as CS,
ISSUR.Shortname,
ISSUR.LegalName
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(
ISSUR.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)

