--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_WFIPORT_188_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BOND') as Tablename,
BOND.Actflag,
BOND.AnnounceDate as Created,
BOND.Acttime as Changed,
BOND.SecID,
SCMST.ISIN,
BOND.BondType,
BOND.DebtMarket,
BOND.CurenCD as DebtCurrency,
BOND.ParValue as NominalValue,
BOND.IssueDate,
BOND.IssueCurrency,
BOND.IssuePrice,
BOND.IssueAmount,
BOND.IssueAmountDate,
BOND.OutstandingAmount,
BOND.OutstandingAmountDate,
BOND.InterestBasis,
BOND.InterestRate,
BOND.InterestAccrualConvention,
BOND.InterestPaymentFrequency,
BOND.IntCommencementDate as InterestCommencementDate,
BOND.FirstCouponDate,
BOND.InterestPayDate1,
BOND.InterestPayDate2,
BOND.InterestPayDate3,
BOND.InterestPayDate4,
bondx.DomesticTaxRate,
bondx.NonResidentTaxRate,
BOND.FRNType,
BOND.FRNIndexBenchmark,
BOND.Markup as FrnMargin,
BOND.MinimumInterestRate as FrnMinInterestRate,
BOND.MaximumInterestRate as FrnMaxInterestRate,
BOND.Rounding as FrnRounding,
bondx.Series,
bondx.Class,
bondx.OnTap,
bondx.MaximumTapAmount,
bondx.TapExpiryDate,
BOND.Guaranteed,
BOND.SecuredBy,
BOND.SecurityCharge,
BOND.Subordinate,
BOND.SeniorJunior,
BOND.WarrantAttached,
BOND.MaturityStructure,
BOND.Perpetual,
BOND.MaturityDate,
BOND.MaturityExtendible,
BOND.Callable,
BOND.Puttable,
bondx.Denomination1,
bondx.Denomination2,
bondx.Denomination3,
bondx.Denomination4,
bondx.Denomination5,
bondx.Denomination6,
bondx.Denomination7,
bondx.MinimumDenomination,
bondx.DenominationMultiple,
BOND.Strip,
BOND.StripInterestNumber, 
BOND.Bondsrc,
BOND.MaturityBenchmark,
BOND.ConventionMethod,
BOND.FrnIntAdjFreq as FrnInterestAdjFreq,
BOND.IntBusDayConv as InterestBusDayConv,
BOND.InterestCurrency,
BOND.MatBusDayConv as MaturityBusDayConv,
BOND.MaturityCurrency, 
bondx.TaxRules,
BOND.VarIntPayDate as VarInterestPaydate,
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
BOND.MatPrice as MaturityPrice,
BOND.MatPriceAsPercent as MaturityPriceAsPercent,
BOND.SinkingFund,
bondx.GoverningLaw,
BOND.Notes
FROM BOND
inner join scmst on bond.secid = scmst.secid
left outer join bondx on bond.secid = bondx.secid
where
(
BOND.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)