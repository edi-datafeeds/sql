print " GENERATING WFI.dbo.eu2bm, please wait..."
go

use WFI
if exists (select * from sysobjects where name = 'eu2bm')
 drop table eu2bm
use wca
select cpopt.secid, count(secid) as ct
into WFI.dbo.eu2bm
from cpopt
where cptype = 'EU'
and cpopt.actflag <> 'D'
GROUP BY cpopt.secid
order by count(secid)
go

print ""
go

print " INDEXING WFI.dbo.eu2bm ,please  wait ....."
GO 
use WFI
ALTER TABLE eu2bm ALTER COLUMN  secid bigint NOT NULL
GO 
use WFI
ALTER TABLE [DBO].[eu2bm] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_eu2bm] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 
