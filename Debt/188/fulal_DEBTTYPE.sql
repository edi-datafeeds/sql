--filepath=o:\Datafeed\Debt\MKREP\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_DEBTTYPE
--fileheadertext=EDI_DEBT_DEBTTYPE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\MKREP\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WFI
SELECT distinct
upper('DEBTTYPE') as TableName,
DEBT_TYPE.IssID,
DEBT_TYPE.Debt_type
FROM 
DEBT_TYPE
