--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_TENDER
--fileheadertext=EDI_WFIPORT_188_TENDER_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'TENDER' as TableName,
REDEM.Actflag,
REDEM.AnnounceDate as Created,
REDEM.Acttime as [Changed],
REDEM.RedemID as TenderID,
SCMST.SecID,
SCMST.ISIN,
REDEM.RedemDate,
REDEM.CurenCD as TenderCurrency,
REDEM.RedemPrice as TenderPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.AmountRedeemed as TenderAmount,
REDEM.RedemPremium as TenderPremium,
REDEM.RedemPercent as TenderPercent,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
REDEM.PriceAsPercent,
REDEM.PremiumAsPercent,
REDEM.RedemNotes as Notes
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
(REDEM.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))