--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_WFIPORT_188_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('MPAY') as TableName,
MPAY.Actflag,
MPAY.AnnounceDate,
MPAY.Acttime,
MPAY.sEvent as EventType,
MPAY.EventID,
MPAY.OptionID,
MPAY.SerialID,
MPAY.SectyCD as ResSectyCD,
MPAY.ResSecID,
RESSCMST.ISIN as ResISIN,
MPAY.RatioNew,
MPAY.RatioOld,
MPAY.Fractions,
MPAY.MinOfrQty,
MPAY.MaxOfrQty,
MPAY.MinQlyQty,
MPAY.MaxQlyQty,
MPAY.Paydate,
MPAY.CurenCD,
MPAY.MinPrice,
MPAY.MaxPrice,
MPAY.TndrStrkPrice,
MPAY.TndrStrkStep,
MPAY.Paytype,
MPAY.DutchAuction,
MPAY.DefaultOpt,
MPAY.OptElectionDate
FROM MPAY
inner join LIQ on 'LIQ' = MPAY.sEvent and LIQ.LiqID = MPAY.EventID
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
LEFT OUTER JOIN SCMST as RESSCMST ON MPAY.ResSecID = RESSCMST.SecID
where
(
MPAY.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)
