--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_WFIPORT_188_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('IRCHG') as TableName,
IRCHG.Actflag,
IRCHG.AnnounceDate as Created,
IRCHG.Acttime as Changed,
IRCHG.IrchgID,
IRCHG.SecID,
SCMST.ISIN,
IRCHG.EffectiveDate,
IRCHG.OldInterestRate,
IRCHG.NewInterestRate,
IRCHG.Eventtype,
IRCHG.Notes 
FROM IRCHG
INNER JOIN BOND ON IRCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(
IRCHG.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)