--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_WFIPORT_188_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CTCHG') as TableName,
CTCHG.Actflag,
CTCHG.AnnounceDate as Created,
CTCHG.Acttime as Changed, 
CTCHG.CtChgID,
CTCHG.SecID,
SCMST.ISIN,
CTCHG.EffectiveDate,
CTCHG.OldResultantRatio,
CTCHG.NewResultantRatio,
CTCHG.OldSecurityRatio,
CTCHG.NewSecurityRatio,
CTCHG.OldCurrency,
CTCHG.NewCurrency,
CTCHG.OldConversionPrice,
CTCHG.NewConversionPrice,
CTCHG.ResSectyCD,
CTCHG.OldResSecID,
CTCHG.NewResSecID,
CTCHG.EventType,
CTCHG.RelEventID,
CTCHG.OldFromDate,
CTCHG.NewFromDate,
CTCHG.OldTodate,
CTCHG.NewToDate,
CTCHG.ConvtID,
CTCHG.OldFXRate,
CTCHG.NewFXRate,
CTCHG.OldPriceAsPercent,
CTCHG.NewPriceAsPercent,
CTCHG.Notes
FROM CTCHG
INNER JOIN BOND ON CTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(
CTCHG.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)