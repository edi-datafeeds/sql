--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SELRT
--fileheadertext=EDI_WFIPORT_188_SELRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SELRT') as TableName,
SELRT.Actflag,
SELRT.AnnounceDate as Created,
SELRT.Acttime as Changed,
SELRT.SelrtID,
SELRT.SecID,
SCMST.ISIN,
SELRT.CntryCD,
SELRT.Restriction
FROM SELRT
INNER JOIN BOND ON SELRT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(
SELRT.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)