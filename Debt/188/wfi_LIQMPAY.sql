--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(feeddate) from tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_WFIPORT_188_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LIQ') as TableName,
LIQ.Actflag,
LIQ.AnnounceDate as Created,
LIQ.Acttime as Changed,
LIQ.LiqID,
LIQ.IssID,
LIQ.Liquidator,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,
MPAY.,

FROM LIQ
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SedID
left outer join mpay on liqid = mpay.eventid and 'LIQ' = sEvent
where
(
BOND.secid in (select portfolio.dbo.wfiport.secid from portfolio.dbo.wfiport)
and LIQ.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)
OR
(
BOND.secid in (select portfolio.dbo.wfihist.secid from portfolio.dbo.wfihist)
and LIQ.actflag<>'D'
)