--filepath=o:\Upload\Acc\188\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_WFIPORT_188_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\188\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('ISCHG') as TableName,
BOND.SecID,
SCMST.ISIN,
ISCHG.Actflag,
ISCHG.AnnounceDate as Created,
ISCHG.Acttime as Changed,
ISCHG.IschgID,
ISCHG.IssID,
ISCHG.NameChangeDate,
ISCHG.IssOldName,
ISCHG.IssNewName,
ISCHG.EventType,
ISCHG.LegalName,
ISCHG.MeetingDateFlag, 
ISCHG.IsChgNotes as Notes
FROM ISCHG
INNER JOIN SCMST ON ISCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(
ISCHG.acttime >= (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
)