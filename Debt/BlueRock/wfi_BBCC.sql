--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
upper('BBCC') as Tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate,
wca.bbcc.Acttime,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.bbcc.BbcId,
wca.bbcc.OldCntrycd,
wca.bbcc.OldCurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.NewCntrycd,
wca.bbcc.NewCurencd,
wca.bbcc.OldBbgcompId,
wca.bbcc.NewBbgcompId,
wca.bbcc.OldBbgcomptk,
wca.bbcc.NewBbgcomptk,
wca.bbcc.RelEventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.bond on wca.bbcc.secid = wca.bond.secid
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.bbcc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))

