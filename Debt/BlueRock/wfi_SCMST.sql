--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCMST') as Tablename,
wca.scmst.ActFlag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.SecId,
wca.scmst.Isin,
wca.scmst.ParentSecid,
wca.scmst.IssId,
wca.scmst.Sectycd,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgcd,
wca.scmst.Curencd,
wca.scmst.ParValue,
wca.scmst.UsCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.Structcd,
wca.scmst.Wkn,
wca.scmst.Regs144a,
wca.scmst.Cfi,
wca.scmst.ScmstNotes as Notes
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.bond.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))






