--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('LAWSTNOTES') as Tablename,
wca.lawst.ActFlag,
wca.lawst.LawstId,
wca.lawst.IssId,
wca.lawst.LawstNotes as Notes
from wca.lawst
where
lawst.Issid in (select issid from client.pfsedol
where (client.pfsedol.accid=244 and client.pfsedol.actflag='I')
or (client.pfsedol.accid=244 and client.pfsedol.actflag='U')
and lawst.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3 ))



