--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('LIQNOTES') as Tablename,
wca.liq.ActFlag,
wca.liq.LiqId,
wca.liq.IssId,
wca.liq.LiquidationTerms
from wca.liq
where
wca.liq.issid in (select issid from client.pfsedol
where (client.pfsedol.accid=244 and client.pfsedol.actflag='I')
or (client.pfsedol.accid=244 and client.pfsedol.actflag='U')
and liq.acttime> (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3 ))

