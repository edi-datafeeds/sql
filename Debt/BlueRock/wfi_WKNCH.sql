--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('WKNCH') as Tablename,
wca.wknch.ActFlag,
wca.wknch.AnnounceDate as Created,
wca.wknch.Acttime as Changed,
wca.wknch.SecId,
wca.wknch.EffectiveDate,
wca.wknch.WknchId,
wca.wknch.EventType,
wca.wknch.ReleventId,
wca.wknch.OldWkn,
wca.wknch.NewWkn
from wca.wknch
inner join wca.bond on wca.wknch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.wknch.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))

