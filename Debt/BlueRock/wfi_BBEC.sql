--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
upper('BBEC') as Tablename,
wca.bbec.Actflag,
wca.bbec.AnnounceDate,
wca.bbec.Acttime,
wca.bbec.BbecId,
wca.bbec.SecId,
wca.bbec.BbeId,
wca.bbec.OldExchgcd,
wca.bbec.OldCurencd,
wca.bbec.EffectiveDate,
wca.bbec.NewExchgcd,
wca.bbec.NewCurencd,
wca.bbec.OldBbgexhId,
wca.bbec.NewBbgexhId,
wca.bbec.OldBbgexhtk,
wca.bbec.NewBbgexhtk,
wca.bbec.RelEventId,
wca.bbec.EventType,
wca.bbec.Notes
from wca.bbec
inner join wca.bond on wca.bbec.secid = wca.bond.secid
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.bbec.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
