--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('MTCHG') as Tablename,
wca.mtchg.ActFlag,
wca.mtchg.AnnounceDate as Created,
wca.mtchg.Acttime as Changed,
wca.mtchg.MtchgId,
wca.mtchg.SecId,
wca.scmst.Isin,
wca.mtchg.NotificationDate,
wca.mtchg.OldMaturityDate,
wca.mtchg.NewMaturityDate,
wca.mtchg.Reason,
wca.mtchg.EventType,
wca.mtchg.OldMaturityBenchmark,
wca.mtchg.NewMaturityBenchmark,
wca.mtchg.Notes as Notes
from wca.mtchg
inner join wca.bond on wca.mtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.mtchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





