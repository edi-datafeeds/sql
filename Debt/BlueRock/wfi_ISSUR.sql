--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('ISSUR') as Tablename,
wca.issur.ActFlag,
wca.issur.AnnounceDate as Created,
wca.issur.Acttime as Changed,
wca.issur.IssId,
wca.issur.SIC,
wca.issur.CIK,
wca.issur.IssuerName,
wca.issur.IndusId,
wca.issur.CntryOfIncorp,
wca.issur.FinancialYearEnd,
wca.issur.ShortName,
wca.issur.LegalName,
wca.issur.CntryOfDom,
wca.issur.StateOfDom,
wca.issur.IssType,
#cast(wca.Issur.issurnotes as varchar(8000))
wca.issur.IssurNotes
#case when wca.issur.cntryofincorp='aa' then 's' when wca.issur.isstype='gov' then 'g' when wca.issur.isstype='govagency' then 'y' else 'c' end as debttype
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.issur.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





