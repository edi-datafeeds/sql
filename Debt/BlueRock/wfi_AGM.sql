--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('AGM') as Tablename,
wca.agm.Actflag,
wca.agm.Announcedate as Created,
wca.agm.Acttime as Changed, 
wca.agm.AgmId,
wca.bond.SecId,
wca.scmst.Isin,
wca.agm.IssId,
wca.agm.AgmDate,
wca.agm.AgmEgm,
wca.agm.AgmNo,
wca.agm.FyeDate,
wca.agm.AgmTime,
wca.agm.Add1,
wca.agm.Add2,
wca.agm.Add3,
wca.agm.Add4,
wca.agm.Add5,
wca.agm.Add6,
wca.agm.City,
wca.agm.Cntrycd,
wca.agm.BondSecid
from wca.agm
inner join wca.scmst on wca.agm.bondsecid = wca.scmst.secid
inner join wca.bond on wca.agm.bondsecid = wca.bond.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.agm.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
