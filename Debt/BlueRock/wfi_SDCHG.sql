--filepath=H:\y.laifa\FixedIncome\Feeds\BlueRock\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=H:\y.laifa\FixedIncome\Feeds\FIDA\Output\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SDCHG') as Tablename,
wca.sdchg.ActFlag,
wca.sdchg.AnnounceDate as Created, 
wca.sdchg.Acttime as Changed,
wca.sdchg.SdchgId,
wca.sdchg.SecId,
wca.scmst.Isin,
wca.sdchg.Cntrycd as OldCntrycd,
wca.sdchg.EffectiveDate,
wca.sdchg.OldSedol,
wca.sdchg.NewSedol,
wca.sdchg.EventType,
wca.sdchg.RCntrycd as OldrCntrycd,
wca.sdchg.ReleventId,
wca.sdchg.NewCntrycd,
wca.sdchg.NewrCntrycd,
wca.sdchg.SdchgNotes as Notes
from wca.sdchg
inner join wca.bond on wca.sdchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.sedol on wca.bond.SecID = wca.sedol.SecID
where
sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='I')
or 
(sedol.sedol in (select client.pfsedol.code from client.pfsedol where accid = 244 and actflag='U')
and wca.sdchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





