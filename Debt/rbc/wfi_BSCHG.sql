--filepath=o:\datafeed\debt\rbc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_BSCHG
--fileheadertext=EDI_BSCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n 
--ZEROROWCHK=N

--# 
use wca
select
upper('BSCHG') as TableName,
BSCHG.Actflag,
BSCHG.AnnounceDate as Created,
BSCHG.Acttime as Changed,
BSCHG.BschgID,
BSCHG.SecID,
SCMST.ISIN,
BSCHG.NotificationDate,
BSCHG.OldBondType,
BSCHG.NewBondType,
BSCHG.OldCurenCD,
BSCHG.NewCurenCD,
BSCHG.OldPIU,
BSCHG.NewPIU,
BSCHG.OldInterestBasis,
BSCHG.NewInterestBasis,
BSCHG.Eventtype,
BSCHG.OldInterestCurrency,
BSCHG.NewInterestCurrency,
BSCHG.OldMaturityCurrency,
BSCHG.NewMaturityCurrency,
BSCHG.OldIntBusDayConv,
BSCHG.NewIntBusDayConv,
BSCHG.OldMatBusDayConv,
BSCHG.NewMatBusDayConv,
BSCHG.BschgNotes as Notes
FROM BSCHG
INNER JOIN BOND ON BSCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and BSCHG.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)
