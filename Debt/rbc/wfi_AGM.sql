--filepath=o:\datafeed\debt\rbc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n 
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('AGM') as TableName,
AGM.Actflag,
AGM.AnnounceDate as Created,
AGM.Acttime as Changed, 
AGM.AGMID,
AGM.BondSecID as SecID,
SCMST.ISIN,
AGM.IssID,
AGM.AGMDate,
AGM.AGMEGM,
AGM.AGMNo,
AGM.FYEDate,
AGM.AGMTime,
AGM.Add1,
AGM.Add2,
AGM.Add3,
AGM.Add4,
AGM.Add5,
AGM.Add6,
AGM.City,
AGM.CntryCD
FROM AGM
INNER JOIN BOND ON AGM.BondSecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and AGM.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)

union

SELECT 
upper('AGM') as TableName,
AGM.Actflag,
AGM.AnnounceDate as Created,
AGM.Acttime as Changed, 
AGM.AGMID,
BOND.SecID,
SCMST.ISIN,
AGM.IssID,
AGM.AGMDate,
AGM.AGMEGM,
AGM.AGMNo,
AGM.FYEDate,
AGM.AGMTime,
AGM.Add1,
AGM.Add2,
AGM.Add3,
AGM.Add4,
AGM.Add5,
AGM.Add6,
AGM.City,
AGM.CntryCD
FROM AGM
INNER JOIN SCMST ON AGM.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and AGM.BondSecID is null
and AGM.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)
