--filepath=o:\datafeed\debt\rbc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n 
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SDCHG') as TableName,
SDCHG.Actflag,
SDCHG.AnnounceDate as Created, 
SDCHG.Acttime as Changed,
SDCHG.SdChgID,
SDCHG.SecID,
SCMST.ISIN,
SDCHG.EffectiveDate,
SDCHG.EventType,
SDCHG.OldSedol,
SDCHG.NewSedol,
SDCHG.CntryCD as OldCntryCD,
SDCHG.NewCntryCD,
SDCHG.RcntryCD as OldRcntryCD,
SDCHG.NewRcntryCD
FROM SDCHG
INNER JOIN BOND ON SDCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and SDCHG.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)
