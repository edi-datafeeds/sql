--filepath=o:\datafeed\debt\rbc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n 
--ZEROROWCHK=N

--# 
use WCA
select
upper('MPAY') as TableName,
MPAY.Actflag,
MPAY.AnnounceDate,
MPAY.Acttime,
MPAY.sEvent as EventType,
MPAY.EventID,
MPAY.OptionID,
MPAY.SerialID,
MPAY.SectyCD as ResSectyCD,
MPAY.ResSecID,
RESSCMST.ISIN as ResISIN,
MPAY.RatioNew,
MPAY.RatioOld,
MPAY.Fractions,
MPAY.MinOfrQty,
MPAY.MaxOfrQty,
MPAY.MinQlyQty,
MPAY.MaxQlyQty,
MPAY.Paydate,
MPAY.CurenCD,
MPAY.MinPrice,
MPAY.MaxPrice,
MPAY.TndrStrkPrice,
MPAY.TndrStrkStep,
MPAY.Paytype,
MPAY.DutchAuction,
MPAY.DefaultOpt,
MPAY.OptElectionDate
FROM MPAY
inner join LIQ on 'LIQ' = MPAY.sEvent and LIQ.LiqID = MPAY.EventID
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
LEFT OUTER JOIN SCMST as RESSCMST ON MPAY.ResSecID = RESSCMST.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and MPAY.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)
