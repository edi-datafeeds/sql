--filepath=o:\datafeed\debt\rbc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_CONV
--fileheadertext=EDI_CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n 
--ZEROROWCHK=N

--# 1
use wca
select
'CONV' as TableName,
CONV.Actflag,
CONV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONV.Acttime) THEN RD.Acttime ELSE CONV.Acttime END as [Changed],
CONV.ConvID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
CONV.FromDate,
CONV.ToDate,
CONV.RatioNew,
CONV.RatioOld,
CONV.CurenCD as ConvCurrency,
CONV.Price,
CONV.MandOptFlag,
CONV.Fractions,
CONV.FXrate,
CONV.PartFinalFlag,
CONV.ConvType,
CONV.PriceAsPercent,
CONV.ResSectyCD,
CONV.ResSecID,
RESSCMST.ISIN as ResISIN,
CONV.ConvNotes as Notes
FROM CONV
INNER JOIN BOND ON CONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON CONV.RdID = RD.RdID
LEFT OUTER JOIN SCMST as RESSCMST ON CONV.ResSecID = RESSCMST.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and Convtype<>'TENDER'
and CONV.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)
