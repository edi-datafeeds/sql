--filepath=o:\datafeed\debt\rbc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n 
--ZEROROWCHK=N

--# 
use WCA
SELECT DISTINCT
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate as Created,
ISSUR.Acttime as Changed,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
'' as HOAdd1,
'' as HOAdd2,
'' as HOAdd3,
'' as HOAdd4,
'' as HOAdd5,
'' as HOAdd6,
'' as HOCity,
'' as HOCntryCD,
'' as HOTel,
'' as HOFax,
'' as HOEmail,
'' as ROAdd1,
'' as ROAdd2,
'' as ROAdd3,
'' as ROAdd4,
'' as ROAdd5,
'' as ROAdd6,
'' as ROCity,
'' as ROCntryCD,
'' as ROTel,
'' as ROFax,
'' as ROEmail,
'' as website,
'' as Chairman,
'' as MD,
'' as CS,
ISSUR.Shortname,
ISSUR.LegalName
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and ISSUR.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)
