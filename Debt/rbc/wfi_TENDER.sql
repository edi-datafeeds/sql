--filepath=o:\datafeed\debt\rbc\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_TENDER
--fileheadertext=EDI_TENDER_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\debt\rbc\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n 
--ZEROROWCHK=N

--# 1
use wca
select
'TENDER' as TableName,
REDEM.Actflag,
REDEM.AnnounceDate as Created,
REDEM.Acttime as [Changed],
REDEM.RedemID as TenderID,
SCMST.SecID,
SCMST.ISIN,
REDEM.RedemDate,
REDEM.CurenCD as TenderCurrency,
REDEM.RedemPrice as TenderPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.AmountRedeemed as TenderAmount,
REDEM.RedemPremium as TenderPremium,
REDEM.RedemPercent as TenderPercent,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
REDEM.PriceAsPercent,
REDEM.PremiumAsPercent,
REDEM.RedemNotes as Notes
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and redemtype='TENDER'
and REDEM.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 2)
