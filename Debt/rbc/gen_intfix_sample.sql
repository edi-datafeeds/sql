use portfolio
delete from intfix_sample
go

use wca
declare @mycount as integer
set @mycount=-cast(getdate()-'2009/09/01' as int)

WHILE @mycount<30
BEGIN

use wca

insert into portfolio.dbo.intfix_sample

select
upper('INTALERT') as Event_Name,
bond.secid,
scmst.isin,
convert(varchar(30),getdate()+@mycount, 112) as Payable_Date,
IssueDate as Issue_Date,
MaturityDate as Maturity_Date,
IntCommencementdate as Interest_Commencement_Date,
FirstCouponDate as First_Coupon_Date,
case when bond.ParValue<>'' then bond.ParValue else scmst.Parvalue end as Par_Value,
case when bond.interestcurrency <>'' then bond.interestcurrency else bond.curencd end as Currency,
bond.interestrate as Interest_Rate,
bond.interestpaymentfrequency as Frequency,
bond.interestaccrualconvention as Accrual_Convention,
bond.InterestBasis as Interest_Basis,
bond.intbusdayconv as Interest_Bus_Day_Convention
from bond
inner join scmst on bond.secid = scmst.secid
where
(substring(scmst.isin,1,2)='CA' 
or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D')
or SCMST.issid in (select issid from ISSUR where cntryofincorp='CA' and actflag<>'D'))
and bond.acttime> (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and scmst.acttime> (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (
convert(varchar(4),year(getdate()+@mycount))+substring(Interestpaydate1,3,2)+substring(Interestpaydate1,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
convert(varchar(4),year(getdate()+@mycount))+substring(Interestpaydate2,3,2)+substring(Interestpaydate2,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
convert(varchar(4),year(getdate()+@mycount))+substring(Interestpaydate3,3,2)+substring(Interestpaydate3,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
convert(varchar(4),year(getdate()+@mycount))+substring(Interestpaydate4,3,2)+substring(Interestpaydate4,1,2)=
 convert(varchar(30),getdate()+@mycount, 112)
or
convert(varchar(30),firstcoupondate, 111)=convert(varchar(30),getdate()+@mycount, 111)
)

and bond.interestrate<>''
and bond.curencd<>''
and (bond.maturitydate>getdate()+@mycount or (bond.maturitydate is null and perpetual<>''))
and (bond.firstcoupondate<getdate()+@mycount or bond.firstcoupondate is null)
and (bond.intcommencementdate<getdate()+@mycount or bond.intcommencementdate is null)
and (bond.issuedate<getdate()+@mycount or bond.issuedate is null)

   set @mycount = @mycount+1
END

go
