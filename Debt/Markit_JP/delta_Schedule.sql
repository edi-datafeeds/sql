--filepath=o:\Datafeed\Debt\Markit_JP\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_DEBT_Schedule_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_JP\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use wca
select
'CPOPT' as sEvent,
CPOPT.CpoptID as EventID,
v20c_920_BOND.Sedol,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.Actflag,
v20c_920_BOND.IssID,
v20c_920_BOND.SecID,
v20c_920_BOND.CntryofIncorp,
v20c_920_BOND.IssuerName,
v20c_920_BOND.SecurityDesc,
v20c_920_BOND.ISIN,
v20c_920_BOND.USCode,
v20c_920_BOND.SecurityStatus,
v20c_920_BOND.PrimaryExchgCD,
v20c_920_BOND.ExCountry,
v20c_920_BOND.RegCountry,
v20c_920_BOND.Bondtype,
v20c_920_BOND.MaturityDate,
v20c_920_BOND.MaturityExtendible,
v20c_920_BOND.IssueCurrency,
v20c_920_BOND.IssueDate,
v20c_920_BOND.NominalValue,
v20c_920_BOND.DebtCurrency,
CPOPT.Currency as ScheduleCurrency,
CPOPT.Price,
CPOPT.CallPut,
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.CPType,
CPOPT.PriceAsPercent,
CPOPN.Notes
from cpopt
INNER JOIN v20c_920_BOND ON cpopt.secid = v20c_920_BOND.secid
LEFT OUTER JOIN CPOPN ON CPOPT.SECID = CPOPN.SECID AND CPOPT.CALLPUT = CPOPN.CALLPUT
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and cpopt.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
