--filepath=o:\Datafeed\Debt\Markit_JP\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BONDFULL
--fileheadertext=EDI_DEBT_BONDFULL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_JP\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BONDFULL') as TableName,
BOND.SecId, 
BOND.Actflag,
BOND.AnnounceDate,
BOND.Acttime,  
BOND.BondType, 
BOND.DebtMarket, 
BOND.CurenCD as DebtCurrency, 
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as NominalValue,
BOND.IssueDate, 
BOND.IssueCurrency, 
BOND.IssuePrice, 
BOND.IssueAmount, 
BOND.IssueAmountDate, 
BOND.OutstandingAmount, 
BOND.OutstandingAmountDate, 
BOND.InterestBasis as IntBasis, 
BOND.InterestRate as IntRate, 
BOND.InterestAccrualConvention as IntAccrualConvention, 
BOND.InterestPaymentFrequency as IntPaymentFrequency, 
BOND.IntCommencementDate, 
BOND.FirstCouponDate, 
BOND.InterestPayDate1 as IntPayDate1, 
BOND.InterestPayDate2 as IntPayDate2, 
BOND.InterestPayDate3 as IntPayDate3, 
BOND.InterestPayDate4 as IntPayDate4, 
BOND.DomesticTaxRate, 
BOND.NonResidentTaxRate, 
BOND.FRNType, 
BOND.FRNIndexBenchmark, 
BOND.Markup as FrnMargin, 
BOND.MinimumInterestRate as FrnMinIntRate, 
BOND.MaximumInterestRate as FrnMaxIntRate, 
BOND.Rounding as FrnRounding, 
BOND.Series, 
BOND.Class, 
BOND.OnTap, 
BOND.MaximumTapAmount, 
BOND.TapExpiryDate, 
BOND.Guaranteed, 
BOND.SecuredBy, 
BOND.SecurityCharge, 
BOND.Subordinate, 
BOND.SeniorJunior, 
BOND.WarrantAttached, 
BOND.MaturityStructure, 
BOND.Perpetual, 
BOND.MaturityDate, 
BOND.MaturityExtendible, 
BOND.Callable, 
BOND.Puttable, 
BOND.Denomination1, 
BOND.Denomination2, 
BOND.Denomination3, 
BOND.Denomination4, 
BOND.Denomination5, 
BOND.Denomination6, 
BOND.Denomination7, 
BOND.MinimumDenomination, 
BOND.DenominationMultiple, 
BOND.Strip, 
BOND.StripInterestNumber,  
BOND.Bondsrc, 
BOND.MaturityBenchmark, 
BOND.ConventionMethod, 
BOND.FrnIntAdjFreq, 
BOND.IntBusDayConv, 
BOND.InterestCurrency as IntCurrency, 
BOND.MatBusDayConv, 
BOND.MaturityCurrency,  
BOND.TaxRules, 
BOND.VarIntPayDate, 
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
case when bond.matprice<>'' then cast(bond.matprice as decimal(18,4))
     when rtrim(bond.largeparvalue)='' then null
     when rtrim(BOND.MatPriceAsPercent)='' then null
     else cast(bond.largeparvalue as decimal(18,0)) * cast(wca.dbo.BOND.MatPriceAsPercent as decimal(18,4))/100 
     end as MatPrice,
BOND.MatPriceAsPercent, 
BOND.SinkingFund, 
BOND.GoverningLaw, 
BOND.Notes as Notes
FROM BOND
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
scmst.isin in (select code from client.dbo.pfisin where accid=211 and actflag<>'D')
and scmst.actflag<>'D'
and bond.actflag<>'D'

