--filepath=o:\Datafeed\Debt\Markit_JP\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_DEBT_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_JP\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime,
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM SEDOL
INNER JOIN BOND ON SEDOL.SecID = BOND.SecID
INNER JOIN SCMST ON SEDOL.SecID = SCMST.SecID
where 
scmst.isin in (select code from client.dbo.pfisin where accid=211 and actflag<>'D')
and scmst.actflag<>'D'
and bond.actflag<>'D'

