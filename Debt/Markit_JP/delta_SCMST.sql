--filepath=o:\Datafeed\Debt\Markit_JP\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_DEBT_Security_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_JP\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate,
SCMST.Acttime,
SCMST.SecID,
SCMST.IssID,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.StatusReason,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.ISIN,
SCMST.X as CommonCode
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or  
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and (bond.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
  or scmst.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)))
