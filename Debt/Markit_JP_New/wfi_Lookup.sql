--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LOOKUP
--fileheadertext=EDI_DEBT_Lookup_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_JP\Full\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
FROM LOOKUPEXTRA
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.Exchgname as Lookup
from EXCHG
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('MICCODE') as TypeGroup,
Exchg.MIC as Code,
Exchg.Exchgname as Lookup
from EXCHG
where MIC <> '' and MIC is not null
and acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('MICMAP') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.MIC as Lookup
from EXCHG
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
cast(Indus.IndusID as varchar(10)) as Code,
Indus.IndusName as Lookup
from INDUS
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
Cntry.Actflag,
Cntry.Acttime,
upper('CNTRY') as TypeGroup,
Cntry.CntryCD as Code,
Cntry.Country as Lookup
from CNTRY
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
Curen.Actflag,
Curen.Acttime,
upper('CUREN') as TypeGroup,
Curen.CurenCD as Code,
Curen.Currency as Lookup
from CUREN
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
Event.Actflag,
Event.Acttime,
upper('EVENT') as TypeGroup,
Event.EventType as Code,
Event.EventName as Lookup
from EVENT
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 3)

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2
and 1=2

order by TypeGroup, Code
