--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_WFISample
--fileheadertext=EDI_WFISample_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N



--# 
use wca
SELECT 
upper('AGM') as TableName,
AGM.Actflag,
AGM.AnnounceDate as Created,
AGM.Acttime as Changed, 
AGM.AGMID,
BOND.SecID,
SCMST.SecID,
AGM.IssID,
AGM.AGMDate,
AGM.AGMEGM,
AGM.AGMNo,
AGM.FYEDate,
AGM.AGMTime,
AGM.Add1,
AGM.Add2,
AGM.Add3,
AGM.Add4,
AGM.Add5,
AGM.Add6,
AGM.City,
AGM.CntryCD,
AGM.BondSecID
FROM AGM
INNER JOIN SCMST ON AGM.BondSecID = SCMST.SecID
INNER JOIN BOND ON AGM.BondSecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and AGM.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use wca
select distinct
upper('AGNCY') as TableName,
AGNCY.Actflag,
AGNCY.AnnounceDate as Created,
AGNCY.Acttime as Changed,
AGNCY.AgncyID,
AGNCY.RegistrarName,
AGNCY.Add1,
AGNCY.Add2,
AGNCY.Add3,
AGNCY.Add4,
AGNCY.Add5,
AGNCY.Add6,
AGNCY.City,
AGNCY.CntryCD,
AGNCY.Website,
AGNCY.Contact1,
AGNCY.Tel1,
AGNCY.Fax1,
AGNCY.Email1,
AGNCY.Contact2,
AGNCY.Tel2,
AGNCY.Fax2,
AGNCY.Email2,
AGNCY.Depository,
AGNCY.State
FROM AGNCY
INNER JOIN SCAGY ON AGNCY.AgncyID = SCAGY.AgncyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and AGNCY.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use wca
select distinct
upper('AGYDT') as TableName,
AGYDT.Actflag,
AGYDT.AnnounceDate as Created,
AGYDT.Acttime as Changed,
AGYDT.AgydtID,
AGYDT.AgncyID,
AGYDT.EffectiveDate,
AGYDT.OldRegistrarName,
AGYDT.OldAdd1,
AGYDT.OldAdd2,
AGYDT.OldAdd3,
AGYDT.OldAdd4,
AGYDT.OldAdd5,
AGYDT.OldAdd6,
AGYDT.OldCity,
AGYDT.OldCntryCD,
AGYDT.OldWebSite,
AGYDT.OldContact1,
AGYDT.OldTel1,
AGYDT.OldFax1,
AGYDT.Oldemail1,
AGYDT.OldContact2,
AGYDT.OldTel2,
AGYDT.OldFax2,
AGYDT.Oldemail2,
AGYDT.OldState,
AGYDT.NewRegistrarName,
AGYDT.NewAdd1,
AGYDT.NewAdd2,
AGYDT.NewAdd3,
AGYDT.NewAdd4,
AGYDT.NewAdd5,
AGYDT.NewAdd6,
AGYDT.NewCity,
AGYDT.NewCntryCD,
AGYDT.NewWebSite,
AGYDT.NewContact1,
AGYDT.NewTel1,
AGYDT.NewFax1,
AGYDT.Newemail1,
AGYDT.NewContact2,
AGYDT.NewTel2,
AGYDT.NewFax2,
AGYDT.Newemail2,
AGYDT.NewState
FROM AGYDT
INNER JOIN SCAGY ON AGYDT.AgncyID = SCAGY.AgncyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and AGYDT.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 1
use WCA
select distinct
upper('BBC') as tablename,
bbc.actflag,
bbc.announcedate,
bbc.acttime,
bbc.bbcid,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
bbc.bbgcompid,
bbc.bbgcomptk
from bbc
inner join scmst on bbc.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and bbc.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 1
use WCA
SELECT
upper('BBCC') as tablename,
BBCC.actflag,
BBCC.announcedate,
BBCC.acttime,
BBCC.bbccid,
BBCC.secid,
BBCC.bbcid,
BBCC.oldcntrycd,
BBCC.oldcurencd,
BBCC.effectivedate,
BBCC.newcntrycd,
BBCC.newcurencd,
BBCC.oldbbgcompid,
BBCC.newbbgcompid,
BBCC.oldbbgcomptk,
BBCC.newbbgcomptk,
BBCC.releventid,
BBCC.eventtype,
BBCC.notes
FROM BBCC
inner join scmst on BBCC.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BBCC.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 1
use WCA
SELECT distinct
upper('BBE') as tablename,
bbe.actflag,
bbe.announcedate,
bbe.acttime,
bbe.bbeid,
bbe.secid,
bbe.exchgcd,
bbe.curencd,
bbe.bbgexhid,
bbe.bbgexhtk
from bbe
inner join scmst on bbe.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and bbe.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 1
use WCA
SELECT
upper('BBEC') as tablename,
BBEC.actflag,
BBEC.announcedate,
BBEC.acttime,
BBEC.bbecid,
BBEC.secid,
BBEC.bbeid,
BBEC.oldexchgcd,
BBEC.oldcurencd,
BBEC.effectivedate,
BBEC.newexchgcd,
BBEC.newcurencd,
BBEC.oldbbgexhid,
BBEC.newbbgexhid,
BBEC.oldbbgexhtk,
BBEC.newbbgexhtk,
BBEC.releventid,
BBEC.eventtype,
BBEC.notes
FROM bbec as BBEC
inner join scmst on BBEC.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BBEC.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('BDC') as TableName,
BDC.Actflag,
BDC.AnnounceDate as Created,
BDC.Acttime as Changed,
BDC.BdcID,
BDC.SecID,
SCMST.SecID,
BDC.BDCAppliedTo,
BDC.CntrID,
BDC.Notes
FROM BDC
INNER JOIN BOND ON BDC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BDC.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
select distinct
upper('BKRP') as TableName,
BKRP.Actflag,
BKRP.AnnounceDate as Created,
BKRP.Acttime as Changed,  
BKRP.BkrpID,
BOND.SecID,
SCMST.SecID,
BKRP.IssID,
BKRP.NotificationDate,
BKRP.FilingDate
FROM BKRP
INNER JOIN SCMST ON BKRP.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BKRP.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
select
upper('BKRPNOTES') as TableName,
BKRP.Actflag,
BKRP.BkrpID,
BKRP.BkrpNotes as Notes
FROM BKRP
where
BKRP.Issid in (
select wca.dbo.scmst.issid from client.dbo.pfisin
inner join wca.dbo.scmst on client.dbo.pfisin.code = wca.dbo.SCMST.SecID
where client.dbo.pfisin.accid=211
and (BKRP.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
or client.dbo.pfisin.actflag='I'))




--# 
use WCA
SELECT 
upper('BOCHG') as TableName,
BOCHG.Actflag,
BOCHG.AnnounceDate as Created,
BOCHG.Acttime as Changed,  
BOCHG.BochgID,
BOCHG.RelEventID,
BOCHG.SecID,
SCMST.SecID,
BOCHG.EffectiveDate,
BOCHG.OldOutValue,
BOCHG.NewOutValue,
BOCHG.EventType,
BOCHG.OldOutDate,
BOCHG.NewOutDate,
BOCHG.BochgNotes as Notes
FROM BOCHG
INNER JOIN BOND ON BOCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BOCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('BOND') as Tablename,
BOND.Actflag,
BOND.AnnounceDate as Created,
BOND.Acttime as Changed,
BOND.SecID,
SCMST.SecID,
BOND.BondType,
BOND.DebtMarket,
BOND.CurenCD as DebtCurrency,
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as NominalValue,
BOND.IssueDate,
BOND.IssueCurrency,
BOND.IssuePrice,
BOND.IssueAmount,
BOND.IssueAmountDate,
BOND.OutstandingAmount,
BOND.OutstandingAmountDate,
BOND.InterestBasis As InterestType,
BOND.InterestRate,
BOND.InterestAccrualConvention,
BOND.InterestPaymentFrequency,
BOND.IntCommencementDate as InterestCommencementDate,
BOND.FirstCouponDate,
BOND.InterestPayDate1,
BOND.InterestPayDate2,
BOND.InterestPayDate3,
BOND.InterestPayDate4,
BOND.DomesticTaxRate,
BOND.NonResidentTaxRate,
BOND.FRNType,
BOND.FRNIndexBenchmark,
BOND.Markup as FrnMargin,
BOND.MinimumInterestRate as FrnMinInterestRate,
BOND.MaximumInterestRate as FrnMaxInterestRate,
BOND.Rounding as FrnRounding,
BOND.Series,
BOND.Class,
BOND.OnTap,
BOND.MaximumTapAmount,
BOND.TapExpiryDate,
BOND.Guaranteed,
BOND.SecuredBy,
BOND.SecurityCharge,
BOND.Subordinate,
BOND.SeniorJunior,
BOND.WarrantAttached,
BOND.MaturityStructure,
BOND.Perpetual,
BOND.MaturityDate,
BOND.MaturityExtendible,
BOND.Callable,
BOND.Puttable,
BOND.Denomination1,
BOND.Denomination2,
BOND.Denomination3,
BOND.Denomination4,
BOND.Denomination5,
BOND.Denomination6,
BOND.Denomination7,
BOND.MinimumDenomination,
BOND.DenominationMultiple,
BOND.Strip,
BOND.StripInterestNumber, 
BOND.Bondsrc,
BOND.MaturityBenchmark,
BOND.ConventionMethod,
BOND.FrnIntAdjFreq as FrnInterestAdjFreq,
BOND.IntBusDayConv as InterestBusDayConv,
BOND.InterestCurrency,
BOND.MatBusDayConv as MaturityBusDayConv,
BOND.MaturityCurrency, 
BOND.TaxRules,
BOND.VarIntPayDate as VarInterestPaydate,
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
case when bond.matprice<>'' then cast(bond.matprice as decimal(18,4))
     when rtrim(bond.largeparvalue)='' then null
     when rtrim(BOND.MatPriceAsPercent)='' then null
     else cast(bond.largeparvalue as decimal(18,0)) * cast(wca.dbo.BOND.MatPriceAsPercent as decimal(18,4))/100 
     end as MaturityPrice,
BOND.MatPriceAsPercent as MaturityPriceAsPercent,
BOND.SinkingFund,
BOND.GovCity,
BOND.GovState,
BOND.GovCntry,
BOND.GovLawLkup as GovLaw,
BOND.GoverningLaw as GovLawNotes,
BOND.Municipal,
BOND.PrivatePlacement,
BOND.Syndicated,
BOND.Tier,
BOND.UppLow,
BOND.Collateral,
BOND.CoverPool,
BOND.PikPay,
BOND.YieldAtIssue,
bond.CoCoTrigger as Coco,
BOND.Notes
FROM BOND
inner join scmst on bond.secid = scmst.secid
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and (bond.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
  or scmst.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)))




--# 
use wca
select
upper('BSCHG') as TableName,
BSCHG.Actflag,
BSCHG.AnnounceDate as Created,
BSCHG.Acttime as Changed,
BSCHG.BschgID,
BSCHG.SecID,
SCMST.SecID,
BSCHG.NotificationDate,
BSCHG.OldBondType,
BSCHG.NewBondType,
BSCHG.OldCurenCD,
BSCHG.NewCurenCD,
BSCHG.OldPIU,
BSCHG.NewPIU,
BSCHG.OldInterestBasis,
BSCHG.NewInterestBasis,
BSCHG.Eventtype,
BSCHG.OldInterestCurrency,
BSCHG.NewInterestCurrency,
BSCHG.OldMaturityCurrency,
BSCHG.NewMaturityCurrency,
BSCHG.OldIntBusDayConv,
BSCHG.NewIntBusDayConv,
BSCHG.OldMatBusDayConv,
BSCHG.NewMatBusDayConv,
BSCHG.BschgNotes as Notes
FROM BSCHG
INNER JOIN BOND ON BSCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BSCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('CNTR') as TableName,
CNTR.Actflag,
CNTR.AnnounceDate as Created,
CNTR.Acttime as Changed,
CNTR.CntrID,
CNTR.CentreName,
CNTR.CntryCD
FROM CNTR
where

CNTR.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)




--# 1
use wca
select
'CONV' as TableName,
CONV.Actflag,
CONV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONV.Acttime) THEN RD.Acttime ELSE CONV.Acttime END as [Changed],
CONV.ConvID,
SCMST.SecID,
SCMST.SecID,
RD.Recdate,
CONV.FromDate,
CONV.ToDate,
CONV.RatioNew,
CONV.RatioOld,
CONV.CurenCD as ConvCurrency,
CONV.CurPair,
CONV.Price,
CONV.MandOptFlag,
CONV.ResSecID,
CONV.ResSectyCD,
RESSCMST.SecID as ResSCMST.SecID,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
CONV.Fractions,
CONV.FXrate,
CONV.PartFinalFlag,
CONV.ConvType,
CONV.RDID,
CONV.PriceAsPercent,
CONV.AmountConverted,
CONV.SettlementDate,
CONV.ConvNotes as Notes
FROM CONV
INNER JOIN BOND ON CONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON CONV.RdID = RD.RdID
LEFT OUTER JOIN SCMST as RESSCMST ON CONV.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and CONV.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)))




--# 
use WCA
SELECT 
upper('CONVT') as TableName,
CONVT.Actflag,
CONVT.AnnounceDate as Created,
CONVT.Acttime as Changed,
CONVT.ConvtID,
SCMST.SecID,
SCMST.SecID,
CONVT.FromDate,
CONVT.ToDate,
CONVT.RatioNew,
CONVT.RatioOld,
CONVT.CurenCD,
CONVT.CurPair,
CONVT.Price,
CONVT.MandOptFlag,
CONVT.ResSecID,
CONVT.SectyCD as ResSectyCD,
RESSCMST.SecID as ResSCMST.SecID,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
CONVT.Fractions,
CONVT.FXrate,
CONVT.PartFinalFlag,
CONVT.PriceAsPercent,
CONVT.ConvtNotes as Notes
FROM CONVT
INNER JOIN BOND ON convt.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as RESSCMST ON CONVT.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and convt.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
'COSNT' as TableName,
COSNT.Actflag,
COSNT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > COSNT.Acttime) THEN RD.Acttime ELSE COSNT.Acttime END as [Changed],
COSNT.RdID,
SCMST.SecID,
RD.Recdate,
SCMST.SecID,
COSNT.ExpiryDate,
COSNT.ExpiryTime,
SUBSTRING(cosnt.TimeZone, 1,3) AS TimeZone,
COSNT.CollateralRelease,
COSNT.Currency,
COSNT.Fee,
COSNT.Notes
FROM COSNT
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and COSNT.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.CpoptID,
SCMST.SecID,
SCMST.SecID,
CPOPT.CallPut, 
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.cptype,
CPOPT.PriceAsPercent,
CPOPT.InWholePart,
CPOPT.FormulaBasedPrice,
CPOPT.Notes as Notes 
FROM CPOPT
INNER JOIN BOND ON CPOPT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and CPOPT.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('CTCHG') as TableName,
CTCHG.Actflag,
CTCHG.AnnounceDate as Created,
CTCHG.Acttime as Changed, 
CTCHG.CtChgID,
CTCHG.SecID,
SCMST.SecID,
CTCHG.EffectiveDate,
CTCHG.OldResultantRatio,
CTCHG.NewResultantRatio,
CTCHG.OldSecurityRatio,
CTCHG.NewSecurityRatio,
CTCHG.OldCurrency,
CTCHG.NewCurrency,
CTCHG.OldCurPair,
CTCHG.NewCurPair,
CTCHG.OldConversionPrice,
CTCHG.NewConversionPrice,
CTCHG.ResSectyCD,
CTCHG.OldResSecID,
CTCHG.NewResSecID,
CTCHG.EventType,
CTCHG.RelEventID,
CTCHG.OldFromDate,
CTCHG.NewFromDate,
CTCHG.OldTodate,
CTCHG.NewToDate,
CTCHG.ConvtID,
CTCHG.OldFXRate,
CTCHG.NewFXRate,
CTCHG.OldPriceAsPercent,
CTCHG.NewPriceAsPercent,
CTCHG.Notes
FROM CTCHG
INNER JOIN BOND ON CTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and CTCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))
and eventtype<>'CLEAN'
and eventtype<>'CORR'




--# 
use WCA
SELECT 
upper('CURRD') as TableName,
CURRD.Actflag,
CURRD.AnnounceDate as Created,
CURRD.Acttime as Changed,
CURRD.CurrdID,
CURRD.SecID,
SCMST.SecID,
CURRD.EffectiveDate,
CURRD.OldCurenCD,
CURRD.NewCurenCD,
CURRD.OldParValue,
CURRD.NewParValue,
CURRD.EventType,
CURRD.CurRdNotes as Notes
FROM CURRD
INNER JOIN BOND ON CURRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and CURRD.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))
and eventtype<>'CLEAN'
and eventtype<>'CORR'




--# 
use WCA
SELECT 
upper('FRNFX') as TableName,
FRNFX.Actflag,
FRNFX.AnnounceDate as Created,
FRNFX.Acttime as Changed,
FRNFX.FrnfxID,
FRNFX.SecID,
SCMST.SecID,
FRNFX.EffectiveDate,
FRNFX.OldFRNType,
FRNFX.OldFRNIndexBenchmark,
FRNFX.OldMarkup As OldFrnMargin,
FRNFX.OldMinimumInterestRate,
FRNFX.OldMaximumInterestRate,
FRNFX.OldRounding,
FRNFX.NewFRNType,
FRNFX.NewFRNindexBenchmark,
FRNFX.NewMarkup As NewFrnMargin,
FRNFX.NewMinimumInterestRate,
FRNFX.NewMaximumInterestRate,
FRNFX.NewRounding,
FRNFX.Eventtype,
FRNFX.OldFrnIntAdjFreq,
FRNFX.NewFrnIntAdjFreq
FROM FRNFX
INNER JOIN BOND ON FRNFX.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and FRNFX.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('ICC') as TableName,
ICC.Actflag,
ICC.AnnounceDate as Created,
ICC.Acttime as Changed,
ICC.IccID,
ICC.SecID,
SCMST.SecID,
ICC.EffectiveDate,
ICC.OldSCMST.SecID,
ICC.NewSCMST.SecID,
ICC.OldUSCode,
ICC.NewUSCode,
ICC.OldVALOREN,
ICC.NewVALOREN,
ICC.EventType,
ICC.RelEventID,
ICC.OldCommonCode,
ICC.NewCommonCode
FROM ICC
INNER JOIN BOND ON ICC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and ICC.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))





--# 
use WCA
SELECT 
upper('IFCHG') as TableName,
IFCHG.Actflag,
IFCHG.AnnounceDate as Created,
IFCHG.Acttime as Changed,
IFCHG.IfchgID,
IFCHG.SecID,
SCMST.SecID,
IFCHG.NotificationDate,
IFCHG.OldIntPayFrqncy,
IFCHG.OldIntPayDate1,
IFCHG.OldIntPayDate2,
IFCHG.OldIntPayDate3,
IFCHG.OldIntPayDate4,
IFCHG.NewIntPayFrqncy,
IFCHG.NewIntPayDate1,
IFCHG.NewIntPayDate2,
IFCHG.NewIntPayDate3,
IFCHG.NewIntPayDate4,
IFCHG.Eventtype
FROM IFCHG
INNER JOIN BOND ON IFCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and IFCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))





--# 
use WCA
SELECT
upper('INCHG') as TableName,
INCHG.Actflag,
INCHG.AnnounceDate as Created,
INCHG.Acttime as Changed,
INCHG.InChgID,
BOND.SecID,
SCMST.SecID,
INCHG.IssID,
INCHG.InChgDate,
INCHG.OldCntryCD,
INCHG.NewCntryCD,
INCHG.EventType 
FROM INCHG
INNER JOIN SCMST ON INCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and INCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))
and (bond.issuedate<=inchg.inchgdate
     or bond.issuedate is null or inchg.inchgdate is null
    )




--# 1
use wca
select
'INDEF' as TableName,
INDEF.Actflag,
INDEF.AnnounceDate as Created,
INDEF.Acttime as Changed,
INDEF.IndefID,
INDEF.SecID,
SCMST.SecID,
INDEF.DefaultType,
INDEF.DefaultDate,
INDEF.Notes
FROM INDEF
INNER JOIN BOND ON INDEF.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and INDEF.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))





--# 
use WCA
SELECT 
upper('INTBC') as TableName,
INTBC.Actflag,
INTBC.AnnounceDate as Created,
INTBC.Acttime as Changed,
INTBC.IntbcID,
INTBC.SecID,
SCMST.SecID,
INTBC.EffectiveDate,
INTBC.OldIntBasis,
INTBC.NewIntBasis,
INTBC.OldIntBDC,
INTBC.NewIntBDC
FROM INTBC
INNER JOIN BOND ON INTBC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and INTBC.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))





--# 1
use wca
select distinct
'INTPY' as TableName,
CASE WHEN INT.Actflag = 'D' or INT.actflag='C' or intpy.actflag is null THEN INT.Actflag ELSE INTPY.Actflag END as Actflag,
INT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > INT.Acttime) and (RD.Acttime > EXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > INT.Acttime) THEN EXDT.Acttime ELSE INT.Acttime END as [Changed],
SCMST.SecID,
SCMST.SecID,
INTPY.BCurenCD as DebtCurrency,
INTPY.BParValue as NominalValue,
SCEXH.ExchgCD,
INT.RdID,
case when intpy.optionid is null then '1' else intpy.optionid end as OptionID,
RD.Recdate,
EXDT.Exdate,
EXDT.Paydate,
INT.InterestFromDate,
INT.InterestToDate,
INT.Days,
INTPY.CurenCD as Interest_Currency,
INTPY.IntRate as Interest_Rate,
INTPY.GrossInterest,
INTPY.NetInterest,
INTPY.DomesticTaxRate,
INTPY.NonResidentTaxRate,
INTPY.RescindInterest,
INTPY.AgencyFees,
INTPY.CouponNo,
--null as CouponID,
INTPY.DefaultOpt,
INTPY.OptElectionDate,
INTPY.AnlCoupRate,
INT.InDefPay
from [INT]
INNER JOIN RD ON INT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON BOND.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID 
     AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID
where
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
and RD.ACTFLAG<>'d'
AND scmst.ACTFLAG<>'d'
AND bond.ACTFLAG<>'d'
AND issur.ACTFLAG<>'d'
AND int.ACTFLAG<>'d'
AND intpy.ACTFLAG<>'d')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and (INTPY.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
or INT.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
or RD.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
or EXDT.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)))





--# 
use WCA
SELECT 
upper('IRCHG') as TableName,
IRCHG.Actflag,
IRCHG.AnnounceDate as Created,
IRCHG.Acttime as Changed,
IRCHG.IrchgID,
IRCHG.SecID,
SCMST.SecID,
IRCHG.EffectiveDate,
IRCHG.OldInterestRate,
IRCHG.NewInterestRate,
IRCHG.Eventtype,
IRCHG.Notes 
FROM IRCHG
INNER JOIN BOND ON IRCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and IRCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))





--# 
use WCA
SELECT 
upper('ISCHG') as TableName,
ISCHG.Actflag,
ISCHG.AnnounceDate as Created,
ISCHG.Acttime as Changed,
ISCHG.IschgID,
BOND.SecID,
SCMST.SecID,
ISCHG.IssID,
ISCHG.NameChangeDate,
ISCHG.IssOldName,
ISCHG.IssNewName,
ISCHG.EventType,
ISCHG.LegalName,
ISCHG.MeetingDateFlag, 
ISCHG.IsChgNotes as Notes
FROM ISCHG
INNER JOIN SCMST ON ISCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and ISCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))
and (bond.issuedate<=ischg.namechangedate
     or bond.issuedate is null or ischg.namechangedate is null
    )




--# 
use WCA
select distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate as Created,
ISSUR.Acttime as Changed,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
ISSUR.Shortname,
ISSUR.LegalName,
ISSUR.CntryofDom,
ISSUR.StateofDom,
cast(ISSUR.IssurNotes As varchar(8000)),
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN ISSUR.Isstype='GOV' THEN 'G' WHEN ISSUR.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and ISSUR.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
select distinct
upper('LAWST') as TableName,
LAWST.Actflag,
LAWST.AnnounceDate as Created,
LAWST.Acttime as Changed,
LAWST.LawstID,
BOND.SecID,
SCMST.SecID,
LAWST.IssID,
LAWST.EffectiveDate,
LAWST.LAType,
LAWST.Regdate
FROM LAWST
INNER JOIN SCMST ON LAWST.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and LAWST.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
select
upper('LAWSTNOTES') as TableName,
LAWST.Actflag,
LAWST.LawstID,
LAWST.LawstNotes as Notes
FROM LAWST
where
LAWST.Issid in (
select wca.dbo.scmst.issid from client.dbo.pfisin
inner join wca.dbo.scmst on client.dbo.pfisin.code = wca.dbo.SCMST.SecID
where client.dbo.pfisin.accid=211
and (LAWST.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
or client.dbo.pfisin.actflag='I'))





--# 
use WCA
SELECT 
upper('LCC') as TableName,
LCC.Actflag,
LCC.AnnounceDate as Created,
LCC.Acttime as Changed,
LCC.LccID,
LCC.SecID,
SCMST.SecID,
LCC.ExchgCD,
LCC.EffectiveDate,
LCC.OldLocalCode,
LCC.NewLocalCode,
LCC.EventType,
LCC.RelEventID
FROM LCC
INNER JOIN BOND ON LCC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and LCC.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))





--# 
use WCA
select distinct
upper('LIQ') as TableName,
LIQ.Actflag,
LIQ.AnnounceDate as Created,
LIQ.Acttime as Changed,
LIQ.LiqID,
BOND.SecID,
SCMST.SecID,
LIQ.IssID,
LIQ.Liquidator,
LIQ.LiqAdd1,
LIQ.LiqAdd2,
LIQ.LiqAdd3,
LIQ.LiqAdd4,
LIQ.LiqAdd5,
LIQ.LiqAdd6,
LIQ.LiqCity,
LIQ.LiqCntryCD,
LIQ.LiqTel,
LIQ.LiqFax,
LIQ.LiqEmail,
LIQ.RdDate
FROM LIQ
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and LIQ.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
select
upper('LIQNOTES') as TableName,
LIQ.Actflag,
LIQ.LiqID,
LIQ.LiquidationTerms
FROM LIQ
where
LIQ.Issid in (
select wca.dbo.scmst.issid from client.dbo.pfisin
inner join wca.dbo.scmst on client.dbo.pfisin.code = wca.dbo.SCMST.SecID
where client.dbo.pfisin.accid=211
and (LIQ.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
or client.dbo.pfisin.actflag='I'))





--# 
use WCA
SELECT 
upper('LSTAT') as TableName,
LSTAT.Actflag,
LSTAT.AnnounceDate as Created,
LSTAT.Acttime as Changed,
LSTAT.LstatID,
SCMST.SecID,
SCMST.SecID,
LSTAT.ExchgCD,
LSTAT.NotificationDate,
LSTAT.EffectiveDate,
LSTAT.LStatStatus,
LSTAT.EventType,
LSTAT.Reason
FROM LSTAT
INNER JOIN BOND ON LSTAT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and LSTAT.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('LTCHG') as TableName,
LTCHG.Actflag,
LTCHG.AnnounceDate as Created,
LTCHG.Acttime as Changed,
LTCHG.LtChgID,
SCMST.SecID,
SCMST.SecID,
LTCHG.ExchgCD,
LTCHG.EffectiveDate,
LTCHG.OldLot,
LTCHG.OldMinTrdQty,
LTCHG.NewLot,
LTCHG.NewMinTrdgQty
FROM LTCHG
INNER JOIN BOND ON LTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and LTCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))





--# 
use WCA
select
upper('MPAY') as TableName,
MPAY.Actflag,
MPAY.AnnounceDate,
MPAY.Acttime,
MPAY.sEvent as EventType,
MPAY.EventID,
MPAY.OptionID,
MPAY.SerialID,
MPAY.SectyCD as ResSectyCD,
MPAY.ResSecID,
RESSCMST.SecID as ResSCMST.SecID,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
MPAY.RatioNew,
MPAY.RatioOld,
MPAY.Fractions,
MPAY.MinOfrQty,
MPAY.MaxOfrQty,
MPAY.MinQlyQty,
MPAY.MaxQlyQty,
MPAY.Paydate,
MPAY.CurenCD,
MPAY.MinPrice,
MPAY.MaxPrice,
MPAY.TndrStrkPrice,
MPAY.TndrStrkStep,
MPAY.Paytype,
MPAY.DutchAuction,
MPAY.DefaultOpt,
MPAY.OptElectionDate
FROM MPAY
inner join LIQ on 'LIQ' = MPAY.sEvent and LIQ.LiqID = MPAY.EventID
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
LEFT OUTER JOIN SCMST as RESSCMST ON MPAY.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and MPAY.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('MTCHG') as TableName,
MTCHG.Actflag,
MTCHG.AnnounceDate as Created,
MTCHG.Acttime as Changed,
MTCHG.MtChgID,
MTCHG.SecID,
SCMST.SecID,
MTCHG.NotificationDate,
MTCHG.OldMaturityDate,
MTCHG.NewMaturityDate,
MTCHG.Reason,
MTCHG.EventType,
MTCHG.OldMaturityBenchmark,
MTCHG.NewMaturityBenchmark,
MTCHG.Notes as Notes
FROM MTCHG
INNER JOIN BOND ON MTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and MTCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('PVRD') as TableName,
PVRD.Actflag,
PVRD.AnnounceDate as Created,
PVRD.Acttime as Changed,
PVRD.PvRdID,
PVRD.SecID,
SCMST.SecID,
PVRD.EffectiveDate,
PVRD.CurenCD,
PVRD.OldParValue,
PVRD.NewParValue,
PVRD.EventType,
PVRD.PvRdNotes as Notes
FROM PVRD
INNER JOIN BOND ON PVRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and PVRD.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('RCONV') as TableName,
RCONV.Actflag,
RCONV.AnnounceDate as Created,
RCONV.Acttime as Changed,
RCONV.RconvID,
RCONV.SecID,
SCMST.SecID,
RCONV.EffectiveDate,
RCONV.OldInterestAccrualConvention,
RCONV.NewInterestAccrualConvention,
RCONV.OldConvMethod,
RCONV.NewConvMethod,
RCONV.Eventtype,
RCONV.Notes
FROM RCONV
INNER JOIN BOND ON RCONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and RCONV.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('RDNOM') as TableName,
RDNOM.Actflag,
RDNOM.AnnounceDate as Created,
RDNOM.Acttime as Changed, 
RDNOM.RdnomID,
RDNOM.SecID,
SCMST.SecID,
RDNOM.EffectiveDate,
RDNOM.OldDenomination1,
RDNOM.OldDenomination2,
RDNOM.OldDenomination3,
RDNOM.OldDenomination4,
RDNOM.OldDenomination5,
RDNOM.OldDenomination6,
RDNOM.OldDenomination7,
RDNOM.OldMinimumDenomination,
RDNOM.OldDenominationMultiple,
RDNOM.NewDenomination1,
RDNOM.NewDenomination2,
RDNOM.NewDenomination3,
RDNOM.NewDenomination4,
RDNOM.NewDenomination5,
RDNOM.NewDenomination6,
RDNOM.NewDenomination7,
RDNOM.NewMinimumDenomination,
RDNOM.NewDenominationMultiple,
RDNOM.Notes
FROM RDNOM
INNER JOIN BOND ON RDNOM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and RDNOM.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 1
use wca
select
'REDEM' as TableName,
REDEM.Actflag,
REDEM.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > REDEM.Acttime) THEN RD.Acttime ELSE REDEM.Acttime END as [Changed],
REDEM.RedemID,
SCMST.SecID,
SCMST.SecID,
RD.Recdate,
REDEM.RedemDate,
REDEM.CurenCD as RedemCurrency,
REDEM.RedemPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.RedemType,
REDEM.AmountRedeemed,
REDEM.RedemPremium,
REDEM.RedemPercent,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
REDEM.PriceAsPercent,
REDEM.PremiumAsPercent,
REDEM.InDefPay,
REDEM.TenderOpenDate,
REDEM.TenderCloseDate,
REDEM.RedemNotes as Notes
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON REDEM.RdID = RD.RdID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and REDEM.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('REDMT') as TableName,
REDMT.Actflag,
REDMT.AnnounceDate as Created,
REDMT.Acttime as Changed,
REDMT.RedmtID,
SCMST.SecID,
SCMST.SecID,
REDMT.RedemptionDate as RedemDate,
REDMT.CurenCD as RedemCurrency,
REDMT.RedemptionPrice as RedemPrice,
REDMT.MandOptFlag,
REDMT.PartFinal,
REDMT.RedemptionType as RedemType,
REDMT.RedemptionAmount as RedemAmount,
REDMT.RedemptionPremium as RedemPremium,
REDMT.RedemInPercent,
REDMT.PriceAsPercent,
REDMT.PremiumAsPercent,
REDMT.RedmtNotes as Notes
FROM REDMT
INNER JOIN BOND ON redmt.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and redmt.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('SACHG') as TableName,
SACHG.Actflag,
SACHG.AnnounceDate as Created,
SACHG.Acttime as Changed,
SACHG.SachgID,
SACHG.ScagyID,
BOND.SecID,
SCMST.SecID,
SACHG.EffectiveDate,
SACHG.OldRelationship,
SACHG.NewRelationship,
SACHG.OldAgncyID,
SACHG.NewAgncyID,
SACHG.OldSpStartDate,
SACHG.NewSpStartDate,
SACHG.OldSpEndDate,
SACHG.NewSpEndDate,
SACHG.OldGuaranteeType,
SACHG.NewGuaranteeType
FROM SACHG
INNER JOIN SCAGY ON SACHG.ScagyID = SCAGY.ScagyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SACHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('SCAGY') as TableName,
SCAGY.Actflag,
SCAGY.AnnounceDate as Created,
SCAGY.Acttime as Changed,
SCAGY.ScagyID,
SCAGY.SecID,
SCMST.SecID,
SCAGY.Relationship,
SCAGY.AgncyID,
SCAGY.GuaranteeType,
SCAGY.SpStartDate,
SCAGY.SpEndDate,
SCAGY.Notes
FROM SCAGY
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SCAGY.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('SCCHG') as TableName,
SCCHG.Actflag,
SCCHG.AnnounceDate as Created,
SCCHG.Acttime as Changed,
SCCHG.ScChgID,
SCCHG.SecID,
SCMST.SecID,
SCCHG.DateofChange,
SCCHG.SecOldName,
SCCHG.SecNewName,
SCCHG.EventType,
SCCHG.OldSectyCD,
SCCHG.NewSectyCD,
SCCHG.OldRegS144A,
SCCHG.NewRegS144A,
SCCHG.ScChgNotes as Notes
FROM SCCHG
INNER JOIN BOND ON SCCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SCCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate as Created,
SCEXH.Acttime as Changed,
SCEXH.ScExhID,
SCEXH.SecID,
SCMST.SecID,
SCEXH.ExchgCD,
SCEXH.ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
null as TradeStatus,
SCEXH.LocalCode,
SUBSTRING(SCEXH.JunkLocalcode,0,50) as JunkLocalcode,
SCEXH.ScexhNotes As Notes
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SCEXH.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate as Created,
SCMST.Acttime as Changed,
SCMST.SecID,
SCMST.SecID,
SCMST.ParentSecID,
SCMST.IssID,
SCMST.SectyCD,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.X as CommonCode,
SCMST.Holding,
SCMST.StructCD,
SCMST.WKN,
SCMST.RegS144A,
SCMST.scmstNotes As Notes
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and (bond.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)
  or scmst.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3)))




--# 
use WCA
SELECT 
upper('SDCHG') as TableName,
SDCHG.Actflag,
SDCHG.AnnounceDate as Created, 
SDCHG.Acttime as Changed,
SDCHG.SdChgID,
SDCHG.SecID,
SCMST.SecID,
SDCHG.CntryCD as OldCntryCD,
SDCHG.EffectiveDate,
SDCHG.OldSedol,
SDCHG.NewSedol,
SDCHG.EventType,
SDCHG.RcntryCD as OldRcntryCD,
SDCHG.RelEventID,
SDCHG.NewCntryCD,
SDCHG.NewRcntryCD,
SDCHG.sdchgNotes As Notes
FROM SDCHG
INNER JOIN BOND ON SDCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SDCHG.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate as Created,
SEDOL.Acttime as Changed, 
SEDOL.SedolId,
SEDOL.SecID,
SCMST.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD 
FROM SEDOL
INNER JOIN BOND ON SEDOL.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SEDOL.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('SELRT') as TableName,
SELRT.Actflag,
SELRT.AnnounceDate as Created,
SELRT.Acttime as Changed,
SELRT.SelrtID,
SELRT.SecID,
SCMST.SecID,
SELRT.CntryCD,
SELRT.Restriction
FROM SELRT
INNER JOIN BOND ON SELRT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SELRT.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
select
upper('SFUND') as TableName,
SFUND.Actflag,
SFUND.AnnounceDate as Created,
SFUND.Acttime as Changed,
SFUND.SfundID,
SFUND.SecID,
SCMST.SecID,
SFUND.SfundDate,
SFUND.CurenCD as Sinking_Currency,
SFUND.Amount,
SFUND.AmountasPercent,
SFUND.SfundNotes as Notes
FROM SFUND
INNER JOIN BOND ON SFUND.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SFUND.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('TRNCH') as TableName,
TRNCH.Actflag,
TRNCH.AnnounceDate as Created,
TRNCH.Acttime as Changed,
TRNCH.TrnchID,
TRNCH.SecID,
SCMST.SecID,
TRNCH.TrnchNumber,
TRNCH.TrancheDate,
TRNCH.TrancheAmount,
TRNCH.ExpiryDate,
TRNCH.Notes as Notes
FROM TRNCH
INNER JOIN BOND ON TRNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and TRNCH.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))




--# 
use WCA
SELECT 
upper('WKNCH') as TableName,
WKNCH.Actflag,
WKNCH.AnnounceDate as Created,
WKNCH.Acttime as Changed,
WKNCH.SecID,
WKNCH.EffectiveDate,
WKNCH.WknChID,
WKNCH.Eventtype,
WKNCH.RelEventID,
WKNCH.OldWKN,
WKNCH.NewWKN
FROM WKNCH
INNER JOIN BOND ON WKNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')

or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and WKNCH.acttime > (select max(acttime)-0.1 from tbl_opslog where seq=3))























