--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('ISCHG') as TableName,
ISCHG.Actflag,
ISCHG.AnnounceDate as Created,
ISCHG.Acttime as Changed,
ISCHG.IschgID,
BOND.SecID,
SCMST.ISIN,
ISCHG.IssID,
ISCHG.NameChangeDate,
ISCHG.IssOldName,
ISCHG.IssNewName,
ISCHG.EventType,
ISCHG.LegalName,
ISCHG.MeetingDateFlag, 
ISCHG.IsChgNotes as Notes
FROM ISCHG
INNER JOIN SCMST ON ISCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
SCMST.isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 211 and actflag<>'I')
or
SCMST.isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 211 and actflag<>'U')
and ISCHG.actflag<>'D'
and (bond.issuedate<=ischg.namechangedate
     or bond.issuedate is null or ischg.namechangedate is null
    )
and ISCHG.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)

