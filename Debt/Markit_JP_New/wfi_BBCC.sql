--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT
upper('BBCC') as tablename,
BBCC.actflag,
BBCC.announcedate,
BBCC.acttime,
BBCC.bbccid,
BBCC.secid,
BBCC.bbcid,
BBCC.oldcntrycd,
BBCC.oldcurencd,
BBCC.effectivedate,
BBCC.newcntrycd,
BBCC.newcurencd,
BBCC.oldbbgcompid,
BBCC.newbbgcompid,
BBCC.oldbbgcomptk,
BBCC.newbbgcomptk,
BBCC.releventid,
BBCC.eventtype,
BBCC.notes
FROM BBCC
inner join scmst on BBCC.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BBCC.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
