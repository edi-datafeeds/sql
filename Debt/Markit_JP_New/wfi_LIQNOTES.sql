--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

use WCA
select
upper('LIQNOTES') as TableName,
LIQ.Actflag,
LIQ.LiqID,
LIQ.LiquidationTerms
FROM LIQ
where
LIQ.Issid in 
(select wca.dbo.scmst.issid from client.dbo.pfisin
inner join wca.dbo.scmst on client.dbo.pfisin.code = wca.dbo.SCMST.isin
where (client.dbo.pfisin.accid=211 and client.dbo.pfisin.actflag='I')
or (client.dbo.pfisin.actflag='U' and LIQ.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)))
