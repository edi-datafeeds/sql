--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT
upper('BBEC') as tablename,
BBEC.actflag,
BBEC.announcedate,
BBEC.acttime,
BBEC.bbecid,
BBEC.secid,
BBEC.bbeid,
BBEC.oldexchgcd,
BBEC.oldcurencd,
BBEC.effectivedate,
BBEC.newexchgcd,
BBEC.newcurencd,
BBEC.oldbbgexhid,
BBEC.newbbgexhid,
BBEC.oldbbgexhtk,
BBEC.newbbgexhtk,
BBEC.releventid,
BBEC.eventtype,
BBEC.notes
FROM bbec as BBEC
inner join scmst on BBEC.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BBEC.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
