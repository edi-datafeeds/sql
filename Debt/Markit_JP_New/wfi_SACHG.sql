--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_SACHG
--fileheadertext=EDI_SACHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SACHG') as TableName,
SACHG.Actflag,
SACHG.AnnounceDate as Created,
SACHG.Acttime as Changed,
SACHG.SachgID,
SACHG.ScagyID,
BOND.SecID,
SCMST.ISIN,
SACHG.EffectiveDate,
SACHG.OldRelationship,
SACHG.NewRelationship,
SACHG.OldAgncyID,
SACHG.NewAgncyID,
SACHG.OldSpStartDate,
SACHG.NewSpStartDate,
SACHG.OldSpEndDate,
SACHG.NewSpEndDate,
SACHG.OldGuaranteeType,
SACHG.NewGuaranteeType
FROM SACHG
INNER JOIN SCAGY ON SACHG.ScagyID = SCAGY.ScagyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SACHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))