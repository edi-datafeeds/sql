--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=EDI_BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('BKRP') as TableName,
BKRP.Actflag,
BKRP.AnnounceDate as Created,
BKRP.Acttime as Changed,  
BKRP.BkrpID,
BOND.SecID,
SCMST.ISIN,
BKRP.IssID,
BKRP.NotificationDate,
BKRP.FilingDate
FROM BKRP
INNER JOIN SCMST ON BKRP.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and BKRP.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
