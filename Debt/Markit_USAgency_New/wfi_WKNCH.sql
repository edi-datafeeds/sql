--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('WKNCH') as TableName,
WKNCH.Actflag,
WKNCH.AnnounceDate as Created,
WKNCH.Acttime as Changed,
WKNCH.SecID,
WKNCH.EffectiveDate,
WKNCH.WknChID,
WKNCH.Eventtype,
WKNCH.RelEventID,
WKNCH.OldWKN,
WKNCH.NewWKN
FROM WKNCH
INNER JOIN BOND ON WKNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and WKNCH.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
