--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('FRNFX') as TableName,
FRNFX.Actflag,
FRNFX.AnnounceDate as Created,
FRNFX.Acttime as Changed,
FRNFX.FrnfxID,
FRNFX.SecID,
SCMST.ISIN,
FRNFX.EffectiveDate,
FRNFX.OldFRNType,
FRNFX.OldFRNIndexBenchmark,
FRNFX.OldMarkup As OldFrnMargin,
FRNFX.OldMinimumInterestRate,
FRNFX.OldMaximumInterestRate,
FRNFX.OldRounding,
FRNFX.NewFRNType,
FRNFX.NewFRNindexBenchmark,
FRNFX.NewMarkup As NewFrnMargin,
FRNFX.NewMinimumInterestRate,
FRNFX.NewMaximumInterestRate,
FRNFX.NewRounding,
FRNFX.Eventtype,
FRNFX.OldFrnIntAdjFreq,
FRNFX.NewFrnIntAdjFreq
FROM FRNFX
INNER JOIN BOND ON FRNFX.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and FRNFX.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)