--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BOCHG
--fileheadertext=EDI_BOCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BOCHG') as TableName,
BOCHG.Actflag,
BOCHG.AnnounceDate as Created,
BOCHG.Acttime as Changed,  
BOCHG.BochgID,
BOCHG.RelEventID,
BOCHG.SecID,
SCMST.ISIN,
BOCHG.EffectiveDate,
BOCHG.OldOutValue,
BOCHG.NewOutValue,
BOCHG.EventType,
BOCHG.OldOutDate,
BOCHG.NewOutDate,
BOCHG.BochgNotes as Notes
FROM BOCHG
INNER JOIN BOND ON BOCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and BOCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
