--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('IRCHG') as TableName,
IRCHG.Actflag,
IRCHG.AnnounceDate as Created,
IRCHG.Acttime as Changed,
IRCHG.IrchgID,
IRCHG.SecID,
SCMST.ISIN,
IRCHG.EffectiveDate,
IRCHG.OldInterestRate,
IRCHG.NewInterestRate,
IRCHG.Eventtype,
IRCHG.Notes 
FROM IRCHG
INNER JOIN BOND ON IRCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and IRCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)