--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_DEBT_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('BBC') as tablename,
bbc.actflag,
bbc.announcedate,
bbc.acttime,
bbc.bbcid,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
bbc.bbgcompid,
bbc.bbgcomptk
from bbc
inner join scmst on bbc.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and bbc.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)