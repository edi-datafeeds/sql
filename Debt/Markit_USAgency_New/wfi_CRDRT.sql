--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=EDI_CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CRDRT') as TableName,
CRDRT.Actflag,
CRDRT.AnnounceDate as Created,
CRDRT.Acttime as Changed, 
CRDRT.SecID,
CRDRT.RatingAgency,
CRDRT.RatingDate,
CRDRT.Rating,
CRDRT.Direction,
CRDRT.WatchList,
CRDRT.WatchListReason
FROM CRDRT
INNER JOIN BOND ON CRDRT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and CRDRT.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
