--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT 
upper('REDMT') as TableName,
REDMT.Actflag,
REDMT.AnnounceDate as Created,
REDMT.Acttime as Changed,
REDMT.RedmtID,
SCMST.SecID,
SCMST.ISIN,
REDMT.RedemptionDate as RedemDate,
REDMT.CurenCD as RedemCurrency,
REDMT.RedemptionPrice as RedemPrice,
REDMT.MandOptFlag,
REDMT.PartFinal,
REDMT.RedemptionType as RedemType,
REDMT.RedemptionAmount as RedemAmount,
REDMT.RedemptionPremium as RedemPremium,
REDMT.RedemInPercent,
REDMT.PriceAsPercent,
REDMT.PremiumAsPercent,
REDMT.RedmtNotes as Notes
FROM REDMT
INNER JOIN BOND ON redmt.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and REDMT.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
