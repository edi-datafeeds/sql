--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BKRPNOTES
--fileheadertext=EDI_BKRPNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('BKRPNOTES') as TableName,
BKRP.Actflag,
BKRP.BkrpID,
BKRP.BkrpNotes as Notes
FROM BKRP
where
BKRP.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and BKRP.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)


