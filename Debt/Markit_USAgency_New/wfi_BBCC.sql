--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_DEBT_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT
upper('BBCC') as tablename,
BBCC.actflag,
BBCC.announcedate,
BBCC.acttime,
BBCC.bbccid,
BBCC.secid,
BBCC.bbcid,
BBCC.oldcntrycd,
BBCC.oldcurencd,
BBCC.effectivedate,
BBCC.newcntrycd,
BBCC.newcurencd,
BBCC.oldbbgcompid,
BBCC.newbbgcompid,
BBCC.oldbbgcomptk,
BBCC.newbbgcomptk,
BBCC.releventid,
BBCC.eventtype,
BBCC.notes
FROM BBCC
inner join scmst on BBCC.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and BBCC.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)