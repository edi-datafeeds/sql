--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate as Created,
SCEXH.Acttime as Changed,
SCEXH.ScExhID,
SCEXH.SecID,
SCMST.ISIN,
SCEXH.ExchgCD,
SCEXH.ListStatus,
SCEXH.Lot,
scexh.MinTrdgQty,
SCEXH.ListDate,
'' as TradeStatus,
SCEXH.LocalCode,
SUBSTRING(SCEXH.JunkLocalcode,0,50) as JunkLocalcode,
SCEXH.ScexhNotes As Notes
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and SCEXH.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)