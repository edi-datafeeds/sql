--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CTCHG') as TableName,
CTCHG.Actflag,
CTCHG.AnnounceDate as Created,
CTCHG.Acttime as Changed, 
CTCHG.CtChgID,
CTCHG.SecID,
SCMST.ISIN,
CTCHG.EffectiveDate,
CTCHG.OldResultantRatio,
CTCHG.NewResultantRatio,
CTCHG.OldSecurityRatio,
CTCHG.NewSecurityRatio,
CTCHG.OldCurrency,
CTCHG.NewCurrency,
CTCHG.OldCurPair,
CTCHG.NewCurPair,
CTCHG.OldConversionPrice,
CTCHG.NewConversionPrice,
CTCHG.ResSectyCD,
CTCHG.OldResSecID,
CTCHG.NewResSecID,
CTCHG.EventType,
CTCHG.RelEventID,
CTCHG.OldFromDate,
CTCHG.NewFromDate,
CTCHG.OldTodate,
CTCHG.NewToDate,
CTCHG.ConvtID,
CTCHG.OldFXRate,
CTCHG.NewFXRate,
CTCHG.OldPriceAsPercent,
CTCHG.NewPriceAsPercent,
CTCHG.Notes
FROM CTCHG
INNER JOIN BOND ON CTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and CTCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
