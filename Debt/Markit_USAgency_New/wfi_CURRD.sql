--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('CURRD') as TableName,
CURRD.Actflag,
CURRD.AnnounceDate as Created,
CURRD.Acttime as Changed,
CURRD.CurrdID,
CURRD.SecID,
SCMST.ISIN,
CURRD.EffectiveDate,
CURRD.OldCurenCD,
CURRD.NewCurenCD,
CURRD.OldParValue,
CURRD.NewParValue,
CURRD.EventType,
CURRD.CurRdNotes as Notes
FROM CURRD
INNER JOIN BOND ON CURRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and CURRD.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)