--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
'COSNT' as TableName,
COSNT.Actflag,
COSNT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > COSNT.Acttime) THEN RD.Acttime ELSE COSNT.Acttime END as [Changed],
COSNT.RdID,
SCMST.SecID,
RD.Recdate,
SCMST.ISIN,
COSNT.ExpiryDate,
COSNT.ExpiryTime,
SUBSTRING(cosnt.TimeZone, 1,3) AS TimeZone,
COSNT.CollateralRelease,
COSNT.Currency,
COSNT.Fee,
COSNT.Notes
FROM COSNT
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and COSNT.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
