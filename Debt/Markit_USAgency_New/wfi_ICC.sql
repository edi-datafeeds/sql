--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ICC
--fileheadertext=EDI_ICC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('ICC') as TableName,
ICC.Actflag,
ICC.AnnounceDate as Created,
ICC.Acttime as Changed,
ICC.IccID,
ICC.SecID,
SCMST.ISIN,
ICC.EffectiveDate,
ICC.OldISIN,
ICC.NewISIN,
ICC.OldUSCode,
ICC.NewUSCode,
ICC.OldVALOREN,
ICC.NewVALOREN,
ICC.EventType,
ICC.RelEventID,
ICC.OldCommonCode,
ICC.NewCommonCode
FROM ICC
INNER JOIN BOND ON ICC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and ICC.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)