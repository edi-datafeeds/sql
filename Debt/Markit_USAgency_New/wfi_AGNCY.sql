--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_DEBT_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select distinct
upper('AGNCY') as TableName,
AGNCY.Actflag,
AGNCY.AnnounceDate as Created,
AGNCY.Acttime as Changed,
AGNCY.AgncyID,
AGNCY.RegistrarName,
AGNCY.Add1,
AGNCY.Add2,
AGNCY.Add3,
AGNCY.Add4,
AGNCY.Add5,
AGNCY.Add6,
AGNCY.City,
AGNCY.CntryCD,
AGNCY.Website,
AGNCY.Contact1,
AGNCY.Tel1,
AGNCY.Fax1,
AGNCY.Email1,
AGNCY.Contact2,
AGNCY.Tel2,
AGNCY.Fax2,
AGNCY.Email2,
AGNCY.Depository,
AGNCY.State
FROM AGNCY
INNER JOIN SCAGY ON AGNCY.AgncyID = SCAGY.AgncyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and agncy.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)