--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_DEBT_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT
upper('BBEC') as tablename,
BBEC.actflag,
BBEC.announcedate,
BBEC.acttime,
BBEC.bbecid,
BBEC.secid,
BBEC.bbeid,
BBEC.oldexchgcd,
BBEC.oldcurencd,
BBEC.effectivedate,
BBEC.newexchgcd,
BBEC.newcurencd,
BBEC.oldbbgexhid,
BBEC.newbbgexhid,
BBEC.oldbbgexhtk,
BBEC.newbbgexhtk,
BBEC.releventid,
BBEC.eventtype,
BBEC.notes
FROM bbec as BBEC
inner join scmst on BBEC.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and BBEC.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)