--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_DEBT_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT distinct
upper('BBE') as tablename,
bbe.actflag,
bbe.announcedate,
bbe.acttime,
bbe.bbeid,
bbe.secid,
bbe.exchgcd,
bbe.curencd,
bbe.bbgexhid,
bbe.bbgexhtk
from bbe
inner join scmst on bbe.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and BBE.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)