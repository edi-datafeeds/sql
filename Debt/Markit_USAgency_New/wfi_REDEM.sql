--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'REDEM' as TableName,
REDEM.Actflag,
REDEM.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > REDEM.Acttime) THEN RD.Acttime ELSE REDEM.Acttime END as [Changed],
REDEM.RedemID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
REDEM.RedemDate,
REDEM.CurenCD as RedemCurrency,
REDEM.RedemPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.RedemType,
REDEM.AmountRedeemed,
REDEM.RedemPremium,
REDEM.RedemPercent,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
REDEM.PriceAsPercent,
REDEM.PremiumAsPercent,
REDEM.InDefPay,
REDEM.TenderOpenDate,
REDEM.TenderCloseDate,
REDEM.RedemNotes as Notes
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON REDEM.RdID = RD.RdID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and REDEM.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
