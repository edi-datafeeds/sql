--filepath=O:\datafeed\debt\Markit_USAgency_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_USAgency_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate as Created,
ISSUR.Acttime as Changed,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
ISSUR.Shortname,
ISSUR.LegalName,
ISSUR.CntryofDom,
ISSUR.StateofDom,
cast(ISSUR.IssurNotes As varchar(8000)),
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN ISSUR.Isstype='GOV' THEN 'G' WHEN ISSUR.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
scmst.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and issur.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
