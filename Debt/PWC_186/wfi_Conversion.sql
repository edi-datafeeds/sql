--filepath=o:\Upload\Acc\186\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_Conversion
--fileheadertext=EDI_WFIPORT_Conversion_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldsepaator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\186\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'Conversion' as EventName,
CONV.ConvID,
CONV.Actflag,
CONV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONV.Acttime) THEN RD.Acttime ELSE CONV.Acttime END as [Changed],
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
CONV.FromDate,
CONV.ToDate,
CONV.RatioNew,
CONV.RatioOld,
CONV.CurenCD as ConvCurrency,
CONV.Price,
CONV.MandOptFlag,
CONV.Fractions,
CONV.FXrate,
CONV.PartFinalFlag,
CONV.ConvType,
CONV.PriceAsPercent,
CONV.ResSectyCD,
CONV.ResSecID,
RESSCMST.ISIN as ResISIN,
CONV.ConvNotes as Notes
FROM CONV
INNER JOIN BOND ON CONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN RD ON CONV.RdID = RD.RdID
LEFT OUTER JOIN SCMST as RESSCMST ON CONV.ResSecID = RESSCMST.SecID
where
(
SCMST.Isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 186 and actflag = 'U')
and (CONV.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
     or RD.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
)
OR
(
SCMST.Isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 186 and actflag = 'I')
and (FromDate>='2008/09/01' or (FromDate is null and CONV.AnnounceDate>='2008/09/01'))
and CONV.actflag<>'D'
)
