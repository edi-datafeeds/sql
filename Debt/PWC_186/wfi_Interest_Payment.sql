--filepath=o:\Upload\Acc\186\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_Interest_Payment
--fileheadertext=EDI_WFIPORT_Interest_Payment_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldsepaator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\186\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'Interest_Payment' as EventName,
INT.RdID,
CASE WHEN INT.Actflag = 'D' or INT.actflag='C' or intpy.actflag is null THEN INT.Actflag ELSE INTPY.Actflag END as Actflag,
INT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > INT.Acttime) and (RD.Acttime > EXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > INT.Acttime) THEN EXDT.Acttime ELSE INT.Acttime END as [Changed],
SCMST.SecID,
SCMST.ISIN,
INTPY.BCurenCD as DebtCurrency,
INTPY.BParValue as NominalValue,
SCEXH.ExchgCD,
RD.Recdate,
EXDT.Exdate,
EXDT.Paydate,
INT.InterestFromDate,
INT.InterestToDate,
INT.Days,
' ' as InterestDefault,
INTPY.CurenCD,
INTPY.IntRate,
INTPY.GrossInterest,
INTPY.NetInterest,
INTPY.DomesticTaxRate,
INTPY.NonResidentTaxRate,
INTPY.RescindInterest,
INTPY.AgencyFees,
INTPY.CouponNo,
null as CouponID,
INTPY.DefaultOpt,
INTPY.OptElectionDate,
INTPY.AnlCoupRate,
[INT].IntNotes as Notes
from [INT]
INNER JOIN RD ON INT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON BOND.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID 
     AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
     AND EXDT.Actflag<>'D'
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID
left outer join lstat on scexh.secid = lstat.secid 
     and scexh.exchgcd = lstat.exchgcd 
     and 'D'= lstat.lstatstatus 
     and scexh.liststatus = 'D'
where
(
SCMST.Isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 186 and actflag = 'U')
and (INT.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
     or RD.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
     or EXDT.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
     or INTPY.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
)
and (scexh.liststatus<>'D' or scexh.liststatus is null)
)
OR
(
SCMST.Isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 186 and actflag = 'I')
and (paydate>='2008/09/01' or (paydate is null and INT.AnnounceDate>='2008/09/01'))
AND (INTPY.actflag<>'D' OR INTPY.ACTFLAG IS NULL)
and INTPY.actflag<>'D'
)
