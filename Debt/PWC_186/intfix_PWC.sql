--filepath=o:\Upload\Acc\186\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_PWC_FIXED_INTEREST
--fileheadertext=EDI_PWC_FIXED_INTEREST_HISTORY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\186\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select distinct portfolio.dbo.intfixpwc.* from portfolio.dbo.intfixpwc
left outer join v54f_920_interest_payment ON portfolio.dbo.intfixpwc.secid = v54f_920_interest_payment.secid
                 and payable_date = paydate
left outer join portfolio.dbo.pwcblock on portfolio.dbo.intfixpwc.secid = portfolio.dbo.pwcblock.secid
where
v54f_920_interest_payment.secid is null
and portfolio.dbo.pwcblock.secid is null