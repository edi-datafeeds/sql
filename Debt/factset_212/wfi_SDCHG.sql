--filepath=o:\upload\acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\212\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SDCHG') as TableName,
SDCHG.Actflag,
SDCHG.AnnounceDate as Created, 
SDCHG.Acttime as Changed,
SDCHG.SdChgID,
SDCHG.SecID,
SCMST.ISIN,
SDCHG.EffectiveDate,
SDCHG.EventType,
SDCHG.OldSedol,
SDCHG.NewSedol,
SDCHG.CntryCD as OldCntryCD,
SDCHG.NewCntryCD,
SDCHG.RcntryCD as OldRcntryCD,
SDCHG.NewRcntryCD
FROM SDCHG
INNER JOIN BOND ON SDCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I')
and SDCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and SDCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
