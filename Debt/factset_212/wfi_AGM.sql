--filepath=o:\upload\acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_AGM
--fileheadertext=EDI_AGM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\212\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
SELECT 
upper('AGM') as TableName,
AGM.Actflag,
AGM.AnnounceDate as Created,
AGM.Acttime as Changed, 
AGM.AGMID,
BOND.SecID,
SCMST.ISIN,
AGM.IssID,
AGM.AGMDate,
AGM.AGMEGM,
AGM.AGMNo,
AGM.FYEDate,
AGM.AGMTime,
AGM.Add1,
AGM.Add2,
AGM.Add3,
AGM.Add4,
AGM.Add5,
AGM.Add6,
AGM.City,
AGM.CntryCD
FROM AGM
INNER JOIN SCMST ON AGM.BondSecID = SCMST.SecID
INNER JOIN BOND ON AGM.BondSecID = BOND.SecID
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I')
and AGM.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and AGM.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
