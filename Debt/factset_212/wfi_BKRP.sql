--filepath=o:\upload\acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_BKRP
--fileheadertext=EDI_BKRP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\212\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('BKRP') as TableName,
BKRP.Actflag,
BKRP.AnnounceDate as Created,
BKRP.Acttime as Changed,  
BKRP.BkrpID,
BOND.SecID,
SCMST.ISIN,
BKRP.IssID,
BKRP.NotificationDate,
BKRP.FilingDate
FROM BKRP
INNER JOIN SCMST ON BKRP.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I')
and BKRP.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and BKRP.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
