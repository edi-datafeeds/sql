--filepath=o:\upload\acc\212\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\212\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'REDEM' as TableName,
REDEM.Actflag,
REDEM.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > REDEM.Acttime) THEN RD.Acttime ELSE REDEM.Acttime END as [Changed],
REDEM.RedemID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
REDEM.RedemDate,
REDEM.CurenCD as RedemCurrency,
--'' as RedemPrice,
case when PremiumAsPercent = '' and redem.RedemType <> 'MAT' then '' else REDEM.RedemPrice end as RedemPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.RedemType,
--'' as AmountRedeemed,
case when PremiumAsPercent = '' and redem.RedemType <> 'MAT' then '' else REDEM.AmountRedeemed end as AmountRedeemed,
--REDEM.RedemPremium,
case when PremiumAsPercent = '' and redem.RedemType <> 'MAT' then '' else REDEM.RedemPremium end as RedemPremium,
--REDEM.RedemPercent,
case when PremiumAsPercent = '' and redem.RedemType <> 'MAT' then '' else REDEM.RedemPercent end as RedemPercent,
-- redem.PoolFactor,
case when redem.secid in 
(select secid from redem as subredem 
where redem.secid=subredem.secid 
and subredem.PoolFactor <> '' 
and subredem.PoolFactor is not null 
or PremiumAsPercent = '' 
and subredem.RedemType <> 'MAT') 
then '' 
WHEN CHARINDEX('.',redem.poolfactor) < 5
     THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
     ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
    
END AS poolfactor,
--REDEM.PriceAsPercent,
case when PremiumAsPercent = '' and redem.RedemType <> 'MAT' then '' else REDEM.PriceAsPercent end as PriceAsPercent,
REDEM.PremiumAsPercent,
REDEM.InDefPay,
REDEM.RedemNotes as Notes
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON REDEM.RdID = RD.RdID
where
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='I')
and REDEM.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
or 
(isin in (select code from client.dbo.pfisin where accid=212 and actflag='U')
and REDEM.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
