--filepath=o:\datafeed\debt\rbcnew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\rbcnew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('RDNOM') as TableName,
RDNOM.Actflag,
RDNOM.AnnounceDate as Created,
RDNOM.Acttime as Changed, 
RDNOM.RdnomID,
RDNOM.SecID,
SCMST.ISIN,
RDNOM.EffectiveDate,
RDNOM.OldDenomination1,
RDNOM.OldDenomination2,
RDNOM.OldDenomination3,
RDNOM.OldDenomination4,
RDNOM.OldDenomination5,
RDNOM.OldDenomination6,
RDNOM.OldDenomination7,
RDNOM.OldMinimumDenomination,
RDNOM.OldDenominationMultiple,
RDNOM.NewDenomination1,
RDNOM.NewDenomination2,
RDNOM.NewDenomination3,
RDNOM.NewDenomination4,
RDNOM.NewDenomination5,
RDNOM.NewDenomination6,
RDNOM.NewDenomination7,
RDNOM.NewMinimumDenomination,
RDNOM.NewDenominationMultiple,
RDNOM.Notes
FROM RDNOM
INNER JOIN BOND ON RDNOM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and RDNOM.acttime>=(select max(wca.dbo.tbl_Opslog.acttime) from wca.dbo.tbl_Opslog where seq = 3)
