--filepath=o:\datafeed\debt\rbcnew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_REDEM
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\rbcnew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'REDEM' as TableName,
REDEM.Actflag,
REDEM.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > REDEM.Acttime) THEN RD.Acttime ELSE REDEM.Acttime END as [Changed],
REDEM.RedemID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
REDEM.RedemDate,
REDEM.CurenCD as RedemCurrency,
REDEM.RedemPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.RedemType,
REDEM.AmountRedeemed,
REDEM.RedemPremium,
REDEM.RedemPercent,
case when indef.defaulttype<>'' then 'T' else 'F' end as RedemDefault,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
REDEM.PriceAsPercent,
REDEM.PremiumAsPercent,
REDEM.InDefPay,
INDEF.Notes as Default_Notes,
REDEM.RedemNotes as Notes
FROM REDEM
INNER JOIN BOND ON REDEM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON REDEM.RdID = RD.RdID
left outer join indef on redem.secid = indef.secid and redem.redemdate = indef.defaultdate and 'REDDEFA' = indef.defaulttype
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and REDEM.acttime>=(select max(wca.dbo.tbl_Opslog.acttime) from wca.dbo.tbl_Opslog where seq = 3)
and redemtype<>'TENDER'
