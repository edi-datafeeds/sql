--filepath=o:\datafeed\debt\rbcnew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_EXOFF
--fileheadertext=EDI_EXOFF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\rbcnew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
'EXOFF' as TableName,
CONV.Actflag,
CONV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONV.Acttime) THEN RD.Acttime ELSE CONV.Acttime END as [Changed],
CONV.ConvID as ExoffID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
CONV.FromDate,
CONV.ToDate,
CONV.RatioNew,
CONV.RatioOld,
CONV.CurenCD as Currency,
CONV.Price,
CONV.MandOptFlag,
CONV.Fractions,
CONV.FXrate,
CONV.PartFinalFlag,
CONV.PriceAsPercent,
CONV.ResSectyCD,
CONV.ResSecID,
RESSCMST.ISIN as ResISIN,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
CONV.ConvNotes as Notes
FROM CONV
INNER JOIN BOND ON CONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON CONV.RdID = RD.RdID
LEFT OUTER JOIN SCMST as RESSCMST ON CONV.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and CONV.acttime>=(select max(wca.dbo.tbl_Opslog.acttime) from wca.dbo.tbl_Opslog where seq = 3)
and Convtype='TENDER'

