--filepath=o:\datafeed\debt\rbcnew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\rbcnew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LIQ') as TableName,
LIQ.Actflag,
LIQ.AnnounceDate as Created,
LIQ.Acttime as Changed,
LIQ.LiqID,
BOND.SecID,
SCMST.ISIN,
LIQ.IssID,
LIQ.Liquidator,
LIQ.LiqAdd1,
LIQ.LiqAdd2,
LIQ.LiqAdd3,
LIQ.LiqAdd4,
LIQ.LiqAdd5,
LIQ.LiqAdd6,
LIQ.LiqCity,
LIQ.LiqCntryCD,
LIQ.LiqTel,
LIQ.LiqFax,
LIQ.LiqEmail,
LIQ.RdDate
FROM LIQ
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
LIQ.Issid in (select wca.dbo.scmst.issid from portfolio.dbo.wfisample
inner join wca.dbo.scmst on portfolio.dbo.wfisample.secid = wca.dbo.scmst.secid)
and LIQ.acttime>=(select max(wca.dbo.tbl_Opslog.acttime) from wca.dbo.tbl_Opslog where seq = 3)
