--filepath=o:\datafeed\debt\rbcnew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\rbcnew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('IFCHG') as TableName,
IFCHG.Actflag,
IFCHG.AnnounceDate as Created,
IFCHG.Acttime as Changed,
IFCHG.IfchgID,
IFCHG.SecID,
SCMST.ISIN,
IFCHG.NotificationDate,
IFCHG.OldIntPayFrqncy,
IFCHG.OldIntPayDate1,
IFCHG.OldIntPayDate2,
IFCHG.OldIntPayDate3,
IFCHG.OldIntPayDate4,
IFCHG.NewIntPayFrqncy,
IFCHG.NewIntPayDate1,
IFCHG.NewIntPayDate2,
IFCHG.NewIntPayDate3,
IFCHG.NewIntPayDate4,
IFCHG.Eventtype
FROM IFCHG
INNER JOIN BOND ON IFCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and IFCHG.acttime>=(select max(wca.dbo.tbl_Opslog.acttime) from wca.dbo.tbl_Opslog where seq = 3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'
