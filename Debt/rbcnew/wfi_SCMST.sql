--filepath=o:\datafeed\debt\rbcnew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\rbcnew\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate as Created,
SCMST.Acttime as Changed,
SCMST.SecID,
SCMST.ISIN,
SCMST.IssID,
SCMST.SectyCD,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.X as CommonCode,
SCMST.Holding,
SCMST.StructCD,
SCMST.RegS144A
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and scmst.acttime>=(select max(wca.dbo.tbl_Opslog.acttime) from wca.dbo.tbl_Opslog where seq = 3)
