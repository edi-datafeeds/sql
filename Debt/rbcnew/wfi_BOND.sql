--filepath=o:\datafeed\debt\rbcnew\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\rbcnew\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BOND') as Tablename,
BOND.Actflag,
BOND.AnnounceDate as Created,
BOND.Acttime as Changed,
BOND.SecID,
SCMST.ISIN,
BOND.BondType,
BOND.DebtMarket,
BOND.CurenCD as DebtCurrency,
BOND.ParValue as NominalValue,
BOND.IssueDate,
BOND.IssueCurrency,
BOND.IssuePrice,
BOND.IssueAmount,
BOND.IssueAmountDate,
BOND.OutstandingAmount,
BOND.OutstandingAmountDate,
BOND.InterestBasis,
BOND.InterestRate,
BOND.InterestAccrualConvention,
BOND.InterestPaymentFrequency,
BOND.IntCommencementDate as InterestCommencementDate,
BOND.FirstCouponDate,
BOND.InterestPayDate1,
BOND.InterestPayDate2,
BOND.InterestPayDate3,
BOND.InterestPayDate4,
BOND.DomesticTaxRate,
BOND.NonResidentTaxRate,
BOND.FRNType,
BOND.FRNIndexBenchmark,
BOND.Markup as FrnMargin,
BOND.MinimumInterestRate as FrnMinInterestRate,
BOND.MaximumInterestRate as FrnMaxInterestRate,
BOND.Rounding as FrnRounding,
BOND.Series,
BOND.Class,
BOND.OnTap,
BOND.MaximumTapAmount,
BOND.TapExpiryDate,
BOND.Guaranteed,
BOND.SecuredBy,
BOND.SecurityCharge,
BOND.Subordinate,
BOND.SeniorJunior,
BOND.WarrantAttached,
BOND.MaturityStructure,
BOND.Perpetual,
BOND.MaturityDate,
BOND.MaturityExtendible,
BOND.Callable,
BOND.Puttable,
BOND.Denomination1,
BOND.Denomination2,
BOND.Denomination3,
BOND.Denomination4,
BOND.Denomination5,
BOND.Denomination6,
BOND.Denomination7,
BOND.MinimumDenomination,
BOND.DenominationMultiple,
BOND.Strip,
BOND.StripInterestNumber, 
BOND.Bondsrc,
BOND.MaturityBenchmark,
BOND.ConventionMethod,
BOND.FrnIntAdjFreq as FrnInterestAdjFreq,
BOND.IntBusDayConv as InterestBusDayConv,
BOND.InterestCurrency,
BOND.MatBusDayConv as MaturityBusDayConv,
BOND.MaturityCurrency, 
BOND.TaxRules,
BOND.VarIntPayDate as VarInterestPaydate,
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
BOND.MatPrice as MaturityPrice,
BOND.MatPriceAsPercent as MaturityPriceAsPercent,
BOND.SinkingFund,
BOND.GoverningLaw,
BOND.Municipal,
BOND.PrivatePlacement,
BOND.Syndicated,
BOND.Tier,
BOND.UppLow,
BOND.Collateral,
BOND.CoverPool,
BOND.PikPay,
BOND.Notes
FROM BOND
inner join scmst on bond.secid = scmst.secid
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and bond.acttime>=(select max(wca.dbo.tbl_Opslog.acttime) from wca.dbo.tbl_Opslog where seq = 3)
