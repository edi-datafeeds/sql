--filepath=o:\upload\acc\192\feed\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=
--EDI_wfisample_CONVT_
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('CONVT') as TableName,
CONVT.Actflag,
CONVT.AnnounceDate as Created,
CONVT.Acttime as Changed,
CONVT.ConvtID,
SCMST.SecID,
SCMST.ISIN,
CONVT.FromDate,
CONVT.ToDate,
CONVT.RatioNew,
CONVT.RatioOld,
CONVT.CurenCD,
CONVT.Price,
CONVT.MandOptFlag,
CONVT.Fractions,
CONVT.FXrate,
CONVT.PartFinalFlag,
CONVT.PriceAsPercent,
CONVT.ResSecID,
RESSCMST.ISIN as ResISIN,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
CONVT.ConvtNotes as Notes
FROM CONVT
INNER JOIN BOND ON convt.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as RESSCMST ON CONVT.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='U')
and convt.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='I')
or SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 192 and actflag='I')
