--filepath=o:\upload\acc\192\feed\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=
--EDI_wfisample_ISSUR_
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate as Created,
ISSUR.Acttime as Changed,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
ISSUR.Shortname,
ISSUR.LegalName,
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN ISSUR.Isstype='GOV' THEN 'G' WHEN ISSUR.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype
from client.dbo.pfissid
inner join issur on client.dbo.pfissid.code = issur.issid and client.dbo.pfissid.accid = 192 and client.dbo.pfissid.actflag<>'D'
where
(ISSUR.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
or client.dbo.pfissid.actflag='I')
