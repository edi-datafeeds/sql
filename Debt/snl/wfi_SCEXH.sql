--filepath=o:\upload\acc\192\feed\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=
--EDI_wfisample_SCEXH_
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate as Created,
SCEXH.Acttime as Changed,
SCEXH.ScExhID,
SCEXH.SecID,
SCMST.ISIN,
SCEXH.ExchgCD,
SCEXH.ListStatus,
SCEXH.Lot,
scexh.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
SUBSTRING(SCEXH.JunkLocalcode,0,50) as JunkLocalcode
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='U')
and SCEXH.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='I')
or SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 192 and actflag='I')
