--filepath=o:\upload\acc\192\feed\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_BOND
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 

--# 
use WCA
select
upper('BOND') as Tablename,
BOND.Actflag,
BOND.AnnounceDate as Created,
BOND.Acttime as Changed,
BOND.SecID,
SCMST.ISIN,
BOND.BondType,
BOND.DebtMarket,
BOND.CurenCD as DebtCurrency,
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as NominalValue,
BOND.IssueDate,
BOND.IssueCurrency,
BOND.IssuePrice,
BOND.IssueAmount,
BOND.IssueAmountDate,
BOND.OutstandingAmount,
BOND.OutstandingAmountDate as OutstandingAmountDate,
BOND.InterestBasis,
BOND.InterestRate,
BOND.InterestAccrualConvention,
BOND.InterestPaymentFrequency,
BOND.IntCommencementDate as InterestCommencementDate,
BOND.FirstCouponDate,
BOND.InterestPayDate1,
BOND.InterestPayDate2,
BOND.InterestPayDate3,
BOND.InterestPayDate4,
bondx.DomesticTaxRate,
bondx.NonResidentTaxRate,
BOND.FRNType,
BOND.FRNIndexBenchmark,
BOND.Markup as FrnMargin,
BOND.MinimumInterestRate as FrnMinInterestRate,
BOND.MaximumInterestRate as FrnMaxInterestRate,
BOND.Rounding as FrnRounding,
bondx.Series,
bondx.Class,
bondx.OnTap,
bondx.MaximumTapAmount,
bondx.TapExpiryDate,
BOND.Guaranteed,
BOND.SecuredBy,
BOND.SecurityCharge,
BOND.Subordinate,
BOND.SeniorJunior,
BOND.WarrantAttached,
BOND.MaturityStructure,
BOND.Perpetual,
BOND.MaturityDate,
BOND.MaturityExtendible,
BOND.Callable,
BOND.Puttable,
bondx.Denomination1,
bondx.Denomination2,
bondx.Denomination3,
bondx.Denomination4,
bondx.Denomination5,
bondx.Denomination6,
bondx.Denomination7,
bondx.MinimumDenomination,
bondx.DenominationMultiple,
BOND.Strip,
BOND.StripInterestNumber, 
BOND.Bondsrc,
BOND.MaturityBenchmark,
BOND.ConventionMethod,
BOND.FrnIntAdjFreq as FrnInterestAdjFreq,
BOND.IntBusDayConv as InterestBusDayConv,
BOND.InterestCurrency,
BOND.MatBusDayConv as MaturityBusDayConv,
BOND.MaturityCurrency, 
bondx.TaxRules,
BOND.VarIntPayDate as VarInterestPaydate,
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
BOND.MatPrice as MaturityPrice,
BOND.MatPriceAsPercent as MaturityPriceAsPercent,
BOND.SinkingFund,
bondx.GoverningLaw,
BOND.Municipal,
BOND.PrivatePlacement,
BOND.Syndicated,
BOND.Notes,
BOND.Tier,
BOND.UppLow,
BOND.Collateral,
BOND.CoverPool,
BOND.CoCoTrigger,
BOND.CoCoAct,
BOND.NonViability,
BONDX.Taxability,
bond.LatestAppliedINTPYAnlCpnRateDate as LatestApplicablePayDate
FROM BOND
inner join scmst on bond.secid = scmst.secid
left outer join bondx on bond.secid = bondx.secid
where
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='U')
and (bond.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
     or scmst.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)))
or SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='I')
or SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 192 and actflag='I')

or SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 990)