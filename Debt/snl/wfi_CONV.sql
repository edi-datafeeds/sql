--filepath=o:\upload\acc\192\feed\
--filenameprefix=EDI
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_CONV
--fileheadertext=
--EDI_wfisample_CONV_
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=|
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\192\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 

--# 1
use wca
select
'CONV' as TableName,
CONV.Actflag,
CONV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONV.Acttime) THEN RD.Acttime ELSE CONV.Acttime END as [Changed],
CONV.ConvID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
CONV.FromDate,
CONV.ToDate,
CONV.RatioNew,
CONV.RatioOld,
CONV.CurenCD as ConvCurrency,
CONV.Price,
CONV.MandOptFlag,
CONV.Fractions,
CONV.FXrate,
CONV.PartFinalFlag,
CONV.ConvType,
CONV.PriceAsPercent,
CONV.ResSectyCD,
CONV.ResSecID,
RESSCMST.ISIN as ResISIN,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
CONV.ConvNotes as Notes
FROM CONV
INNER JOIN BOND ON CONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN RD ON CONV.RdID = RD.RdID
LEFT OUTER JOIN SCMST as RESSCMST ON CONV.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='U')
and CONV.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
and Convtype<>'TENDER'
)
or
(SCMST.issid in (select client.dbo.pfissid.code from client.dbo.pfissid where accid = 192 and actflag='I')
and convtype<>'TENDER')
or 
(SCMST.secid in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 192 and actflag='I')
and convtype='TENDER')

