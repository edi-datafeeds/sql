--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('REDMT') as Tablename,
wca.redmt.ActFlag,
wca.redmt.AnnounceDate as Created,
wca.redmt.Acttime as Changed,
wca.redmt.RedmtId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.redmt.RedemptionDate as RedemDate,
wca.redmt.Curencd as RedemCurrency,
wca.redmt.RedemptionPrice as RedemPrice,
wca.redmt.MandOptFlag,
wca.redmt.PartFinal,
wca.redmt.RedemptionType as RedemType,
wca.redmt.RedemptionAmount as RedemAmount,
wca.redmt.RedemptionPremium as RedemPremium,
wca.redmt.RedemInPercent,
wca.redmt.PriceAsPercent,
wca.redmt.PremiumAsPercent,
wca.redmt.RedmtNotes as Notes
from wca.redmt
inner join wca.bond on wca.redmt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.redmt.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)