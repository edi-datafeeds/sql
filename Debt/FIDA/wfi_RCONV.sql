--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_RCONV
--fileheadertext=EDI_RCONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('RCONV') as Tablename,
wca.rconv.ActFlag,
wca.rconv.AnnounceDate as Created,
wca.rconv.Acttime as Changed,
wca.rconv.RconvId,
wca.rconv.SecId,
wca.scmst.Isin,
wca.rconv.EffectiveDate,
wca.rconv.OldInterestAccrualConvention,
wca.rconv.NewInterestAccrualConvention,
wca.rconv.OldConvMethod,
wca.rconv.NewConvMethod,
wca.rconv.EventType,
wca.rconv.Notes
from wca.rconv
inner join wca.bond on wca.rconv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.rconv.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)