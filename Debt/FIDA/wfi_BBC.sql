--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select distinct
upper('BBC') as Tablename,
wca.bbc.ActFlag,
wca.bbc.AnnounceDate,
wca.bbc.Acttime,
wca.bbc.BbcId,
wca.bbc.SecId,
wca.bbc.Cntrycd,
wca.bbc.Curencd,
wca.bbc.BbgcompId,
wca.bbc.Bbgcomptk
from wca.bbc
inner join wca.bond on wca.bbc.secid = wca.bond.secid
inner join wca.scmst on wca.bbc.secid = wca.scmst.secid
INNER JOIN wca.scexh ON wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.bbc.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)