--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_USTRCE_Full
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=N
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
-- upper('wca.bond') as tablename,
-- wca.bond.actflag,
-- wca.bond.announcedate as created,
-- wca.bond.acttime as changed,
wca.bond.secid
-- wca.scmst.isin,
-- wca.bond.bondtype,
-- wca.bond.debtmarket,
-- wca.bond.curencd as debtcurrency,
-- case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue when wca.bond.parvalue<>'' and wca.bond.parvalue<> 0 then wca.bond.parvalue else wca.scmst.parvalue end as NominalValue,
-- wca.bond.issuedate,
-- wca.bond.issuecurrency,
-- wca.bond.issueprice,
-- wca.bond.issueamount,
-- wca.bond.issueamountdate,
-- wca.bond.outstandingamount,
-- wca.bond.outstandingamountdate,
-- wca.bond.interestbasis,
-- wca.bond.interestrate,
-- wca.bond.interestaccrualconvention,
-- wca.bond.interestpaymentfrequency,
-- wca.bond.intcommencementdate,
-- wca.bond.firstcoupondate,
-- wca.bond.interestpaydate1,
-- wca.bond.interestpaydate2,
-- wca.bond.interestpaydate3,
-- wca.bond.interestpaydate4,
-- wca.bond.domestictaxrate,
-- wca.bond.nonresidenttaxrate,
-- wca.bond.frntype,
-- wca.bond.frnindexbenchmark,
-- wca.bond.markup as frnmargin,
-- wca.bond.minimuminterestrate as frnmininterestrate,
-- wca.bond.maximuminterestrate as frnmaxinterestrate,
-- wca.bond.rounding,
-- wca.bond.series,
-- wca.bond.class,
-- wca.bond.ontap,
-- wca.bond.maximumtapamount,
-- wca.bond.tapexpirydate,
-- wca.bond.guaranteed,
-- wca.bond.securedby,
-- wca.bond.securitycharge,
-- wca.bond.subordinate,
-- wca.bond.seniorjunior,
-- wca.bond.warrantattached,
-- wca.bond.maturitystructure,
-- wca.bond.perpetual,
-- wca.bond.maturitydate,
-- wca.bond.maturityextendible,
-- wca.bond.callable,
-- wca.bond.puttable,
-- wca.bond.denomination1,
-- wca.bond.denomination2,
-- wca.bond.denomination3,
-- wca.bond.denomination4,
-- wca.bond.denomination5,
-- wca.bond.denomination6,
-- wca.bond.denomination7,
-- wca.bond.minimumdenomination,
-- wca.bond.denominationmultiple,
-- wca.bond.strip,
-- wca.bond.stripinterestnumber, 
-- wca.bond.bondsrc,
-- wca.bond.maturitybenchmark,
-- wca.bond.conventionmethod,
-- wca.bond.frnintadjfreq as frninterestadjfreq,
-- wca.bond.intbusdayconv as interestbusdayconv,
-- wca.bond.interestcurrency,
-- wca.bond.matbusdayconv as maturitybusdayconv,
-- wca.bond.maturitycurrency, 
-- wca.bond.taxrules,
-- wca.bond.varintpaydate as varinterestpaydate,
-- wca.bond.priceaspercent,
-- wca.bond.payoutmode,
-- wca.bond.cumulative,
-- case when wca.bond.matprice<>'' then cast(wca.bond.matprice as decimal(18,4))
--      when rtrim(wca.bond.largeparvalue)='' then null
--      when rtrim(wca.bond.matpriceaspercent)='' then null
--      else cast(wca.bond.largeparvalue as decimal(18,0)) * cast(wca.bond.matpriceaspercent as decimal(18,4))/100 
--      end as matprice,
-- wca.bond.matpriceaspercent,
-- wca.bond.sinkingfund,
-- wca.bond.govcity,
-- wca.bond.govstate,
-- wca.bond.govcntry,
-- wca.bond.govlawlkup as govlaw,
-- wca.bond.governinglaw as govlawnotes,
-- wca.bond.municipal,
-- wca.bond.privateplacement,
-- wca.bond.syndicated,
-- wca.bond.tier,
-- wca.bond.upplow,
-- wca.bond.collateral,
-- wca.bond.coverpool,
-- wca.bond.pikpay,
-- wca.bond.yieldatissue,
-- wca.bond.CoCoTrigger as Coco,
-- wca.bond.notes
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.bond.actflag<>'d'
and wca.scmst.statusflag <> 'i'
and wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis ='fxd'
and wca.bond.curencd = 'usd'
AND (CASE WHEN wca.bond.OutstandingAmount >= 350    

                THEN '1'

                ELSE 

                CASE WHEN wca.bond.OutstandingAmount is NULL  And bond.IssueAmount >= 350  

                THEN '1'

                ELSE '0' END

                END = '1')
order by wca.bond.outstandingamount
