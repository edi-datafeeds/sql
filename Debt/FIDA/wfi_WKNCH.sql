--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('WKNCH') as Tablename,
wca.wknch.ActFlag,
wca.wknch.AnnounceDate as Created,
wca.wknch.Acttime as Changed,
wca.wknch.SecId,
wca.wknch.EffectiveDate,
wca.wknch.WknchId,
wca.wknch.EventType,
wca.wknch.ReleventId,
wca.wknch.OldWkn,
wca.wknch.NewWkn
from wca.wknch
inner join wca.bond on wca.wknch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.wknch.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)