--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
upper('BBCC') as Tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate,
wca.bbcc.Acttime,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.bbcc.BbcId,
wca.bbcc.OldCntrycd,
wca.bbcc.OldCurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.NewCntrycd,
wca.bbcc.NewCurencd,
wca.bbcc.OldBbgcompId,
wca.bbcc.NewBbgcompId,
wca.bbcc.OldBbgcomptk,
wca.bbcc.NewBbgcomptk,
wca.bbcc.RelEventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.bond on wca.bbcc.secid = wca.bond.secid
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.bbcc.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)