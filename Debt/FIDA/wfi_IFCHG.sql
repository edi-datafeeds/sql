--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('IFCHG') as Tablename,
wca.ifchg.ActFlag,
wca.ifchg.AnnounceDate as Created,
wca.ifchg.Acttime as Changed,
wca.ifchg.IfchgId,
wca.ifchg.SecId,
wca.scmst.Isin,
wca.ifchg.NotificationDate,
wca.ifchg.OldintPayFrqncy,
wca.ifchg.OldintPayDate1,
wca.ifchg.OldintPayDate2,
wca.ifchg.OldintPayDate3,
wca.ifchg.OldintPayDate4,
wca.ifchg.NewintPayFrqncy,
wca.ifchg.NewintPayDate1,
wca.ifchg.NewintPayDate2,
wca.ifchg.NewintPayDate3,
wca.ifchg.NewintPayDate4,
wca.ifchg.EventType
from wca.ifchg
inner join wca.bond on wca.ifchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.ifchg.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)