--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
upper('BBEC') as Tablename,
wca.bbec.Actflag,
wca.bbec.AnnounceDate,
wca.bbec.Acttime,
wca.bbec.BbecId,
wca.bbec.SecId,
wca.bbec.BbeId,
wca.bbec.OldExchgcd,
wca.bbec.OldCurencd,
wca.bbec.EffectiveDate,
wca.bbec.NewExchgcd,
wca.bbec.NewCurencd,
wca.bbec.OldBbgexhId,
wca.bbec.NewBbgexhId,
wca.bbec.OldBbgexhtk,
wca.bbec.NewBbgexhtk,
wca.bbec.RelEventId,
wca.bbec.EventType,
wca.bbec.Notes
from wca.bbec
inner join wca.bond on wca.bbec.secid = wca.bond.secid
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.bbec.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
