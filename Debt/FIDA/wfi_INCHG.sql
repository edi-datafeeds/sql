--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('INCHG') as Tablename,
wca.inchg.ActFlag,
wca.inchg.AnnounceDate as Created,
wca.inchg.Acttime as Changed,
wca.inchg.InchgId,
wca.bond.SecId,
wca.scmst.Isin,
wca.inchg.IssId,
wca.inchg.InchgDate,
wca.inchg.OldCntrycd,
wca.inchg.NewCntrycd,
wca.inchg.EventType 
from wca.inchg
inner join wca.scmst on wca.inchg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.inchg.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)