--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCCHG') as Tablename,
wca.scchg.ActFlag,
wca.scchg.AnnounceDate as Created,
wca.scchg.Acttime as Changed,
wca.scchg.ScchgId,
wca.scchg.SecId,
wca.scmst.Isin,
wca.scchg.DateOfChange,
wca.scchg.SecOldName,
wca.scchg.SecNewName,
wca.scchg.EventType,
wca.scchg.OldSectycd,
wca.scchg.NewSectycd,
wca.scchg.OldRegs144a,
wca.scchg.NewRegs144a,
wca.scchg.ScchgNotes as Notes
from wca.scchg
inner join wca.bond on wca.scchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.scchg.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)