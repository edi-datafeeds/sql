--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('SFUND') as Tablename,
wca.sfund.ActFlag,
wca.sfund.AnnounceDate as Created,
wca.sfund.Acttime as Changed,
wca.sfund.SfundId,
wca.sfund.SecId,
wca.scmst.Isin,
wca.sfund.SfundDate,
wca.sfund.Curencd as Sinking_Currency,
wca.sfund.Amount,
wca.sfund.AmountAsPercent,
wca.sfund.SfundNotes as Notes
from wca.sfund
inner join wca.bond on wca.sfund.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.sfund.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)