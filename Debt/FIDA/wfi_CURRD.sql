--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CURRD') as Tablename,
wca.currd.ActFlag,
wca.currd.AnnounceDate as Created,
wca.currd.Acttime as Changed,
wca.currd.CurrdId,
wca.currd.SecId,
wca.scmst.Isin,
wca.currd.EffectiveDate,
wca.currd.OldCurencd,
wca.currd.NewCurencd,
wca.currd.OldParValue,
wca.currd.NewParValue,
wca.currd.EventType,
wca.currd.CurrdNotes as Notes
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.currd.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
