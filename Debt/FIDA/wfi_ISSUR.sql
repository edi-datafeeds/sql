--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('ISSUR') as Tablename,
wca.issur.ActFlag,
wca.issur.AnnounceDate as Created,
wca.issur.Acttime as Changed,
wca.issur.IssId,
wca.issur.IssuerName,
wca.issur.IndusId,
wca.issur.CntryOfIncorp,
wca.issur.FinancialYearEnd,
wca.issur.ShortName,
wca.issur.LegalName,
wca.issur.CntryOfDom,
wca.issur.StateOfDom,
wca.issur.IssType,
#cast(wca.Issur.issurnotes as varchar(8000))
wca.issur.IssurNotes
#case when wca.issur.cntryofincorp='aa' then 's' when wca.issur.isstype='gov' then 'g' when wca.issur.isstype='govagency' then 'y' else 'c' end as debttype
from wca.issur
inner join wca.scmst on wca.issur.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.issur.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)
order by wca.issur.isstype