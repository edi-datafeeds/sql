--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.tbl_opslog.Feeddate),112) from wca.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_CPOPT11
--fileheadertext=EDI_CPOPT_11_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.CpoptID,
SCMST.SecID,
SCMST.ISIN,
CPOPT.CallPut, 
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.cptype,
CPOPT.PriceAsPercent,
CPOPT.InWholePart,
CPOPT.FormulaBasedPrice,
CPOPT.Notes as Notes 
FROM CPOPT
INNER JOIN BOND ON CPOPT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
SCMST.isin in (select client.dbo.pfsecid.code from client.dbo.pfsecid where accid = 990')
-and CPOPT.actflag<>'D'
and cpoptid>5972563 and cpoptid < 6466894