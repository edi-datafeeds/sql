--filepath=O:\Upload\Acc\238\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=EDI_CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\238\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CRCHG') as Tablename,
wca.crchg.ActFlag,
wca.crchg.AnnounceDate as Created,
wca.crchg.Acttime as Changed, 
wca.crchg.CrchgId,
wca.crchg.SecId,
wca.crchg.RatingAgency,
wca.crchg.RatingDate,
wca.crchg.OldRating,
wca.crchg.NewRating,
wca.crchg.Direction,
wca.crchg.WatchList,
wca.crchg.WatchListReason
from wca.crchg
inner join wca.bond on wca.crchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
where
wca.scexh.exchgcd = 'ustrce'
and wca.bond.interestbasis = 'FXD'
and wca.crchg.acttime> (select date_sub(max(feeddate), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3)