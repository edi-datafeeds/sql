--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LAWSTNOTES') as TableName,
LAWST.Actflag,
LAWST.LawstID,
LAWST.LawstNotes as Notes
FROM LAWST
where
LAWST.Issid in (
select wca.dbo.scmst.issid from client.dbo.pfisin
inner join wca.dbo.scmst on client.dbo.pfisin.code = wca.dbo.scmst.isin
where client.dbo.pfisin.accid=219
and (LAWST.acttime > (select max(feeddate)-0.1 from tbl_Opslog where seq = 3)
or client.dbo.pfisin.actflag='I'))
