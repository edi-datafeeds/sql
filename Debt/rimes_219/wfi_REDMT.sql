--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('REDMT') as TableName,
REDMT.Actflag,
REDMT.AnnounceDate as Created,
REDMT.Acttime as Changed,
REDMT.RedmtID,
SCMST.SecID,
SCMST.ISIN,
REDMT.RedemptionDate as RedemDate,
REDMT.CurenCD as RedemCurrency,
REDMT.RedemptionPrice as RedemPrice,
REDMT.MandOptFlag,
REDMT.PartFinal,
REDMT.RedemptionType as RedemType,
REDMT.RedemptionAmount as RedemAmount,
REDMT.RedemptionPremium as RedemPremium,
REDMT.RedemInPercent,
REDMT.PriceAsPercent,
REDMT.PremiumAsPercent,
REDMT.RedmtNotes as Notes
FROM REDMT
INNER JOIN BOND ON redmt.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=219 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=219 and actflag='U')
and redmt.acttime > (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
