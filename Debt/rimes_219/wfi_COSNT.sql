--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
'COSNT' as TableName,
COSNT.Actflag,
COSNT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > COSNT.Acttime) THEN RD.Acttime ELSE COSNT.Acttime END as [Changed],
COSNT.RdID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
COSNT.ExpiryDate,
COSNT.ExpiryTime,
SUBSTRING(cosnt.TimeZone, 1,3) AS TimeZone,
COSNT.CollateralRelease,
COSNT.Currency,
COSNT.Fee,
COSNT.Notes
FROM COSNT
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=219 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=219 and actflag='U')
and COSNT.acttime > (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
