--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('FRNFX') as TableName,
FRNFX.Actflag,
FRNFX.AnnounceDate as Created,
FRNFX.Acttime as Changed,
FRNFX.FrnfxID,
FRNFX.SecID,
SCMST.ISIN,
FRNFX.EffectiveDate,
FRNFX.OldFRNType,
FRNFX.OldFRNIndexBenchmark,
FRNFX.OldMarkup,
FRNFX.OldMinimumInterestRate,
FRNFX.OldMaximumInterestRate,
FRNFX.OldRounding,
FRNFX.NewFRNType,
FRNFX.NewFRNindexBenchmark,
FRNFX.NewMarkup,
FRNFX.NewMinimumInterestRate,
FRNFX.NewMaximumInterestRate,
FRNFX.NewRounding,
FRNFX.Eventtype,
FRNFX.OldFrnIntAdjFreq,
FRNFX.NewFrnIntAdjFreq
FROM FRNFX
INNER JOIN BOND ON FRNFX.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=219 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=219 and actflag='U')
and FRNFX.acttime > (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
