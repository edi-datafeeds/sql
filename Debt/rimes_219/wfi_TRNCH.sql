--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('TRNCH') as TableName,
TRNCH.Actflag,
TRNCH.AnnounceDate as Created,
TRNCH.Acttime as Changed,
TRNCH.TrnchNumber,
TRNCH.SecID,
SCMST.ISIN,
TRNCH.TrancheDate,
TRNCH.TrancheAmount,
TRNCH.ExpiryDate,
TRNCH.Notes as Notes
FROM TRNCH
INNER JOIN BOND ON TRNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=219 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=219 and actflag='U')
and TRNCH.acttime > (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))


