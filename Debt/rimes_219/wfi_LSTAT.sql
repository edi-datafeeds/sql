--filepath=o:\upload\acc\219\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=EDI_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('LSTAT') as TableName,
LSTAT.Actflag,
LSTAT.AnnounceDate as Created,
LSTAT.Acttime as Changed,
LSTAT.LstatID,
SCMST.SecID,
SCMST.ISIN,
LSTAT.ExchgCD,
LSTAT.NotificationDate,
LSTAT.EffectiveDate,
LSTAT.LStatStatus,
LSTAT.EventType,
LSTAT.Reason
FROM LSTAT
INNER JOIN BOND ON LSTAT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=219 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=219 and actflag='U')
and LSTAT.acttime > (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
