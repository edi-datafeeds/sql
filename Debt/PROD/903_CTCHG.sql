--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_CTCHG
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n



--# 
use WCA
SELECT 
upper('CTCHG') as TableName,
CTCHG.Actflag,
CTCHG.AnnounceDate,
CTCHG.Acttime, 
CTCHG.CTChgID,
CTCHG.SecID,
CTCHG.EffectiveDate,
CTCHG.OldResultantRatio,
CTCHG.NewResultantRatio,
CTCHG.OldSecurityRatio,
CTCHG.NewSecurityRatio,
CTCHG.OldCurrency,
CTCHG.NewCurrency,
CTCHG.OldConversionPrice,
CTCHG.NewConversionPrice,
CTCHG.ResSectyCD as ResSectyCD,
CTCHG.OldResSecID,
CTCHG.NewResSecID,
CTCHG.EventType ,
CTCHG.RelEventID,
CTCHG.OldFromDate,
CTCHG.NewFromDate,
CTCHG.OldTodate,
CTCHG.NewToDate,
CTCHG.ConvtID,
CTCHG.OldFXRate,
CTCHG.NewFXRate,
CTCHG.Notes as Notes
FROM CTCHG
where 
acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
