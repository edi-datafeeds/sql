--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_BOND
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 
use WCA
SELECT 
upper('BOND') as TableName,
BOND.Actflag,
BOND.AnnounceDate,
BOND.Acttime,  
BOND.SecId, 
BOND.BondType, 
BOND.DebtMarket, 
BOND.CurenCD, 
BOND.ParValue, 
BOND.IssueDate, 
BOND.IssueCurrency, 
BOND.IssuePrice, 
BOND.IssueAmount, 
BOND.IssueAmountDate, 
BOND.OutstandingAmount, 
BOND.OutstandingAmountDate, 
BOND.InterestBasis, 
BOND.InterestRate, 
BOND.InterestAccrualConvention, 
BOND.InterestPaymentFrequency, 
BOND.IntCommencementDate, 
BOND.FirstCouponDate, 
BOND.InterestPayDate1, 
BOND.InterestPayDate2, 
BOND.InterestPayDate3, 
BOND.InterestPayDate4, 
BOND.DomesticTaxRate, 
BOND.NonResidentTaxRate, 
BOND.FRNType, 
BOND.FRNIndexBenchmark, 
BOND.Markup, 
BOND.MinimumInterestRate, 
BOND.MaximumInterestRate, 
BOND.Rounding, 
BOND.Series, 
BOND.Class, 
BOND.OnTap, 
BOND.MaximumTapAmount, 
BOND.TapExpiryDate, 
BOND.Guaranteed, 
BOND.SecuredBy, 
BOND.SecurityCharge, 
BOND.Subordinate, 
BOND.SeniorJunior, 
BOND.WarrantAttached, 
BOND.MaturityStructure, 
BOND.Perpetual, 
BOND.MaturityDate, 
BOND.MaturityExtendible, 
BOND.Callable, 
BOND.Puttable, 
BOND.Denomination1, 
BOND.Denomination2, 
BOND.Denomination3, 
BOND.Denomination4, 
BOND.Denomination5, 
BOND.Denomination6, 
BOND.Denomination7, 
BOND.MinimumDenomination, 
BOND.DenominationMultiple, 
BOND.Strip, 
BOND.StripInterestNumber,  
BOND.Bondsrc, 
BOND.MaturityBenchmark, 
' ' as IssuePriceInPercentage, 
BOND.ConventionMethod, 
BOND.FrnIntAdjFreq, 
BOND.IntBusDayConv, 
BOND.InterestCurrency, 
BOND.MatBusDayConv, 
BOND.MaturityCurrency,  
BOND.TaxRules, 
BOND.VarIntPayDate, 
BOND.GoverningLaw, 
BOND.Notes as Notes
FROM BOND
where 
BOND.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
