--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_CONV
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n


--# 
use WCA
SELECT 
upper('CONV') as TableName,
CONV.Actflag,
CONV.AnnounceDate,
CONV.Acttime,
CONV.ConvID,
CONV.SecID,
CONV.FromDate,
CONV.ToDate,
CONV.RatioNew,
CONV.RatioOld,
CONV.CurenCD,
CONV.Price,
CONV.MandOptFlag,
CONV.ResSecID,
CONV.ResSectyCD,
CONV.Fractions,
CONV.FXrate,
CONV.PartFinalFlag,
CONV.ConvType,
' ' as PriceInPercent,
CONV.RDID,
CONV.ConvNotes as Notes
FROM CONV
where 
acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
