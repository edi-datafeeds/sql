--filepath=o:\Datafeed\Debt\WFIfeed\Daily_All\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ALL
--fileheadertext=EDI_DEBT_ALL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\All\Daily_All\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 17
use wca
select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM SEDOL
INNER JOIN BOND ON SEDOL.SecID = BOND.SecID
where 
SEDOL.acttime >= '2019-11-14 00:00:00.000' AND SEDOL.acttime < '2019-11-14 17:30:00.000'

--# 18
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate,
SCMST.Acttime,
SCMST.SecID,
SCMST.IssID,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.StatusReason,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.ISIN,
SCMST.X as CommonCode
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
SCMST.acttime >= '2019-11-14 00:00:00.000' AND SCMST.acttime < '2019-11-14 17:30:00.000'

--# 19
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when SCEXH.ListStatus='N' or SCEXH.ListStatus='' then 'L' ELSE SCEXH.ListStatus end as ListStatus,
SCEXH.Lot,
scexh.MinTrdgQty,
SCEXH.ListDate,
'' as TradeStatus,
SCEXH.LocalCode
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
where 
SCEXH.acttime >= '2019-11-14 00:00:00.000' AND SCEXH.acttime < '2019-11-14 17:30:00.000'

--# 20
use WCA
SELECT 
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
ISSUR.acttime >= '2019-11-14 00:00:00.000' AND ISSUR.acttime < '2019-11-14 17:30:00.000'



--# 21
USE WCA
SELECT *
FROM v51f_920_Currency_Redenomination
WHERE Changed >= '2019-11-14 00:00:00.000' AND Changed < '2019-11-14 17:30:00.000'
ORDER BY EventID desc



--# 22
USE WCA
SELECT *  
FROM v54f_920_Interest_Payment
WHERE Changed >= '2019-11-14 00:00:00.000' AND Changed < '2019-11-14 17:30:00.000'
ORDER BY EventID desc
