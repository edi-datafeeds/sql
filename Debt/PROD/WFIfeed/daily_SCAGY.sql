--filepath=o:\Datafeed\Debt\WFIfeed\Agency\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_DEBT_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\Agency\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select distinct
upper('SCAGY') as TableName,
SCAGY.Actflag,
SCAGY.AnnounceDate,
SCAGY.Acttime,
SCAGY.ScagyID,
SCAGY.SecID,
SCAGY.AgncyID,
SCAGY.Relationship,
SCAGY.GuaranteeType,
SCAGY.SpStartDate,
SCAGY.SpEndDate
FROM SCAGY
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCAGY.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
