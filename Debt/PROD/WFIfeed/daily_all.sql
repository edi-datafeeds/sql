--filepath=o:\Datafeed\Debt\WFIfeed\Daily_All\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ALL
--fileheadertext=EDI_DEBT_ALL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\All\Daily_All\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT *
FROM v51f_920_FRN_Fixings
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc


--# 2
USE WCA
SELECT *
FROM v51f_920_Sedol_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc


--# 3
USE WCA
SELECT *
FROM v50f_920_Issuer_Name_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc


--# 4
USE WCA
SELECT *
FROM v51f_920_Reconvention
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 5
USE WCA
SELECT *
FROM v51f_920_International_Code_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 6
USE WCA
SELECT *  
FROM v51f_920_Interest_Frequency_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 7
USE WCA
SELECT *
FROM v50f_920_Incorporation_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 8
USE WCA
SELECT *
FROM v51f_920_Interest_Basis_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 9
USE WCA
SELECT *  
FROM v51f_920_Interest_Rate_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 10
USE WCA
SELECT *
FROM v51f_920_Redenomination
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 11
USE WCA
SELECT *  
FROM v51f_920_Bond_Static_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 12
USE WCA
SELECT *
FROM v51f_920_Security_Description_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 13
USE WCA
SELECT *  
FROM v53f_920_Redemption
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 14
USE WCA
SELECT *  
FROM v51f_920_Maturity_Change
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 15
USE WCA
SELECT *  
FROM v51f_920_Schedule
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc

--# 16
use WCA
SELECT 
upper('BONDFULL') as TableName,
BOND.SecId, 
BOND.Actflag,
BOND.AnnounceDate,
BOND.Acttime,  
BOND.BondType, 
BOND.DebtMarket, 
BOND.CurenCD as DebtCurrency, 
BOND.ParValue as NominalValue, 
BOND.IssueDate, 
BOND.IssueCurrency, 
BOND.IssuePrice, 
BOND.IssueAmount, 
BOND.IssueAmountDate, 
BOND.OutstandingAmount, 
BOND.OutstandingAmountDate, 
BOND.InterestBasis as IntBasis, 
BOND.InterestRate as IntRate, 
BOND.InterestAccrualConvention as IntAccrualConvention, 
BOND.InterestPaymentFrequency as IntPaymentFrequency, 
BOND.IntCommencementDate, 
BOND.FirstCouponDate, 
BOND.InterestPayDate1 as IntPayDate1, 
BOND.InterestPayDate2 as IntPayDate2, 
BOND.InterestPayDate3 as IntPayDate3, 
BOND.InterestPayDate4 as IntPayDate4, 
bondx.DomesticTaxRate, 
bondx.NonResidentTaxRate, 
BOND.FRNType, 
BOND.FRNIndexBenchmark, 
BOND.Markup as FrnMargin, 
BOND.MinimumInterestRate as FrnMinIntRate, 
BOND.MaximumInterestRate as FrnMaxIntRate, 
BOND.Rounding as FrnRounding, 
bondx.Series, 
bondx.Class, 
bondx.OnTap, 
bondx.MaximumTapAmount, 
bondx.TapExpiryDate, 
BOND.Guaranteed, 
BOND.SecuredBy, 
BOND.SecurityCharge, 
BOND.Subordinate, 
BOND.SeniorJunior, 
BOND.WarrantAttached, 
BOND.MaturityStructure, 
BOND.Perpetual, 
BOND.MaturityDate, 
BOND.MaturityExtendible, 
BOND.Callable, 
BOND.Puttable, 
bondx.Denomination1, 
bondx.Denomination2, 
bondx.Denomination3, 
bondx.Denomination4, 
bondx.Denomination5, 
bondx.Denomination6, 
bondx.Denomination7, 
bondx.MinimumDenomination, 
bondx.DenominationMultiple, 
BOND.Strip, 
BOND.StripInterestNumber,  
BOND.Bondsrc, 
BOND.MaturityBenchmark, 
BOND.ConventionMethod, 
BOND.FrnIntAdjFreq, 
BOND.IntBusDayConv, 
BOND.InterestCurrency as IntCurrency, 
BOND.MatBusDayConv, 
BOND.MaturityCurrency,  
bondx.TaxRules, 
BOND.VarIntPayDate, 
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
BOND.MatPrice, 
BOND.MatPriceAsPercent, 
BOND.SinkingFund, 
bondx.GoverningLaw, 
BOND.Notes as Notes
FROM BOND
left outer join bondx on bond.secid = bondx.secid
where 
BOND.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)



--# 17
use wca
select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM SEDOL
INNER JOIN BOND ON SEDOL.SecID = BOND.SecID
where 
SEDOL.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 18
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate,
SCMST.Acttime,
SCMST.SecID,
SCMST.IssID,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.StatusReason,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.ISIN,
SCMST.X as CommonCode
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
SCMST.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 19
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when SCEXH.ListStatus='N' or SCEXH.ListStatus='' then 'L' ELSE SCEXH.ListStatus end as ListStatus,
SCEXH.Lot,
scexh.MinTrdgQty,
SCEXH.ListDate,
'' as TradeStatus,
SCEXH.LocalCode
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
where 
SCEXH.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 20
use WCA
SELECT 
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
ISSUR.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)



--# 21
USE WCA
SELECT *
FROM v51f_920_Currency_Redenomination
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc



--# 22
USE WCA
SELECT *  
FROM v54f_920_Interest_Payment
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc
