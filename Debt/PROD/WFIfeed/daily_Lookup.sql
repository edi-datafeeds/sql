--filepath=o:\Datafeed\Debt\WFIfeed\Lookup\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_LOOKUP
--fileheadertext=EDI_DEBT_Lookup_AL
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\Lookup\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
FROM LOOKUPEXTRA

union

SELECT
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD

union

SELECT
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.Exchgname as Lookup
from EXCHG

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('MICCODE') as TypeGroup,
Exchg.MIC as Code,
Exchg.Exchgname as Lookup
from EXCHG
where MIC <> '' and MIC is not null

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('MICMAP') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.MIC as Lookup
from EXCHG

union

SELECT
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
cast(Indus.IndusID as varchar(10)) as Code,
Indus.IndusName as Lookup
from INDUS

union

SELECT
Cntry.Actflag,
Cntry.Acttime,
upper('CNTRY') as TypeGroup,
Cntry.CntryCD as Code,
Cntry.Country as Lookup
from CNTRY

union

SELECT
Curen.Actflag,
Curen.Acttime,
upper('CUREN') as TypeGroup,
Curen.CurenCD as Code,
Curen.Currency as Lookup
from CUREN

union

SELECT
Event.Actflag,
Event.Acttime,
upper('EVENT') as TypeGroup,
Event.EventType as Code,
Event.EventName as Lookup
from EVENT

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2

order by TypeGroup, Code
