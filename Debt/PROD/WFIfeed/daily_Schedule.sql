--filepath=o:\Datafeed\Debt\WFIfeed\Schedule\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_DEBT_Schedule_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\Schedule\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N
--ZEROROWCHK=N

--# 1
USE WCA
SELECT *  
FROM v51f_920_Schedule
WHERE Changed >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc
