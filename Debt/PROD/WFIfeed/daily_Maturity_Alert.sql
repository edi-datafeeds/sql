--filepath=o:\Datafeed\Debt\WFIfeed\Maturity\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_MTALRT
--fileheadertext=EDI_DEBT_Maturity_Alert_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
USE WCA
SELECT
'MTALRT' as sEvent,
v20c_920_BOND.AnnounceDate,
getdate() as Changed,
'U' as Actflag,
v20c_920_BOND.SecID as EventID,
v20c_920_BOND.IssID,
v20c_920_BOND.SecID,
v20c_920_BOND.CntryofIncorp,
v20c_920_BOND.IssuerName,
v20c_920_BOND.SecurityDesc,
v20c_920_BOND.ISIN,
v20c_920_BOND.USCode,
v20c_920_BOND.SecurityStatus,
v20c_920_BOND.PrimaryExchgCD,
v20c_920_BOND.Sedol,
v20c_920_BOND.ExCountry,
v20c_920_BOND.RegCountry,
v20c_920_BOND.DebtType,
v20c_920_BOND.MaturityDate,
v20c_920_BOND.MaturityExtendible,
v20c_920_BOND.IssueCurrency,
v20c_920_BOND.IssueDate,
v20c_920_BOND.NominalValue,
v20c_920_BOND.DebtCurrency,
v20c_920_BOND.MaturityStructure,
v20c_920_BOND.MatPriceAsPercent,
v20c_920_BOND.MatPrice,
v20c_920_BOND.MaturityCurrency,
v20c_920_BOND.MatBusDayConv,
v20c_920_BOND.MaturityBenchmark
FROM v20c_920_BOND
WHERE MaturityDate is not null
and MaturityDate >= getdate()+22
and MaturityDate < getdate()+27
ORDER BY MaturityDate desc
