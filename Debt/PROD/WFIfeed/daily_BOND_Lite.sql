--filepath=o:\Datafeed\Debt\WFIfeed\Refdata3\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_BONDLITE
--fileheadertext=EDI_DEBT_BONDLITE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\Refdata3\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BONDLITE') as TableName,
BOND.SecId, 
BOND.Actflag,
BOND.AnnounceDate as Created,
BOND.Acttime as Changed,  
BOND.BondType, 
BOND.DebtMarket, 
BOND.CurenCD as DebtCurrency, 
BOND.ParValue as NominalValue, 
BOND.IssueDate, 
BOND.IssueCurrency, 
BOND.IssuePrice, 
BOND.IssueAmount, 
BOND.IssueAmountDate, 
BOND.InterestBasis as IntBasis,
BOND.InterestRate, 
BOND.InterestAccrualConvention IntAccrualConvention, 
BOND.InterestPaymentFrequency as IntPaymentFrequency, 
BOND.IntCommencementDate, 
BOND.FirstCouponDate, 
BOND.FRNType, 
BOND.MinimumInterestRate as FrnMinIntRate, 
BOND.MaximumInterestRate as FrnMaxIntRate, 
BOND.Series, 
BOND.Class, 
BOND.Guaranteed, 
BOND.SecuredBy, 
BOND.SecurityCharge, 
BOND.Subordinate, 
BOND.SeniorJunior, 
BOND.MaturityStructure, 
BOND.Perpetual, 
BOND.MaturityDate, 
BOND.MaturityExtendible, 
BOND.Bondsrc, 
BOND.InterestCurrency, 
BOND.MaturityCurrency,  
BOND.PriceAsPercent
FROM BOND
where 
BOND.acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
