--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_CPOPT
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate,
CPOPT.Acttime,
CPOPT.CpoptID,
CPOPT.CallPut, 
CPOPT.SecID,
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.PriceAsPercent,
CPOPT.CPType,
CPOPT.Notes as Notes 
FROM CPOPT
where 
acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)