--filepath=o:\Upload\Acc\185\feed\
--filenameprefix=STANDING_FULL_
--filename=
--filenamesql=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),120) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=
--fileheadertext=EDI_Sutherlands_185_STANDING_FULL_yyyy-mm-dd
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\185\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select distinct
bond.secid,
portfolio.dbo.fisin.code as ISIN,
agncy.RegistrarName as Trustee,
agncy.actflag,
issur.Issuername as Issuer_Name,
scmst.SecurityDesc as Description,
bond.OutstandingAmount as Amount_Outstanding,
bond.InterestBasis as Coupon_Type,
bond.InterestRate as Coupon_Amount,
bond.curencd as Currency,
case when redem.redemtype = 'AMT' then 'Y'
     when BOND.sinkingfund<>'' then BOND.sinkingfund
     when bond.bondsrc='OC' or bond.bondsrc='PE' or bond.bondsrc='PR'
          or bond.bondsrc='PS' or bond.bondsrc='TS' then 'N'
     else '' end as Sinkable,
case when bond.Callable<>'' and bond.Callable is not null then bond.Callable 
     when bond.bondsrc='OC' or bond.bondsrc='PE' or bond.bondsrc='PR'
          or bond.bondsrc='PS' or bond.bondsrc='TS' then 'N'
     else '' end as Callable,
case when bond.Puttable<>'' and bond.Puttable is not null then bond.Puttable 
     when bond.bondsrc='OC' or bond.bondsrc='PE' or bond.bondsrc='PR'
          or bond.bondsrc='PS' or bond.bondsrc='TS' then 'N'
     else '' end as Puttable,
case when bond.subordinate<>'' and bond.subordinate is not null then bond.subordinate 
     when bond.bondsrc='OC' or bond.bondsrc='PE' or bond.bondsrc='PR'
          or bond.bondsrc='PS' or bond.bondsrc='TS' then 'N'
     else '' end as Subordinated,
case when bond.Perpetual<>'' and bond.Perpetual is not null then bond.Perpetual 
     when bond.MaturityDate is not null then 'N'
     when bond.bondsrc='OC' or bond.bondsrc='PE' or bond.bondsrc='PR'
          or bond.bondsrc='PS' or bond.bondsrc='TS' then 'N'
     else '' end as Perpetual,
case when bond.MaturityExtendible<>'' and bond.MaturityExtendible is not null then bond.MaturityExtendible 
     when bond.bondsrc='OC' or bond.bondsrc='PE' or bond.bondsrc='PR'
          or bond.bondsrc='PS' or bond.bondsrc='TS' then 'N'
     else '' end as Extendible,
bond.InterestPaymentFrequency as Coupon_Frequency,
bond.IssueAmount as Issuing_Amount,
issur.cntryofincorp as Issuer_Country,
bond.InterestAccrualConvention as Day_Count_Convention,
bond.MaturityBenchmark as Index_Linked,
'' as Calculation_Type,
bond.MaturityDate as Maturity_Date,
bond.IssueDate as Issue_Date,
bond.AnnounceDate as Announcement_Date,
bond.SeniorJunior as Debt_Type_1,
bond.Subordinate as Debt_Type_2,
issur.IssID as Asset_Type_Link

from portfolio.dbo.fisin
left outer join scmst on portfolio.dbo.fisin.code = Isin
left outer join bond on scmst.secid = bond.secid
left outer join issur on scmst.issid = issur.issid
left outer join redem on bond.secid = redem.secid and 'AMT'= redem.redemtype
left outer join scagy on bond.secid = scagy.secid and 'TST'= scagy.relationship
left outer join agncy on scagy.agncyid = agncy.agncyid
where 
portfolio.dbo.fisin.accid  = 185
and portfolio.dbo.fisin.Actflag<>'D'
and RegistrarName is not null
and (scagy.actflag<>'D' or scagy.actflag is null)
and bond.secid = 169529
order by portfolio.dbo.fisin.code

--select * from scagy
--where secid = 169529

