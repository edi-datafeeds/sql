--filenameprefix=SINKABLE_FULL_
--filename=
--filenamesql=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),120) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=
--fileheadertext=EDI_Sutherlands_185_SINKABLE_FULL_yyyy-mm-dd
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\185\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select
portfolio.dbo.fisin.code as ISIN,
case when sfund.sfundid is not null then 'SFUND'
     else 'AMT'
     end as Sinkable_Type,
case when sfund.sfundid is not null then sfund.sfunddate
     else redem.redemdate
     end as Effective_Date,
case when sfund.sfundid is not null then sfund.curencd
     else redem.curencd
     end as Currency,
case when sfund.sfundid is not null then sfund.Amount
     else redem.redemprice
     end as Amount,
case when sfund.sfundid is not null then sfund.Amountaspercent
     else redem.priceaspercent
     end as Amount_as_Percent,
case when sfund.sfundid is not null then null
     else redem.poolfactor
     end as Pool_Factor
from portfolio.dbo.fisin
left outer join scmst on portfolio.dbo.fisin.code = Isin
left outer join redem on scmst.secid = redem.secid and 'AMT'= redem.redemtype
left outer join sfund on scmst.secid = sfund.secid
where 
portfolio.dbo.fisin.accid  = 185
and portfolio.dbo.fisin.Actflag<>'D'
and scmst.secid is not null
and (redem.redemid is not null or sfund.sfundid is not null)
order by isin
