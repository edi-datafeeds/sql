--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_DEBT_FRN_Fixings_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
USE WCA
SELECT *
FROM v51f_920_FRN_Fixings
inner join portfolio.dbo.Markit_issid on v51f_920_FRN_Fixings.issid = portfolio.dbo.Markit_issid.issid
where actflag <> 'D'