--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BONDFULL
--fileheadertext=EDI_DEBT_BONDFULL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BONDFULL') as TableName,
BOND.SecId, 
BOND.Actflag,
BOND.AnnounceDate,
BOND.Acttime,  
BOND.BondType, 
BOND.DebtMarket, 
BOND.CurenCD as DebtCurrency, 
BOND.ParValue as NominalValue, 
BOND.IssueDate, 
BOND.IssueCurrency, 
BOND.IssuePrice, 
BOND.IssueAmount, 
BOND.IssueAmountDate, 
BOND.OutstandingAmount, 
BOND.OutstandingAmountDate, 
BOND.InterestBasis as IntBasis, 
BOND.InterestRate as IntRate, 
BOND.InterestAccrualConvention as IntAccrualConvention, 
BOND.InterestPaymentFrequency as IntPaymentFrequency, 
BOND.IntCommencementDate, 
BOND.FirstCouponDate, 
BOND.InterestPayDate1 as IntPayDate1, 
BOND.InterestPayDate2 as IntPayDate2, 
BOND.InterestPayDate3 as IntPayDate3, 
BOND.InterestPayDate4 as IntPayDate4, 
bondx.DomesticTaxRate, 
bondx.NonResidentTaxRate, 
BOND.FRNType, 
BOND.FRNIndexBenchmark, 
BOND.Markup as FrnMargin, 
BOND.MinimumInterestRate as FrnMinIntRate, 
BOND.MaximumInterestRate as FrnMaxIntRate, 
BOND.Rounding as FrnRounding, 
bondx.Series, 
bondx.Class, 
bondx.OnTap, 
bondx.MaximumTapAmount, 
bondx.TapExpiryDate, 
BOND.Guaranteed, 
BOND.SecuredBy, 
BOND.SecurityCharge, 
BOND.Subordinate, 
BOND.SeniorJunior, 
BOND.WarrantAttached, 
BOND.MaturityStructure, 
BOND.Perpetual, 
BOND.MaturityDate, 
BOND.MaturityExtendible, 
BOND.Callable, 
BOND.Puttable, 
bondx.Denomination1, 
bondx.Denomination2, 
bondx.Denomination3, 
bondx.Denomination4, 
bondx.Denomination5, 
bondx.Denomination6, 
bondx.Denomination7, 
bondx.MinimumDenomination, 
bondx.DenominationMultiple, 
BOND.Strip, 
BOND.StripInterestNumber,  
BOND.Bondsrc, 
BOND.MaturityBenchmark, 
BOND.ConventionMethod, 
BOND.FrnIntAdjFreq, 
BOND.IntBusDayConv, 
BOND.InterestCurrency as IntCurrency, 
BOND.MatBusDayConv, 
BOND.MaturityCurrency,  
bondx.TaxRules, 
BOND.VarIntPayDate, 
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
BOND.MatPrice, 
BOND.MatPriceAsPercent, 
BOND.SinkingFund, 
bondx.GoverningLaw, 
BOND.Notes as Notes
FROM BOND
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
left outer join bondx on bond.secid = bondx.secid
where 
scmst.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and bond.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
