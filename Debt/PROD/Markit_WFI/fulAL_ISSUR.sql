--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_DEBT_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
scmst.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and scmst.actflag<>'D'
and bond.actflag<>'D'
and (scmst.statusflag<>'I' or scmst.statusflag is null)
and BOND.MaturityDate>getdate()-31
