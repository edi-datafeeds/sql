--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_DEBT_Incorporation_Change_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
USE WCA
SELECT *
FROM v50f_920_Incorporation_Change
inner join portfolio.dbo.Markit_issid on v50f_920_Incorporation_Change.issid = portfolio.dbo.Markit_issid.issid
where actflag <> 'D'