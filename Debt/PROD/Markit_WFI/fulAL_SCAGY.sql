--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_DEBT_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select distinct
upper('SCAGY') as TableName,
SCAGY.Actflag,
SCAGY.AnnounceDate,
SCAGY.Acttime,
SCAGY.ScagyID,
SCAGY.SecID,
SCAGY.AgncyID,
SCAGY.Relationship,
SCAGY.GuaranteeType,
SCAGY.SpStartDate,
SCAGY.SpEndDate
FROM SCAGY
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and SCAGY.actflag<>'D'
and scmst.actflag<>'D'
and bond.actflag<>'D'
and (scmst.statusflag<>'I' or scmst.statusflag is null)
and BOND.MaturityDate>getdate()-31
