--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_DEBT_Interest_Payment_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
USE WCA
SELECT *  
FROM v54f_920_Interest_Payment
inner join portfolio.dbo.Markit_issid on v54f_920_Interest_Payment.issid = portfolio.dbo.Markit_issid.issid
where
changed > (select max(feeddate) from tbl_Opslog where seq = 3)