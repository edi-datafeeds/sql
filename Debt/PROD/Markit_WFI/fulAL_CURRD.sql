--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_DEBT_Currency_Redenomination_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
USE WCA
SELECT *
FROM v51f_920_Currency_Redenomination
inner join portfolio.dbo.Markit_issid on v51f_920_Currency_Redenomination.issid = portfolio.dbo.Markit_issid.issid
where actflag <> 'D'