--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_REDMT
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n



--# 
use WCA
SELECT 
upper('REDMT') as TableName,
REDMT.Actflag,
REDMT.AnnounceDate,
REDMT.Acttime,
REDMT.RedmtID,
REDMT.SecID,
REDMT.RedemptionDate,
REDMT.CurenCD,
REDMT.RedemptionPrice,
REDMT.MandOptFlag,
REDMT.PartFinal,
REDMT.RedemptionType,
REDMT.RedemptionAmount as AmountRedeemed,
REDMT.RedemptionPremium,
REDMT.RedemInPercent,
' ' as RedemInPercent,
REDMT.RedmtNotes as Notes
FROM REDMT
where 
acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)

