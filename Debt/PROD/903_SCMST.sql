--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_SCMST
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate,
SCMST.Acttime,
SCMST.SecID,
SCMST.ParentSecID,
SCMST.IssID,
SCMST.SectyCD,
SCMST.SecurityDesc,
SCMST.Statusflag,
SCMST.StatusReason,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.IssuePrice,
SCMST.PaidUpValue,
SCMST.Voting,
SCMST.FYENPPDate,
SCMST.USCode,
SCMST.ISIN,
SCMST.VALOREN,
SCMST.X as CommonCode,
SCMST.CFI,
SCMST.SharesOutstanding,
SCMST.UmprgID,
SCMST.Holding,
SCMST.NoParValue,
SCMST.REGS144A, 
SCMST.ScmstNotes as Notes
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
SCMST.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
