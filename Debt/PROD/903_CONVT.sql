--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_CONVT
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n



--# 
use WCA
SELECT 
upper('CONVT') as TableName,
CONVT.Actflag,
CONVT.AnnounceDate,
CONVT.Acttime,
CONVT.ConvtID,
CONVT.SecID,
CONVT.FromDate,
CONVT.ToDate,
CONVT.RatioNew,
CONVT.RatioOld,
CONVT.CurenCD,
CONVT.Price,
CONVT.MandOptFlag,
CONVT.ResSecID,
CONVT.SectyCD as ResSectyCD,
CONVT.Fractions,
CONVT.FXrate,
CONVT.PartFinalFlag,
' ' as PriceInPercent,
CONVT.ConvtNotes as Notes
FROM CONVT
where 
acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
