--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_RTCHG
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_RTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n



--# 
use wca
SELECT 
upper('RTCHG') as TableName,
RTCHG.Actflag,
RTCHG.AnnounceDate,
RTCHG.Acttime,
RTCHG.RtchgID,
RTCHG.SecID,
RTCHG.OldRedemDate as OldRedemptionDate,
RTCHG.OldCurenCD,
RTCHG.OldRedemPrice,
RTCHG.OldRedemPremium as OldRedemptionPremium,
RTCHG.OldRedemPercFlag,
RTCHG.OldAmountRedeemed,
RTCHG.OldRedemPercflag as OldRedemInPercent,
RTCHG.NewRedemDate as NewRedemptionDate,
RTCHG.NewCurenCD,
RTCHG.NewRedemPrice,
RTCHG.NewRedemPremium as NewRedemptionPremium,
RTCHG.NewRedemPercFlag,
RTCHG.NewAmountRedeemed,
RTCHG.NewRedemPercflag as NewRedemInPercent,
RTCHG.EventType,
RTCHG.EventID as RelEventID,
RTCHG.Notes
FROM RTCHG
where 
acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)