--filepath=o:\Datafeed\Debt\903\
--filenameprefix=
--filename=yyyymmdd_REDEM
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_REDEM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\903\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n



--# 
use WCA
SELECT 
upper('REDEM') as TableName,
REDEM.Actflag,
REDEM.AnnounceDate,
REDEM.Acttime,
REDEM.RedemID,
REDEM.SecID,
REDEM.RedemDate as RedemptionDate,
REDEM.CurenCD,
REDEM.RedemPrice as RedemptionPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.AmountRedeemed,
REDEM.RedemPremium as RedemptionPremium,
' ' as  RedemInPercent,
REDEM.RedemType,
' ' AS RedemDefault,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
REDEM.Rdid,
REDEM.PriceAsPercent,
REDEM.RedemNotes as Notes 
FROM REDEM
where 
acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
