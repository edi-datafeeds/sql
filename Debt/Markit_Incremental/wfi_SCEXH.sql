--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCEXH') as Tablename,
wca.scexh.ActFlag,
wca.scexh.AnnounceDate as Created,
wca.scexh.Acttime as Changed,
wca.scexh.ScexhId,
wca.scexh.SecId,
wca.scmst.Isin,
wca.scexh.Exchgcd,
wca.scexh.ListStatus,
wca.scexh.Lot,
wca.scexh.MinTrdgQty,
wca.scexh.ListDate,
'' as TradeStatus,
wca.scexh.LocaLcode,
SUBSTRING(wca.scexh.JunkLocalCode, 1, 50) as JunkLocalCode,
wca.scexh.ScexhNotes as Notes
from wca.scexh
inner join wca.bond on wca.scexh.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scexh.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)