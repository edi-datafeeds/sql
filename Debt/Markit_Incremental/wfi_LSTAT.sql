--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=EDI_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('LSTAT') as Tablename,
wca.lstat.ActFlag,
wca.lstat.AnnounceDate as Created,
wca.lstat.Acttime as Changed,
wca.lstat.LstatId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.lstat.Exchgcd,
wca.lstat.NotificationDate,
wca.lstat.EffectiveDate,
wca.lstat.LstatStatus,
wca.lstat.EventType,
wca.lstat.Reason
from wca.lstat
inner join wca.bond on wca.lstat.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.lstat.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)