--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=EDI_AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select distinct
upper('AGYDT') as TableName,
AGYDT.Actflag,
AGYDT.AnnounceDate as Created,
AGYDT.Acttime as Changed,
AGYDT.AgydtID,
AGYDT.AgncyID,
AGYDT.EffectiveDate,
AGYDT.OldRegistrarName,
AGYDT.OldAdd1,
AGYDT.OldAdd2,
AGYDT.OldAdd3,
AGYDT.OldAdd4,
AGYDT.OldAdd5,
AGYDT.OldAdd6,
AGYDT.OldCity,
AGYDT.OldCntryCD,
AGYDT.OldWebSite,
AGYDT.OldContact1,
AGYDT.OldTel1,
AGYDT.OldFax1,
AGYDT.Oldemail1,
AGYDT.OldContact2,
AGYDT.OldTel2,
AGYDT.OldFax2,
AGYDT.Oldemail2,
AGYDT.OldState,
AGYDT.NewRegistrarName,
AGYDT.NewAdd1,
AGYDT.NewAdd2,
AGYDT.NewAdd3,
AGYDT.NewAdd4,
AGYDT.NewAdd5,
AGYDT.NewAdd6,
AGYDT.NewCity,
AGYDT.NewCntryCD,
AGYDT.NewWebSite,
AGYDT.NewContact1,
AGYDT.NewTel1,
AGYDT.NewFax1,
AGYDT.Newemail1,
AGYDT.NewContact2,
AGYDT.NewTel2,
AGYDT.NewFax2,
AGYDT.Newemail2,
AGYDT.NewState
FROM AGYDT
INNER JOIN SCAGY ON AGYDT.AgncyID = SCAGY.AgncyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and AGYDT.acttime > (select max(acttime)-0.1 from tbl_Opslog))
