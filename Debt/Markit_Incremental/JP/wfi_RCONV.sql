--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_RCONV
--fileheadertext=EDI_RCONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('RCONV') as TableName,
RCONV.Actflag,
RCONV.AnnounceDate as Created,
RCONV.Acttime as Changed,
RCONV.RconvID,
RCONV.SecID,
SCMST.ISIN,
RCONV.EffectiveDate,
RCONV.OldInterestAccrualConvention,
RCONV.NewInterestAccrualConvention,
RCONV.OldConvMethod,
RCONV.NewConvMethod,
RCONV.Eventtype,
RCONV.Notes
FROM RCONV
INNER JOIN BOND ON RCONV.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and RCONV.acttime > (select max(acttime)-0.1 from tbl_Opslog))