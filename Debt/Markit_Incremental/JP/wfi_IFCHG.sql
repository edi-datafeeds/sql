--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('IFCHG') as TableName,
IFCHG.Actflag,
IFCHG.AnnounceDate as Created,
IFCHG.Acttime as Changed,
IFCHG.IfchgID,
IFCHG.SecID,
SCMST.ISIN,
IFCHG.NotificationDate,
IFCHG.OldIntPayFrqncy,
IFCHG.OldIntPayDate1,
IFCHG.OldIntPayDate2,
IFCHG.OldIntPayDate3,
IFCHG.OldIntPayDate4,
IFCHG.NewIntPayFrqncy,
IFCHG.NewIntPayDate1,
IFCHG.NewIntPayDate2,
IFCHG.NewIntPayDate3,
IFCHG.NewIntPayDate4,
IFCHG.Eventtype
FROM IFCHG
INNER JOIN BOND ON IFCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and IFCHG.acttime > (select max(acttime)-0.1 from tbl_Opslog))