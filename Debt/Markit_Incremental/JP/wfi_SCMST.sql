--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate as Created,
SCMST.Acttime as Changed,
SCMST.SecID,
SCMST.ISIN,
SCMST.ParentSecID,
SCMST.IssID,
SCMST.SectyCD,
SCMST.SecurityDesc,
'StatusFlag' = case when Statusflag = '' then 'A' else Statusflag end,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.X as CommonCode,
SCMST.Holding,
SCMST.StructCD,
SCMST.WKN,
SCMST.RegS144A,
SCMST.scmstNotes As Notes
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or  
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and (bond.acttime > (select max(acttime)-0.1 from tbl_Opslog)
  or scmst.acttime > (select max(acttime)-0.1 from tbl_Opslog)))