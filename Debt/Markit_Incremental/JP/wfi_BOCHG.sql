--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BOCHG
--fileheadertext=EDI_BOCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BOCHG') as TableName,
BOCHG.Actflag,
BOCHG.AnnounceDate as Created,
BOCHG.Acttime as Changed,  
BOCHG.BochgID,
BOCHG.RelEventID,
BOCHG.SecID,
SCMST.ISIN,
BOCHG.EffectiveDate,
BOCHG.OldOutValue,
BOCHG.NewOutValue,
BOCHG.EventType,
BOCHG.OldOutDate,
BOCHG.NewOutDate,
BOCHG.BochgNotes as Notes
FROM BOCHG
INNER JOIN BOND ON BOCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and BOCHG.acttime > (select max(acttime)-0.1 from tbl_Opslog))
