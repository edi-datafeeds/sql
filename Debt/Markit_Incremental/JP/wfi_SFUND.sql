--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('SFUND') as TableName,
SFUND.Actflag,
SFUND.AnnounceDate as Created,
SFUND.Acttime as Changed,
SFUND.SfundID,
SFUND.SecID,
SCMST.ISIN,
SFUND.SfundDate,
SFUND.CurenCD as Sinking_Currency,
SFUND.Amount,
SFUND.AmountasPercent,
SFUND.SfundNotes as Notes
FROM SFUND
INNER JOIN BOND ON SFUND.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and SFUND.acttime > (select max(acttime)-0.1 from tbl_Opslog))