--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_WKNCH
--fileheadertext=EDI_WKNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('WKNCH') as TableName,
WKNCH.Actflag,
WKNCH.AnnounceDate as Created,
WKNCH.Acttime as Changed,
WKNCH.SecID,
WKNCH.EffectiveDate,
WKNCH.WknChID,
WKNCH.Eventtype,
WKNCH.RelEventID,
WKNCH.OldWKN,
WKNCH.NewWKN
FROM WKNCH
INNER JOIN BOND ON WKNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and WKNCH.acttime > (select max(acttime)-0.1 from tbl_Opslog))