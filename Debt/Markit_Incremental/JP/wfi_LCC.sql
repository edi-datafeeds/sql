--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LCC
--fileheadertext=EDI_LCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('LCC') as TableName,
LCC.Actflag,
LCC.AnnounceDate as Created,
LCC.Acttime as Changed,
LCC.LccID,
LCC.SecID,
SCMST.ISIN,
LCC.ExchgCD,
LCC.EffectiveDate,
LCC.OldLocalCode,
LCC.NewLocalCode,
LCC.EventType,
LCC.RelEventID
FROM LCC
INNER JOIN BOND ON LCC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and LCC.acttime > (select max(acttime)-0.1 from tbl_Opslog))