--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LSTAT
--fileheadertext=EDI_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('LSTAT') as TableName,
LSTAT.Actflag,
LSTAT.AnnounceDate as Created,
LSTAT.Acttime as Changed,
LSTAT.LstatID,
SCMST.SecID,
SCMST.ISIN,
LSTAT.ExchgCD,
LSTAT.NotificationDate,
LSTAT.EffectiveDate,
LSTAT.LStatStatus,
LSTAT.EventType,
LSTAT.Reason
FROM LSTAT
INNER JOIN BOND ON LSTAT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and LSTAT.acttime > (select max(acttime)-0.1 from tbl_Opslog))