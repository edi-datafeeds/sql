--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CONVT') as TableName,
CONVT.Actflag,
CONVT.AnnounceDate as Created,
CONVT.Acttime as Changed,
CONVT.ConvtID,
SCMST.SecID,
SCMST.ISIN,
CONVT.FromDate,
CONVT.ToDate,
CONVT.RatioNew,
CONVT.RatioOld,
CONVT.CurenCD,
CONVT.CurPair,
CONVT.Price,
CONVT.MandOptFlag,
CONVT.ResSecID,
CONVT.SectyCD as ResSectyCD,
RESSCMST.ISIN as ResISIN,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
CONVT.Fractions,
CONVT.FXrate,
CONVT.PartFinalFlag,
CONVT.PriceAsPercent,
CONVT.ConvtNotes as Notes
FROM CONVT
INNER JOIN BOND ON convt.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as RESSCMST ON CONVT.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where 
SCMST.isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(SCMST.isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and CONVT.acttime > (select max(acttime)-0.1 from tbl_Opslog))