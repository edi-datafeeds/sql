--filepath=O:\datafeed\debt\Markit_JP_New\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_JP_New\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('RDNOM') as TableName,
RDNOM.Actflag,
RDNOM.AnnounceDate as Created,
RDNOM.Acttime as Changed, 
RDNOM.RdnomID,
RDNOM.SecID,
SCMST.ISIN,
RDNOM.EffectiveDate,
RDNOM.OldDenomination1,
RDNOM.OldDenomination2,
RDNOM.OldDenomination3,
RDNOM.OldDenomination4,
RDNOM.OldDenomination5,
RDNOM.OldDenomination6,
RDNOM.OldDenomination7,
RDNOM.OldMinimumDenomination,
RDNOM.OldDenominationMultiple,
RDNOM.NewDenomination1,
RDNOM.NewDenomination2,
RDNOM.NewDenomination3,
RDNOM.NewDenomination4,
RDNOM.NewDenomination5,
RDNOM.NewDenomination6,
RDNOM.NewDenomination7,
RDNOM.NewMinimumDenomination,
RDNOM.NewDenominationMultiple,
RDNOM.Notes
FROM RDNOM
INNER JOIN BOND ON RDNOM.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
isin in (select code from client.dbo.pfisin where accid=211 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=211 and actflag='U')
and RDNOM.acttime > (select max(acttime)-0.1 from tbl_Opslog))