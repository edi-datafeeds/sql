--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SDCHG') as Tablename,
wca.sdchg.ActFlag,
wca.sdchg.AnnounceDate as Created, 
wca.sdchg.Acttime as Changed,
wca.sdchg.SdchgId,
wca.sdchg.SecId,
wca.scmst.Isin,
wca.sdchg.Cntrycd as OldCntrycd,
wca.sdchg.EffectiveDate,
wca.sdchg.OldSedol,
wca.sdchg.NewSedol,
wca.sdchg.EventType,
wca.sdchg.RCntrycd as OldrCntrycd,
wca.sdchg.ReleventId,
wca.sdchg.NewCntrycd,
wca.sdchg.NewrCntrycd,
wca.sdchg.SdchgNotes as Notes
from wca.sdchg
inner join wca.bond on wca.sdchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.sdchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)