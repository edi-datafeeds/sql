--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
upper('BBCC') as Tablename,
wca.bbcc.ActFlag,
wca.bbcc.AnnounceDate,
wca.bbcc.Acttime,
wca.bbcc.BbccId,
wca.bbcc.SecId,
wca.bbcc.BbcId,
wca.bbcc.OldCntrycd,
wca.bbcc.OldCurencd,
wca.bbcc.EffectiveDate,
wca.bbcc.NewCntrycd,
wca.bbcc.NewCurencd,
wca.bbcc.OldBbgcompId,
wca.bbcc.NewBbgcompId,
wca.bbcc.OldBbgcomptk,
wca.bbcc.NewBbgcomptk,
wca.bbcc.RelEventId,
wca.bbcc.EventType,
wca.bbcc.Notes
from wca.bbcc
inner join wca.bond on wca.bbcc.secid = wca.bond.secid
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
where
wca.bbcc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
