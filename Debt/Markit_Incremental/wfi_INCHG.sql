--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('INCHG') as Tablename,
wca.inchg.ActFlag,
wca.inchg.AnnounceDate as Created,
wca.inchg.Acttime as Changed,
wca.inchg.InchgId,
wca.bond.SecId,
wca.scmst.Isin,
wca.inchg.IssId,
wca.inchg.InchgDate,
wca.inchg.OldCntrycd,
wca.inchg.NewCntrycd,
wca.inchg.EventType 
from wca.inchg
inner join wca.scmst on wca.inchg.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.inchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)