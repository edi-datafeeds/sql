--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('scagy') as Tablename,
wca.scagy.ActFlag,
wca.scagy.AnnounceDate as Created,
wca.scagy.Acttime as Changed,
wca.scagy.ScagyId,
wca.scagy.SecId,
wca.scmst.Isin,
wca.scagy.Relationship,
wca.scagy.AgncyId,
wca.scagy.GuaranteeType,
wca.scagy.SpStartDate,
wca.scagy.SpendDate,
wca.scagy.Notes
from wca.scagy
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scagy.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)