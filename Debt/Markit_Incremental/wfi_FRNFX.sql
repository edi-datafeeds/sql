--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_FRNFX
--fileheadertext=EDI_FRNFX_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('FRNFX') as Tablename,
wca.frnfx.ActFlag,
wca.frnfx.AnnounceDate as Created,
wca.frnfx.Acttime as Changed,
wca.frnfx.FrnFxid,
wca.frnfx.SecId,
wca.scmst.Isin,
wca.frnfx.EffectiveDate,
wca.frnfx.OldFrnType,
wca.frnfx.OldFrnIndexBenchmark,
wca.frnfx.OldMarkup as OldFrnMargin,
wca.frnfx.OldMinimumInterestRate,
wca.frnfx.OldMaximumInterestRate,
wca.frnfx.OldRounding,
wca.frnfx.NewFrnType,
wca.frnfx.NewFrnIndexBenchmark,
wca.frnfx.NewMarkup as NewFrnMargin,
wca.frnfx.NewMinimumInterestRate,
wca.frnfx.NewMaximumInterestRate,
wca.frnfx.NewRounding,
wca.frnfx.EventType,
wca.frnfx.OldFrnIntAdjFreq,
wca.frnfx.NewFrnIntAdjFreq
from wca.frnfx
inner join wca.bond on wca.frnfx.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.frnfx.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)