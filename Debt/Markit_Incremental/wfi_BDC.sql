--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BDC
--fileheadertext=EDI_BDC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('BDC') as Tablename,
wca.bdc.ActFlag,
wca.bdc.AnnounceDate as Created,
wca.bdc.Acttime as Changed,
wca.bdc.BdcId,
wca.bdc.SecId,
wca.scmst.Isin,
wca.bdc.BdcAppliedTo,
wca.bdc.CntrId,
wca.bdc.Notes
from wca.bdc
inner join wca.bond on wca.bdc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bdc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)
