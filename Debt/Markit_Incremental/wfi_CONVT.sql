--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CONVT') as Tablename,
wca.convt.ActFlag,
wca.convt.Announcedate as Created,
wca.convt.Acttime as Changed,
wca.convt.ConvtId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.convt.FromDate,
wca.convt.ToDate,
wca.convt.RatioNew,
wca.convt.RatioOld,
wca.convt.Curencd,
wca.convt.CurPair,
wca.convt.Price,
wca.convt.MandoptFlag,
wca.convt.ResSecId,
wca.convt.Sectycd as ResSectycd,
resscmst.Isin as ResIsin,
resissur.Issuername as ResIssuername,
resscmst.SecurityDesc as ResSecurityDesc,
wca.convt.Fractions,
wca.convt.FxRate,
wca.convt.PartFinalFlag,
wca.convt.PriceAsPercent,
wca.convt.ConvtNotes as Notes
from wca.convt
inner join wca.bond on wca.convt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = resscmst.secid
left outer join wca.issur as resissur on resscmst.issid = wca.resissur.issid
where
wca.convt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)