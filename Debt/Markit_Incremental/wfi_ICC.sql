--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ICC
--fileheadertext=EDI_ICC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('ICC') as Tablename,
wca.icc.ActFlag,
wca.icc.AnnounceDate as Created,
wca.icc.Acttime as Changed,
wca.icc.IccId,
wca.icc.SecId,
wca.scmst.Isin,
wca.icc.EffectiveDate,
wca.icc.OldIsin,
wca.icc.NewIsin,
wca.icc.OldUsCode,
wca.icc.NewUsCode,
wca.icc.OldValoren,
wca.icc.NewValoren,
wca.icc.EventType,
wca.icc.RelEventId,
wca.icc.OldCommonCode,
wca.icc.NewCommonCode
from wca.icc
inner join wca.bond on wca.icc.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.icc.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)