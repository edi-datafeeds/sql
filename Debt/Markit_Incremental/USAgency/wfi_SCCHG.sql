--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('SCCHG') as TableName,
SCCHG.Actflag,
SCCHG.AnnounceDate as Created,
SCCHG.Acttime as Changed,
SCCHG.ScChgID,
SCCHG.SecID,
SCMST.ISIN,
SCCHG.DateofChange,
SCCHG.SecOldName,
SCCHG.SecNewName,
SCCHG.EventType,
SCCHG.OldSectyCD,
SCCHG.NewSectyCD,
SCCHG.OldRegS144A,
SCCHG.NewRegS144A,
SCCHG.ScChgNotes as Notes
FROM SCCHG
INNER JOIN BOND ON SCCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and SCCHG.acttime > (select max(acttime)-0.1 from tbl_Opslog)