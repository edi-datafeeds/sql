--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('TRNCH') as TableName,
TRNCH.Actflag,
TRNCH.AnnounceDate as Created,
TRNCH.Acttime as Changed,
TRNCH.TrnchID,
TRNCH.SecID,
SCMST.ISIN,
TRNCH.TrnchNumber,
TRNCH.TrancheDate,
TRNCH.TrancheAmount,
TRNCH.ExpiryDate,
TRNCH.Notes as Notes
FROM TRNCH
INNER JOIN BOND ON TRNCH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and TRNCH.acttime > (select max(acttime)-0.1 from tbl_Opslog)
