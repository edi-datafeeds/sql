--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=EDI_CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CRCHG') as TableName,
CRCHG.Actflag,
CRCHG.AnnounceDate as Created,
CRCHG.Acttime as Changed, 
CRCHG.CrChgID,
CRCHG.SecID,
CRCHG.RatingAgency,
CRCHG.RatingDate,
CRCHG.OldRating,
CRCHG.NewRating,
CRCHG.Direction,
CRCHG.WatchList,
CRCHG.WatchListReason
FROM CRCHG
INNER JOIN BOND ON CRCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and CRCHG.acttime > (select max(acttime)-0.1 from tbl_Opslog)
