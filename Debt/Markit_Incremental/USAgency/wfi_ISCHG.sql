--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_ISCHG
--fileheadertext=EDI_ISCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('ISCHG') as TableName,
ISCHG.Actflag,
ISCHG.AnnounceDate as Created,
ISCHG.Acttime as Changed,
ISCHG.IschgID,
BOND.SecID,
SCMST.ISIN,
ISCHG.IssID,
ISCHG.NameChangeDate,
ISCHG.IssOldName,
ISCHG.IssNewName,
ISCHG.EventType,
ISCHG.LegalName,
ISCHG.MeetingDateFlag, 
ISCHG.IsChgNotes as Notes
FROM ISCHG
INNER JOIN SCMST ON ISCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and ISCHG.acttime > (select max(acttime)-0.1 from tbl_Opslog)
and ISCHG.actflag<>'D'
and (bond.issuedate<=ischg.namechangedate
     or bond.issuedate is null or ischg.namechangedate is null
    )

