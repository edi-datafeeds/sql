--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INTBC
--fileheadertext=EDI_INTBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('INTBC') as TableName,
INTBC.Actflag,
INTBC.AnnounceDate as Created,
INTBC.Acttime as Changed,
INTBC.IntbcID,
INTBC.SecID,
SCMST.ISIN,
INTBC.EffectiveDate,
INTBC.OldIntBasis,
INTBC.NewIntBasis,
INTBC.OldIntBDC,
INTBC.NewIntBDC
FROM INTBC
INNER JOIN BOND ON INTBC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and INTBC.acttime > (select max(acttime)-0.1 from tbl_Opslog)