--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LIQ
--fileheadertext=EDI_LIQ_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('LIQ') as TableName,
LIQ.Actflag,
LIQ.AnnounceDate as Created,
LIQ.Acttime as Changed,
LIQ.LiqID,
BOND.SecID,
SCMST.ISIN,
LIQ.IssID,
LIQ.Liquidator,
LIQ.LiqAdd1,
LIQ.LiqAdd2,
LIQ.LiqAdd3,
LIQ.LiqAdd4,
LIQ.LiqAdd5,
LIQ.LiqAdd6,
LIQ.LiqCity,
LIQ.LiqCntryCD,
LIQ.LiqTel,
LIQ.LiqFax,
LIQ.LiqEmail,
LIQ.RdDate
FROM LIQ
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and LIQ.acttime > (select max(acttime)-0.1 from tbl_Opslog)
