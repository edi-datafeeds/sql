--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LIQNOTES') as TableName,
LIQ.Actflag,
LIQ.LiqID,
LIQ.LiquidationTerms
FROM LIQ
where 
LIQ.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and LIQ.acttime > (select max(acttime)-0.1 from tbl_Opslog)
