--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_DEBT_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT distinct
upper('BBE') as tablename,
bbe.actflag,
bbe.announcedate,
bbe.acttime,
bbe.bbeid,
bbe.secid,
bbe.exchgcd,
bbe.curencd,
bbe.bbgexhid,
bbe.bbgexhtk
from bbe
inner join scmst on bbe.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and BBE.acttime > (select max(acttime)-0.1 from tbl_Opslog)