--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('PVRD') as TableName,
PVRD.Actflag,
PVRD.AnnounceDate as Created,
PVRD.Acttime as Changed,
PVRD.PvRdID,
PVRD.SecID,
SCMST.ISIN,
PVRD.EffectiveDate,
PVRD.CurenCD,
PVRD.OldParValue,
PVRD.NewParValue,
PVRD.EventType,
PVRD.PvRdNotes as Notes
FROM PVRD
INNER JOIN BOND ON PVRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and PVRD.acttime > (select max(acttime)-0.1 from tbl_Opslog)
