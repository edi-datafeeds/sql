--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_CPOPT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.CpoptID,
SCMST.SecID,
SCMST.ISIN,
CPOPT.CallPut, 
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.cptype,
CPOPT.PriceAsPercent,
CPOPT.InWholePart,
CPOPT.FormulaBasedPrice,
CPOPN.Notes as Notes 
FROM CPOPT
INNER JOIN BOND ON CPOPT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN CPOPN ON CPOPT.SECID = CPOPN.SECID AND CPOPT.CALLPUT = CPOPN.CALLPUT
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and CPOPT.acttime > (select max(acttime)-0.1 from tbl_Opslog)
