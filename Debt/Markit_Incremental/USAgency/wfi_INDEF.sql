--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INDEF
--fileheadertext=EDI_INDEF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select
'INDEF' as TableName,
INDEF.Actflag,
INDEF.AnnounceDate as Created,
INDEF.Acttime as Changed,
INDEF.IndefID,
INDEF.SecID,
SCMST.ISIN,
INDEF.DefaultType,
INDEF.DefaultDate,
INDEF.Notes
FROM INDEF
INNER JOIN BOND ON INDEF.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and INDEF.acttime > (select max(acttime)-0.1 from tbl_Opslog)
