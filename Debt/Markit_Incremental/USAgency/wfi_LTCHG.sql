--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=EDI_LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('LTCHG') as TableName,
LTCHG.Actflag,
LTCHG.AnnounceDate as Created,
LTCHG.Acttime as Changed,
LTCHG.LtChgID,
SCMST.SecID,
SCMST.ISIN,
LTCHG.ExchgCD,
LTCHG.EffectiveDate,
LTCHG.OldLot,
LTCHG.OldMinTrdQty,
LTCHG.NewLot,
LTCHG.NewMinTrdgQty
FROM LTCHG
INNER JOIN BOND ON LTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and LTCHG.acttime > (select max(acttime)-0.1 from tbl_Opslog)
