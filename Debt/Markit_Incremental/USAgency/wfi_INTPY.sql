--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use wca
select distinct
'INTPY' as TableName,
CASE WHEN INT.Actflag = 'D' or INT.actflag='C' or intpy.actflag is null THEN INT.Actflag ELSE INTPY.Actflag END as Actflag,
INT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > INT.Acttime) and (RD.Acttime > EXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > INT.Acttime) THEN EXDT.Acttime ELSE INT.Acttime END as [Changed],
SCMST.SecID,
SCMST.ISIN,
INTPY.BCurenCD as DebtCurrency,
INTPY.BParValue as NominalValue,
SCEXH.ExchgCD,
INT.RdID,
case when intpy.optionid is null then '1' else intpy.optionid end as OptionID,
RD.Recdate,
EXDT.Exdate,
EXDT.Paydate,
INT.InterestFromDate,
INT.InterestToDate,
INT.Days,
INTPY.CurenCD as Interest_Currency,
INTPY.IntRate as Interest_Rate,
INTPY.GrossInterest,
INTPY.NetInterest,
INTPY.DomesticTaxRate,
INTPY.NonResidentTaxRate,
INTPY.RescindInterest,
INTPY.AgencyFees,
INTPY.CouponNo,
--null as CouponID,
INTPY.DefaultOpt,
INTPY.OptElectionDate,
INTPY.AnlCoupRate,
INT.InDefPay
from [INT]
INNER JOIN RD ON INT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON BOND.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID 
     AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID
where
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
--and INTPY.acttime > (select max(acttime)-0.1 from tbl_Opslog)
and (RD.acttime> (select max(acttime)-0.1 from tbl_Opslog)
or scmst.acttime> (select max(acttime)-0.1 from tbl_Opslog)
or bond.acttime> (select max(acttime)-0.1 from tbl_Opslog)
or issur.acttime> (select max(acttime)-0.1 from tbl_Opslog)
or int.acttime> (select max(acttime)-0.1 from tbl_Opslog)
or intpy.acttime> (select max(acttime)-0.1 from tbl_Opslog))
