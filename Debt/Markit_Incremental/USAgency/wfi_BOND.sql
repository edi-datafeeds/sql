--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('BOND') as Tablename,
BOND.Actflag,
BOND.AnnounceDate as Created,
BOND.Acttime as Changed,
BOND.SecID,
SCMST.ISIN,
BOND.BondType,
BOND.DebtMarket,
BOND.CurenCD as DebtCurrency,
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as NominalValue,
BOND.IssueDate,
BOND.IssueCurrency,
BOND.IssuePrice,
BOND.IssueAmount,
BOND.IssueAmountDate,
BOND.OutstandingAmount,
BOND.OutstandingAmountDate,
BOND.InterestBasis,
BOND.InterestRate,
BOND.InterestAccrualConvention,
BOND.InterestPaymentFrequency,
BOND.IntCommencementDate,
BOND.FirstCouponDate,
BOND.InterestPayDate1,
BOND.InterestPayDate2,
BOND.InterestPayDate3,
BOND.InterestPayDate4,
bondx.DomesticTaxRate,
bondx.NonResidentTaxRate,
BOND.FRNType,
BOND.FRNIndexBenchmark,
BOND.Markup as FrnMargin,
BOND.MinimumInterestRate as FrnMinInterestRate,
BOND.MaximumInterestRate as FrnMaxInterestRate,
BOND.Rounding,
bondx.Series,
bondx.Class,
bondx.OnTap,
bondx.MaximumTapAmount,
bondx.TapExpiryDate,
BOND.Guaranteed,
BOND.SecuredBy,
BOND.SecurityCharge,
BOND.Subordinate,
BOND.SeniorJunior,
BOND.WarrantAttached,
BOND.MaturityStructure,
BOND.Perpetual,
BOND.MaturityDate,
BOND.MaturityExtendible,
BOND.Callable,
BOND.Puttable,
bondx.Denomination1,
bondx.Denomination2,
bondx.Denomination3,
bondx.Denomination4,
bondx.Denomination5,
bondx.Denomination6,
bondx.Denomination7,
bondx.MinimumDenomination,
bondx.DenominationMultiple,
BOND.Strip,
BOND.StripInterestNumber, 
BOND.Bondsrc,
BOND.MaturityBenchmark,
BOND.ConventionMethod,
BOND.FrnIntAdjFreq as FrnInterestAdjFreq,
BOND.IntBusDayConv as InterestBusDayConv,
BOND.InterestCurrency,
BOND.MatBusDayConv as MaturityBusDayConv,
BOND.MaturityCurrency, 
bondx.TaxRules,
BOND.VarIntPayDate as VarInterestPaydate,
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
case when bond.matprice<>'' then cast(bond.matprice as decimal(18,4))
     when rtrim(bond.largeparvalue)='' then null
     when rtrim(BOND.MatPriceAsPercent)='' then null
     else cast(bond.largeparvalue as decimal(18,0)) * cast(wca.dbo.BOND.MatPriceAsPercent as decimal(18,4))/100 
     end as MatPrice,
BOND.MatPriceAsPercent,
BOND.SinkingFund,
bondx.GovCity,
bondx.GovState,
bondx.GovCntry,
bondx.GovLawLkup as GovLaw,
bondx.GoverningLaw as GovLawNotes,
BOND.Municipal,
BOND.PrivatePlacement,
BOND.Syndicated,
BOND.Tier,
BOND.UppLow,
BOND.Collateral,
BOND.CoverPool,
BOND.PikPay,
BOND.YieldAtIssue,
bond.CoCoTrigger as Coco,
BOND.Notes
FROM BOND
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
left outer join bondx on bond.secid = bondx.secid
where
scmst.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and bond.acttime > (select max(acttime)-0.1 from tbl_Opslog)
