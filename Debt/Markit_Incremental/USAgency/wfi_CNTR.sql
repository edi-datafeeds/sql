--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CNTR
--fileheadertext=EDI_CNTR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CNTR') as TableName,
CNTR.Actflag,
CNTR.AnnounceDate as Created,
CNTR.Acttime as Changed,
CNTR.CntrID,
CNTR.CentreName,
CNTR.CntryCD
FROM CNTR
where
CNTR.actflag<>'D'
and CNTR.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)