--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SDCHG
--fileheadertext=EDI_SDCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use WCA
SELECT 
upper('SDCHG') as TableName,
SDCHG.Actflag,
SDCHG.AnnounceDate as Created, 
SDCHG.Acttime as Changed,
SDCHG.SdChgID,
SDCHG.SecID,
SCMST.ISIN,
SDCHG.CntryCD as OldCntryCD,
SDCHG.EffectiveDate,
SDCHG.OldSedol,
SDCHG.NewSedol,
SDCHG.EventType,
SDCHG.RcntryCD as OldRcntryCD,
SDCHG.RelEventID,
SDCHG.NewCntryCD,
SDCHG.NewRcntryCD,
SDCHG.sdchgNotes As Notes
FROM SDCHG
INNER JOIN BOND ON SDCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where 
scmst.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and SDCHG.acttime > (select max(acttime)-0.1 from tbl_Opslog)
