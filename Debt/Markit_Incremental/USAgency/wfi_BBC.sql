--filepath=o:\Datafeed\Debt\Markit_WFI\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_DEBT_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_WFI\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('BBC') as tablename,
bbc.actflag,
bbc.announcedate,
bbc.acttime,
bbc.bbcid,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
bbc.bbgcompid,
bbc.bbgcomptk
from bbc
inner join scmst on bbc.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where 
SCMST.issid in (select portfolio.dbo.Markit_issid.issid from portfolio.dbo.Markit_issid)
and bbc.acttime > (select max(acttime)-0.1 from tbl_Opslog)