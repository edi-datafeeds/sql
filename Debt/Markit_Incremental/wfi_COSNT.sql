--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
'COSNT' as Tablename,
wca.cosnt.Actflag,
wca.cosnt.Announcedate as Created,
case when (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.cosnt.Acttime) then wca.rd.Acttime else wca.cosnt.Acttime end as Changed,
wca.cosnt.RdId,
wca.scmst.SecId,
wca.rd.RecDate,
wca.scmst.Isin,
wca.cosnt.ExpiryDate,
wca.cosnt.ExpiryTime,
SUBSTRING(wca.cosnt.TimeZone, 1,3) AS TimeZone,
wca.cosnt.CollateralRelease,
wca.cosnt.Currency,
wca.cosnt.Fee,
wca.cosnt.Notes
from wca.cosnt
inner join wca.rd on wca.cosnt.rdid = wca.rd.rdid
inner join wca.bond on wca.rd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.cosnt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)