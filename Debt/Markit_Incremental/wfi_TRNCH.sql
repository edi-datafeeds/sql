--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_TRNCH
--fileheadertext=EDI_TRNCH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('TRNCH') as Tablename,
wca.trnch.ActFlag,
wca.trnch.AnnounceDate as Created,
wca.trnch.Acttime as Changed,
wca.trnch.TrnchId,
wca.trnch.SecId,
wca.scmst.Isin,
wca.trnch.TrnchNumber,
wca.trnch.TrancheDate,
wca.trnch.TrancheAmount,
wca.trnch.ExpiryDate,
wca.trnch.Notes as Notes
from wca.trnch
inner join wca.bond on wca.trnch.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.trnch.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)