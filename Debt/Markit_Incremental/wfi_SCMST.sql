--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCMST') as Tablename,
wca.scmst.ActFlag,
wca.scmst.AnnounceDate as Created,
wca.scmst.Acttime as Changed,
wca.scmst.SecId,
wca.scmst.Isin,
wca.scmst.ParentSecid,
wca.scmst.IssId,
wca.scmst.Sectycd,
wca.scmst.SecurityDesc,
case when wca.scmst.StatusFlag = '' then 'A' else wca.scmst.StatusFlag end as StatusFlag,
wca.scmst.PrimaryExchgcd,
wca.scmst.Curencd,
wca.scmst.ParValue,
wca.scmst.UsCode,
wca.scmst.X as CommonCode,
wca.scmst.Holding,
wca.scmst.Structcd,
wca.scmst.Wkn,
wca.scmst.Regs144a,
wca.scmst.Cfi,
wca.scmst.ScmstNotes as Notes
from wca.scmst
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)