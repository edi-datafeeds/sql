--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BSCHG
--fileheadertext=EDI_BSCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('BSCHG') as Tablename,
wca.bschg.ActFlag,
wca.bschg.AnnounceDate as Created,
wca.bschg.Acttime as Changed,
wca.bschg.BschgId,
wca.bschg.SecId,
wca.scmst.Isin,
wca.bschg.NotificationDate,
wca.bschg.OldBondType,
wca.bschg.NewBondType,
wca.bschg.OldCurencd,
wca.bschg.NewCurencd,
wca.bschg.OldPIU,
wca.bschg.NewPIU,
wca.bschg.OldInterestBasis,
wca.bschg.NewInterestBasis,
wca.bschg.EventType,
wca.bschg.OldInterestCurrency,
wca.bschg.NewInterestCurrency,
wca.bschg.OldMaturityCurrency,
wca.bschg.NewMaturityCurrency,
wca.bschg.OldIntBusDayConv,
wca.bschg.NewIntBusDayConv,
wca.bschg.OldMatBusDayConv,
wca.bschg.NewMatBusDayConv,
wca.bschg.BschgNotes as Notes
from wca.bschg
inner join wca.bond on wca.bschg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.bschg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)