--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('PVRD') as Tablename,
wca.pvrd.ActFlag,
wca.pvrd.AnnounceDate as Created,
wca.pvrd.Acttime as Changed,
wca.pvrd.PvrdId,
wca.pvrd.SecId,
wca.scmst.Isin,
wca.pvrd.EffectiveDate,
wca.pvrd.Curencd,
wca.pvrd.OldParValue,
wca.pvrd.NewParValue,
wca.pvrd.EventType,
wca.pvrd.PvrdNotes as Notes
from wca.pvrd
inner join wca.bond on wca.pvrd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.pvrd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)