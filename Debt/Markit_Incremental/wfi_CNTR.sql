--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CNTR
--fileheadertext=EDI_CNTR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CNTR') as Tablename,
wca.cntr.ActFlag,
wca.cntr.Announcedate as Created,
wca.cntr.Acttime as Changed,
wca.cntr.CntrId,
wca.cntr.CentreName,
wca.cntr.Cntrycd
from wca.cntr
#where
#
#wca.cntr.actflag<>'d'
#wca.cntr.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where
