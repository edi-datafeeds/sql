--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CONV
--fileheadertext=EDI_CONV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
select
upper('CONV') as Tablename,
wca.conv.ActFlag,
wca.conv.Announcedate as Created,
case when (wca.rd.Acttime is not null) and (wca.rd.Acttime > wca.conv.Acttime) then wca.rd.Acttime else wca.conv.Acttime end as Changed,
wca.conv.ConvId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.rd.RecDate,
wca.conv.FromDate,
wca.conv.ToDate,
wca.conv.RatioNew,
wca.conv.RatioOld,
wca.conv.Curencd as ConvCurrency,
wca.conv.CurPair,
wca.conv.Price,
wca.conv.MandoptFlag,
wca.conv.ResSecId,
wca.conv.ResSectycd,
resscmst.Isin as ResIsin,
resissur.IssuerName as ResIssuerName,
resscmst.SecurityDesc as ResSecurityDesc,
wca.conv.Fractions,
wca.conv.FxRate,
wca.conv.PartFinalFlag,
wca.conv.ConvType,
wca.conv.RdId,
wca.conv.PriceAsPercent,
wca.conv.AmountConverted,
wca.conv.SettlementDate,
wca.conv.ConvNotes as Notes
from wca.conv
inner join wca.bond on wca.conv.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.rd on wca.conv.rdid = wca.rd.rdid
left outer join wca.scmst as resscmst on wca.conv.ressecid = resscmst.secid
left outer join wca.issur as resissur on resscmst.issid = resissur.issid
where
wca.conv.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)