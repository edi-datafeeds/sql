--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('SCCHG') as Tablename,
wca.scchg.ActFlag,
wca.scchg.AnnounceDate as Created,
wca.scchg.Acttime as Changed,
wca.scchg.ScchgId,
wca.scchg.SecId,
wca.scmst.Isin,
wca.scchg.DateOfChange,
wca.scchg.SecOldName,
wca.scchg.SecNewName,
wca.scchg.EventType,
wca.scchg.OldSectycd,
wca.scchg.NewSectycd,
wca.scchg.OldRegs144a,
wca.scchg.NewRegs144a,
wca.scchg.ScchgNotes as Notes
from wca.scchg
inner join wca.bond on wca.scchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)