--filepath=O:\Datafeed\Debt\Markit_Incremental\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CURRD
--fileheadertext=EDI_CURRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\Markit_Incremental\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select seq from wca.tbl_opslog order by acttime desc limit 1
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CURRD') as Tablename,
wca.currd.ActFlag,
wca.currd.AnnounceDate as Created,
wca.currd.Acttime as Changed,
wca.currd.CurrdId,
wca.currd.SecId,
wca.scmst.Isin,
wca.currd.EffectiveDate,
wca.currd.OldCurencd,
wca.currd.NewCurencd,
wca.currd.OldParValue,
wca.currd.NewParValue,
wca.currd.EventType,
wca.currd.CurrdNotes as Notes
from wca.currd
inner join wca.bond on wca.currd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.currd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog)