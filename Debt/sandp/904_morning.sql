--filepath=o:\Datafeed\Debt\extracts\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.904
--suffix=
--fileheadertext=EDI_DEBT_904_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\904\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 
use WCA
SELECT 
upper('AGM') as TableName,
AGM.Actflag,
AGM.AnnounceDate,
AGM.Acttime, 
AGM.AGMID,
AGM.IssID,
AGM.AGMDate,
AGM.AGMEGM,
AGM.AGMNo,
AGM.FYEDate,
AGM.AGMTime,
AGM.Add1,
AGM.Add2,
AGM.Add3,
AGM.Add4,
AGM.Add5,
AGM.Add6,
AGM.City,
AGM.CntryCD,
AGM.BondSecID
FROM AGM
inner join BOND on AGM.BondSecID = BOND.secid
where 
(AGM.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('AGNCY') as TableName,
AGNCY.Actflag,
AGNCY.AnnounceDate, 
AGNCY.Acttime, 
AGNCY.AgncyID, 
AGNCY.RegistrarName, 
AGNCY.Add1,
AGNCY.Add2, 
AGNCY.Add3, 
AGNCY.Add4, 
AGNCY.Add5, 
AGNCY.Add6, 
AGNCY.City, 
AGNCY.CntryCD, 
AGNCY.WebSite, 
AGNCY.Contact1, 
AGNCY.Tel1, 
AGNCY.Fax1, 
AGNCY.email1, 
AGNCY.Contact2, 
AGNCY.Tel2, 
AGNCY.Fax2, 
AGNCY.email2, 
AGNCY.Depository 
FROM AGNCY
where 
AGNCY.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)


--# 
use WCA
SELECT 
upper('AGYDT') as TableName,
AGYDT.Actflag,
AGYDT.AnnounceDate, 
AGYDT.Acttime,
AGYDT.AgydtID, 
AGYDT.AgncyID,
AGYDT.EffectiveDate,
AGYDT.OldRegistrarName,
AGYDT.OldAdd1,
AGYDT.OldAdd2,
AGYDT.OldAdd3,
AGYDT.OldAdd4,
AGYDT.OldAdd5,
AGYDT.OldAdd6,
AGYDT.OldCity,
AGYDT.OldCntryCD,
AGYDT.OldWebSite,
AGYDT.OldContact1,
AGYDT.OldTel1,
AGYDT.OldFax1,
AGYDT.Oldemail1,
AGYDT.OldContact2,
AGYDT.OldTel2,
AGYDT.OldFax2,
AGYDT.Oldemail2,
AGYDT.NewRegistrarName,
AGYDT.NewAdd1,
AGYDT.NewAdd2,
AGYDT.NewAdd3,
AGYDT.NewAdd4,
AGYDT.NewAdd5,
AGYDT.NewAdd6,
AGYDT.NewCity,
AGYDT.NewCntryCD,
AGYDT.NewWebSite,
AGYDT.NewContact1,
AGYDT.NewTel1,
AGYDT.NewFax1,
AGYDT.Newemail1,
AGYDT.NewContact2,
AGYDT.NewTel2,
AGYDT.NewFax2,
AGYDT.Newemail2 
FROM AGYDT
where 
AGYDT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)


--# 
use WCA
SELECT 
upper('ANN') as TableName,
ANN.Actflag,
ANN.AnnounceDate, 
ANN.Acttime,  
ANN.AnnID,
ANN.IssID, 
ANN.EventType , 
ANN.NotificationDate,   
ANN.AnnNotes as Notes
FROM ANN
where 
ANN.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)

--# 
use WCA
SELECT 
upper('ARR') as TableName,
ARR.Actflag,
ARR.AnnounceDate,
ARR.Acttime, 
ARR.RdID, 
ARR.ArrNotes as Notes
FROM ARR
inner join rd on arr.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(arr.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)



--# 
use WCA
SELECT 
upper('ASSM') as TableName,
ASSM.Actflag, 
ASSM.AnnounceDate,
ASSM.Acttime, 
ASSM.AssmID, 
ASSM.SecID, 
ASSM.ExchgCD, 
ASSM.ResSecID, 
ASSM.AssimilationDate, 
ASSM.SectyCD as ResSectyCD
FROM ASSM
inner join bond on ASSM.secid = bond.secid
where 
(assm.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('BB') as TableName,
BB.Actflag, 
BB.AnnounceDate,
BB.Acttime, 
BB.BBID, 
BB.SecID, 
BB.RdID, 
BB.OnOffFlag, 
BB.StartDate, 
BB.EndDate,  
BB.MinAcpQty, 
BB.MaxAcpQty, 
BB.BBMinPct, 
BB.BBMaxPct,
BB.BBNotes as Notes
FROM BB
where 
1=2

--#
use WCA
SELECT 
upper('BDC') as TableName,
BDC.Actflag,
BDC.AnnounceDate,
BDC.Acttime,
BDC.BDCID,
BDC.SecID,
BDC.BDCAppliedTo,
BDC.CntrID,
BDC.Notes
FROM BDC
inner join bond on BDC.secid = bond.secid
where 
(bdc.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('BOCHG') as TableName,
BOCHG.Actflag,
BOCHG.AnnounceDate,
BOCHG.Acttime,  
BOCHG.BochgID, 
BOCHG.SecID, 
BOCHG.EffectiveDate, 
BOCHG.OldOutValue, 
BOCHG.NewOutValue, 
BOCHG.EventType , 
BOCHG.OldOutDate, 
BOCHG.NewOutDate, 
BOCHG.BochgNotes as Notes
FROM BOCHG
inner join bond on BOCHG.secid = bond.secid
where 
(BOCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('BOND') as TableName,
BOND.Actflag,
BOND.AnnounceDate,
BOND.Acttime,  
BOND.SecId, 
BOND.BondType, 
BOND.DebtMarket, 
BOND.CurenCD, 
BOND.ParValue, 
BOND.IssueDate, 
BOND.IssueCurrency, 
BOND.IssuePrice, 
BOND.IssueAmount, 
BOND.IssueAmountDate, 
BOND.OutstandingAmount, 
BOND.OutstandingAmountDate, 
BOND.InterestBasis, 
BOND.InterestRate, 
BOND.InterestAccrualConvention, 
BOND.InterestPaymentFrequency, 
BOND.IntCommencementDate, 
BOND.FirstCouponDate, 
BOND.InterestPayDate1, 
BOND.InterestPayDate2, 
BOND.InterestPayDate3, 
BOND.InterestPayDate4, 
BOND.DomesticTaxRate, 
BOND.NonResidentTaxRate, 
BOND.FRNType, 
BOND.FRNIndexBenchmark, 
BOND.Markup, 
BOND.MinimumInterestRate, 
BOND.MaximumInterestRate, 
BOND.Rounding, 
BOND.Series, 
BOND.Class, 
BOND.OnTap, 
BOND.MaximumTapAmount, 
BOND.TapExpiryDate, 
BOND.Guaranteed, 
BOND.SecuredBy, 
BOND.SecurityCharge, 
BOND.Subordinate, 
BOND.SeniorJunior, 
BOND.WarrantAttached, 
BOND.MaturityStructure, 
BOND.Perpetual, 
BOND.MaturityDate, 
BOND.MaturityExtendible, 
BOND.Callable, 
BOND.Puttable, 
BOND.Denomination1, 
BOND.Denomination2, 
BOND.Denomination3, 
BOND.Denomination4, 
BOND.Denomination5, 
BOND.Denomination6, 
BOND.Denomination7, 
BOND.MinimumDenomination, 
BOND.DenominationMultiple, 
BOND.Strip, 
BOND.StripInterestNumber,  
BOND.Bondsrc, 
BOND.MaturityBenchmark, 
BOND.ConventionMethod, 
BOND.FrnIntAdjFreq, 
BOND.IntBusDayConv, 
BOND.InterestCurrency, 
BOND.MatBusDayConv, 
BOND.MaturityCurrency,  
BOND.TaxRules, 
BOND.VarIntPayDate, 
BOND.PriceAsPercent,
BOND.PayOutMode,
BOND.Cumulative,
BOND.GoverningLaw, 
BOND.Tier, 
BOND.UppLow, 
BOND.Collateral, 
BOND.CoverPool, 
BOND.PikPay, 
BOND.Notes as Notes
FROM BOND
inner join scmst on bond.secid = scmst.secid
where 
(
(BOND.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or (SCMST.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
)
or bond.secid in (select secid from portfolio.dbo.pushsap)




--# 
use WCA
SELECT 
upper('BSCHG') as TableName,
BSCHG.Actflag,
BSCHG.AnnounceDate,
BSCHG.Acttime,
BSCHG.BschgID,
BSCHG.SecID,
BSCHG.NotificationDate,
BSCHG.OldBondType,
BSCHG.NewBondType,
BSCHG.OldCurenCD,
BSCHG.NewCurenCD,
BSCHG.OldPIU,
BSCHG.NewPIU,
BSCHG.OldInterestBasis,
BSCHG.NewInterestBasis,
BSCHG.Eventtype,
BSCHG.OldInterestCurrency,
BSCHG.NewInterestCurrency,
BSCHG.OldMaturityCurrency,
BSCHG.NewMaturityCurrency,
BSCHG.OldIntBusDayConv,
BSCHG.NewIntBusDayConv,
BSCHG.OldMatBusDayConv,
BSCHG.NewMatBusDayConv,
BSCHG.BschgNotes as Notes
FROM BSCHG
where 
(BSCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)



--# 
use WCA
SELECT 
upper('CNTR') as TableName,
CNTR.Actflag,
CNTR.AnnounceDate,
CNTR.Acttime,
CNTR.CntrID,
CNTR.CentreName,
CNTR.CntryCD
FROM CNTR
where 
CNTR.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)


--# 
use WCA
SELECT 
upper('COSNT') as TableName,
COSNT.Actflag,
COSNT.AnnounceDate,
COSNT.Acttime,
COSNT.RDID,
COSNT.ExpiryDate,
COSNT.ExpiryTime,
SUBSTRING(cosnt.TimeZone, 1,3) AS TimeZone,
COSNT.CollateralRelease,
COSNT.Currency,
COSNT.Fee,
COSNT.Notes as Notes
FROM COSNT
inner join rd on COSNT.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(COSNT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('CONV') as TableName,
CONV.Actflag,
CONV.AnnounceDate,
CONV.Acttime,
CONV.ConvID,
CONV.SecID,
CONV.FromDate,
CONV.ToDate,
CONV.RatioNew,
CONV.RatioOld,
CONV.CurenCD,
CONV.Price,
CONV.MandOptFlag,
CONV.ResSecID,
CONV.ResSectyCD,
CONV.Fractions,
CONV.FXrate,
CONV.PartFinalFlag,
CONV.ConvType,
CONV.RDID,
CONV.PriceAsPercent,
CONV.ConvNotes as Notes
FROM CONV
inner join bond on CONV.secid = bond.secid
where 
(CONV.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('CONVT') as TableName,
CONVT.Actflag,
CONVT.AnnounceDate,
CONVT.Acttime,
CONVT.ConvtID,
CONVT.SecID,
CONVT.FromDate,
CONVT.ToDate,
CONVT.RatioNew,
CONVT.RatioOld,
CONVT.CurenCD,
CONVT.Price,
CONVT.MandOptFlag,
CONVT.ResSecID,
CONVT.SectyCD as ResSectyCD,
CONVT.Fractions,
CONVT.FXrate,
CONVT.PartFinalFlag,
CONVT.PriceAsPercent,
CONVT.ConvtNotes as Notes
FROM CONVT
inner join bond on convt.secid = bond.secid
where 
(CONVT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('CPOPT') as TableName,
CPOPT.Actflag,
CPOPT.AnnounceDate,
CPOPT.Acttime,
CPOPT.CpoptID,
CPOPT.CallPut, 
CPOPT.SecID,
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.Currency,
CPOPT.Price,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
CPOPT.CPType,
CPOPT.PriceAsPercent,
CPOPT.Notes as Notes 
FROM CPOPT
where 
(CPOPT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('CRCHG') as TableName,
CRCHG.Actflag,
CRCHG.AnnounceDate,
CRCHG.Acttime,
CRCHG.CrchgID,
CRCHG.SecId,
CRCHG.RatingAgency,
CRCHG.RatingDate,
CRCHG.OldRating,
CRCHG.NewRating,
CRCHG.Direction,
CRCHG.WatchList,
CRCHG.WatchListReason
FROM CRCHG
where 
(CRCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('CRDRT') as TableName,
CRDRT.Actflag,
CRDRT.AnnounceDate,
CRDRT.Acttime, 
CRDRT.SecID,
CRDRT.RatingAgency,
CRDRT.RatingDate,
CRDRT.Rating,
CRDRT.Direction,
CRDRT.WatchList,
CRDRT.WatchListReason
FROM CRDRT
where 
(CRDRT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('CTCHG') as TableName,
CTCHG.Actflag,
CTCHG.AnnounceDate,
CTCHG.Acttime, 
CTCHG.CTChgID,
CTCHG.SecID,
CTCHG.EffectiveDate,
CTCHG.OldResultantRatio,
CTCHG.NewResultantRatio,
CTCHG.OldSecurityRatio,
CTCHG.NewSecurityRatio,
CTCHG.OldCurrency,
CTCHG.NewCurrency,
CTCHG.OldConversionPrice,
CTCHG.NewConversionPrice,
CTCHG.ResSectyCD,
CTCHG.OldResSecID,
CTCHG.NewResSecID,
CTCHG.EventType ,
CTCHG.RelEventID,
CTCHG.OldFromDate,
CTCHG.NewFromDate,
CTCHG.OldTodate,
CTCHG.NewToDate,
CTCHG.ConvtID,
CTCHG.OldFXRate,
CTCHG.NewFXRate,
CTCHG.OldPriceAsPercent,
CTCHG.NewPriceAsPercent,
CTCHG.Notes
FROM CTCHG
where 
(CTCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('CTX') as TableName,
CTX.Actflag,
CTX.AnnounceDate, 
CTX.Acttime,
CTX.CtXID,
CTX.SecID,
CTX.StartDate,
CTX.EndDate,
CTX.ResSecID,
CTX.EventType ,
CTX.SectyCD as ResSectyCD,
CTX.CtxNotes as Notes
FROM CTX
where 
1=2

--# 
use WCA
SELECT 
upper('CURRD') as TableName,
CURRD.Actflag,
CURRD.AnnounceDate,
CURRD.Acttime,
CURRD.CurrdID,
CURRD.SecID,
CURRD.EffectiveDate,
CURRD.OldCurenCD,
CURRD.NewCurenCD,
CURRD.OldParValue,
CURRD.NewParValue,
CURRD.EventType ,
CURRD.CurRdNotes as Notes
FROM CURRD
inner join bond on CURRD.secid = bond.secid
where 
(CURRD.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('DMRGR') as TableName,
DMRGR.Actflag,
DMRGR.AnnounceDate,
DMRGR.Acttime,
DMRGR.RdID,
DMRGR.EffectiveDate,
DMRGR.DmrgrNotes as Notes
FROM DMRGR
inner join rd on dmrgr.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(DMRGR.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('DPRCP') as TableName,
DPRCP.Actflag,
DPRCP.AnnounceDate,
DPRCP.Acttime, 
DPRCP.SecID,
DPRCP.UnSecID,
DPRCP.DRRatio,
DPRCP.USRatio,
DPRCP.SpnFlag,
DPRCP.DRtype,
DPRCP.DepBank,
DPRCP.LevDesc,
DPRCP.OtherDepBank,
DPRCP.DprcpNotes as Notes
FROM DPRCP
where 
1=2

--# 
use WCA
SELECT 
upper('DRCHG') as TableName,
DRCHG.Actflag,
DRCHG.AnnounceDate,
DRCHG.Acttime, 
DRCHG.DRCHGID,
DRCHG.SecID,
DRCHG.EffectiveDate,
DRCHG.OldDRRatio,
DRCHG.NewDRRatio,
DRCHG.OldUNRatio,
DRCHG.NewUNRatio,
DRCHG.OldUNSecID,
DRCHG.NewUNSecID,
DRCHG.EventType ,
DRCHG.OldDepbank,
DRCHG.NewDepbank,
DRCHG.OldDRtype,
DRCHG.NewDRtype,
DRCHG.OldLevel,
DRCHG.NewLevel,
DRCHG.DrchgNotes as Notes
FROM DRCHG
where 
1=2

--# 
use WCA
SELECT 
upper('EXDT') as TableName,
EXDT.Actflag,
EXDT.AnnounceDate,
EXDT.Acttime,
EXDT.RdID,
EXDT.EventType,
EXDT.ExchgCD,
EXDT.ExDate,
EXDT.PayDate,
EXDT.PayDate2,
EXDT.ExCalc,
EXDT.PayCalc
FROM EXDT
inner join rd on exdt.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(EXDT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or rd.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('FRNFX') as TableName,
FRNFX.Actflag,
FRNFX.AnnounceDate,
FRNFX.Acttime,
FRNFX.FrnfxID,
FRNFX.SecId,
FRNFX.EffectiveDate,
FRNFX.OldFRNType,
FRNFX.OldFRNIndexBenchmark,
' ' as OldMarkup,
FRNFX.OldMinimumInterestRate,
FRNFX.OldMaximumInterestRate,
FRNFX.OldRounding,
FRNFX.NewFRNType,
FRNFX.NewFRNindexBenchmark,
' ' as NewMarkup,
FRNFX.NewMinimumInterestRate,
FRNFX.NewMaximumInterestRate,
FRNFX.NewRounding,
FRNFX.Eventtype,
FRNFX.OldFrnIntAdjFreq,
FRNFX.NewFrnIntAdjFreq
FROM FRNFX
inner join bond on FRNFX.secid = bond.secid
where 
(FRNFX.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)



--# 
use WCA
SELECT 
upper('FYCHG') as TableName,
FYCHG.Actflag,
FYCHG.AnnounceDate,
FYCHG.Acttime,
FYCHG.FychgID,
FYCHG.IssID,
FYCHG.NotificationDate,
FYCHG.OldFYStartDate,
FYCHG.OldFYEndDate,
FYCHG.NewFYStartDate,
FYCHG.NewFYEndDate,
FYCHG.Eventtype
FROM FYCHG
inner join scmst on FYCHG.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where 
(FYCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('ICC') as TableName,
ICC.Actflag,
ICC.AnnounceDate,
ICC.Acttime,
ICC.IccID,
ICC.SecID,
ICC.EffectiveDate,
ICC.OldISIN,
ICC.NewISIN,
ICC.OldUSCode,
ICC.NewUSCode,
ICC.OldVALOREN,
ICC.NewVALOREN,
ICC.EventType ,
ICC.RelEventID,
ICC.OldCommonCode,
ICC.NewCommonCode
FROM ICC
inner join bond on icc.secid = bond.secid
where 
(ICC.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('IFCHG') as TableName,
IFCHG.Actflag,
IFCHG.AnnounceDate,
IFCHG.Acttime,
IFCHG.IfchgID,
IFCHG.SecID,
IFCHG.NotificationDate,
IFCHG.OldIntPayFrqncy,
IFCHG.OldIntPayDate1,
IFCHG.OldIntPayDate2,
IFCHG.OldIntPayDate3,
IFCHG.OldIntPayDate4,
IFCHG.NewIntPayFrqncy,
IFCHG.NewIntPayDate1,
IFCHG.NewIntPayDate2,
IFCHG.NewIntPayDate3,
IFCHG.NewIntPayDate4,
IFCHG.Eventtype
FROM IFCHG
inner join bond on IFCHG.secid = bond.secid
where 
(IFCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('INCHG') as TableName,
INCHG.Actflag,
INCHG.AnnounceDate,
INCHG.Acttime,
INCHG.InChgID,
INCHG.IssID,
INCHG.InChgDate,
INCHG.OldCntryCD,
INCHG.NewCntryCD,
INCHG.EventType 
FROM INCHG
inner join scmst on INCHG.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where 
(INCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('INT') as TableName,
INT.Actflag,
INT.AnnounceDate,
INT.Acttime,
INT.RdID,
INT.InterestFromDate,
INT.InterestToDate,
INT.Days,
' ' as InterestDefault,
INT.InDefPay,
INT.INTNotes as Notes
FROM INT
INNER JOIN RD ON INT.RDID = RD.RDID
where 
(INT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or rd.secid in (select secid from portfolio.dbo.pushsap)


--# 
use wca
SELECT 
upper('INTBC') as TableName,
INTBC.Actflag,
INTBC.AnnounceDate,
INTBC.Acttime,
INTBC.IntbcID,
INTBC.SecID,
INTBC.EffectiveDate,
INTBC.OldIntBasis,
INTBC.NewIntBasis,
INTBC.OldIntBDC,
INTBC.NewIntBDC
FROM INTBC
where 
(INTBC.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('INTPY') as TableName,
INTPY.Actflag,
INTPY.AnnounceDate,
INTPY.Acttime, 
INTPY.RDID,
case when intpy.optionid is null then '1' else intpy.optionid end as OptionID,
INTPY.CurenCD,
' ' as InterestPercent,
INTPY.GrossInterest,
INTPY.NetInterest,
INTPY.DomesticTaxRate,
INTPY.NonResidentTaxRate,
INTPY.RescindInterest,
INTPY.AgencyFees,
INTPY.CouponNo,
null as CouponID,
INTPY.bCurenCD,
INTPY.bParValue,
INTPY.IntRate,
INTPY.DefaultOpt,
INTPY.OptElectionDate,
INTPY.AnlCoupRate,
INTPY.IntType,
INTPY.SectyCD as ResSectyCD,
INTPY.ResSecID,
INTPY.RatioNew,
INTPY.RatioOld,
INTPY.Fractions,
INTPY.RescindStockInterest
FROM INTPY
INNER JOIN RD ON INTPY.RDID = RD.RDID
where 
(INTPY.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or rd.secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('IRCHG') as TableName,
IRCHG.Actflag,
IRCHG.AnnounceDate,
IRCHG.Acttime,
IRCHG.IRChgID,
IRCHG.SecID,
IRCHG.EffectiveDate,
IRCHG.OldInterestRate,
IRCHG.NewInterestRate,
IRCHG.Eventtype,
IRCHG.Notes 
FROM IRCHG
where 
(IRCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('ISCHG') as TableName,
ISCHG.Actflag,
ISCHG.AnnounceDate,
ISCHG.Acttime,
ISCHG.IsChgID,
ISCHG.IssID,
ISCHG.NameChangeDate,
ISCHG.IssOldName,
ISCHG.IssNewName,
ISCHG.EventType ,
ISCHG.LegalName,
ISCHG.MeetingDateFlag, 
ISCHG.IsChgNotes as Notes
FROM ISCHG
where 
ISCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)


--# 
use WCA
SELECT distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
'' as HOAdd1,
'' as HOAdd2,
'' as HOAdd3,
'' as HOAdd4,
'' as HOAdd5,
'' as HOAdd6,
'' as HOCity,
'' as HOCntryCD,
'' as HOTel,
'' as HOFax,
'' as HOEmail,
'' as ROAdd1,
'' as ROAdd2,
'' as ROAdd3,
'' as ROAdd4,
'' as ROAdd5,
'' as ROAdd6,
'' as ROCity,
'' as ROCntryCD,
'' as ROTel,
'' as ROFax,
'' as ROEmail,
'' as website,
'' as Chairman,
'' as MD,
'' as CS,
ISSUR.Shortname,
ISSUR.LegalName,
'' as Notes
FROM ISSUR
INNER JOIN scmst ON ISSUR.IssID = SCMST.IssID
INNER JOIN bond ON SCMST.SecID = BOND.SecID
where 
(ISSUR.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('LAWST') as TableName,
LAWST.Actflag,
LAWST.AnnounceDate,
LAWST.Acttime,
LAWST.LawstID,
LAWST.IssID,
LAWST.EffectiveDate,
LAWST.LAType,
LAWST.Regdate,
LAWST.LawstNotes as Notes
FROM LAWST
where 
LAWST.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)


--# 
use WCA
SELECT 
upper('LCC') as TableName,
LCC.Actflag,
LCC.AnnounceDate,
LCC.Acttime,
LCC.LccID,
LCC.SecID,
LCC.ExchgCD,
LCC.EffectiveDate,
LCC.OldLocalCode,
LCC.NewLocalCode,
LCC.EventType ,
LCC.RelEventID
FROM LCC
inner join bond on lcc.secid = bond.secid
where 
(LCC.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('LIQ') as TableName,
LIQ.Actflag,
LIQ.AnnounceDate,
LIQ.Acttime,
LIQ.LiqID,
LIQ.IssID,
LIQ.Liquidator,
LIQ.LiqAdd1,
LIQ.LiqAdd2,
LIQ.LiqAdd3,
LIQ.LiqAdd4,
LIQ.LiqAdd5,
LIQ.LiqAdd6,
LIQ.LiqCity,
LIQ.LiqCntryCD,
LIQ.LiqTel,
LIQ.LiqFax,
LIQ.LiqEmail,
LIQ.RdDate,
LIQ.LiquidationTerms
FROM LIQ
where 
LIQ.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)


--# 
use WCA
SELECT 
upper('LSTAT') as TableName,
LSTAT.Actflag,
LSTAT.AnnounceDate,
LSTAT.Acttime,
LSTAT.LstatID,
LSTAT.SecID,
LSTAT.ExchgCD,
LSTAT.NotificationDate,
LSTAT.EffectiveDate,
LSTAT.LStatStatus,
LSTAT.EventType ,
LSTAT.Reason
FROM LSTAT
inner join bond on lstat.secid = bond.secid
where 
(LSTAT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('LTCHG') as TableName,
LTCHG.Actflag,
LTCHG.AnnounceDate,
LTCHG.Acttime,
LTCHG.LtChgID,
LTCHG.SecID,
LTCHG.ExchgCD,
LTCHG.EffectiveDate,
LTCHG.OldLot,
LTCHG.OldMinTrdQty,
LTCHG.NewLot,
LTCHG.NewMinTrdgQty
FROM LTCHG
inner join bond on LTCHG.secid = bond.secid
where 
(LTCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('MPAY') as TableName,
MPAY.Actflag,
MPAY.AnnounceDate,
MPAY.Acttime,
MPAY.sEvent as EventType,
MPAY.EventID,
MPAY.OptionID,
MPAY.SerialID,
MPAY.SectyCD as ResSectyCD,
MPAY.ResSecID,
MPAY.RatioNew,
MPAY.RatioOld,
MPAY.Fractions,
MPAY.MinOfrQty,
MPAY.MaxOfrQty,
MPAY.MinQlyQty,
MPAY.MaxQlyQty,
MPAY.Paydate,
MPAY.CurenCD,
MPAY.MinPrice,
MPAY.MaxPrice,
MPAY.TndrStrkPrice,
MPAY.TndrStrkStep,
MPAY.Paytype,
MPAY.DutchAuction,
MPAY.DefaultOpt,
MPAY.OptElectionDate
FROM MPAY
where 
actflag = 'Z'


--# 
use WCA
SELECT 
upper('MRGR') as TableName,
MRGR.Actflag,
MRGR.AnnounceDate,
MRGR.Acttime,
MRGR.RdID,
MRGR.EffectiveDate,
MRGR.AppointedDate,
MRGR.MrgrStatus,
MRGR.Companies,
MRGR.MrgrTerms, 
MRGR.ApprovalStatus
FROM MRGR
inner join rd on MRGR.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(MRGR.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('MTCHG') as TableName,
MTCHG.Actflag,
MTCHG.AnnounceDate,
MTCHG.Acttime,
MTCHG.MtChgID,
MTCHG.secID,
MTCHG.NotificationDate,
MTCHG.OldMaturityDate,
MTCHG.NewMaturityDate,
MTCHG.Reason,
MTCHG.EventType ,
MTCHG.OldMaturityBenchmark,
MTCHG.NewMaturityBenchmark,
MTCHG.Notes as Notes
FROM MTCHG
where
(MTCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('NLIST') as TableName,
NLIST.Actflag,
NLIST.AnnounceDate, 
NLIST.Acttime,
NLIST.ScexhID
FROM NLIST
inner join scexh on NLIST.scexhid = scexh.scexhid
inner join bond on scexh.secid = bond.secid
where 
(NLIST.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use wca
SELECT 
upper('ODDLT') as TableName,
ODDLT.Actflag,
ODDLT.AnnounceDate,
ODDLT.Acttime,  
ODDLT.RdID, 
ODDLT.StartDate, 
ODDLT.EndDate,
ODDLT.MinAcpQty, 
ODDLT.MaxAcpQty, 
ODDLT.BuyIn, 
ODDLT.BuyInCurenCD, 
ODDLT.BuyInPrice, 
ODDLT.Notes
FROM ODDLT
inner join rd on ODDLT.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(ODDLT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('PVRD') as TableName,
PVRD.Actflag,
PVRD.AnnounceDate,
PVRD.Acttime,
PVRD.PvRdID,
PVRD.SecID,
PVRD.EffectiveDate,
PVRD.CurenCD,
PVRD.OldParValue,
PVRD.NewParValue,
PVRD.EventType ,
PVRD.PvRdNotes as Notes
FROM PVRD
inner join bond on pvrd.secid = bond.secid
where 
(PVRD.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('RCONV') as TableName,
RCONV.Actflag,
RCONV.AnnounceDate,
RCONV.Acttime,
RCONV.RconvID,
RCONV.SECID,
RCONV.EffectiveDate,
RCONV.OldInterestAccrualConvention,
RCONV.NewInterestAccrualConvention,
RCONV.OldConvMethod,
RCONV.NewConvMethod,
RCONV.Eventtype,
RCONV.Notes as Notes
FROM RCONV
inner join bond on RCONV.secid = bond.secid
where 
(RCONV.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('RD') as TableName,
RD.Actflag, 
RD.AnnounceDate,
RD.Acttime,
RD.RdID,
RD.SecID,
RD.Recdate as FromDate,
RD.ToDate,
RD.RegistrationDate,
RD.SectyCD as ResSectyCD,
RD.RecCalc,
RD.RDNotes as Notes 
FROM RD
inner join bond on rd.secid = bond.secid
where 
(RD.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('RDNOM') as TableName,
RDNOM.Actflag,
RDNOM.AnnounceDate,
RDNOM.Acttime, 
RDNOM.RdnomID,
RDNOM.SecID,
RDNOM.EffectiveDate,
RDNOM.OldDenomination1,
RDNOM.OldDenomination2,
RDNOM.OldDenomination3,
RDNOM.OldDenomination4,
RDNOM.OldDenomination5,
RDNOM.OldDenomination6,
RDNOM.OldDenomination7,
RDNOM.OldMinimumDenomination,
RDNOM.OldDenominationMultiple,
RDNOM.NewDenomination1,
RDNOM.NewDenomination2,
RDNOM.NewDenomination3,
RDNOM.NewDenomination4,
RDNOM.NewDenomination5,
RDNOM.NewDenomination6,
RDNOM.NewDenomination7,
RDNOM.NewMinimumDenomination,
RDNOM.NewDenominationMultiple,
RDNOM.Notes as Notes
FROM RDNOM
inner join bond on RDNOM.secid = bond.secid
where 
(RDNOM.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('RDPRT') as TableName,
RDPRT.Actflag, 
RDPRT.AnnounceDate,
RDPRT.Acttime,
RDPRT.RdID,
RDPRT.EventType,
RDPRT.Priority
FROM RDPRT
inner join rd on RDPRT.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(RDPRT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('REDEM') as TableName,
REDEM.Actflag,
REDEM.AnnounceDate,
REDEM.Acttime,
REDEM.RedemID,
REDEM.SecID,
REDEM.RedemDate,
REDEM.CurenCD,
REDEM.RedemPrice,
REDEM.MandOptFlag,
REDEM.PartFinal,
REDEM.AmountRedeemed,
REDEM.RedemPremium,
REDEM.RedemPercent,
REDEM.RedemType,
' ' AS RedemDefault,
-- redem.poolfactor,
CASE WHEN CHARINDEX('.',redem.poolfactor) < 5
                THEN substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+9)
                ELSE substring(redem.poolfactor,1,CHARINDEX('.',redem.poolfactor)+8)
                END AS poolfactor,
REDEM.Rdid,
REDEM.PriceAsPercent,
REDEM.PremiumAsPercent,
REDEM.InDefPay,
REDEM.RedemNotes as Notes 
FROM REDEM
inner join bond on REDEM.secid = bond.secid
where 
(REDEM.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('REDMT') as TableName,
REDMT.Actflag,
REDMT.AnnounceDate,
REDMT.Acttime,
REDMT.RedmtID,
REDMT.SecID,
REDMT.RedemptionDate as RedemDate,
REDMT.CurenCD,
REDMT.RedemptionPrice as RedemPrice,
REDMT.MandOptFlag,
REDMT.PartFinal,
REDMT.RedemptionType as RedemType,
REDMT.RedemptionAmount as AmountRedeemed,
REDMT.RedemptionPremium as RedemPremium,
' ' as RedemPercent,
REDMT.PriceAsPercent,
REDMT.PremiumAsPercent,
REDMT.RedmtNotes as Notes
FROM REDMT
inner join bond on REDMT.secid = bond.secid
where 
(REDMT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)



--# 
use WCA
SELECT 
upper('ROCHG') as TableName,
ROCHG.Actflag,
ROCHG.AnnounceDate,ROCHG.Acttime,
ROCHG.ROCHGID, 
ROCHG.IssID,
ROCHG.DateofChange,
ROCHG.OldHOAdd1,
ROCHG.OldHOAdd2,
ROCHG.OldHOAdd3,
ROCHG.OldHOAdd4,
ROCHG.OldHOAdd5,
ROCHG.OldHOAdd6,
ROCHG.OldHOCity,
ROCHG.OldHOCntryCD,
ROCHG.OldHOTel,
ROCHG.OldHOFax,
ROCHG.OldHOEmail,
ROCHG.OldROAdd1,
ROCHG.OldROAdd2,
ROCHG.OldROAdd3,
ROCHG.OldROAdd4,
ROCHG.OldROAdd5,
ROCHG.OldROAdd6,
ROCHG.OldROCity,
ROCHG.OldROCntryCD,
ROCHG.OldROTel,
ROCHG.OldROFax,
ROCHG.OldROEmail,
ROCHG.OldWebsite,
ROCHG.OldChairman,
ROCHG.OldMD,
ROCHG.OldCS,
ROCHG.NewHOAdd1,
ROCHG.NewHOAdd2,
ROCHG.NewHOAdd3,
ROCHG.NewHOAdd4,
ROCHG.NewHOAdd5,
ROCHG.NewHOAdd6,
ROCHG.NewHOCity,
ROCHG.NewHOCntryCD,
ROCHG.NewHOTel,
ROCHG.NewHOFax,
ROCHG.NewHOEmail,
ROCHG.NewROAdd1,
ROCHG.NewROAdd2,
ROCHG.NewROAdd3,
ROCHG.NewROAdd4,
ROCHG.NewROAdd5,
ROCHG.NewROAdd6,
ROCHG.NewROCity,
ROCHG.NewROCntryCD,
ROCHG.NewROTel,
ROCHG.NewROFax,
ROCHG.NewROEmail,
ROCHG.NewWebsite,
ROCHG.NewChairman,
ROCHG.NewMD,
ROCHG.NewCS 
FROM ROCHG
where 
ROCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)



--# 
use WCA
SELECT 
upper('SACHG') as TableName,
SACHG.Actflag,
SACHG.AnnounceDate,
SACHG.Acttime,
SACHG.SachgID,
SACHG.ScagyID,
SACHG.EffectiveDate,
SACHG.OldRelationship,
SACHG.NewRelationship,
SACHG.OldAgncyID,
SACHG.OldSpStartDate,
SACHG.NewSpStartDate,
SACHG.OldSpEndDate,
SACHG.NewSpEndDate,
SACHG.OldGuaranteeType,
SACHG.NewGuaranteeType
FROM SACHG
where 
SACHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3)



--# 
use WCA
SELECT 
upper('SCAGY') as TableName,
SCAGY.Actflag,
SCAGY.AnnounceDate,
SCAGY.Acttime,
SCAGY.ScagyID,
SCAGY.SecID,
SCAGY.Relationship,
SCAGY.AgncyID,
SCAGY.GuaranteeType,
SCAGY.SpStartDate,
SCAGY.SpEndDate,
SCAGY.Notes
FROM SCAGY
where 
(SCAGY.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('SCCHG') as TableName,
SCCHG.Actflag,
SCCHG.AnnounceDate,
SCCHG.Acttime,
SCCHG.ScChgID,
SCCHG.SecID,
SCCHG.DateofChange,
SCCHG.SecOldName,
SCCHG.SecNewName,
SCCHG.EventType,
SCCHG.OldSectyCD,
SCCHG.NewSectyCD,
SCCHG.OldRegS144A,
SCCHG.NewRegS144A,
SCCHG.ScChgNotes as Notes
FROM SCCHG
inner join bond on SCCHG.secid = bond.secid
where 
(SCCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)



--# 
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
SCEXH.ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
null as TradeStatus,
SCEXH.LocalCode,
'' as RTSCD,
'' AS BLTICK,
SCEXH.ScexhNotes as Notes
FROM SCEXH
inner join bond on SCEXH.secid = bond.secid
where 
(SCEXH.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate,
SCMST.Acttime,
SCMST.SecID,
SCMST.ParentSecID,
SCMST.IssID,
SCMST.SectyCD,
SCMST.SecurityDesc,
SCMST.Statusflag,
SCMST.StatusReason,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.IssuePrice,
SCMST.PaidUpValue,
SCMST.Voting,
SCMST.FYENPPDate,
SCMST.USCode,
SCMST.ISIN,
SCMST.VALOREN,
SCMST.X as CommonCode,
SCMST.CFI,
SCMST.SharesOutstanding,
SCMST.UmprgID,
SCMST.Holding,
SCMST.NoParValue,
SCMST.REGS144A, 
SCMST.ScmstNotes as Notes
FROM SCMST
inner join bond on scmst.secid = bond.secid
where 
(
(SCMST.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or (BOND.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
)
or bond.secid in (select secid from portfolio.dbo.pushsap)



--# 
use WCA
SELECT 
upper('SCSWP') as TableName,
SCSWP.Actflag,
SCSWP.AnnounceDate,
SCSWP.Acttime,
SCSWP.RdID,
SCSWP.OldRatio as RatioOld,
SCSWP.NewRatio as RatioNew,
SCSWP.Fractions,
SCSWP.ResSecID,
SCSWP.SectyCD as ResSectyCD,
SCSWP.ScSwpNotes as Notes
FROM SCSWP
inner join rd on SCSWP.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
where 
(SCSWP.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('SDCHG') as TableName,
SDCHG.Actflag,
SDCHG.AnnounceDate, 
SDCHG.Acttime,
SDCHG.SdChgID,
SDCHG.SecID,
SDCHG.CntryCD as OldCntryCD,
SDCHG.EffectiveDate,
SDCHG.OldSEDOL,
SDCHG.NewSEDOl,
SDCHG.EventType ,
SDCHG.RcntryCD as OldRcntryCD,
SDCHG.RelEventID,
SDCHG.NewCntryCD,
SDCHG.NewRcntryCD,
SDCHG.SdChgNotes as Notes
FROM SDCHG
inner join bond on SDCHG.secid = bond.secid
where 
(SDCHG.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('SECRC') as TableName,
SECRC.Actflag,
SECRC.AnnounceDate,
SECRC.Acttime,
SECRC.SecRcID,
SECRC.SecID as OldSecID,
SECRC.EffectiveDate,
SECRC.ResSecID as NewSecID,
SECRC.EventType ,
SECRC.SectyCD as ResSectyCD,
SECRC.RatioOld,
SECRC.RatioNew,
SECRC.SecRcNotes as Notes
FROM SECRC
inner join bond on SECRC.secid = bond.secid
where 
(SECRC.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)

--# 
use WCA
SELECT 
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD 
FROM SEDOL
inner join bond on SEDOL.secid = bond.secid
where 
(SEDOL.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)

--# 
use wca
SELECT 
upper('SELRT') as TableName,
SELRT.Actflag,
SELRT.AnnounceDate,
SELRT.Acttime,
SELRT.SelrtID,
SELRT.SecID,
SELRT.CntryCD,
SELRT.Restriction
FROM SELRT
where 
(SELRT.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('TKOVR') as TableName,
TKOVR.Actflag,
TKOVR.AnnounceDate,
TKOVR.Acttime,
TKOVR.TkovrID,
TKOVR.RdID,
TKOVR.SecID,
TKOVR.OfferorIssID,
TKOVR.OfferorName,
TKOVR.Hostile,
TKOVR.OpenDate,
TKOVR.CloseDate,
TKOVR.TkovrStatus,
TKOVR.PreOfferQty,
TKOVR.PreOfferPercent,
TKOVR.TargetQuantity,
TKOVR.TargetPercent,
TKOVR.UnconditionalDate,
TKOVR.CmAcqDate,
TKOVR.MinAcpQty,
TKOVR.MaxAcpQty,
TKOVR.MiniTkovr,
TKOVR.TkovrNotes as Notes
FROM TKOVR
where 
1=2


--# 
use WCA
SELECT 
upper('TRNCH') as TableName,
TRNCH.Actflag,
TRNCH.AnnounceDate,
TRNCH.Acttime,
TRNCH.SecId,
TRNCH.TrnchNumber,
TRNCH.TrancheDate,
TRNCH.TrancheAmount,
TRNCH.ExpiryDate,
TRNCH.Notes as Notes
FROM TRNCH
inner join bond on TRNCH.secid = bond.secid
where 
(TRNCH.acttime between (select max(feeddate) from tbl_opslog where seq = 3)
and (select max(feeddate)+1 from tbl_opslog where seq = 3))
or bond.secid in (select secid from portfolio.dbo.pushsap)


--# 
use WCA
SELECT 
upper('UMCHG') as TableName,
UMCHG.Actflag,
UMCHG.AnnounceDate,
UMCHG.Acttime,
UMCHG.UmChID,
UMCHG.UmbrellaProgrammeID,
UMCHG.EffectiveDate,
UMCHG.Event,
UMCHG.OldUmbrellaProgrammeName,
UMCHG.NewUmbrellaProgrammeName,
UMCHG.Notes as Notes
FROM UMCHG
where 
1=2


--# 
use WCA
SELECT 
upper('UMPRG') as TableName,
UMPRG.Actflag,
UMPRG.AnnounceDate,
UMPRG.Acttime,
UMPRG.UmPrgID,
UMPRG.UmbProgName
FROM UMPRG
where 
1=2

--# 
use WCA
SELECT 
upper('UMTRF') as TableName,
UMTRF.Actflag,
UMTRF.AnnounceDate,
UMTRF.Acttime,
UMTRF.UmtrfID,
UMTRF.SecID,
UMTRF.EffectiveDate,
UMTRF.OldUmPrgID,
UMTRF.NewUmPrgID,
UMTRF.EventType,
UMTRF.Notes
FROM UMTRF
where 
1=2