--filepath=o:\Datafeed\Debt\Markit_US\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.txt
--suffix=_LOOKUP
--fileheadertext=EDI_DEBT_Lookup_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_US\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
FROM LOOKUPEXTRA
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
Exchg.Actflag,
Exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.Exchgname as Lookup
from EXCHG
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
Exchg.Actflag,
Exchg.Acttime,
upper('MICCODE') as TypeGroup,
Exchg.MIC as Code,
Exchg.Exchgname as Lookup
from EXCHG
where MIC <> '' and MIC is not null
and acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
Exchg.Actflag,
Exchg.Acttime,
upper('MICMAP') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.MIC as Lookup
from EXCHG
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
cast(Indus.IndusID as varchar(10)) as Code,
Indus.IndusName as Lookup
from INDUS
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
Cntry.Actflag,
Cntry.Acttime,
upper('CNTRY') as TypeGroup,
Cntry.CntryCD as Code,
Cntry.Country as Lookup
from CNTRY
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
Curen.Actflag,
Curen.Acttime,
upper('CUREN') as TypeGroup,
Curen.CurenCD as Code,
Curen.Currency as Lookup
from CUREN
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
Event.Actflag,
Event.Acttime,
upper('EVENT') as TypeGroup,
Event.EventType as Code,
Event.EventName as Lookup
from EVENT
inner join bond on v51f_920_Bond_Static_Change.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
acttime > (select max(feeddate) from tbl_Opslog where seq = 1)

union

select *
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2
and 1=2

order by TypeGroup, Code
