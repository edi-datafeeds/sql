--filepath=o:\Datafeed\Debt\Markit_US\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.txt
--suffix=_CPOPT
--fileheadertext=EDI_DEBT_Schedule_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\Markit_US\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
use wca
select
'CPOPT' as sEvent,
CPOPT.CpoptID as EventID,
v20c_920_BOND.Sedol,
CPOPT.AnnounceDate as Created,
CPOPT.Acttime as Changed,
CPOPT.Actflag,
v20c_920_BOND.IssID,
v20c_920_BOND.SecID,
v20c_920_BOND.CntryofIncorp,
v20c_920_BOND.IssuerName,
v20c_920_BOND.SecurityDesc,
v20c_920_BOND.ISIN,
v20c_920_BOND.USCode,
v20c_920_BOND.SecurityStatus,
v20c_920_BOND.PrimaryExchgCD,
v20c_920_BOND.ExCountry,
v20c_920_BOND.RegCountry,
v20c_920_BOND.Bondtype,
v20c_920_BOND.MaturityDate,
v20c_920_BOND.MaturityExtendible,
v20c_920_BOND.IssueCurrency,
v20c_920_BOND.IssueDate,
v20c_920_BOND.NominalValue,
v20c_920_BOND.DebtCurrency,
CPOPT.Currency as ScheduleCurrency,
CPOPT.Price,
CPOPT.CallPut,
CPOPT.FromDate,
CPOPT.ToDate,
CPOPT.NoticeFrom,
CPOPT.NoticeTo,
CPOPT.MandatoryOptional,
CPOPT.MinNoticeDays,
CPOPT.MaxNoticeDays,
case when CPOPT.cptype='KO' then 'KO'
     when portfolio.dbo.CPOPT_Count.ct>1 then 'BM'
     when portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is not null then 'US'
     when portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is null then 'EU'
     else CPOPT.CPType end as cptype,
CPOPT.PriceAsPercent,
CPOPT.Notes
from issur
INNER JOIN v20c_920_BOND ON issur.issid = v20c_920_BOND.IssID
inner join cpopt on v20c_920_BOND.SecID = cpopt.SecID
left outer join portfolio.dbo.CPOPT_Count on cpopt.secid = portfolio.dbo.CPOPT_Count.secid
inner join bond on v20c_920_BOND.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
cpopt.acttime  > (select max(feeddate) from tbl_Opslog where seq = 1)
and issur.isstype<>'GOV' 
and issur.isstype<>'GOVAGENCY'
and issur.cntryofincorp<>'AA'
and bond.municipal<>'Y'

