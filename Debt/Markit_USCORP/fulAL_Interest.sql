--filepath=o:\Datafeed\Debt\Markit_US\Full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_DEBT_Interest_Payment_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Debt\Markit_US\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 1
USE WCA
select *
FROM v54f_920_Interest_Payment
inner join issur on v54f_920_Interest_Payment.issid =issur.issid
inner join bond on v54f_920_Interest_Payment.secid = bond.secid
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
v54f_920_Interest_Payment.actflag<>'D'
and issur.isstype<>'GOV' 
and issur.isstype<>'GOVAGENCY'
and issur.cntryofincorp<>'AA'
and bond.municipal<>'Y'
and BOND.MaturityDate>getdate()-31
