--filepath=o:\Datafeed\Debt\Markit_US\Full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_AGNCY
--fileheadertext=EDI_DEBT_AGNCY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Debt\Markit_US\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select
upper('AGNCY') as TableName,
AGNCY.Actflag,
AGNCY.AnnounceDate,
AGNCY.Acttime,
AGNCY.AgncyID,
AGNCY.RegistrarName,
AGNCY.Add1,
AGNCY.Add2,
AGNCY.Add3,
AGNCY.Add4,
AGNCY.Add5,
AGNCY.Add6,
AGNCY.City,
AGNCY.CntryCD,
AGNCY.Website,
AGNCY.Contact1,
AGNCY.Tel1,
AGNCY.Fax1,
AGNCY.Email1,
AGNCY.Contact2,
AGNCY.Tel2,
AGNCY.Fax2,
AGNCY.Email2,
AGNCY.Depository
FROM AGNCY
INNER JOIN SCAGY ON AGNCY.AgncyID = SCAGY.AgncyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
inner join scexh on bond.secid=scexh.secid and 'D'<>scexh.liststatus and 'US'=substring(scexh.exchgcd,1,2)
where
issur.isstype<>'GOV' 
and issur.isstype<>'GOVAGENCY'
and issur.cntryofincorp<>'AA'
and bond.municipal<>'Y'
and AGNCY.actflag<>'D'
and SCAGY.actflag<>'D'
and scmst.actflag<>'D'
and bond.actflag<>'D'
and (scmst.statusflag<>'I' or scmst.statusflag is null)
and BOND.MaturityDate>getdate()-31
