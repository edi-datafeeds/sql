--filepath=o:\Datafeed\Debt\Markit_US\Full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LOOKUP
--fileheadertext=EDI_DEBT_Lookup_AL
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Debt\Markit_US\Lookup\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
FROM LOOKUPEXTRA

union

select *
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP

union

select *
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD

union

select *
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY

union

select *
Exchg.Actflag,
Exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.Exchgname as Lookup
from EXCHG

union

select *
Exchg.Actflag,
Exchg.Acttime,
upper('MICCODE') as TypeGroup,
Exchg.MIC as Code,
Exchg.Exchgname as Lookup
from EXCHG
where MIC <> '' and MIC is not null

union

select *
Exchg.Actflag,
Exchg.Acttime,
upper('MICMAP') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.MIC as Lookup
from EXCHG

union

select *
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
cast(Indus.IndusID as varchar(10)) as Code,
Indus.IndusName as Lookup
from INDUS

union

select *
Cntry.Actflag,
Cntry.Acttime,
upper('CNTRY') as TypeGroup,
Cntry.CntryCD as Code,
Cntry.Country as Lookup
from CNTRY

union

select *
Curen.Actflag,
Curen.Acttime,
upper('CUREN') as TypeGroup,
Curen.CurenCD as Code,
Curen.Currency as Lookup
from CUREN

union

select *
Event.Actflag,
Event.Acttime,
upper('EVENT') as TypeGroup,
Event.EventType as Code,
Event.EventName as Lookup
from EVENT

union

select *
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2

order by TypeGroup, Code
