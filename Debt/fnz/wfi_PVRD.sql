--filepath=o:\datafeed\debt\fnz\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\fnz\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('PVRD') as TableName,
PVRD.Actflag,
PVRD.AnnounceDate as Created,
PVRD.Acttime as Changed,
PVRD.PvRdID,
PVRD.SecID,
SCMST.ISIN,
PVRD.EffectiveDate,
PVRD.CurenCD,
PVRD.OldParValue,
PVRD.NewParValue,
PVRD.EventType,
PVRD.PvRdNotes as Notes
FROM PVRD
INNER JOIN BOND ON PVRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='I')
or (scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='U')
     and PVRD.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
