--filepath=o:\datafeed\debt\fnz\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\datafeed\debt\fnz\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LAWSTNOTES') as TableName,
LAWST.Actflag,
LAWST.LawstID,
LAWST.LawstNotes as Notes
FROM LAWST
where
LAWST.Issid in (select wca.dbo.scmst.issid from portfolio.dbo.fisin
inner join wca.dbo.scmst on portfolio.dbo.fisin.code = wca.dbo.scmst.isin 
       and accid=209 and portfolio.dbo.actflag='I')
or
(LAWST.Issid in (select wca.dbo.scmst.issid from portfolio.dbo.fisin
inner join wca.dbo.scmst on portfolio.dbo.fisin.code = wca.dbo.scmst.isin 
       and accid=209 and portfolio.dbo.actflag='U')
and LAWST.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
