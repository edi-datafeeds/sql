--filepath=o:\datafeed\debt\fnz\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=EDI_LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\fnz\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('LTCHG') as TableName,
LTCHG.Actflag,
LTCHG.AnnounceDate as Created,
LTCHG.Acttime as Changed,
LTCHG.LtChgID,
SCMST.SecID,
SCMST.ISIN,
LTCHG.ExchgCD,
LTCHG.EffectiveDate,
LTCHG.OldLot,
LTCHG.OldMinTrdQty,
LTCHG.NewLot,
LTCHG.NewMinTrdgQty
FROM LTCHG
INNER JOIN BOND ON LTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='I')
or (scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='U')
     and LTCHG.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
