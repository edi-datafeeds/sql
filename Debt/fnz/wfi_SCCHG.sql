--filepath=o:\datafeed\debt\fnz\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\fnz\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCCHG') as TableName,
SCCHG.Actflag,
SCCHG.AnnounceDate as Created,
SCCHG.Acttime as Changed,
SCCHG.ScChgID,
SCCHG.SecID,
SCMST.ISIN,
SCCHG.DateofChange,
SCCHG.SecOldName,
SCCHG.SecNewName,
SCCHG.EventType,
SCCHG.OldSectyCD,
SCCHG.NewSectyCD,
SCCHG.OldRegS144A,
SCCHG.NewRegS144A,
SCCHG.ScChgNotes as Notes
FROM SCCHG
INNER JOIN BOND ON SCCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='I')
or (scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='U')
     and SCCHG.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
