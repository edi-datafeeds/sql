--filepath=o:\datafeed\debt\fnz\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\debt\fnz\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate as Created,
SCEXH.Acttime as Changed,
SCEXH.ScExhID,
SCEXH.SecID,
SCMST.ISIN,
SCEXH.ExchgCD,
SCEXH.ListStatus,
SCEXH.Lot,
scexh.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
SUBSTRING(SCEXH.JunkLocalcode,0,50) as JunkLocalcode
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='I')
or (scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='U')
     and SCEXH.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
