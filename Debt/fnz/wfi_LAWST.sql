--filepath=o:\datafeed\debt\fnz\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_LAWST
--fileheadertext=EDI_LAWST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\datafeed\debt\fnz\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 
use WCA
select
upper('LAWST') as TableName,
LAWST.Actflag,
LAWST.AnnounceDate as Created,
LAWST.Acttime as Changed,
LAWST.LawstID,
BOND.SecID,
SCMST.ISIN,
LAWST.IssID,
LAWST.EffectiveDate,
LAWST.LAType,
LAWST.Regdate
FROM LAWST
INNER JOIN SCMST ON LAWST.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='I')
or (scmst.isin in (select code from portfolio.dbo.fisin where accid = 209 and actflag ='U')
     and LAWST.acttime> (select max(feeddate)-0.1 from tbl_Opslog where seq = 3))
