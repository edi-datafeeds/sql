--filepath=o:\datafeed\debt\fnz\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_FIXED_INTEREST
--fileheadertext=EDI_FIXED_INTEREST_HISTORY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select distinct portfolio.dbo.intfix_sample.* from portfolio.dbo.intfix_sample
left outer join v54f_920_interest_payment ON portfolio.dbo.intfix_sample.secid = v54f_920_interest_payment.secid
                 and payable_date = paydate
where
v54f_920_interest_payment.secid is null
