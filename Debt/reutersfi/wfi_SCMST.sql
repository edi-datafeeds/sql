--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate as Created,
SCMST.Acttime as Changed,
SCMST.SecID,
SCMST.ISIN,
SCMST.IssID,
SCMST.SectyCD,
SCMST.SecurityDesc,
case when SCMST.Statusflag<>'I' or SCMST.Statusflag is null then 'A' ELSE 'I' end as StatusFlag,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.X as CommonCode,
SCMST.Holding,
SCMST.StructCD,
SCMST.RegS144A
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='AU' or substring(exchgcd,1,2)='HK' or substring(exchgcd,1,2)='NZ' and actflag<>'D')
and scmst.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)