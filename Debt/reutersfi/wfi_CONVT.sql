--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_CONVT
--fileheadertext=EDI_CONVT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CONVT') as TableName,
CONVT.Actflag,
CONVT.AnnounceDate as Created,
CONVT.Acttime as Changed,
CONVT.ConvtID,
SCMST.SecID,
SCMST.ISIN,
CONVT.FromDate,
CONVT.ToDate,
CONVT.RatioNew,
CONVT.RatioOld,
CONVT.CurenCD,
CONVT.Price,
CONVT.MandOptFlag,
CONVT.Fractions,
CONVT.FXrate,
CONVT.PartFinalFlag,
CONVT.PriceAsPercent,
CONVT.SectyCD as ResSectyCD,
CONVT.ResSecID,
RESSCMST.ISIN as ResISIN,
RESISSUR.Issuername as ResIssuername,
RESSCMST.SecurityDesc as ResSecurityDesc,
CONVT.ConvtNotes as Notes
FROM CONVT
INNER JOIN BOND ON convt.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as RESSCMST ON CONVT.ResSecID = RESSCMST.SecID
LEFT OUTER JOIN ISSUR as RESISSUR ON RESSCMST.IssID = RESISSUR.IssID
where
BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='AU' or substring(exchgcd,1,2)='HK' or substring(exchgcd,1,2)='NZ' and actflag<>'D')
and convt.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
