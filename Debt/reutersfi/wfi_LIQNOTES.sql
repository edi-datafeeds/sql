--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LIQNOTES') as TableName,
LIQ.Actflag,
LIQ.LiqID,
LIQ.LiquidationTerms
FROM LIQ
where
LIQ.Issid in (select wca.dbo.scmst.issid from wca.dbo.scmst
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid where
 substring(exchgcd,1,2)='AU' or substring(exchgcd,1,2)='HK' or substring(exchgcd,1,2)='NZ' and scexh.actflag<>'D')

and LIQ.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
