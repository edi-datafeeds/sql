--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LCC
--fileheadertext=EDI_LCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('LCC') as TableName,
LCC.Actflag,
LCC.AnnounceDate as Created,
LCC.Acttime as Changed,
LCC.LccID,
LCC.SecID,
SCMST.ISIN,
LCC.ExchgCD,
LCC.EffectiveDate,
LCC.OldLocalCode,
LCC.NewLocalCode,
LCC.EventType,
LCC.RelEventID
FROM LCC
INNER JOIN BOND ON LCC.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='AU' or substring(exchgcd,1,2)='HK' or substring(exchgcd,1,2)='NZ' and actflag<>'D')
and LCC.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'
