--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_IFCHG
--fileheadertext=EDI_IFCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('IFCHG') as TableName,
IFCHG.Actflag,
IFCHG.AnnounceDate as Created,
IFCHG.Acttime as Changed,
IFCHG.IfchgID,
IFCHG.SecID,
SCMST.ISIN,
IFCHG.NotificationDate,
IFCHG.OldIntPayFrqncy,
IFCHG.OldIntPayDate1,
IFCHG.OldIntPayDate2,
IFCHG.OldIntPayDate3,
IFCHG.OldIntPayDate4,
IFCHG.NewIntPayFrqncy,
IFCHG.NewIntPayDate1,
IFCHG.NewIntPayDate2,
IFCHG.NewIntPayDate3,
IFCHG.NewIntPayDate4,
IFCHG.Eventtype
FROM IFCHG
INNER JOIN BOND ON IFCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='AU' or substring(exchgcd,1,2)='HK' or substring(exchgcd,1,2)='NZ' and actflag<>'D')
and IFCHG.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and eventtype<>'CLEAN'
and eventtype<>'CORR'
