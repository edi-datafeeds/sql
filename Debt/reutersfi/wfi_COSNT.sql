--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_COSNT
--fileheadertext=EDI_COSNT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=o:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
'COSNT' as TableName,
COSNT.Actflag,
COSNT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > COSNT.Acttime) THEN RD.Acttime ELSE COSNT.Acttime END as [Changed],
COSNT.RdID,
SCMST.SecID,
SCMST.ISIN,
RD.Recdate,
COSNT.ExpiryDate,
COSNT.ExpiryTime,
SUBSTRING(cosnt.TimeZone, 1,3) AS TimeZone,
COSNT.CollateralRelease,
COSNT.Currency,
COSNT.Fee,
COSNT.Notes
FROM COSNT
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='AU' or substring(exchgcd,1,2)='HK' or substring(exchgcd,1,2)='NZ' and actflag<>'D')
and COSNT.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
