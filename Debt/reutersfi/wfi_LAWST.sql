--filepath=o:\datafeed\debt\wfisample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LAWST
--fileheadertext=EDI_LAWST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\datafeed\debt\wfisample\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 
use WCA
select
upper('LAWST') as TableName,
LAWST.Actflag,
LAWST.AnnounceDate as Created,
LAWST.Acttime as Changed,
LAWST.LawstID,
BOND.SecID,
SCMST.ISIN,
LAWST.IssID,
LAWST.EffectiveDate,
LAWST.LAType,
LAWST.Regdate
FROM LAWST
INNER JOIN SCMST ON LAWST.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='AU' or substring(exchgcd,1,2)='HK' or substring(exchgcd,1,2)='NZ' and actflag<>'D')
and LAWST.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
