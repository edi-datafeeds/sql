print " GENERATING portfolio.dbo.CPOPT_Count, please wait..."
go

use portfolio
if exists (select * from sysobjects where name = 'CPOPT_Count')
 drop table CPOPT_Count
use wca
select cpopt.secid, count(secid) as ct
into portfolio.dbo.CPOPT_Count
from cpopt
where
cpopt.actflag <> 'D'
GROUP BY cpopt.secid
order by count(secid)
go

print ""
go

print " INDEXING portfolio.dbo.CPOPT_Count ,please  wait ....."
GO 
use portfolio
ALTER TABLE CPOPT_Count ALTER COLUMN  secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_Count] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_Count] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 
