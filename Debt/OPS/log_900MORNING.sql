use wca
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = '900MORNING')
set @tdate = getdate()

if @fdate<@tdate-0.6
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( '900MORNING', @fdate, @tdate)
    print "Log record creation successful"
  end
else
  print "Already run in the last day !!!"
  go