--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate as Created,
ISSUR.Acttime as Changed,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.IndusID,
ISSUR.CntryofIncorp,
ISSUR.FinancialYearEnd,
ISSUR.Shortname,
ISSUR.LegalName,
CASE WHEN ISSUR.CntryofIncorp='AA' THEN 'S' WHEN ISSUR.Isstype='GOV' THEN 'G' WHEN ISSUR.Isstype='GOVAGENCY' THEN 'Y' ELSE 'C' END as Debttype
FROM ISSUR
INNER JOIN SCMST ON ISSUR.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and ISSUR.actflag<>'D'
