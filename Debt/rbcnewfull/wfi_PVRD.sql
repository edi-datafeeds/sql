--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('PVRD') as TableName,
PVRD.Actflag,
PVRD.AnnounceDate as Created,
PVRD.Acttime as Changed,
PVRD.PvRdID,
PVRD.SecID,
SCMST.ISIN,
PVRD.EffectiveDate,
PVRD.CurenCD,
PVRD.OldParValue,
PVRD.NewParValue,
PVRD.EventType,
PVRD.PvRdNotes as Notes
FROM PVRD
INNER JOIN BOND ON PVRD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and PVRD.actflag<>'D'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
