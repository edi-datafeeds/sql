--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_IRCHG
--fileheadertext=EDI_IRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('IRCHG') as TableName,
IRCHG.Actflag,
IRCHG.AnnounceDate as Created,
IRCHG.Acttime as Changed,
IRCHG.IrchgID,
IRCHG.SecID,
SCMST.ISIN,
IRCHG.EffectiveDate,
IRCHG.OldInterestRate,
IRCHG.NewInterestRate,
IRCHG.Eventtype,
IRCHG.Notes 
FROM IRCHG
INNER JOIN BOND ON IRCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and IRCHG.actflag<>'D'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
