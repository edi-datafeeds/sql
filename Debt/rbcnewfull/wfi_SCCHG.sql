--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_SCCHG
--fileheadertext=EDI_SCCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCCHG') as TableName,
SCCHG.Actflag,
SCCHG.AnnounceDate as Created,
SCCHG.Acttime as Changed,
SCCHG.ScChgID,
SCCHG.SecID,
SCMST.ISIN,
SCCHG.DateofChange,
SCCHG.SecOldName,
SCCHG.SecNewName,
SCCHG.EventType,
SCCHG.OldSectyCD,
SCCHG.NewSectyCD,
SCCHG.OldRegS144A,
SCCHG.NewRegS144A,
SCCHG.ScChgNotes as Notes
FROM SCCHG
INNER JOIN BOND ON SCCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and SCCHG.actflag<>'D'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
