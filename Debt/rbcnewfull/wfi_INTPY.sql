--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_INTPY
--fileheadertext=EDI_INTPY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select distinct
'INTPY' as TableName,
CASE WHEN INT.Actflag = 'D' or INT.actflag='C' or intpy.actflag is null THEN INT.Actflag ELSE INTPY.Actflag END as Actflag,
INT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > INT.Acttime) and (RD.Acttime > EXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > INT.Acttime) THEN EXDT.Acttime ELSE INT.Acttime END as [Changed],
SCMST.SecID,
SCMST.ISIN,
INTPY.BCurenCD as DebtCurrency,
INTPY.BParValue as NominalValue,
SCEXH.ExchgCD,
case when intpy.optionid is null then '1' else intpy.optionid end as OptionID,
INT.RdID,
RD.Recdate,
EXDT.Exdate,
EXDT.Paydate,
INT.InterestFromDate,
INT.InterestToDate,
INT.Days,
' ' as InterestDefault,
INTPY.CurenCD as Interest_Currency,
INTPY.IntRate as Interest_Rate,
INTPY.GrossInterest,
INTPY.NetInterest,
INTPY.DomesticTaxRate,
INTPY.NonResidentTaxRate,
INTPY.RescindInterest,
INTPY.AgencyFees,
INTPY.CouponNo,
null as CouponID,
INTPY.DefaultOpt,
INTPY.OptElectionDate,
INTPY.AnlCoupRate,
INT.InDefPay
from [INT]
INNER JOIN RD ON INT.RdID = RD.RdID
INNER JOIN BOND ON RD.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON BOND.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID 
     AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
     AND EXDT.actflag<>'D'
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and RD.ACTFLAG<>'d'
AND scmst.ACTFLAG<>'d'
AND bond.ACTFLAG<>'d'
AND issur.ACTFLAG<>'d'
AND int.ACTFLAG<>'d'
AND intpy.ACTFLAG<>'d'
