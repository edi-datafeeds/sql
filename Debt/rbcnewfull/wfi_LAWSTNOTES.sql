--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('LAWSTNOTES') as TableName,
LAWST.Actflag,
LAWST.LawstID,
LAWST.LawstNotes as Notes
FROM LAWST
where
LAWST.Issid in (select wca.dbo.scmst.issid from portfolio.dbo.wfisample
inner join wca.dbo.scmst on portfolio.dbo.wfisample.secid = wca.dbo.scmst.secid)
and LAWST.actflag<>'D'
