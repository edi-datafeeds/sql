--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT
upper('INCHG') as TableName,
INCHG.Actflag,
INCHG.AnnounceDate as Created,
INCHG.Acttime as Changed,
INCHG.InChgID,
BOND.SecID,
SCMST.ISIN,
INCHG.IssID,
INCHG.InChgDate,
INCHG.OldCntryCD,
INCHG.NewCntryCD,
INCHG.EventType 
FROM INCHG
INNER JOIN SCMST ON INCHG.IssID = SCMST.IssID
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and INCHG.actflag<>'D'
and (bond.issuedate<=inchg.inchgdate
     or bond.issuedate is null or inchg.inchgdate is null
    )
and eventtype<>'CLEAN'
and eventtype<>'CORR'
