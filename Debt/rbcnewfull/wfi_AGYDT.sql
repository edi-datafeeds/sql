--filepath=o:\datafeed\debt\Mergent\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=EDI_AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=o:\datafeed\debt\Mergent\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use wca
select distinct
upper('AGYDT') as TableName,
AGYDT.Actflag,
AGYDT.AnnounceDate as Created,
AGYDT.Acttime as Changed,
AGYDT.AgydtID,
AGYDT.AgncyID,
AGYDT.EffectiveDate,
AGYDT.OldRegistrarName,
AGYDT.OldAdd1,
AGYDT.OldAdd2,
AGYDT.OldAdd3,
AGYDT.OldAdd4,
AGYDT.OldAdd5,
AGYDT.OldAdd6,
AGYDT.OldCity,
AGYDT.OldCntryCD,
AGYDT.OldWebSite,
AGYDT.OldContact1,
AGYDT.OldTel1,
AGYDT.OldFax1,
AGYDT.Oldemail1,
AGYDT.OldContact2,
AGYDT.OldTel2,
AGYDT.OldFax2,
AGYDT.Oldemail2,
AGYDT.NewRegistrarName,
AGYDT.NewAdd1,
AGYDT.NewAdd2,
AGYDT.NewAdd3,
AGYDT.NewAdd4,
AGYDT.NewAdd5,
AGYDT.NewAdd6,
AGYDT.NewCity,
AGYDT.NewCntryCD,
AGYDT.NewWebSite,
AGYDT.NewContact1,
AGYDT.NewTel1,
AGYDT.NewFax1,
AGYDT.Newemail1,
AGYDT.NewContact2,
AGYDT.NewTel2,
AGYDT.NewFax2,
AGYDT.Newemail2 
FROM AGYDT
INNER JOIN SCAGY ON AGYDT.AgncyID = SCAGY.AgncyID
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
(substring(scmst.isin,1,2)='CA' or BOND.secid in (select secid from SCEXH where substring(exchgcd,1,2)='CA' and actflag<>'D'))
and AGYDT.actflag<>'D'
