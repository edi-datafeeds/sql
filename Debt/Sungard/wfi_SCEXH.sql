--filepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate as Created,
SCEXH.Acttime as Changed,
SCEXH.ScExhID,
SCEXH.SecID,
SCMST.ISIN,
SCEXH.ExchgCD,
SCEXH.ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
'' as TradeStatus,
SCEXH.LocalCode,
SUBSTRING(SCEXH.JunkLocalcode,0,50) as JunkLocalcode,
SCEXH.ScexhNotes As Notes
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and SCEXH.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))