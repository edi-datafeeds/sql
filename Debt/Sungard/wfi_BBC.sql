--filepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_BBC
--fileheadertext=EDI_BBC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
select distinct
upper('BBC') as tablename,
bbc.actflag,
bbc.announcedate,
bbc.acttime,
bbc.bbcid,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
bbc.bbgcompid,
bbc.bbgcomptk
from bbc
inner join scmst on bbc.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and BBC.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
