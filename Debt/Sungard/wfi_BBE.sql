--filepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_BBE
--fileheadertext=EDI_BBE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT distinct
upper('BBE') as tablename,
bbe.actflag,
bbe.announcedate,
bbe.acttime,
bbe.bbeid,
bbe.secid,
bbe.exchgcd,
bbe.curencd,
bbe.bbgexhid,
bbe.bbgexhtk
from bbe
inner join scmst on bbe.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and BBE.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
