--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select
upper('MPAY') as TableName,
wca.mpay.Actflag,
wca.mpay.AnnounceDate,
wca.mpay.Acttime,
wca.mpay.sEvent as EventType,
wca.mpay.EventID,
wca.mpay.OptionID,
wca.mpay.SerialID,
wca.mpay.SectyCD as ResSectyCD,
wca.mpay.ResSecID,
wca.resscmst.ISIN as ResISIN,
wca.resissur.Issuername as ResIssuername,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.MinOfrQty,
wca.mpay.MaxOfrQty,
wca.mpay.MinQlyQty,
wca.mpay.MaxQlyQty,
wca.mpay.Paydate,
wca.mpay.CurenCD,
wca.mpay.MinPrice,
wca.mpay.MaxPrice,
wca.mpay.TndrStrkPrice,
wca.mpay.TndrStrkStep,
wca.mpay.Paytype,
wca.mpay.DutchAuction,
wca.mpay.DefaultOpt,
wca.mpay.OptElectionDate
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.mpay.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.mpay.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.mpay.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.mpay.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and mpay.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3));