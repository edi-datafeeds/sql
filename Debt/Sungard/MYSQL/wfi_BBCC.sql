--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBCC
--fileheadertext=EDI_BBCC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBCC') as tablename,
wca.bbcc.actflag,
wca.bbcc.announcedate,
wca.bbcc.acttime,
wca.bbcc.bbccid,
wca.bbcc.secid,
wca.bbcc.bbcid,
wca.bbcc.oldcntrycd,
wca.bbcc.oldcurencd,
wca.bbcc.effectivedate,
wca.bbcc.newcntrycd,
wca.bbcc.newcurencd,
wca.bbcc.oldbbgcompid,
wca.bbcc.newbbgcompid,
wca.bbcc.oldbbgcomptk,
wca.bbcc.newbbgcomptk,
wca.bbcc.releventid,
wca.bbcc.eventtype,
wca.bbcc.notes
FROM wca.bbcc
inner join wca.scmst on wca.bbcc.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.bbcc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.bbcc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.bbcc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.bbcc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and bbcc.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3));


