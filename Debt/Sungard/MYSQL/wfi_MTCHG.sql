--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_MTCHG
--fileheadertext=EDI_MTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MTCHG') as TableName,
wca.mtchg.Actflag,
wca.mtchg.AnnounceDate as Created,
wca.mtchg.Acttime as Changed,
wca.mtchg.MtChgID,
wca.mtchg.SecID,
wca.scmst.ISIN,
wca.mtchg.NotificationDate,
wca.mtchg.OldMaturityDate,
wca.mtchg.NewMaturityDate,
wca.mtchg.Reason,
wca.mtchg.EventType,
wca.mtchg.OldMaturityBenchmark,
wca.mtchg.NewMaturityBenchmark,
wca.mtchg.Notes as Notes
from wca.mtchg
inner join wca.bond on wca.mtchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.mtchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.mtchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.mtchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.mtchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and mtchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3));