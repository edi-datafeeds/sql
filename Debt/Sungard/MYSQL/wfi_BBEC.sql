--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BBEC
--fileheadertext=EDI_BBEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

SELECT
upper('BBEC') as tablename,
wca.bbec.actflag,
wca.bbec.announcedate,
wca.bbec.acttime,
wca.bbec.bbecid,
wca.bbec.secid,
wca.bbec.bbeid,
wca.bbec.oldexchgcd,
wca.bbec.oldcurencd,
wca.bbec.effectivedate,
wca.bbec.newexchgcd,
wca.bbec.newcurencd,
wca.bbec.oldbbgexhid,
wca.bbec.newbbgexhid,
wca.bbec.oldbbgexhtk,
wca.bbec.newbbgexhtk,
wca.bbec.releventid,
wca.bbec.eventtype,
wca.bbec.notes
FROM wca.bbec
inner join wca.scmst on wca.bbec.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.bbec.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.bbec.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.bbec.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.bbec.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and bbec.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3));