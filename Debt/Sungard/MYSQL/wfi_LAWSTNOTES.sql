--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_LAWSTNOTES
--fileheadertext=EDI_LAWSTNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\219\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 


select
upper('LAWSTNOTES') as TableName,
wca.lawst.Actflag,
wca.lawst.LawstID,
wca.lawst.LawstNotes as Notes
from wca.lawst
where
wca.lawst.issid in (select wca.scmst.issid from client.pfisin inner join wca.scmst on client.pfisin.code = wca.scmst.isin where client.pfisin.accid=237
and (wca.lawst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or client.pfisin.actflag='I'))

or wca.lawst.issid in (select wca.scmst.issid from client.pfuscode inner join wca.scmst on client.pfuscode.code = wca.scmst.uscode where client.pfuscode.accid=237
and (wca.lawst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or client.pfuscode.actflag='I'))

or wca.lawst.issid in (select wca.scmst.issid from client.pfsecid inner join wca.scmst on client.pfsecid.code = wca.scmst.secid where client.pfsecid.accid=237
and (wca.lawst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or client.pfsecid.actflag='I'))

or wca.lawst.issid in (select client.pfsedol.issid from client.pfsedol where client.pfsedol.accid=237
and (wca.lawst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or client.pfsedol.actflag='I'));