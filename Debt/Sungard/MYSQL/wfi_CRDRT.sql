--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CRDRT
--fileheadertext=EDI_CRDRT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRDRT') as TableName,
CRDRT.Actflag,
CRDRT.AnnounceDate as Created,
CRDRT.Acttime as Changed, 
CRDRT.SecID,
CRDRT.RatingAgency,
CRDRT.RatingDate,
CRDRT.Rating,
CRDRT.Direction,
CRDRT.WatchList,
CRDRT.WatchListReason
FROM CRDRT
INNER JOIN BOND ON CRDRT.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and CRDRT.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
