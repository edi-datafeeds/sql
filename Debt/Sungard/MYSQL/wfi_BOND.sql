--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('BOND') as Tablename,
wca.bond.Actflag,
wca.bond.AnnounceDate as Created,
wca.bond.Acttime as Changed,
wca.bond.SecID,
wca.scmst.ISIN,
wca.bond.BondType,
wca.bond.DebtMarket,
wca.bond.CurenCD as DebtCurrency,
case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue when wca.bond.parvalue<>'' and wca.bond.parvalue<> 0 then wca.bond.parvalue else wca.scmst.parvalue end as NominalValue,
wca.bond.IssueDate,
wca.bond.IssueCurrency,
wca.bond.IssuePrice,
wca.bond.IssueAmount,
wca.bond.IssueAmountDate,
wca.bond.OutstandingAmount,
wca.bond.OutstandingAmountDate,
wca.bond.InterestBasis,
wca.bond.InterestRate,
wca.bond.InterestAccrualConvention,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.FirstCouponDate,
wca.bond.InterestPayDate1,
wca.bond.InterestPayDate2,
wca.bond.InterestPayDate3,
wca.bond.InterestPayDate4,
wca.bondx.DomesticTaxRate,
wca.bondx.NonResidentTaxRate,
wca.bond.FRNType,
wca.bond.FRNIndexBenchmark,
wca.bond.Markup as FrnMargin,
wca.bond.MinimumInterestRate as FrnMinInterestRate,
wca.bond.MaximumInterestRate as FrnMaxInterestRate,
wca.bond.Rounding,
wca.bondx.Series,
wca.bondx.Class,
wca.bondx.OnTap,
wca.bondx.MaximumTapAmount,
wca.bondx.TapExpiryDate,
wca.bond.Guaranteed,
wca.bond.SecuredBy,
wca.bond.SecurityCharge,
wca.bond.Subordinate,
wca.bond.SeniorJunior,
wca.bond.WarrantAttached,
wca.bond.MaturityStructure,
wca.bond.Perpetual,
wca.bond.MaturityDate,
wca.bond.MaturityExtendible,
wca.bond.Callable,
wca.bond.Puttable,
wca.bondx.Denomination1,
wca.bondx.Denomination2,
wca.bondx.Denomination3,
wca.bondx.Denomination4,
wca.bondx.Denomination5,
wca.bondx.Denomination6,
wca.bondx.Denomination7,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.bond.Strip,
wca.bond.StripInterestNumber, 
wca.bond.Bondsrc,
wca.bond.MaturityBenchmark,
wca.bond.ConventionMethod,
wca.bond.FrnIntAdjFreq as FrnInterestAdjFreq,
wca.bond.IntBusDayConv as InterestBusDayConv,
wca.bond.InterestCurrency,
wca.bond.MatBusDayConv as MaturityBusDayConv,
wca.bond.MaturityCurrency, 
wca.bondx.TaxRules,
wca.bond.VarIntPayDate as VarInterestPaydate,
wca.bond.PriceAsPercent,
wca.bond.PayOutMode,
wca.bond.Cumulative,
case when wca.bond.matprice<>'' then cast(wca.bond.matprice as decimal(18,4))
     when rtrim(wca.bond.largeparvalue)='' then null
     when rtrim(wca.bond.MatPriceAsPercent)='' then null
     else cast(wca.bond.largeparvalue as decimal(18,0)) * cast(wca.bond.MatPriceAsPercent as decimal(18,4))/100 
     end as MatPrice,
wca.bond.MatPriceAsPercent,
wca.bond.SinkingFund,
wca.bondx.GovCity,
wca.bondx.GovState,
wca.bondx.GovCntry,
wca.bondx.GovLawLkup as GovLaw,
wca.bondx.GoverningLaw as GovLawNotes,
wca.bond.Municipal,
wca.bond.PrivatePlacement,
wca.bond.Syndicated,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.Collateral,
wca.bond.CoverPool,
wca.bond.PikPay,
wca.bond.YieldAtIssue,
wca.bond.CoCoTrigger as Coco,
wca.bond.Notes
FROM wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
where 
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.bond.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.bond.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.bond.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.bond.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and (bond.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or scmst.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)));