--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('RDNOM') as TableName,
wca.rdnom.Actflag,
wca.rdnom.AnnounceDate as Created,
wca.rdnom.Acttime as Changed, 
wca.rdnom.RdnomID,
wca.rdnom.SecID,
wca.scmst.ISIN,
wca.rdnom.EffectiveDate,
wca.rdnom.OldDenomination1,
wca.rdnom.OldDenomination2,
wca.rdnom.OldDenomination3,
wca.rdnom.OldDenomination4,
wca.rdnom.OldDenomination5,
wca.rdnom.OldDenomination6,
wca.rdnom.OldDenomination7,
wca.rdnom.OldMinimumDenomination,
wca.rdnom.OldDenominationMultiple,
wca.rdnom.NewDenomination1,
wca.rdnom.NewDenomination2,
wca.rdnom.NewDenomination3,
wca.rdnom.NewDenomination4,
wca.rdnom.NewDenomination5,
wca.rdnom.NewDenomination6,
wca.rdnom.NewDenomination7,
wca.rdnom.NewMinimumDenomination,
wca.rdnom.NewDenominationMultiple,
wca.rdnom.Notes
from wca.rdnom
inner join wca.bond on wca.rdnom.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.rdnom.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.rdnom.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.rdnom.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.rdnom.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and rdnom.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3));