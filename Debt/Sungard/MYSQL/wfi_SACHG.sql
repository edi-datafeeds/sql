--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SACHG
--fileheadertext=EDI_SACHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('SACHG') as TableName,
wca.sachg.Actflag,
wca.sachg.AnnounceDate as Created,
wca.sachg.Acttime as Changed,
wca.sachg.SachgID,
wca.sachg.ScagyID,
wca.bond.SecID,
wca.scmst.ISIN,
wca.sachg.EffectiveDate,
wca.sachg.OldRelationship,
wca.sachg.NewRelationship,
wca.sachg.OldAgncyID,
wca.sachg.NewAgncyID,
wca.sachg.OldSpStartDate,
wca.sachg.NewSpStartDate,
wca.sachg.OldSpEndDate,
wca.sachg.NewSpEndDate,
wca.sachg.OldGuaranteeType,
wca.sachg.NewGuaranteeType
from wca.sachg
inner join wca.scagy on wca.sachg.scagyid = wca.scagy.scagyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.sachg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.sachg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.sachg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.sachg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and sachg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3));