--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=EDI_AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

select distinct
upper('AGYDT') as TableName,
wca.agydt.Actflag,
wca.agydt.AnnounceDate as Created,
wca.agydt.Acttime as Changed,
wca.agydt.AgydtID,
wca.agydt.AgncyID,
wca.agydt.EffectiveDate,
wca.agydt.OldRegistrarName,
wca.agydt.OldAdd1,
wca.agydt.OldAdd2,
wca.agydt.OldAdd3,
wca.agydt.OldAdd4,
wca.agydt.OldAdd5,
wca.agydt.OldAdd6,
wca.agydt.OldCity,
wca.agydt.OldCntryCD,
wca.agydt.OldWebSite,
wca.agydt.OldContact1,
wca.agydt.OldTel1,
wca.agydt.OldFax1,
wca.agydt.Oldemail1,
wca.agydt.OldContact2,
wca.agydt.OldTel2,
wca.agydt.OldFax2,
wca.agydt.Oldemail2,
wca.agydt.OldState,
wca.agydt.NewRegistrarName,
wca.agydt.NewAdd1,
wca.agydt.NewAdd2,
wca.agydt.NewAdd3,
wca.agydt.NewAdd4,
wca.agydt.NewAdd5,
wca.agydt.NewAdd6,
wca.agydt.NewCity,
wca.agydt.NewCntryCD,
wca.agydt.NewWebSite,
wca.agydt.NewContact1,
wca.agydt.NewTel1,
wca.agydt.NewFax1,
wca.agydt.Newemail1,
wca.agydt.NewContact2,
wca.agydt.NewTel2,
wca.agydt.NewFax2,
wca.agydt.Newemail2,
wca.agydt.NewState
FROM wca.agydt
inner join wca.scagy on wca.agydt.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='I')
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='I')
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='I')
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='I')
or
(wca.scmst.isin in (select code from client.pfisin where accid=237 and actflag='U')
and wca.agydt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select code from client.pfsecid where accid=237 and actflag='U')
and wca.agydt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.secid in (select secid from client.pfsedol where accid=237 and actflag='U')
and wca.agydt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
or wca.scmst.uscode in (select code from client.pfuscode where accid=237 and actflag='U')
and wca.agydt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3)
and agydt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3));
