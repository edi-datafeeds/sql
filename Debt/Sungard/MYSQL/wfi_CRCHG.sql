--filepath=O:\Prodman\Dev\WFI\Feeds\Sungard\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_CRCHG
--fileheadertext=EDI_CRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('CRCHG') as TableName,
CRCHG.Actflag,
CRCHG.AnnounceDate as Created,
CRCHG.Acttime as Changed, 
CRCHG.CrChgID,
CRCHG.SecID,
CRCHG.RatingAgency,
CRCHG.RatingDate,
CRCHG.OldRating,
CRCHG.NewRating,
CRCHG.Direction,
CRCHG.WatchList,
CRCHG.WatchListReason
FROM CRCHG
INNER JOIN BOND ON CRCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and CRCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
