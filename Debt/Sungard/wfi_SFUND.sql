--filepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_SFUND
--fileheadertext=EDI_SFUND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
select
upper('SFUND') as TableName,
SFUND.Actflag,
SFUND.AnnounceDate as Created,
SFUND.Acttime as Changed,
SFUND.SfundID,
SFUND.SecID,
SCMST.ISIN,
SFUND.SfundDate,
SFUND.CurenCD as Sinking_Currency,
SFUND.Amount,
SFUND.AmountasPercent,
SFUND.SfundNotes as Notes
FROM SFUND
INNER JOIN BOND ON SFUND.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and SFUND.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))

