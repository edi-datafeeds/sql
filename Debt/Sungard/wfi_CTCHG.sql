--filepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('CTCHG') as TableName,
CTCHG.Actflag,
CTCHG.AnnounceDate as Created,
CTCHG.Acttime as Changed, 
CTCHG.CtChgID,
CTCHG.SecID,
SCMST.ISIN,
CTCHG.EffectiveDate,
CTCHG.OldResultantRatio,
CTCHG.NewResultantRatio,
CTCHG.OldSecurityRatio,
CTCHG.NewSecurityRatio,
CTCHG.OldCurrency,
CTCHG.NewCurrency,
CTCHG.OldCurPair,
CTCHG.NewCurPair,
CTCHG.OldConversionPrice,
CTCHG.NewConversionPrice,
CTCHG.ResSectyCD,
CTCHG.OldResSecID,
CTCHG.NewResSecID,
CTCHG.EventType,
CTCHG.RelEventID,
CTCHG.OldFromDate,
CTCHG.NewFromDate,
CTCHG.OldTodate,
CTCHG.NewToDate,
CTCHG.ConvtID,
CTCHG.OldFXRate,
CTCHG.NewFXRate,
CTCHG.OldPriceAsPercent,
CTCHG.NewPriceAsPercent,
CTCHG.Notes
FROM CTCHG
INNER JOIN BOND ON CTCHG.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and CTCHG.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))