--filepath=H:\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.txt
--suffix=_SCAGY
--fileheadertext=EDI_SCAGY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\237\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
use WCA
SELECT 
upper('SCAGY') as TableName,
SCAGY.Actflag,
SCAGY.AnnounceDate as Created,
SCAGY.Acttime as Changed,
SCAGY.ScagyID,
SCAGY.SecID,
SCMST.ISIN,
SCAGY.Relationship,
SCAGY.AgncyID,
SCAGY.GuaranteeType,
SCAGY.SpStartDate,
SCAGY.SpEndDate,
SCAGY.Notes
FROM SCAGY
INNER JOIN BOND ON SCAGY.SecID = BOND.SecID
INNER JOIN SCMST ON BOND.SecID = SCMST.SecID
where
isin in (select code from client.dbo.pfisin where accid=237 and actflag='I')
or 
(isin in (select code from client.dbo.pfisin where accid=237 and actflag='U')
and SCAGY.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
