--filepath=O:\Upload\Acc\241\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CTCHG
--fileheadertext=EDI_CTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('ctchg') as Tablename,
wca.ctchg.ActFlag,
wca.ctchg.AnnounceDate as Created,
wca.ctchg.Acttime as Changed, 
wca.ctchg.Ctchgid,
wca.ctchg.SecId,
wca.scmst.Isin,
wca.ctchg.EffectiveDate,
wca.ctchg.OldResultantRatio,
wca.ctchg.NewResultantRatio,
wca.ctchg.OldSecurityRatio,
wca.ctchg.NewSecurityRatio,
wca.ctchg.OldCurrency,
wca.ctchg.NewCurrency,
wca.ctchg.OldCurPair,
wca.ctchg.NewCurPair,
wca.ctchg.OldConversionPrice,
wca.ctchg.NewConversionPrice,
wca.ctchg.ResSectycd,
wca.ctchg.OldResSecid,
wca.ctchg.NewResSecid,
wca.ctchg.EventType,
wca.ctchg.ReleventId,
wca.ctchg.OldFromDate,
wca.ctchg.NewFromDate,
wca.ctchg.OldToDate,
wca.ctchg.NewToDate,
wca.ctchg.ConvtId,
wca.ctchg.OldFxRate,
wca.ctchg.NewFxRate,
wca.ctchg.OldPriceasPercent,
wca.ctchg.NewPriceasPercent,
wca.ctchg.Notes
from wca.ctchg
inner join wca.bond on wca.ctchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=241 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=241 and actflag='U')
and wca.ctchg.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))

