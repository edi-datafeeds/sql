--filepath=O:\Upload\Acc\241\feed\\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=EDI_LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('LTCHG') as Tablename,
wca.ltchg.ActFlag,
wca.ltchg.AnnounceDate as Created,
wca.ltchg.Acttime as Changed,
wca.ltchg.LtchgId,
wca.scmst.SecId,
wca.scmst.Isin,
wca.ltchg.Exchgcd,
wca.ltchg.EffectiveDate,
wca.ltchg.OldLot,
wca.ltchg.OldMintrdQty,
wca.ltchg.NewLot,
wca.ltchg.NewMintrdgQty
from wca.ltchg
inner join wca.bond on wca.ltchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=241 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=241 and actflag='U')
and wca.bond.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





