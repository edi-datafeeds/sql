--filepath=O:\Upload\Acc\241\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_AGYDT
--fileheadertext=EDI_AGYDT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select distinct
upper('AGYDT') as Tablename,
wca.agydt.ActFlag,
wca.agydt.Announcedate as Created,
wca.agydt.Acttime as Changed,
wca.agydt.AgydtId,
wca.agydt.AgncyId,
wca.agydt.EffectiveDate,
wca.agydt.OldRegistrarName,
wca.agydt.OldAdd1,
wca.agydt.OldAdd2,
wca.agydt.OldAdd3,
wca.agydt.OldAdd4,
wca.agydt.OldAdd5,
wca.agydt.OldAdd6,
wca.agydt.OldCity,
wca.agydt.OldCntrycd,
wca.agydt.OldWebsite,
wca.agydt.OldContact1,
wca.agydt.OldTel1,
wca.agydt.OldFax1,
wca.agydt.OldEmail1,
wca.agydt.OldContact2,
wca.agydt.OldTel2,
wca.agydt.OldFax2,
wca.agydt.OldEmail2,
wca.agydt.OldState,
wca.agydt.NewRegistrarName,
wca.agydt.NewAdd1,
wca.agydt.NewAdd2,
wca.agydt.NewAdd3,
wca.agydt.NewAdd4,
wca.agydt.NewAdd5,
wca.agydt.NewAdd6,
wca.agydt.NewCity,
wca.agydt.NewCntrycd,
wca.agydt.NewWebsite,
wca.agydt.NewContact1,
wca.agydt.NewTel1,
wca.agydt.NewFax1,
wca.agydt.NewEmail1,
wca.agydt.NewContact2,
wca.agydt.NewTel2,
wca.agydt.NewFax2,
wca.agydt.NewEmail2,
wca.agydt.NewState
from wca.agydt
inner join wca.scagy on wca.agydt.agncyid = wca.scagy.agncyid
inner join wca.bond on wca.scagy.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=241 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=241 and actflag='U')
and wca.agydt.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
