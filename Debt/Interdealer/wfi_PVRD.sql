--filepath=O:\Upload\Acc\241\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_PVRD
--fileheadertext=EDI_PVRD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('PVRD') as Tablename,
wca.pvrd.ActFlag,
wca.pvrd.AnnounceDate as Created,
wca.pvrd.Acttime as Changed,
wca.pvrd.PvrdId,
wca.pvrd.SecId,
wca.scmst.Isin,
wca.pvrd.EffectiveDate,
wca.pvrd.Curencd,
wca.pvrd.OldParValue,
wca.pvrd.NewParValue,
wca.pvrd.EventType,
wca.pvrd.PvrdNotes as Notes
from wca.pvrd
inner join wca.bond on wca.pvrd.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=241 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=241 and actflag='U')
and wca.pvrd.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





