--filepath=O:\Upload\Acc\241\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_LIQNOTES
--fileheadertext=EDI_LIQNOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('LIQNOTES') as Tablename,
wca.liq.ActFlag,
wca.liq.LiqId,
wca.liq.IssId,
wca.liq.LiquidationTerms
from wca.liq
where
wca.liq.issid in
(select wca.scmst.issid from client.pfisin
inner join wca.scmst on client.pfisin.code = wca.scmst.isin
where (client.pfisin.accid=241 and client.pfisin.actflag='I')
or (client.pfisin.accid=241 and client.pfisin.actflag='U'
and liq.acttime> (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3 )))
