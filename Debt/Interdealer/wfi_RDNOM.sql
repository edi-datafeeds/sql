--filepath=O:\Upload\Acc\241\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_RDNOM
--fileheadertext=EDI_RDNOM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('rdnom') as Tablename,
wca.rdnom.ActFlag,
wca.rdnom.AnnounceDate as Created,
wca.rdnom.Acttime as Changed, 
wca.rdnom.RdnomId,
wca.rdnom.SecId,
wca.scmst.Isin,
wca.rdnom.EffectiveDate,
wca.rdnom.OldDenomination1,
wca.rdnom.OldDenomination2,
wca.rdnom.OldDenomination3,
wca.rdnom.OldDenomination4,
wca.rdnom.OldDenomination5,
wca.rdnom.OldDenomination6,
wca.rdnom.OldDenomination7,
wca.rdnom.OldMinimumDenomination,
wca.rdnom.OldDenominationMultiple,
wca.rdnom.NewDenomination1,
wca.rdnom.NewDenomination2,
wca.rdnom.NewDenomination3,
wca.rdnom.NewDenomination4,
wca.rdnom.NewDenomination5,
wca.rdnom.NewDenomination6,
wca.rdnom.NewDenomination7,
wca.rdnom.NewMinimumDenomination,
wca.rdnom.NewDenominationMultiple,
wca.rdnom.Notes
from wca.rdnom
inner join wca.bond on wca.rdnom.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
isin in (select code from client.pfisin where accid=241 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=241 and actflag='U')
and wca.rdnom.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))





