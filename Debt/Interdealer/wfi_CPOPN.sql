--filepath=O:\Upload\Acc\241\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_CPOPN
--fileheadertext=EDI_CPOPN_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select 
upper('CPOPN') as Tablename,
wca.cpopn.ActFlag,
wca.cpopn.AnnounceDate as Created,
wca.cpopn.Acttime as Changed,
wca.cpopn.CpopnId,
wca.scmst.SecId,
wca.cpopn.CallPut, 
wca.cpopn.Notes as Notes 
from wca.cpopn
INNER JOIN wca.bond ON wca.cpopn.SecID = wca.bond.SecID
INNER JOIN wca.scmst ON wca.bond.SecID = wca.scmst.SecID
where
isin in (select code from client.pfisin where accid=241 and actflag='I')
or 
(isin in (select code from client.pfisin where accid=241 and actflag='U')
and wca.cpopn.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))
