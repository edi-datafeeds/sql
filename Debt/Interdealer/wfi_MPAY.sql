--filepath=O:\Upload\Acc\241\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=SELECT DATE_FORMAT((select max(feeddate)from wca.tbl_opslog where seq = 3), '%Y%m%d')
--fileextension=.txt
--suffix=_MPAY
--fileheadertext=EDI_MPAY_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\upload\acc\241\Feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 
select
upper('MPAY') as Tablename,
wca.mpay.Actflag,
wca.mpay.AnnounceDate,
wca.mpay.Acttime,
wca.mpay.Sevent as EventType,
wca.mpay.EventId,
wca.mpay.OptionId,
wca.mpay.SerialId,
wca.mpay.Sectycd as ResSectycd,
wca.mpay.ResSecid,
wca.resscmst.Isin as ResIsin,
wca.resissur.IssuerName as ResIssuerName,
wca.resscmst.SecurityDesc as ResSecurityDesc,
wca.mpay.RatioNew,
wca.mpay.RatioOld,
wca.mpay.Fractions,
wca.mpay.MinOfrQty,
wca.mpay.MaxOfrQty,
wca.mpay.MinQlyQty,
wca.mpay.MaxQlyQty,
wca.mpay.PayDate,
wca.mpay.Curencd,
wca.mpay.MinPrice,
wca.mpay.MaxPrice,
wca.mpay.TndrStrkPrice,
wca.mpay.TndrStrkStep,
wca.mpay.PayType,
wca.mpay.DutchAuction,
wca.mpay.DefaultOpt,
wca.mpay.OptElectionDate
from wca.mpay
inner join wca.liq on 'liq' = wca.mpay.sevent and wca.liq.liqid = wca.mpay.eventid
inner join wca.scmst on wca.liq.issid = wca.scmst.issid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
left outer join wca.scmst as resscmst on wca.mpay.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid
where
wca.scmst.isin in (select code from client.pfisin where accid=241 and actflag='I')
or 
(wca.scmst.isin in (select code from client.pfisin where accid=241 and actflag='U')
and wca.mpay.acttime > (select date_sub(max(acttime), interval '2:24' hour_minute) from wca.tbl_opslog where seq = 3))






