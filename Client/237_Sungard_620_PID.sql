--filepath=o:\upload\acc\237\feed\PID\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\237\feed\PID\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select * from v54f_620_Dividend_PID
WHERE
changed>=(select max(feeddate) from tbl_opslog where seq=3)
and(isin in (select code from client.dbo.pfisin where accid=237 and client.dbo.pfisin.actflag='U')
or sedol in (select code from client.dbo.pfsedol where accid=237 and client.dbo.pfsedol.actflag='U')
or uscode in (select code from client.dbo.pfuscode where accid=237 and client.dbo.pfuscode.actflag='U') 
or secid in (select code from client.dbo.pfsecid where accid=237 and client.dbo.pfsecid.actflag='U'))
and (exchgcd = primaryexchgcd or primaryexchgcd = '')
ORDER BY CaRef

--# 2
if ((select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=237) > 0
OR (select count(client.dbo.pfsedol.actflag) from client.dbo.pfsedol where client.dbo.pfsedol.actflag = 'I' AND accid=237) > 0
OR (select count(client.dbo.pfuscode.actflag) from client.dbo.pfuscode where client.dbo.pfuscode.actflag = 'I' AND accid=237) > 0
OR (select count(client.dbo.pfsecid.actflag) from client.dbo.pfsecid where client.dbo.pfsecid.actflag = 'I' AND accid=237) > 0)
begin
use wca
SELECT * 
FROM v54f_620_Dividend_PID
WHERE (CHANGED > getdate()-183)
AND 
(isin in (select code from client.dbo.pfisin where accid=237 and client.dbo.pfisin.actflag='I')
or sedol in (select code from client.dbo.pfsedol where accid=237 and client.dbo.pfsedol.actflag='I')
or uscode in (select code from client.dbo.pfuscode where accid=237 and client.dbo.pfuscode.actflag='I')
or secid in (select code from client.dbo.pfsecid where accid=237 and client.dbo.pfsecid.actflag='I'))
and (exchgcd = primaryexchgcd or primaryexchgcd = '')
ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Dividend_PID