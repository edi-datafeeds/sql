--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_620_LATENOTICE_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\217\latenotice\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 

 

--# 1
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 103
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Liquidationdate is not null
and created>Liquidationdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 105
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)




--# 108
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 110
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 112
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 113
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Closedate is not null
and created>Closedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 115
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)




--# 117
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 118
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 119
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 121
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 122
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 123
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 124
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 125
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)

--# 126
use wca
SELECT * 
FROM v54f_620_Bonus_Rights 
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 127
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 128
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and NotificationDate is not null
and created>NotificationDate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 131
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and NameChangeDate is not null
and created>NameChangeDate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 133
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and (DateOfChange>=getdate()-0 and DateOfChange<getdate()+4)
and DateOfChange is not null
and created>DateOfChange 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 134
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)




--# 136
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and exdate is not null
and created>Exdate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 137
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 138
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 139
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Redemptiondate is not null
and created>Redemptiondate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)


--# 140
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)



--# 141
use wca
SELECT * 
FROM v51f_612_xShares_Outstanding_Change
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and Effectivedate is not null
and created>Effectivedate 
and changed>(select max(feeddate) from tbl_Opslog where seq = 3)
