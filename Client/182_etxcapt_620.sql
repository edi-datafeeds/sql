--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\182\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 


--# 1
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Liquidationdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 2
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 3
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 4
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 5
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 6
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
SecStatus <> 'I' 
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Closedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 7
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 8
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 9
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 10
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 11
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 12
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 13
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 14
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 15
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 16
use wca
SELECT * 
FROM v54f_620_Bonus_Rights 
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 17
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 18
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and NotificationDate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 19
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and NameChangeDate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 20
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and DateOfChange>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 21
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 22
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 23
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 24
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 25
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Redemptiondate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 26
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and Effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 27
use wca
SELECT * 
FROM v50f_620_Company_Meeting
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and agmdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 28
use wca
SELECT *
FROM v53f_620_Call
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and duedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 29
use wca
SELECT *
FROM v51f_620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and enddate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 30
use wca
SELECT * 
FROM v51f_620_Conversion_Terms
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and todate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 31
use wca
SELECT * 
FROM v53f_620_Buy_Back
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and enddate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 32
use wca
SELECT * 
FROM v54f_620_Arrangement
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 33
use wca
SELECT * 
FROM v54f_620_Divestment
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 34
use wca
SELECT * 
FROM v54f_620_Security_Swap 
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 35
use wca
SELECT *
FROM v50f_620_Financial_Year_Change
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and notificationdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 36
use wca
SELECT *
FROM v50f_620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and inchgdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 37
use wca
SELECT *
FROM v52f_620_Assimilation
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and assimilationdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 38
use wca
SELECT *
FROM v52f_620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 39
use wca
SELECT * 
FROM v52f_620_New_Listing
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and created>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 40
use wca
SELECT * 
FROM v50f_620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and notificationdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 41
use wca
SELECT * 
FROM v51f_620_Parvalue_Redenomination
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 42
use wca
SELECT * 
FROM v51f_620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and effectivedate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 43
use wca
SELECT * 
FROM v54f_620_Franking
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and exdate>(select max(feeddate) from tbl_Opslog where seq = 3))

--# 44
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE
SecStatus = 'X'