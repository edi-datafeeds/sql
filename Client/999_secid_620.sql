--filepath=o:\upload\acc\999\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\999\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 51
use wca
SELECT * 
FROM v50f_620_Company_Meeting 
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 52
use wca
SELECT *
FROM v53f_620_Call
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 53
use wca
SELECT * 
FROM v50f_620_Liquidation
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef, OptionID, SerialID

--# 54
use wca
SELECT *
FROM v51f_620_Certificate_Exchange
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 55
use wca
SELECT * 
FROM v51f_620_International_Code_Change
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 56
use wca
SELECT * 
FROM v51f_620_Conversion_Terms
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 57
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 58
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 59
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 60
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 61
use wca
SELECT * 
FROM v53f_620_Buy_Back
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef, OptionID, SerialID

--# 62
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 63
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef, OptionID, SerialID

--# 64
use wca
SELECT * 
FROM v54f_620_Arrangement
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 65
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef


--# 67
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 68
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef, OptionID, SerialID

--# 69
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef, OptionID, SerialID

--# 70
use wca
SELECT * 
FROM v54f_620_Divestment
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 71
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 72
use wca
SELECT * 
FROM v54f_620_Merger
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef, OptionID, SerialID

--# 73
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 74
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 75
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 76
use wca
SELECT * 
FROM v54f_620_Security_Swap 
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 77
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 78
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 79
use wca
SELECT *
FROM v50f_620_Financial_Year_Change
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 80
use wca
SELECT *
FROM v50f_620_Incorporation_Change
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 81
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 82
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 83
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 84
use wca
SELECT *
FROM v52f_620_Assimilation
WHERE (CHANGED > getdate()-20)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 85
use wca
SELECT *
FROM v52f_620_Listing_Status_Change
WHERE (CHANGED > getdate()-20)
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef

--# 86
use wca
SELECT *
FROM v52f_620_Local_Code_Change
WHERE (CHANGED > getdate()-20)
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef

--# 87
use wca
SELECT * 
FROM v52f_620_New_Listing
WHERE (CHANGED > getdate()-20)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef

--# 88
use wca
SELECT * 
FROM v50f_620_Announcement
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
ORDER BY CaRef

--# 89
use wca
SELECT * 
FROM v51f_620_Parvalue_Redenomination 
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 90
use wca
SELECT * 
FROM v51f_620_Currency_Redenomination 
WHERE (CHANGED > getdate()-20)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 91
use wca
SELECT * 
FROM v53f_620_Return_of_Capital 
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef


--# 92
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 93
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 94
use wca
SELECT * 
FROM v54f_620_Franking
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 95
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef

--# 96
use wca
SELECT * 
FROM v54f_620_Bonus_Rights
WHERE (CHANGED > getdate()-20)
AND 
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (secid in (select code from client.dbo.pfsecid where accid=999 and client.dbo.pfsecid.actflag<>'D'))
 ORDER BY CaRef
