--filepath=o:\upload\acc\213\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.682
--suffix=
--fileheadertext=EDI_WCA_682_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\682\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventCD' as f1,
'EventID' as f2,
'Sedol' as f3,
'SecID' as f2,
'Isin' as f2,
'Uscode' as f2,
'Market' as f2,
'Created' as f7,
'Changed' as f8,
'Actflag' as f9,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select
'SDCHG' as EventCD,
stab.SdchgID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
'' as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldSedol as OldStatic,
stab.NewSedol as NewStatic
FROM SDCHG as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN sedol ON scmst.secID = sedol.secid and stab.cntrycd=sedol.cntrycd
where
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.effectivedate is null and stab.announcedate>getdate()-92))
and (stab.OldSedol <> '' or stab.NewSedol <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'


--# 3
use wca
select
'ICC' as EventCD,
stab.IccID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
'' as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldIsin as OldStatic,
stab.NewIsin as NewStatic
FROM ICC as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN sedol ON scmst.secID = sedol.secid
where
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.effectivedate is null and stab.announcedate>getdate()-92))
and (stab.OldIsin <> '' or stab.NewIsin <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'


--# 4
use wca
select
'USCC' as EventCD,
stab.IccID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
'' as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
FROM ICC as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN sedol ON scmst.secID = sedol.secid
where
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.effectivedate is null and stab.announcedate>getdate()-92))
and (stab.OldUscode <> '' or stab.NewUscode <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'



--# 5
use wca
select
'INCHG' as EventCD,
stab.InchgID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
'' as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.inchgdate is null
     then stab.announcedate
     ELSE stab.inchgdate
     END as EffectiveDate,
stab.OldCntryCD as OldStatic,
stab.NewCntryCD as NewStatic
FROM INCHG as stab
INNER JOIN SCMST ON stab.IssID = scmst.Issid
INNER JOIN sedol ON scmst.secID = sedol.secid
where
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.inchgdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.inchgdate is null and stab.announcedate>getdate()-92))
and (stab.OldCntryCD <> '' or stab.NewCntryCD <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'


--# 6
use wca
select
'ISCHG' as EventCD,
stab.IschgID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
'' as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.namechangedate is null
     then stab.announcedate
     else stab.namechangedate
     end as EffectiveDate,
stab.IssOldName as OldStatic,
stab.IssNewName as NewStatic
FROM ischg as stab
INNER JOIN SCMST ON stab.IssID = scmst.Issid
INNER JOIN sedol ON scmst.secID = sedol.secid
where 
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.namechangedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.namechangedate is null and stab.announcedate>getdate()-92))
and (stab.IssOldName <> '' or stab.IssNewName <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'


--# 7
use wca
select
'LCC' as EventCD,
stab.LccID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
case when mic='' then exchg.exchgcd else exchg.mic end as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldLocalCode as OldStatic,
stab.NewLocalCode as NewStatic
FROM lcc as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
inner join exchg ON stab.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where 
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.effectivedate is null and stab.announcedate>getdate()-92))
and (stab.OldLocalCode <> '' or stab.NewLocalCode <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'


--# 8
use wca
select
'SCCHG' as EventCD,
stab.SCCHGID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
'' as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.dateofchange is null
     then stab.announcedate
     else stab.dateofchange
     end as EffectiveDate,
stab.SecOldName as OldStatic,
stab.SecNewName as NewStatic
FROM scchg as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN sedol ON scmst.secID = sedol.secid
where 
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.dateofchange>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.dateofchange is null and stab.announcedate>getdate()-92))
and (stab.SecOldName <> '' or stab.SecNewName <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'


--# 9
use wca
select
'LTCHG' as EventCD,
stab.LTCHGID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
case when mic='' then exchg.exchgcd else exchg.mic end as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
'' as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldLot as OldStatic,
stab.NewLot as NewStatic
FROM ltchg as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
inner join exchg ON stab.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where 
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.effectivedate is null and stab.announcedate>getdate()-92))
and (stab.OldLot <> '' or stab.NewLot <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'


--# 10
use wca
select
'LSTAT' as EventCD,
stab.LstatID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
case when mic='' then exchg.exchgcd else exchg.mic end as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
'' as OldStatic,
stab.LStatStatus as NewStatic
FROM lstat as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
inner join exchg ON stab.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.effectivedate is null and stab.announcedate>getdate()-92))
and stab.LStatStatus <> ''
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'


--# 11
use wca
select
'NLIST' as EventCD,
stab.scexhID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
case when mic='' then exchg.exchgcd else exchg.mic end as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
case when SCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when SCEXH.Listdate is not null 
     then SCEXH.Listdate 
     else stab.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic
FROM nlist as stab
INNER JOIN SCEXH ON stab.scexhid = SCEXH.scexhid
INNER JOIN SCMST ON SCEXH.secid = SCMST.secid
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (SCEXH.Listdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (SCEXH.Listdate is null and stab.announcedate>getdate()-92))
and sedol.actflag<>'D'
and stab.actflag<>'D'


--# 12
use wca
select
'CURRD' as EventCD,
stab.CurrdID as EventID,
sedol.sedol,
scmst.SecID,
scmst.Isin,
scmst.Uscode,
'' as Market,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldCurenCD as OldStatic,
stab.NewCurenCD as NewStatic
FROM CURRD as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN sedol ON scmst.secID = sedol.secid
where
sedol in (select code from client.dbo.pfsedol where accid=213 and actflag<>'D')
and (stab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (stab.effectivedate is null and stab.announcedate>getdate()-92))
and (stab.OldCurenCD <> '' or stab.NewCurenCD <> '')
and sedol.actflag<>'D'
and stab.actflag<>'D'
and stab.eventtype<>'CORR'
and stab.eventtype<>'CLEAN'
