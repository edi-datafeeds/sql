--filepath=o:\upload\acc\205\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\205\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N


--# 42
use wca2
SELECT * 
FROM t620_Dividend
WHERE
(isin in (select code from client.dbo.pfisin where accid=205 and client.dbo.pfisin.actflag='U'))
ORDER BY CaRef

--# 92
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=205) > 0
begin
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE (paydate>getdate())
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=205 and client.dbo.pfisin.actflag = 'I'))
and actflag<>'D'
ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Dividend 
