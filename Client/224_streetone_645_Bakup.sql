--filepath=c:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.645
--suffix=
--fileheadertext=EDI_STATIC_645_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\224\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT distinct
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
left outer join scmst on issur.issid = scmst.issid
WHERE
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='U')
and issur.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='I'))


--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM scmst
WHERE
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='U')
and scmst.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='I'))


--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
WHERE
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='U')
and sedol.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='I'))




--# 4
use WCA
SELECT distinct
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
inner join scmst on scexh.secid = scmst.secid
WHERE
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='U')
and scexh.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='I'))



--# 5
use WCA
SELECT distinct
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
inner join scexh on exchg.exchgcd = scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
WHERE
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='U')
and exchg.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='I'))
