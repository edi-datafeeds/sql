--filepath=c:\datafeed\equity\maitland\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=_ALERT
--fileheadertext=EDI_620_ALERT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 


--# 1
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate() and Exdate<getdate()+4)


--# 103
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Liquidationdate>=getdate()-0 and Liquidationdate<getdate()+4)
order by changed


--# 105
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed




--# 108
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed



--# 110
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed



--# 112
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed



--# 113
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and ((Closedate>=getdate()-0 and Closedate<getdate()+4)
or (CmAcqdate>=getdate()-0 and CmAcqdate<getdate()+4)
or (TkovrPaydate>=getdate()-0 and TkovrPaydate<getdate()+4))
order by changed



--# 115
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed




--# 117
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed



--# 118
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed


--# 119
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed



--# 121
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed



--# 122
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed


--# 123
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed



--# 124
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed



--# 125
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed

--# 126
use wca
SELECT * 
FROM v54f_620_Bonus_Rights 
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed



--# 127
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed



--# 128
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (NotificationDate>=getdate()-0 and NotificationDate<getdate()+4)
order by changed



--# 131
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (NameChangeDate>=getdate()-0 and NameChangeDate<getdate()+4)
order by changed



--# 133
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (DateOfChange>=getdate()-0 and DateOfChange<getdate()+4)
order by changed


--# 134
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed




--# 136
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Exdate>=getdate()-0 and Exdate<getdate()+4)
order by changed




--# 137
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed



--# 138
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed

--# 139
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Redemptiondate>=getdate()-0 and Redemptiondate<getdate()+4)
order by changed


--# 140
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed



--# 141
use wca
SELECT * 
FROM v51f_612_xShares_Outstanding_Change
WHERE
sedol in (select code from client.dbo.pfsedol where accid=217 and actflag<>'D')
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+4)
order by changed
