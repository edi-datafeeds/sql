--#
use wca
select distinct
'DIV' as EventCD,
etab.RdID as EventID,
case when divpy.optionid is null then '1'
     else divpy.optionid end as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'OptElectionDate' as Date4Type,
OptElectionDate as Date4,
'PeriodEndDate' as Date5Type,
PeriodEndDate as Date5,
'FYEdate' as Date6Type,
FYEdate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
case when divpy.Divtype<>'' then divpy.Divtype
     when divpy.rationew<>'' and divpy.curencd<>'' then 'B'
     when divpy.curencd<>'' then 'C'
     when divpy.rationew<>'' then 'S'
     else '' end as Paytype,
case when divpy.defaultopt='T' then 'Y' else '' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
divpy.RatioNew,
divpy.RatioOld,
divpy.Fractions,
divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
divpy.NetDividend Rate2,
'Frequency' as Field1Name,
case when Frequency='' then Divperiodcd 
     else frequency end as Field1,
'Marker' as Field2Name,
Marker as Field2,
'DivAlertCD' as Field3Name,
case when nildividend='T' then 'NIL'
     when divrescind='T' then 'DRC'
     when recindstockdiv='T' then 'SRC'
     when recindcashdiv='T' then 'CRC'
     when approxflag='T' then 'APR'
     when tbaflag='T' then 'TBA'
     else '' end as Field3,
'Taxrate' as Field4Name,
Taxrate as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from DIV as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'div' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'div' = pexdt.eventtype
left outer JOIN v10s_DIVPY AS DIVPY ON etab.divid = divpy.divid
left outer join scmst as resscmst on divpy.ressecid = resscmst.secid
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'

