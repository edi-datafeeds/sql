--filepath=o:\upload\acc\213\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.689
--suffix=
--fileheadertext=EDI_WCA_689_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\213\feed\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select
'EventCD' as f1,
'EventID' as f2,
'Fieldname' as f3,
'Notestext' as f4

--# 2
use wca
SELECT
'CTX' as EventCD,
etab.CtxID as EventID,
'Notes' as Fieldname,
CtxNotes as NotesText
FROM CTX as etab
inner join sedol on etab.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.EndDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.EndDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.EndDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.EndDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.CtxNotes as varchar(22))<>'No further information'
and len(cast(etab.CtxNotes as varchar(255)))<>0



--# 3
use wca
SELECT
'REDEM' as EventCD,
etab.RedemID as EventID,
'Notes' as Fieldname,
RedemNotes as NotesText
FROM REDEM as etab
inner join sedol on etab.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.RedemDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.RedemDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.RedemDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.RedemDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.RedemNotes as varchar(22))<>'No further information'
and len(cast(etab.RedemNotes as varchar(255)))<>0

--# 4
use wca
SELECT
'SCCHG' as EventCD,
etab.ScChgID as EventID,
'Notes' as Fieldname,
ScChgNotes as NotesText
FROM SCCHG as etab
inner join sedol on etab.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.DateofChange>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.DateofChange is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.DateofChange>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.DateofChange is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.ScChgNotes as varchar(22))<>'No further information'
and len(cast(etab.ScChgNotes as varchar(255)))<>0

--# 5
use wca
SELECT
'SECRC' as EventCD,
etab.SecRcID as EventID,
'Notes' as Fieldname,
SecRcNotes as NotesText
FROM SECRC as etab
inner join sedol on etab.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.EffectiveDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.EffectiveDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.EffectiveDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.EffectiveDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.SecRcNotes as varchar(22))<>'No further information'
and len(cast(etab.SecRcNotes as varchar(255)))<>0

--# 6
use wca
SELECT
'SDCHG' as EventCD,
etab.SdChgID as EventID,
'Notes' as Fieldname,
SdChgNotes as NotesText
FROM SDCHG as etab
inner join sedol on etab.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.EffectiveDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.EffectiveDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.EffectiveDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.EffectiveDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.SdChgNotes as varchar(22))<>'No further information'
and len(cast(etab.SdChgNotes as varchar(255)))<>0

--# 7
use wca
SELECT
'BB' as EventCD,
etab.BBID as EventID,
'Notes' as Fieldname,
BBNotes as NotesText
FROM BB as etab
inner join sedol on etab.secid = sedol.secid
left outer join rd on etab.rdid = rd.rdid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.EndDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.EndDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.EndDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.EndDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.BBNotes as varchar(22))<>'No further information'
and len(cast(etab.BBNotes as varchar(255)))<>0

--# 8
use wca
SELECT
'CALL' as EventCD,
etab.CallID as EventID,
'Notes' as Fieldname,
CallNotes as NotesText
FROM CALL as etab
inner join sedol on etab.secid = sedol.secid
left outer join rd on etab.rdid = rd.rdid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.DueDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.DueDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.DueDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.DueDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.CallNotes as varchar(22))<>'No further information'
and len(cast(etab.CallNotes as varchar(255)))<>0
 
--# 9
use wca
SELECT
'CAPRD' as EventCD,
etab.CaprdID as EventID,
'Notes' as Fieldname,
CapRdNotes as NotesText
FROM CAPRD as etab
inner join sedol on etab.secid = sedol.secid
left outer join rd on etab.rdid = rd.rdid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.effectivedate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.effectivedate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.CapRdNotes as varchar(22))<>'No further information'
and len(cast(etab.CapRdNotes as varchar(255)))<>0

--# 10
use wca
SELECT
'RCAP' as EventCD,
etab.RcapID as EventID,
'Notes' as Fieldname,
RcapNotes as NotesText
FROM RCAP as etab
inner join sedol on etab.secid = sedol.secid
left outer join rd on etab.rdid = rd.rdid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.effectivedate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.effectivedate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.RcapNotes as varchar(22))<>'No further information'
and len(cast(etab.RcapNotes as varchar(255)))<>0


--# 11
use wca
SELECT
'TKOVR' as EventCD,
etab.TkovrID as EventID,
'Notes' as Fieldname,
TkovrNotes as NotesText
FROM TKOVR as etab
inner join sedol on etab.secid = sedol.secid
left outer join rd on etab.rdid = rd.rdid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.CloseDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.CloseDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.CloseDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.CloseDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.TkovrNotes as varchar(22))<>'No further information'
and len(cast(etab.TkovrNotes as varchar(255)))<>0

--# 12
use wca
SELECT
'ARR' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
ArrNotes as NotesText
from ARR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'arr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'arr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.ArrNotes as varchar(22))<>'No further information'
and len(cast(etab.ArrNotes as varchar(255)))<>0

--# 13
use wca
SELECT
'BON' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
BonNotes as NotesText
from BON as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'bon' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'bon' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.BonNotes as varchar(22))<>'No further information'
and len(cast(etab.BonNotes as varchar(255)))<>0

--# 14
use wca
SELECT
'CONSD' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
ConsdNotes as NotesText
from CONSD as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'consd' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'consd' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.ConsdNotes as varchar(22))<>'No further information'
and len(cast(etab.ConsdNotes as varchar(255)))<>0

--# 15
use wca
SELECT
'DMRGR' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DmrgrNotes as NotesText
from DMRGR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'dmrgr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'dmrgr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.DmrgrNotes as varchar(22))<>'No further information'
and len(cast(etab.DmrgrNotes as varchar(255)))<>0

--# 16
use wca
SELECT
'DIST' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DistNotes as NotesText
from DIST as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'dist' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'dist' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.DistNotes as varchar(22))<>'No further information'
and len(cast(etab.DistNotes as varchar(255)))<>0

--# 17
use wca
SELECT
'DVST' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DvstNotes as NotesText
from DVST as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'dvst' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'dvst' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.DvstNotes as varchar(22))<>'No further information'
and len(cast(etab.DvstNotes as varchar(255)))<>0

--# 18
use wca
SELECT
'ENT' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
EntNotes as NotesText
from ENT as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'ent' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'ent' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.EntNotes as varchar(22))<>'No further information'
and len(cast(etab.EntNotes as varchar(255)))<>0

--# 19
use wca
SELECT
'MRGR' as EventCD,
etab.RdID as EventID,
'MrgrTerms' as Fieldname,
MrgrTerms
from MRGR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'mrgr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'mrgr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.MrgrTerms as varchar(22))<>'No further information'
and len(cast(etab.MrgrTerms as varchar(255)))<>0

--# 20
use wca
SELECT
'MRGR' as EventCD,
etab.RdID as EventID,
'Companies' as Fieldname,
Companies
from MRGR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'mrgr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'mrgr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.Companies as varchar(22))<>'No further information'
and len(cast(etab.Companies as varchar(255)))<>0

--# 21
use wca
SELECT
'MRGR' as EventCD,
etab.RdID as EventID,
'ApprovalStatus' as Fieldname,
ApprovalStatus
from MRGR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'mrgr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'mrgr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.ApprovalStatus as varchar(22))<>'No further information'
and len(cast(etab.ApprovalStatus as varchar(255)))<>0

--# 22
use wca
SELECT
'PRF' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
PrfNotes
from PRF as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'prf' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'prf' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.PrfNotes as varchar(22))<>'No further information'
and len(cast(etab.PrfNotes as varchar(255)))<>0

--# 23
use wca
SELECT
'PO' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
PONotes
from PO as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.offercloses>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or etab.offercloses is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.offercloses>(select max(feeddate)-1000 from wca.dbo.tbl_opslog where seq=3) 
         or (etab.offercloses is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.PONotes as varchar(22))<>'No further information'
and len(cast(etab.PONotes as varchar(255)))<>0

--# 24
use wca
SELECT
'RTS' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
RtsNotes
from RTS as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'rts' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'rts' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.RtsNotes as varchar(22))<>'No further information'
and len(cast(etab.RtsNotes as varchar(255)))<>0

--# 25
use wca
SELECT
'SCSWP' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
ScSwpNotes
from SCSWP as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'scswp' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'scswp' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.ScSwpNotes as varchar(22))<>'No further information'
and len(cast(etab.ScSwpNotes as varchar(255)))<>0

--# 26
use wca
SELECT
'SD' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
SDNotes
from SD as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'sd' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'sd' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.SDNotes as varchar(22))<>'No further information'
and len(cast(etab.SDNotes as varchar(255)))<>0

--# 27
use wca
SELECT
'DIV' as EventCD,
etab.RdID as EventID,
'Notes' as Fieldname,
DIVNotes
from DIV as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'div' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'div' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate())) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.DIVNotes as varchar(22))<>'No further information'
and len(cast(etab.DIVNotes as varchar(255)))<>0



--# 28
use wca
SELECT
'BKRP' as EventCD,
etab.BkrpID as EventID,
'Notes' as Fieldname,
BkrpNotes as NotesText
FROM BKRP as etab
inner join scmst on etab.issid = scmst.issid
inner join sedol on scmst.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.notificationdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.notificationdate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.notificationdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.notificationdate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.BkrpNotes as varchar(22))<>'No further information'
and len(cast(etab.BkrpNotes as varchar(255)))<>0


--# 29
use wca
SELECT
'LIQ' as EventCD,
etab.LiqID as EventID,
'Notes' as Fieldname,
LiquidationTerms as NotesText
FROM LIQ as etab
inner join scmst on etab.issid = scmst.issid
inner join sedol on scmst.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.RdDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.RdDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.RdDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.RdDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
and cast(etab.LiquidationTerms as varchar(22))<>'No further information'
and len(cast(etab.LiquidationTerms as varchar(255)))<>0
