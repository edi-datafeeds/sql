--filepath=o:\Upload\Acc\185\feed\
--filenameprefix=CALL_FULL_
--filename=
--filenamesql=select convert(varchar(10),max(wca.dbo.tbl_Opslog.feeddate),120) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=
--fileheadertext=EDI_Sutherlands_185_CALL_FULL_yyyy-mm-dd
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\185\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
select
client.dbo.pfisin.code as ISIN,
case when CPOPT.CPType='KO' then null
     else CPOPT.CPType
     end as Call_Style,
case when CPOPT.CPType='KO' then null
     when CPOPT.todate=CPOPT.fromdate then CPOPT.fromdate
     when CPOPT.todate is null then CPOPT.fromdate
     else null
     end as Next_Call_Date,
case when CPOPT.CPType='KO' then null
     when CPOPT.todate<> CPOPT.fromdate and CPOPT.todate is not null then CPOPT.fromdate
     else null
     end as From_Date,
case when CPOPT.CPType='KO' then null
     when CPOPT.todate<> CPOPT.fromdate and CPOPT.todate is not null then CPOPT.todate
     else null
     end as To_Date,
case when CPOPT.CPType='KO' then CPOPT.CPType
     else null
     end as KO_Call_Style,
case when CPOPT.CPType<>'KO' then null
     when CPOPT.todate=CPOPT.fromdate then CPOPT.fromdate
     when CPOPT.todate is null then CPOPT.fromdate
     else null
     end as KO_Next_Call_Date,
case when CPOPT.CPType<>'KO' then null
     when CPOPT.todate<> CPOPT.fromdate and CPOPT.todate is not null then CPOPT.fromdate
     else null
     end as KO_From_Date,
case when CPOPT.CPType<>'KO' then null
     when CPOPT.todate<> CPOPT.fromdate and CPOPT.todate is not null then CPOPT.todate
     else null
     end as KO_To_Date
from client.dbo.pfisin
left outer join scmst on client.dbo.pfisin.code = Isin
left outer join bond on scmst.secid = bond.secid
left outer join cpopt on scmst.secid = cpopt.secid
where 
client.dbo.pfisin.accid  = 185
and client.dbo.pfisin.Actflag<>'D'
and cpopt.cpoptid is not null
and scmst.secid is not null
and bond.callable<>'N'
order by isin
