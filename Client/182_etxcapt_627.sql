--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.627
--suffix=
--fileheadertext=EDI_627_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\182\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 


--# 1
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
SecStatus <> 'I'
and (isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='U')
and changed >= (select max(feeddate) from tbl_Opslog where seq = 3))
or
(isin in (select client.dbo.pfisin.code from client.dbo.pfisin where accid = 182 and actflag='I')
and exdate>(select max(feeddate) from tbl_Opslog where seq = 3))
