--filepath=o:\upload\acc\34\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\34\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA2
SELECT * 
FROM t610_Company_Meeting 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 2
USE WCA2
SELECT *
FROM t610_Call
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 3
USE WCA2
SELECT * 
FROM t610_Liquidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))


--# 4
USE WCA2
SELECT *
FROM t610_Certificate_Exchange
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 5
USE WCA2
SELECT * 
FROM t610_International_Code_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 6
USE WCA2
SELECT * 
FROM t610_Preference_Conversion
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 7
USE WCA2
SELECT * 
FROM t610_Preference_Redemption
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 8
USE WCA2
SELECT * 
FROM t610_Security_Reclassification
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 9
USE WCA2
SELECT * 
FROM t610_Lot_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 10
USE WCA2
SELECT * 
FROM t610_Sedol_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 11
USE WCA2
SELECT * 
FROM t610_Buy_Back
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))


--# 12
USE WCA2
SELECT * 
FROM t610_Capital_Reduction
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 13
USE WCA2
SELECT * 
FROM t610_Takeover
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))


--# 14
USE WCA2
SELECT * 
FROM t610_Arrangement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 15
USE WCA2
SELECT * 
FROM t610_Bonus
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 16
USE WCA2
SELECT * 
FROM t610_Bonus_Rights
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 17
USE WCA2
SELECT * 
FROM t610_Consolidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 18
USE WCA2
SELECT * 
FROM t610_Demerger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))


--# 19
USE WCA2
SELECT * 
FROM t610_Distribution
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))


--# 20
USE WCA2
SELECT * 
FROM t610_Divestment
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 21
USE WCA2
SELECT * 
FROM t610_Entitlement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 22
USE WCA2
SELECT * 
FROM t610_Merger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))


--# 23
USE WCA2
SELECT * 
FROM t610_Preferential_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 24
USE WCA2
SELECT * 
FROM t610_Purchase_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 25
USE WCA2
SELECT * 
FROM t610_Rights 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 26
USE WCA2
SELECT * 
FROM t610_Security_Swap 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 27
USE WCA2
SELECT *
FROM t610_Subdivision
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 28
USE WCA2
SELECT *
FROM t610_Bankruptcy 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 29
USE WCA2
SELECT *
FROM t610_Financial_Year_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 30
USE WCA2
SELECT *
FROM t610_Incorporation_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 31
USE WCA2
SELECT *
FROM t610_Issuer_Name_change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 32
USE WCA2
SELECT *
FROM t610_Lawsuit
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 33
USE WCA2
SELECT *
FROM t610_Security_Description_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 34
USE WCA2
SELECT *
FROM t610_Assimilation
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 35
USE WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 36
USE WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 37
USE WCA2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 38
USE WCA2
SELECT * 
FROM t610_Announcement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 39
USE WCA2
SELECT * 
FROM t610_Parvalue_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 40
USE WCA2
SELECT * 
FROM t610_Currency_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 41
USE WCA2
SELECT * 
FROM t610_Return_of_Capital 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))




--# 42
USE WCA2
SELECT * 
FROM t610_Dividend
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 43
USE WCA2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))


--# 44
USE WCA2
SELECT * 
FROM t610_Franking
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))



--# 45
USE WCA
SELECT * 
FROM v54f_701_Dividend
WHERE
changed >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='U'))
ORDER BY EventID, Sedol


--# 51
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v50f_610_Company_Meeting 
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Company_Meeting 

--# 52
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v53f_610_Call
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Call 

--# 53
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v50f_610_Liquidation
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Liquidation 

--# 54
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v51f_610_Certificate_Exchange
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Certificate_Exchange 

--# 55
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_International_Code_Change
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_International_Code_Change 

--# 56
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Preference_Conversion
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Preference_Conversion 

--# 57
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Preference_Redemption
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Preference_Redemption 

--# 58
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Security_Reclassification
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Security_Reclassification 

--# 59
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v52f_610_Lot_Change
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v52f_610_Lot_Change 

--# 60
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v52f_610_Sedol_Change
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v52f_610_Sedol_Change 

--# 61
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Buy_Back
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Buy_Back 

--# 62
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Capital_Reduction
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Capital_Reduction 

--# 63
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Takeover
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Takeover 

--# 64
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Arrangement
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Arrangement 

--# 65
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Bonus
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Bonus 

--# 66
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Bonus_Rights
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Bonus_Rights 

--# 67
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Consolidation
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Consolidation 

--# 68
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Demerger
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Demerger 

--# 69
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Distribution
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Distribution 

--# 70
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Divestment
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Divestment 

--# 71
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Entitlement
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Entitlement 

--# 72
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Merger
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Merger 

--# 73
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Preferential_Offer
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Preferential_Offer 

--# 74
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Purchase_Offer
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Purchase_Offer 

--# 75
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Rights 
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Rights 

--# 76
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Security_Swap 
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Security_Swap 

--# 77
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v54f_610_Subdivision
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Subdivision 

--# 78
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Bankruptcy 
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Bankruptcy 

--# 79
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Financial_Year_Change
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Financial_Year_Change 

--# 80
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Incorporation_Change
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Incorporation_Change 

--# 81
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Issuer_Name_change
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Issuer_Name_change 

--# 82
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Lawsuit
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Lawsuit 

--# 83
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v51f_610_Security_Description_Change
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Security_Description_Change 

--# 84
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT *
FROM v52f_610_Assimilation
WHERE (CHANGED > (getdate()-35))
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v52f_610_Assimilation 

--# 85
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE (CHANGED > getdate()+1)
end
else
SELECT top 0 * FROM t610_Listing_Status_Change 

--# 86
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE (CHANGED > getdate()+1)
end
else
SELECT top 0 * FROM t610_Local_Code_Change 

--# 87
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v52f_610_New_Listing
WHERE (CHANGED > (getdate()-35))
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v52f_610_New_Listing 

--# 88
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v50f_610_Announcement
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Announcement 

--# 89
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Parvalue_Redenomination 
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Parvalue_Redenomination 

--# 90
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Currency_Redenomination 
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Currency_Redenomination 

--# 91
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Return_of_Capital 
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Return_of_Capital 


--# 92
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Dividend
WHERE (CHANGED > getdate()-35)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Dividend 



--# 93
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Dividend_Reinvestment_Plan
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Dividend_Reinvestment_Plan 


--# 94
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Franking
WHERE (CHANGED > (getdate()-35))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Franking 


--# 95
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_701_Dividend
WHERE (CHANGED > getdate()-35)
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag='I'))
ORDER BY EventID, Sedol
end
else
SELECT top 0 * FROM v54f_701_Dividend 


--# 101
USE WCA
SELECT * 
FROM v50f_610_Company_Meeting 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (AGMdate>=getdate()+1 and AGMdate<getdate()+5)



--# 103
USE WCA
SELECT * 
FROM v50f_610_Liquidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Liquidationdate>=getdate()+1 and Liquidationdate<getdate()+5)


--# 105
USE WCA
SELECT * 
FROM v51f_610_International_Code_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)



--# 106
USE WCA
SELECT * 
FROM v51f_610_Preference_Conversion
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Fromdate>=getdate()+1 and Fromdate<getdate()+5)



--# 107
USE WCA
SELECT * 
FROM v51f_610_Preference_Redemption
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Redemptiondate>=getdate()+1 and Redemptiondate<getdate()+5)



--# 108
USE WCA
SELECT * 
FROM v51f_610_Security_Reclassification
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)



--# 110
USE WCA
SELECT * 
FROM v52f_610_Sedol_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)



--# 112
USE WCA
SELECT * 
FROM v53f_610_Capital_Reduction
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)



--# 113
USE WCA
SELECT * 
FROM v53f_610_Takeover
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and ((Closedate>=getdate()+1 and Closedate<getdate()+5)
or (CmAcqdate>=getdate()+1 and CmAcqdate<getdate()+5))



--# 114
USE WCA
SELECT * 
FROM v54f_610_Arrangement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)


--# 115
USE WCA
SELECT * 
FROM v54f_610_Bonus
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 116
USE WCA
SELECT * 
FROM v54f_610_Bonus_Rights
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 117
USE WCA
SELECT * 
FROM v54f_610_Consolidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 118
USE WCA
SELECT * 
FROM v54f_610_Demerger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)


--# 119
USE WCA
SELECT * 
FROM v54f_610_Distribution
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 121
USE WCA
SELECT * 
FROM v54f_610_Entitlement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 122
USE WCA
SELECT * 
FROM v54f_610_Merger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)


--# 123
USE WCA
SELECT * 
FROM v54f_610_Preferential_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 124
USE WCA
SELECT * 
FROM v54f_610_Purchase_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 125
USE WCA
SELECT * 
FROM v54f_610_Rights 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 127
USE WCA
SELECT *
FROM v54f_610_Subdivision
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Exdate>=getdate()+1 and Exdate<getdate()+5)



--# 128
USE WCA
SELECT *
FROM v50f_610_Bankruptcy 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (NotificationDate>=getdate()+1 and NotificationDate<getdate()+5)



--# 131
USE WCA
SELECT *
FROM v50f_610_Issuer_Name_change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (NameChangeDate>=getdate()+1 and NameChangeDate<getdate()+5)



--# 133
USE WCA
SELECT *
FROM v51f_610_Security_Description_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (DateOfChange>=getdate()+1 and DateOfChange<getdate()+5)



--# 134
USE WCA
SELECT *
FROM v52f_610_Assimilation
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (AssimilationDate>=getdate()+1 and AssimilationDate<getdate()+5)


--# 139
USE WCA
SELECT * 
FROM v51f_610_Parvalue_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)


--# 140
USE WCA
SELECT * 
FROM v51f_610_Currency_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=34 and actflag<>'D'))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)

