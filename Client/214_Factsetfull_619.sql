--filepath=o:\upload\acc\214\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.619
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\213\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
use wca
select
scmst.SecID,
case when SHOCH.shochid is not null then SHOCH.AnnounceDate 
     else null
     end as Created,
case when SHOCH.shochid is not null then SHOCH.Acttime
     else null
     end as Changed,
case when SHOCH.Actflag='D' or scmst.actflag='D' then 'D'
     else ''
     end as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.ISIN,
scmst.USCode,
case when scmst.Statusflag = 'A' then 'A'
     when scmst.Statusflag = '' then 'A' 
     when scmst.Statusflag is null then 'A' 
     ELSE 'I' END as Statusflag,
scmst.PrimaryExchgCD,
scmst.SectyCD,
exchg.exchgname as Exchange,
scexh.ExchgCD,
exchg.MIC,
exchg.cntrycd as ExCountry,
sedol.rcntrycd as RegCountry,
sedol.Sedol,
scexh.LocalCode,
case when scexh.ListStatus = '' then 'L'
     when scexh.ListStatus = 'N' then 'L'
     else scexh.ListStatus
     end as ListingStatus,
scexh.ListDate,
SHOCH.shochid as EventID,
SHOCH.EffectiveDate,
SHOCH.OldSos,
case when SHOCH.shochid is not null then SHOCH.NewSos 
     else scmst.sharesoutstanding 
     end as NewSos,
SHOCH.ShochNotes
from client.dbo.pfisin
left outer join scmst on client.dbo.pfisin.code = scmst.isin
        and client.dbo.pfisin.accid = 214
        and client.dbo.pfisin.actflag <> 'D'
left outer JOIN issur on scmst.issid = issur.issid
left outer JOIN scexh on scmst.secid = scexh.secid
left outer JOIN exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scexh.secid = sedol.secid and exchg.cntrycd=sedol.cntrycd
LEFT OUTER JOIN SHOCHSEQ1 ON scmst.SecID = SHOCHSEQ1.secid and 1 = shochseq1.seqnum
LEFT OUTER JOIN SHOCH ON shochseq1.shochid = SHOCH.shochID
where
(scmst.PrimaryExchgCD=SCEXH.ExchgCD or scmst.PrimaryExchgCD='')
