--filepath=c:\datafeed\equity\maitland\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=_ALERT
--fileheadertext=EDI_620_PAYABLE_ALERT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\217\feed\pay\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 


--# 1
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and (PayDate>=getdate()-0 and PayDate<getdate()+4)


--# 136
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
isin in (select code from client.dbo.pfisin where accid=217 and actflag<>'D')
and (PayDate>=getdate()-0 and PayDate<getdate()+4)
order by changed



