
 
'BB' as EventCD,
'CALL' as EventCD,
'CallNotes' as Fieldname,
'CAPRD' as EventCD,
'CONVT' as EventCD,
'ConvtNotes' as Fieldname,
'CTX' as EventCD,
'CTXNOTES' as Fieldname,
'DMRGR' as EventCD,
'EventCD' as f1,
'EventID' as f2,
'Fieldname' as f3,
'LIQ' as EventCD,
'LiquidationTerms' as Fieldname,
'Notes' as Fieldname,
'Notestext' as f4
'RdNotes' as Fieldname,
'REDMT' as EventCD,
'RedmtNotes' as Fieldname,
'SDCHG' as EventCD,
'SdChgNotes' as Fieldname,
'SECRC' as EventCD,
'SecRcNotes' as Fieldname,
((sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
(RelatedEvent <> 'Clean' or RelatedEvent is null)
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
CallID as EventID,
CallNotes as NotesText
CapRdNotes as NotesText
CONVTID as EventID,
ConvtNotes as NotesText
CTXID as EventID,
CTXNOTES as NotesText
DmrgrNotes as NotesText
else
end
FROM v50f_620_Bankruptcy 
FROM v50f_620_Class_Action
FROM v50f_620_Liquidation

FROM v51f_620_Certificate_Exchange*
FROM v51f_620_Conversion_Terms*
FROM v51f_620_Conversion_Terms_Change NA
FROM v51f_620_Currency_Redenomination* 
FROM v51f_620_International_Code_Change No Notes
FROM v51f_620_Parvalue_Redenomination* 
FROM v51f_620_Redemption_Terms*
FROM v51f_620_Security_Description_Change*
FROM v51f_620_Security_Reclassification*

FROM v52f_620_Assimilation*
FROM v52f_620_Listing_Status_Change*
FROM v52f_620_Local_Code_Change*
FROM v52f_620_Lot_Change*
FROM v52f_620_New_Listing*
FROM v52f_620_Sedol_Change*

FROM v53f_620_Buy_Back*
FROM v53f_620_Call*
FROM v53f_620_Capital_Reduction*
FROM v53f_620_Return_of_Capital* 
FROM v53f_620_Takeover*

FROM v54f_620_Arrangement*
FROM v54f_620_Bonus*
FROM v54f_620_Consolidation*
FROM v54f_620_Demerger*
FROM v54f_620_Distribution*
FROM v54f_620_Divestment*
FROM v54f_620_Dividend*
FROM v54f_620_Dividend_Reinvestment_Plan No Notes
FROM v54f_620_Entitlement*
FROM v54f_620_Franking No Notes
FROM v54f_620_Merger*
FROM v54f_620_Preferential_Offer*
FROM v54f_620_Purchase_Offer*
FROM v54f_620_Rights*
FROM v54f_620_Security_Swap*
FROM v54f_620_Subdivision*

LiquidationTerms as NotesText
rd.RdNotes as NotesText
RedmtID as EventID,
RedmtNotes as NotesText
SdChgID as EventID,
SdChgNotes as NotesText
SecRcID as EventID,
SecRcNotes as NotesText
