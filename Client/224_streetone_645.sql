--filepath=c:\
--filenameprefix=AL
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.645
--suffix=
--fileheadertext=EDI_STATIC_645_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\224\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.USCode,
scmst.ISIN,
issur.IssID,
issur.IssuerName,
issur.IndusID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.LocalCode
FROM scmst
left outer join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
 
WHERE
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='U')
and scmst.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
and scmst.PrimaryExchgCD = scexh.ExchgCD)
or
(scmst.secid in (select code from client.dbo.pfsecid where accid = 224 and actflag='I')
and scmst.PrimaryExchgCD = scexh.ExchgCD)


