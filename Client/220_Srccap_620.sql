--filepath=o:\upload\acc\220\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\220\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca2
SELECT * 
FROM t620_Company_Meeting 
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 2
use wca2
SELECT *
FROM t620_Call
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 3
use wca2
SELECT * 
FROM t620_Liquidation
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 4
use wca2
SELECT *
FROM t620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 5
use wca2
SELECT * 
FROM t620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 6
use wca2
SELECT * 
FROM t620_Conversion_Terms
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef


--# 7
use wca2
SELECT * 
FROM t620_Redemption_Terms
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 8
use wca2
SELECT * 
FROM t620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 9
use wca2
SELECT * 
FROM t620_Lot_Change
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM t620_sedol_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 11
use wca2
SELECT * 
FROM t620_Buy_Back
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 12
use wca2
SELECT * 
FROM t620_Capital_Reduction
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 13
use wca2
SELECT * 
FROM t620_Takeover
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 14
use wca2
SELECT * 
FROM t620_Arrangement
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 15
use wca2
SELECT * 
FROM t620_Bonus
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 17
use wca2
SELECT * 
FROM t620_Consolidation
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 18
use wca2
SELECT * 
FROM t620_Demerger
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 19
use wca2
SELECT * 
FROM t620_Distribution
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 20
use wca2
SELECT * 
FROM t620_Divestment
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 21
use wca2
SELECT * 
FROM t620_Entitlement
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 22
use wca2
SELECT * 
FROM t620_Merger
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 23
use wca2
SELECT * 
FROM t620_Preferential_Offer
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 24
use wca2
SELECT * 
FROM t620_Purchase_Offer
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 25
use wca2
SELECT * 
FROM t620_Rights 
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 26
use wca2
SELECT * 
FROM t620_Security_Swap 
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM t620_Subdivision
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 28
use wca2
SELECT *
FROM t620_Bankruptcy 
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 29
use wca2
SELECT *
FROM t620_Financial_Year_Change
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 30
use wca2
SELECT *
FROM t620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM t620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 32
use wca2
SELECT *
FROM t620_Class_Action
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
 ORDER BY CaRef

--# 34
use wca2
SELECT *
FROM t620_Assimilation
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM t620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM t620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_New_Listing
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 38
use wca2
SELECT * 
FROM t620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 39
use wca2
SELECT * 
FROM t620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 40
use wca2
SELECT * 
FROM t620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 41
use wca2
SELECT * 
FROM t620_Return_of_Capital 
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef


--# 42
use wca2
SELECT * 
FROM t620_Dividend
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 43
use wca2
SELECT * 
FROM t620_Dividend_Reinvestment_Plan
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 44
use wca2
SELECT * 
FROM t620_Franking
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef


--# 45
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620_Bonus_Rights
WHERE
(isin in (select code from client.dbo.pfisin where accid=220 and client.dbo.pfisin.actflag='U'))
-- and primaryex<>'No'
ORDER BY CaRef



--# 51
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v50f_620_Company_Meeting 
WHERE (agmdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Company_Meeting 

--# 52
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v53f_620_Call
WHERE (duedate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v53f_620_Call 

--# 53
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v50f_620_Liquidation
WHERE (recdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v50f_620_Liquidation 

--# 54
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v51f_620_Certificate_Exchange
WHERE (enddate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Certificate_Exchange 

--# 55
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v51f_620_International_Code_Change
WHERE (effectivedate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_International_Code_Change 

--# 56
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v51f_620_Conversion_Terms
WHERE (todate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Conversion_Terms

--# 57
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE (redemptiondate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Redemption_Terms

--# 58
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE (effectivedate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Security_Reclassification 

--# 59
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE (effectivedate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Lot_Change 

--# 60
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v52f_620_sedol_change
WHERE (effectivedate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_sedol_change 

--# 61
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v53f_620_Buy_Back
WHERE (enddate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v53f_620_Buy_Back 

--# 62
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE (effectivedate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v53f_620_Capital_Reduction 

--# 63
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE (closedate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v53f_620_Takeover 

--# 64
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Arrangement
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Arrangement 

--# 65
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Bonus 


--# 67
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Consolidation 

--# 68
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_620_Demerger 

--# 69
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_620_Distribution 

--# 70
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Divestment
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Divestment 

--# 71
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Entitlement 

--# 72
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Merger
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_620_Merger 

--# 73
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Preferential_Offer 

--# 74
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE (offercloses  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Purchase_Offer 

--# 75
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Rights 

--# 76
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Security_Swap 
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Security_Swap 

--# 77
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Subdivision 

--# 78
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE (notificationdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Bankruptcy 

--# 79
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v50f_620_Financial_Year_Change
WHERE (notificationdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Financial_Year_Change 

--# 80
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v50f_620_Incorporation_Change
WHERE (inchgdate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Incorporation_Change 

--# 81
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE (namechangedate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Issuer_Name_change 

--# 82
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE (effectivedate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Class_Action

--# 83
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE (dateofchange  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Security_Description_Change 

--# 84
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v52f_620_Assimilation
WHERE (assimilationdate  > getdate()-1)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Assimilation 

--# 85
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v52f_620_Listing_Status_Change
WHERE (effectivedate  > getdate()-1)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' 
ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Listing_Status_Change 

--# 86
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT *
FROM v52f_620_Local_Code_Change
WHERE (effectivedate  > getdate()-1)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' 
ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Local_Code_Change 

--# 87
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v52f_620_New_Listing
WHERE (created  > getdate()-1)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No' 
ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_New_Listing 

--# 88
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v50f_620_Announcement
WHERE (notificationdate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Announcement 

--# 89
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v51f_620_Parvalue_Redenomination 
WHERE (effectivedate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Parvalue_Redenomination 

--# 90
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v51f_620_Currency_Redenomination 
WHERE (effectivedate  > getdate()-1)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Currency_Redenomination 

--# 91
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v53f_620_Return_of_Capital 
WHERE (effectivedate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v53f_620_Return_of_Capital 


--# 92
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Dividend 

--# 93
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Dividend_Reinvestment_Plan 

--# 94
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Franking
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Franking 

--# 95
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE (effectivedate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Conversion_Terms_Change

--# 96
if (select count(client.dbo.pfisin.actflag)  from client.dbo.pfisin where client.dbo.pfisin.actflag = 'I' and accid=220) > 0
begin
use wca
SELECT * 
FROM v54f_620_Bonus_Rights
WHERE (exdate  > getdate()-1)
AND 
(secid in (select wca.dbo.scmst.secid from wca.dbo.scmst inner join client.dbo.pfisin on wca.dbo.scmst.isin = client.dbo.pfisin.code where accid=220 and client.dbo.pfisin.actflag='I'))
-- and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Bonus_Rights
