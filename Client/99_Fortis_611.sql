--filepath=o:\upload\acc\99\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.611
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\99\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n




--# 1
USE WCA2
SELECT * 
FROM t610_Dividend
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (isin in (select code from client.dbo.pfisin where accid=99 and actflag='U'))
ORDER BY EventID, ExchgCD, isin


--# 2
if (select count(actflag)  from client.dbo.pfisin where actflag = 'I' and accid=34) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Dividend
WHERE (CHANGED >= (getdate()-183))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (isin in (select code from client.dbo.pfisin where accid=99 and actflag='I'))
ORDER BY EventID, ExchgCD, isin
end
else
SELECT top 0 * FROM v54f_610_Dividend 

