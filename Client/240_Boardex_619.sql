--filepath=o:\upload\acc\240\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.619
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\240\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
use wca
select
scmst.SecID,
case when shh.shochid is not null then shh.AnnounceDate 
     else null
     end as Created,
case when shh.shochid is not null then shh.Acttime
     else null
     end as Changed,
case when shh.Actflag='D' or scmst.actflag='D' then 'D'
     else ''
     end as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.ISIN,
scmst.USCode,
case when scmst.Statusflag = 'A' then 'A'
     when scmst.Statusflag = '' then 'A' 
     when scmst.Statusflag is null then 'A' 
     ELSE 'I' END as Statusflag,
scmst.PrimaryExchgCD,
scmst.SectyCD,
exchg.exchgname as Exchange,
scexh.ExchgCD,
exchg.MIC,
exchg.cntrycd as ExCountry,
sedol.rcntrycd as RegCountry,
sedol.Sedol,
scexh.LocalCode,
case when scexh.ListStatus = '' then 'L'
     when scexh.ListStatus = 'N' then 'L'
     else scexh.ListStatus
     end as ListingStatus,
scexh.ListDate,
shh.shochid as EventID,
shh.EffectiveDate,
shh.OldSos,
case when shh.shochid is not null then shh.NewSos 
     else scmst.sharesoutstanding 
     end as NewSos,
shh.ShochNotes
FROM scmst
inner JOIN Sectygrp on scmst.SectyCD = Sectygrp.SectyCD and 3>secgrpid
inner join ISSUR ON SCMST.IssID = ISSUR.IssID
inner JOIN scexh ON scmst.SecID = scexh.SecID and 'D'<>scexh.liststatus
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scexh.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd and 'D'<>sedol.actflag
left outer join shoch as shh on scmst.secid = shh.secid and 'D'<>shh.actflag
where
isin in (select code from client.dbo.pfisin where accid = 240 and actflag<>'D')
and shh.shochid in (select top 1 shochid from wca.dbo.shoch where scmst.secid=shoch.secid order by wca.dbo.shoch.secid, effectivedate desc)
and ((shh.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
or (isin in (select code from client.dbo.pfisin where accid = 240 and actflag='I')))
