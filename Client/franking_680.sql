--#
use wca
select distinct
'FRANK' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Paytype,
'' as DefaultOpt,
'' as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'Frankdiv' as Rate1Type,
Frankdiv Rate1,
'UnFrankdiv' as Rate2Type,
UnFrankdiv Rate2,
'FrankFlag' as Field1Name,
FrankFlag as Field1,
'FrankCntryCD' as Field2Name,
etab.CntryCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from FRANK as etab
inner join div on etab.divid = div.rdid
inner join rd on div.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join exchg on etab.cntrycd = exchg.cntrycd and 'D'<>exchg.actflag
inner join scexh on exchg.exchgcd = scexh.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'div' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'div' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'


