MT564 Covered

AGM - Company Meeting
ASSM - Assimilation
BB - Buy Back
BKRP - Bankruptcy
BON - Bonus
CALL - Call
CAPRD - Capital Reduction
CONSD - Consolidation
CTX - Certificate Exchange
CURRD - Currency Redenomination
DIST - Distribution
DIV - Dividend
DMRGR - Demerger
DRIP - Drip
DVST - Divestment
ENT - Entitlement
ISCHG - Issuer Name Change
LAWST - Lawsuit
LIQ - Liquidation
LSTAT - Listing Status Change
NLIST - New  Listing
PO - Purchase Offer
PVRD - Parvalue Redenomination
RCAP - Return of Capital
RTS - Rights
SD - Subdivision
TKOVR - Takeover

MT564 NOT covered

ANN - Announcement
ARR - Arrangement
FRANK _ Franking
FYCHG - Financial Year End Change
ICC - International Code Change
INCHG - Incorporation Change
LCC - Local Code Change
LTCHG - Lot Change
PRF - Preferential Offer
SCCHG - Security Description Change
SCSWP - Security Swap
SDCHG - Sedol Change
SECRC - Security Reclassification
