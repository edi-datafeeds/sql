use ipo
select ipo.issuer, wca.dbo.issur.issuername, ipo.isin, wca.dbo.scmst.isin, ipo.symbol, wca.dbo.scexh.localcode

from ipo
left outer join wca.dbo.scexh on ipo.symbol = wca.dbo.scexh.localcode
                  and wca.dbo.scexh.exchgcd = ipo.exchgcd
left outer join wca.dbo.scmst on wca.dbo.scexh.secid = wca.dbo.scmst.secid
left outer join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid

where ipo.actflag <> 'D'
and wca.dbo.scexh.secid is not null
and len(wca.dbo.scmst.isin)=12
and len(ipo.isin) = 12
and ipo.isin <> wca.dbo.scmst.isin



select count(*) from ipo
where actflag <> 'D'

where len(isin) = 12
