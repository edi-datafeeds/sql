--filepath=o:\Datafeed\IPO\103i\
--filenameprefix=
--filenamealt=
--filenamesql=select convert(varchar(30),max(wca.dbo.feedlog.Todate), 112) from wca.dbo.feedlog where wca.dbo.feedlog.feedfreq = 'thricedaily'
--fileextension=.103
--suffix=
--fileheadersql=select 'EDI_IPO_103_' + convert(varchar(30),max(wca.dbo.feedlog.Todate), 112) from wca.dbo.feedlog where wca.dbo.feedlog.feedfreq = 'thricedaily'
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\103i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 wca.dbo.tbl_opslog.seq FROM wca.dbo.tbl_opslog order by wca.dbo.tbl_opslog.acttime desc
--sevent=n
--shownulls=n
--filenamealt=

--# 1
declare @logdate as datetime
set @logdate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)

SELECT 
wca.dbo.ipo.AnnounceDate as Created,
@logdate as Modified,
wca.dbo.ipo.IpoID+100000 as ID,
substring(wca.dbo.ipo.IPOStatus,1,1) as Status,
wca.dbo.ipo.Actflag,
wca.dbo.issur.Issuername as Issuer,
wca.dbo.issur.CntryofIncorp as CntryInc,
wca.dbo.Scexh.ExchgCd,
wca.dbo.scmst.ISIN,
wca.dbo.sedol.Sedol,
wca.dbo.scmst.USCode,
wca.dbo.scexh.Localcode as Symbol,
wca.dbo.scmst.SecurityDesc as SecDescription,
wca.dbo.scmst.CurenCD as PVCurrency,
wca.dbo.scmst.ParValue,
wca.dbo.scmst.SecTyCD as Sectype,
wca.dbo.scexh.Lot as LotSize,
wca.dbo.ipo.SubPeriodFrom,
wca.dbo.ipo.SubperiodTo,
wca.dbo.ipo.MinSharesOffered,
wca.dbo.ipo.MaxSharesOffered,
wca.dbo.ipo.SharesOutstanding,
wca.dbo.ipo.TOFCurrency as Currency,
wca.dbo.ipo.SharePriceLowest,
wca.dbo.ipo.SharePriceHighest,
wca.dbo.ipo.ProposedPrice,
wca.dbo.ipo.InitialPrice,
'' as InitialTradedVolume,
wca.dbo.ipo.Underwriter,
wca.dbo.ipo.DealType,
wca.dbo.ipo.LawFirm,
wca.dbo.ipo.TransferAgent,
'' as IndusCode,
wca.dbo.ipo.FirstTradingDate,
'' as SecLevel,
wca.dbo.ipo.TOFCurrency,
case when wca.dbo.ipo.TotalOfferSize<>'' then cast(cast(wca.dbo.ipo.TotalOfferSize as decimal(15,6))*1000000 as bigint) else null end as TotalOfferSize,
wca.dbo.exchg.mic as MIC,
wca.dbo.ipo.Notes
from wca.dbo.IPO
inner join wca.dbo.scmst on wca.dbo.ipo.secid = wca.dbo.scmst.secid
inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
inner join wca.dbo.scexh on wca.dbo.ipo.secid = wca.dbo.scexh.secid
inner join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid and wca.dbo.exchg.cntrycd = wca.dbo.sedol.cntrycd

where
(wca.dbo.ipo.acttime >=(select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.ipo.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
or wca.dbo.scmst.acttime >= (select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.scmst.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
     and getdate()-60<wca.dbo.ipo.AnnounceDate
or wca.dbo.scexh.acttime >= (select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.scexh.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
     and getdate()-60<wca.dbo.ipo.AnnounceDate
or wca.dbo.sedol.acttime >= (select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.sedol.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
     and getdate()-60<wca.dbo.ipo.AnnounceDate)
and wca.dbo.scexh.actflag<>'D'
and wca.dbo.scexh.liststatus<>'D'
and wca.dbo.scmst.primaryexchgcd = wca.dbo.scexh.exchgcd
