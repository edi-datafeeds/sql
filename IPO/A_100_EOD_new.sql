--filepath=o:\Datafeed\IPO\100\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.100
--suffix=
--fileheadertext=EDI_IPO_100_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fileheaderdate=yyyymmdd
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\IPO\100\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filenamealt=

--# 1

declare @logdate as datetime
set @logdate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)

SELECT 
wca.dbo.ipo.AnnounceDate as Created,
@logdate as Modified,
wca.dbo.ipo.IpoID+100000 as ID,
substring(wca.dbo.ipo.IPOStatus,1,1) as Status,
wca.dbo.ipo.Actflag,
wca.dbo.issur.Issuername,
wca.dbo.issur.CntryofIncorp,
wca.dbo.Scexh.ExchgCd,
wca.dbo.scmst.ISIN,
wca.dbo.sedol.Sedol,
wca.dbo.scmst.USCode,
wca.dbo.scexh.Localcode as Symbol,
wca.dbo.scmst.SecurityDesc as SecDescription,
wca.dbo.scmst.CurenCD as PVCurrency,
wca.dbo.scmst.ParValue,
wca.dbo.scmst.SecTyCD as Sectype,
wca.dbo.scexh.Lot as LotSize,
wca.dbo.ipo.SubPeriodFrom,
wca.dbo.ipo.SubperiodTo,
wca.dbo.ipo.MinSharesOffered,
wca.dbo.ipo.MaxSharesOffered,
wca.dbo.ipo.SharesOutstanding,
wca.dbo.ipo.TOFCurrency as Currency,
wca.dbo.ipo.SharePriceLowest,
wca.dbo.ipo.SharePriceHighest,
wca.dbo.ipo.ProposedPrice,
wca.dbo.ipo.InitialPrice,
'' as InitialTradedVolume,
wca.dbo.ipo.Underwriter,
wca.dbo.ipo.DealType,
wca.dbo.ipo.LawFirm,
wca.dbo.ipo.TransferAgent,
'' as IndusCode,
wca.dbo.ipo.FirstTradingDate,
'' as SecLevel,
wca.dbo.ipo.Notes
from wca.dbo.IPO
inner join wca.dbo.scmst on wca.dbo.ipo.secid = wca.dbo.scmst.secid
inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
inner join wca.dbo.scexh on wca.dbo.ipo.secid = wca.dbo.scexh.secid
inner join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid and wca.dbo.exchg.cntrycd = wca.dbo.sedol.cntrycd

where
(
wca.dbo.ipo.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
or (wca.dbo.scmst.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
     and getdate()-60<wca.dbo.ipo.AnnounceDate)
or (wca.dbo.scexh.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
     and getdate()-60<wca.dbo.ipo.AnnounceDate)
or (wca.dbo.sedol.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
     and getdate()-60<wca.dbo.ipo.AnnounceDate)
)
and wca.dbo.scmst.primaryexchgcd = wca.dbo.scexh.exchgcd
