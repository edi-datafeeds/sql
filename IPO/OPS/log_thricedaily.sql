use wca
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'thricedaily')
set @tdate = getdate()

if @fdate<@tdate-0.05
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'thricedaily', @fdate, @tdate)
  end
go