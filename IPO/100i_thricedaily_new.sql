--filepath=o:\Datafeed\IPO\100i\
--filenameprefix=
--filenamealt=
--filenamesql=select convert(varchar(30),max(wca.dbo.feedlog.Todate), 112) from wca.dbo.feedlog where wca.dbo.feedlog.feedfreq = 'thricedaily'
--fileextension=.100
--suffix=
--fileheadersql=select 'EDI_IPO_100_' + convert(varchar(30),max(wca.dbo.feedlog.Todate), 112) from wca.dbo.feedlog where wca.dbo.feedlog.feedfreq = 'thricedaily'
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\100i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 wca.dbo.tbl_opslog.seq FROM wca.dbo.tbl_opslog order by wca.dbo.tbl_opslog.acttime desc
--sevent=n
--shownulls=n
--filenamealt=

--# 1
select
'Created' as dummy,
'Modified' as dummy,
'ID' as dummy,
'Status' as dummy,
'Actflag' as dummy,
'Issuer' as dummy,
'CntryInc' as dummy,
'ExchgCd' as dummy,
'ISIN' as dummy,
'Sedol' as dummy,
'USCode' as dummy,
'Symbol' as dummy,
'SecDescription' as dummy,
'PVCurrency' as dummy,
'ParValue' as dummy,
'SecType' as dummy,
'LotSize' as dummy,
'SubPeriodFrom' as dummy,
'SubperiodTo' as dummy,
'MinSharesOffered' as dummy,
'MaxSharesOffered' as dummy,
'SharesOutstanding' as dummy,
'Currency' as dummy,
'SharePriceLowest' as dummy,
'SharePriceHighest' as dummy,
'ProposedPrice' as dummy,
'InitialPrice' as dummy,
'InitialTradedVolume' as dummy,
'Underwriter' as dummy,
'DealType' as dummy,
'LawFirm' as dummy,
'TransferAgent' as dummy,
'IndusCode' as dummy,
'FirstTradingDate' as dummy,
'SecLevel' as dummy,
'Notes'

--# 3
declare @logdate as datetime
set @logdate = (select max(wca.dbo.tbl_opslog.acttime) FROM wca.dbo.tbl_opslog)

use wca
SELECT 
wca.dbo.ipo.AnnounceDate as Created,
@logdate as Modified,
wca.dbo.ipo.IpoID+100000 as ID,
substring(wca.dbo.ipo.IPOStatus,1,1) as Status,
wca.dbo.ipo.Actflag,
wca.dbo.issur.Issuername,
wca.dbo.issur.CntryofIncorp,
wca.dbo.Scexh.ExchgCd,
wca.dbo.scmst.ISIN,
wca.dbo.sedol.Sedol,
wca.dbo.scmst.USCode,
wca.dbo.scexh.Localcode as Symbol,
wca.dbo.scmst.SecurityDesc as SecDescription,
wca.dbo.scmst.CurenCD as PVCurrency,
wca.dbo.scmst.ParValue,
wca.dbo.scmst.SecTyCD as Sectype,
wca.dbo.scexh.Lot as LotSize,
wca.dbo.ipo.SubPeriodFrom,
wca.dbo.ipo.SubperiodTo,
wca.dbo.ipo.MinSharesOffered,
wca.dbo.ipo.MaxSharesOffered,
wca.dbo.ipo.SharesOutstanding,
wca.dbo.ipo.TOFCurrency as Currency,
wca.dbo.ipo.SharePriceLowest,
wca.dbo.ipo.SharePriceHighest,
wca.dbo.ipo.ProposedPrice,
wca.dbo.ipo.InitialPrice,
'' as InitialTradedVolume,
wca.dbo.ipo.Underwriter,
wca.dbo.ipo.DealType,
wca.dbo.ipo.LawFirm,
wca.dbo.ipo.TransferAgent,
'' as IndusCode,
wca.dbo.ipo.FirstTradingDate,
'' as SecLevel,
wca.dbo.ipo.Notes
from wca.dbo.IPO
inner join wca.dbo.scmst on wca.dbo.ipo.secid = wca.dbo.scmst.secid
inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
inner join wca.dbo.scexh on wca.dbo.ipo.secid = wca.dbo.scexh.secid
inner join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid and wca.dbo.exchg.cntrycd = wca.dbo.sedol.cntrycd

where
(wca.dbo.ipo.acttime >=(select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.ipo.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
or wca.dbo.scmst.acttime >= (select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.scmst.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
     and getdate()-60<wca.dbo.ipo.AnnounceDate
or wca.dbo.scexh.acttime >= (select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.scexh.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
     and getdate()-60<wca.dbo.ipo.AnnounceDate
or wca.dbo.sedol.acttime >= (select max(FromDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
and wca.dbo.sedol.acttime<(select max(ToDate) from wca.dbo.FEEDLOG where FeedFreq = 'thricedaily')
     and getdate()-60<wca.dbo.ipo.AnnounceDate)
and wca.dbo.scexh.actflag<>'D'
and wca.dbo.scexh.liststatus<>'D'
and wca.dbo.scmst.primaryexchgcd = wca.dbo.scexh.exchgcd
