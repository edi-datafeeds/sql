--filepath=o:\Datafeed\IPO\100\
--filenameprefix=
--filenamesql=select convert(varchar(30),getdate()-3.2, 112) as Filenametext
--fileextension=.100
--suffix=
--fileheadertext=EDI_IPO_100_
--fileheadersql=select convert(varchar(30),getdate()-3.2, 112) as Filenametext
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\IPO\100\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filenamealt=

--# 1
use IPO
SELECT 
Created,
Modified,
ID,
Status,
Actflag,
Issuer,
CntryInc,
ExchgCd,
ISIN,
Sedol,
USCode,
Symbol,
SecDescription,
PVCurrency,
ParValue,
SecType,
LotSize,
SubPeriodFrom,
SubperiodTo,
MinSharesOffered,
MaxSharesOffered,
SharesOutstanding,
Currency,
SharePriceLowest,
SharePriceHighest,
ProposedPrice,
InitialPrice,
InitialTradedVolume,
Underwriter,
DealType,
LawFirm,
TransferAgent,
IndusCode,
FirstTradingDate,
SecLevel,
Notes
from IPO
where modified > getdate()-3.6
and modified < getdate()-2.6
and release = 'Y'