--filepath=o:\Datafeed\IPO\100i\
--filenameprefix=
--filenamealt=
--filenamesql=select convert(varchar(30),max(ipo.dbo.feedlog.Todate), 112) from ipo.dbo.feedlog where ipo.dbo.feedlog.feedfreq = 'thricedaily'
--fileextension=.100
--suffix=
--fileheadersql=select 'EDI_IPO_100_' + convert(varchar(30),max(ipo.dbo.feedlog.Todate), 112) from ipo.dbo.feedlog where ipo.dbo.feedlog.feedfreq = 'thricedaily'
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\IPO\100i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 wca.dbo.tbl_opslog.seq FROM wca.dbo.tbl_opslog order by wca.dbo.tbl_opslog.acttime desc
--sevent=n
--shownulls=n
--filenamealt=

--# 1
use IPO
SELECT 
Created,
Modified,
ID,
Status,
Actflag,
Issuer,
CntryInc,
ExchgCd,
ISIN,
Sedol,
USCode,
Symbol,
SecDescription,
PVCurrency,
ParValue,
SecType,
LotSize,
SubPeriodFrom,
SubperiodTo,
MinSharesOffered,
MaxSharesOffered,
SharesOutstanding,
Currency,
SharePriceLowest,
SharePriceHighest,
ProposedPrice,
InitialPrice,
InitialTradedVolume,
Underwriter,
DealType,
LawFirm,
TransferAgent,
IndusCode,
FirstTradingDate,
SecLevel,
Notes
from IPO
where
modified>=(select max(FromDate) from FEEDLOG where FeedFreq = 'thricedaily')
and modified<(select max(ToDate) from FEEDLOG where FeedFreq = 'thricedaily')
and release = 'Y'