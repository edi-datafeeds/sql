--filepath=o:\Datafeed\IPO\101_test\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.101
--suffix=
--fileheadertext=EDI_IPO_101_test_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\IPO\101_test\
--fieldheaders=
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filenamealt=

--# 1
select
'Created' as dummy,
'Modified' as dummy,
'ID' as dummy,
'Status' as dummy,
'Actflag' as dummy,
'Issuer' as dummy,
'CntryInc' as dummy,
'ExchgCd' as dummy,
'ISIN' as dummy,
'Sedol' as dummy,
'USCode' as dummy,
'Symbol' as dummy,
'SecDescription' as dummy,
'PVCurrency' as dummy,
'ParValue' as dummy,
'SecType' as dummy,
'LotSize' as dummy,
'SubPeriodFrom' as dummy,
'SubperiodTo' as dummy,
'MinSharesOffered' as dummy,
'MaxSharesOffered' as dummy,
'SharesOutstanding' as dummy,
'Currency' as dummy,
'SharePriceLowest' as dummy,
'SharePriceHighest' as dummy,
'ProposedPrice' as dummy,
'InitialPrice' as dummy,
'InitialTradedVolume' as dummy,
'Underwriter' as dummy,
'DealType' as dummy,
'LawFirm' as dummy,
'TransferAgent' as dummy,
'IndusCode' as dummy,
'FirstTradingDate' as dummy,
'SecLevel' as dummy,
'TOFCurrency' as dummy,
'TotalOfferSize' as dummy,
'Notes'

--# 2
use IPO
SELECT 
'IPO' as Datasource,
Created,
Modified,
ID,
Status,
Actflag,
Issuer,
CntryInc,
ExchgCd,
ISIN,
Sedol,
USCode,
Symbol,
SecDescription,
PVCurrency,
ParValue,
SecType,
LotSize,
SubPeriodFrom,
SubperiodTo,
MinSharesOffered,
MaxSharesOffered,
SharesOutstanding,
Currency,
SharePriceLowest,
SharePriceHighest,
ProposedPrice,
InitialPrice,
InitialTradedVolume,
Underwriter,
DealType,
LawFirm,
TransferAgent,
IndusCode,
FirstTradingDate,
SecLevel,
TOFCurrency,
TotalOfferSize,
Notes
from IPO
where
modified>=(select max(FromDate) from FEEDLOG where FeedFreq = 'EVENING')
and modified<(select max(ToDate) from FEEDLOG where FeedFreq = 'EVENING')
and release = 'Y'

--# 3

SELECT 
'WCA' as Datasource,
wca.dbo.ipo.AnnounceDate as Created,
wca.dbo.ipo.Acttime as Modified,
wca.dbo.ipo.IpoID as ID,
substring(wca.dbo.ipo.IPOStatus,1,1) as Status,
wca.dbo.ipo.Actflag,
wca.dbo.issur.Issuername,
wca.dbo.issur.CntryofIncorp,
wca.dbo.Scexh.ExchgCd,
wca.dbo.scmst.ISIN,
wca.dbo.sedol.Sedol,
wca.dbo.scmst.USCode,
wca.dbo.scexh.Localcode as Symbol,
wca.dbo.scmst.SecurityDesc as SecDescription,
wca.dbo.scmst.CurenCD as PVCurrency,
wca.dbo.scmst.ParValue,
wca.dbo.scmst.SecTyCD as Sectype,
wca.dbo.scexh.Lot as LotSize,
wca.dbo.ipo.SubPeriodFrom,
wca.dbo.ipo.SubperiodTo,
wca.dbo.ipo.MinSharesOffered,
wca.dbo.ipo.MaxSharesOffered,
wca.dbo.ipo.SharesOutstanding,
' ' as Currency,
wca.dbo.ipo.SharePriceLowest,
wca.dbo.ipo.SharePriceHighest,
wca.dbo.ipo.ProposedPrice,
wca.dbo.ipo.InitialPrice,
' ' as InitialTradedVolume,
wca.dbo.ipo.Underwriter,
wca.dbo.ipo.DealType,
wca.dbo.ipo.LawFirm,
wca.dbo.ipo.TransferAgent,
' ' as IndusCode,
wca.dbo.ipo.FirstTradingDate,
' ' as SecLevel,
wca.dbo.ipo.TOFCurrency,
wca.dbo.ipo.TotalOfferSize,
wca.dbo.ipo.Notes
from wca.dbo.IPO
inner join wca.dbo.scmst on wca.dbo.ipo.secid = wca.dbo.scmst.secid
inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
inner join wca.dbo.scexh on wca.dbo.ipo.secid = wca.dbo.scexh.secid
inner join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid and wca.dbo.exchg.cntrycd = wca.dbo.sedol.cntrycd

where
wca.dbo.ipo.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
