--filepath=o:\Datafeed\IPO\101_full\
--filenameprefix=
--filename=ALyymmdd
--filenamealt=
--fileextension=.101
--suffix=
--fileheadertext=EDI_IPO_101_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filenamealt=

--# 1
use wca
SELECT 
wca.dbo.ipo.AnnounceDate as Created,
wca.dbo.ipo.acttime as Modified,
wca.dbo.ipo.IpoID as ID,
substring(wca.dbo.ipo.IPOStatus,1,1) as Status,
wca.dbo.ipo.Actflag,
wca.dbo.issur.Issuername,
wca.dbo.issur.CntryofIncorp,
wca.dbo.Scexh.ExchgCd,
wca.dbo.scmst.ISIN,
wca.dbo.sedol.Sedol,
wca.dbo.scmst.USCode,
wca.dbo.scexh.Localcode as Symbol,
wca.dbo.scmst.SecurityDesc as SecDescription,
wca.dbo.scmst.CurenCD as PVCurrency,
wca.dbo.scmst.ParValue,
wca.dbo.scmst.SecTyCD as Sectype,
wca.dbo.scexh.Lot as LotSize,
wca.dbo.ipo.SubPeriodFrom,
wca.dbo.ipo.SubperiodTo,
wca.dbo.ipo.MinSharesOffered,
wca.dbo.ipo.MaxSharesOffered,
wca.dbo.ipo.SharesOutstanding,
wca.dbo.ipo.TOFCurrency as Currency,
wca.dbo.ipo.SharePriceLowest,
wca.dbo.ipo.SharePriceHighest,
wca.dbo.ipo.ProposedPrice,
wca.dbo.ipo.InitialPrice,
'' as InitialTradedVolume,
wca.dbo.ipo.Underwriter,
wca.dbo.ipo.DealType,
wca.dbo.ipo.LawFirm,
wca.dbo.ipo.TransferAgent,
'' as IndusCode,
wca.dbo.ipo.FirstTradingDate,
'' as SecLevel,
wca.dbo.ipo.TOFCurrency,
case when wca.dbo.ipo.TotalOfferSize<>'' then cast(cast(wca.dbo.ipo.TotalOfferSize as decimal(15,6))*1000000 as bigint) else null end as TotalOfferSize,
wca.dbo.ipo.Notes
from wca.dbo.IPO
inner join wca.dbo.scmst on wca.dbo.ipo.secid = wca.dbo.scmst.secid
inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
inner join wca.dbo.scexh on wca.dbo.ipo.secid = wca.dbo.scexh.secid
inner join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid and wca.dbo.exchg.cntrycd = wca.dbo.sedol.cntrycd
where wca.dbo.ipo.AnnounceDate between '2010/01/01' and '2010/12/31'
and wca.dbo.ipo.actflag <> 'D'
and wca.dbo.scmst.primaryexchgcd = wca.dbo.scexh.exchgcd
order by wca.dbo.ipo.AnnounceDate
