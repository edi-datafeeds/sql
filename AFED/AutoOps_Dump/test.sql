/*
MySQL Data Transfer
Source Host: 192.168.12.160
Source Database: AutoOps
Target Host: 192.168.12.160
Target Database: AutoOps
Date: 24/01/2011 21:28:50
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for test
-- ----------------------------
CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `testfield` double NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `test` VALUES ('1', '1');
INSERT INTO `test` VALUES ('2', '12');
INSERT INTO `test` VALUES ('3', '123.1');
INSERT INTO `test` VALUES ('4', '1234');
