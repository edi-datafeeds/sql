/*
MySQL Data Transfer
Source Host: 192.168.12.160
Source Database: AutoOps
Target Host: 192.168.12.160
Target Database: AutoOps
Date: 24/01/2011 21:28:05
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for FTPTrigger
-- ----------------------------
CREATE TABLE `FTPTrigger` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `host` varchar(45) NOT NULL default '',
  `port` int(10) unsigned NOT NULL default '0',
  `user` varchar(45) NOT NULL default '',
  `pass` varchar(45) NOT NULL default '',
  `folder` varchar(255) NOT NULL default '',
  `filemask` varchar(255) NOT NULL default '',
  `localonly` enum('N','Y') NOT NULL default 'Y',
  `endswith` enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `FTPTrigger` VALUES ('1', 'ftp.exchange-data.net', '21', 'barrel', 'FTP:K376:lcnb', '/Custom/Bahar/WCAFEED/', '\\YYYY\\MM\\DD.z01', 'N', 'N');
INSERT INTO `FTPTrigger` VALUES ('3', 'ftp.exchange-data.net', '21', 'barrel', 'FTP:K376:lcnb', '/Custom/Bahar/WCAFEED/', '\\YYYY\\MM\\DD.z02', 'N', 'N');
INSERT INTO `FTPTrigger` VALUES ('4', 'ftp.exchange-data.net', '21', 'barrel', 'FTP:K376:lcnb', '/Custom/Bahar/WCAFEED/', '\\YYYY\\MM\\DD.z03', 'N', 'N');
INSERT INTO `FTPTrigger` VALUES ('5', '194.169.1.31', '21', 'lsestaging\\sedolprod_edi', 'sEp3t3mB3r', '/SEDOL/Changes/Module 1/Daily/\\YYYY\\MM\\DD', '\\YYYY\\MM@180000_\\YYYY\\MM\\DD120000_IDC_1_TAB.zip', 'N', 'N');
INSERT INTO `FTPTrigger` VALUES ('6', '194.169.1.31', '21', 'lsestaging\\sedolprod_edi', 'sEp3t3mB3r', '/SEDOL/Changes/Module 1/Daily/\\YYYY\\MM\\DD', '\\YYYY\\MM@180000_\\YYYY\\MM\\DD120000_FDC_1_TAB.zip', 'N', 'N');
INSERT INTO `FTPTrigger` VALUES ('7', 'ftp.exchange-data.net', '21', 'barrel', 'FTP:K376:lcnb', '/IPO/100/', '\\YYYY\\MM\\DD.100', 'N', 'N');
INSERT INTO `FTPTrigger` VALUES ('15', '', '0', '', '', '\\\\fileserver01\\ops\\auto\\Tasks\\Monthly\\triggers\\', '1stTue.txt', 'Y', 'N');
INSERT INTO `FTPTrigger` VALUES ('16', '', '0', '', '', '\\\\fileserver01\\ops\\auto\\Tasks\\Monthly\\triggers\\', '20th.txt', 'Y', 'N');
INSERT INTO `FTPTrigger` VALUES ('11', '194.169.1.31', '21', 'lsestaging\\sedolprod_edi', 'sEp3t3mB3r', '/SEDOL/Changes/Module 1/Daily/\\YYYY\\MM\\DD', '\\YYYY\\MM@180000_\\YYYY\\MM\\DD120000_IDC_1_TAB.zip', 'N', 'N');
INSERT INTO `FTPTrigger` VALUES ('14', '', '0', '', '', '\\\\fileserver01\\ops\\auto\\Tasks\\Monthly\\triggers\\', '1st.txt', 'Y', 'N');
INSERT INTO `FTPTrigger` VALUES ('17', '', '0', '', '', '\\\\fileserver01\\ops\\auto\\Tasks\\Monthly\\triggers\\', 'lastday.txt', 'Y', 'N');
INSERT INTO `FTPTrigger` VALUES ('18', '', '0', '', '', '\\\\fileserver01\\ops\\datafeed\\SMF\\smffeed\\', '\\YYYY\\MM@180000_\\YYYY\\MM\\DD120000_IDC_1_TAB.zip', 'Y', 'N');
INSERT INTO `FTPTrigger` VALUES ('19', 'londonrates.gpsfx.com', '21', 'londonrates', 'y%g0T8Qw8', '/', '\\LondonRates.csv', 'N', 'N');
