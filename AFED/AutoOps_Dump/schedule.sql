/*
MySQL Data Transfer
Source Host: 192.168.12.160
Source Database: AutoOps
Target Host: 192.168.12.160
Target Database: AutoOps
Date: 24/01/2011 21:28:35
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for schedule
-- ----------------------------
CREATE TABLE `schedule` (
  `schedulename` varchar(45) NOT NULL default '' COMMENT 'Schedule Name',
  `description` varchar(255) default NULL COMMENT 'Description',
  `taskfile` varchar(255) NOT NULL default '' COMMENT 'Task File Path',
  `run_at` varchar(5) NOT NULL default '' COMMENT 'Runs At',
  `alert_at` varchar(5) NOT NULL default '' COMMENT 'Alert At',
  `days` varchar(14) NOT NULL default '' COMMENT 'Run on Days',
  `exclude_holidays` int(10) unsigned NOT NULL default '0' COMMENT 'Exclude Holidays',
  `fin_code` varchar(45) NOT NULL default '' COMMENT 'Holiday FIN Code',
  `last_run` int(10) unsigned default '0' COMMENT 'Last Run (int)',
  `acttime` datetime default '2000-01-01 00:00:00' COMMENT 'Action Time',
  `status` varchar(45) NOT NULL default '' COMMENT 'Status',
  `retry_count` int(10) unsigned NOT NULL default '0' COMMENT 'Retry Count',
  `tried` int(10) unsigned NOT NULL default '0' COMMENT 'Retried',
  `trigger_id` int(10) unsigned NOT NULL default '0' COMMENT 'Trigger Id',
  `deadlined` enum('Y','N') NOT NULL default 'N' COMMENT 'Execute on DealLine Queue',
  `aux1` enum('Y','N') NOT NULL default 'N' COMMENT 'Execute on Auxillary1 Queue',
  `aux2` enum('Y','N') NOT NULL default 'N' COMMENT 'Execute on Auxillary2 Queue',
  `ftp` enum('Y','N') NOT NULL default 'Y',
  `test` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`schedulename`),
  KEY `ix_run_at` (`run_at`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='schedule table for the automatic operation system';

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `schedule` VALUES ('Morning Ops (Daily)', null, '%TF%\\Morning\\MorningOps.tsk', '07:00', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 08:31:42', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('Tuesday Morning Ops', null, '%TF%\\Weekly\\TuesdayMorning\\TuesdayMorningOps.tsk', '08:32', '', '3,', '0', '', '20110118', '2011-01-18 08:40:03', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('SMF4 Full (IDC)', 'London Stock Exchange Security Master File.', '%TF%\\SMF\\smf4.tsk', '', '', '2,3,4,5,6', '1', 'UK_LSE', '20110124', '2011-01-24 12:39:16', 'Done', '0', '0', '11', 'N', 'N', 'N', 'N', 'Y');
INSERT INTO `schedule` VALUES ('WCA First Inc.', 'WCA First Inc. Webload', '%TF%\\WCAWebload\\WcaWebload_1.tsk', '08:30', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 08:22:52', 'Done', '0', '0', '1', 'Y', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('FTP File Cule', 'Culling of Expired Files on Remote FTP Servers', '%TF%\\Early Morning Tasks\\FTP File Cule.tsk', '04:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 04:52:47', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('WCA Second Inc', 'WCA Second Inc Webload', '%TF%\\WCAWebload\\WcaWebload_2.tsk', '', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 14:15:25', 'Done', '0', '0', '3', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('WCA Third Inc', 'WCA Third Inc Webload', '%TF%\\WCAWebload\\WcaWebload_3.tsk', '', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 19:21:28', 'Done', '0', '0', '4', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('EOD Batch 1', 'End Of Day Batch 1 (Starts Batch 2 on Aux and then Batch 3 on DLE)', '%TF%\\WCAEOD\\EndofDayOpsSection1.tsk', '', '', '', '0', '', '20110117', '2011-01-17 20:05:34', 'Done', '0', '0', '0', 'Y', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('Wednsday Ops', null, '%TF%\\Weekly\\WednesdayMorning\\Wednesday_ops.tsk', '09:00', '', '4,', '0', '', '20110119', '2011-01-19 08:58:24', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('EMTS_4', 'EMTS Loader 4', '%TF%\\EMTS\\EMTSLoader_4.tsk', '21:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 20:58:22', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'Y');
INSERT INTO `schedule` VALUES ('EMTS_1', 'EMTS Loader 1', '%TF%\\EMTS\\EMTSLoader_1.tsk', '13:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 13:02:22', 'Done', '0', '0', '0', 'N', 'N', 'Y', 'N', 'N');
INSERT INTO `schedule` VALUES ('SMF Incremental (FDC)', 'London Stock Exchange Security Master File.', '%TF%\\SMF\\smf_inc.tsk', '07:00', '', '2,3,4,5,6', '1', 'UK_LSE', '20110124', '2011-01-24 07:14:42', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('EOD Batch 2', 'End Of Day Batch 2', '%TF%\\WCAEOD\\EndofDayOpsSection2.tsk', '', '', '', '0', '', '20091222', '2009-12-22 20:30:17', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('EOD Batch 3', 'End Of Day Batch 3', '%TF%\\WCAEOD\\EndofDayOpsSection3.tsk', '', '', '', '0', '', '20100730', '2010-07-30 22:02:28', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('CAB Transfer 1', null, '%TF%\\CABTrans\\CABTrans.tsk', '12:00', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 11:58:21', 'Done', '0', '0', '0', 'N', 'N', 'Y', 'N', 'N');
INSERT INTO `schedule` VALUES ('CAB Transfer 2', null, '%TF%\\CABTrans\\CABTrans.tsk', '14:00', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 13:58:27', 'Done', '0', '0', '0', 'N', 'N', 'Y', 'N', 'N');
INSERT INTO `schedule` VALUES ('CAB Transfer 3', null, '%TF%\\CABTrans\\CABTrans.tsk', '16:00', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 16:02:13', 'Done', '0', '0', '0', 'N', 'N', 'Y', 'N', 'N');
INSERT INTO `schedule` VALUES ('CAB Transfer 4', null, '%TF%\\CABTrans\\CABTrans.tsk', '18:00', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 17:58:29', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('CAB Transfer 5', null, '%TF%\\CABTrans\\CABTrans.tsk', '20:00', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 21:24:24', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('123 Transfer 1', null, '%TF%\\123Trans\\123Trans.tsk', '11.45', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 10:58:13', 'Done', '0', '0', '0', 'Y', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('123 Transfer 2', null, '%TF%\\123Trans\\123Trans.tsk', '14.45', '', '2,3,4,5,6', '0', '', '20110124', '2011-01-24 13:58:44', 'Done', '0', '0', '0', 'Y', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('Fixed Income Parser 23:45', 'Fixed Income Parser - Himadri', '%TF%\\FixedIncPsr\\fi_psr.tsk', '23:45', '', '2,3,4,5,6,', '0', '', '20110121', '2011-01-21 23:43:29', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('Fixed Income Parser 21:00', 'Fixed Income Parser - Himadri', '%TF%\\FixedIncPsr\\fi_psr.tsk', '21:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 20:58:14', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('WCA_DAILY', 'WCA Daily', '%TF%\\WCADaily\\WCA_Daily.tsk', '', '', '', '0', '', '20101230', '2010-12-30 17:11:34', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('TESTING Exclude FIN HOLS', 'TEST', '%TF%\\test.tsk', '', '', '', '1', 'UK_LSE', '20110124', '2011-01-24 15:37:22', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('1st Working Day', '1st Working Day of the Month Ops', '%TF%\\Monthly\\1st\\1stDay.tsk', '', '', '2,3,4,5,6,', '0', '', '20110103', '2011-01-03 10:13:50', 'Done', '0', '0', '14', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('1st Tuesday', '1st Tuesday of the month ops', '%TF%\\Monthly\\1stTue\\test.tsk', '', '', '3', '0', '', '20110118', '2011-01-18 08:32:32', 'Done', '0', '0', '15', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('Monthly Checks', 'Monthly Ops Date Checks', '%TF%\\Monthly\\Monthly.tsk', '10:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 09:58:06', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('20th Working Day', '20th Working Day ofthe Month Ops', '%TF%\\Monthly\\20th\\20th.tsk', '', '', '2,3,4,5,6,', '0', '', '20110120', '2011-01-20 09:59:46', 'Done', '0', '0', '16', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('Monday WSO', null, '%TF%\\WSO\\MondayWso.tsk', '10:30', '', '2,', '0', '', '20110124', '2011-01-24 10:32:16', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('LastDay', 'Last working Day of the month', '%TF%\\Monthly\\LastDay\\LastDay.tsk', '11.30', '', '2,3,4,5,6,', '0', '', '20101231', '2010-12-31 11:32:27', 'Done', '0', '0', '17', 'Y', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('MarkitDivCon_12pm', null, '%TF%\\MarkitDivCon\\MarkitDivCon.tsk', '12:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 11:59:20', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('15022_steps', '15022_steps', '%TF%\\15022_STEPS\\15022_steps.tsk', '', '', '', '0', '', '20110124', '2011-01-24 20:09:13', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('MarkitDivCon_15pm', null, '%TF%\\MarkitDivCon\\MarkitDivCon.tsk', '15:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 14:59:19', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('MarkitDivCon_18pm', null, '%TF%\\MarkitDivCon\\MarkitDivCon.tsk', '18:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 17:59:25', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('MarkitDivCon_21pm', null, '%TF%\\MarkitDivCon\\MarkitDivCon.tsk', '21:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 20:59:42', 'Done', '0', '0', '0', 'N', 'N', 'Y', 'N', 'N');
INSERT INTO `schedule` VALUES ('MarkitDivCon_00am', null, '%TF%\\MarkitDivCon\\MarkitDivCon.tsk', '23:50', '', '2,3,4,5,6,', '0', '', '20110121', '2011-01-21 23:49:05', 'Done', '0', '0', '0', 'N', 'N', 'Y', 'N', 'N');
INSERT INTO `schedule` VALUES ('OPSFTP_FTPTRANS', 'OPS FTP', '%TF%\\FTPTrans\\opsftp_ftptrans.tsk', '', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 19:41:08', 'Done', '0', '0', '7', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('SSN', 'SSN', '%TF%\\SSN\\ssn.tsk', '', '', '', '0', '', '20110124', '2011-01-24 19:39:38', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('EMTS_2', 'EMTS Loader 2', '%TF%\\EMTS\\EMTSLoader_2.tsk', '16:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 16:02:01', 'Done', '0', '0', '0', 'N', 'N', 'Y', 'N', 'N');
INSERT INTO `schedule` VALUES ('EMTS_3', 'EMTS Loader 3', '%TF%\\EMTS\\EMTSLoader_3.tsk', '20:00', '', '2,3,4,5,6,', '0', '', '20110124', '2011-01-24 20:13:31', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'Y');
INSERT INTO `schedule` VALUES ('YL TEST FILE', 'YL TEST TASK FILE', '%TF%\\test\\yl_test.tsk', '', '', '', '0', '', '20110120', '2011-01-20 21:02:56', 'Done', '0', '0', '0', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('EOD Emailers', 'EOD WCA & WDI Emailers', '%TF%\\test\\emailers.tsk', '', '', '', '0', '', '20110121', '2011-01-21 20:55:00', 'Done', '0', '0', '0', 'N', 'N', 'N', 'N', 'N');
INSERT INTO `schedule` VALUES ('Citihfs_11_30pm', 'Citihfs_11_30pm', '%TF%\\15022_STEPS\\Citihfs_11_30pm.tsk', '00:15', '', '3,4,5,6,7', '0', '', '20110122', '2011-01-22 02:05:05', 'Done', '0', '0', '0', 'Y', 'N', 'N', 'N', 'N');
