update afed_data.source set forecastflag=0;
update afed_data.source set forecastflag=1 where src_id in (
                select DISTINCT src_id from afed_data.list_series_value where vt_id=3 and effective_date > NOW() 
);
update afed_data.sub_source set forecastflag=0;
update afed_data.sub_source set forecastflag=1 where sbsrc_id in (
select DISTINCT sbsrc_id from afed_data.list_series_value where vt_id=3 and effective_date > NOW()
);