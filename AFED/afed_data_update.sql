# This Query is to disable all dublicate except the last one
  update afed_data.list_series_value set active=0 , actflag='D',ModifiedBy=1,acttime=Now(),ActionBy=1,ActionComment ='This is a duplicate record'
  WHERE value_id NOT IN 
      (SELECT ID FROM 
         (SELECT MAX(value_id) AS ID 
          FROM afed_data.list_series_value          
          GROUP BY 
  	src_id,
  	sbsrc_id,
  	lbl_id,
  	type_cd,
  	cntrycd,
  	sc_id,
  	unt_id,	
  	publication_date,
  	effective_date,
  	frq_id,
  	vt_id,
  	dist_id,
  value
 ) X) ;
 
 #Insert all inactive records into deletion table(list_series_value_deletions)
INSERT IGNORE INTO list_series_value_deletions
SELECT * FROM afed_data.list_series_value where active=0;


#Delete all inactive record from original source table(list_series_value)
Delete From list_series_value where active=0;