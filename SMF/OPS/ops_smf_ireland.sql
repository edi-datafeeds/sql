insert into automate_tasks.test_smf (isin, issuer_name,sedol, sectype, sec_desc, cinc_code, creg_code, pv_currency, actdate)
select distinct
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.security.Sedol,
smf4.security.Sectype,
smf4.security.Longdesc,
smf4.issuer.cinccode,
smf4.security.cregcode ,
smf4.security.UnitofqCurrCode,
current_timestamp()
from smf4.security
inner join smf4.issuer on smf4.security.issuerid = smf4.issuer.issuerid
where ( substring(smf4.security.isin,1,2) IN ('IE','LU','FR','CH','AT','BE','ES','IT','NL','GB','GR','PT')  )
AND (smf4.security.sectype='CL') AND `Statusflag` = 'T'
AND smf4.security.`HeaderDate` >= ( current_timestamp - INTERVAL 1 HOUR )
order by isin;