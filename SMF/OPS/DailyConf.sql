USE SMF4
DECLARE @FROMDATE AS DATETIME
SET @FROMDATE=(SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
DECLARE @TODATE AS DATETIME
SET @TODATE=(SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
if exists (select * from sysobjects where name = 'DailyConf')
	drop table DailyConf
SELECT Security.SecurityID, 
tempActtime =
case
  WHEN issuer.actdate < security.actdate THEN security.actdate
else
  issuer.actdate
END, 
tempActflag =
case
  WHEN issuer.actdate < security.actdate THEN security.actflag
else
  issuer.actflag
END


into DailyConf
FROM Security
INNER JOIN Issuer ON Security.issuerID = Issuer.IssuerID
WHERE 
(Security.Actdate >= @FROMDATE and Security.Actdate<=@TODATE
AND (Security.Confirmation is not null))
or
(Security.headerdate >=@FROMDATE
and security.headerdate<=@TODATE
AND (Security.Confirmation is not null))


UNION
SELECT Security.SecurityID,
tempActtime =
case
  WHEN issuer.actdate < security.actdate THEN security.actdate
else
  issuer.actdate
END, 
tempActflag =
case
  WHEN issuer.actdate < security.actdate THEN security.actflag
else
  issuer.actflag
END
FROM  Issuer
INNER JOIN Security ON Security.issuerID = Issuer.IssuerID
WHERE 
(Issuer.Actdate >= @FROMDATE and Issuer.Actdate<=@TODATE
AND (Security.Actflag <> 'D')
AND (Security.Statusflag <> 'D')
AND (Security.Confirmation is not null))
or
(Issuer.headerdate >=@FROMDATE
and Issuer.headerdate<=@TODATE
AND (Security.Actflag <> 'D')
AND (Security.Statusflag <> 'D')
AND (Security.Confirmation is not null))

go
