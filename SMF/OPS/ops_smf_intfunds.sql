insert into xdes.intf_ref (isin, issuername, security_description, cntryofregister,  acttime )
select distinct
smf4.security.Isin,
smf4.issuer.Issuername,
smf4.security.Longdesc,
smf4.security.cregcode ,
current_timestamp()
from smf4.security
inner join smf4.issuer on smf4.security.issuerid = smf4.issuer.issuerid
where ( substring(smf4.security.isin,1,2) IN ('IE','LU','FR','CH','AT','BE','ES','IT','NL','GB','GR','PT')  )
AND (smf4.security.sectype='CL') AND `Statusflag` = 'T'
AND smf4.security.`HeaderDate` >= ( current_timestamp - INTERVAL 1 HOUR )
order by isin;

insert into xdes.intf_ref (security_id,isin, issuername,umbrellaname, security_description, sedol,local_code,cfi_code,cntryofincorp,cntryofregister, currency, mic_code,listing_country,acttime )
select distinct
s.securityid,
s.Isin,
i.Issuername as issuername,
i.Issuername as umbrellaname,
s.Longdesc as security_description,
s.Sedol,
m.TIDM As local_code,
s.Cficode as cfi_code,
i.Cinccode as cntryofincorp ,
s.Cregcode as  cntryofregister,
s.UnitofqCurrCode as currency,
s.CntryCD as listing_country,
m.MIC as mic_code,
current_timestamp()
from smf4.security as s
inner join smf4.issuer as i on s.issuerid = i.issuerid
Left Outer JOIN smf4.Market as m ON s.SecurityID = m.SecurityID  And s.OPOL=m.MIC
where ( substring(s.isin,1,2) IN ('IE','LU','FR','CH','AT','BE','ES','IT','NL','GB','GR','PT')  )
AND (s.sectype='CL') AND s.Statusflag = 'T'
AND s.HeaderDate >= ( current_timestamp - INTERVAL 1 HOUR )
AND s.actflag = 'I'
order by s.isin;

UPDATE xdes.intf_ref AS u 
JOIN smf4.security AS s ON u.isin = s.isin
INNER JOIN smf4.issuer AS i ON s.issuerid = i.issuerid
Left Outer JOIN smf4.Market as m ON s.SecurityID = m.SecurityID  And s.OPOL=m.MIC
SET 
u.issuername = i.issuername, 
u.umbrellaname = i.Issuername,
u.security_description = s.Longdesc, 
u.Sedol=s.Sedol,
u.local_code=m.TIDM ,
u.cfi_code=s.Cficode,
u.cntryofincorp=i.Cinccode,
u.cntryofregister = s.cregcode,  
u.currency=s.UnitofqCurrCode,
u.listing_country=s.CntryCD,
u.mic_code=m.MIC,
u.acttime = current_timestamp(),
u.actflag = 'U'
WHERE ( substring(s.isin,1,2) IN ('IE','LU','FR','CH','AT','BE','ES','IT','NL','GB','GR','PT')  )
	AND (s.sectype='CL') AND s.`Statusflag` = 'T'
	AND s.`HeaderDate` >= ( current_timestamp - INTERVAL 1 HOUR )
	AND s.actflag = 'U';
