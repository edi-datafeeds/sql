--filepath=o:\Datafeed\Smf\AIGSMF\
--filenameprefix=AIGSMF
--filename=
--filenamealt=
--fileextension=.TXT
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=n:\Smf\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use smf4
SELECT
'"'+replace(Issuer.Issuername,'"','`')+'"' as """Issuer_Name""",
'"'+replace(Security.Longdesc,'"','`')+'"' as """Description""",
'"'+Security.Statusflag+'"' as """Status""",
case when Security.Sectype = 'AF' or Security.Sectype = 'AQ' or Security.Sectype = 'AS' 
         or Security.Sectype = 'AT' or Security.Sectype = 'AU' THEN UPPER('"E"')
     ELSE UPPER('"A"')
END as """Sectype""",
case when security.Formflag is null then '""' ELSE'"'+Security.Formflag+'"' END as """Security_Form""",
'"'+Issuer.Cinccode+'"' as """Country_of_Inc""",
'"'+Security.Cregcode+'"' as """Reg_Country""",
case when security.isin is null then '""' ELSE'"'+Security.Isin+'"' END as """ISIN""",
'"'+Security.Sedol+'"' as """SEDOL"""
FROM Security
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN Sectype ON Security.sectype = Sectype.Code 
WHERE 
sectype.TypeGroup='Equity'
and security.confirmation = 'C'
AND security.confirmation is not null
and security.actflag<>'D'
and (cinccode = 'AL'
or cinccode = 'AM'
or cinccode = 'AN'
or cinccode = 'AT'
or cinccode = 'BE'
or cinccode = 'BG'
or cinccode = 'CH'
or cinccode = 'CY'
or cinccode = 'CZ'
or cinccode = 'DE'
or cinccode = 'DK'
or cinccode = 'ES'
or cinccode = 'FI'
or cinccode = 'FR'
or cinccode = 'GB'
or cinccode = 'GR'
or cinccode = 'HR'
or cinccode = 'ID'
or cinccode = 'IT'
or cinccode = 'LI'
or cinccode = 'LU'
or cinccode = 'NL'
or cinccode = 'NO'
or cinccode = 'PL'
or cinccode = 'PT'
or cinccode = 'RU'
or cinccode = 'SE'
or cinccode = 'US'
or cinccode = 'ZZ'
or cinccode = 'AU'
or cinccode = 'NZ')
and (cregcode = 'AL'
or cregcode = 'AM'
or cregcode = 'AN'
or cregcode = 'AT'
or cregcode = 'BE'
or cregcode = 'BG'
or cregcode = 'CH'
or cregcode = 'CY'
or cregcode = 'CZ'
or cregcode = 'DE'
or cregcode = 'DK'
or cregcode = 'ES'
or cregcode = 'FI'
or cregcode = 'FR'
or cregcode = 'GB'
or cregcode = 'GR'
or cregcode = 'HR'
or cregcode = 'ID'
or cregcode = 'IT'
or cregcode = 'LI'
or cregcode = 'LU'
or cregcode = 'NL'
or cregcode = 'NO'
or cregcode = 'PL'
or cregcode = 'PT'
or cregcode = 'RU'
or cregcode = 'SE'
or cregcode = 'US'
or cregcode = 'ZZ'
or cregcode = 'AU'
or cregcode = 'NZ')
