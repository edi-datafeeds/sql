--filepath=o:\Datafeed\SMF\120\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.120
--suffix=
--fileheadertext=EDI_SMF_120_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\120\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4
SELECT
Security.Sedol,
Security.Isin, 
Issuer.Issuername, 
replace(Security.Longdesc,'|',' ') as longdesc,
Security.Cregcode,
Security.Statusflag, 
Security.OPOL, 
Security.Sectype,
udr.dbo.udrnew.DRtype,
Security.Restrictions
FROM Dailyconf
INNER JOIN Security ON Security.SecurityID = Dailyconf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN Udr.dbo.Udrnew ON Security.Sedol = udr.dbo.udrnew.DRSedol
ORDER BY Security.Sedol
