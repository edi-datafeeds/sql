--filepath=o:\Datafeed\SMF\CB\
--filenameprefix=CB
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=FW
--archive=y
--archivepath=n:\No_Cull_Feeds\CB\
--fieldheaders=n
--filetidy=y
--fwoffsets=LS7,LS12,LS35,LS40,LS4,LS2,LS2,LS2,LS1,LS14
--incremental=n
--sevent=n
--shownulls=n


--# 1
use smf4
SELECT
Security.Sedol,
security.Isin,
Issuer.Issuername,
security.Longdesc,
security.OPOL,
'  ' as dummy1,
security.Cregcode,
'  ' as dummy2,
Security.Statusflag,
security.Eventcode as dummy3
FROM DailyConf
INNER JOIN Security ON Security.SecurityID = DailyConf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
ORDER BY Security.Sedol
