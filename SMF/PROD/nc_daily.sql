--filepath=o:\Datafeed\SMF\CB\
--filenameprefix=NC
--filename=yymmdd
--filenamealt=
--fileextension=.TXT
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\CB\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
use wca
select
'NC '+substring(cast(year(getdate()) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(getdate()) as char(2))),len(month(getdate())),2)+
substring('0'+rtrim(cast(day(getdate()) as char(2))),len(day(getdate())),2)+' '+
substring(cast(year(getdate()) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(getdate()) as char(2))),len(month(getdate())),2)+
substring('0'+rtrim(cast(day(getdate()) as char(2))),len(day(getdate())),2)


--# 2
use wca
SELECT
'"'+v50f_620_issuer_name_change.Sedol+'"',
'"'+substring(upper(replace(replace(v50f_620_issuer_name_change.issoldname,',',''),'.',''))+'                                  ',1,35)+'"',
'"'+substring(upper(replace(replace(v50f_620_issuer_name_change.issnewname,',',''),'.',''))+'                                  ',1,35)+'"'
from v50f_620_issuer_name_change
where namechangedate>getdate()-1 and namechangedate<getdate()
and v50f_620_issuer_name_change.Sedol <>''
and v50f_620_issuer_name_change.Sedol is not null
and v50f_620_issuer_name_change.relatedevent<>'CLEAN'
ORDER BY v50f_620_issuer_name_change.Sedol


--# 3
use wca
select 
substring('000000'+rtrim(cast(count(v50f_620_issuer_name_change.Sedol)+2 as char(7))),len(count(v50f_620_issuer_name_change.Sedol)+2),7)
+'ENDOFFILE'
from v50f_620_issuer_name_change
where namechangedate>getdate()-1 and namechangedate<getdate()
and v50f_620_issuer_name_change.Sedol <>''
and v50f_620_issuer_name_change.Sedol is not null
and v50f_620_issuer_name_change.relatedevent<>'CLEAN'
