--filepath=o:\Datafeed\SMF\010\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.010
--suffix=
--fileheadertext=H11YYYYMMDD0000001684I
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\010\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4
SELECT
Issuer.Actdate,
Issuer.Actflag,
Issuer.IssuerID,
Issuer.Issuername,
Issuer.Cinccode,
'' as dummytab
FROM Issuer
Where (Issuer.headerdate > getdate()-1)
AND (Issuer.Confirmation='C')
and (Issuer.Confirmation is not null)
