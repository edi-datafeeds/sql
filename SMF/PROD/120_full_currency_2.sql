--filepath=o:\Datafeed\SMF\120\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.120
--suffix=_2
--fileheadertext=EDI_SMF_120_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\SMF\120\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n

--# 1
smf4.security.sedol as SEDOL,
smf4.security.isin as ISIN,
smf4.issuer.issuername as ISSUERNAME,
replace(smf4.security.longdesc,'|',' ') as LONGDESC,
smf4.security.cregcode as CREGCODE,
smf4.security.statusflag as STATUSFLAG,
smf4.security.opol as OPOL,
smf4.security.sectype as SECTYPE,
wca.dprcp.DRTYPE as DRtype,
smf4.security.RESTRICTIONS,
case when length(smf4.security.unitofqcurrcode)<>3 then '' else smf4.security.unitofqcurrcode end as UNITOFQCURRCODE,
smf4.security.confirmation as CONFIRMATION
from smf4.security
left outer join smf4.issuer on smf4.security.issuerid = smf4.issuer.issuerid
left outer join wca.sedol on smf4.security.sedol = wca.sedol.sedol
left outer join wca.dprcp on wca.sedol.secid = wca.dprcp.secid
where
security.actflag<>'D' and
securityid>102100000 and securityid<115000001
-- select count(securityid) from security
-- _0 and securityid<1000001
-- _1 and securityid>1000000 and securityid<102100001
-- _2 and securityid>102100000 and securityid<115000001
-- _3 and securityid>115000000 and securityid<120000001
-- _4 and securityid>120000000

