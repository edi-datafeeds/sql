--filepath=o:\Datafeed\SMF\ml\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.ml
--suffix=
--fileheadertext=
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\ml\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT
Dailyconf.SecurityID,
Dailyconf.TempActflag,
Issuer.Issuername,
Issuer.Cinccode,
'' as dummy1,
Security.Cficode,
Security.OPOL,
Security.SecType,
Security.Isin,
Security.IssuerID,
' ' as Tidisplay,
replace(Security.Longdesc,'|',' ') as longdesc,
Security.Statusflag,
Security.Sedol,
Security.Unitofq,
Security.UnitofqCurrCode,
Security.Formflag,
'' as dummy2
FROM DailyConf
INNER JOIN Security ON Security.SecurityID = DailyConf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
where sectype <> 'SP'
