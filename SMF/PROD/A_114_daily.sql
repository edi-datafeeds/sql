--filepath=o:\Datafeed\SMF\114\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.114
--suffix=
--fileheadertext=EDI_SMF_114_YYYYMMDD
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\114\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4

SELECT
Security.Actflag,
Security.Actdate,
Issuer.Issuername,
Security.Sedol,
Security.Isin,
Security.Longdesc,
Security.CloseDate,
Security.CouponIR,
Security.PVCurrenCD,
Security.Parvalue,
' ' as Classflag,
Security.Sectype,
Security.EventDate,
Security.Eventcode,
Security.Statusflag,
Security.Amtrans1,
Security.Amtrans2,
Security.Amtrans3,
Security.Amtrans4
FROM Security
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
left outer join sectype on security.sectype = sectype.code
where (sectype.typegroup = 'Debt' or sectype.typegroup is null)
and closedate between getdate() and getdate()+5
and closedate is not null
ORDER BY Security.Sedol


/*Format
field		Datatype(maxwidth)	Description
Actflag,	Char(1)			Action (I=Insert, U=Update, D=Delete, F=Full Alignment)
Acttime,	Date(10)		Action time (Last modified)
Issuername,	Varchar(35)		Issuername
Sedol,		Char(7)			Sedol
Isin,		Char(12)		ISIN
Longdesc,	Varchar(40)		Security Description
CloseDate,	Date(10)		Redemption Date
CouponIR,	Decimal(18,8)		Coupon Interest rate
PVCurrenCD,	Char(3)			Par Value ISO Currency code
Parvalue,	Decimal(18,8)		Par Value
Classflag,	Char(1)			Security Class (*)
SecType,	Char(2)			Security Type (*)
EventDate,	Date(10)		Security Event Date
Eventcode,	Char(2)			Security Event Code (*)
Statusflag,	Char(1)			Status Flag (T=Trading, C=Coded, D=Defunct)
Amtrans1,	Decimal(18,8)		Amount of Transfer 1
Amtrans2,	Decimal(18,8)		Amount of Transfer 2
Amtrans3,	Decimal(18,8)		Amount of Transfer 3
Amtrans4	Decimal(18,8)		Amount of Transfer 4
(*) See SMF feed documentation for all possible values and meanings
Date format = YYYY/MM/DD
*/ 