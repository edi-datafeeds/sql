--filepath=o:\Datafeed\SMF\CB\
--filenameprefix=FD
--filename=yymmdd
--filenamealt=
--fileextension=.TXT
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\CB\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use smf4
select
'FD ' + substring(cast(year(getdate()) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(getdate()) as char(2))),len(month(getdate())),2)+
substring('0'+rtrim(cast(day(getdate()) as char(2))),len(day(getdate())),2)+' '+
substring(cast(year(getdate()) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(getdate()) as char(2))),len(month(getdate())),2)+
substring('0'+rtrim(cast(day(getdate()) as char(2))),len(day(getdate())),2)

--# 2
use smf4
SELECT
'"'+Security.Sedol+'"',
'"'+substring(replace(Issuer.Issuername,'"','`')+'                                  ',1,35)+'"',
'"'+substring(replace(security.Longdesc,'"','`')+'                                       ',1,40)+'"',
'"'+substring(replace(security.background,'"','`')+'                           ',1,27)+'"',
'"'+substring(cast(year(security.eventdate) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(security.eventdate) as char(2))),len(month(security.eventdate)),2)+
substring('0'+rtrim(cast(day(security.eventdate) as char(2))),len(day(security.eventdate)),2)+'"'
FROM dailyconf
INNER JOIN Security ON Security.SecurityID = dailyconf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
where 
security.statusflag = 'D' or tempactflag='D'
ORDER BY Security.Sedol


--# 3
use smf4
select 
substring('000000'+rtrim(cast(count(dailyconf.securityid)+2 as char(7))),len(count(dailyconf.securityid)+2),7)
+'ENDOFFILE'
from dailyconf
inner join security on dailyconf.securityid = security.securityid
where 
security.statusflag = 'D' or tempactflag='D'
