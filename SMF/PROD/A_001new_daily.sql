--filepath=o:\datafeed\smf\001new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.001
--suffix=
--fileheadertext=H11YYYYMMDD0000001684I
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\001\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4
SELECT
Issuer.Actdate,
Issuer.Actflag,
Issuer.IssuerID,
Issuer.Cinccode,
Issuer.BusinessID,
'' as Dummy,
Issuer.Issuername
FROM Issuer
Where
(Issuer.Actdate >= (SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and Issuer.Actdate<=(SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
AND (Issuer.Confirmation='C')
and (Issuer.Confirmation is not null))
or
(Issuer.headerdate >= (SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and Issuer.headerdate <= (SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
AND (Issuer.Confirmation='C')
and (Issuer.Confirmation is not null))

