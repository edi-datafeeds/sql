--filepath=o:\Datafeed\Smf\129\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.129
--suffix=
--fileheadertext=EDI_SMF_129_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\129\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use smf4
SELECT
Security.Sedol,
Security.Isin, 
Issuer.Issuername, 
Security.Longdesc,
Security.Cregcode,
Security.Statusflag, 
Security.OPOL, 
Security.Sectype,
udr.dbo.udrnew.DRtype,
Security.Restrictions
FROM Dailyconf
INNER JOIN Security ON Security.SecurityID = Dailyconf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN Udr.dbo.Udrnew ON Security.Sedol = udr.dbo.udrnew.DRSedol
WHERE
(security.confirmation = 'C')
AND (security.confirmation is not null)
ORDER BY Security.Sedol
