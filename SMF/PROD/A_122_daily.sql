--filepath=o:\Datafeed\Smf\122\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.122
--suffix=
--fileheadertext=EDI_SMF_122_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\122\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT
dailyconf.*,
smf4.dbo.security.Actflag,
smf4.dbo.security.Actdate,
smf4.dbo.security.SecurityID,
smf4.dbo.security.PrevSedol,
smf4.dbo.security.IssuerID,
smf4.dbo.security.Sedol,
smf4.dbo.security.Isin,
smf4.dbo.security.Cregcode,
smf4.dbo.security.CouponIR,
smf4.dbo.security.Parvalue,
smf4.dbo.security.PvCurrenCD,
smf4.dbo.security.UnderlyingInstr,
smf4.dbo.security.UnderlyingIssuer,
smf4.dbo.security.StrikePrice,
smf4.dbo.security.StrikePriceCurrency,
smf4.dbo.security.Formflag,
smf4.dbo.security.SecType,
smf4.dbo.security.Amtrans1,
smf4.dbo.security.Amtrans2,
smf4.dbo.security.Amtrans3,
smf4.dbo.security.Amtrans4,
smf4.dbo.security.Unitofq,
smf4.dbo.security.UnitofqCurrCode,
smf4.dbo.security.Cficode,
smf4.dbo.security.CloseDate,
smf4.dbo.security.ClosingDateType,
smf4.dbo.security.Restrictions,
smf4.dbo.security.OPOL,
smf4.dbo.security.PrimaryListing,
smf4.dbo.security.NewInfosource,
smf4.dbo.security.Longdesc,
smf4.dbo.security.Shortdesc,
smf4.dbo.security.Background,
smf4.dbo.security.Accrualflag,
smf4.dbo.security.Assentflag,
smf4.dbo.security.Eventcode,
smf4.dbo.security.Eventdate,
smf4.dbo.security.Statusflag,
smf4.dbo.security.Confirmation,
smf4.dbo.security.CAID,
smf4.dbo.security.CAType,
smf4.dbo.security.CreDate,
smf4.dbo.security.HeaderDate,
' ' as fundflag,
' ' as cregstat,
' ' as dealflag,
' ' as Localcode,
' ' as classflag,
' ' as sysindic,
smf4.dbo.market.Tidm as Tidisplay,
' ' as Previssname,
smf4.dbo.Security.OPOL as MIC,
substring(smf4.dbo.Security.OPOL,2,3) as Infosource,
' ' as typeflag,
smf4.dbo.issuer.*
FROM DailyConf
INNER JOIN Security ON Security.SecurityID = DailyConf.SecurityID
INNER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
left outer join market on security.securityid = market.securityid
            and security.opol = market.mic
            and (security.opol = 'XLON' or security.opol = 'AIMX')
