--filepath=o:\datafeed\smf\001new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.002
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\002new\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use smf4
SELECT
Security.Actdate,
Security.Actflag,
Security.SecurityID,
Security.Amtrans1,
Security.Amtrans2,
Security.Amtrans3,
Security.Amtrans4,
Security.Assentflag,
Security.PrimaryListing,
Security.Cficode,
Security.Cregcode,
Security.Restrictions,
Security.Confirmation,
Security.Background,
upper(Security.OPOL),
Security.Isin,
Security.IssuerID,
'' as StrikePriceCurrency,
Security.Closedate,
substring(Security.ClosingDateType,1,10) as ClosingDateType,
Security.Longdesc,
'' as Shortdesc,
Security.Eventcode,
EvntDate = CASE WHEN (Security.EventDate < '1950/01/01') THEN NULL
             ELSE Security.EventDate END,
Security.Formflag,
Security.Statusflag,
Security.SecType,
Security.Sedol,
'' as ParValue,
Market.Tidm as Tidisplay,
Security.Unitofq,
Security.UnitofqCurrCode,
Security.Accrualflag
FROM Security
inner join sectype on Security.Sectype=sectype.code
left outer join market on security.securityid = market.securityid
            and 'XLON' = market.mic
where
(Security.Actdate >= (SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and security.Actdate<=(SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and Security.Confirmation is not null
and Security.longdesc not like '%STOCK CONNECT%'
and (sectype.typegroup <> 'Other' or
 security.sectype = 'CG' or
 security.sectype = 'CJ' or
 security.sectype = 'CL'))
or
(Security.headerdate >= (SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and security.headerdate <= (SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and Security.Confirmation is not null
and Security.longdesc not like '%STOCK CONNECT%'
and (sectype.typegroup <> 'Other' or
 security.sectype = 'CG' or
 security.sectype = 'CJ' or
 security.sectype = 'CL')
 )