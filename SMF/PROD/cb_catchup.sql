--filepath=o:\Datafeed\SMF\
--filenameprefix=CB
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=
--outputstyle=FW
--archive=y
--archivepath=n:\SMF\CB\
--fieldheaders=n
--filetidy=n
--fwoffsets=LS7,LS12,LS35,LS40,LS4,LS2,LS2,LS2,LS1,LS14
--incremental=n
--sevent=n
--shownulls=n



--# 1
use smf4
SELECT
Security.Sedol,
security.Isin,
Issuer.Issuername,
security.Longdesc,
security.OPOL,
'  ' as dummy1,
security.Cregcode,
'  ' as dummy2,
Security.Statusflag,
security.Eventcode as dummy3
FROM security
INNER JOIN issuer ON Security.issuerid = issuer.issuerID
where
(issuer.actdate between '2010/12/01' and '2011/02/15'
or security.actdate between '2010/12/01' and '2011/02/15')
