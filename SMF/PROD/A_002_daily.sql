--filepath=o:\Datafeed\SMF\001\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.002
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyymmdd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\002\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
use smf4
SELECT
Security.Actdate,
Security.Actflag,
Security.SecurityID,
Security.Amtrans1,
Security.Amtrans2,
Security.Amtrans3,
Security.Amtrans4,
Security.Assentflag,
' ' as fundflag,
Security.Cficode,
Security.Cregcode,
' ' as cregstat,
' ' as dealflag,
Security.Background,
upper(Security.OPOL),
Security.Isin,
Security.IssuerID,
' ' as Localcode,
Security.Closedate,
' ' as classflag,
Security.Longdesc,
Security.Shortdesc,
Security.Eventcode,
EvntDate = CASE WHEN (Security.EventDate < '1950/01/01') THEN NULL
             ELSE Security.EventDate END,
Security.Formflag,
Security.Statusflag,
Security.SecType,
Security.Sedol,
' ' as sysindic,
' ' as TiDisplay,
Security.Unitofq,
Security.UnitofqCurrCode,
Security.Accrualflag
FROM Security
where
(Security.Actdate >= (SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and security.Actdate<=(SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and (Security.Confirmation is not null)
and (Security.longdesc not like '%STOCK CONNECT%'))
or
(Security.headerdate >= (SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and security.headerdate <= (SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and (Security.Confirmation is not null)
and (Security.longdesc not like '%STOCK CONNECT%'))
