--filepath=o:\Datafeed\SMF\120i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.120
--suffix=
--fileheadertext=EDI_SMF_120_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\120i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select 1 as seq
--sevent=n
--shownulls=n


--# 1
use smf4
DECLARE @FROMDATE AS DATETIME
SET @FROMDATE=(SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
SELECT
Security.Sedol as SEDOL,
Security.Isin as ISIN, 
Issuer.Issuername as ISSUERNAME, 
replace(Security.Longdesc,'|',' ') as LONGDESC,
Security.Cregcode as CREGCODE,
Security.Statusflag as STATUSFLAG, 
Security.OPOL as OPOL, 
Security.Sectype as SECTYPE,
wca.dbo.dprcp.DRtype,
Security.RESTRICTIONS,
case when len(Security.UnitofQcurrcode)<>3 then '' else Security.UnitofQcurrcode end as UNITOFQCURRCODE,
security.confirmation as CONFIRMATION
FROM Dailyallsmf
INNER JOIN Security ON Security.SecurityID = Dailyallsmf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN wca.dbo.sedol ON Security.Sedol = wca.dbo.sedol.Sedol
LEFT OUTER JOIN wca.dbo.dprcp ON wca.dbo.sedol.secid = wca.dbo.dprcp.secid
where
1=2
ORDER BY Security.Sedol
