--filepath=o:\Datafeed\SMF\021\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.021
--suffix=
--fileheadertext=H11YYYYMMDD0000001684S
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=HH:MM:SS
--forcetime=n
--filefootertext=T110000000000
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\021\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
use smf4
SELECT
Security.Actdate,
Security.Actflag,
Security.SecurityID,
Security.Cregcode,
upper(Security.OPOL),
Security.Isin,
Security.IssuerID,
Security.Longdesc,
Security.Eventcode,
Security.Statusflag,
Security.Sedol,
Security.Sectype,
'' as dummytab
FROM Security
Where 
(Security.Actdate >= (SELECT MAX(FROMDATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY')
and security.Actdate<=(SELECT MAX(TODATE) FROM FEEDLOG WHERE FEEDFREQ='SMFDAILY'
AND (Security.Confirmation='C')
and (Security.Confirmation is not null))
or
(Security.headerdate >= '2009/09/08'
and security.Actdate<'2009/09/09'
AND (Security.Confirmation='C')
and (Security.Confirmation is not null)))
