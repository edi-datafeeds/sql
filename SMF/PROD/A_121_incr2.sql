--filepath=o:\Datafeed\SMF\121i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.121
--suffix=
--fileheadertext=EDI_SMF_121_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\121i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select 1 as seq
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT
Security.Sedol,
Security.Isin, 
Issuer.Issuername, 
replace(Security.Longdesc,'|',' ') as longdesc,
Security.Cregcode,
Security.Statusflag, 
Security.OPOL, 
Security.Sectype,
wca.dbo.dprcp.DRtype,
Security.Restrictions,
Security.UnitofQcurrcode,
Security.Confirmation
FROM Dailyallsmf
INNER JOIN Security ON Security.SecurityID = Dailyallsmf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN wca.dbo.sedol ON Security.Sedol = wca.dbo.sedol.Sedol
LEFT OUTER JOIN wca.dbo.dprcp ON wca.dbo.sedol.secid = wca.dbo.dprcp.secid
ORDER BY Security.Sedol
