--filepath=o:\Datafeed\SMF\120\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.120
--suffix=_3
--fileheadertext=EDI_SMF_120_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\SMF\120\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT
Security.Sedol,
Security.Isin, 
Issuer.Issuername, 
replace(Security.Longdesc,'|',' ') as longdesc,
Security.Cregcode,
Security.Statusflag, 
Security.OPOL, 
Security.Sectype,
wca.dbo.dprcp.DRtype,
Security.Restrictions,
Security.UnitofQcurrcode
FROM security
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN wca.dbo.sedol ON Security.Sedol = wca.dbo.sedol.Sedol
LEFT OUTER JOIN wca.dbo.dprcp ON wca.dbo.sedol.secid = wca.dbo.dprcp.secid
where
security.confirmation='C'
and securityid>115000000 and securityid<120000001
-- select count(securityid) from security
-- _0 and securityid<1000001
-- _1 and securityid>1000000 and securityid<102100001
-- _2 and securityid>102100000 and securityid<115000001
-- _3 and securityid>115000000 and securityid<120000001
-- _4 and securityid>120000000

