--filepath=o:\Datafeed\SMF\121i\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.121
--suffix=
--fileheadertext=EDI_SMF_121_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\SMF\121i\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select 1 as seq
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT
Security.Sedol as SEDOL,
Security.Isin as ISIN, 
Issuer.Issuername as ISSUERNAME, 
replace(Security.Longdesc,'|',' ') as LONGDESC,
Security.Cregcode as CREGCODE,
Security.Statusflag as STATUSFLAG, 
Security.OPOL as OPOL, 
Security.Sectype as SECTYPE,
wca.dbo.dprcp.DRtype,
Security.RESTRICTIONS,
case when len(Security.UnitofQcurrcode)<>3 then '' else Security.UnitofQcurrcode end as UNITOFQCURRCODE,
security.confirmation as CONFIRMATION
FROM security
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
LEFT OUTER JOIN wca.dbo.sedol ON Security.Sedol = wca.dbo.sedol.Sedol
LEFT OUTER JOIN wca.dbo.dprcp ON wca.dbo.sedol.secid = wca.dbo.dprcp.secid
where
Security.headerdate>(select max(feeddate) from tbl_opslog)
and issuer.issuerid is not null
union
SELECT
Security.Sedol as SEDOL,
Security.Isin as ISIN, 
Issuer.Issuername as ISSUERNAME, 
replace(Security.Longdesc,'|',' ') as LONGDESC,
Security.Cregcode as CREGCODE,
Security.Statusflag as STATUSFLAG, 
Security.OPOL as OPOL, 
Security.Sectype as SECTYPE,
wca.dbo.dprcp.DRtype,
Security.RESTRICTIONS,
case when len(Security.UnitofQcurrcode)<>3 then '' else Security.UnitofQcurrcode end as UNITOFQCURRCODE,
security.confirmation as CONFIRMATION
FROM issuer
LEFT OUTER JOIN security ON issuer.IssuerID = security.IssuerID 
LEFT OUTER JOIN wca.dbo.sedol ON Security.Sedol = wca.dbo.sedol.Sedol
LEFT OUTER JOIN wca.dbo.dprcp ON wca.dbo.sedol.secid = wca.dbo.dprcp.secid
where
issuer.headerdate>(select max(feeddate) from tbl_opslog)
and security.securityid is not null