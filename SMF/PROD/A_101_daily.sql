--filepath=o:\Datafeed\SMF\101\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.101
--suffix=
--fileheadertext=EDI_SMF_101_yyyymmdd
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\smf\101\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
use smf4
SELECT
Dailyconf.tempActtime AS Actdate,
Dailyconf.tempActflag AS Actflag,
Security.SecurityID,
Security.Cregcode AS CountryofReg,
' ' as newInfosource,
Security.OPOL,
Security.Isin,
Security.IssuerID,
Issuer.Issuername,
Security.Longdesc AS SecurityDescription,
Security.Sedol,
Security.Statusflag,
Security.Eventcode,
Issuer.Cinccode AS CountryofInc,
Security.Sectype,
smf4.security.DomesticListingIndicator as PrimaryListing
FROM Dailyconf
INNER JOIN Security ON Security.SecurityID = Dailyconf.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
ORDER BY Security.SecurityID

/*Format
field		Datatype(maxwidth)	Description
Acttime,	Date(10)		Action time (Last modified)
Actflag,	Char(1)			Action (I=Insert, U=Update, D=Delete, F=Full Alignment)
SecurityID,	Int(10)			Unique Global internal number at the sedol level
Cregcode,	Char(2)			ISO country of Reg Code (*)
Newinfosource,	Char(4)			New infosource
OPOL,		Char(3)			Official place of listing (MIC or UNL or FMQ - fund manager quote)
Isin,		Char(12)		ISIN
IssuerID,	Int(10)			Unique Global internal number for LSE issuer
Issuername,	Varchar(35)		Issuername
Longdesc,	Varchar(40)		Security Description
Sedol,		Char(7)		        Sedol code
Statusflag,	Char(1)		        Trading, Coded, Defunct
Eventcode,	Char(2)			Security Event Code (*)
Cinccode,	Char(2)			ISO country of Incorporation Code (*)
Sectype,	Char(2)			New style Security Type (*)
PrimaryListing	Char(1)			1 = Primary

(*) See SMF feed documentation for all possible values and meanings
Date format = YYYY/MM/DD
*/ 