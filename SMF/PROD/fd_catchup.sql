--filepath=o:\Datafeed\SMF\
--filenameprefix=FD
--filename=yymmdd
--filenamealt=
--fileextension=.TXT
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=,
--outputstyle=
--archive=n
--archivepath=n:\SMF\CB\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use smf4
select
'FD ' + substring(cast(year(getdate()) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(getdate()) as char(2))),len(month(getdate())),2)+
substring('0'+rtrim(cast(day(getdate()) as char(2))),len(day(getdate())),2)+' '+
substring(cast(year(getdate()) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(getdate()) as char(2))),len(month(getdate())),2)+
substring('0'+rtrim(cast(day(getdate()) as char(2))),len(day(getdate())),2)

--# 2
use smf4
SELECT
'"'+Security.Sedol+'"',
'"'+substring(replace(Issuer.Issuername,'"','`')+'                                  ',1,35)+'"',
'"'+substring(replace(security.Longdesc,'"','`')+'                                       ',1,40)+'"',
'"'+substring(replace(security.background,'"','`')+'                           ',1,27)+'"',
'"'+substring(cast(year(security.eventdate) as varchar(4)),3,2)+
substring('0'+rtrim(cast(month(security.eventdate) as char(2))),len(month(security.eventdate)),2)+
substring('0'+rtrim(cast(day(security.eventdate) as char(2))),len(day(security.eventdate)),2)+'"'
FROM Security
inner JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
where 
security.statusflag = 'D'
and (issuer.actdate between '2010/12/01' and '2011/02/15'
or security.actdate between '2010/12/01' and '2011/02/15')


--# 3
use smf4
select 
substring('000000'+rtrim(cast(count(security.securityid)+2 as char(7))),len(count(security.securityid)+2),7)
+'ENDOFFILE'
FROM Security
inner JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
where 
security.statusflag = 'D'
and (issuer.actdate between '2010/12/01' and '2011/02/15'
or security.actdate between '2010/12/01' and '2011/02/15')
