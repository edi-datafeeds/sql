--filepath=o:\Datafeed\Smf\131\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.131
--suffix=
--fileheadertext=EDI_SMF_131_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\131\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
use smf4
SELECT
security.Sedol,
security.PrevSedol,
security.Actflag,
security.Actdate,
security.Confirmation,
security.Isin,
issuer.Issuername,
issuer.Cinccode,
security.Longdesc,
security.Cregcode,
security.Statusflag,
security.OPOL,
security.Sectype,
security.Eventdate,
eventtype.Description,
security.Sectype,
security.Background
FROM dailyconf
INNER JOIN security ON security.SecurityID = dailyconf.SecurityID
LEFT OUTER JOIN issuer ON security.IssuerID = issuer.IssuerID
LEFT OUTER JOIN eventtype ON security.Eventcode = eventtype.code
WHERE
(security.sectype = 'CL'
or security.sectype = 'CK')
and cinccode = 'GB'