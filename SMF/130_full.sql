--filepath=o:\Datafeed\Smf\130_full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.130
--suffix=
--fileheadertext=EDI_SMF_130_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Smf\130_full\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use smf4
SELECT
Security.Actdate,
case when Security.credate = Security.headerdate then upper('I') else Security.Actflag end as Actflag,
Issuer.IssuerID,
replace(replace(replace(Issuer.Issuername,char(39),'`'),'"','`'),',',';') as Issuername,
Issuer.Cinccode,
Issuer.BusinessID,
replace(replace(replace(Issuer.Comment,char(39),'`'),'"','`'),',',';') as Comment,
Issuer.IssuerStatus,
Security.Sedol,
Security.Isin,
replace(replace(replace(Security.Longdesc,char(39),'`'),'"','`'),',',';') as Longdesc,
Security.Cregcode,
Security.OPOL,
Security.Sectype,
Security.Statusflag,
Security.Eventcode,
Security.Eventdate,
Security.Prevsedol,
Security.CloseDate,
Security.ClosingDateType,
Security.PrimaryListing,
replace(replace(replace(Security.Background,char(39),'`'),'"','`'),',',';') as Background,
Security.AssentFlag,
Security.Confirmation
FROM Security
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
WHERE 
security.sectype<>'CM'
and  security.sectype<>'CN'
and issuer.actflag<>'D'
