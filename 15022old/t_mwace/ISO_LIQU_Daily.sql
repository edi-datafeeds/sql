--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_LIQU
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=LIQ
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select    
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + rtrim(cast(t_EQ_LIQU.EventID as char(10))) + ' and sEvent = '+ char(39) + 'LIQ'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
t_EQ_LIQU.Changed,
'select  LiquidationTerms As Notes FROM LIQ WHERE LIQID = '+ cast(t_EQ_LIQU.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'113'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_LIQU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_LIQU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_LIQU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_LIQU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_LIQU.RdDate <>'' AND t_EQ_LIQU.RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_LIQU.RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From t_EQ_LIQU
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

