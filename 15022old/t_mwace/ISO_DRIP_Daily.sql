--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DRIP
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_DVCA_DVSE.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(t_EQ_DVCA_DVSE.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_DVCA_DVSE.Actflag_1 = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DVCA_DVSE.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DVCA_DVSE.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DVCA_DVSE.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DVCA_DVSE.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DRIP',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DVCA_DVSE.Exdate <>'' AND t_EQ_DVCA_DVSE.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_DVCA_DVSE.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN t_EQ_DVCA_DVSE.Recdate <>'' AND t_EQ_DVCA_DVSE.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DVCA_DVSE.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_DVCA_DVSE.DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN t_EQ_DVCA_DVSE.DivPeriodCD='UN' OR  t_EQ_DVCA_DVSE.DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN t_EQ_DVCA_DVSE.DivPeriodCD='SPL' OR  t_EQ_DVCA_DVSE.DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     ELSE ':22F::DIVI//REGR'
     END,
/* START In case of Franking */
CASE WHEN t_EQ_DVCA_DVSE.UnFrankDiv <> ''
                 AND t_EQ_DVCA_DVSE.UnFrankDiv IS NOT NULL
                    AND t_EQ_DVCA_DVSE.CurenCD_1 IS NOT NULL
     THEN ':92J::GRSS//UNFR/'+t_EQ_DVCA_DVSE.CurenCD_1+ltrim(cast(t_EQ_DVCA_DVSE.UnFrankDiv as char(15)))
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVCA_DVSE.FrankDiv <> ''
             AND t_EQ_DVCA_DVSE.FrankDiv IS NOT NULL
                    AND t_EQ_DVCA_DVSE.CurenCD_1 IS NOT NULL
     THEN ':92J::GRSS//FLFR/'+t_EQ_DVCA_DVSE.CurenCD_1+ltrim(cast(t_EQ_DVCA_DVSE.FrankDiv as char(15)))
     ELSE ''
     END AS COMMASUB,
/* END In case of Franking */
/* RDNotes populated by rendering programme :70E::TXNR// */
/*/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_DVCA_DVSE.RdID as char(16)) as Notes, */*/ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN t_EQ_DVCA_DVSE.CurenCD_1 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVCA_DVSE.CurenCD_1
     END,
':17B::DFLT//Y',
CASE WHEN t_EQ_DVCA_DVSE.DripLastDate <>'' AND t_EQ_DVCA_DVSE.DripLastDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_DVCA_DVSE.DripLastDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_DVCA_DVSE.Paydate <>'' 
             AND t_EQ_DVCA_DVSE.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVCA_DVSE.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN t_EQ_DVCA_DVSE.Netdividend_1 <> ''
                 AND t_EQ_DVCA_DVSE.Netdividend_1 IS NOT NULL
     THEN ':92F::NETT//'+t_EQ_DVCA_DVSE.CurenCD_1+ltrim(cast(t_EQ_DVCA_DVSE.Netdividend_1 as char(15)))
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN t_EQ_DVCA_DVSE.Grossdividend_1 <> ''
             AND t_EQ_DVCA_DVSE.Grossdividend_1 IS NOT NULL
     THEN ':92F::GRSS//'+t_EQ_DVCA_DVSE.CurenCD_1+ltrim(cast(t_EQ_DVCA_DVSE.Grossdividend_1 as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
CASE WHEN t_EQ_DVCA_DVSE.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVCA_DVSE.Taxrate_1 as char(15))
     ELSE ':92K::TAXR//UKWN' 
     END as COMMASUB,
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//DRIP',
':17B::DFLT//N',
CASE WHEN t_EQ_DVCA_DVSE.Fractions_1 = 'C'
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_DVCA_DVSE.Fractions_1 = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_DVCA_DVSE.Fractions_1 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_DVCA_DVSE.Fractions_1 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_DVCA_DVSE.Fractions_1 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_DVCA_DVSE.Fractions_1 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN t_EQ_DVCA_DVSE.DripLastDate <>'' AND t_EQ_DVCA_DVSE.DripLastDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_DVCA_DVSE.DripLastDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_DVCA_DVSE.DripReinvPrice <>'' 
             AND t_EQ_DVCA_DVSE.DripReinvPrice IS NOT NULL
     THEN ':90B::REIN//ACTU/'+t_EQ_DVCA_DVSE.CurenCD_1+ltrim(cast(t_EQ_DVCA_DVSE.DripReinvPrice as char(15)))
     ELSE ':90E::REIN//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB1,
CASE WHEN t_EQ_DVCA_DVSE.DripPaydate <>'' AND t_EQ_DVCA_DVSE.DripPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVCA_DVSE.DripPaydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_DVCA_DVSE
WHERE
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and Actflag <> ''
and DripCntryCD <> ''
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

