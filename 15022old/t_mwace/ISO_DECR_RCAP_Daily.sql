--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DECR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_DECR_RCAP.Changed,
'select  RcapNotes As Notes FROM RCAP WHERE RCAPID = '+ cast(t_EQ_DECR_RCAP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'146'+t_EQ_DECR_RCAP.EXCHGID+cast(t_EQ_DECR_RCAP.Seqnum as char(1)) as CAref1,
t_EQ_DECR_RCAP.EventID as CAREF2,
t_EQ_DECR_RCAP.SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_DECR_RCAP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DECR_RCAP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DECR_RCAP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DECR_RCAP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DECR_RCAP.RecDate <>'' and t_EQ_DECR_RCAP.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DECR_RCAP.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_DECR_RCAP.EffectiveDate <>'' and t_EQ_DECR_RCAP.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_DECR_RCAP.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN t_EQ_DECR_RCAP.CSPYDate <>'' and t_EQ_DECR_RCAP.CSPYDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DECR_RCAP.CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN t_EQ_DECR_RCAP.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_DECR_RCAP.CurenCD
     END,
':17B::DFLT//Y',
CASE WHEN t_EQ_DECR_RCAP.CashBak <>'' AND t_EQ_DECR_RCAP.CashBak IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+t_EQ_DECR_RCAP.CurenCD+
          +substring(t_EQ_DECR_RCAP.CashBak,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as Commasub,
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_DECR_RCAP
LEFT OUTER JOIN wca.dbo.CAPRD on t_EQ_DECR_RCAP.SecID = wca.dbo.CAPRD.SecID
                    and t_EQ_DECR_RCAP.EffectiveDate = wca.dbo.CAPRD.EffectiveDate
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and wca.dbo.CAPRD.Secid is null
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

