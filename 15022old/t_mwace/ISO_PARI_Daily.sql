--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_PARI
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_PARI.Changed,
'' as ISOHDR,
':16R:GENL',
'124'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_PARI.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_PARI.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_PARI.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_PARI.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PARI',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_PARI.AssimilationDate <>'' and t_EQ_PARI.AssimilationDate is not null
     THEN ':98A::PPDT//'+CONVERT ( varchar , t_EQ_PARI.AssimilationDate,112)
     ELSE ':98B::PPDT//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
/* CASE WHEN t_EQ_PARI.RatioNew <>''
            AND t_EQ_PARI.RatioNew IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_PARI.RatioNew as char (15)))+
                    '/'+rtrim(cast(t_EQ_PARI.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB, 
next line is a placebo, needs a ratioold,rationew added to parent */
':92K::NEWO//UKWN',
CASE WHEN t_EQ_PARI.AssimilationDate <>'' and t_EQ_PARI.AssimilationDate is not null
     THEN ':98A::PPDT//'+CONVERT ( varchar , t_EQ_PARI.AssimilationDate,112)
     ELSE ':98B::PPDT//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_PARI
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

