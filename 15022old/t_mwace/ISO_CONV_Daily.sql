--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_CONV
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_CONV.Changed,
'select  ConvNotes As Notes FROM CONV WHERE ConvID = '+ cast(t_EQ_CONV.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'125'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN t_EQ_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':25D::PROC//COMP', 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_CONV.FromDate <>'' AND t_EQ_CONV.FromDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_CONV.FromDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN t_EQ_CONV.ToDate <>'' AND t_EQ_CONV.ToDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_CONV.EventID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONV',
CASE WHEN t_EQ_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN t_EQ_CONV.ToDate <>'' AND t_EQ_CONV.ToDate IS NOT NULL
       AND t_EQ_CONV.FromDate <>'' AND t_EQ_CONV.FromDate IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , t_EQ_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , t_EQ_CONV.ToDate,112)
     WHEN t_EQ_CONV.FromDate <>'' AND t_EQ_CONV.FromDate IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , t_EQ_CONV.FromDate,112)+'/UKWN'
     WHEN t_EQ_CONV.ToDate <>'' AND t_EQ_CONV.ToDate IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , t_EQ_CONV.ToDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN t_EQ_CONV.Price <>'' AND t_EQ_CONV.Price IS NOT NULL
     THEN ':90B::EXER//ACTU/'+t_EQ_CONV.CurenCD+
          +substring(t_EQ_CONV.Price,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN t_EQ_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN t_EQ_CONV.RatioNew <>''
            AND t_EQ_CONV.RatioNew IS NOT NULL
          AND t_EQ_CONV.RatioOld <>''
            AND t_EQ_CONV.RatioOld IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(t_EQ_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_CONV
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and Actflag <> ''
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

