--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DECR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_DECR_CAPRD.Changed,
'select  CapRdNotes As Notes FROM CAPRD WHERE CAPRDID = '+ cast(t_EQ_DECR_CAPRD.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'148'+t_EQ_DECR_CAPRD.EXCHGID+cast(t_EQ_DECR_CAPRD.Seqnum as char(1)) as CAref1,
t_EQ_DECR_CAPRD.EventID as CAREF2,
t_EQ_DECR_CAPRD.SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_DECR_CAPRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DECR_CAPRD.RecDate <>'' and t_EQ_DECR_CAPRD.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_DECR_CAPRD.EffectiveDate <>'' and t_EQ_DECR_CAPRD.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN t_EQ_DECR_CAPRD.PayDate <>'' and t_EQ_DECR_CAPRD.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':70E::TXNR//',
'Old Par Value ' + t_EQ_DECR_CAPRD.OldParValue,
'New Par Value ' + t_EQ_DECR_CAPRD.NewParValue,
':16S:CADETL',
CASE WHEN newTFB1<>''
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN newTFB1<>''
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN newTFB1<>''
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN newTFB1<>''
     THEN ':17B::DFLT//Y'
     ELSE ''
     END,
CASE WHEN newTFB1<>''
     THEN ':35B:' + newTFB1
     ELSE ''
     END,
CASE WHEN newTFB1<>''
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
'' as Notes,
'-}$'
From t_EQ_DECR_CAPRD
LEFT OUTER JOIN wca.dbo.RCAP on t_EQ_DECR_CAPRD.SecID = wca.dbo.RCAP.SecID
                    and t_EQ_DECR_CAPRD.EffectiveDate = wca.dbo.RCAP.EffectiveDate
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and wca.dbo.RCAP.Secid is null
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

