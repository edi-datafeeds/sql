--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXWA
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_EXWA.Changed,
'select  WartmNotes As Notes FROM WARTM WHERE SECID = '+ cast(t_EQ_EXWA.SecID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'300'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_EXWA.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_EXWA.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_EXWA.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_EXWA.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXWA',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_EXWA.ExpirationDate <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , t_EQ_EXWA.ExpirationDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN t_EQ_EXWA.ExpirationDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_EXWA.ExpirationDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_EXWA.ToDate <>'' AND t_EQ_EXWA.ToDate IS NOT NULL
       AND t_EQ_EXWA.FromDate <>'' AND t_EQ_EXWA.FromDate IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , t_EQ_EXWA.FromDate,112)+'/'
          +CONVERT ( varchar , t_EQ_EXWA.ToDate,112)
     WHEN t_EQ_EXWA.FromDate <>'' AND t_EQ_EXWA.FromDate IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , t_EQ_EXWA.FromDate,112)+'/UKWN'
     WHEN t_EQ_EXWA.ToDate <>'' AND t_EQ_EXWA.ToDate IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , t_EQ_EXWA.ToDate,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN t_EQ_EXWA.PricePerShare <> ''
     THEN ':90B::MRKT//ACTU/'+CurenCD+rtrim(cast(t_EQ_EXWA.PricePerShare as char(15)))
     ELSE ':90E::MRKT//UKWN'
     END as COMMASUB,
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN t_EQ_EXWA.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_EXWA.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_EXWA.StrikePrice <> ''
     THEN ':90B::EXER//ACTU/'+CurenCD+rtrim(cast(t_EQ_EXWA.StrikePrice as char(15)))
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN exerTFB1<>''
     THEN ':35B:' + exerTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_EXWA.RatioNew <>''
            AND t_EQ_EXWA.RatioNew IS NOT NULL
          AND t_EQ_EXWA.RatioOld <>''
            AND t_EQ_EXWA.RatioOld IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_EXWA.RatioNew as char (15)))+
                    '/'+rtrim(cast(t_EQ_EXWA.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
/* CASE WHEN t_EQ_EXWA.Paydate <>'' 
               AND t_EQ_EXWA.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXWA.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END, */
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_EXWA
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and EventID is not null
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

