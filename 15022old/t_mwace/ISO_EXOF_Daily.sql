--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXOF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_EXOF_CTX.Changed,
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(t_EQ_EXOF_CTX.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'127'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_EXOF_CTX.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_EXOF_CTX.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_EXOF_CTX.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_EXOF_CTX.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_EXOF_CTX.EndDate <>'' AND t_EQ_EXOF_CTX.EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_EXOF_CTX.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_EXOF_CTX.EndDate <>'' AND t_EQ_EXOF_CTX.EndDate IS NOT NULL
       AND t_EQ_EXOF_CTX.StartDate <>'' AND t_EQ_EXOF_CTX.StartDate IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.StartDate,112)+'/'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.EndDate,112)
     WHEN t_EQ_EXOF_CTX.StartDate <>'' AND t_EQ_EXOF_CTX.StartDate IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.StartDate,112)+'/UKWN'
     WHEN t_EQ_EXOF_CTX.EndDate <>'' AND t_EQ_EXOF_CTX.EndDate IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.EndDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:ISIN UKWN'
     END,
':92D::NEWO//1,/1,',
/* Currently no suitable paydate
CASE WHEN t_EQ_EXOF_CTX.Paydate <>'' 
             AND t_EQ_EXOF_CTX.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXOF_CTX.Paydate,112)
     WHEN t_EQ_EXOF_CTX.paydate <>'' 
             AND t_EQ_EXOF_CTX.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXOF_CTX.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_EXOF_CTX
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and Actflag <> ''
and upper(eventtype)<>'CLEAN'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

