--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_MRGR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=MRGR
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + ltrim(cast(t_EQ_MRGR.EventID as char(10))) + ' and sEvent = '+ char(39) + 'MRGR'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
t_EQ_MRGR.Changed,
'select  MrgrTerms As Notes FROM MRGR WHERE RdID = '+ cast(t_EQ_MRGR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'126'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_MRGR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_MRGR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_MRGR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_MRGR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MRGR',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_MRGR.EffectiveDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_MRGR.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN t_EQ_MRGR.RecDate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_MRGR.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_MRGR.RDID as char(16)) as Notes, */ 
':16S:CADETL',
/* START OF CASH BLOCK */
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From t_EQ_MRGR
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

