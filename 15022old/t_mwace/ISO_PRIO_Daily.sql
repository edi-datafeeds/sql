--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_PRIO
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_PRIO.Changed,
'select  PrfNotes As Notes FROM PRF WHERE RDID = '+ cast(t_EQ_PRIO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'110'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_PRIO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_PRIO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_PRIO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_PRIO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_PRIO.Exdate <>'' AND t_EQ_PRIO.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_PRIO.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN t_EQ_PRIO.Recdate <>'' AND t_EQ_PRIO.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_PRIO.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
 /* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_PRIO.EventID as char(16)) as Notes, */ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
':17B::DFLT//N',
CASE WHEN t_EQ_PRIO.CurenCD <> '' AND t_EQ_PRIO.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + t_EQ_PRIO.CurenCD
     END,
CASE WHEN t_EQ_PRIO.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_PRIO.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_PRIO.EndSubscription <>'' AND t_EQ_PRIO.EndSubscription IS NOT NULL
       AND t_EQ_PRIO.StartSubscription <>'' AND t_EQ_PRIO.StartSubscription IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , t_EQ_PRIO.StartSubscription,112)+'/'
          +CONVERT ( varchar , t_EQ_PRIO.EndSubscription,112)
     WHEN t_EQ_PRIO.StartSubscription <>'' AND t_EQ_PRIO.StartSubscription IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , t_EQ_PRIO.StartSubscription,112)+'/UKWN'
     WHEN t_EQ_PRIO.EndSubscription <>'' AND t_EQ_PRIO.EndSubscription IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , t_EQ_PRIO.EndSubscription,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN t_EQ_PRIO.MaxPrice <>'' AND t_EQ_PRIO.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+t_EQ_PRIO.CurenCD+
          +substring(t_EQ_PRIO.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_PRIO.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_PRIO.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_PRIO.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_PRIO.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_PRIO.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_PRIO.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN t_EQ_PRIO.RatioNew <>''
            AND t_EQ_PRIO.RatioNew IS NOT NULL
          AND t_EQ_PRIO.RatioOld <>''
            AND t_EQ_PRIO.RatioOld IS NOT NULL
     THEN ':92D::NEWO//'+ltrim(cast(t_EQ_PRIO.RatioNew as char (15)))+
                    '/'+ltrim(cast(t_EQ_PRIO.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN t_EQ_PRIO.paydate2 <>'' 
             AND t_EQ_PRIO.paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_PRIO.paydate2,112)
     WHEN t_EQ_PRIO.paydate <>'' 
             AND t_EQ_PRIO.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_PRIO.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_PRIO
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and Actflag <> ''
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3


