--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DVOP
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_DVOP.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(t_EQ_DVOP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_DVOP.Actflag_1 = ''
     THEN ':23G:CANC'
     WHEN t_EQ_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVOP',
CASE WHEN Divtype_1 = 'B' AND DIVTYPE_2 = ''
                AND DIVTYPE_3 = '' AND DIVTYPE_4 = '' 
                AND DIVTYPE_5 = '' AND DIVTYPE_6 = ''
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//CHOS'
     END,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DVOP.Exdate <> '' 
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_DVOP.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN t_EQ_DVOP.Recdate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_DVOP.DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN t_EQ_DVOP.DivPeriodCD='UN' OR  t_EQ_DVOP.DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN t_EQ_DVOP.DivPeriodCD='SPL' OR  t_EQ_DVOP.DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     ELSE ':22F::DIVI//REGR'
     END,
/* START In case of Franking */
CASE WHEN t_EQ_DVOP.UnFrankDiv <> ''
                 AND t_EQ_DVOP.UnFrankDiv IS NOT NULL
     THEN ':92J::GRSS//UNFR/'+CurenCD_1+ltrim(cast(t_EQ_DVOP.UnFrankDiv as char(15)))
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.FrankDiv <> ''
             AND t_EQ_DVOP.FrankDiv IS NOT NULL
     THEN ':92J::GRSS//FLFR/'+CurenCD_1+ltrim(cast(t_EQ_DVOP.FrankDiv as char(15)))
     END as COMMASUB,
/* END In case of Franking */     
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_DVOP.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* CASH OPTION START_1 */
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
     THEN ':13A::CAON//001'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_1 <> '' AND t_EQ_DVOP.CurenCD_1 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_1
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C' and t_EQ_DVOP.DefaultOpt_1='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_1 = 'C'
     THEN ':17B::DFLT//N'
ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN Divtype_1 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
            AND  t_EQ_DVOP.Netdividend_1 <> ''
     THEN ':92F::NETT//'+CurenCD_1+ltrim(cast(t_EQ_DVOP.Netdividend_1 as char(15)))
     WHEN Divtype_1 = 'C'
            AND  t_EQ_DVOP.Netdividend_1 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
            AND t_EQ_DVOP.Grossdividend_1 <> ''
     THEN ':92F::GRSS//'+CurenCD_1+ltrim(cast(t_EQ_DVOP.Grossdividend_1 as char(15)))
     WHEN Divtype_1 = 'C'
            AND t_EQ_DVOP.Grossdividend_1 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
           AND t_EQ_DVOP.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_1 as char(15))
     WHEN Divtype_1 = 'C'
           AND t_EQ_DVOP.Taxrate_1 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_1 */
/* STOCK OPTION START_1 */
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S' and t_EQ_DVOP.DefaultOpt_1='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_1 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S' AND resTFB1_1 <> '' 
     THEN ':35B:' + resTFB1_1
     WHEN Divtype_1 = 'S'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
            AND  t_EQ_DVOP.RatioNew_1 <> ''
            AND  t_EQ_DVOP.RatioOld_1 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_1,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_1,1,15)
     WHEN Divtype_1 = 'S'
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_1 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_1 */
/* BOTH OPTION START_1 */
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 <> 'B'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_1 <> '' AND t_EQ_DVOP.CurenCD_1 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_1
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B' and t_EQ_DVOP.DefaultOpt_1='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_1 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
           AND t_EQ_DVOP.Paydate is not null 
             AND t_EQ_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.Paydate,112)
     WHEN Divtype_1 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
            AND  t_EQ_DVOP.Netdividend_1 <> ''
     THEN ':92F::NETT//'+CurenCD_1+ltrim(cast(t_EQ_DVOP.Netdividend_1 as char(15)))
     WHEN Divtype_1 = 'B'
            AND  t_EQ_DVOP.Netdividend_1 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
            AND t_EQ_DVOP.Grossdividend_1 <> ''
     THEN ':92F::GRSS//'+CurenCD_1+ltrim(cast(t_EQ_DVOP.Grossdividend_1 as char(15)))
     WHEN Divtype_1 = 'B'
            AND t_EQ_DVOP.Grossdividend_1 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
           AND t_EQ_DVOP.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_1 as char(15))
     WHEN Divtype_1 = 'B'
           AND t_EQ_DVOP.Taxrate_1 is null
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B' AND resTFB1_1 <> '' 
     THEN ':35B:' + resTFB1_1
     WHEN Divtype_1 = 'B'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
            AND  t_EQ_DVOP.RatioNew_1 <> '' AND  t_EQ_DVOP.RatioOld_1 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_1,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_1,1,15)
     WHEN Divtype_1 = 'B'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_1 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR Divtype_1 = ''
     THEN ''
     WHEN Divtype_1 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_1 */
/* CASH OPTION START_2 */
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
     THEN ':13A::CAON//002'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_2 <> '' AND t_EQ_DVOP.CurenCD_2 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_2
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C' and t_EQ_DVOP.DefaultOpt_2='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_2 = 'C'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN Divtype_2 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
            AND  t_EQ_DVOP.Netdividend_2 <> ''
     THEN ':92F::NETT//'+CurenCD_2+ltrim(cast(t_EQ_DVOP.Netdividend_2 as char(15)))
     WHEN Divtype_2 = 'C'
            AND  t_EQ_DVOP.Netdividend_2 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
            AND t_EQ_DVOP.Grossdividend_2 <> ''
     THEN ':92F::GRSS//'+CurenCD_2+ltrim(cast(t_EQ_DVOP.Grossdividend_2 as char(15)))
     WHEN Divtype_2 = 'C'
            AND t_EQ_DVOP.Grossdividend_2 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
           AND t_EQ_DVOP.Taxrate_2 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_2 as char(15))
     WHEN Divtype_2 = 'C'
           AND t_EQ_DVOP.Taxrate_2 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_2 */
/* STOCK OPTION START_2 */
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
     THEN ':13A::CAON//002'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S' and t_EQ_DVOP.DefaultOpt_2='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_2 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S' AND resTFB1_2 <> '' 
     THEN ':35B:' + resTFB1_2
     WHEN Divtype_2 = 'S'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
            AND  t_EQ_DVOP.RatioNew_2 <> '' AND  t_EQ_DVOP.RatioOld_2 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_2,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_2,1,15)
     WHEN Divtype_2 = 'S'
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_2 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_2 */
/* BOTH OPTION START_2 */
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
     THEN ':13A::CAON//002'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 <> 'B'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_2 <> '' AND t_EQ_DVOP.CurenCD_2 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_2
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B' and t_EQ_DVOP.DefaultOpt_2='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_2 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
           AND t_EQ_DVOP.Paydate is not null 
             AND t_EQ_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.Paydate,112)
     WHEN Divtype_2 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
            AND  t_EQ_DVOP.Netdividend_2 <> ''
     THEN ':92F::NETT//'+CurenCD_2+ltrim(cast(t_EQ_DVOP.Netdividend_2 as char(15)))
     WHEN Divtype_2 = 'B'
            AND  t_EQ_DVOP.Netdividend_2 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
            AND t_EQ_DVOP.Grossdividend_2 <> ''
     THEN ':92F::GRSS//'+CurenCD_2+ltrim(cast(t_EQ_DVOP.Grossdividend_2 as char(15)))
     WHEN Divtype_2 = 'B'
            AND t_EQ_DVOP.Grossdividend_2 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
           AND t_EQ_DVOP.Taxrate_2 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_2 as char(15))
     WHEN Divtype_2 = 'B'
           AND t_EQ_DVOP.Taxrate_2 is null
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B' AND resTFB1_2 <> '' 
     THEN ':35B:' + resTFB1_2
     WHEN Divtype_2 = 'B'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
            AND  t_EQ_DVOP.RatioNew_2 <> ''
            AND  t_EQ_DVOP.RatioOld_2 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_2,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_2,1,15)
     WHEN Divtype_2 = 'B'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_2 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR Divtype_2 = ''
     THEN ''
     WHEN Divtype_2 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_2 */
/* CASH OPTION START_3 */
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
     THEN ':13A::CAON//003'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_3 <> '' AND t_EQ_DVOP.CurenCD_3 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_3
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C' and t_EQ_DVOP.DefaultOpt_3='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_3 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN Divtype_3 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
            AND  t_EQ_DVOP.Netdividend_3 <> ''
     THEN ':92F::NETT//'+CurenCD_3+ltrim(cast(t_EQ_DVOP.Netdividend_3 as char(15)))
     WHEN Divtype_3 = 'C'
            AND  t_EQ_DVOP.Netdividend_3 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
            AND t_EQ_DVOP.Grossdividend_3 <> ''
     THEN ':92F::GRSS//'+CurenCD_3+ltrim(cast(t_EQ_DVOP.Grossdividend_3 as char(15)))
     WHEN Divtype_3 = 'C'
            AND t_EQ_DVOP.Grossdividend_3 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
           AND t_EQ_DVOP.Taxrate_3 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_3 as char(15))
     WHEN Divtype_3 = 'C'
           AND t_EQ_DVOP.Taxrate_3 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_3 */
/* STOCK OPTION START_3 */
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
     THEN ':13A::CAON//003'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S' and t_EQ_DVOP.DefaultOpt_3='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_3 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S' AND resTFB1_3 <> '' 
     THEN ':35B:' + resTFB1_3
     WHEN Divtype_3 = 'S'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
            AND  t_EQ_DVOP.RatioNew_3 <> ''
            AND  t_EQ_DVOP.RatioOld_3 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_3,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_3,1,15)
     WHEN Divtype_3 = 'S'
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_3 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_3 */
/* BOTH OPTION START_3 */
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
     THEN ':13A::CAON//003'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 <> 'B'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_3 <> '' AND t_EQ_DVOP.CurenCD_3 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_3
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B' and t_EQ_DVOP.DefaultOpt_3='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_3 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
           AND t_EQ_DVOP.Paydate is not null 
             AND t_EQ_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.Paydate,112)
     WHEN Divtype_3 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
            AND  t_EQ_DVOP.Netdividend_3 <> ''
     THEN ':92F::NETT//'+CurenCD_3+ltrim(cast(t_EQ_DVOP.Netdividend_3 as char(15)))
     WHEN Divtype_3 = 'B'
            AND  t_EQ_DVOP.Netdividend_3 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
            AND t_EQ_DVOP.Grossdividend_3 <> ''
     THEN ':92F::GRSS//'+CurenCD_3+ltrim(cast(t_EQ_DVOP.Grossdividend_3 as char(15)))
     WHEN Divtype_3 = 'B'
            AND t_EQ_DVOP.Grossdividend_3 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
           AND t_EQ_DVOP.Taxrate_3 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_3 as char(15))
     WHEN Divtype_3 = 'B'
           AND t_EQ_DVOP.Taxrate_3 is null
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B' AND resTFB1_3 <> '' 
     THEN ':35B:' + resTFB1_3
     WHEN Divtype_3 = 'B'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
            AND  t_EQ_DVOP.RatioNew_3 <> ''
            AND  t_EQ_DVOP.RatioOld_3 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_3,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_3,1,15)
     WHEN Divtype_3 = 'B'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_3 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR Divtype_3 = ''
     THEN ''
     WHEN Divtype_3 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_3 */
/* CASH OPTION START_4 */
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
     THEN ':13A::CAON//004'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_4 <> '' AND t_EQ_DVOP.CurenCD_4 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_4
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C' and t_EQ_DVOP.DefaultOpt_4='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_4 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN Divtype_4 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
            AND  t_EQ_DVOP.Netdividend_4 <> ''
     THEN ':92F::NETT//'+CurenCD_4+ltrim(cast(t_EQ_DVOP.Netdividend_4 as char(15)))
     WHEN Divtype_4 = 'C'
            AND  t_EQ_DVOP.Netdividend_4 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
            AND t_EQ_DVOP.Grossdividend_4 <> ''
     THEN ':92F::GRSS//'+CurenCD_4+ltrim(cast(t_EQ_DVOP.Grossdividend_4 as char(15)))
     WHEN Divtype_4 = 'C'
            AND t_EQ_DVOP.Grossdividend_4 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
           AND t_EQ_DVOP.Taxrate_4 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_4 as char(15))
     WHEN Divtype_4 = 'C'
           AND t_EQ_DVOP.Taxrate_4 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_4 */
/* STOCK OPTION START_4 */
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
     THEN ':13A::CAON//004'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S' and t_EQ_DVOP.DefaultOpt_4='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_4 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S' AND resTFB1_4 <> '' 
     THEN ':35B:' + resTFB1_4
     WHEN Divtype_4 = 'S'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
            AND  t_EQ_DVOP.RatioNew_4 <> ''
            AND  t_EQ_DVOP.RatioOld_4 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_4,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_4,1,15)
     WHEN Divtype_4 = 'S'
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_4 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_4 */
/* BOTH OPTION START_4 */
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
     THEN ':13A::CAON//004'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 <> 'B'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_4 <> '' AND t_EQ_DVOP.CurenCD_4 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_4
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B' and t_EQ_DVOP.DefaultOpt_4='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_4 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
           AND t_EQ_DVOP.Paydate is not null 
             AND t_EQ_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.Paydate,112)
     WHEN Divtype_4 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
            AND  t_EQ_DVOP.Netdividend_4 <> ''
     THEN ':92F::NETT//'+CurenCD_4+ltrim(cast(t_EQ_DVOP.Netdividend_4 as char(15)))
     WHEN Divtype_4 = 'B'
            AND  t_EQ_DVOP.Netdividend_4 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
            AND t_EQ_DVOP.Grossdividend_4 <> ''
     THEN ':92F::GRSS//'+CurenCD_4+ltrim(cast(t_EQ_DVOP.Grossdividend_4 as char(15)))
     WHEN Divtype_4 = 'B'
            AND t_EQ_DVOP.Grossdividend_4 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
           AND t_EQ_DVOP.Taxrate_4 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_4 as char(15))
     WHEN Divtype_4 = 'B'
           AND t_EQ_DVOP.Taxrate_4 is null
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B' AND resTFB1_4 <> '' 
     THEN ':35B:' + resTFB1_4
     WHEN Divtype_4 = 'B'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
            AND  t_EQ_DVOP.RatioNew_4 <> ''
            AND  t_EQ_DVOP.RatioOld_4 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_4,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_4,1,15)
     WHEN Divtype_4 = 'B'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_4 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR Divtype_4 = ''
     THEN ''
     WHEN Divtype_4 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_4 */
/* CASH OPTION START_5 */
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
     THEN ':13A::CAON//005'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_5 <> '' AND t_EQ_DVOP.CurenCD_5 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_5
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C' and t_EQ_DVOP.DefaultOpt_5='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_5 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN Divtype_5 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
            AND  t_EQ_DVOP.Netdividend_5 <> ''
     THEN ':92F::NETT//'+CurenCD_5+ltrim(cast(t_EQ_DVOP.Netdividend_5 as char(15)))
     WHEN Divtype_5 = 'C'
            AND  t_EQ_DVOP.Netdividend_5 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
            AND t_EQ_DVOP.Grossdividend_5 <> ''
     THEN ':92F::GRSS//'+CurenCD_5+ltrim(cast(t_EQ_DVOP.Grossdividend_5 as char(15)))
     WHEN Divtype_5 = 'C'
            AND t_EQ_DVOP.Grossdividend_5 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
           AND t_EQ_DVOP.Taxrate_5 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_5 as char(15))
     WHEN Divtype_5 = 'C'
           AND t_EQ_DVOP.Taxrate_5 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_5 */
/* STOCK OPTION START_5 */
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
     THEN ':13A::CAON//005'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S' and t_EQ_DVOP.DefaultOpt_5='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_5 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S' AND resTFB1_5 <> '' 
     THEN ':35B:' + resTFB1_5
     WHEN Divtype_5 = 'S'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
            AND  t_EQ_DVOP.RatioNew_5 <> ''
            AND  t_EQ_DVOP.RatioOld_5 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_5,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_5,1,15)
     WHEN Divtype_5 = 'S'
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_5 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_5 */
/* BOTH OPTION START_5 */
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
     THEN ':13A::CAON//005'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 <> 'B'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_5 <> '' AND t_EQ_DVOP.CurenCD_5 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_5
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B' and t_EQ_DVOP.DefaultOpt_5='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_5 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
           AND t_EQ_DVOP.Paydate is not null 
             AND t_EQ_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.Paydate,112)
     WHEN Divtype_5 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
            AND  t_EQ_DVOP.Netdividend_5 <> ''
     THEN ':92F::NETT//'+CurenCD_5+ltrim(cast(t_EQ_DVOP.Netdividend_5 as char(15)))
     WHEN Divtype_5 = 'B'
            AND  t_EQ_DVOP.Netdividend_5 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
            AND t_EQ_DVOP.Grossdividend_5 <> ''
     THEN ':92F::GRSS//'+CurenCD_5+ltrim(cast(t_EQ_DVOP.Grossdividend_5 as char(15)))
     WHEN Divtype_5 = 'B'
            AND t_EQ_DVOP.Grossdividend_5 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
           AND t_EQ_DVOP.Taxrate_5 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_5 as char(15))
     WHEN Divtype_5 = 'B'
           AND t_EQ_DVOP.Taxrate_5 is null
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B' AND resTFB1_5 <> '' 
     THEN ':35B:' + resTFB1_5
     WHEN Divtype_5 = 'B'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
            AND  t_EQ_DVOP.RatioNew_5 <> ''
            AND  t_EQ_DVOP.RatioOld_5 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_5,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_5,1,15)
     WHEN Divtype_5 = 'B'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_5 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR Divtype_5 = ''
     THEN ''
     WHEN Divtype_5 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_5 */
/* CASH OPTION START_6 */
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
     THEN ':13A::CAON//006'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_6 <> '' AND t_EQ_DVOP.CurenCD_6 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_6
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C' and t_EQ_DVOP.DefaultOpt_6='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_6 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN Divtype_6 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
            AND  t_EQ_DVOP.Netdividend_6 <> ''
     THEN ':92F::NETT//'+CurenCD_6+ltrim(cast(t_EQ_DVOP.Netdividend_6 as char(15)))
     WHEN Divtype_6 = 'C'
            AND  t_EQ_DVOP.Netdividend_6 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
            AND t_EQ_DVOP.Grossdividend_6 <> ''
     THEN ':92F::GRSS//'+CurenCD_6+ltrim(cast(t_EQ_DVOP.Grossdividend_6 as char(15)))
     WHEN Divtype_6 = 'C'
            AND t_EQ_DVOP.Grossdividend_6 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
           AND t_EQ_DVOP.Taxrate_6 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_6 as char(15))
     WHEN Divtype_6 = 'C'
           AND t_EQ_DVOP.Taxrate_6 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_6 */
/* STOCK OPTION START_6 */
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
     THEN ':13A::CAON//006'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S' and t_EQ_DVOP.DefaultOpt_6='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_6 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S' AND resTFB1_6 <> '' 
     THEN ':35B:' + resTFB1_6
     WHEN Divtype_6 = 'S'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
            AND  t_EQ_DVOP.RatioNew_6 <> ''
            AND  t_EQ_DVOP.RatioOld_6 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_6,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_6,1,15)
     WHEN Divtype_6 = 'S'
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_6 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_6 */
/* BOTH OPTION START_6 */
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
     THEN ':13A::CAON//006'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 <> 'B'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_6 <> '' AND t_EQ_DVOP.CurenCD_6 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_6
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B' and t_EQ_DVOP.DefaultOpt_6='T' 
     THEN ':17B::DFLT//Y'
     WHEN Divtype_6 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
           AND t_EQ_DVOP.Paydate is not null 
             AND t_EQ_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.Paydate,112)
     WHEN Divtype_6 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
            AND  t_EQ_DVOP.Netdividend_6 <> ''
     THEN ':92F::NETT//'+CurenCD_6+ltrim(cast(t_EQ_DVOP.Netdividend_6 as char(15)))
     WHEN Divtype_6 = 'B'
            AND  t_EQ_DVOP.Netdividend_6 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
            AND t_EQ_DVOP.Grossdividend_6 <> ''
     THEN ':92F::GRSS//'+CurenCD_6+ltrim(cast(t_EQ_DVOP.Grossdividend_6 as char(15)))
     WHEN Divtype_6 = 'B'
            AND t_EQ_DVOP.Grossdividend_6 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
           AND t_EQ_DVOP.Taxrate_6 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_6 as char(15))
     WHEN Divtype_6 = 'B'
           AND t_EQ_DVOP.Taxrate_6 is null
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B' AND resTFB1_6 <> '' 
     THEN ':35B:' + resTFB1_6
     WHEN Divtype_6 = 'B'
     THEN ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
            AND  t_EQ_DVOP.RatioNew_6 <> ''
            AND  t_EQ_DVOP.RatioOld_6 <> ''
     THEN ':92D::ADEX//'+substring(t_EQ_DVOP.RatioNew_6,1,15)
                        +'/'+substring(t_EQ_DVOP.RatioOld_6,1,15)
     WHEN Divtype_6 = 'B'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
             AND t_EQ_DVOP.Paydate2 is not null 
                AND t_EQ_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate2,112)
     WHEN Divtype_6 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR Divtype_6 = ''
     THEN ''
     WHEN Divtype_6 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_6 */
'' as Notes,
'-}$'
From t_EQ_DVOP
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and Actflag <> ''
and Divtype_1 <> ''
/* START eliminate Cash only Stock only AND Both only */
AND (not (DIVTYPE_2 = ''
AND DIVTYPE_3 = ''
AND DIVTYPE_4 = ''
AND DIVTYPE_5 = ''
AND DIVTYPE_6 = ''))
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3
