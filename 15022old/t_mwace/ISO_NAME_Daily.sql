--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_NAME
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_CHAN_NAME.Changed,
'select  IschgNotes As Notes FROM ISCHG WHERE ISCHGID = '+ cast(t_EQ_CHAN_NAME.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'153'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_CHAN_NAME.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_CHAN_NAME.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_CHAN_NAME.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_CHAN_NAME.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//NAME',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_CHAN_NAME.NameChangeDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_CHAN_NAME.NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':70E::NAME//' + substring(t_EQ_CHAN_NAME.IssNewname,1,35) as TIDYTEXT,
':16S:CADETL',
'' as Notes,
'-}$'
From t_EQ_CHAN_NAME
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and upper(eventtype)<>'CLEAN'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

