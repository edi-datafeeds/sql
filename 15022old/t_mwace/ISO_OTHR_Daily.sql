--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_OTHR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select   
t_EQ_OTHR_ARR.Changed,
'select  ArrNotes As Notes FROM Arr WHERE RDID = '+ cast(t_EQ_OTHR_ARR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'112'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_OTHR_ARR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_OTHR_ARR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_OTHR_ARR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_OTHR_ARR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_OTHR_ARR.ExDate <>'' and t_EQ_OTHR_ARR.ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_OTHR_ARR.ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN t_EQ_OTHR_ARR.PayDate <>'' and t_EQ_OTHR_ARR.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_OTHR_ARR.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN t_EQ_OTHR_ARR.RecDate <>'' and t_EQ_OTHR_ARR.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_OTHR_ARR.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From t_EQ_OTHR_ARR
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

