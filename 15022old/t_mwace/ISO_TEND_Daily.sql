--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_TEND
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=Y
--sEvent=TKOVR

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' 
+ rtrim(cast(t_EQ_TEND_TKOVR.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) 
+ ' Order By OptionID, SerialID' as MPAYlink,
t_EQ_TEND_TKOVR.Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(t_EQ_TEND_TKOVR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'131'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_TEND_TKOVR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_TEND_TKOVR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_TEND_TKOVR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_TEND_TKOVR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//VOLU', 
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_TEND_TKOVR.Opendate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_TEND_TKOVR.opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.CmAcqdate <> ''
     THEN ':98A::WUCO//'+CONVERT ( varchar , t_EQ_TEND_TKOVR.CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.CloseDate <> ''
       AND t_EQ_TEND_TKOVR.OpenDate <> ''
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.OpenDate,112)+'/'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.CloseDate,112)
     WHEN t_EQ_TEND_TKOVR.OpenDate <> ''
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.OpenDate,112)+'/UKWN'
     WHEN t_EQ_TEND_TKOVR.CloseDate <> ''
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.CloseDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.CloseDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_TEND_TKOVR.CloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.UnconditionalDate <>'' and t_EQ_TEND_TKOVR.UnconditionalDate > getdate()
     THEN ':22F::TAKO//COND'
     ELSE ':22F::TAKO//UNCO'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From t_EQ_TEND_TKOVR
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

