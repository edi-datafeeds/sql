--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXRI
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca2
select    
t_EQ_EXRI_RTS.Changed,
'select  RTSNotes As Notes FROM RTS WHERE RDID = '+ cast(t_EQ_EXRI_RTS.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'118'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_EXRI_RTS.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_EXRI_RTS.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_EXRI_RTS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_EXRI_RTS.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXRI',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
/*':20C::RELA//117'+t_EQ_EXRI_RTS.EXCHGID+cast(t_EQ_EXRI_RTS.Seqnum as char(1))+ cast(t_EQ_EXRI_RTS.EventID as char(16)) as CAref,*/
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_EXRI_RTS.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_EXRI_RTS.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_EXRI_RTS.EndTrade <> '' 
       AND t_EQ_EXRI_RTS.StartTrade <> '' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartTrade,112) + '/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndTrade,112)
     WHEN t_EQ_EXRI_RTS.StartTrade <> '' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartTrade,112) + '//UKWN'
     WHEN t_EQ_EXRI_RTS.EndTrade <> '' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_EXRI_RTS.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN t_EQ_EXRI_RTS.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_EXRI_RTS.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_EXRI_RTS.EndSubscription <>'' AND t_EQ_EXRI_RTS.EndSubscription IS NOT NULL
       AND t_EQ_EXRI_RTS.StartSubscription <>'' AND t_EQ_EXRI_RTS.StartSubscription IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartSubscription,112)+'/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndSubscription,112)
     WHEN t_EQ_EXRI_RTS.StartSubscription <>'' AND t_EQ_EXRI_RTS.StartSubscription IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartSubscription,112)+'/UKWN'
     WHEN t_EQ_EXRI_RTS.EndSubscription <>'' AND t_EQ_EXRI_RTS.EndSubscription IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndSubscription,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN t_EQ_EXRI_RTS.IssuePrice <> ''
     THEN ':90B::SUPR//ACTU/'+CurenCD+ltrim(cast(t_EQ_EXRI_RTS.IssuePrice as char(15)))
     ELSE ':90E::SUPR//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_EXRI_RTS.RatioNew <>''
               AND t_EQ_EXRI_RTS.RatioOld <>''
     AND t_EQ_EXRI_RTS.RatioNew is not null
               AND t_EQ_EXRI_RTS.RatioOld is not null
     THEN ':92D::NWRT//'+ltrim(cast(t_EQ_EXRI_RTS.RatioNew as char (15)))+
                    '/'+ltrim(cast(t_EQ_EXRI_RTS.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* EXERCISE OPTION END */
/* SELL RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SLLE',
':17B::DFLT//N',
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* SELL RIGHTS OPTION END */
/* LAPSE RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//003',
':22F::CAOP//LAPS',
':17B::DFLT//N',
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* LAPSE RIGHTS OPTION END */
'' as Notes,
'-}$'
From t_EQ_EXRI_RTS
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3

