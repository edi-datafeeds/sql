--FilePath=O:\Datafeed\15022\mwace\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_TEND
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(changed) as maxdate from wca2.dbo.t_eq_dvca_dvse
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\mwace\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

t_EQ_ACTV
--# 1
use wca2
select    
t_EQ_TEND_PO.Changed,
'select  PONotes As Notes FROM PO WHERE RDID = '+ cast(t_EQ_TEND_PO.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'132'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_TEND_PO.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_TEND_PO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_TEND_PO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_TEND_PO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN'
     THEN '/GB/' + Sedol
     ELSE ''
     END,
CASE WHEN Localcode <> ''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_TEND_PO.OfferOpens <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_TEND_PO.OfferOpens,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN t_EQ_TEND_PO.OfferCloses <> ''
       AND t_EQ_TEND_PO.OfferOpens <> ''
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferOpens,112)+'/'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferCloses,112)
     WHEN t_EQ_TEND_PO.OfferOpens <> ''
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferOpens,112)+'/UKWN'
     WHEN t_EQ_TEND_PO.OfferCloses <> ''
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferCloses,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN t_EQ_TEND_PO.OfferCloses <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_TEND_PO.OfferCloses,112)
     ELSE ':98B::MKDT//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
/*/*'select  RDNotes As Notes FROM rd WHERE RDID = '+ cast(t_EQ_TEND_PO.RDID as char(16)) as Notes, */ */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN t_EQ_TEND_PO.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_TEND_PO.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_TEND_PO.Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_TEND_PO.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN t_EQ_TEND_PO.TndrStrkPrice <> '' 
     THEN ':90B::OFFR//ACTU/'+t_EQ_TEND_PO.CurenCD+
          +substring(t_EQ_TEND_PO.TndrStrkPrice,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
CASE WHEN t_EQ_TEND_PO.MaxPrice <>'' AND t_EQ_TEND_PO.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+t_EQ_TEND_PO.CurenCD+
          +substring(t_EQ_TEND_PO.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_TEND_PO
WHERE 
mainTFB1 <> ''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref1, caref2 desc, caref3
