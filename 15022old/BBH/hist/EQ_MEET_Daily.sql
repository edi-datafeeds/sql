--SEMETABLE=client.dbo.seme_BBH
--FileName=edi_YYYYMMDD
--MEET_AGM_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'' as ISOHDR,
':16R:GENL',
'161'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':94E::MEET//'+substring(rtrim(Country),1,35) as TIDYTEXT,
substring(rtrim(Add1),1,35) as TIDYTEXT,
substring(rtrim(Add1),36,35) as TIDYTEXT,
substring(rtrim(Add2),1,35) as TIDYTEXT,
substring(rtrim(Add2),36,35) as TIDYTEXT,
substring(rtrim(Add3),1,35) as TIDYTEXT,
substring(rtrim(Add4),1,35) as TIDYTEXT,
substring(rtrim(Add5),1,35) as TIDYTEXT,
substring(rtrim(Add6),1,35) as TIDYTEXT,
substring(rtrim(City),1,35) as TIDYTEXT,
':16S:CADETL',
'-}$'
From v_EQ_MEET
WHERE 
sedol<>''
and mcd<>'' and mcd is not null
and isin in (select code from client.dbo.pfisin where accid = 995 and actflag<>'D')
and (AGMEGM = 'CG' or AGMEGM = 'GM' or AGMEGM = 'EG' or AGMEGM = 'SG' or AGMEGM = 'AG')
order by caref2 desc, caref1, caref3
