--SEMETABLE=client.dbo.seme_BBH
--FileName=edi_YYYYMMDD
--DRIP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'101'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE 
     WHEN  Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  Actflag = 'C'
     THEN ':23G:CANC'
     WHEN  DripActflag = 'D'
     THEN ':23G:CANC'
     WHEN  DripActflag = 'C'
     THEN ':23G:CANC'
     WHEN  divpyActflag = 'D'
     THEN ':23G:CANC'
     WHEN  divpyActflag = 'C'
     THEN ':23G:CANC'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DRIP',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN  MCD <>''
     THEN ':94B::PLIS//EXCH/' +  MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN  Exdate <>'' AND  Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar ,  Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN  Recdate <>'' AND  Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar ,  recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN  DripLastDate <>'' AND  DripLastDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar ,  DripLastDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  Paydate <>'' AND  Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN structcd='REIT' and Netdividend <> '' AND Netdividend IS NOT NULL
     THEN ':92J::NETT//REES/'+ CurenCD+ltrim(cast(Netdividend as char(15)))
     WHEN Netdividend <> '' AND Netdividend IS NOT NULL
     THEN ':92F::NETT//'+ CurenCD+ltrim(cast(Netdividend as char(15)))
     WHEN Grossdividend <> '' AND  Grossdividend IS NOT NULL
     THEN ''
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN structcd='REIT' and Grossdividend <> '' AND  Grossdividend IS NOT NULL
     THEN ':92J::GRSS//REES/'+ CurenCD+ltrim(cast(Grossdividend as char(15)))
     WHEN Grossdividend <> '' AND  Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ CurenCD+ltrim(cast(Grossdividend as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB2,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN  DripReinvPrice <>'' AND  DripReinvPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+ CurenCD+ltrim(cast( DripReinvPrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
CASE WHEN  DripPaydate <>'' AND  DripPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  DripPaydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From v_EQ_DRIP_2011
WHERE 
mcd<>'' and mcd is not null
and isin in (select code from client.dbo.pfisin where accid = 995 and actflag<>'D')
and (Exdate>getdate()-183 or exdate is null or drippaydate>getdate()-183 or drippaydate is null)
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=AnnounceDate))
order by caref2 desc, caref1, caref3
