--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_XMET
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct 
v_FI_XMET.Changed,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'361' as Caref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN v_FI_XMET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_XMET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_XMET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_XMET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN v_FI_XMET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN v_FI_XMET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN v_FI_XMET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_XMET.AGMEGM = 'BH'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_XMET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_XMET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_XMET.AGMdate <>''
     THEN ':98A::MEET//'+CONVERT ( varchar , v_FI_XMET.AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
'-}$'
From v_FI_XMET
WHERE 
mainTFB2<>''
     and (changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
          and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag<>'D')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag<>'D')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag<>'D')))
order by caref2 desc, caref1, caref3
