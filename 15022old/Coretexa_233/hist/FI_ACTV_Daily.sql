--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD_235959
-- YYYYMMDD_FI564_ACTV
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2


--# 1
use wca
select distinct 
v_FI_ACTV.Changed,
'select case when len(cast(Reason as varchar(24)))=22 then null else Reason end As Notes FROM LSTAT WHERE LSTATID = '+ cast(v_FI_ACTV.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'350' as Caref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN v_FI_ACTV.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_ACTV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_ACTV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_ACTV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ACTV',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_ACTV.NotificationDate is not null and v_FI_ACTV.NotificationDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , v_FI_ACTV.NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN v_FI_ACTV.EffectiveDate is not null and v_FI_ACTV.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_ACTV.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From v_FI_ACTV
WHERE
1=2
/* mainTFB2<>''
and (Effectivedate>getdate()-8 or Effectivedate is null and Announcedate>getdate()-30)
     and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='I'))
and LStatStatus = 'R'
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3 */
