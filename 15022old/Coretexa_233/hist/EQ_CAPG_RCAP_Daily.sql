--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD_235959
--CAPG_RCAP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\Coretexa\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  RcapNotes As Notes FROM RCAP WHERE RCAPID = '+ cast(EventID as char(16)) as ChkNotes, 
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'165' as CAref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CAPG',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN RecDate <>'' and RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN EffectiveDate <>'' and effectivedate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN CSPYDate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN CashBak <>'' AND CashBak IS NOT NULL
     THEN ':92F::GRSS//'+CurenCD+
          +substring(CashBak,1,15)
     ELSE ':92K::GRSS//UKWN'
     END as Commasub,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From wca.dbo.v_EQ_CAPG_RCAP
WHERE 
mainTFB2<>''
and exchgid<>'' and exchgid is not null

and (Effectivedate>getdate()-8 or Effectivedate is null and Announcedate>getdate()-30)
     and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='I'))

order by caref2 desc, caref1, caref3

