--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD_235959
-- YYYYMMDD_FI564_MCAL
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct 
v_FI_REDM.Changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then null else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(v_FI_REDM.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'410' as Caref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN v_FI_REDM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_REDM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_REDM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_REDM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MCAL',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_REDM.RedemDate is not null and v_FI_REDM.RedemDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_REDM.RedemDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_FI_REDM.CurenCD  <>''
     THEN ':11A::OPTN//' + v_FI_REDM.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN RedemPercent <>''
     THEN ':90A::OFFR//PRCT/' + RedemPercent
     WHEN nominalvalue <>'' and RedemPrice<>''
          and nominalvalue <>'0' and RedemPrice<>'0'
     THEN ':90A::OFFR//PRCT/' + cast(cast(cast(RedemPrice as decimal(14,5))*100
                                 /cast(nominalvalue as decimal(14,5)) as money) as varchar (30))
     WHEN RedemPrice <> ''
     THEN ':90B::OFFR//ACTU/' + CurenCD + RedemPrice
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_REDM
WHERE
mainTFB2<>''
and (RedemDate>getdate()-8 or RedemDate is null and Announcedate>getdate()-30)
     and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='I'))
and (indefpay='' or indefpay = 'P')
and redemtype <> 'PUT'
and mandoptflag = 'M'
and partfinal = 'F'
and not (redemtype='MAT' or maturitydate=redemdate)
and redemdate <>''
and redemdate <> '1800/01/01'
order by caref2 desc, caref1, caref3
