--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD_235959
-- YYYYMMDD_FI564_MEET
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct 
v_FI_MEET.Changed,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'361' as Caref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN v_FI_MEET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_MEET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_MEET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_MEET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN v_FI_MEET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN v_FI_MEET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN v_FI_MEET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_MEET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_MEET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_MEET.AGMdate <>''
     THEN ':98A::MEET//'+CONVERT ( varchar , v_FI_MEET.AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//Meeting Address', 
substring(rtrim(v_FI_MEET.Add1),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add1),36,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add2),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add3),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add4),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add5),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add6),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.City),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Country),1,35) as TIDYTEXT,
':16S:ADDINFO',
'-}$'
From v_FI_MEET
WHERE 
mainTFB2<>''
and (AgmDate>getdate()-8 or AgmDate is null and Announcedate>getdate()-30)
     and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='I'))
and (AGMEGM = 'CG' or AGMEGM = 'GM' or AGMEGM = 'EG' or AGMEGM = 'SG' or AGMEGM = 'AG')
order by caref2 desc, caref1, caref3
