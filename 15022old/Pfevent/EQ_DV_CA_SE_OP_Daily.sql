--SEMETABLE=client.dbo.seme_pfevent
--FileName=edi_YYYYMMDD
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\sungard\
--FileTidy=N
--sEvent=DIV
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( maintab.eventid as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
cntrycd as MPAYLINK3,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'100'+EXCHGID as CAref1,
maintab.eventid as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN  maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN  maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DV' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , maintab.changed,112) + replace(convert ( varchar, maintab.changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM/' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     ELSE ''
     END,
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CG'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='INS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='MEM'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='SUP'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='TEI'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='ARR'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ONE'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='UN'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='VAR'
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
'' as StartseqE,
'' as Notes, 
'-}$'
From v_EQ_DV_CA_SE_OP as maintab
inner join client.dbo.pfevent as ev on 'DIV' = ev.eventcd and maintab.eventid=ev.eventid
left outer join wca.dbo.divpy as divopt1 on maintab.eventid = divopt1.divid and 1=divopt1.optionid and 'D'<>divopt1.actflag
left outer join wca.dbo.divpy as divopt2 on maintab.eventid = divopt2.divid and 2=divopt2.optionid and 'D'<>divopt2.actflag
left outer join wca.dbo.divpy as divopt3 on maintab.eventid = divopt3.divid and 3=divopt3.optionid and 'D'<>divopt3.actflag
WHERE 
mainTFB1<>''
and (cntrycd='CA' or cntrycd='US')
and mcd<>''
and not (maintab.divperiodcd='ISC' and divopt2.divid is null and divopt3.divid is null)
and (paydate>getdate()-183 or paydate is null)
order by caref2 desc, caref1, caref3
