--SEMETABLE=client.dbo.seme_baillie
--FileName=edi_YYYYMMDD
--MRGR_TKOVR_PREC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=10000
--MESSTERMINATOR=
--TEXTCLEAN=2

--
--# 1
use wca
select distinct
Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01BAILLIEXXXXO0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.CmAcqdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.CmAcqdate is not null
              and (SR.CAMV<>'MAND' or SR.TFB<>':35B:'+mainTFB1)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '130' END as CAref1, */
'130' as CAref1,
maintab.EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MRGR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(Localcode,'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.announcedate,112) ELSE '' END,
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+replace(substring(maintab.Offerorname,1,23),'�','e')
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:/US/999999999',
'NOT AVAILABLE AT PRESENT',
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':92K::ADEX//UKWN',
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_TEND_TKOVR as maintab
left outer join wca.dbo.MPAY on maintab.eventid = wca.dbo.MPAY.eventid and 'TKOVR' = wca.dbo.MPAY.sevent
                                         and 'D'<>wca.dbo.MPAY.actflag
/* left outer join client.dbo.seme_baillie as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'CANC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'MRGR'=SR.CAEVMAIN */
WHERE 
mainTFB1<>''
and ((maintab.opendate is null or maintab.cmAcqdate is not null)
  and (maintab.closedate is not null or maintab.cmAcqdate is not null)
  and maintab.minitkovr<>'T'
  or (maintab.Actflag='D'))
and wca.dbo.MPAY.eventid is null
and (((maintab.isin in (select code from client.dbo.pfisin where accid=127 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=127 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=127 and actflag='I'))
and (maintab.closedate>getdate() or maintab.cmAcqdate > getdate()))
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=127 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=127 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=127 and actflag='U'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)))
and wca.dbo.MPAY.eventid is null
order by caref2 desc, caref1, caref3
