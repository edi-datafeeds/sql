--SEMETABLE=client.dbo.seme_baillie
--FileName=edi_YYYYMMDD
--CHAN_SECRC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=10000
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select SecrcNotes As Notes FROM SECRC WHERE SecRcID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01BAILLIEXXXXO0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Effectivedate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Effectivedate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '191' END as CAref1, */
'191' as CAref1,
EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(Localcode,'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, announcedate,112) ELSE '' END,
CASE WHEN Effectivedate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Effectivedate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//TERM',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN isnull(NewISIN,'')<>'' THEN ':35B:ISIN ' + NewISIN
     WHEN isnull(resisin,'')<>'' THEN ':35B:ISIN ' + resisin
     else '' end,
case when isnull(NewISIN,'')='' and isnull(resisin,'')='' then ':35B:/US/999999999' else '' end,
case when isnull(NewISIN,'')='' and isnull(resisin,'')='' then 'NOT AVAILABLE AT PRESENT' else '' end,
':16R:FIA',
':94B::PLIS//EXCH/SECM',
':16S:FIA',
':92D::NEWO//1,/1,',
CASE WHEN Effectivedate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , Effectivedate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_CHAN_SECRC as maintab
/* left outer join client.dbo.seme_baillie as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CHAN'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=127 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=127 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=127 and actflag='I'))
and Effectivedate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=127 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=127 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=127 and actflag='U'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)))
and maintab.secid<>maintab.ressecid
order by caref2 desc, caref1, caref3
