--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--PRIO_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select PrfNotes As Notes FROM PRF WHERE PrfID = '+ cast(maintab.PrfID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01NEWGGB20XXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Exdate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '110' END as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN maintab.primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN maintab.primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(maintab.cfi)=6 THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASE',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD
     ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN maintab.EndSubscription<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN maintab.EndSubscription<>''
       AND maintab.StartSubscription<>''
      THEN ':69A::PWAL//'
          +CONVERT ( varchar , maintab.StartSubscription,112)+'/'
          +CONVERT ( varchar , maintab.EndSubscription,112)
     WHEN maintab.StartSubscription<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , maintab.StartSubscription,112)+'/UKWN'
     WHEN maintab.EndSubscription<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , maintab.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN maintab.RatioNew<>'' AND maintab.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(maintab.RatioNew as char (15)))+
                    '/'+ltrim(cast(maintab.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN paydate2<>'' AND paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate2,112)
     WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
case when prf2.prfid is not null then ':16R:SECMOVE' ELSE '' end,
case when prf2.prfid is not null then ':22H::CRDB//CRED' ELSE '' end,
case WHEN prf2.prfid is not null and prf2scmst.isin is not null and prf2scmst.isin<>''
     THEN ':35B:ISIN ' + prf2scmst.isin
     WHEN prf2.prfid is not null
     THEN ':35B:UKWN'
     ELSE ''
     END,
case when prf2.prfid is not null then ':16R:FIA' ELSE '' end,
CASE 
     WHEN prf2.prfid is not null
     THEN ':94B::PLIS//SECM'
     ELSE ''
     END,
case when prf2.prfid is not null then ':16S:FIA' ELSE '' end,
CASE WHEN prf2.prfid is not null and prf2.RatioNew<>'' AND prf2.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(prf2.RatioNew as char (15)))+
                    '/'+ltrim(cast(prf2.RatioOld as char (15))) 
     WHEN prf2.prfid is not null
     THEN ':92K::ADEX//UKWN' 
     ELSE ''
     END as COMMASUB,
CASE WHEN prf2.prfid is not null and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     WHEN prf2.prfid is not null
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
case when prf2.prfid is not null then ':16S:SECMOVE' ELSE '' end,
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN maintab.MaxPrice<>''
     THEN ':90B::PRPP//ACTU/'+maintab.CurenCD+
          +substring(maintab.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
case when prf2.prfid is not null then ':16R:CASHMOVE' ELSE '' end,
case when prf2.prfid is not null then ':22H::CRDB//DEBT' ELSE '' end,
CASE WHEN prf2.prfid is not null and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     WHEN prf2.prfid is not null
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN prf2.prfid is not null and prf2.MaxPrice<>''
     THEN ':90B::PRPP//ACTU/'+prf2.CurenCD+
          +substring(prf2.MaxPrice,1,15)
     WHEN prf2.prfid is not null
     THEN ':90E::PRPP//UKWN'
     ELSE ''
     END as COMMASUB,
case when prf2.prfid is not null then ':16S:CASHMOVE' ELSE '' end,
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_EQ_PRIO_PRF as maintab
left outer join serialseq on 'PRF'=serialseq.eventcd and maintab.eventid = serialseq.rdid and 2=serialseq.seqnum
left outer join prf as prf2 on serialseq.uniqueid = prf2.prfid
left outer join scmst as prf2scmst on prf2.ressecid = prf2scmst.secid
left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'PRIO'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
     or maintab.uscode in (select code from client.dbo.pfuscode where accid=996 and actflag='I'))
and exdate ='2015/06/16')
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='U')
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
order by caref2 desc, caref1, caref3
