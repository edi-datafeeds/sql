--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_DLST
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
v_FI_ACTV.Changed,
'select case when len(cast(Reason as varchar(24)))=22 then rtrim(char(32)) else Reason end As Notes FROM LSTAT WHERE LSTATID = '+ cast(v_FI_ACTV.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '351'+EXCHGID+cast(Seqnum as char(1))
     ELSE '351'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_ACTV.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_ACTV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_ACTV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_ACTV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DLST',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>''
     then ':11A::DENO//'+debtcurrency
     else ''
     end,
case when MaturityDate is not null
     then ':98A::MATU//'++CONVERT(varchar, MaturityDate,112)
     else ''
     end,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_ACTV.NotificationDate is not null and v_FI_ACTV.NotificationDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , v_FI_ACTV.NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN v_FI_ACTV.EffectiveDate is not null and v_FI_ACTV.EffectiveDate <>''
     THEN ':98A::TSDT//'+CONVERT ( varchar , v_FI_ACTV.EffectiveDate,112)
     ELSE ':98B::TSDT//UKWN'
     END,
':16S:CADETL',
'' as Notes, 
'-}$'
From v_FI_ACTV
WHERE
mainTFB1<>''
and (((isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
     or uscode in (select code from client.dbo.pfuscode where accid=996 and actflag='I'))
and effectivedate ='2015/06/16')
OR
(isin in (select code from client.dbo.pfisin where accid=996 and actflag='U')
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and LStatStatus = 'D'
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
