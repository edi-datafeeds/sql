--FilePath=o:\Datafeed\15022\2011_Danske\
--FileName=YYYYMMDD_EQ15022
--RHDI_RTS_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\2011_Danske\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--TFBNUMBER=1

--# 1
use wca
select
Changed,
'' as ISOHDR,
':16R:GENL',
'160'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHDI',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN Recdate <>'' AND Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':22F::SELL//RENO',
':22F::RHDI//EXRI',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
':17B::DFLT//Y',
CASE WHEN EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN EndTrade <> '' 
       AND StartTrade <> '' 
     THEN ':69A::REVO//'
          +CONVERT ( varchar , StartTrade,112) + '/'
          +CONVERT ( varchar , EndTrade,112)
     WHEN StartTrade <> '' 
     THEN ':69C::REVO//'
          +CONVERT ( varchar , StartTrade,112) + '/UKWN'
     WHEN EndTrade <> '' 
     THEN ':69E::REVO//UKWN/'
          +CONVERT ( varchar , EndTrade,112)
     ELSE ':69J::REVO//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN traTFB1<>'' and traTFB1<>'UKWN'
     THEN ':35B:' + traTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':92D::ADEX//1,/1,',
CASE WHEN StartTrade <>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_RHDI_RTS
WHERE 
mainTFB1<>''
and (primaryexchgcd=exchgcd or primaryexchgcd='')
and (mcd<>'' and mcd is not null)
and Exdate>getdate()-31 and Exdate is not null
and CalcListdate<=Announcedate
and CalcDelistdate>=Announcedate
AND changed>=(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
order by caref2 desc, caref1, caref3