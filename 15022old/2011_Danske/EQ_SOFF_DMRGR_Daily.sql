--FilePath=o:\Datafeed\15022\2011_Danske\
--FileName=YYYYMMDD_EQ15022
--SOFF_DMRGR_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\2011_Danske\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DMRGR
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--TFBNUMBER=1

--# 1
use wca
select
'select distinct * from V10s_MPAY'
+ ' where (v10s_MPAY.actflag='+char(39)+'I'+char(39)+ ' or v10S_MPAY.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'DMRGR'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
Changed,
paydate as PAYLINK,
'' as ISOHDR,
':16R:GENL',
'138'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SOFF',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <> '' and  ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN Recdate <> '' and Recdate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , Recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'-}$'
From v_EQ_SOFF_DMRGR
WHERE 
mainTFB1<>''
and (primaryexchgcd=exchgcd or primaryexchgcd='')
and (mcd<>'' and mcd is not null)
and Exdate>getdate()-31 and Exdate is not null
and CalcListdate<=Announcedate
and CalcDelistdate>=Announcedate
AND (changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
      or (select max(acttime) from mpay where mpay.sevent='DMRGR' and mpay.eventid=v_EQ_SOFF_DMRGR.eventid)>=
      (select max(feeddate) from wca.dbo.tbl_opslog where seq=3))
order by caref2 desc, caref1, caref3