--SEMETABLE=client.dbo.seme_danske
--FileName=YYYYMMDD_EQ15022
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\2011_Danske\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--TFBNUMBER=1


--# 1
use wca
select distinct top 5
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
cntrycd as MPAYLINK3,
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
':23G:NEWM' AS MessageStatus,
':22F::CAEV//DVCA',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN maintab.cfi <> '' THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CG'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='INS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='MEM'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='SUP'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='TEI'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='ARR'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ONE'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='UN'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='VAR'
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN mainoptn.CurenCD <> '' AND mainoptn.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + mainoptn.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  Paydate <>'' AND  Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN  mainoptn.Netdividend <> '' AND mainoptn.Netdividend IS NOT NULL
     THEN ':92F::NETT/REES/'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Netdividend as char(15)))
     WHEN  mainoptn.Grossdividend <> '' AND  mainoptn.Grossdividend IS NOT NULL
     THEN ''
     ELSE ':92K::NETT/REES/UKWN'
     END as COMMASUB,
CASE WHEN  mainoptn.Grossdividend <> ''
             AND  mainoptn.Grossdividend IS NOT NULL
     THEN ':92F::GRSS/REES/'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))
     ELSE ':92K::GRSS/REES/UKWN' 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
from v_EQ_DV_CA_SE_OP as maintab
inner join scmst on maintab.secid=scmst.secid and 'REIT'=scmst.structcd
left outer join divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid
left outer join divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
WHERE 
mainTFB1<>''
and (maintab.primaryexchgcd=exchgcd or maintab.primaryexchgcd='')
and (mcd<>'' and mcd is not null)
and maintab.dripcntrycd=''
and not maintab.sectycd='DR'
and not divperiodcd='ISC'
and (exdate>getdate()-1 or (exdate is null and maintab.announcedate>getdate()-31))
and mainoptn.divid is not null 
and (mainoptn.divtype='C' or mainoptn.divtype='B')
and divopt2.divid is null 
and divopt3.divid is null
and (paydate>getdate()-183 or paydate is null)
order by caref2 desc, caref1, caref3
