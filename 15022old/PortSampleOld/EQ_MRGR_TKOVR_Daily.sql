--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--TEND_TKOVR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=MRGR
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=7900
--TEXTCLEAN=2

--
--# 1
use wca
select distinct
'select distinct * from V10s_MPY2'
+ ' where (Actflag='+char(39)+'I'+char(39) +' or Actflag='+char(39)+'U'+char(39)
+') and eventid=' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent='+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype<>'+ char(39) + 'D'+ char(39)
+ ' and (defopt='+ char(39) + 'T'+ char(39)
+ ' or '+ char(39) + 'T'+ char(39) +' not in '
+ ' (select defaultopt from mpay where eventid=' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and Actflag<>'+ char(39) + 'D'+ char(39) +'))'
+ ' Order By OptionID,SerialID' as MPAYlink,
Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
null as PAYLINK,
Opendate as OPENLINK,
maintab.Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
case when 1=1 then (select '{1:F01CLIENTBICXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.CmAcqdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.CmAcqdate is not null
              and (SR.CAMV<>'MAND' or SR.TFB<>':35B:'+mainTFB1)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '130' END as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MRGR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.mpay.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+upper(replace(substring(maintab.Offerorname,1,23),'�','e'))
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes, 
'-}$'
From v_EQ_TEND_TKOVR as maintab
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'TKOVR' = wca.dbo.mpay.sevent
                                       and 'D'<>wca.dbo.mpay.actflag
left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'TEND'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=996 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=996 and actflag='I'))
and CmAcqdate > getdate()-183)
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=996 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=996 and actflag='U'))
and changed>(select max(feeddate)-366 as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and CmAcqdate is not null
order by caref2 desc, caref1, caref3

