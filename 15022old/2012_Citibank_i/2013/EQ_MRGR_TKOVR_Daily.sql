--SEMETABLE=client.dbo.seme_citibank_i
--FileName=edi_YYYYMMDD
--TEND_TKOVR_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=MRGR
--TFBNUMBER=2
--INCREMENTAL=ON


--# 1
use wca
select distinct
'select distinct * from V10s_MPY2'
+ ' where (Actflag='+char(39)+'I'+char(39) +' or Actflag='+char(39)+'U'+char(39)
+') and eventid=' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent='+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype<>'+ char(39) + 'D'+ char(39)
+ ' and (defopt='+ char(39) + 'T'+ char(39)
+ ' or '+ char(39) + 'T'+ char(39) +' not in '
+ ' (select defaultopt from mpay where eventid=' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and Actflag<>'+ char(39) + 'D'+ char(39) +'))'
+ ' Order By OptionID,SerialID' as MPAYlink,
Changed,
null as PAYLINK,
Opendate as OPENLINK,
maintab.Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'130' as CAref1,
maintab.EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MRGR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(lse.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , Recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.mpay.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+substring(maintab.Offerorname,1,23)
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'-}$'
From wca2.dbo.i_EQ_TEND_TKOVR as maintab
left outer join smf4.dbo.security as lse on maintab.sedol = lse.sedol
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'TKOVR' = wca.dbo.mpay.sevent
                                             and 'D'=wca.dbo.mpay.paytype and 'D'<>wca.dbo.mpay.actflag
WHERE 
maintab.sedol<>'' and maintab.sedol is not null
and maintab.opol<>'SHSC' and maintab.opol<>'XSSC'
and CmAcqdate is not null
/*and CalcListdate<=maintab.announcedate
and CalcDelistdate>=maintab.announcedate*/
order by caref2 desc, caref1, caref3
