--SEMETABLE=client.dbo.seme_citibank_i
--FileName=edi_YYYYMMDD
--MEET_AGM_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'161' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
CASE WHEN AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(lse.opol)
     END,
/* CASE WHEN pvcurrency <>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':94E::MEET//'+Country as TIDYTEXT,
substring(rtrim(Add1),1,35) as TIDYTEXT,
substring(rtrim(Add1),36,35) as TIDYTEXT,
substring(rtrim(Add2),1,35) as TIDYTEXT,
substring(rtrim(Add2),36,35) as TIDYTEXT,
substring(rtrim(Add3),1,35) as TIDYTEXT,
substring(rtrim(Add4),1,35) as TIDYTEXT,
substring(rtrim(Add5),1,35) as TIDYTEXT,
substring(rtrim(Add6),1,35) as TIDYTEXT,
substring(rtrim(City),1,35) as TIDYTEXT,
':16S:CADETL',
'-}$'
From wca2.dbo.i_EQ_MEET as maintab
left outer join smf4.dbo.security as lse on maintab.sedol = lse.sedol
WHERE 
maintab.sedol<>'' and maintab.sedol is not null
and maintab.opol<>'SHSC' and maintab.opol<>'XSSC'
/* and AGMDate>getdate()-8 and AGMDate is not null */
and (AGMEGM = 'CG' or AGMEGM = 'GM' or AGMEGM = 'EG' or AGMEGM = 'SG' or AGMEGM = 'AG')
and CalcListdate<=announcedate
and CalcDelistdate>=announcedate
order by caref2 desc, caref1, caref3
