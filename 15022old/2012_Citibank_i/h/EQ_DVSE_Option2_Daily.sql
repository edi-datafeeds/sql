--SEMETABLE=client.dbo.seme_citibank_i
--FileName=edi_YYYYMMDD
--DVSE_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=
--archive=off
--ArchivePath=n:\15022\Citibank\
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=ON

--# 1
use wca
select distinct
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'104' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN  maintab.Actflag = 'D' or mainoptn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'C' or mainoptn.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVSE' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN mainoptn.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN mainoptn.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN mainoptn.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN mainoptn.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN mainoptn.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN mainoptn.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resSCMST.isin<>''
     THEN ':35B:' + resSCMST.isin
     WHEN mainoptn.ressecid is null
     THEN ':35B:UKWN'
     ELSE ':35B:' + sedol
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN mainoptn.RatioNew <>''
          AND mainoptn.RatioOld <>''
     THEN ':92D::ADEX//'+ltrim(cast(mainoptn.RatioNew as char (15)))+
                    '/'+ltrim(cast(mainoptn.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN paydate<>'' and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
from wca2.dbo.h_EQ_DV_CA_SE_OP as maintab
left outer join divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 2=mainoptn.optionid and 'D'<>mainoptn.actflag and 'C'<>mainoptn.actflag
left outer join scmst as ResSCMST ON mainoptn.ResSecID = ResSCMST.SecID
left outer join divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 1=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
WHERE 
sedol<>'' and sedol is not null
and maintab.dripcntrycd=''
and mainoptn.divid is not null 
and (mainoptn.divtype='B' or mainoptn.divtype='S')
and divopt2.divid is null
and divopt3.divid is null
order by caref2 desc, caref1, caref3
