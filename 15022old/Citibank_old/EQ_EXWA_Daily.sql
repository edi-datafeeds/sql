--SEMETABLE=client.dbo.seme_citibank_i
--FileName=edi_YYYYMMDD
--EXWA_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON

--# 1
use wca
select distinct
Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'300' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXWA' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(lse.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN ExpirationDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , ExpirationDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN ExpirationDate IS NOT NULL
     THEN ':98A::EXPI//'+CONVERT ( varchar , ExpirationDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN ToDate IS NOT NULL
       AND FromDate IS NOT NULL
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/'
          +CONVERT ( varchar , ToDate,112)
     WHEN FromDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/UKWN'
     WHEN ToDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN exerTFB1<>''
     THEN ':35B:' + exerTFB1
     ELSE ':35B:' + 'UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew<>''
          AND RatioOld<>''
     THEN ':92D::NEWO//'+rtrim(cast(RatioNew as char (15)))+
                    '/'+rtrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN FromDate IS NOT NULL AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN FromDate IS NOT NULL AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN maintab.StrikePrice<>''
     THEN ':90B::PRPP//ACTU/'+CurenCD+rtrim(cast(maintab.StrikePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From wca2.dbo.i_EQ_EXWA as maintab
left outer join smf4.dbo.security as lse on maintab.sedol = lse.sedol
WHERE 
maintab.sedol<>'' and maintab.sedol is not null
/* and ExpirationDate>getdate()-8 and ExpirationDate is not null */
and CalcListdate<=announcedate
and CalcDelistdate>=announcedate
and EventID is not null
order by caref2 desc, caref1, caref3
