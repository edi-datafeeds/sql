--SEMETABLE=client.dbo.seme_citibank_i
--FileName=edi_YYYYMMDD
--SUSP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON

--# 1
use wca
select distinct
Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'152' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SUSP' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(lse.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN NotificationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN EffectiveDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'-}$'
From wca2.dbo.i_EQ_ACTV as maintab
inner join smf4.dbo.security as lse on maintab.sedol = lse.sedol and maintab.mcd = lse.opol
WHERE 
maintab.sedol<>'' and maintab.sedol is not null
/* and effectivedate>getdate()-8 and effectivedate is not null */
and LStatStatus = 'S'
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3