--SEMETABLE=client.dbo.seme_citibank_i
--FileName=edi_YYYYMMDD
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_Citibank_i\
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=ON

--# 1
use wca
select distinct
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'103' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN  maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEP//DISN',
':22F::CAEV//DRCA' as CAEV,
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
/* CASE WHEN pvcurrency <>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN divopt1.CurenCD <> '' AND divopt1.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + divopt1.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  Paydate <>'' AND Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN  divopt1.depfees <>'' and divopt1.depfees <>'0'
     THEN ':92F::CHAR//USD'+ltrim(cast(divopt1.depfees as char(15)))
     ELSE ':92K::CHAR//UKWN'
     END as COMMASUB,
CASE WHEN divopt1.Grossdividend <> '' AND divopt1.Grossdividend IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+ divopt1.CurenCD+ltrim(cast(divopt1.Grossdividend as char(15)))
     ELSE ''
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From wca2.dbo.i_EQ_DV_CA_SE_OP as maintab
left outer join divpy as divopt1 on maintab.eventid = divopt1.divid and 1=divopt1.optionid and 'D'<>divopt1.actflag
left outer join divpy as divopt2 on maintab.eventid = divopt2.divid and 2=divopt2.optionid and 'D'<>divopt2.actflag
left outer join divpy as divopt3 on maintab.eventid = divopt3.divid and 3=divopt3.optionid and 'D'<>divopt3.actflag
left outer join wca.dbo.drip on maintab.eventid=wca.dbo.drip.divid
WHERE 
sedol<>'' and sedol is not null
and wca.dbo.drip.divid is null
and divopt2.divid is null
and divopt3.divid is null
and divopt1.divid is not null
and divopt1.divtype='C'
and maintab.divperiodcd<>'ISC'
and maintab.sectycd='DR'
and (paydate>getdate()-183 or paydate is null)
order by caref2 desc, caref1, caref3
