--SEMETABLE=client.dbo.seme_sungard
--FileName=edi_YYYYMMDD
--EXRI_RTS_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\sungard\
--FileTidy=N
--TFBNUMBER=3
--INCREMENTAL=OFF
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  RTSNotes As Notes FROM RTS WHERE RDID = '+ cast(EventID as char(16)) as ChkNotes, 
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'118'+EXCHGID as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXRI',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , announcedate,112) + replace(convert ( varchar, announcedate, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
':16R:LINK',
'160' as CAref4,
':16S:LINK',
':16S:GENL',
':16R:USECU',
':97A::SAFE//GENR' as MT568only,
':35B:' + mainTFB3,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM/' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndSubscription <>''
       AND StartSubscription <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/'
          +CONVERT ( varchar , EndSubscription,112)
     WHEN StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/UKWN'
     WHEN EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB3<>'UKWN' and resTFB3<>''
     THEN ':35B:' + resTFB3
     ELSE ':35B:' + mainTFB3
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN EndTrade <> '' 
       AND StartTrade <> '' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/'
          +CONVERT ( varchar , EndTrade,112)
     WHEN StartTrade <> '' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/UKWN'
     WHEN EndTrade <> '' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
':92D::NEWO//1,/1,',
CASE WHEN StartTrade <>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN StartTrade <>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN IssuePrice <> ''
     THEN ':90B::PRPP//ACTU/'+CurenCD+ltrim(cast(IssuePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//LAPS',
':17B::DFLT//Y',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_EQ_EXRI_RTS
WHERE 
mainTFB3<>''
and changed>'2013/09/09'
and uscode in (select code from client.dbo.pfuscode where accid=252)
and exchgid<>'' and exchgid is not null
and 'US' = substring(exchgcd,1,2)
order by caref2 desc, caref1, caref3