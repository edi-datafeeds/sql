--SEMETABLE=client.dbo.seme_citiuat
--FileName=edi_YYYYMMDD
--DVCA_UT_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\Citibank\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select
case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end as Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
maintab.cntrycd as MPAYLINK3,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN (maintab.exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.exdate is not null
       OR maintab.recdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.recdate is not null)
              and SR.TFB<>':35B:/GB/'+maintab.Sedol
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '106' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//ACCU' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,112) + replace(convert ( varchar, case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE '' END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     WHEN maintab.Actflag = 'D'
     THEN ':98A::XDTE//20990101'
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN maintab.equalisation<>'' and maintab.equalisation <>'0' and maintab.equalisation is not null
     THEN ':92F::EQUL//'+ maintab.CurenCD+ltrim(cast(maintab.equalisation as char(15)))
     WHEN  nildividend='T'
     THEN ''
     ELSE ':92K::EQUL//UKWN'
     END as COMMASUB,
CASE 
     WHEN  nildividend='T'
     THEN ':92F::NETT//GBP0,'
     WHEN maintab.NetDividend<>'' AND maintab.NetDividend<>'0' AND maintab.NetDividend is not null
     THEN ':92J::NETT//FUPU/'+ maintab.CurenCD+ltrim(cast(maintab.Netdividend as char(15)))
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN maintab.Group2NetDiv<>'' AND maintab.Group2NetDiv<>'0' and maintab.Group2NetDiv is not null
     THEN ':92J::NETT//PAPU/'+ maintab.CurenCD+ltrim(cast(maintab.Group2NetDiv as char(15)))
     ELSE ''
     END as COMMASUB,
CASE WHEN maintab.Taxrate<>'' AND maintab.Taxrate IS NOT NULL
     THEN ':92A::TAXC//'+maintab.TaxRate
     ELSE ''
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
from wca2.dbo.i_EQ_DVCA_ACCU as maintab
left outer join client.dbo.seme_citiuat as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'ACCU'=SR.CAEVMAIN
WHERE
maintab.sedol<>'' and maintab.sedol is not null
order by caref2 desc, caref1, caref3
