--SEMETABLE=client.dbo.seme_citiuat
--FileName=edi_YYYYMMDD
--PRIO_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end as Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Exdate is not null
              and SR.TFB<>':35B:/GB/'+maintab.Sedol
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '110' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,112) + replace(convert ( varchar, case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE '' END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     WHEN maintab.Actflag = 'D'
     THEN ':98A::XDTE//20990101'
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASE',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN maintab.EndSubscription<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN maintab.EndSubscription<>''
       AND maintab.StartSubscription<>''
      THEN ':69A::PWAL//'
          +CONVERT ( varchar , maintab.StartSubscription,112)+'/'
          +CONVERT ( varchar , maintab.EndSubscription,112)
     WHEN maintab.StartSubscription<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , maintab.StartSubscription,112)+'/UKWN'
     WHEN maintab.EndSubscription<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , maintab.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.secid<>maintab.ressecid and resTFB1<>''
     THEN ':35B:' + resTFB1
     WHEN maintab.secid<>maintab.ressecid and resTFB1=''
     THEN ':35B:UKWN'
     ELSE ':35B:' + mainTFB2
     END,
':16R:FIA',
CASE
     WHEN maintab.secid<>maintab.ressecid and resTFB1<>''
     THEN ':94B::PLIS//SECM'
     WHEN maintab.secid<>maintab.ressecid and resTFB1=''
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
':16S:FIA',
CASE WHEN maintab.RatioNew<>'' AND maintab.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(maintab.RatioNew as char (15)))+
                    '/'+ltrim(cast(maintab.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN paydate2<>'' AND paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate2,112)
     WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
case when prf2.prfid is not null then ':16R:SECMOVE' ELSE '' end,
case when prf2.prfid is not null then ':22H::CRDB//CRED' ELSE '' end,
case WHEN prf2.prfid is not null and prf2scmst.isin is not null and prf2scmst.isin<>''
     THEN ':35B:ISIN ' + prf2scmst.isin
     WHEN prf2.prfid is not null
     THEN ':35B:UKWN'
     ELSE ''
     END,
case when prf2.prfid is not null then ':16R:FIA' ELSE '' end,
CASE 
     WHEN prf2.prfid is not null
     THEN ':94B::PLIS//SECM'
     ELSE ''
     END,
case when prf2.prfid is not null then ':16S:FIA' ELSE '' end,
CASE WHEN prf2.prfid is not null and prf2.RatioNew<>'' AND prf2.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(prf2.RatioNew as char (15)))+
                    '/'+ltrim(cast(prf2.RatioOld as char (15))) 
     WHEN prf2.prfid is not null
     THEN ':92K::ADEX//UKWN' 
     ELSE ''
     END as COMMASUB,
CASE WHEN prf2.prfid is not null and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     WHEN prf2.prfid is not null
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
case when prf2.prfid is not null then ':16S:SECMOVE' ELSE '' end,
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN maintab.MaxPrice<>''
     THEN ':90B::PRPP//ACTU/'+maintab.CurenCD+
          +substring(maintab.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
case when prf2.prfid is not null then ':16R:CASHMOVE' ELSE '' end,
case when prf2.prfid is not null then ':22H::CRDB//DEBT' ELSE '' end,
CASE WHEN prf2.prfid is not null and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     WHEN prf2.prfid is not null
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN prf2.prfid is not null and prf2.MaxPrice<>''
     THEN ':90B::PRPP//ACTU/'+prf2.CurenCD+
          +substring(prf2.MaxPrice,1,15)
     WHEN prf2.prfid is not null
     THEN ':90E::PRPP//UKWN'
     ELSE ''
     END as COMMASUB,
case when prf2.prfid is not null then ':16S:CASHMOVE' ELSE '' end,
':16S:CAOPTN',
'-}$'
From wca2.dbo.i_EQ_PRIO_PRF as maintab
inner join serialseq as mainprf on maintab.eventid = mainprf.rdid and 1=mainprf.seqnum and 'PRF'=mainprf.eventcd
left outer join client.dbo.seme_citiuat as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'PRIO'=SR.CAEVMAIN
left outer join serialseq on maintab.eventid = serialseq.rdid and 2=serialseq.seqnum and 'PRF'=serialseq.eventcd
left outer join prf as prf2 on serialseq.uniqueid = prf2.prfid
left outer join scmst as prf2scmst on prf2.ressecid = prf2scmst.secid
WHERE
maintab.sedol<>'' and maintab.sedol is not null
/* and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog) */
/* and Exdate>getdate()-8 and Exdate is not null */
order by caref2 desc, caref1, caref3
