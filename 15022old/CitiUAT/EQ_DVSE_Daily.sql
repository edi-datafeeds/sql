--SEMETABLE=client.dbo.seme_citiuat
--FileName=edi_YYYYMMDD
--DVSE_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=
--archive=off
--ArchivePath=n:\15022\Citibank\
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end as Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN (maintab.exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.exdate is not null
       OR maintab.recdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.recdate is not null)
              and (SR.CAMV<>'MAND' or SR.TFB<>':35B:/GB/'+maintab.Sedol)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '104' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C' or mainoptn.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D' or mainoptn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVSE' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,112) + replace(convert ( varchar, case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE ''
     END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN exdate2<>'' and Exdate2 IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate2,112)
     WHEN maintab.Actflag = 'D'
     THEN ':98A::XDTE//20990101'
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN mainoptn.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN mainoptn.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN mainoptn.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN mainoptn.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN mainoptn.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN mainoptn.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
case when ResSEDOL.sedol is not null and ResSEDOL.sedol<>''
     THEN ':35B:/GB/' + ResSEDOL.sedol
     WHEN mainoptn.ressecid is null
     THEN ':35B:/GB/' + maintab.sedol
     WHEN resSCMST.isin<>''
     THEN ':35B:ISIN ' + resSCMST.isin
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
case when ResSEDOL.opol is not null and ResSEDOL.opol<>''
     then ':94B::PLIS//EXCH/'+ResSEDOL.opol
     when mainoptn.ressecid is null
     then ':94B::PLIS//EXCH/'+maintab.opol
     else ':94B::PLIS//EXCH/SECM'
     end,
':16S:FIA',
CASE WHEN mainoptn.RatioNew<>''
          AND mainoptn.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(mainoptn.RatioNew as char (15)))+
                    '/'+ltrim(cast(mainoptn.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN paydate2<>'' and Paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate2,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
from wca2.dbo.i_EQ_DV_CA_SE_OP as maintab
left outer join client.dbo.seme_citiuat as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'DVSE'=SR.CAEVMAIN
left outer join v10s_divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid
left outer join scmst as ResSCMST ON mainoptn.ResSecID = ResSCMST.SecID
left outer join sedolseq3 as ResSEDOL ON mainoptn.ResSecID = ResSEDOL.SecID
                             and maintab.cntrycd=ResSEDOL.CntryCD
                             and maintab.rcntrycd=ResSEDOL.RCntryCD
                             and maintab.STCurrency=ResSEDOL.CurenCD
                             and maintab.opol=ResSEDOL.Opol
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
WHERE
maintab.sedol<>'' and maintab.sedol is not null
and maintab.dripcntrycd=''
and mainoptn.divid is not null 
and mainoptn.approxflag<>'T'
and (mainoptn.divtype='B' or mainoptn.divtype='S')
and divopt2.divid is null
and divopt3.divid is null
order by caref2 desc, caref1, caref3
