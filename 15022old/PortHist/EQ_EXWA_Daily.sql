--SEMETABLE=client.dbo.seme_sample
--FileName=YYYYMMDD
--EXWA_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed as Changed,
'select  WartmNotes As Notes FROM WARTM WHERE SECID = '+ cast(SecID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01SAMPLE22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'170'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXWA' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , maintab.Changed,112) + replace(convert ( varchar, maintab.Changed,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:ISIN ' + maintab.isin as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD
     ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN ExpirationDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , ExpirationDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN ExpirationDate IS NOT NULL
     THEN ':98A::EXPI//'+CONVERT ( varchar , ExpirationDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN ToDate IS NOT NULL
       AND FromDate IS NOT NULL
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/'
          +CONVERT ( varchar , ToDate,112)
     WHEN FromDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/UKWN'
     WHEN ToDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN exerTFB1<>''
     THEN ':35B:' + exerTFB1
     ELSE ':35B:' + 'UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew<>''
          AND RatioOld<>''
     THEN ':92D::NEWO//'+rtrim(cast(RatioNew as char (15)))+
                    '/'+rtrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN FromDate IS NOT NULL AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN FromDate IS NOT NULL AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN maintab.StrikePrice<>''
     THEN ':90B::PRPP//ACTU/'+maintab.CurenCD+rtrim(cast(maintab.StrikePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_EXWA as maintab
left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.SecID=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'EXWA'=SR.CAEVMAIN
WHERE
maintab.isin<>'' and maintab.isin is not null
and substring(exchgcd,1,2)='AU'
and ((maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
and ExpirationDate > getdate())
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
and changed>(select max(feeddate)+1 from wca.dbo.tbl_Opslog where seq = 3)))
and EventID is not null
order by caref2 desc, caref1, caref3
