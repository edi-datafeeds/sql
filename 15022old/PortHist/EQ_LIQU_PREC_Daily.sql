--SEMETABLE=client.dbo.seme_sample
--FileName=YYYYMMDD
--LIQU_PREC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed as Changed,
'select  LiquidationTerms As Notes FROM LIQ WHERE LIQID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01SAMPLE22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'113'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , maintab.Changed,112) + replace(convert ( varchar, maintab.Changed,08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:ISIN ' + maintab.isin as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
':11A::OPTN//USD',
':17B::DFLT//N',
':16R:CASHMOVE',
':22H::CRDB//CRED',
':98B::PAYD//UKWN',
':90E::OFFR//UKWN',
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_LIQU as maintab
left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.SecID=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'LIQU'=SR.CAEVMAIN
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'LIQ' = wca.dbo.mpay.sevent
                                         and 'D'<>wca.dbo.mpay.actflag
WHERE
maintab.isin<>'' and maintab.isin is not null
and substring(exchgcd,1,2)='AU'
and ((maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
and RdDate > getdate())
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
and changed>(select max(feeddate)+1 from wca.dbo.tbl_Opslog where seq = 3)))
and wca.dbo.mpay.eventid is null
order by caref2 desc, caref1, caref3
