--SEMETABLE=client.dbo.seme_tscape
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_CONV
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
v_FI_CONV.Changed,
'select case when len(cast(ConvNotes as varchar(24)))<30 then rtrim(char(32)) else ConvNotes end As Notes FROM CONV WHERE ConvID = '+ cast(v_FI_CONV.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '325'+EXCHGID+cast(Seqnum as char(1))
     ELSE '325'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_FI_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>''
     then ':11A::DENO//'+debtcurrency
     else ''
     end,
case when MaturityDate is not null
     then ':98A::MATU//'++CONVERT(varchar, MaturityDate,112)
     else ''
     end,
':16S:FIA',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_CONV.FromDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/* CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END, */
/*'select  RDcase when len(cast(Reason as varchar(24)))=22 then rtrim(char(32)) else BkrpNotes end As Notes FROM RD WHERE RDID = '+ cast(v_FI_CONV.EventID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
       AND v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     WHEN v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/UKWN'
     WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_FI_CONV.Price  <>'' AND v_FI_CONV.Price <>''
     THEN ':90B::CINL//ACTU/'+v_FI_CONV.CurenCD+
          +substring(v_FI_CONV.Price,1,15)
     ELSE ':90E::CINL//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_CONV.resTFB1 <>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
CASE WHEN v_FI_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_FI_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_FI_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_FI_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_FI_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_FI_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v_FI_CONV.RatioNew <>''
          AND v_FI_CONV.RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_FI_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(v_FI_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN v_FI_CONV.SettlementDate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , v_FI_CONV.SettlementDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_CONV
WHERE
mainTFB1<>''
and (((isin in (select code from client.dbo.pfisin where accid=287 and actflag='I')
      or uscode in (select code from client.dbo.pfuscode where accid=287 and actflag='I')
      or sedol in (select code from client.dbo.pfsedol where accid=287 and actflag='I'))
and ToDate > getdate())
OR
((isin in (select code from client.dbo.pfisin where accid=287 and actflag='U')
or uscode in (select code from client.dbo.pfuscode where accid=287 and actflag='U')
or sedol in (select code from client.dbo.pfsedol where accid=287 and actflag='U'))
and changed>'2015/11/05' and changed<'2015/11/06'))

and convtype<>'BB'
and convtype<>'TENDER'
and Actflag  <>''
order by caref2 desc, caref1, caref3
