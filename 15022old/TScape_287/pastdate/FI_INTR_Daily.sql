--SEMETABLE=client.dbo.seme_tscape
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_INTR
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct top 50
v_FI_INTR.Changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then rtrim(char(32)) else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(v_FI_INTR.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '400'+EXCHGID+cast(Seqnum as char(1))
     ELSE '400'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_INTR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_INTR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_INTR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_INTR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INTR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' and localcode is not null
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency='USD'
     then ':22F::MICO//A006'
     else ''
     end,
case when debtcurrency<>''
     then ':11A::DENO//'+debtcurrency
     else ''
     end,
case when MaturityDate is not null
     then ':98A::MATU//'++CONVERT ( varchar , v_FI_INTR.MaturityDate,112)
     else ''
     end,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_INTR.ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_FI_INTR.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v_FI_INTR.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_FI_INTR.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN InterestFromdate <>'' AND InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/'
          +CONVERT ( varchar , InterestToDate,112)
     WHEN InterestFromDate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/UKWN'
     WHEN InterestToDate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , InterestToDate,112)
     ELSE ':69J::INPE//UKWN'
     END,
case when debtcurrency<>'USD' and days is not null
     then ':99A:DAAC//'+cast(days as varchar(20))+','
     else ''
     end,
CASE WHEN v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTR//' + v_FI_INTR.AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> ''
     THEN ':11A::OPTN//' + CurenCD 
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_INTR.Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , v_FI_INTR.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN v_FI_INTR.InterestPaymentFrequency = 'ANL'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + v_FI_INTR.AnlCoupRate
     WHEN v_FI_INTR.InterestPaymentFrequency = 'SMA'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'QTR'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'MNT'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,  
'-}$'
From v_FI_INTR
WHERE
mainTFB1<>''
and (((isin in (select code from client.dbo.pfisin where accid=287 and actflag='I')
      or uscode in (select code from client.dbo.pfuscode where accid=287 and actflag='I')
      or sedol in (select code from client.dbo.pfsedol where accid=287 and actflag='I'))
and recdate > getdate())
OR
((isin in (select code from client.dbo.pfisin where accid=287 and actflag='U')
or uscode in (select code from client.dbo.pfuscode where accid=287 and actflag='U')
or sedol in (select code from client.dbo.pfsedol where accid=287 and actflag='U'))
and changed>'2015/11/05' and changed<'2015/11/06'))
and (indefpay='' or indefpay = 'P')
order by caref2 desc, caref1, caref3
