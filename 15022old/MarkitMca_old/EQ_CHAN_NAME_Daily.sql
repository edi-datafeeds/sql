--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_YYYYMMDD
--CHAN_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select IschgNotes As Notes FROM ISCHG WHERE ISCHGID = '+ cast(EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.NameChangeDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.NameChangeDate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '153' END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(IssOldname,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, AnnounceDate,112) ELSE '' END,
CASE WHEN NameChangeDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//NAME',
':70E::NAME//' + substring(replace(IssNewname,'&','and'),1,35) as TIDYTEXT,
':16S:CADETL',
CASE WHEN newisin <>'' THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN newisin <>'' THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN newisin <>'' THEN ':22F::CAOP//SECU' ELSE '' END,
CASE WHEN newisin <>'' THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16R:SECMOVE' ELSE '' END,
CASE WHEN newisin <>'' THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN newisin <>'' THEN ':35B:ISIN ' + newisin ELSE '' END,
CASE WHEN newisin <>'' THEN ':16R:FIA' ELSE '' END,
CASE WHEN newisin <>'' THEN ':94B::PLIS//EXCH/SECM' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16S:FIA' ELSE '' END,
CASE WHEN newisin <>'' THEN ':92D::NEWO//1,/1,' ELSE '' END,
CASE WHEN newisin <>'' and icceffectivedate is not null 
     THEN ':98A::PAYD//'+CONVERT ( varchar , icceffectivedate,112)
     WHEN newisin <>'' and icceffectivedate is null 
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN newisin <>'' THEN ':16S:SECMOVE' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16S:CAOPTN' ELSE '' END,
'' as Notes, 
'-}$'
From wca2.dbo.i_EQ_CHAN_NAME as maintab
left outer join client.dbo.seme_markitmca as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CHAN'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
