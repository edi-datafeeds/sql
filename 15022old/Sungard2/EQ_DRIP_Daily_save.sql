--SEMETABLE=client.dbo.seme_sungard2
--FileName=edi_YYYYMMDD
--DRIP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON

--# 1
use wca
select distinct
Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'101' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE 
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN  DripActflag = 'D'
     THEN ':23G:CANC'
     WHEN  DripActflag = 'C'
     THEN ':23G:WITH'
     WHEN  DivpyActflag = 'D'
     THEN ':23G:CANC'
     WHEN  DivpyActflag = 'C'
     THEN ':23G:WITH'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DRIP' as CAEV,
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(lse.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     ELSE ''
     END,
CASE WHEN   Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar ,  Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CG'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='INS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='MEM'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='SUP'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='TEI'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='ARR'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ONE'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='UN'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='VAR'
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN DripLastDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar ,  DripLastDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN maintab.structcd='XREIT' and maintab.Netdividend<>'' AND maintab.Netdividend IS NOT NULL
     THEN ':92J::NETT//REES/'+ maintab.CurenCD+ltrim(cast(maintab.Netdividend as char(15)))
     WHEN maintab.Netdividend<>'' AND maintab.Netdividend IS NOT NULL
     THEN ':92F::NETT//'+ maintab.CurenCD+ltrim(cast(maintab.Netdividend as char(15)))
     WHEN maintab.Grossdividend<>'' AND  maintab.Grossdividend IS NOT NULL
     THEN ''
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN maintab.structcd='XREIT' and maintab.Grossdividend<>'' AND  maintab.Grossdividend IS NOT NULL
     THEN ':92J::GRSS//REES/'+ maintab.CurenCD+ltrim(cast(maintab.Grossdividend as char(15)))
     WHEN maintab.Grossdividend<>'' AND  maintab.Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ maintab.CurenCD+ltrim(cast(maintab.Grossdividend as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
CASE WHEN maintab.frankdiv<>''
     THEN ':92J::GRSS//FLFR/'+ maintab.CurenCD+ltrim(cast(maintab.frankdiv as char(15))) +'/ACTU'
     ELSE '' 
     END as COMMASUB,
CASE WHEN maintab.unfrankdiv<>''
     THEN ':92J::GRSS//UNFR/'+ maintab.CurenCD+ltrim(cast(maintab.unfrankdiv as char(15))) +'/ACTU'
     ELSE '' 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:/GB/' + maintab.Sedol as TFB,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN  DripReinvPrice<>'' AND  DripReinvPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+ maintab.CurenCD+ltrim(cast( DripReinvPrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
CASE WHEN  dripPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  DripPaydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_DRIP_2011 as maintab
left outer join smf4.dbo.security as lse on maintab.sedol = lse.sedol
left outer join divpy as divopt1 on maintab.eventid = divopt1.divid 
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag
                   and 1=divopt1.optionid and 'C'<>divopt1.actflag
left outer join divpy as divopt2 on maintab.eventid = divopt2.divid 
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join divpy as divopt3 on maintab.eventid = divopt3.divid 
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
WHERE 
maintab.sedol in (select code from client.dbo.pfsedol where accid=996 and actflag<>'D')
and changed>getdate()-32
/* and (exdate>getdate()-8 or (exdate is null and maintab.announcedate>getdate()-31) 
     or maintab.actflag='C' or maintab.actflag='D') */
and not ((divopt1.actflag is not null and divopt2.actflag is not null)
  or (divopt1.actflag is not null and divopt3.actflag is not null)      
  or (divopt2.actflag is not null and divopt3.actflag is not null))
and CalcListdate<=maintab.announcedate
and CalcDelistdate>=maintab.announcedate
and (CalcListdate<=Exdate or (Exdate is null and CalcListdate<=maintab.announcedate))
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=maintab.announcedate))
order by caref2 desc, caref1, caref3
