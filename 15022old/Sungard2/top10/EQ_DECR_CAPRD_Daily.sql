--SEMETABLE=client.dbo.seme_citibank_i
--FileName=edi_YYYYMMDD
--DECR_CAPRD_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct top 10
Changed,
'select case when len(cast(caprdnotes as varchar(255)))>40 then caprdnotes else rtrim(char(32)) end As Notes FROM CAPRD WHERE CAPRDID = '+ cast(EventID as char(16)) as ChkNotes, 
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'148' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(lse.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN effectivedate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT/'+NewParValue as commasub,
':16S:CADETL',
CASE WHEN rcapid is not null THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN rcapid is not null THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22F::CAOP//CASH' ELSE '' END,
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
CASE WHEN rcapid is not null THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16R:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN CSPYDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     WHEN rcapid is not null THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN CashBak<>'' AND CashBak IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+CurenCD+substring(CashBak,1,15)
     WHEN rcapid is not null THEN ':90E::OFFR//UKWN'
     ELSE ''
     END as Commasub,
CASE WHEN rcapid is not null THEN ':16S:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16S:CAOPTN' ELSE '' END,
/* '' as Notes, */
'-}$'
From v_EQ_DECR_CAPRD as maintab
left outer join smf4.dbo.security as lse on maintab.sedol = lse.sedol
WHERE 
maintab.sedol<>''
and rtrim(OldParValue)<>'' and rtrim(NewParValue)<>''
and rtrim(OldParValue)<>'0' and rtrim(NewParValue)<>'0'
and cast(rtrim(OldParValue) as float(16,7))>cast(rtrim(NewParValue) as float(16,7))
/* and effectivedate>getdate()-8 and effectivedate is not null */
and CalcListdate<=announcedate
and CalcDelistdate>=announcedate
order by caref2 desc, caref1, caref3
