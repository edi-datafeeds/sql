--SEMETABLE=client.dbo.seme_sungard2
--FileName=edi_YYYYMMDD
--EXOF_CTX_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON

--# 1
use wca
select distinct
Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'127' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(lse.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
CASE WHEN EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndDate IS NOT NULL
       AND StartDate IS NOT NULL
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/'
          +CONVERT ( varchar , EndDate,112)
     WHEN StartDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/UKWN'
     WHEN EndDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>'UKWN' and resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':92K::NEWO//UKWN',
CASE WHEN StartDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_EXOF_CTX as maintab
left outer join smf4.dbo.security as lse on maintab.sedol = lse.sedol
WHERE 
maintab.sedol in (select code from client.dbo.pfsedol where accid=996 and actflag<>'D')
and changed>getdate()-32
/*and Enddate>getdate()-8 and Enddate is not null */
and CalcListdate<=announcedate
and CalcDelistdate>=announcedate
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
