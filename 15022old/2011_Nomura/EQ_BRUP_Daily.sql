--SEMETABLE=client.dbo.seme_sample
--FileName=YYYYMMDD_EQ_MT564
--BRUP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2011_Nomura\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1

--# 1
use wca
select
Changed,
/* 'select  BkrpNotes As Notes FROM BKRP WHERE BKRPID = '+ cast(EventID as char(16)) as ChkNotes, */ 
'' as ISOHDR,
':16R:GENL',
'119'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BRUP',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN NotificationDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN FilingDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , FilingDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'-}$'
From v_EQ_BRUP
WHERE 
mainTFB1<>''
and (primaryexchgcd=exchgcd or primaryexchgcd='')
and (mcd<>'' and mcd is not null)
and NotificationDate>getdate()-31 and NotificationDate is not null
and CalcListdate<=Announcedate
and CalcDelistdate>=Announcedate
AND changed>=(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
order by caref2 desc, caref1, caref3