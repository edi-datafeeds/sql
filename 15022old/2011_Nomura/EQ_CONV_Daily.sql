--SEMETABLE=client.dbo.seme_sample
--FileName=YYYYMMDD_EQ_MT564
--CONV_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2011_Nomura\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1

--# 1
use wca
select
Changed,
'' as ISOHDR,
':16R:GENL',
'125'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END as CHOICE1,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN FromDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD
     END,
':17B::DFLT//N',
CASE WHEN ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN ToDate <>'' AND FromDate <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/'
          +CONVERT ( varchar , ToDate,112)
     WHEN FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/UKWN'
     WHEN ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>'UKWN' and resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew is not null AND RatioOld  is not null
          and RatioNew <>'' AND RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(RatioNew as char (15)))+
                    '/'+rtrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN FromDate <>'' AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' END,':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN FromDate <>'' AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN Price <>''
     THEN ':90B::EXER//ACTU/'+CurenCD+
          +substring(Price,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_CONV
WHERE 
mainTFB1<>''
and (primaryexchgcd=exchgcd or primaryexchgcd='')
and (mcd<>'' and mcd is not null)
and ToDate>getdate()-31 and ToDate is not null
and CalcListdate<=Announcedate
and CalcDelistdate>=Announcedate
AND changed>=(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and Actflag <> ''
order by caref2 desc, caref1, caref3