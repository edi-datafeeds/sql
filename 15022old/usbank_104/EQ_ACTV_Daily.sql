--SEMETABLE=client.dbo.seme_usbank
--FileName=edi_YYYYMMDD
--ACTV_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=n
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=1
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select Reason As Notes FROM LSTAT WHERE LSTATID = '+ cast(EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01USBKUS4TAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '150'+EXCHGID
     ELSE '150'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ACTV' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN notificationdate<>'' and NotificationDate is not null
     THEN ':98A::ANOU//'+CONVERT ( varchar , NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN effectivedate<>'' and EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
/* '' as Notes, */ 
'-}'
From v_EQ_ACTV as maintab
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=104 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=104 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=104 and actflag='I'))
      and effectivedate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=104 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=104 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=104 and actflag='U'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)))
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
and Actflag <> ''
and LStatStatus = 'R'
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
