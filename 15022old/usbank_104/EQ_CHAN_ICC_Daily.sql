--SEMETABLE=client.dbo.seme_usbank
--FileName=edi_YYYYMMDD
--CHAN_ICC_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=1
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
case when 1=1 then (select '{1:F01USBKUS4TAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '192'+EXCHGID
     ELSE '192'
     END as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:ISIN ' + oldisin as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, announcedate,112) ELSE '' END,
CASE WHEN Effectivedate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Effectivedate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//TERM',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:ISIN ' + maintab.newisin,
':16R:FIA',
':94B::PLIS//EXCH/SECM',
':16S:FIA',
':92D::NEWO//1,/1,',
CASE WHEN Effectivedate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , Effectivedate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}'
From v_EQ_CHAN_ICC as maintab
WHERE
maintab.oldisin<>''
and ((maintab.oldisin in (select code from client.dbo.pfisin where accid=104 and actflag='I')
and effectivedate > getdate())
OR
(maintab.oldisin in (select code from client.dbo.pfisin where accid=104 and actflag='U')
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and maintab.oldisin <>'' and maintab.newisin<>''
and maintab.oldisin<>maintab.newisin
and maintab.eventtype<>'CAPRD'
and maintab.eventtype<>'CLEAN'
and maintab.eventtype<>'CONSD'
and maintab.eventtype<>'CORR'
and maintab.eventtype<>'DEFUNCT'
and maintab.eventtype<>'ISCHG'
and maintab.eventtype<>'RESTORE'
and maintab.eventtype<>'SD'
order by caref2 desc, caref1, caref3
