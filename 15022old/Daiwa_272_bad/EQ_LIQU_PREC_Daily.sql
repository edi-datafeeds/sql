--SEMETABLE=client.dbo.seme_daiwa_272
--FileName=edi_YYYYMMDD
--LIQU_PREC_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=LIQ
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select distinct
'select distinct * from V10s_MPAY'
+ ' where (Actflag='+char(39)+'I'+char(39)+ ' or Actflag='+char(39)+'U'+char(39)
+') and eventid=' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent='+ char(39) + 'LIQ'+ char(39)
+ ' and Paytype<>'+ char(39) + 'D'+ char(39)
+ ' Order By OptionID,SerialID' as MPAYlink,
Changed,
null as PAYLINK,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'113'+EXCHGID+'1' as CAref1,
maintab.EventID as CAREF2,
maintab.Secid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE '' END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
':11A::OPTN//USD',
':17B::DFLT//N',
':16R:CASHMOVE',
':22H::CRDB//CRED',
':98B::PAYD//UKWN',
':90E::OFFR//UKWN',
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
from wca2.dbo.t_EQ_LIQU as maintab
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'LIQ' = wca.dbo.mpay.sevent
                                             and 'D'=wca.dbo.mpay.paytype and 'D'<>wca.dbo.mpay.actflag
WHERE 
wca.dbo.mpay.eventid is null
and exchgid<>'' and exchgid is not null
and (maintab.exchgcd=maintab.primaryexchgcd or maintab.primaryexchgcd='')
and maintab.isin in (select code from client.dbo.pfisin where accid=272 and actflag<>'D')
order by caref2 desc, caref1, caref3
