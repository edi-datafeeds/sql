--SEMETABLE=client.dbo.seme_daiwa_272
--FileName=edi_YYYYMMDD
--CHAN_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select distinct
Changed,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'153'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
maintab.Secid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:' + mainTFB1 as TFB,
/* case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end, */
substring(IssOldname,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ') ELSE '' END,
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
/* CASE WHEN cfi <> '' THEN ':12C::CLAS//' +cfi ELSE '' END, */
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN NameChangeDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//NAME',
':70E::NAME//' + substring(replace(IssNewname,'&','and'),1,35) as TIDYTEXT,
':16S:CADETL',
CASE WHEN newisin <>'' THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN newisin <>'' THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN newisin <>'' THEN ':22F::CAOP//SECU' ELSE '' END,
CASE WHEN newisin <>'' THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16R:SECMOVE' ELSE '' END,
CASE WHEN newisin <>'' THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN newisin <>'' THEN ':35B:ISIN ' + newisin ELSE '' END,
CASE WHEN newisin <>'' THEN ':16R:FIA' ELSE '' END,
CASE WHEN newisin <>'' THEN ':94B::PLIS//SECM' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16S:FIA' ELSE '' END,
CASE WHEN newisin <>'' THEN ':92D::NEWO//1,/1,' ELSE '' END,
CASE WHEN newisin <>'' and icceffectivedate is not null 
     THEN ':98A::PAYD//'+CONVERT ( varchar , icceffectivedate,112)
     WHEN newisin <>'' and icceffectivedate is null 
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN newisin <>'' THEN ':16S:SECMOVE' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16S:CAOPTN' ELSE '' END,
'-}$'
from v_EQ_CHAN_NAME as maintab
WHERE 
upper(eventtype)<>'CLEAN'
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and exchgid<>'' and exchgid is not null
and (maintab.exchgcd=maintab.primaryexchgcd or maintab.primaryexchgcd='')
and maintab.isin in (select code from client.dbo.pfisin where accid=272 and actflag<>'D')
order by caref2 desc, caref1, caref3
