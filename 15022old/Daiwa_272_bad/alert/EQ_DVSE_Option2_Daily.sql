--SEMETABLE=client.dbo.seme_daiwa_272
--FileName=edi_YYYYMMDD
--DVSE_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\Citibank\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select distinct
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
maintab.Secid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C' or mainoptn.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D' or mainoptn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVSE' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:' + mainTFB1 as TFB,
/* case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end, */
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ') ELSE '' END,
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN maintab.PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN maintab.PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
/* CASE WHEN maintab.cfi <> '' THEN ':12C::CLAS//'+maintab.cfi ELSE '' END, */
/* CASE WHEN maintab.pvcurrency<>'' THEN ':11A::DENO//' + maintab.pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN exdate2<>'' and Exdate2 IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate2,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN mainoptn.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN mainoptn.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN mainoptn.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN mainoptn.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN mainoptn.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN mainoptn.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resSCMST.isin<>''
     THEN ':35B:ISIN ' + resSCMST.isin
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN mainoptn.RatioNew<>''
          AND mainoptn.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(mainoptn.RatioNew as char (15)))+
                    '/'+ltrim(cast(mainoptn.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN paydate2<>'' and Paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate2,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
from v_EQ_DV_CA_SE_OP as maintab
left outer join v10s_divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 2=mainoptn.optionid and 'D'<>mainoptn.actflag and 'C'<>mainoptn.actflag
left outer join scmst as ResSCMST ON mainoptn.ResSecID = ResSCMST.SecID
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 1=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
WHERE 
maintab.dripcntrycd=''
and mainoptn.divid is not null 
and mainoptn.divtype='S'
and not (divperiodcd='ISC' or marker='ISC')
and divopt2.divid is null
and divopt3.divid is null
and maintab.dripcntrycd=''
and ((maintab.isin in (select code from client.dbo.pfisin where accid=272 and actflag='I')
and exdate between getdate()-1 and getdate()+1000 and maintab.actflag<>'D')
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=272 and actflag='U')
and (changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
or exdate between getdate()-1 and getdate())))
and exchgid<>'' and exchgid is not null
and (maintab.exchgcd=maintab.primaryexchgcd or maintab.primaryexchgcd='')
order by caref2 desc, caref1, caref3
