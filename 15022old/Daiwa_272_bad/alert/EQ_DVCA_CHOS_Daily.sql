--SEMETABLE=client.dbo.seme_daiwa_272
--FileName=edi_YYYYMMDD
--DVCA_CHOS_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\Citibank\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DIV
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select distinct
Changed,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
maintab.frankdiv as FLFR,
maintab.unfrankdiv as UNFR,
case when maintab.StructCD<>'XREIT' then '' else maintab.StructCD end as structcd,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
maintab.Secid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVCA' as CAEV,
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:' + mainTFB1 as TFB,
/* case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end, */
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ') ELSE '' END,
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
/* CASE WHEN cfi <> '' THEN ':12C::CLAS//' +cfi ELSE '' END, */
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
'' as StartseqE,
'-}$'
From v_EQ_DV_CA_SE_OP as maintab
left outer join v10s_divpy as divopt1 on maintab.eventid = divopt1.divid 
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag
                   and 1=divopt1.optionid and 'C'<>divopt1.actflag
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid 
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid 
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
left outer join v10s_divpy as divopt4 on maintab.eventid = divopt4.divid 
                   and 4=divopt4.optionid and 'D'<>divopt4.actflag
                   and 4=divopt4.optionid and 'C'<>divopt4.actflag
left outer join v10s_divpy as divopt5 on maintab.eventid = divopt5.divid 
                   and 5=divopt5.optionid and 'D'<>divopt5.actflag
                   and 5=divopt5.optionid and 'C'<>divopt5.actflag
left outer join v10s_divpy as divopt6 on maintab.eventid = divopt6.divid 
                   and 6=divopt6.optionid and 'D'<>divopt6.actflag
                   and 6=divopt6.optionid and 'C'<>divopt6.actflag
left outer join v10s_divpy as divopt7 on maintab.eventid = divopt7.divid 
                   and 7=divopt7.optionid and 'D'<>divopt7.actflag
                   and 7=divopt7.optionid and 'C'<>divopt7.actflag
left outer join v10s_divpy as divopt8 on maintab.eventid = divopt8.divid 
                   and 8=divopt8.optionid and 'D'<>divopt8.actflag
                   and 8=divopt8.optionid and 'C'<>divopt8.actflag
WHERE
((maintab.isin in (select code from client.dbo.pfisin where accid=272 and actflag='I')
and exdate between getdate()-1 and getdate()+1000 and maintab.actflag<>'D')
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=272 and actflag='U')
and (changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
or exdate between getdate()-1 and getdate())))
and (divopt1.divtype = 'C' or divopt1.actflag is null)
and (divopt2.divtype = 'C' or divopt2.actflag is null)
and (divopt3.divtype = 'C' or divopt3.actflag is null)
and (divopt4.divtype = 'C' or divopt4.actflag is null)
and (divopt5.divtype = 'C' or divopt5.actflag is null)
and (divopt6.divtype = 'C' or divopt6.actflag is null)
and (divopt7.divtype = 'C' or divopt7.actflag is null)
and (divopt8.divtype = 'C' or divopt8.actflag is null)
and ((divopt1.divtype='C' and divopt2.divtype='C')
  or (divopt1.divtype='C' and divopt3.divtype='C')      
  or (divopt2.divtype='C' and divopt3.divtype='C'))
and maintab.NilDividend<>'T'
and maintab.dripcntrycd=''
and exchgid<>'' and exchgid is not null
and (maintab.exchgcd=maintab.primaryexchgcd or maintab.primaryexchgcd='')
order by caref2 desc, caref1, caref3
