--SEMETABLE=client.dbo.seme_daiwa_272
--FileName=edi_YYYYMMDD
--CAPG_RCAP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(rcapnotes as varchar(255)))>40 then RcapNotes else rtrim(char(32)) end As Notes FROM RCAP WHERE RCAPID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'103'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
maintab.Secid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CAPG' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:' + mainTFB1 as TFB,
/* case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end, */
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ') ELSE '' END,
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
/* CASE WHEN cfi <> '' THEN ':12C::CLAS//' +cfi ELSE '' END, */
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN effectivedate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN CSPYDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN CashBak<>'' AND CashBak IS NOT NULL
     THEN ':92F::GRSS//'+CurenCD+substring(CashBak,1,15)
     ELSE ':92K::GRSS//UKWN'
     END as Commasub,
':16S:CASHMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
from wca2.dbo.t_EQ_CAPG_RCAP as maintab
WHERE 
caprdid is null
and maintab.rocbasis='SOA'
and exchgid<>'' and exchgid is not null
and (maintab.exchgcd=maintab.primaryexchgcd or maintab.primaryexchgcd='')
and maintab.isin in (select code from client.dbo.pfisin where accid=272 and actflag<>'D')
order by caref2 desc, caref1, caref3
