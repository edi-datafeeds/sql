use wca
select
CASE WHEN EXCHGID is null
     THEN '31500001'
     ELSE '315'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_BIDS.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_BIDS.Actflag = 'C'
     THEN 'CANC'
     WHEN v_FI_BIDS.Actflag = 'I' 
     THEN 'NEWM'
     WHEN v_FI_BIDS.Actflag = 'U'
     THEN 'REPL'
     ELSE ''
     END AS Status,
'VOLU' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_BIDS.Startdate is not null
     THEN CONVERT ( varchar , v_FI_BIDS.StartDate,111)
     ELSE 'UKWN'
     END as EFFD,
CASE WHEN v_FI_BIDS.Enddate is not null
     THEN CONVERT ( varchar , v_FI_BIDS.EndDate,111)
     ELSE 'UKWN'
     END as MKDT,
CASE WHEN v_FI_BIDS.StartDate is not null
     THEN CONVERT ( varchar , v_FI_BIDS.StartDate,111)
     ELSE 'UKWN'
     END PWAL_from,
CASE WHEN v_FI_BIDS.EndDate is not null
     THEN CONVERT ( varchar , v_FI_BIDS.EndDate,111)
     ELSE 'UKWN'
     END PWAL_to,
CASE WHEN v_FI_BIDS.MaxQlyQty  is not null
     THEN substring(v_FI_BIDS.MaxQlyQty,1,15)
     END as QTSO,
'001' as CAON,
'CASH' as CAOP,
CASE WHEN v_FI_BIDS.CurenCD is not null
     THEN v_FI_BIDS.CurenCD
     ELSE 'UKWN'
     END as OPTN,
'No' as DFLT,
CASE WHEN v_FI_BIDS.Paydate is not null 
     THEN CONVERT ( varchar , v_FI_BIDS.paydate,111)
     ELSE 'UKWN'
     END as PAYD,
CASE WHEN v_FI_BIDS.MaxPrice  is not null
     THEN substring(v_FI_BIDS.MaxPrice,1,15)
     ELSE 'UKWN'
     END as OFFR,
(select CAST(BBNotes AS varchar(1000)) FROM BB WHERE BBID = v_FI_BIDS.EventID) as ChkNotes
From v_FI_BIDS
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
and Actflag is not null
order by caref2 desc, caref1, caref3
