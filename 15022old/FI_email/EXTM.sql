use wca
select 
CASE WHEN EXCHGID is null
     THEN '41500001'
     ELSE '415'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN Actflag = 'I'
     THEN 'NEWM'
     WHEN Actflag = 'U'
     THEN 'REPL'
     WHEN Actflag = 'D'
     THEN 'CANC'
     WHEN Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'MAND' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN NewMaturityDate is not null 
     THEN CONVERT ( varchar , NewMaturityDate,111)
     ELSE 'UKWN'
     END as EXTM,
'001' as CAON,
'CASH' as CAOP,
CASE WHEN PVCurrency  is not null
     THEN PVCurrency
     ELSE 'UKWN'
     END as OPTN,
(select CAST(Notes AS varchar(1000)) FROM MTCHG WHERE MtchgID = EventID) as ChkNotes
From v_FI_EXTM
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
order by caref2 desc, caref1, caref3
