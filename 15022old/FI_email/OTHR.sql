use wca
select 
CASE WHEN EXCHGID is null
     THEN '31200001'
     ELSE '312'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN Actflag = 'I'
     THEN 'NEWM'
     WHEN Actflag = 'U'
     THEN 'REPL'
     WHEN Actflag = 'D'
     THEN 'CANC'
     WHEN Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'VOLU' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN ExDate is not null
     THEN CONVERT ( varchar , ExDate,111)
     ELSE 'UKWN'
     END as XDTE,
CASE WHEN PayDate is not null
     THEN CONVERT ( varchar , PayDate,111)
     ELSE 'UKWN'
     END as PAYD,
CASE WHEN RecDate is not null
     THEN CONVERT ( varchar , RecDate,111)
     ELSE 'UKWN'
     END as RDTE,
(select CAST(ArrNotes AS varchar(1000)) FROM ARR WHERE RdID = EventID) as ChkNotes
From v_FI_OTHR_ARR
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
order by caref2 desc, caref1, caref3
