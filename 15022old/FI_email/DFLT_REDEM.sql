use wca
select 
CASE WHEN EXCHGID is null
     THEN '41000001'
     ELSE '410'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_REDM.Actflag = 'I'
     THEN 'NEWM'
     WHEN v_FI_REDM.Actflag = 'U'
     THEN 'REPL'
     WHEN v_FI_REDM.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_REDM.Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'MAND' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_REDM.RedemDate is not null AND v_FI_REDM.RedemDate IS NOT NULL
     THEN ':98A::REDM//'+CONVERT ( varchar , v_FI_REDM.RedemDate,112)
     ELSE ':98A::REDM//UKWN'
     END,
'001' as CAON,
'CASH' as CAOP,
CASE WHEN v_FI_REDM.CurenCD  is not null
     THEN v_FI_REDM.CurenCD
     ELSE 'UKWN'
     END as OPTN,
'Yes' as DFLT,
CASE WHEN v_FI_REDM.RedemPercent  is not null
     THEN CONVERT ( varchar , v_FI_REDM.RedemPercent,112)
     ELSE 'UKWN'
     END as PRCT,
CASE WHEN v_FI_REDM.RedemPrice is not null
     THEN CONVERT ( varchar , v_FI_REDM.RedemPrice,112)
     END as ACTU,
(select CAST(RedemNotes AS varchar(1000)) FROM REDEM WHERE RedemID = v_FI_REDM.EventID) as ChkNotes
From v_FI_REDM
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
and redemtype ='DEF'
order by caref2 desc, caref1, caref3
