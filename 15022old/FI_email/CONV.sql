use wca
select 
CASE WHEN EXCHGID is null
     THEN '32500001'
     ELSE '325'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_CONV.Actflag = 'I' 
     THEN 'NEWM'
     WHEN v_FI_CONV.Actflag = 'U'
     THEN 'REPL'
     WHEN v_FI_CONV.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_CONV.Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN 'MAND'
     ELSE 'VOLU'
     END as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_CONV.FromDate is not null
     THEN CONVERT ( varchar , v_FI_CONV.FromDate,111)
     ELSE 'UKWN'
     END as EFFD,
CASE WHEN v_FI_CONV.ToDate is not null
     THEN CONVERT ( varchar , v_FI_CONV.ToDate,111)
     ELSE 'UKWN'
     END as MKDT,
'001' as CAON,
'CONV' as CAOP,
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN 'Yes'
     ELSE 'No'
     END as DFLT,
CASE WHEN v_FI_CONV.FromDate is not null
     THEN CONVERT ( varchar , v_FI_CONV.FromDate,111)
     ELSE 'UKWN'
     END PWAL_from,
CASE WHEN v_FI_CONV.ToDate is not null
     THEN CONVERT ( varchar , v_FI_CONV.ToDate,111)
     ELSE 'UKWN'
     END PWAL_to,
CASE WHEN v_FI_CONV.Price  is not null AND v_FI_CONV.Price IS NOT NULL
     THEN ':90B::EXER//ACTU/'+v_FI_CONV.CurenCD+
          +substring(v_FI_CONV.Price,1,15)
     ELSE 'UKWN'
     END as EXER,
'CRED' as CRDB,
resTFB1 as Identifier,
CASE WHEN v_FI_CONV.Fractions = 'C'
     THEN 'CINL'
     WHEN v_FI_CONV.Fractions = 'U'
     THEN 'RDUP'
     WHEN v_FI_CONV.Fractions = 'D'
     THEN 'RDDN'
     WHEN v_FI_CONV.Fractions = 'B'
     THEN 'BUYU'
     WHEN v_FI_CONV.Fractions = 'T'
     THEN 'DIST'
     WHEN v_FI_CONV.Fractions = 'S'
     THEN 'STAN'
     ELSE '' 
     END as DISF,
CASE WHEN v_FI_CONV.RatioNew  is not null
     THEN v_FI_CONV.RatioNew
     ELSE 'UKWN'
     END as NEWO_new,
CASE WHEN v_FI_CONV.RatioOld  is not null
     THEN v_FI_CONV.RatioOld
     ELSE 'UKWN'
     END as NEWO_for_old,
'UKWN' as PAYD,
(select CAST(ConvNotes AS varchar(1000)) FROM CONV WHERE ConvID = v_FI_CONV.EventID) as ChkNotes
From v_FI_CONV
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
and Actflag  is not null
order by caref2 desc, caref1, caref3
