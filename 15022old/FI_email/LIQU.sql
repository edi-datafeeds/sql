use wca
select 
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + rtrim(cast(v_FI_LIQU.EventID as char(10))) + ' and sEvent = '+ char(39) + 'LIQ'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
CASE WHEN EXCHGID is null
     THEN '31300001'
     ELSE '313'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_LIQU.Actflag = 'I'
     THEN 'NEWM'
     WHEN v_FI_LIQU.Actflag = 'U'
     THEN 'REPL'
     WHEN v_FI_LIQU.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_LIQU.Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_LIQU.RdDate is not null AND v_FI_LIQU.RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_LIQU.RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
(select CAST(LiquidationTerms AS varchar(1000)) FROM LIQ WHERE LiqID = v_FI_LIQU.EventID) as ChkNotes
From v_FI_LIQU
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
order by caref2 desc, caref1, caref3
