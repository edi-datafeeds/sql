use wca
select 
CASE WHEN EXCHGID is null
     THEN '40000001'
     ELSE '400'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN RescindInterest = 'T'
     THEN 'CANC'
     WHEN Actflag = 'I'
     THEN 'NEWM'
     WHEN Actflag = 'U'
     THEN 'REPL'
     WHEN Actflag = 'D'
     THEN 'CANC'
     WHEN Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'MAND' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_INTR.ExDate is not null
     THEN CONVERT ( varchar , v_FI_INTR.Exdate,111)
     ELSE 'UKWN'
     END as XDTE,
CASE WHEN v_FI_INTR.RecDate is not null
     THEN CONVERT ( varchar , v_FI_INTR.recdate,111)
     ELSE 'UKWN'
     END as RDTE,
CASE WHEN InterestFromdate is not null
     THEN CONVERT ( varchar , InterestFromDate,111)
     ELSE 'UKWN'
     END as INPE_from,
CASE WHEN InterestTodate is not null
     THEN CONVERT ( varchar , InterestToDate,111)
     ELSE 'UKWN'
     END as INPE_to,
CASE WHEN AnlCoupRate is not null
     THEN AnlCoupRate
     ELSE 'UKWN'
     END as INTR,
CASE WHEN OptionID = 1
     THEN '001' 
     WHEN OptionID = 2
     THEN '002' 
     WHEN OptionID = 3
     THEN '003' 
     ELSE '001'
     END as CAON,
'CASH' as CAOP,
CASE WHEN CurenCD  is not null
     THEN CurenCD
     ELSE 'UKWN'
     END as OPTN,
CASE WHEN OptionID = 1
     THEN 'Yes' 
     ELSE 'No'
     END as DFLT,
CASE WHEN Paydate is not null
     THEN CONVERT ( varchar , PayDate,111)
     ELSE 'UKWN'
     END as PAYD,
CASE WHEN InterestPaymentFrequency = 'ANL'
           AND AnlCoupRate is not null
     THEN AnlCoupRate
     WHEN InterestPaymentFrequency = 'SMA'
           AND AnlCoupRate is not null
     THEN cast(cast(AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN InterestPaymentFrequency = 'QTR'
           AND AnlCoupRate is not null
     THEN cast(cast(AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN InterestPaymentFrequency = 'MNT'
           AND AnlCoupRate is not null
     THEN cast(cast(AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE 'UKWN'
     END as INTP,
(select CAST(IntNotes AS varchar(1000)) FROM INT WHERE RdID = EventID) as ChkNotes
From v_FI_INTR
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
and interestdefault is not null
and interestdefault = 'FD'
and inttype='C'
order by caref2 desc, caref1, caref3