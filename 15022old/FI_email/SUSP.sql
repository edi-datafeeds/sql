use wca
select 
v_FI_ACTV.Changed,
CASE WHEN EXCHGID is null
     THEN '35200001'
     ELSE '352'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_ACTV.Actflag = 'I'
     THEN 'NEWM'
     WHEN v_FI_ACTV.Actflag = 'U'
     THEN 'REPL'
     WHEN v_FI_ACTV.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_ACTV.Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'MAND' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN NotificationDate is not null
     THEN CONVERT ( varchar , NotificationDate,111)
     ELSE 'UKWN'
     END as ANOU,
CASE WHEN EffectiveDate is not null
     THEN CONVERT ( varchar , EffectiveDate,111)
     ELSE 'UKWN'
     END as TSDT,
(select CAST(Reason AS varchar(1000)) FROM LSTAT WHERE LSTATID = v_FI_ACTV.EventID) as ChkNotes
From v_FI_ACTV
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
and LStatStatus = 'S'
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
