use wca
select 
CASE WHEN EXCHGID is null
     THEN '35300001'
     ELSE '353'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_CHAN_NAME.Actflag = 'I'
     THEN 'NEWM'
     WHEN v_FI_CHAN_NAME.Actflag = 'U'
     THEN 'REPL'
     WHEN v_FI_CHAN_NAME.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_CHAN_NAME.Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'MAND' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_CHAN_NAME.NameChangedate is not null
     THEN CONVERT ( varchar , v_FI_CHAN_NAME.NameChangeDate,111)
     ELSE 'UKWN'
     END as EFFD,
'NAME' as CHAN,
v_FI_CHAN_NAME.IssOldname as NAME_old,
v_FI_CHAN_NAME.IssNewname as NAME_new,
(select CAST(IschgNotes AS varchar(1000)) FROM ISCHG WHERE ISCHGID = v_FI_CHAN_NAME.EventID) as ChkNotes
From v_FI_CHAN_NAME
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
