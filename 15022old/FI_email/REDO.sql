use wca
select 
CASE WHEN EXCHGID is null
     THEN '32500001'
     ELSE '325'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_REDO.Actflag = 'I' 
     THEN 'NEWM'
     WHEN v_FI_REDO.Actflag = 'U'
     THEN 'REPL'
     WHEN v_FI_REDO.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_REDO.Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'MAND' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_REDO.EffectiveDate is not null
     THEN CONVERT ( varchar , v_FI_REDO.EffectiveDate,111)
     ELSE 'UKWN'
     END as EFFD,
'001' as CAON,
'SECU' as CAOP,
'Yes' as DFLT,
'CRED' as CRDB,
v_FI_REDO.NewCurenCD as DENO,
CASE WHEN v_FI_REDO.NewCurenCD=v_FI_REDO.Current_Currency
    THEN v_FI_REDO.MinimumDenomination
    ELSE ''
    END as MINO,
CASE WHEN v_FI_REDO.newparvalue is not null
     THEN v_FI_REDO.newparvalue
     ELSE 'UKWN'
     END as NEWO_new,
CASE WHEN v_FI_REDO.oldparvalue  is not null
     THEN v_FI_REDO.oldparvalue
     ELSE 'UKWN'
     END as NEWO_old,
(select CAST(CurrdNotes AS varchar(1000)) FROM CURRD WHERE CurrdID = v_FI_REDO.EventID) as ChkNotes
From v_FI_REDO
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
and NewCurenCD is not null
and Actflag  is not null
order by caref2 desc, caref1, caref3
