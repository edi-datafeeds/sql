use wca
select 
CASE WHEN EXCHGID is null
     THEN '31900001'
     ELSE '319'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
CASE WHEN v_FI_BRUP.Actflag = 'I'
     THEN 'NEWM'
     WHEN v_FI_BRUP.Actflag = 'U'
     THEN 'REPL'
     WHEN v_FI_BRUP.Actflag = 'D'
     THEN 'CANC'
     WHEN v_FI_BRUP.Actflag = 'C'
     THEN 'CANC'
     ELSE ''
     END AS Status,
'MAND' as CAMV,
CONVERT ( varchar , changed,111) +' '+ convert ( varchar, changed, 08) as PREP,
'COMP' as [PROC], 
ISIN,
UScode,
SEDOL,
substring(Issuername,1,35) as Issuer,
substring(Securitydesc,1,35) as [Description],
CASE WHEN localcode <> ''
     THEN localcode
     ELSE ''
     END as TS,
MCD as PLIS,
CASE WHEN v_FI_BRUP.NotificationDate is not null
     THEN CONVERT ( varchar , v_FI_BRUP.NotificationDate,111)
     ELSE 'UKWN'
     END as ANOU,
CASE WHEN v_FI_BRUP.FilingDate is not null
     THEN CONVERT ( varchar , v_FI_BRUP.FilingDate,111)
     ELSE 'UKWN'
     END as EFFD,
(select CAST(BkrpNotes AS varchar(1000)) FROM BKRP WHERE BKRPID = v_FI_BRUP.EventID) as ChkNotes
From v_FI_BRUP
WHERE
changed between (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3 and feeddate = (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
and (isin in (select code from portfolio.dbo.pf)
or uscode in (select code from portfolio.dbo.pf)
or sedol in (select code from portfolio.dbo.pf))
order by caref2 desc, caref1, caref3
