--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--DECR_CAPRD_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(caprdnotes as varchar(255)))>40 then caprdnotes else rtrim(char(32)) end As Notes FROM CAPRD WHERE CAPRDID = '+ cast(EventID as char(16)) as ChkNotes, 
'{1:F01NUSEGB21XXXO0300000054}{2:I564EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.effectivedate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.effectivedate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '148' END as CAref1, */
'148' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN effectivedate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT/'+NewParValue as commasub,
':16S:CADETL',
CASE WHEN rcapid is not null THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN rcapid is not null THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22F::CAOP//CASH' ELSE '' END,
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
CASE WHEN rcapid is not null THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16R:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN CSPYDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     WHEN rcapid is not null THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN CashBak<>'' AND CashBak IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+CurenCD+substring(CashBak,1,15)
     WHEN rcapid is not null THEN ':90E::OFFR//UKWN'
     ELSE ''
     END as Commasub,
CASE WHEN rcapid is not null THEN ':16S:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16S:CAOPTN' ELSE '' END,
'-}$'
From v_EQ_DECR_CAPRD as maintab
/* left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'DECR'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=997 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=997 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=997 and actflag='I'))
and effectivedate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=997 and actflag<>'X')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=997 and actflag<>'X')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=997 and actflag<>'X'))
and changed>(select max(acttime)-730 from wca.dbo.tbl_opslog)))
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
and rtrim(OldParValue)<>'' and rtrim(NewParValue)<>''
and rtrim(OldParValue)<>'0' and rtrim(NewParValue)<>'0'
and cast(rtrim(OldParValue) as float(16,7))>cast(rtrim(NewParValue) as float(16,7))
/* and effectivedate>getdate()-8 and effectivedate is not null */
order by caref2 desc, caref1, caref3
