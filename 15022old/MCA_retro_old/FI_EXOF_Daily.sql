--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_EXOF
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct top 50
v_FI_CONV.Changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then rtrim(char(32)) else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(v_FI_CONV.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '325'+EXCHGID+cast(Seqnum as char(1))
     ELSE '325'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_CONV.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN v_FI_CONV.InterestRate <> '' THEN ':92A::INTR//'+v_FI_CONV.InterestRate ELSE ':92K::INTR//UKWN' END as COMMASUB,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
/*':70E::OFFO//'+substring(Issuername,1,35) as TIDYTEXT,*/
':16S:CADETL',
':16R:CAOPTN',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
       AND v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     WHEN v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/UKWN'
     WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_FI_CONV.Price  <>'' AND v_FI_CONV.Price <>''
     THEN ':90B::CINL//ACTU/'+v_FI_CONV.CurenCD+
          +substring(v_FI_CONV.Price,1,15)
     ELSE ':90E::CINL//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_CONV.resTFB1 <>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
CASE WHEN v_FI_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_FI_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_FI_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_FI_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_FI_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_FI_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v_FI_CONV.RatioNew <>''
          AND v_FI_CONV.RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_FI_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(v_FI_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN v_FI_CONV.SettlementDate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , v_FI_CONV.SettlementDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':13A::CAON//003',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN Todate <>''
     THEN ':98A::EXPI//' + CONVERT(varchar , Todate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN Fromdate<>'' AND Todate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Fromdate,112)+'/'
          +CONVERT ( varchar , Todate,112)
     WHEN Fromdate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Fromdate,112)+'/UKWN'
     WHEN Todate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Todate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,  
'-}$'
From v_FI_CONV
WHERE
mainTFB1<>''
/* and substring(primaryexchgcd,1,2)<>'US' */
and (eventid=76314 or eventid=76315)
and convtype='BB'
and Actflag  <>''
order by caref2 desc, caref1, caref3
