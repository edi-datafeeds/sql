--SEMETABLE=client.dbo.seme_mwace
--FileName=edi_YYYYMMDD
--RHDI_RENO_RTS_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=

--# 1
use wca
select distinct
maintab.Changed,
'select case when len(cast(RTSNotes as varchar(255)))>40 then RTSNotes else rtrim(char(32)) end As Notes FROM RTS WHERE RDID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01EDIXXXXXXISO0300000054}{2:O5641825151030EDIXXXXXXISO03000000541510301825N}{4:' as ISOHDR,
':16R:GENL',
'117'+EXCHGID+cast(maintab.Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHDI' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN' THEN '/GB/' + Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':22F::SELL//RENO',
':22F::RHDI//EXRI',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
':17B::DFLT//Y',
CASE WHEN EndSubscription<>''
     THEN ':98A::EXPI//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN traTFB1<>'' and traTFB1<>'UKWN'
     THEN ':35B:' + traTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN EndTrade<>'' 
       AND StartTrade<>'' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/'
          +CONVERT ( varchar , EndTrade,112)
     WHEN StartTrade<>'' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/UKWN'
     WHEN EndTrade<>'' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
CASE WHEN RatioNew <>'' AND RatioOld <>''
     THEN ':92D::ADEX//'+ltrim(cast(RatioNew as char (15)))+
                    '/'+ltrim(cast(RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN Paydate<>'' AND Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar, Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_EQ_RHDI_RTS as maintab
left outer join serialseq on maintab.eventid = serialseq.rdid and 2=serialseq.seqnum and 'RTS'=serialseq.eventcd
WHERE 
mainTFB1<>''
and changed>'2017/03/08' and changed<'2017/03/09'
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and serialseq.uniqueid is null
and ppsecid is null
and ((lapsedpremium='' or lapsedpremium='0' or lapsedpremium is null) 
    or (rationew<>'' and rationew<>'0' and rationew is not null))
and CalcListdate<=announcedate
and CalcDelistdate>=announcedate
order by caref2 desc, caref1, caref3
