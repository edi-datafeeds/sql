--SEMETABLE=client.dbo.seme_citiuat
--FileName=edi_YYYYMMDD
--DVCA_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=
--archive=off
--ArchivePath=n:\15022\Citibank\
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end as Changed,
CASE WHEN maintab.cntrycd='MY' THEN
          'select case when len(cast(DivNotes as varchar(255)))>40 then DivNotes else rtrim(char(32)) end As Notes FROM DIV WHERE DIVID = '+ cast(EventID as char(16))
     ELSE 'select case when 1=2 then rtrim(char(32)) end as notes'
     END as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN (maintab.exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.exdate is not null
       OR maintab.recdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.recdate is not null)
              and (SR.CAMV<>'MAND' or SR.TFB<>':35B:/GB/'+maintab.Sedol)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '100' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C' or mainoptn.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D' or mainoptn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVCA' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,112) + replace(convert ( varchar, case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE ''
     END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     WHEN maintab.Actflag = 'D'
     THEN ':98A::XDTE//20990101'
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN mainoptn.CurenCD<>'' AND mainoptn.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + mainoptn.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE 
     WHEN mainoptn.approxflag='T'
     THEN ''
     WHEN mainoptn.Netdividend<>'' AND mainoptn.Netdividend IS NOT NULL
     THEN ':92F::NETT//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Netdividend as char(15)))
     WHEN mainoptn.Grossdividend<>'' AND  mainoptn.Grossdividend IS NOT NULL
     THEN ''
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE 
     WHEN mainoptn.approxflag='T' and mainoptn.Grossdividend <> '' AND  mainoptn.Grossdividend IS NOT NULL
     THEN ':92F::PDIV//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))
     WHEN mainoptn.approxflag='T' and mainoptn.Netdividend <> '' AND  mainoptn.Netdividend IS NOT NULL
     THEN ':92F::PDIV//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Netdividend as char(15)))
     WHEN mainoptn.Grossdividend<>'' AND  mainoptn.Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))
     WHEN maintab.frankdiv='' and maintab.unfrankdiv=''
     THEN ':92K::GRSS//UKWN' 
     ELSE '' 
     END as COMMASUB,
CASE WHEN maintab.frankdiv<>''
     THEN ':92J::GRSS//FLFR/'+ mainoptn.CurenCD+ltrim(cast(maintab.frankdiv as char(15))) +'/ACTU'
     ELSE '' 
     END as COMMASUB,
CASE WHEN maintab.unfrankdiv<>''
     THEN ':92J::GRSS//UNFR/'+ mainoptn.CurenCD+ltrim(cast(maintab.unfrankdiv as char(15))) +'/ACTU'
     ELSE '' 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
/*'' as Notes,*/
'-}$'
from wca2.dbo.i_EQ_DV_CA_SE_OP as maintab
left outer join client.dbo.seme_citiuat as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'DVCA'=SR.CAEVMAIN
left outer join v10s_divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
WHERE
maintab.sedol<>'' and maintab.sedol is not null
and maintab.announcedate>DATEADD(DAY,30,maintab.defunctacttime)
and maintab.defunctacttime is not null
and maintab.dripcntrycd=''
and not (divperiodcd='ISC' or marker='ISC')
and not (maintab.cntryofincorp='JP' and maintab.cntrycd='JP')
and mainoptn.divid is not null 
and (mainoptn.divtype='C' or mainoptn.divtype='B')
and divopt2.divid is null 
and divopt3.divid is null
order by caref2 desc, caref1, caref3
