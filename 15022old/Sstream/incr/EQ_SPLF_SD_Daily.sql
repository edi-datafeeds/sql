--SEMETABLE=client.dbo.seme_sstream
--FileName=edi_YYYYMMDD
--SPLF_SD_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  SDNotes As Notes FROM SD WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'120'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLF',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::EFFD//UKWN'
     END as DateEx,
CASE WHEN Recdate <>'' AND Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newTFB1<>''
     THEN ':35B:' + newTFB1
     ELSE ':35B:' + mainTFB1
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew <>''
          AND RatioOld <>''
     THEN ':92D::NEWO//'+ltrim(cast(RatioNew as char (15)))+
                    '/'+ltrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN Paydate <>'' AND Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END as DatePay,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_EQ_SPLF
WHERE 
mainTFB1<>''
and changed between '2014/11/11 04:00:00' and '2014/11/11 11:45:00'
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3