--SEMETABLE=client.dbo.seme_otkritie_i
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_INTR
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\otkritie\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct 
wca2.dbo.i_FI_INTR.Changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then rtrim(char(32)) else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(wca2.dbo.i_FI_INTR.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '400'+EXCHGID
     ELSE '400'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN wca2.dbo.i_FI_INTR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN wca2.dbo.i_FI_INTR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN wca2.dbo.i_FI_INTR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN wca2.dbo.i_FI_INTR.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INTR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' and localcode is not null
     THEN '/TS/' + replace(Localcode,'%','PCT')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
CASE WHEN debtcurrency<>'' THEN ':11A::DENO//' + debtcurrency END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN wca2.dbo.i_FI_INTR.ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , wca2.dbo.i_FI_INTR.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN wca2.dbo.i_FI_INTR.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , wca2.dbo.i_FI_INTR.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN wca2.dbo.i_FI_INTR.InterestFromdate <>''
       AND wca2.dbo.i_FI_INTR.InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , wca2.dbo.i_FI_INTR.InterestToDate,112)+'/'
          +CONVERT ( varchar , wca2.dbo.i_FI_INTR.InterestFromDate,112)
     WHEN wca2.dbo.i_FI_INTR.InterestTodate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , wca2.dbo.i_FI_INTR.InterestToDate,112)+'/UKWN'
     WHEN wca2.dbo.i_FI_INTR.InterestFromdate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , wca2.dbo.i_FI_INTR.InterestFromDate,112)
     ELSE ':69J::INPE//UKWN/UKWN'
     END,
CASE WHEN wca2.dbo.i_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTR//' + wca2.dbo.i_FI_INTR.AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN wca2.dbo.i_FI_INTR.Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , wca2.dbo.i_FI_INTR.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN wca2.dbo.i_FI_INTR.InterestPaymentFrequency = 'ANL'
           AND wca2.dbo.i_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + wca2.dbo.i_FI_INTR.AnlCoupRate
     WHEN wca2.dbo.i_FI_INTR.InterestPaymentFrequency = 'SMA'
           AND wca2.dbo.i_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(wca2.dbo.i_FI_INTR.AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN wca2.dbo.i_FI_INTR.InterestPaymentFrequency = 'QTR'
           AND wca2.dbo.i_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(wca2.dbo.i_FI_INTR.AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN wca2.dbo.i_FI_INTR.InterestPaymentFrequency = 'MNT'
           AND wca2.dbo.i_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(wca2.dbo.i_FI_INTR.AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,  
'-}$'
From wca2.dbo.i_FI_INTR
WHERE
mainTFB1<>''and (indefpay='' or indefpay = 'P')
and isin in (select code from client.dbo.pfisin where accid=981)
order by caref2 desc, caref1, caref3

