--SEMETABLE=client.dbo.seme_otkritie_i
--FileName=edi_YYYYMMDD
--REDM_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\otkritie\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1

use wca
select distinct
Changed,
'select RedemNotes As Notes FROM REDEM WHERE RedemID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'410'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDM',
case when mandoptflag = 'M' then ':22F::CAMV//MAND' else ':22F::CAMV//VOLU' end,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(Localcode,'%','PCT')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN RedemPercent <> ''
     THEN ':90B::REDM//PRCT/' + CONVERT ( varchar , RedemPercent,112)
     WHEN RedemPrice is not null
     THEN ':90B::REDM//ACTU/' + CurenCD + CONVERT ( varchar , RedemPrice,112)
     ELSE ':90F::REDM//UKWN'
     END as COMMASUB,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN RedemDate <>'' AND RedemDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN RedemPercent <>''
     THEN ':90A::OFFR//PRCT/'+RedemPercent
     ELSE ':90A::OFFR//PRCT/100,'
     END as commasub,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,  
'-}$'
From v_EQ_REDM
WHERE
mainTFB1<>''
and changed>getdate()-92
and exchgid<>'' and exchgid is not null
and (exchgcd=primaryexchgcd or primaryexchgcd='')
and redemdate <>''
and redemdate is not null
and redemdate <> '1800/01/01'
and mandoptflag<>''
and isin in (select code from client.dbo.pfisin where accid=981)
order by caref2 desc, caref1, caref3

