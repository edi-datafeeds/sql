--SEMETABLE=client.dbo.seme_otkritie_i
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_BRUP
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\otkritie\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct 
v_FI_BRUP.Changed,
'select case when len(cast(BkrpNotes as varchar(24)))=22 then rtrim(char(32)) else BkrpNotes end As Notes FROM BKRP WHERE BKRPID = '+ cast(v_FI_BRUP.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '319'+EXCHGID
     ELSE '319'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_BRUP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_BRUP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_BRUP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_BRUP.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BRUP' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(Localcode,'%','PCT')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
CASE WHEN debtcurrency<>'' THEN ':11A::DENO//' + debtcurrency END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_BRUP.NotificationDate is not null and v_FI_BRUP.NotificationDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , v_FI_BRUP.NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN v_FI_BRUP.FilingDate is not null and v_FI_BRUP.FilingDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_BRUP.FilingDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes, 
'-}$'
From v_FI_BRUP
WHERE
mainTFB1<>''
and changed>getdate()-92
and isin in (select code from client.dbo.pfisin where accid=981)
order by caref2 desc, caref1, caref3
