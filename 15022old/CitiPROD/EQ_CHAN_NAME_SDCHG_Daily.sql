--SEMETABLE=client.dbo.seme_citiprod
--FileName=edi_YYYYMMDD
--CHAN_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end as Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN maintab.NameChangeDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.NameChangeDate is not null
              and SR.TFB<>':35B:/GB/'+maintab.Sedol
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '153' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,112) + replace(convert ( varchar, case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(IssOldname,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE '' END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN NameChangeDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//NAME',
':70E::NAME//' + substring(replace(IssNewname,'&','and'),1,35) as TIDYTEXT,
':16S:CADETL',
CASE WHEN newsedol <>'' THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':22F::CAOP//SECU' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':16R:SECMOVE' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':35B:/GB/' + newsedol ELSE '' END,
CASE WHEN newsedol <>'' THEN ':16R:FIA' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':94B::PLIS//SECM' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':16S:FIA' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':92D::NEWO//1,/1,' ELSE '' END,
CASE WHEN newsedol <>'' and sdchgeffectivedate is not null 
     THEN ':98A::PAYD//'+CONVERT ( varchar , sdchgeffectivedate,112)
     WHEN newsedol <>'' and sdchgeffectivedate is null 
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN newsedol <>'' THEN ':16S:SECMOVE' ELSE '' END,
CASE WHEN newsedol <>'' THEN ':16S:CAOPTN' ELSE '' END,
'-}$'
From wca2.dbo.i_EQ_CHAN_NAME as maintab
left outer join client.dbo.seme_citiprod as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CHAN'=SR.CAEVMAIN
WHERE
maintab.sedol<>'' and maintab.sedol is not null
/*and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)*/
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
