--SEMETABLE=client.dbo.seme_nomura_i
--FileName=edi_YYYYMMDD
--BIDS_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_nomura_i\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=BB
--TFBNUMBER=1
--INCREMENTAL=ON

--# 1
use wca
select distinct
'select distinct V10s_MPAY.* from V10s_MPAY'
+ ' where (v10s_MPAY.Actflag='+char(39)+'I'+char(39)+ ' or v10S_MPAY.Actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'BB'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
changed,
null as PAYLINK,
Startdate as OPENLINK,
Enddate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:NOMAGB2LAXXX0000000000}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'F01EDIXGB2LXISO}{4:' as ISOHDR,
':16R:GENL',
'115' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' THEN '/TS/' + Localcode ELSE '' END,
':16R:FIA',
CASE WHEN MCD <>'' THEN ':94B::PLIS//EXCH/' + MCD END,
/* CASE WHEN pvcurrency <>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN StartDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN MaxQlyQty<>''
     THEN ':36B::QTSO//UNIT/'+substring(MaxQlyQty,1,15) + ','
     ELSE '' END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'-}$'
From wca2.dbo.i_EQ_BIDS
WHERE 
MAINTFB1<>''
and (mcd<>'' and mcd is not null)
and (primaryexchgcd=exchgcd or primaryexchgcd='')
and Enddate>getdate()-8 and Enddate is not null
and CalcListdate<=announcedate
and CalcDelistdate>=announcedate
and Actflag <> ''
order by caref2 desc, caref1, caref3
