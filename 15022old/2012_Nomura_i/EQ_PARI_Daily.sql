--SEMETABLE=client.dbo.seme_nomura_i
--FileName=edi_YYYYMMDD
--PARI_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_nomura_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON

--# 1
use wca
select distinct
Changed,
'{1:NOMAGB2LAXXX0000000000}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'F01EDIXGB2LXISO}{4:' as ISOHDR,
':16R:GENL',
'124' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PARI',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' THEN '/TS/' + Localcode ELSE '' END,
':16R:FIA',
CASE WHEN MCD <>'' THEN ':94B::PLIS//EXCH/' + MCD END,
/* CASE WHEN pvcurrency <>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN AssimilationDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , AssimilationDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>'UKWN' and resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB2
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
/* CASE WHEN RatioNew <>''
            AND RatioNew IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(RatioNew as char (15)))+
                    '/'+rtrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB, 
next line is a placebo, needs a ratioold,rationew added to parent */
':92K::NEWO//UKWN',
CASE WHEN AssimilationDate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , AssimilationDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From wca2.dbo.i_EQ_PARI
WHERE 
MAINTFB1<>''
and (mcd<>'' and mcd is not null)
and (primaryexchgcd=exchgcd or primaryexchgcd='')
and AssimilationDate>getdate()-8 and AssimilationDate is not null
and CalcListdate<=announcedate
and CalcDelistdate>=announcedate
order by caref2 desc, caref1, caref3
