--SEMETABLE=client.dbo.seme_RAT
--FileName=edi_YYYYMMDD
--CHAN_LTCHG_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\RAT\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Effectivedate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Effectivedate is not null
              and SR.TFB<>':35B:/GB/'+maintab.Sedol
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '199' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE '' END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
/* CASE WHEN pvcurrency <>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN EffectiveDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//TERM',
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//RE/TRADABLE UNIT CHANGE',
'OLD 1UNIT/'+cast(maintab.oldlot as varchar(20))+'SHS',
'NEW 1UNIT/'+cast(maintab.newlot as varchar(20))+'SHS',
':16S:ADDINFO',
'-}$'
From v_EQ_CHAN_LTCHG as maintab
left outer join client.dbo.seme_rat as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CHAN'=SR.CAEVMAIN
WHERE
maintab.sedol<>'' and maintab.sedol is not null
and (maintab.opol=maintab.MCD 
     or (maintab.opol='SHSC' and maintab.MCD='XHGK')
     or (maintab.opol='XSSC' and maintab.MCD='XSHG')
     or (maintab.opol='BMEX' and maintab.MCD='XMAD'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and maintab.oldlot is not null
and maintab.newlot is not null
and maintab.eventtype<>'CLEAN'
and maintab.eventtype<>'CORR'
and maintab.oldlot<>maintab.newlot
order by caref2 desc, caref1, caref3
