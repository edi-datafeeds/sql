not used because renderer doesn't exclude when mpay is missing
--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--ODLT_PREC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\smartstream\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
Changed,
'select  Notes FROM ODDLT WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'163'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.eventid as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ODLT' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.announcedate,112) ELSE '' END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
':11A::OPTN//USD',
':17B::DFLT//N',
CASE WHEN Enddate<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , Enddate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN Enddate<>'' AND Startdate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Startdate,112)+'/'
          +CONVERT ( varchar , Enddate,112)
     WHEN Startdate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Startdate,112)+'/UKWN'
     WHEN Enddate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Enddate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN MaxAcpQty <>''
     THEN ':36B::FOLQ//UNIT/'+MaxAcpQty
     ELSE ':36C::FOLQ//UKWN'
     END as COMMASUB,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.PayDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , maintab.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':90E::OFFR//UKWN',
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_EQ_ODLT as maintab
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'ODDLT' = wca.dbo.mpay.sevent
                                       and 'D'<>wca.dbo.mpay.actflag
WHERE
mainTFB1<>'' and maintab.actflag<>'D'
and exchgid<>'' and exchgid is not null
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
/* and cntrycd='AU' */
and maintab.announcedate>'2014/10/31'
and wca.dbo.mpay.eventid is null
and maintab.opol=maintab.MCD
order by caref2 desc, caref1, caref3
