--SEMETABLE=client.dbo.seme_smartstream
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_EXTM
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
v_FI_EXTM.Changed,
'select case when len(cast(Notes as varchar(24)))=22 then rtrim(char(32)) else Notes end As Notes FROM MTCHG WHERE MtchgID = '+ cast(v_FI_EXTM.EventID as char(16)) as ChkNotes,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '415'+EXCHGID+cast(Seqnum as char(1))
     ELSE '415'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_EXTM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_EXTM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_EXTM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_EXTM.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXTM'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_EXTM.NewMaturityDate is not null and v_FI_EXTM.NewMaturityDate <>''
     THEN ':98A::MATU//'+CONVERT ( varchar , v_FI_EXTM.NewMaturityDate,112)
     ELSE ':98A::MATU//UKWN'
     END,
':16S:CADETL',
'' as Notes,  
'-}$'
From v_FI_EXTM
WHERE
	mainTFB1<>''
order by caref2 desc, caref1, caref3
