--SEMETABLE=client.dbo.seme_smartstream
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_INTR
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
v_FI_INTR.Changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then rtrim(char(32)) else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(v_FI_INTR.EventID as char(16)) as ChkNotes,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '400'+EXCHGID+cast(Seqnum as char(1))
     ELSE '400'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_INTR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_INTR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_INTR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_INTR.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INTR'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' and localcode is not null
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_INTR.ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_FI_INTR.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v_FI_INTR.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_FI_INTR.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v_FI_INTR.InterestFromdate <>''
       AND v_FI_INTR.InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , v_FI_INTR.InterestFromDate,112)+'/'
          +CONVERT ( varchar , v_FI_INTR.InterestToDate,112)
     WHEN v_FI_INTR.InterestTodate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , v_FI_INTR.InterestFromDate,112)+'/UKWN'
     WHEN v_FI_INTR.InterestFromdate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , v_FI_INTR.InterestToDate,112)
     ELSE ':69J::INPE//UKWN'
     END,
CASE WHEN v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTR//' + v_FI_INTR.AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_INTR.Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , v_FI_INTR.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN v_FI_INTR.InterestPaymentFrequency = 'ANL'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + v_FI_INTR.AnlCoupRate
     WHEN v_FI_INTR.InterestPaymentFrequency = 'SMA'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'QTR'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'MNT'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,  
'-}$'
From v_FI_INTR
WHERE
	mainTFB1<>''
and (indefpay='' or indefpay = 'P')
order by caref2 desc, caref1, caref3
