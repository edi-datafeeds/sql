--SEMETABLE=client.dbo.seme_smartstream
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_CLSA
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
v_FI_CLSA.Changed,
'select case when len(cast(LawstNotes as varchar(24)))=22 then rtrim(char(32)) else LawstNotes end As Notes FROM LAWST WHERE LAWSTID = '+ cast(v_FI_CLSA.EventID as char(16)) as ChkNotes,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '349'+EXCHGID+cast(Seqnum as char(1))
     ELSE '349'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_CLSA.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_CLSA.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CLSA.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CLSA.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CLSA'as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_CLSA.Effectivedate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_CLSA.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes, 
'-}$'
From v_FI_CLSA
WHERE
	mainTFB1<>''
order by caref2 desc, caref1, caref3
