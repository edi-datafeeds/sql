--SEMETABLE=client.dbo.seme_nasdaq
--FileName=BRUP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select top 20
Changed,
'select  BkrpNotes As Notes FROM BKRP WHERE BKRPID = '+ cast(EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'119'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BRUP',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN NotificationDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN FilingDate <>''
     THEN ':98A::FILL//'+CONVERT ( varchar , FilingDate,112)
     ELSE ':98B::FILL//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From v_EQ_BRUP
WHERE 
sedol<>''
and (mcd<>'' and mcd is not null)
-- /*
     and (sedol in (select code from client.dbo.pfsedol where accid = 232 and actflag='U')
          or isin in (select code from client.dbo.pfisin where accid = 232 and actflag='U')
          or uscode in (select code from client.dbo.pfuscode where accid = 232 and actflag='U'))
     OR 
    (NotificationDate>getdate()-183
     and (sedol in (select code from client.dbo.pfsedol where accid = 232 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 232 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 232 and actflag='I')))
-- */
order by caref2 desc, caref1, caref3