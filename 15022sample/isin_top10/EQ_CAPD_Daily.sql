--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD_ISIN_EQ
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2


--# 1
use wca
select distinct  
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'102'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN  maintab.Actflag = 'D' or mainoptn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'C' or mainoptn.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEP//DISN',
':22F::CAEV//CAPD' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>'' and MCD is not null
     THEN ':94B::PLIS//EXCH/' + MCD ELSE ':94B::PLIS//SECM'
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN mainoptn.CurenCD <> '' AND mainoptn.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + mainoptn.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  Paydate <>'' AND  Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN  mainoptn.Netdividend <> '' AND mainoptn.Netdividend IS NOT NULL
     THEN ':92F::NETT//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Netdividend as char(15)))
     WHEN  mainoptn.Grossdividend <> '' AND  mainoptn.Grossdividend IS NOT NULL
     THEN ''
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN  mainoptn.Grossdividend <> ''
             AND  mainoptn.Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_DV_CA_SE_OP as maintab
left outer join divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid
left outer join divpy as divopt2 on maintab.eventid = divopt2.divid and 2=divopt2.optionid and 'C'<>divopt2.actflag and 'D'<>divopt2.actflag
left outer join divpy as divopt3 on maintab.eventid = divopt3.divid and 3=divopt3.optionid and 'C'<>divopt3.actflag and 'D'<>divopt3.actflag
left outer join wca.dbo.drip on maintab.eventid=wca.dbo.drip.divid
WHERE
mainTFB1<>''
and changed>'2011/01/01'
and primaryexchgcd=exchgcd
and wca.dbo.drip.divid is null
and mainoptn.divid is not null 
and divopt2.divid is null 
and divopt3.divid is null
and (mainoptn.divtype='C' or mainoptn.divtype='B')
and divperiodcd='ISC'
and (paydate>getdate()-183 or paydate is null)
order by caref2 desc, caref1, caref3
