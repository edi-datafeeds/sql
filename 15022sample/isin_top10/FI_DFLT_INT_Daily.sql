--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD_ISIN_FI
-- YYYYMMDD_FI564_DFLt_INT
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct top 10
Changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then null else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null 
     THEN '401'+EXCHGID+cast(Seqnum as char(1)) 
     ELSE '401' END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DFLT' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' and localcode is not null
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>'' and MCD is not null
     THEN ':94B::PLIS//EXCH/' + MCD ELSE ':94B::PLIS//SECM'
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN InterestFromdate <>''
       AND InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , InterestToDate,112)+'/'
          +CONVERT ( varchar , InterestFromDate,112)
     WHEN InterestTodate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , InterestToDate,112)+'/UKWN'
     WHEN InterestFromdate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , InterestFromDate,112)
     ELSE ':69J::INPE//UKWN/UKWN'
     END,
CASE WHEN AnlCoupRate  <>''
     THEN ':92A::INTR//' + AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD  <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN InterestPaymentFrequency = 'ANL'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + AnlCoupRate
     WHEN InterestPaymentFrequency = 'SMA'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN InterestPaymentFrequency = 'QTR'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN InterestPaymentFrequency = 'MNT'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_INTR
WHERE
mainTFB1<>''
and changed>'2011/01/01'
and primaryexchgcd=exchgcd
and indefpay <>''
and indefpay = 'F'
order by caref2 desc, caref1, caref3
