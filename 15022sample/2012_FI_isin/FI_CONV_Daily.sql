--SEMETABLE=client.dbo.seme_fi_isin
--FileName=edi_YYYYMMDD_mt564
-- YYYYMMDD_FI564_CONV
--FileNameNotes=edi_YYYYMMDD_mt568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_CONV.Changed,
'select case when len(cast(ConvNotes as varchar(24)))=22 then null else ConvNotes end As Notes FROM CONV WHERE ConvID = '+ cast(v_FI_CONV.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '32500001'
     ELSE '325'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_FI_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_CONV.localcode <> ''
     THEN '/TS/' + v_FI_CONV.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> ''
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> ''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> ''
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_CONV.FromDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/* CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END, */
/*'select  RDcase when len(cast(Reason as varchar(24)))=22 then null else BkrpNotes end As Notes FROM RD WHERE RDID = '+ cast(v_FI_CONV.EventID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONV',
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
       AND v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     WHEN v_FI_CONV.FromDate is not null and v_FI_CONV.FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/UKWN'
     WHEN v_FI_CONV.ToDate is not null and v_FI_CONV.ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_FI_CONV.Price  <>'' AND v_FI_CONV.Price <>''
     THEN ':90B::EXER//ACTU/'+v_FI_CONV.CurenCD+
          +substring(v_FI_CONV.Price,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_CONV.resTFB1 <>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
CASE WHEN v_FI_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_FI_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_FI_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_FI_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_FI_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_FI_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v_FI_CONV.RatioNew <>''
          AND v_FI_CONV.RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_FI_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(v_FI_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
/*Currently no suitable paydate
CASE WHEN v_FI_CONV.PayDate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_FI_CONV.Paydate,112)
     WHEN v_FI_CONV.payDate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_FI_CONV.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_FI_CONV
WHERE
mainTFB1<>''
and changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and Actflag  <>''
order by caref2 desc, caref1, caref3
