--SEMETABLE=client.dbo.seme_fi_isin
--FileName=edi_YYYYMMDD_mt564
-- YYYYMMDD_FI564_PRED
--FileNameNotes=edi_YYYYMMDD_mt568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_REDM.Changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then null else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(v_FI_REDM.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '41000001'
     ELSE '410'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,'' as SRID,
CASE WHEN v_FI_REDM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_REDM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_REDM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_REDM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRED',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_REDM.localcode <> ''
     THEN '/TS/' + v_FI_REDM.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> ''
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> ''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN v_FI_REDM.CurenCD <> ''
     THEN ':11A::DENO//'+v_FI_REDM.CurenCD
     END,
CASE WHEN v_FI_REDM.MaturityDate <>''
     THEN ':98A::MATU//'+CONVERT ( varchar , v_FI_REDM.MaturityDate,112)
     ELSE ':98B::MATU//UKWN'
     END,
CASE WHEN v_FI_REDM.InterestRate <> ''
     THEN ':92A::INTR//'+v_FI_REDM.InterestRate
     ELSE ':92K::INTR//UKWN'
     END,
CASE WHEN MCD <> ''
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_REDM.RedemDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_REDM.RedemDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_FI_REDM.CurenCD  <>''
     THEN ':11A::OPTN//' + v_FI_REDM.CurenCD
     ELSE ':11A::OPTN//UKWN'
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN RedemPercent <>''
     THEN ':90A::OFFR//PRCT/' + RedemPercent
     WHEN nominalvalue <>'' and RedemPrice<>''
          and nominalvalue <>'0' and RedemPrice<>'0'
     THEN ':90A::OFFR//PRCT/' + cast(cast(cast(RedemPrice as decimal(14,5))*100
                                 /cast(nominalvalue as decimal(14,5)) as money) as varchar (30))
     WHEN RedemPrice <> ''
     THEN ':90B::OFFR//ACTU/' + CurenCD + RedemPrice
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
CASE WHEN RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , RedemDate,112)
     ELSE ':98F::PAYD//UKWN'
     END,
':16S:CASHMOVE',
/* POOLFACTOR SECTION GOES HERE */
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_REDM
WHERE
mainTFB1<>''
and changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (indefpay='' or indefpay = 'P')
and partfinal = 'P'
and redemdate <>''
and redemdate <> '1800/01/01'
and not (redemtype='MAT' or maturitydate=redemdate)
and poolfactor <> ''
and redemtype <> 'PUT'
order by caref2 desc, caref1, caref3
