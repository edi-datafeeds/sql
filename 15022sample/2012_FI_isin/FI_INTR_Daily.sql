--SEMETABLE=client.dbo.seme_fi_isin
--FileName=edi_YYYYMMDD_mt564
-- YYYYMMDD_FI564_INTR
--FileNameNotes=edi_YYYYMMDD_mt568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_INTR.Changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then null else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(v_FI_INTR.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '40000001'
     ELSE '400'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,'' as SRID,
CASE WHEN v_FI_INTR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_INTR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_INTR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_INTR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INTR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_INTR.localcode <> ''
     THEN '/TS/' + v_FI_INTR.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> ''
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> ''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> ''
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_INTR.ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_FI_INTR.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v_FI_INTR.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_FI_INTR.recdate,112)
     ELSE ':98A::RDTE//UKWN'
     END,
CASE WHEN v_FI_INTR.InterestFromdate <>''
       AND v_FI_INTR.InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , v_FI_INTR.InterestToDate,112)+'/'
          +CONVERT ( varchar , v_FI_INTR.InterestFromDate,112)
     WHEN v_FI_INTR.InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , v_FI_INTR.InterestToDate,112)+'/UKWN'
     WHEN v_FI_INTR.InterestFromdate <>''
     THEN ':69A::INPE//UKWN/'
          +CONVERT ( varchar , v_FI_INTR.InterestFromDate,112)
     ELSE ':69A::INPE//UKWN/UKWN'
     END,
CASE WHEN v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTR//' + v_FI_INTR.AnlCoupRate
     ELSE ':92A::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_INTR.Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , v_FI_INTR.PayDate,112)
     ELSE ':98F::PAYD//UKWN'
     END,
CASE WHEN v_FI_INTR.InterestPaymentFrequency = 'ANL'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + v_FI_INTR.AnlCoupRate
     WHEN v_FI_INTR.InterestPaymentFrequency = 'SMA'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'QTR'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'MNT'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92A::INTP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_INTR
WHERE
mainTFB1<>''
and changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (indefpay='' or indefpay = 'P')
order by caref2 desc, caref1, caref3
