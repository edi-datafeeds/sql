--SEMETABLE=client.dbo.seme_fi_isin
--FileName=edi_YYYYMMDD_mt564
-- YYYYMMDD_FI564_EXTM
--FileNameNotes=edi_YYYYMMDD_mt568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_EXTM.Changed,
'select case when len(cast(Notes as varchar(24)))=22 then null else Notes end As Notes FROM MTCHG WHERE MtchgID = '+ cast(v_FI_EXTM.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '41500001'
     ELSE '415'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,'' as SRID,
CASE WHEN v_FI_EXTM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_EXTM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_EXTM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_EXTM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXTM',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_EXTM.localcode <> ''
     THEN '/TS/' + v_FI_EXTM.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> ''
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> ''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> ''
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_EXTM.NewMaturityDate is not null and v_FI_EXTM.NewMaturityDate <>''
     THEN ':98A::MATU//'+CONVERT ( varchar , v_FI_EXTM.NewMaturityDate,112)
     ELSE ':98A::MATU//UKWN'
     END,
':16S:CADETL',
'' as Notes, 
'-}$'
From v_FI_EXTM
WHERE
mainTFB1<>''
and changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
order by caref2 desc, caref1, caref3
