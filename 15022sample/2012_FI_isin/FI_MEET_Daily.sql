--SEMETABLE=client.dbo.seme_fi_isin
--FileName=edi_YYYYMMDD_mt564
-- YYYYMMDD_FI564_MEET
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_MEET.Changed,
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '36100001'
     ELSE '361'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_MEET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_MEET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_MEET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_MEET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN v_FI_MEET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN v_FI_MEET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN v_FI_MEET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_MEET.AGMEGM = 'BH'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_MEET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_MEET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_MEET.localcode <> ''
     THEN '/TS/' + v_FI_MEET.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> ''
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> ''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> ''
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_MEET.AGMdate <>''
     THEN ':98A::MEET//'+CONVERT ( varchar , v_FI_MEET.AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//' + substring(rtrim(v_FI_MEET.Add1),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add1),36,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add2),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add2),36,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add3),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add4),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add5),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Add6),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.City),1,35) as TIDYTEXT,
substring(rtrim(v_FI_MEET.Country),1,35) as TIDYTEXT,
':16S:ADDINFO',
'-}$'
From v_FI_MEET
WHERE 
mainTFB1<>''
and changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
order by caref2 desc, caref1, caref3
