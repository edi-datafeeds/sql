--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_DFLT_INT
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_INTR.Changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then null else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(v_FI_INTR.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '40000001'
     ELSE '400'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
case when Sedol is not null then SedolID else SecID end as CAREF3,'' as SRID,
CASE WHEN v_FI_INTR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_INTR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_INTR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_INTR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DFLT',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN MCD is not null and MCD <>''
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_INTR.ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_FI_INTR.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v_FI_INTR.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_FI_INTR.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v_FI_INTR.InterestFromdate <>''
       AND v_FI_INTR.InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , v_FI_INTR.InterestToDate,112)+'/'
          +CONVERT ( varchar , v_FI_INTR.InterestFromDate,112)
     WHEN v_FI_INTR.InterestTodate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , v_FI_INTR.InterestToDate,112)+'/UKWN'
     WHEN v_FI_INTR.InterestFromdate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , v_FI_INTR.InterestFromDate,112)
     ELSE ':69J::INPE//UKWN/UKWN'
     END,
CASE WHEN v_FI_INTR.AnlCoupRate  <>''
     THEN ':92A::INTR//' + v_FI_INTR.AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_FI_INTR.CurenCD  <>''
     THEN ':11A::OPTN//' + v_FI_INTR.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_INTR.Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , v_FI_INTR.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN v_FI_INTR.InterestPaymentFrequency = 'ANL'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + v_FI_INTR.AnlCoupRate
     WHEN v_FI_INTR.InterestPaymentFrequency = 'SMA'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'QTR'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN v_FI_INTR.InterestPaymentFrequency = 'MNT'
           AND v_FI_INTR.AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(v_FI_INTR.AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
/* '' as Notes, */ 
'-}$'
From v_FI_INTR
WHERE
mainTFB2<>''
     and ((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
          and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='U')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='U')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='U')))
     OR 
    (Paydate>getdate()-183
     and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='I'))))
and indefpay <>''
and indefpay = 'F'
order by caref2 desc, caref1, caref3