--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_CONS
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_CONS.Changed,
'select case when len(cast(Notes as varchar(24)))=22 then null else Notes end As Notes FROM COSNT WHERE RDID = '+ cast(v_FI_CONS.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '41600001'
     ELSE '416'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
case when Sedol is not null then SedolID else SecID end as CAREF3,
'' as SRID,
CASE WHEN v_FI_CONS.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_CONS.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CONS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CONS.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN MCD is not null and MCD <>''
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_CONS.RecDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , v_FI_CONS.RecDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN v_FI_CONS.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_FI_CONS.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONN',
':17B::DFLT//N',
':17B::WTHD//Y',
CASE WHEN v_FI_CONS.Expirydate <>''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_FI_CONS.Expirydate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
':17B::WTHD//Y',
CASE WHEN v_FI_CONS.Expirydate <>''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_FI_CONS.Expirydate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From v_FI_CONS
WHERE
mainTFB2<>''
     and ((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
          and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='U')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='U')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='U')))
     OR 
    (Expirydate>getdate()-183
     and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='I'))))
order by caref2 desc, caref1, caref3
