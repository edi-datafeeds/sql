--SEMETABLE=client.dbo.seme_coretexa
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_REDO
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\No_Cull_Feeds\15022\2012_fi_isin\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct 
v_FI_REDO.Changed,
'select case when len(cast(CurrdNotes as varchar(24)))=22 then null else CurrdNotes end As Notes FROM CURRD WHERE CurrdID = '+ cast(v_FI_REDO.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '32500001'
     ELSE '325'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
case when Sedol is not null then SedolID else SecID end as CAREF3,
'' as SRID,
CASE WHEN v_FI_REDO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_FI_REDO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_REDO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_REDO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN MCD is not null and MCD <>''
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_REDO.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_REDO.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_REDO.mainTFB2 <>''
     THEN ':35B:' + mainTFB2
     ELSE ':35B:UKWN'
     END,
CASE WHEN MCD <> ''
     THEN ':16R:FIA'
     END,
CASE WHEN v_FI_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + v_FI_REDO.MCD
':11A::DENO//'+v_FI_REDO.NewCurenCD,
CASE WHEN v_FI_REDO.NewCurenCD=v_FI_REDO.Current_Currency and v_FI_REDO.MinimumDenomination<>''
    THEN ':36B::MINO'+v_FI_REDO.MinimumDenomination
    END,
CASE WHEN MCD <> ''
     THEN ':16S:FIA'
     END,
CASE WHEN v_FI_REDO.newparvalue <>''
            AND v_FI_REDO.oldparvalue <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_FI_REDO.newparvalue as char (15)))+
                    '/'+rtrim(cast(v_FI_REDO.oldparvalue as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From v_FI_REDO
WHERE
mainTFB2<>''
     and ((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
          and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='U')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='U')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='U')))
     OR 
    (EffectiveDate>getdate()-183
     and (sedol in (select code from client.dbo.pfsedol where accid = 233 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 233 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 233 and actflag='I'))))
and NewCurenCD <>''
and Actflag  <>''
order by caref2 desc, caref1, caref3
