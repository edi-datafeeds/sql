--SEMETABLE=client.dbo.seme_sample
--FilePrefix=
--FileName=YYYYMMDD_FI564_REDO
--FileNameNotes=YYYYMMDD_FI568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--Archive=off
--ArchivePath=n:\15022\fi_sample\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select top 20 
v_FI_REDO.Changed,
'select  CurrdNotes As Notes FROM CURRD WHERE CurrdID = '+ cast(v_FI_REDO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '32500001'
     ELSE '325'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_FI_REDO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_FI_REDO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_REDO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_REDO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_REDO.localcode <> ''
     THEN '/TS/' + v_FI_REDO.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_REDO.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_REDO.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB1,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16R:FIA'
     END,
CASE WHEN v_FI_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + v_FI_REDO.MCD
     END,
':11A::DENO//'+v_FI_REDO.NewCurenCD,
CASE WHEN v_FI_REDO.NewCurenCD=v_FI_REDO.Current_Currency
    THEN ':36B::MINO'+v_FI_REDO.MinimumDenomination
    END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16S:FIA'
     END,
/*CASE WHEN v_FI_REDO.oldparvalue  is not null and v_FI_REDO.newparvalue  is not null
     and cast(v_FI_REDO.oldparvalue as decimal(11,5))>0 and cast(v_FI_REDO.newparvalue as decimal(11,5))>0
     THEN ':92D::NEWO//1,/'+cast(cast(v_FI_REDO.oldparvalue as decimal(11,5))/cast(v_FI_REDO.newparvalue as decimal(11,5)) as varchar(30))
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,*/
CASE WHEN v_FI_REDO.newparvalue IS NOT NULL
            AND v_FI_REDO.oldparvalue IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(v_FI_REDO.newparvalue as char (15)))+
                    '/'+rtrim(cast(v_FI_REDO.oldparvalue as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_FI_REDO
WHERE
mainTFB1<>'' 

and NewCurenCD is not null
and Actflag  is not null
order by caref2 desc, caref1, caref3
