--SEMETABLE=client.dbo.seme_sample
--FilePrefix=
--FileName=YYYYMMDD_FI564_EXTM
--FileNameNotes=YYYYMMDD_FI568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--Archive=off
--ArchivePath=n:\15022\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select top 20 
v_FI_EXTM.Changed,
'select Notes FROM MTCHG WHERE MtchgID = '+ cast(v_FI_EXTM.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '41500001'
     ELSE '415'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_EXTM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_EXTM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_EXTM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_EXTM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXTM',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_EXTM.localcode <> ''
     THEN '/TS/' + v_FI_EXTM.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_EXTM.NewMaturityDate is not null AND v_FI_EXTM.NewMaturityDate IS NOT NULL
     THEN ':98A::MATU//'+CONVERT ( varchar , v_FI_EXTM.NewMaturityDate,112)
     ELSE ':98A::MATU//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_FI_EXTM.PVCurrency  is not null
     THEN ':11A::OPTN//' + v_FI_EXTM.PVCurrency
     ELSE ':11A::OPTN//UKWN'
     END,
':17B::DFLT//Y',
':16S:CAOPTN',
/* Notes populated by rendering programme :70E::TXNR// */
'' as Notes, 
'-}$'
From v_FI_EXTM
WHERE
mainTFB1<>'' 

order by caref2 desc, caref1, caref3
