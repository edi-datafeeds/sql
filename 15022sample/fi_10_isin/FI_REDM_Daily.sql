--SEMETABLE=client.dbo.seme_sample
--FilePrefix=
--FileName=YYYYMMDD_FI564_REDM
--FileNameNotes=YYYYMMDD_FI568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--Archive=off
--ArchivePath=n:\15022\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select top 20 
v_FI_REDM.Changed,
'select RedemNotes As Notes FROM REDEM WHERE RedemID = '+ cast(v_FI_REDM.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '41000001'
     ELSE '410'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_REDM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_REDM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_REDM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_REDM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDM',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_REDM.localcode <> ''
     THEN '/TS/' + v_FI_REDM.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_REDM.RedemDate is not null AND v_FI_REDM.RedemDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_REDM.RedemDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_FI_REDM.CurenCD  is not null
     THEN ':11A::OPTN//' + v_FI_REDM.CurenCD
     ELSE ':11A::OPTN//UKWN'
     END,
':17B::DFLT//Y',
CASE WHEN v_FI_REDM.RedemPercent is not null
     THEN ':90A::OFFR//PRCT/' + CONVERT ( varchar , v_FI_REDM.RedemPercent,112)
     WHEN v_FI_REDM.RedemPrice is not null
     THEN ':90B::OFFR//ACTU/' + v_FI_REDM.CurenCD + CONVERT ( varchar , v_FI_REDM.RedemPrice,112)
     ELSE ':90A::OFFR//PRCT/UKWN'
     END as COMMASUB,
':16S:CAOPTN',
/* Notes populated by rendering programme :70E::TXNR// */
'' as Notes, 
'-}$'
From v_FI_REDM
WHERE
mainTFB1<>'' 

and (redemdefault is null or redemdefault = 'PD')
and mandoptflag = 'M'
and (partfinal = 'F' or maturitydate=redemdate)
and redemdate is not null
and redemdate <> '1800/01/01'
and (redemtype='MAT' or maturitydate=redemdate)
order by caref2 desc, caref1, caref3
