--SEMETABLE=client.dbo.seme_sample
--FilePrefix=
--FileName=YYYYMMDD_FI564_CONS
--FileNameNotes=YYYYMMDD_FI568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--Archive=off
--ArchivePath=n:\15022\fi_sample\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select top 20 
v_FI_CONS.Changed,
'select Notes FROM COSNT WHERE RDID = '+ cast(v_FI_CONS.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '41600001'
     ELSE '416'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_FI_CONS.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_CONS.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CONS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CONS.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_CONS.localcode <> ''
     THEN '/TS/' + v_FI_CONS.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_CONS.RecDate is not null
     THEN ':98A::ANOU//'+CONVERT ( varchar , v_FI_CONS.RecDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
/* CASE WHEN v_FI_CONS.Expirydate is not null
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_FI_CONS.Expirydate,112)
     ELSE ':98B::EXPI//UKWN'
     END, */
':16S:CADETL',
'' as Notes,
'-}$'
From v_FI_CONS
WHERE
mainTFB1<>'' 

order by caref2 desc, caref1, caref3
