--SEMETABLE=client.dbo.seme_sample
--FilePrefix=
--FileName=YYYYMMDD_FI564_CONV
--FileNameNotes=YYYYMMDD_FI568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--Archive=off
--ArchivePath=n:\15022\fi_sample\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select top 20 
v_FI_CONV.Changed,
'select  ConvNotes As Notes FROM CONV WHERE ConvID = '+ cast(v_FI_CONV.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is null
     THEN '32500001'
     ELSE '325'+EXCHGID+cast(Seqnum as char(1))
     END as Caref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_FI_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_FI_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v_FI_CONV.localcode <> ''
     THEN '/TS/' + v_FI_CONV.Localcode
     ELSE ''
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16R:FIA'
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN MCD <> '' and MCD is not null
     THEN ':16S:FIA'
     END,
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_CONV.FromDate is not null AND v_FI_CONV.FromDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_CONV.FromDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/* CASE WHEN v_FI_CONV.ToDate is not null AND v_FI_CONV.ToDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END, */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_FI_CONV.EventID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONV',
CASE WHEN v_FI_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN v_FI_CONV.ToDate is not null AND v_FI_CONV.ToDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_FI_CONV.ToDate is not null AND v_FI_CONV.ToDate IS NOT NULL
       AND v_FI_CONV.FromDate is not null AND v_FI_CONV.FromDate IS NOT NULL
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     WHEN v_FI_CONV.FromDate is not null AND v_FI_CONV.FromDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_FI_CONV.FromDate,112)+'/UKWN'
     WHEN v_FI_CONV.ToDate is not null AND v_FI_CONV.ToDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_FI_CONV.ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_FI_CONV.Price  is not null AND v_FI_CONV.Price IS NOT NULL
     THEN ':90B::EXER//ACTU/'+v_FI_CONV.CurenCD+
          +substring(v_FI_CONV.Price,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + resTFB1,
CASE WHEN v_FI_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_FI_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_FI_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_FI_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_FI_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_FI_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v_FI_CONV.RatioNew  is not null
          AND v_FI_CONV.RatioOld  is not null
     THEN ':92D::NEWO//'+rtrim(cast(v_FI_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(v_FI_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
/*Currently no suitable paydate
CASE WHEN v_FI_CONV.PayDate is not null 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_FI_CONV.Paydate,112)
     WHEN v_FI_CONV.payDate is not null 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_FI_CONV.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_FI_CONV
WHERE
mainTFB1<>'' 

and Actflag  is not null
order by caref2 desc, caref1, caref3
