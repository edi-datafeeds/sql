--filepath=o:\smfsql\align\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.101
--suffix=
--fileheadertext=EDI_SMF_101_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use smf4
SELECT
Security.Actdate,
Security.Actflag,
Security.SecurityID,
Security.Cregcode AS CountryofReg,
Security.NewInfosource,
Security.OPOL,
Security.Isin,
Security.IssuerID,
Issuer.Issuername,
Security.Longdesc AS SecurityDescription,
Security.Sedol,
Security.Statusflag,
Security.Eventcode,
Issuer.Cinccode AS CountryofInc,
Security.Sectype,
Security.PrimaryListing
FROM Security
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
WHERE 
(Security.Actflag<>'D')
AND (Security.Confirmation='C')
and (Security.Confirmation is not null)

/*Format
field		Datatype(maxwidth)	Description
Actflag,	Char(1)			Action (I=Insert, U=Update, D=Delete, F=Full Alignment)
Acttime,	Date(10)		Action time (Last modified)
SecurityID,		Int(10)		Unique Global internal number at the sedol level
Cregcode,	Char(2)			ISO country of Reg Code (*)
Infosource,	Char(3)			Old infosource
OPOL,		Char(3)			Official place of listing (MIC or UNL or FMQ - fund manager quote)
Isin,		Char(12)		ISIN
IssuerID,	Int(10)			Unique Global internal number for LSE issuer
Issuername,	Varchar(35)		Issuername
Longdesc,	Varchar(40)		Security Description
Sedol,		Char(7)		        Sedol code
Eventcode,	Char(2)			Security Event Code (*)
Cinccode,	Char(2)			ISO country of Incorporation Code (*)
Sectype,	Char(2)			New style Security Type (*)
PrimaryListing	Char(1)			1 = Primary

(*) See SMF feed documentation for all possible values and meanings
Date format = YYYY/MM/DD
*/ 