Declare @AlignDate datetime
Declare @StartDate datetime
Declare @EndDate datetime

set @AlignDate = '2002/03/01'



--filepath=o:\datafeed\WCA\604\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.604
--suffix=
--fileheadertext=EDI_REORG
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\604\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n




/* Use script Var and Sets*/
--UseIt 2002/03/01

--# 1
USE WCA
SELECT * 
FROM v50f_604_Company_Meeting 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 2
USE WCA
SELECT *
FROM v53f_604_Call
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 3
USE WCA
SELECT * 
FROM v50f_604_Liquidation
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 4
USE WCA
SELECT *
FROM v51f_604_Certificate_Exchange
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 5
USE WCA
SELECT * 
FROM v51f_604_International_Code_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 6
USE WCA
SELECT * 
FROM v51f_604_Preference_Conversion
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 7
use wca
SELECT * 
FROM v51f_604_Preference_Redemption
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 8
use wca
SELECT * 
FROM v51f_604_Security_Reclassification
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 9
use wca
SELECT * 
FROM v52f_604_Lot_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 10
use wca
SELECT * 
FROM v52f_604_Sedol_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 11
use wca
SELECT * 
FROM v53f_604_Buy_Back
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 12
use wca
SELECT * 
FROM v53f_604_Capital_Reduction
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 14
use wca
SELECT * 
FROM v53f_604_Takeover
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 15
use wca
SELECT * 
FROM v54f_604_Arrangement
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 16
use wca
SELECT * 
FROM v54f_604_Bonus
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 17
use wca
SELECT * 
FROM v54f_604_Bonus_Rights
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 18
use wca
SELECT * 
FROM v54f_604_Consolidation
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 19
use wca
SELECT * 
FROM v54f_604_Demerger
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 20
use wca
SELECT * 
FROM v54f_604_Distribution
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 21
use wca
SELECT * 
FROM v54f_604_Divestment
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 22
use wca
SELECT * 
FROM v54f_604_Entitlement
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 23
use wca
SELECT * 
FROM v54f_604_Merger
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 24
use wca
SELECT * 
FROM v54f_604_Preferential_Offer
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 25
use wca
SELECT * 
FROM v54f_604_Purchase_Offer
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 26
use wca
SELECT * 
FROM v54f_604_Rights 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 27
use wca
SELECT * 
FROM v54f_604_Security_Swap 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 28
use wca
SELECT *
FROM v54f_604_Subdivision
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol


/* These Views Not Required By Fidelity Align but are in the daily */
--# 29
use wca
SELECT *
FROM v50f_604_Bankruptcy 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 30
use wca
SELECT *
FROM v50f_604_Financial_Year_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 31
use wca
SELECT *
FROM v50f_604_Incorporation_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 32
use wca
SELECT *
FROM v50f_604_Issuer_Name_change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 33
use wca
SELECT *
FROM v50f_604_Lawsuit
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 34
use wca
SELECT *
FROM v50f_604_Registered_Office_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 35
use wca
SELECT *
FROM v51f_604_Registrar_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 36
use wca
SELECT *
FROM v51f_604_Security_Description_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 37
use wca
SELECT *
FROM v52f_604_Assimilation
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 38
use wca
SELECT *
FROM v52f_604_Listing_Status_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 39
use wca
SELECT *
FROM v52f_604_Local_Code_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 40
use wca
SELECT * 
FROM v52f_604_New_Listing
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 42
use wca
SELECT * 
FROM v50f_604_Announcement
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 43
use wca
SELECT * 
FROM v51f_604_Parvalue_Redenomination 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 44
USE WCA
SELECT * 
FROM v51f_604_Currency_Redenomination 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol

--# 45
USE WCA
SELECT * 
FROM v53f_604_Return_of_Capital 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, xSedol
