--filepath=o:\datafeed\wca\608\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.608
--suffix=
--fileheadertext=EDI_REORG
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\608_daily
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
USE WCA
SELECT * 
FROM v50f_608_Company_Meeting 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 2
USE WCA
SELECT *
FROM v53f_608_Call
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 3
USE WCA
SELECT * 
FROM v50f_608_Liquidation
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol, OptionID, SerialID

--# 4
USE WCA
SELECT *
FROM v51f_608_Certificate_Exchange
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 5
USE WCA
SELECT * 
FROM v51f_608_International_Code_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 6
USE WCA
SELECT * 
FROM v51f_608_Preference_Conversion
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 7
use wca
SELECT * 
FROM v51f_608_Preference_Redemption
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 8
use wca
SELECT * 
FROM v51f_608_Security_Reclassification
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 9
use wca
SELECT * 
FROM v52f_608_Lot_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 10
use wca
SELECT * 
FROM v52f_608_Sedol_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 11
use wca
SELECT * 
FROM v53f_608_Buy_Back
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol, OptionID, SerialID

--# 12
use wca
SELECT * 
FROM v53f_608_Capital_Reduction
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 14
use wca
SELECT * 
FROM v53f_608_Takeover
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol, OptionID, SerialID

--# 15
use wca
SELECT * 
FROM v54f_608_Arrangement
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 16
use wca
SELECT * 
FROM v54f_608_Bonus
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 17
use wca
SELECT * 
FROM v54f_608_Bonus_Rights
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 18
use wca
SELECT * 
FROM v54f_608_Consolidation
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 19
use wca
SELECT * 
FROM v54f_608_Demerger
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol, OptionID, SerialID

--# 20
use wca
SELECT * 
FROM v54f_608_Distribution
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol, OptionID, SerialID

--# 21
use wca
SELECT * 
FROM v54f_608_Divestment
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 22
use wca
SELECT * 
FROM v54f_608_Entitlement
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 23
use wca
SELECT * 
FROM v54f_608_Merger
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol, OptionID, SerialID

--# 24
use wca
SELECT * 
FROM v54f_608_Preferential_Offer
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 25
use wca
SELECT * 
FROM v54f_608_Purchase_Offer
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 26
use wca
SELECT * 
FROM v54f_608_Rights 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 27
use wca
SELECT * 
FROM v54f_608_Security_Swap 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 28
use wca
SELECT *
FROM v54f_608_Subdivision
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 29
use wca
SELECT *
FROM v50f_608_Bankruptcy 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 30
use wca
SELECT *
FROM v50f_608_Financial_Year_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 31
use wca
SELECT *
FROM v50f_608_Incorporation_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 32
use wca
SELECT *
FROM v50f_608_Issuer_Name_change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 33
use wca
SELECT *
FROM v50f_608_Lawsuit
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 34
use wca
SELECT *
FROM v50f_608_Registered_Office_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 35
use wca
SELECT *
FROM v51f_608_Registrar_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 36
use wca
SELECT *
FROM v51f_608_Security_Description_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 37
use wca
SELECT *
FROM v52f_608_Assimilation
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 38
use wca
SELECT *
FROM v52f_608_Listing_Status_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 39
use wca
SELECT *
FROM v52f_608_Local_Code_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 40
use wca
SELECT * 
FROM v52f_608_New_Listing
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 42
use wca
SELECT * 
FROM v50f_608_Announcement
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 43
use wca
SELECT * 
FROM v51f_608_Parvalue_Redenomination 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 44
USE WCA
SELECT * 
FROM v51f_608_Currency_Redenomination 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol

--# 45
USE WCA
SELECT * 
FROM v53f_608_Return_of_Capital 
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExCountry, Exchange, Sedol
