--filepath=o:\udrsql\aigdup\
--filenameprefix=AIGUD
--filename=r
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=YYYYMMDD
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=,	
--outputstyle=
--archive=Y
--archivepath=n:\udr\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--shownulls=




--# 1
use udr
SELECT 
'"' + drSedol+'"' as [".Drsedol"],
'"' + Issuername+'"' AS [".Issuername"], 
'"' + Descrip+'"' AS [".Drdescrip"],
'"' + unSedol+'"' AS [".Unsedol"], 
'"' + unDescrip+'"' AS [".Undescrip"],
'"' + Uscode+'"' AS [".Cusip"], 
'"' + drIsin+'"' AS [".Drisin"],
'"' + unIsin+'"' AS [".Unisin"]
FROM UDRNEW
WHERE 
drSedol IS NOT NULL
AND unSedol IS NOT NULL
AND LTRIM(drSedol) <> ''
AND LTRIM(unSedol) <> ''
and Statusflag<>'I'
and DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'
order by udrnew.uscode, 
case when drusSEDOLActtime is not null THEN drusSEDOLActtime ELSE drSEDOLActtime END desc,
case when unSEDOLActtime is not null THEN unSEDOLActtime ELSE ungbSEDOLActtime END







/*
Fieldname Datatype(Maxwidth)	Notes
DRSedol, Char(7)
Issuername, varchar(70)
DRDescrip, varchar(70)
UNSedol, Char(7)
UNDescrip,varchar(70)
Uscode, Char(9)
CUSIP, Char(12)
UNIsin, Char(12)
*/