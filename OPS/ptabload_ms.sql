---------------------------------------------------------------------------
INSERT INTO wca.dbo.bochg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldOutValue, NewOutValue, bochgID, bochgNotes, Eventtype, OldOutDate, NewOutDate, ReleventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldOutValue, NewOutValue, efdtlinkid, zbochgNotes, Eventtype, OldOutDate, NewOutDate, ReleventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zbochg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select bochgid from wca.dbo.bochg)
and zbochgid = (select top 1 zbochgid from wca.dbo.zbochg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldOutValue=z.OldOutValue, m.bochgNotes=z.zbochgNotes, m.Eventtype=z.Eventtype,
m.OldOutDate=z.OldOutDate, m.ReleventID=z.ReleventID,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.bochg as m
inner join wca.dbo.zbochg as z on m.bochgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zbochgid = (select top 1 zbochgid from wca.dbo.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewOutValue=z.NewOutValue, m.bochgNotes=z.zbochgNotes, m.Eventtype=z.Eventtype,
m.NewOutDate=z.NewOutDate, m.ReleventID=z.ReleventID,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.bochg as m
inner join wca.dbo.zbochg as z on m.bochgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zbochgid = (select top 1 zbochgid from wca.dbo.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.bochg as m
inner join wca.dbo.zbochg as z on m.bochgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zbochgid = (select top 1 zbochgid from wca.dbo.zbochg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
---------------------------------------------------------------------------
INSERT INTO wca.dbo.bschg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldBondType, NewBondType, OldCurenCD, NewCurenCD, OldPIU, NewPIU, OldInterestBasis, NewInterestBasis,
Eventtype, BschgID, BschgNotes, OldInterestCurrency, NewInterestCurrency, OldMaturityCurrency, NewMaturityCurrency,
OldIntBusDayConv, NewIntBusDayConv, OldMatBusDayConv, NewMatBusDayConv, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldBondType, NewBondType, OldCurenCD, NewCurenCD, OldPIU, NewPIU, OldInterestBasis, NewInterestBasis,
Eventtype, EfdtLinkID, zbschgNotes, OldInterestCurrency, NewInterestCurrency, OldMaturityCurrency, NewMaturityCurrency,
OldIntBusDayConv, NewIntBusDayConv, OldMatBusDayConv, NewMatBusDayConv, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate FROM wca.dbo.zbschg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select bschgid from wca.dbo.bschg)
and zbschgid = (select top 1 zbschgid from wca.dbo.zbschg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.OldBondType=z.OldBondType, m.OldCurenCD=z.OldCurenCD, m.OldPIU=z.OldPIU,
m.OldInterestBasis=z.OldInterestBasis, m.Eventtype=z.Eventtype, m.BschgNotes=z.zbschgNotes, m.OldInterestCurrency=z.OldInterestCurrency,
m.OldMaturityCurrency=z.OldMaturityCurrency, m.OldIntBusDayConv=z.OldIntBusDayConv, m.OldMatBusDayConv=z.OldMatBusDayConv,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.bschg as m
inner join wca.dbo.zbschg as z on m.bschgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zbschgid = (select top 1 zbschgid from wca.dbo.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.NewBondType=z.NewBondType, m.NewCurenCD=z.NewCurenCD, m.NewPIU=z.NewPIU,
m.NewInterestBasis=z.NewInterestBasis, m.Eventtype=z.Eventtype, m.BschgNotes=z.zbschgNotes, m.NewInterestCurrency=z.NewInterestCurrency,
m.NewMaturityCurrency=z.NewMaturityCurrency, m.NewIntBusDayConv=z.NewIntBusDayConv, m.NewMatBusDayConv=z.NewMatBusDayConv,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.bschg as m
inner join wca.dbo.zbschg as z on m.bschgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zbschgid = (select top 1 zbschgid from wca.dbo.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.bschg as m
inner join wca.dbo.zbschg as z on m.bschgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zbschgid = (select top 1 zbschgid from wca.dbo.zbschg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.crchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, RatingAgency, NewRatingDate, OldRating, NewRating, OldDirection, OldWatchList, OldWatchListReason, crchgID, EventType,
OldRatingDate, NewDirection, NewWatchList, NewWatchListReason, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, RatingAgency, NewRatingDate, OldRating, NewRating, OldDirection, OldWatchList, OldWatchListReason, EfdtLinkID, EventType,
OldRatingDate, NewDirection, NewWatchList, NewWatchListReason,
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zcrchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select crchgid from wca.dbo.crchg)
and zcrchgid = (select top 1 zcrchgid from wca.dbo.zcrchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecId=z.SecId, m.RatingAgency=z.RatingAgency, m.OldRating=z.OldRating, m.OldDirection=z.OldDirection, m.OldWatchList=z.OldWatchList,
m.OldWatchListReason=z.OldWatchListReason, m.EventType=z.EventType, m.OldRatingDate=z.OldRatingDate,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.crchg as m
inner join wca.dbo.zcrchg as z on m.crchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zcrchgid = (select top 1 zcrchgid from wca.dbo.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecId=z.SecId, m.RatingAgency=z.RatingAgency, m.NewRatingDate=z.NewRatingDate, m.NewRating=z.NewRating, m.EventType=z.EventType,
m.NewDirection=z.NewDirection, m.NewWatchList=z.NewWatchList, m.NewWatchListReason=z.NewWatchListReason,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.crchg as m
inner join wca.dbo.zcrchg as z on m.crchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zcrchgid = (select top 1 zcrchgid from wca.dbo.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.crchg as m
inner join wca.dbo.zcrchg as z on m.crchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zcrchgid = (select top 1 zcrchgid from wca.dbo.zcrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
---------------------------------------------------------------------------
INSERT INTO wca.dbo.ctchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldResultantRatio, NewResultantRatio, OldSecurityRatio, NewSecurityRatio, OldCurrency, NewCurrency, OldConversionPrice,
NewConversionPrice, ResSecTyCD, OldResSecID, NewResSecID, EventType, RelEventID, Notes, ctchgID, OldFromDate, NewFromDate, OldTodate, NewToDate,
ConvtID, OldFXRate, NewFXRate, OldPriceAsPercent, NewPriceAsPercent, OldCurPair, NewCurPair, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldResultantRatio, NewResultantRatio, OldSecurityRatio, NewSecurityRatio, OldCurrency, NewCurrency, OldConversionPrice,
NewConversionPrice, ResSecTyCD, OldResSecID, NewResSecID, EventType, RelEventID, zctchgNotes, efdtlinkid, OldFromDate, NewFromDate, OldTodate,
NewToDate, ConvtID, OldFXRate, NewFXRate, OldPriceAsPercent, NewPriceAsPercent, OldCurPair, NewCurPair, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate FROM wca.dbo.zctchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ctchgid from wca.dbo.ctchg)
and zctchgid = (select top 1 zctchgid from wca.dbo.zctchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.OldResultantRatio=z.OldResultantRatio, m.OldSecurityRatio=z.OldSecurityRatio, 
m.OldCurrency=z.OldCurrency, m.OldConversionPrice=z.OldConversionPrice, m.ResSecTyCD=z.ResSecTyCD, m.OldResSecID=z.OldResSecID,
m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.OldFromDate=z.OldFromDate, m.OldTodate=z.OldTodate, m.ConvtID=z.ConvtID,
m.OldFXRate=z.OldFXRate, m.OldPriceAsPercent=z.OldPriceAsPercent, m.OldCurPair=z.OldCurPair,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ctchg as m
inner join wca.dbo.zctchg as z on m.ctchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zctchgid = (select top 1 zctchgid from wca.dbo.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.NewResultantRatio=z.NewResultantRatio, m.NewSecurityRatio=z.NewSecurityRatio, 
m.NewCurrency=z.NewCurrency, m.NewConversionPrice=z.NewConversionPrice, m.ResSecTyCD=z.ResSecTyCD, m.NewResSecID=z.NewResSecID,
m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.Notes=z.zctchgNotes, m.NewFromDate=z.NewFromDate, m.NewTodate=z.NewTodate, m.ConvtID=z.ConvtID,
m.NewFXRate=z.NewFXRate, m.NewPriceAsPercent=z.NewPriceAsPercent, m.NewCurPair=z.NewCurPair,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ctchg as m
inner join wca.dbo.zctchg as z on m.ctchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zctchgid = (select top 1 zctchgid from wca.dbo.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.ctchg as m
inner join wca.dbo.zctchg as z on m.ctchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zctchgid = (select top 1 zctchgid from wca.dbo.zctchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.ffc (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFreeFloat, NewFreeFloat, EventType, FfcID, OldFreeFloatDate, NewFreeFloatDate,
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFreeFloat, NewFreeFloat, EventType, EfdtLinkID, OldFreeFloatDate, NewFreeFloatDate,
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zffc as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ffcid from wca.dbo.ffc)
and zffcid = (select top 1 zffcid from wca.dbo.zffc as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldFreeFloat=z.OldFreeFloat, m.EventType=z.EventType, m.OldFreeFloatDate=z.OldFreeFloatDate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ffc as m
inner join wca.dbo.zffc as z on m.ffcID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zffcid = (select top 1 zffcid from wca.dbo.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, NewFreeFloat=z.NewFreeFloat, m.EventType=z.EventType, m.NewFreeFloatDate=z.NewFreeFloatDate,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ffc as m
inner join wca.dbo.zffc as z on m.ffcID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zffcid = (select top 1 zffcid from wca.dbo.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.ffc as m
inner join wca.dbo.zffc as z on m.ffcID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zffcid = (select top 1 zffcid from wca.dbo.zffc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.ifchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldIntPayFrqncy, OldIntPayDate1, OldIntPayDate2, OldIntPayDate3, OldIntPayDate4, NewIntPayFrqncy, NewIntPayDate1,
NewIntPayDate2, NewIntPayDate3, NewIntPayDate4, IfchgID, EventType, OldVarIntPaydate, NewVarIntPaydate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldIntPayFrqncy, OldIntPayDate1, OldIntPayDate2, OldIntPayDate3, OldIntPayDate4, NewIntPayFrqncy, NewIntPayDate1,
NewIntPayDate2, NewIntPayDate3, NewIntPayDate4, EfdtLinkID, EventType, OldVarIntPaydate, NewVarIntPaydate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zifchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ifchgid from wca.dbo.ifchg)
and zifchgid = (select top 1 zifchgid from wca.dbo.zifchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.OldIntPayFrqncy=z.OldIntPayFrqncy, m.OldIntPayDate1=z.OldIntPayDate1,
m.OldIntPayDate2=z.OldIntPayDate2, m.OldIntPayDate3=z.OldIntPayDate3, m.OldIntPayDate4=z.OldIntPayDate4, m.EventType=z.EventType,
m.OldVarIntPaydate=z.OldVarIntPaydate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ifchg as m
inner join wca.dbo.zifchg as z on m.ifchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zifchgid = (select top 1 zifchgid from wca.dbo.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.NewIntPayFrqncy=z.NewIntPayFrqncy, m.NewIntPayDate1=z.NewIntPayDate1,
m.NewIntPayDate2=z.NewIntPayDate2, m.NewIntPayDate3=z.NewIntPayDate3, m.NewIntPayDate4=z.NewIntPayDate4, m.EventType=z.EventType,
m.NewVarIntPaydate=z.NewVarIntPaydate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ifchg as m
inner join wca.dbo.zifchg as z on m.ifchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zifchgid = (select top 1 zifchgid from wca.dbo.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.ifchg as m
inner join wca.dbo.zifchg as z on m.ifchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zifchgid = (select top 1 zifchgid from wca.dbo.zifchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.icc (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldISIN, NewISIN, OldUSCode, NewUSCode, OldVALOREN, NewVALOREN, IccID, EventType, RelEventID, OldCommonCode, NewCommonCode, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldISIN, NewISIN, OldUSCode, NewUSCode, OldVALOREN, NewVALOREN, EfdtLinkID, EventType, RelEventID, OldCommonCode, NewCommonCode, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zicc as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select iccid from wca.dbo.icc)
and ziccid = (select top 1 ziccid from wca.dbo.zicc as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldISIN=z.OldISIN, m.OldUSCode=z.OldUSCode, m.OldVALOREN=z.OldVALOREN, m.EventType=z.EventType,
m.RelEventID=z.RelEventID, m.OldCommonCode=z.OldCommonCode, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.icc as m
inner join wca.dbo.zicc as z on m.iccID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.ziccid = (select top 1 ziccid from wca.dbo.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewISIN=z.NewISIN, m.NewUSCode=z.NewUSCode, m.NewVALOREN=z.NewVALOREN, m.EventType=z.EventType,
m.RelEventID=z.RelEventID, m.NewCommonCode=z.NewCommonCode, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.icc as m
inner join wca.dbo.zicc as z on m.iccID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.ziccid = (select top 1 ziccid from wca.dbo.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.icc as m
inner join wca.dbo.zicc as z on m.iccID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.ziccid = (select top 1 ziccid from wca.dbo.zicc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.irchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestRate, NewInterestRate, IRChgID, Notes, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestRate, NewInterestRate, EfdtLinkID, zIRChgNotes, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zirchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select irchgid from wca.dbo.irchg)
and zirchgid = (select top 1 zirchgid from wca.dbo.zirchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldInterestRate=z.OldInterestRate, m.Notes=z.zIRChgNotes, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.irchg as m
inner join wca.dbo.zirchg as z on m.irchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zirchgid = (select top 1 zirchgid from wca.dbo.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewInterestRate=z.NewInterestRate, m.Notes=z.zIRChgNotes, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.irchg as m
inner join wca.dbo.zirchg as z on m.irchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zirchgid = (select top 1 zirchgid from wca.dbo.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.irchg as m
inner join wca.dbo.zirchg as z on m.irchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zirchgid = (select top 1 zirchgid from wca.dbo.zirchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.ischg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
IssID, NameChangeDate, IssOldName, IssNewName, EventType, IsChgID, IsChgNotes, LegalName, MeetingDateFlag, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
IssID, NameChangeDate, IssOldName, IssNewName, EventType, EfdtLinkID, zIsChgNotes, LegalName, MeetingDateFlag, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zischg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ischgid from wca.dbo.ischg)
and zischgid = (select top 1 zischgid from wca.dbo.zischg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.IssID=z.IssID, m.NameChangeDate=z.NameChangeDate, m.IssOldName=z.IssOldName, m.EventType=z.EventType, m.IsChgNotes=z.zIsChgNotes,
m.LegalName=z.LegalName, m.MeetingDateFlag=z.MeetingDateFlag, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ischg as m
inner join wca.dbo.zischg as z on m.ischgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zischgid = (select top 1 zischgid from wca.dbo.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.IssID=z.IssID, m.NameChangeDate=z.NameChangeDate, m.IssNewName=z.IssNewName, m.EventType=z.EventType, m.IsChgNotes=z.zIsChgNotes,
m.LegalName=z.LegalName, m.MeetingDateFlag=z.MeetingDateFlag, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ischg as m
inner join wca.dbo.zischg as z on m.ischgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zischgid = (select top 1 zischgid from wca.dbo.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.ischg as m
inner join wca.dbo.zischg as z on m.ischgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zischgid = (select top 1 zischgid from wca.dbo.zischg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.frnfx (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFRNType, OldFRNIndexBenchmark, OldMarkup, OldMinimumInterestRate, OldMaximumInterestRate, OldRounding, NewFRNType, 
NewFRNindexBenchmark, NewMarkup, NewMinimumInterestRate, NewMaximumInterestRate, NewRounding, FrnfxID, EventType, OldFrnIntAdjFreq, NewFrnIntAdjFreq, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldFRNType, OldFRNIndexBenchmark, OldMarkup, OldMinimumInterestRate, OldMaximumInterestRate, OldRounding, NewFRNType,
NewFRNindexBenchmark, NewMarkup, NewMinimumInterestRate, NewMaximumInterestRate, NewRounding, EfdtLinkID, EventType, OldFrnIntAdjFreq, NewFrnIntAdjFreq, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zfrnfx as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select frnfxid from wca.dbo.frnfx)
and zfrnfxid = (select top 1 zfrnfxid from wca.dbo.zfrnfx as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldFRNType=z.OldFRNType, m.OldFRNIndexBenchmark=z.OldFRNIndexBenchmark, m.OldMarkup=z.OldMarkup,
m.OldMinimumInterestRate=z.OldMinimumInterestRate, m.OldMaximumInterestRate=z.OldMaximumInterestRate, m.OldRounding=z.OldRounding, m.EventType=z.EventType,
m.OldFrnIntAdjFreq=z.OldFrnIntAdjFreq, m.NewFrnIntAdjFreq=z.NewFrnIntAdjFreq, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.frnfx as m
inner join wca.dbo.zfrnfx as z on m.frnfxID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zfrnfxid = (select top 1 zfrnfxid from wca.dbo.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate,  m.NewFRNType=z.NewFRNType, m.NewFRNindexBenchmark=z.NewFRNindexBenchmark, m.NewMarkup=z.NewMarkup,
m.NewMinimumInterestRate=z.NewMinimumInterestRate, m.NewMaximumInterestRate=z.NewMaximumInterestRate, m.NewRounding=z.NewRounding, m.EventType=z.EventType,
m.OldFrnIntAdjFreq=z.OldFrnIntAdjFreq, m.NewFrnIntAdjFreq=z.NewFrnIntAdjFreq, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.frnfx as m
inner join wca.dbo.zfrnfx as z on m.frnfxID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zfrnfxid = (select top 1 zfrnfxid from wca.dbo.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.frnfx as m
inner join wca.dbo.zfrnfx as z on m.frnfxID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zfrnfxid = (select top 1 zfrnfxid from wca.dbo.zfrnfx as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.lcc (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLocalCode, NewLocalCode, LccID, EventType, RelEventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLocalCode, NewLocalCode, EfdtLinkID, EventType, RelEventID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zlcc as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select lccid from wca.dbo.lcc)
and zlccid = (select top 1 zlccid from wca.dbo.zlcc as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.OldLocalCode=z.OldLocalCode, m.EventType=z.EventType, m.RelEventID=z.RelEventID, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.lcc as m
inner join wca.dbo.zlcc as z on m.lccID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zlccid = (select top 1 zlccid from wca.dbo.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.NewLocalCode=z.NewLocalCode, m.EventType=z.EventType, m.RelEventID=z.RelEventID, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.lcc as m
inner join wca.dbo.zlcc as z on m.lccID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zlccid = (select top 1 zlccid from wca.dbo.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.lcc as m
inner join wca.dbo.zlcc as z on m.lccID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zlccid = (select top 1 zlccid from wca.dbo.zlcc as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.lstat (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
LstatID, SecID, ExchgCD, NotificationDate, EffectiveDate, LStatStatus, Reason, Eventtype, OldLStatStatus, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
EfdtLinkID, SecID, ExchgCD, NotificationDate, EffectiveDate, LStatStatus, Reason, Eventtype, OldLStatStatus, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zlstat as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select lstatid from wca.dbo.lstat)
and zlstatid = (select top 1 zlstatid from wca.dbo.zlstat as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.NotificationDate=z.NotificationDate, m.EffectiveDate=z.EffectiveDate, m.Reason=z.Reason,
m.Eventtype=z.Eventtype, m.OldLStatStatus=z.OldLStatStatus, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.lstat as m
inner join wca.dbo.zlstat as z on m.lstatID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zlstatid = (select top 1 zlstatid from wca.dbo.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.NotificationDate=z.NotificationDate, m.EffectiveDate=z.EffectiveDate, m.LStatStatus=z.LStatStatus,
m.Reason=z.Reason, m.Eventtype=z.Eventtype, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.lstat as m
inner join wca.dbo.zlstat as z on m.lstatID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zlstatid = (select top 1 zlstatid from wca.dbo.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.lstat as m
inner join wca.dbo.zlstat as z on m.lstatID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zlstatid = (select top 1 zlstatid from wca.dbo.zlstat as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.ltchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLot, OldMinTrdQty, NewLot, NewMinTrdgQty, LtChgID, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, ExchgCD, EffectiveDate, OldLot, OldMinTrdQty, NewLot, NewMinTrdgQty, EfdtLinkID, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zltchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select ltchgid from wca.dbo.ltchg)
and zltchgid = (select top 1 zltchgid from wca.dbo.zltchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.OldLot=z.OldLot, m.OldMinTrdQty=z.OldMinTrdQty, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ltchg as m
inner join wca.dbo.zltchg as z on m.ltchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zltchgid = (select top 1 zltchgid from wca.dbo.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.ExchgCD=z.ExchgCD, m.EffectiveDate=z.EffectiveDate, m.NewLot=z.NewLot, m.NewMinTrdgQty=z.NewMinTrdgQty, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.ltchg as m
inner join wca.dbo.zltchg as z on m.ltchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zltchgid = (select top 1 zltchgid from wca.dbo.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.ltchg as m
inner join wca.dbo.zltchg as z on m.ltchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zltchgid = (select top 1 zltchgid from wca.dbo.zltchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.mtchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldMaturityDate, NewMaturityDate, Reason, MtChgID, Notes, EventType, OldMaturityBenchmark, NewMaturityBenchmark, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, NotificationDate, OldMaturityDate, NewMaturityDate, Reason, EfdtLinkID, zMtChgNotes, EventType, OldMaturityBenchmark, NewMaturityBenchmark, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zmtchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select mtchgid from wca.dbo.mtchg)
and zmtchgid = (select top 1 zmtchgid from wca.dbo.zmtchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.OldMaturityDate=z.OldMaturityDate, m.Reason=z.Reason, m.Notes=z.zMtChgNotes,
m.EventType=z.EventType, m.OldMaturityBenchmark=z.OldMaturityBenchmark, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.mtchg as m
inner join wca.dbo.zmtchg as z on m.mtchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zmtchgid = (select top 1 zmtchgid from wca.dbo.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.NotificationDate=z.NotificationDate, m.NewMaturityDate=z.NewMaturityDate, m.Reason=z.Reason, m.Notes=z.zMtChgNotes,
m.EventType=z.EventType, m.NewMaturityBenchmark=z.NewMaturityBenchmark, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.mtchg as m
inner join wca.dbo.zmtchg as z on m.mtchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zmtchgid = (select top 1 zmtchgid from wca.dbo.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.mtchg as m
inner join wca.dbo.zmtchg as z on m.mtchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zmtchgid = (select top 1 zmtchgid from wca.dbo.zmtchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.rconv (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestAccrualConvention, NewInterestAccrualConvention, Notes, RconvID, OldConvMethod, NewConvMethod, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldInterestAccrualConvention, NewInterestAccrualConvention, zRconvNotes, EfdtLinkID, OldConvMethod, NewConvMethod, EventType, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zrconv as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select rconvid from wca.dbo.rconv)
and zrconvid = (select top 1 zrconvid from wca.dbo.zrconv as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldInterestAccrualConvention=z.OldInterestAccrualConvention, m.Notes=z.zRconvNotes, m.OldConvMethod=z.OldConvMethod,  m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.rconv as m
inner join wca.dbo.zrconv as z on m.rconvID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zrconvid = (select top 1 zrconvid from wca.dbo.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewInterestAccrualConvention=z.NewInterestAccrualConvention, m.Notes=z.zRconvNotes, m.NewConvMethod=z.NewConvMethod, m.EventType=z.EventType, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.rconv as m
inner join wca.dbo.zrconv as z on m.rconvID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zrconvid = (select top 1 zrconvid from wca.dbo.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.rconv as m
inner join wca.dbo.zrconv as z on m.rconvID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zrconvid = (select top 1 zrconvid from wca.dbo.zrconv as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.rdnom (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldDenomination1, OldDenomination2, OldDenomination3, OldDenomination4, OldDenomination5, OldDenomination6,
OldDenomination7, OldMinimumDenomination, OldDenominationMultiple, NewDenomination1, NewDenomination2, NewDenomination3,
NewDenomination4, NewDenomination5, NewDenomination6, NewDenomination7, NewMinimumDenomination, NewDenominationMultiple, Notes, RdnomID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldDenomination1, OldDenomination2, OldDenomination3, OldDenomination4, OldDenomination5, OldDenomination6,
OldDenomination7, OldMinimumDenomination, OldDenominationMultiple, NewDenomination1, NewDenomination2, NewDenomination3,
NewDenomination4, NewDenomination5, NewDenomination6, NewDenomination7, NewMinimumDenomination, NewDenominationMultiple, zRdnomNotes, EfdtLinkID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zrdnom as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select rdnomid from wca.dbo.rdnom)
and zrdnomid = (select top 1 zrdnomid from wca.dbo.zrdnom as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldDenomination1=z.OldDenomination1, m.OldDenomination2=z.OldDenomination2,
m.OldDenomination3=z.OldDenomination3, m.OldDenomination4=z.OldDenomination4, m.OldDenomination5=z.OldDenomination5,
m.OldDenomination6=z.OldDenomination6, m.OldDenomination7=z.OldDenomination7, m.OldMinimumDenomination=z.OldMinimumDenomination,
m.OldDenominationMultiple=z.OldDenominationMultiple, m.Notes=z.zRdnomNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.rdnom as m
inner join wca.dbo.zrdnom as z on m.rdnomID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zrdnomid = (select top 1 zrdnomid from wca.dbo.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewDenomination1=z.NewDenomination1, m.NewDenomination2=z.NewDenomination2,
m.NewDenomination3=z.NewDenomination3, m.NewDenomination4=z.NewDenomination4, m.NewDenomination5=z.NewDenomination5,
m.NewDenomination6=z.NewDenomination6, m.NewDenomination7=z.NewDenomination7, m.NewMinimumDenomination=z.NewMinimumDenomination,
m.NewDenominationMultiple=z.NewDenominationMultiple, m.Notes=z.zRdnomNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.rdnom as m
inner join wca.dbo.zrdnom as z on m.rdnomID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zrdnomid = (select top 1 zrdnomid from wca.dbo.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.rdnom as m
inner join wca.dbo.zrdnom as z on m.rdnomID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zrdnomid = (select top 1 zrdnomid from wca.dbo.zrdnom as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.scchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, DateofChange, SecOldName, SecNewName, EventType, ScChgID, ScChgNotes, OldSectyCD, NewSectyCD, OldRegS144A, NewRegS144A, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, DateofChange, SecOldName, SecNewName, EventType, EfdtLinkID, zScChgNotes, OldSectyCD, NewSectyCD, OldRegS144A, NewRegS144A, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zscchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select scchgid from wca.dbo.scchg)
and zscchgid = (select top 1 zscchgid from wca.dbo.zscchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.DateofChange=z.DateofChange, m.SecOldName=z.SecOldName, m.EventType=z.EventType, m.ScChgNotes=z.zScChgNotes, 
m.OldSectyCD=z.OldSectyCD, m.OldRegS144A=z.OldRegS144A, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.scchg as m
inner join wca.dbo.zscchg as z on m.scchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zscchgid = (select top 1 zscchgid from wca.dbo.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.DateofChange=z.DateofChange, m.SecNewName=z.SecNewName, m.EventType=z.EventType, m.ScChgNotes=z.zScChgNotes,
m.NewSectyCD=z.NewSectyCD, m.NewRegS144A=z.NewRegS144A, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.scchg as m
inner join wca.dbo.zscchg as z on m.scchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zscchgid = (select top 1 zscchgid from wca.dbo.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.scchg as m
inner join wca.dbo.zscchg as z on m.scchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zscchgid = (select top 1 zscchgid from wca.dbo.zscchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.shoch (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldSos, NewSos, ShochID, ShochNotes, EventType, RelEventID, OldOutstandingDate, NewOutstandingDate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecID, EffectiveDate, OldSos, NewSos, EfdtLinkID, zShochNotes, EventType, RelEventID, OldOutstandingDate, NewOutstandingDate, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.zshoch as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select shochid from wca.dbo.shoch)
and zshochid = (select top 1 zshochid from wca.dbo.zshoch as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.OldSos=z.OldSos, m.ShochNotes=z.zShochNotes, m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.OldOutstandingDate=z.OldOutstandingDate, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.shoch as m
inner join wca.dbo.zshoch as z on m.shochID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zshochid = (select top 1 zshochid from wca.dbo.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecID=z.SecID, m.EffectiveDate=z.EffectiveDate, m.NewSos=z.NewSos, m.ShochNotes=z.zShochNotes, m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.NewOutstandingDate=z.NewOutstandingDate,
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.shoch as m
inner join wca.dbo.zshoch as z on m.shochID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.zshochid = (select top 1 zshochid from wca.dbo.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.shoch as m
inner join wca.dbo.zshoch as z on m.shochID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.zshochid = (select top 1 zshochid from wca.dbo.zshoch as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
--------------------------------------------------------------------------
INSERT INTO wca.dbo.trchg (OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldTreasurySharesDate, NewTreasurySharesDate, OldTreasuryShares, NewTreasuryShares, EventType, RelEventID, Notes, TrchgID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate)
SELECT OpsTime, Opsflag, Acttime, Actflag, GCDateTime,
SecId, EffectiveDate, OldTreasurySharesDate, NewTreasurySharesDate, OldTreasuryShares, NewTreasuryShares, EventType, RelEventID, zTrchgNotes, EfdtLinkID, 
Source, SourceDate, GCAction, GCDate, GCTime, AnnounceDate
FROM wca.dbo.ztrchg as main
where
main.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and main.actflag<>'D'
and efdtlinkid not in (select trchgid from wca.dbo.trchg)
and ztrchgid = (select top 1 ztrchgid from wca.dbo.ztrchg as sub 
                where main.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag,
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.OldTreasurySharesDate=z.OldTreasurySharesDate, m.OldTreasuryShares=z.OldTreasuryShares,m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.Notes=z.zTrchgNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.trchg as m
inner join wca.dbo.ztrchg as z on m.trchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.ztrchgid = (select top 1 ztrchgid from wca.dbo.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid)
UPDATE m
set m.Acttime=(select max(acttime) from wca.dbo.tbl_opslog),
m.SecId=z.SecId, m.EffectiveDate=z.EffectiveDate, m.NewTreasurySharesDate=z.NewTreasurySharesDate, m.NewTreasuryShares=z.NewTreasuryShares, m.EventType=z.EventType, m.RelEventID=z.RelEventID, m.Notes=z.zTrchgNotes, 
m.Source=z.Source, m.SourceDate=z.SourceDate, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime, m.AnnounceDate=z.AnnounceDate
FROM wca.dbo.trchg as m
inner join wca.dbo.ztrchg as z on m.trchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and z.actflag<>'D'
and z.ztrchgid = (select top 1 ztrchgid from wca.dbo.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'D'
                order by sub.serialid desc)
UPDATE m
set m.Acttime=z.Acttime, m.Actflag=z.Actflag, m.GCAction=z.GCAction, m.GCDate=z.GCDate, m.GCTime=z.GCTime
FROM wca.dbo.trchg as m
inner join wca.dbo.ztrchg as z on m.trchgID=z.EfdtLinkID
where
z.acttime>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and 0 = (select count(*) from wca.dbo.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                  and sub.actflag<>'C'
                  and sub.actflag<>'D')
and z.ztrchgid = (select top 1 ztrchgid from wca.dbo.ztrchg as sub 
                where z.efdtlinkid=sub.efdtlinkid
                order by sub.serialid desc)
