--filepath=o:\smfsql\fe\
--filenameprefix=
--filename=YYYYMMDD
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=
--archivepath=
--fieldheaders=
--filetidy=
--fwoffsets=
--incremental=
--shownulls=



--# 1
use smf4
SELECT
Security.Sedol,
Security.Isin,
Issuer.Issuername,
Security.Longdesc,
Issuer.Cinccode,
Issuer.ICBIndustry as Induscode,
Security.PVCurrenCD as Currcode,
Security.Actflag,
Security.Actdate,
Market.TIDM as Tidisplay
FROM Security 
INNER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
left outer join Market on security.securityID = Market.securityID
            and Market.MIC = 'XLON'
WHERE Security.Actflag <> 'D'
AND (Security.Statusflag <> 'D'
   OR Security.Actdate BETWEEN GETDATE() - 90 AND GETDATE())
AND Security.SecType <> 'CB'
AND Security.SecType <> 'CC'
AND Security.SecType <> 'CM'
AND Security.SecType <> 'CN'

