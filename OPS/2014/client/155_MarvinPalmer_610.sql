--filepath=o:\upload\acc\155\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt= 
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\155\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA2
SELECT * 
FROM t610_Company_Meeting 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 2
USE WCA2
SELECT *
FROM t610_Call
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 3
USE WCA2
SELECT * 
FROM t610_Liquidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 4
USE WCA2
SELECT *
FROM t610_Certificate_Exchange
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 5
USE WCA2
SELECT * 
FROM t610_International_Code_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 6
USE WCA2
SELECT * 
FROM t610_Preference_Conversion
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 7
use WCA2
SELECT * 
FROM t610_Preference_Redemption
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 8
use WCA2
SELECT * 
FROM t610_Security_Reclassification
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 9
use WCA2
SELECT * 
FROM t610_Lot_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 10
use WCA2
SELECT * 
FROM t610_Sedol_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 11
use WCA2
SELECT * 
FROM t610_Buy_Back
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 12
use WCA2
SELECT * 
FROM t610_Capital_Reduction
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 13
use WCA2
SELECT * 
FROM t610_Takeover
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 14
use WCA2
SELECT * 
FROM t610_Arrangement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 15
use WCA2
SELECT * 
FROM t610_Bonus
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 16
use WCA2
SELECT * 
FROM t610_Bonus_Rights
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 17
use WCA2
SELECT * 
FROM t610_Consolidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 18
use WCA2
SELECT * 
FROM t610_Demerger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 19
use WCA2
SELECT * 
FROM t610_Distribution
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 20
use WCA2
SELECT * 
FROM t610_Divestment
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 21
use WCA2
SELECT * 
FROM t610_Entitlement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 22
use WCA2
SELECT * 
FROM t610_Merger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 23
use WCA2
SELECT * 
FROM t610_Preferential_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 24
use WCA2
SELECT * 
FROM t610_Purchase_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 25
use WCA2
SELECT * 
FROM t610_Rights 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 26
use WCA2
SELECT * 
FROM t610_Security_Swap 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 27
use WCA2
SELECT *
FROM t610_Subdivision
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 28
use WCA2
SELECT *
FROM t610_Bankruptcy 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 29
use WCA2
SELECT *
FROM t610_Financial_Year_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 30
use WCA2
SELECT *
FROM t610_Incorporation_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 31
use WCA2
SELECT *
FROM t610_Issuer_Name_change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 32
use WCA2
SELECT *
FROM t610_Lawsuit
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 33
use WCA2
SELECT *
FROM t610_Security_Description_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 34
use WCA2
SELECT *
FROM t610_Assimilation
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 35
use WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 36
use WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 37
use WCA2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 38
use WCA2
SELECT * 
FROM t610_Announcement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 39
use WCA2
SELECT * 
FROM t610_Parvalue_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 40
USE WCA2
SELECT * 
FROM t610_Currency_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 41
USE WCA2
SELECT * 
FROM t610_Return_of_Capital 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))


--# 42
USE WCA2
SELECT * 
FROM t610_Dividend
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 43
USE WCA2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))

--# 44
USE WCA2
SELECT * 
FROM t610_Franking
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='U'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='U'))


--# 51
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v50f_610_Company_Meeting 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Company_Meeting 


--# 52
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v53f_610_Call
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Call 

--# 53
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v50f_610_Liquidation
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Liquidation 

--# 54
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v51f_610_Certificate_Exchange
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Certificate_Exchange 

--# 55
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v51f_610_International_Code_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_International_Code_Change 

--# 56
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v51f_610_Preference_Conversion
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Preference_Conversion 

--# 57
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v51f_610_Preference_Redemption
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Preference_Redemption 

--# 58
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v51f_610_Preference_Redemption
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Preference_Redemption 

--# 59
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v52f_610_Lot_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v52f_610_Lot_Change 

--# 60
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v52f_610_Sedol_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v52f_610_Sedol_Change 

--# 61
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v53f_610_Buy_Back
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Buy_Back 

--# 62
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v53f_610_Capital_Reduction
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Capital_Reduction 

--# 63
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v53f_610_Takeover
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Takeover 

--# 64
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Arrangement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Arrangement 

--# 65
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Bonus
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Bonus 

--# 66
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Bonus_Rights
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Bonus_Rights 

--# 67
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Consolidation
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Consolidation 

--# 68
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Demerger
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Demerger 

--# 69
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Distribution
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Distribution 

--# 70
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Divestment
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Divestment 

--# 71
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Entitlement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Entitlement 

--# 72
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Merger
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Merger 

--# 73
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Preferential_Offer
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Preferential_Offer 

--# 74
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Purchase_Offer
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Purchase_Offer 

--# 75
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Rights 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Rights 

--# 76
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Security_Swap 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Security_Swap 

--# 77
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v54f_610_Subdivision
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Subdivision 

--# 78
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v50f_610_Bankruptcy 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Bankruptcy 

--# 79
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v50f_610_Financial_Year_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Financial_Year_Change 

--# 80
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v50f_610_Incorporation_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Incorporation_Change 

--# 81
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v50f_610_Issuer_Name_change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Issuer_Name_change 

--# 82
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v50f_610_Lawsuit
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Lawsuit 

--# 83
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v51f_610_Security_Description_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Security_Description_Change 

--# 84
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT *
FROM v52f_610_Assimilation
WHERE (CHANGED > getdate()-183)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v52f_610_Assimilation 

--# 85
use WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE (CHANGED > getdate()+1)

--# 86
use WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE (CHANGED > getdate()+1)

--# 87
use WCA2
SELECT *
FROM t610_New_Listing
WHERE (CHANGED > getdate()+1)

--# 88
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v50f_610_Announcement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v50f_610_Announcement 

--# 89
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v51f_610_Parvalue_Redenomination 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Parvalue_Redenomination 

--# 90
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v51f_610_Currency_Redenomination 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v51f_610_Currency_Redenomination 

--# 91
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v53f_610_Return_of_Capital 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v53f_610_Return_of_Capital 


--# 92
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Dividend
WHERE 
(sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
and (CHANGED > getdate()-183)
AND  (PAYDATE > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
end
else
SELECT top 0 * FROM v54f_610_Dividend 

--# 93
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Dividend_Reinvestment_Plan
WHERE
(sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
and (CHANGED > getdate()-183)
AND  (PAYDATE > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
end
else
SELECT top 0 * FROM v54f_610_Dividend_Reinvestment_Plan 

--# 94
if ((select count(actflag) from client.dbo.pfsedol where actflag = 'I' and accid=155) > 0
    or (select count(actflag) from client.dbo.pfuscode where actflag = 'I' and accid=155) > 0)
begin
USE WCA
SELECT * 
FROM v54f_610_Franking
WHERE (CHANGED > getdate()-183)
AND  (PAYDATE > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=155 and actflag='I'))
     or (uscode in (select code from client.dbo.pfuscode where accid=155 and actflag='I'))
end
else
SELECT top 0 * FROM v54f_610_Franking 
