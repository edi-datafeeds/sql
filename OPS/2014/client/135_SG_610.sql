--filepath=o:\Upload\Acc\135\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldsepaator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\135\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA2
SELECT * 
FROM t610_Company_Meeting 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 2
USE WCA2
SELECT *
FROM t610_Call
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 3
USE WCA2
SELECT * 
FROM t610_Liquidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA2
SELECT *
FROM t610_Certificate_Exchange
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 5
USE WCA2
SELECT * 
FROM t610_International_Code_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 6
USE WCA2
SELECT * 
FROM t610_Preference_Conversion
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 7
USE WCA2
SELECT * 
FROM t610_Preference_Redemption
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 8
USE WCA2
SELECT * 
FROM t610_Security_Reclassification
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 9
USE WCA2
SELECT * 
FROM t610_Lot_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 10
USE WCA2
SELECT * 
FROM t610_Sedol_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 11
USE WCA2
SELECT * 
FROM t610_Buy_Back
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 12
USE WCA2
SELECT * 
FROM t610_Capital_Reduction
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 13
USE WCA2
SELECT * 
FROM t610_Takeover
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 14
USE WCA2
SELECT * 
FROM t610_Arrangement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 15
USE WCA2
SELECT * 
FROM t610_Bonus
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 16
USE WCA2
SELECT * 
FROM t610_Bonus_Rights
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 17
USE WCA2
SELECT * 
FROM t610_Consolidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 18
USE WCA2
SELECT * 
FROM t610_Demerger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 19
USE WCA2
SELECT * 
FROM t610_Distribution
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 20
USE WCA2
SELECT * 
FROM t610_Divestment
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 21
USE WCA2
SELECT * 
FROM t610_Entitlement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 22
USE WCA2
SELECT * 
FROM t610_Merger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 23
USE WCA2
SELECT * 
FROM t610_Preferential_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 24
USE WCA2
SELECT * 
FROM t610_Purchase_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 25
USE WCA2
SELECT * 
FROM t610_Rights 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 26
USE WCA2
SELECT * 
FROM t610_Security_Swap 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 27
USE WCA2
SELECT *
FROM t610_Subdivision
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 28
USE WCA2
SELECT *
FROM t610_Bankruptcy 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 29
USE WCA2
SELECT *
FROM t610_Financial_Year_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 30
USE WCA2
SELECT *
FROM t610_Incorporation_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 31
USE WCA2
SELECT *
FROM t610_Issuer_Name_change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 32
USE WCA2
SELECT *
FROM t610_Lawsuit
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 33
USE WCA2
SELECT *
FROM t610_Security_Description_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 34
USE WCA2
SELECT *
FROM t610_Assimilation
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 35
USE WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 36
USE WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 37
USE WCA2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 38
USE WCA2
SELECT * 
FROM t610_Announcement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 39
USE WCA2
SELECT * 
FROM t610_Parvalue_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 40
USE WCA2
SELECT * 
FROM t610_Currency_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 41
USE WCA2
SELECT * 
FROM t610_Return_of_Capital 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol


--# 42
USE WCA2
SELECT ev.*
FROM t610_SG_Dividend as ev
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))
and (paydate>getdate()-183 or paydate is null)

ORDER BY EventID, ExchgCD, Sedol

--# 42
USE WCA2
SELECT ev.*
FROM t610_SG_reit_Dividend as ev
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))
and (paydate>getdate()-183 or paydate is null)
ORDER BY EventID, ExchgCD, Sedol



--# 44
USE WCA2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 45
USE WCA2
SELECT * 
FROM t610_Franking
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol


--# 51
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v50f_610_Company_Meeting 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v50f_610_Company_Meeting 

--# 52
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v53f_610_Call
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v53f_610_Call 

--# 53
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v50f_610_Liquidation
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID
end
else
SELECT top 0 * FROM v50f_610_Liquidation 

--# 54
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v51f_610_Certificate_Exchange
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_Certificate_Exchange 

--# 55
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_International_Code_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_International_Code_Change 

--# 56
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Preference_Conversion
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_Preference_Conversion 

--# 57
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Preference_Redemption
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_Preference_Redemption 

--# 58
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Security_Reclassification
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_Security_Reclassification 

--# 59
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v52f_610_Lot_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v52f_610_Lot_Change 

--# 60
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v52f_610_Sedol_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v52f_610_Sedol_Change 

--# 61
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Buy_Back
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID
end
else
SELECT top 0 * FROM v53f_610_Buy_Back 

--# 62
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Capital_Reduction
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v53f_610_Capital_Reduction 

--# 63
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Takeover
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID
end
else
SELECT top 0 * FROM v53f_610_Takeover 

--# 64
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Arrangement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Arrangement 

--# 65
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Bonus
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Bonus 

--# 66
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Bonus_Rights
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Bonus_Rights 

--# 67
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Consolidation
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Consolidation 

--# 68
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Demerger
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_610_Demerger 

--# 69
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Distribution
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_610_Distribution 

--# 70
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Divestment
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Divestment 

--# 71
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Entitlement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Entitlement 

--# 72
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Merger
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_610_Merger 

--# 73
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Preferential_Offer
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Preferential_Offer 

--# 74
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Purchase_Offer
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Purchase_Offer 

--# 75
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Rights 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Rights 

--# 76
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Security_Swap 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Security_Swap 

--# 77
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v54f_610_Subdivision
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Subdivision 

--# 78
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Bankruptcy 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v50f_610_Bankruptcy 

--# 79
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Financial_Year_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v50f_610_Financial_Year_Change 

--# 80
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Incorporation_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v50f_610_Incorporation_Change 

--# 81
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Issuer_Name_change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v50f_610_Issuer_Name_change 

--# 82
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v50f_610_Lawsuit
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v50f_610_Lawsuit 

--# 83
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v51f_610_Security_Description_Change
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_Security_Description_Change 

--# 84
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT *
FROM v52f_610_Assimilation
WHERE (CHANGED > getdate()-183)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v52f_610_Assimilation 

--# 85
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE (CHANGED > getdate()+1)
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM t610_Listing_Status_Change 

--# 86
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE (CHANGED > getdate()+1)
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM t610_Local_Code_Change 

--# 87
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v52f_610_New_Listing
WHERE (CHANGED > getdate()-183)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v52f_610_New_Listing 

--# 88
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v50f_610_Announcement
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v50f_610_Announcement 

--# 89
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Parvalue_Redenomination 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_Parvalue_Redenomination 

--# 90
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v51f_610_Currency_Redenomination 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v51f_610_Currency_Redenomination 

--# 91
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v53f_610_Return_of_Capital 
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v53f_610_Return_of_Capital 


--# 92
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT ev.* 
FROM v54f_610_SG_Dividend as ev
inner join wca.dbo.scmst on ev.secid = wca.dbo.scmst.secid
WHERE 
CHANGED > getdate()-183
and wca.dbo.scmst.structcd <> 'REIT'
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (paydate>getdate()-183 or paydate is null)
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_SG_Dividend 


--# 93
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT ev.* 
FROM v54f_610_SG_reit_Dividend as ev
inner join wca.dbo.scmst on ev.secid = wca.dbo.scmst.secid
WHERE 
CHANGED > getdate()-183
and wca.dbo.scmst.structcd = 'REIT'
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
and (paydate>getdate()-183 or paydate is null)
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_SG_reit_Dividend



--# 94
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Dividend_Reinvestment_Plan
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Dividend_Reinvestment_Plan 


--# 95
if (select count(actflag)  from client.dbo.pfsedol where actflag = 'I' and accid = 135) > 0
begin
USE WCA
SELECT * 
FROM v54f_610_Franking
WHERE (CHANGED > getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from client.dbo.pfsedol where accid=135 and actflag='I'))
ORDER BY EventID, ExchgCD, Sedol
end
else
SELECT top 0 * FROM v54f_610_Franking 
