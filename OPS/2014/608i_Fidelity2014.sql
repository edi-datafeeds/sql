-- fpx=
-- fdt=SELECT CONVERT(VARCHAR(10),MAX(wca.dbo.tbl_opslog.Feeddate),112) FROM wca.dbo.tbl_opslog 
-- fsx=SELECT TOP 1 CONCAT('_', seq ) FROM wca.dbo.tbl_opslog ORDER BY acttime DESC
-- hpx=EDI_REORG
-- hdt=SELECT CONVERT(VARCHAR(10),MAX(wca.dbo.tbl_opslog.Feeddate),112) FROM wca.dbo.tbl_opslog
-- hsx=SELECT TOP 1 CONCAT('_', seq ) FROM wca.dbo.tbl_opslog ORDER BY acttime DESC
-- eof=EDI_ENDOFFILE
-- fex=.608
-- fty=y
-- arc=n

--# 1
USE wca
SELECT v51f_608_International_Code_Change.* 
FROM v51f_608_International_Code_Change
LEFT OUTER JOIN DPRCP on v51f_608_International_Code_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)

--# 2
USE wca
SELECT v52f_608_sedol_Change.* 
FROM v52f_608_sedol_Change
LEFT OUTER JOIN DPRCP on v52f_608_sedol_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
