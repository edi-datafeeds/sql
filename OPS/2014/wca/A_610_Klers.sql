--filepath=o:\datafeed\wca\610_Klers\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\610_Klers\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
USE WCA2
SELECT t610_Company_Meeting.* 
FROM t610_Company_Meeting
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 2
USE WCA2
SELECT t610_Call.*
FROM t610_Call
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 3
USE WCA2
SELECT t610_Liquidation.* 
FROM t610_Liquidation
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA2
SELECT t610_Certificate_Exchange.*
FROM t610_Certificate_Exchange
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 5
USE WCA2
SELECT t610_International_Code_Change.* 
FROM t610_International_Code_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 6
USE WCA2
SELECT t610_Preference_Conversion.* 
FROM t610_Preference_Conversion
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 7
USE WCA2
SELECT t610_Preference_Redemption.* 
FROM t610_Preference_Redemption
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 8
USE WCA2
SELECT t610_Security_Reclassification.* 
FROM t610_Security_Reclassification
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 9
USE WCA2
SELECT t610_Lot_Change.* 
FROM t610_Lot_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 10
USE WCA2
SELECT t610_Sedol_Change.* 
FROM t610_Sedol_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 11
USE WCA2
SELECT t610_Buy_Back.* 
FROM t610_Buy_Back
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 12
USE WCA2
SELECT t610_Capital_Reduction.* 
FROM t610_Capital_Reduction
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 14
USE WCA2
SELECT t610_Takeover.* 
FROM t610_Takeover
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 15
USE WCA2
SELECT t610_Arrangement.* 
FROM t610_Arrangement
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 16
USE WCA2
SELECT t610_Bonus.* 
FROM t610_Bonus
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 17
USE WCA2
SELECT t610_Bonus_Rights.* 
FROM t610_Bonus_Rights
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 18
USE WCA2
SELECT t610_Consolidation.* 
FROM t610_Consolidation
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 19
USE WCA2
SELECT t610_Demerger.* 
FROM t610_Demerger
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 20
USE WCA2
SELECT t610_Distribution.* 
FROM t610_Distribution
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 21
USE WCA2
SELECT t610_Divestment.* 
FROM t610_Divestment
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 22
USE WCA2
SELECT t610_Entitlement.* 
FROM t610_Entitlement
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 23
USE WCA2
SELECT t610_Merger.* 
FROM t610_Merger
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 24
USE WCA2
SELECT t610_Preferential_Offer.* 
FROM t610_Preferential_Offer
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 25
USE WCA2
SELECT t610_Purchase_Offer.* 
FROM t610_Purchase_Offer
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 26
USE WCA2
SELECT t610_Rights.* 
FROM t610_Rights 
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 27
USE WCA2
SELECT t610_Security_Swap.* 
FROM t610_Security_Swap 
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 28
USE WCA2
SELECT t610_Subdivision.*
FROM t610_Subdivision
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 29
USE WCA2
SELECT t610_Bankruptcy.*
FROM t610_Bankruptcy 
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 30
USE WCA2
SELECT t610_Financial_Year_Change.*
FROM t610_Financial_Year_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 31
USE WCA2
SELECT t610_Incorporation_Change.*
FROM t610_Incorporation_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 32
USE WCA2
SELECT t610_Issuer_Name_change.*
FROM t610_Issuer_Name_change
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 33
USE WCA2
SELECT t610_Lawsuit.*
FROM t610_Lawsuit
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 34
USE WCA2
SELECT t610_Security_Description_Change.*
FROM t610_Security_Description_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 35
USE WCA2
SELECT t610_Assimilation.*
FROM t610_Assimilation
inner join continent on ExCountry = Continent.CntryCD
WHERE 
(SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 36
USE WCA2
SELECT t610_Listing_Status_Change.*
FROM t610_Listing_Status_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 37
USE WCA2
SELECT t610_Local_Code_Change.*
FROM t610_Local_Code_Change
inner join continent on ExCountry = Continent.CntryCD
WHERE 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 38
USE WCA2
SELECT t610_New_Listing.* 
FROM t610_New_Listing
inner join continent on ExCountry = Continent.CntryCD
WHERE 
(SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 39
USE WCA2
SELECT t610_Announcement.* 
FROM t610_Announcement
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 40
USE WCA2
SELECT t610_Parvalue_Redenomination.* 
FROM t610_Parvalue_Redenomination 
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 41
USE WCA2
SELECT t610_Currency_Redenomination.* 
FROM t610_Currency_Redenomination 
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 42
USE WCA2
SELECT t610_Return_of_Capital.* 
FROM t610_Return_of_Capital 
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) AND (rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol

--# 43
USE WCA2
SELECT t610_Dividend.* 
FROM t610_Dividend
inner join continent on ExCountry = Continent.CntryCD
WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
AND (rtrim(upper(Continent.Continent)) = 'EUROPE' OR rtrim(upper(Continent.Continent)) = 'ASIA')
ORDER BY EventID, ExchgCD, Sedol
