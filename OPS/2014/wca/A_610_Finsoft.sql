--filepath=o:\Datafeed\WCA\610_Finsoft\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\610_Finsoft\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA2
SELECT * 
FROM t610_Company_Meeting 

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 2
USE WCA2
SELECT *
FROM t610_Call

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 3
USE WCA2
SELECT * 
FROM t610_Liquidation

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA2
SELECT *
FROM t610_Certificate_Exchange

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 5
USE WCA2
SELECT * 
FROM t610_International_Code_Change

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 6
USE WCA2
SELECT * 
FROM t610_Preference_Conversion

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 7
USE WCA2
SELECT * 
FROM t610_Preference_Redemption

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 8
USE WCA2
SELECT * 
FROM t610_Security_Reclassification

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 9
USE WCA2
SELECT * 
FROM t610_Lot_Change

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 10
USE WCA2
SELECT * 
FROM t610_Sedol_Change

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 11
USE WCA2
SELECT * 
FROM t610_Buy_Back

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 12
USE WCA2
SELECT * 
FROM t610_Capital_Reduction

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 14
USE WCA2
SELECT * 
FROM t610_Takeover

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 15
USE WCA2
SELECT * 
FROM t610_Arrangement

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 16
USE WCA2
SELECT * 
FROM t610_Bonus

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 17
USE WCA2
SELECT * 
FROM t610_Bonus_Rights

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 18
USE WCA2
SELECT * 
FROM t610_Consolidation

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 19
USE WCA2
SELECT * 
FROM t610_Demerger

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 20
USE WCA2
SELECT * 
FROM t610_Distribution

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 21
USE WCA2
SELECT * 
FROM t610_Divestment

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 22
USE WCA2
SELECT * 
FROM t610_Entitlement

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 23
USE WCA2
SELECT * 
FROM t610_Merger

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 24
USE WCA2
SELECT * 
FROM t610_Preferential_Offer

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 25
USE WCA2
SELECT * 
FROM t610_Purchase_Offer

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 26
USE WCA2
SELECT * 
FROM t610_Rights 

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 27
USE WCA2
SELECT * 
FROM t610_Security_Swap 

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 28
USE WCA2
SELECT *
FROM t610_Subdivision

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 29
USE WCA2
SELECT *
FROM t610_Bankruptcy 

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 30
USE WCA2
SELECT *
FROM t610_Financial_Year_Change

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 31
USE WCA2
SELECT *
FROM t610_Incorporation_Change

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 32
USE WCA2
SELECT *
FROM t610_Issuer_Name_change

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 33
USE WCA2
SELECT *
FROM t610_Lawsuit

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 34
USE WCA2
SELECT *
FROM t610_Security_Description_Change

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 35
USE WCA2
SELECT *
FROM t610_Assimilation

WHERE (ExCountry = 'US' or ExCountry = 'CA')
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 36
USE WCA2
SELECT *
FROM t610_Listing_Status_Change

WHERE (ExCountry = 'US' or ExCountry = 'CA')
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 37
USE WCA2
SELECT *
FROM t610_Local_Code_Change

WHERE (ExCountry = 'US' or ExCountry = 'CA')
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 38
USE WCA2
SELECT * 
FROM t610_New_Listing

WHERE (ExCountry = 'US' or ExCountry = 'CA')
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 39
USE WCA2
SELECT * 
FROM t610_Announcement

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 40
USE WCA2
SELECT * 
FROM t610_Parvalue_Redenomination 

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 41
USE WCA2
SELECT * 
FROM t610_Currency_Redenomination 

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 42
USE WCA2
SELECT * 
FROM t610_Return_of_Capital 

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 43
USE WCA2
SELECT * 
FROM t610_Dividend

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
AND ExCountry = 'AU'
ORDER BY EventID, ExchgCD, Sedol

--# 44
USE WCA2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
AND ExCountry = 'AU'
ORDER BY EventID, ExchgCD, Sedol

--# 45
USE WCA2
SELECT * 
FROM t610_Franking

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
AND ExCountry = 'AU'
ORDER BY EventID, ExchgCD, Sedol
