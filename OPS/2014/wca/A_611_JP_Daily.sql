--filepath=o:\Datafeed\WCA\611_JP\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.611
--suffix=
--fileheadertext=EDI_JP_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\WCA\611_JP\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA2
SELECT * 
FROM t610_Dividend

WHERE (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
and substring(exchgCD,1,2) = 'JP'
ORDER BY EventID, ExchgCD, Sedol

