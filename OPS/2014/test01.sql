-- fpx=
-- fdt=SELECT CONVERT(VARCHAR(10),MAX(wca.dbo.tbl_opslog.Feeddate),112) FROM wca.dbo.tbl_opslog 
-- fsx=SELECT TOP 1 CONCAT('_', seq ) FROM wca.dbo.tbl_opslog ORDER BY acttime DESC
-- hpx=EDI_REORG
-- hdt=SELECT CONVERT(VARCHAR(10),MAX(wca.dbo.tbl_opslog.Feeddate),112) FROM wca.dbo.tbl_opslog
-- hsx=SELECT TOP 1 CONCAT('_', seq ) FROM wca.dbo.tbl_opslog ORDER BY acttime DESC
-- eof=EDI_ENDOFFILE
-- fex=.608
-- fty=y
-- arc=n

--# 1
USE wca
SELECT 
Notes
FROM rum

--# 10
USE wca
SELECT v52f_608_sedol_Change.* 
FROM v52f_608_sedol_Change
LEFT OUTER JOIN DPRCP on v52f_608_sedol_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (v52f_608_sedol_Change.Newsedol <> '       ') AND (v52f_608_sedol_Change.Newsedol <> '') 
AND (v52f_608_sedol_Change.Newsedol is not null) 
AND (v52f_608_sedol_Change.Oldsedol<>v52f_608_sedol_Change.Newsedol) 
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
and relatedevent<>'merger'
and relatedevent<>'conversion'
and relatedevent<>'issuer name change'
and relatedevent<>'subdivision'
and relatedevent<>'consolidation'
and relatedevent<>'security swap'
and relatedevent<>'arrangement'
ORDER BY EventID, ExCountry, Exchange, isin