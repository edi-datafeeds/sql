--filepath=O:\Datafeed\WCA\608_not_us_ca\2014\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.608
--suffix=
--fileheadertext=EDI_REORG
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\608_not_us_ca\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq from wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT v50f_608_Company_Meeting.* 
FROM v50f_608_Company_Meeting 
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 2
USE WCA
SELECT v53f_608_Call.*
FROM v53f_608_Call
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 3
USE WCA
SELECT v50f_608_Liquidation.* 
FROM v50f_608_Liquidation
LEFT OUTER JOIN DPRCP on v50f_608_Liquidation.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin, OptionID, SerialID

--# 4
USE WCA
SELECT v51f_608_Certificate_Exchange.*
FROM v51f_608_Certificate_Exchange
LEFT OUTER JOIN DPRCP on v51f_608_Certificate_Exchange.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 5
USE WCA
SELECT v51f_608_International_Code_Change.* 
FROM v51f_608_International_Code_Change
LEFT OUTER JOIN DPRCP on v51f_608_International_Code_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (v51f_608_International_Code_Change.NewISIN <> '            ') 
AND (v51f_608_International_Code_Change.NewISIN <> '') 
AND (v51f_608_International_Code_Change.NewISIN is not null) 
AND (v51f_608_International_Code_Change.OldISIN<>v51f_608_International_Code_Change.NewISIN) 
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
and relatedevent<>'merger'
and relatedevent<>'conversion'
and relatedevent<>'issuer name change'
and relatedevent<>'subdivision'
and relatedevent<>'consolidation'
and relatedevent<>'security swap'
and relatedevent<>'arrangement'
ORDER BY EventID, ExCountry, Exchange, isin

--# 6
USE WCA
SELECT v51f_608_Preference_Conversion.* 
FROM v51f_608_Preference_Conversion
LEFT OUTER JOIN DPRCP on v51f_608_Preference_Conversion.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 7
use wca
SELECT v51f_608_Preference_Redemption.* 
FROM v51f_608_Preference_Redemption
LEFT OUTER JOIN DPRCP on v51f_608_Preference_Redemption.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 8
use wca
SELECT v51f_608_Security_Reclassification.* 
FROM v51f_608_Security_Reclassification
LEFT OUTER JOIN DPRCP on v51f_608_Security_Reclassification.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 9
use wca
SELECT v52f_608_Lot_Change.* 
FROM v52f_608_Lot_Change
LEFT OUTER JOIN DPRCP on v52f_608_Lot_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 10
use wca
SELECT v52f_608_sedol_Change.* 
FROM v52f_608_sedol_Change
LEFT OUTER JOIN DPRCP on v52f_608_sedol_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (v52f_608_sedol_Change.Newsedol <> '       ') AND (v52f_608_sedol_Change.Newsedol <> '') 
AND (v52f_608_sedol_Change.Newsedol is not null) 
AND (v52f_608_sedol_Change.Oldsedol<>v52f_608_sedol_Change.Newsedol) 
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
and relatedevent<>'merger'
and relatedevent<>'conversion'
and relatedevent<>'issuer name change'
and relatedevent<>'subdivision'
and relatedevent<>'consolidation'
and relatedevent<>'security swap'
and relatedevent<>'arrangement'
ORDER BY EventID, ExCountry, Exchange, isin

--# 11
use wca
SELECT v53f_608_Buy_Back.* 
FROM v53f_608_Buy_Back
LEFT OUTER JOIN DPRCP on v53f_608_Buy_Back.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin, OptionID, SerialID

--# 12
use wca
SELECT v53f_608_Capital_Reduction.* 
FROM v53f_608_Capital_Reduction
LEFT OUTER JOIN DPRCP on v53f_608_Capital_Reduction.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 14
use wca
SELECT v53f_608_Takeover.* 
FROM v53f_608_Takeover
LEFT OUTER JOIN DPRCP on v53f_608_Takeover.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin, OptionID, SerialID

--# 15
use wca
SELECT v54f_608_Arrangement.* 
FROM v54f_608_Arrangement
LEFT OUTER JOIN DPRCP on v54f_608_Arrangement.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 16
use wca
SELECT v54f_608_fidBonus.* 
FROM v54f_608_fidBonus
LEFT OUTER JOIN DPRCP on v54f_608_fidBonus.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 17
use wca
SELECT v54f_608_Bonus_Rights.* 
FROM v54f_608_Bonus_Rights
LEFT OUTER JOIN DPRCP on v54f_608_Bonus_Rights.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 18
use wca
SELECT v54f_608_Consolidation.* 
FROM v54f_608_Consolidation
LEFT OUTER JOIN DPRCP on v54f_608_Consolidation.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 19
use wca
SELECT v54f_608_Demerger.* 
FROM v54f_608_Demerger
LEFT OUTER JOIN DPRCP on v54f_608_Demerger.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin, OptionID, SerialID

--# 20
use wca
SELECT v54f_608_Distribution.* 
FROM v54f_608_Distribution
LEFT OUTER JOIN DPRCP on v54f_608_Distribution.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin, OptionID, SerialID

--# 21
use wca
SELECT v54f_608_Divestment.* 
FROM v54f_608_Divestment
LEFT OUTER JOIN DPRCP on v54f_608_Divestment.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 22
use wca
SELECT v54f_608_Entitlement.* 
FROM v54f_608_Entitlement
LEFT OUTER JOIN DPRCP on v54f_608_Entitlement.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 23
use wca
SELECT v54f_608_Merger.* 
FROM v54f_608_Merger
LEFT OUTER JOIN DPRCP on v54f_608_Merger.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin, OptionID, SerialID

--# 24
use wca
SELECT v54f_608_Preferential_Offer.* 
FROM v54f_608_Preferential_Offer
LEFT OUTER JOIN DPRCP on v54f_608_Preferential_Offer.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 25
use wca
SELECT v54f_608_Purchase_Offer.* 
FROM v54f_608_Purchase_Offer
LEFT OUTER JOIN DPRCP on v54f_608_Purchase_Offer.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 26
use wca
SELECT v54f_608_Rights.* 
FROM v54f_608_Rights 
LEFT OUTER JOIN DPRCP on v54f_608_Rights.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 27
use wca
SELECT v54f_608_Security_Swap.* 
FROM v54f_608_Security_Swap 
LEFT OUTER JOIN DPRCP on v54f_608_Security_Swap.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 28
use wca
SELECT v54f_608_Subdivision.*
FROM v54f_608_Subdivision
LEFT OUTER JOIN DPRCP on v54f_608_Subdivision.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin


/* These Views Not Required By Fidelity Align but are in the daily */
--# 29
use wca
SELECT *
FROM v50f_608_Bankruptcy 
LEFT OUTER JOIN DPRCP on v50f_608_Bankruptcy.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 30
use wca
SELECT *
FROM v50f_608_Financial_Year_Change
LEFT OUTER JOIN DPRCP on v50f_608_Financial_Year_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 31
use wca
SELECT *
FROM v50f_608_Incorporation_Change
LEFT OUTER JOIN DPRCP on v50f_608_Incorporation_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 32
use wca
SELECT *
FROM v50f_608_Issuer_Name_change
LEFT OUTER JOIN DPRCP on v50f_608_Issuer_Name_change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 33
use wca
SELECT *
FROM v50f_608_Lawsuit
LEFT OUTER JOIN DPRCP on v50f_608_Lawsuit.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 34
use wca
SELECT *
FROM v50f_608_Registered_Office_Change
LEFT OUTER JOIN DPRCP on v50f_608_Registered_Office_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 35
use wca
SELECT *
FROM v51f_608_Registrar_Change
LEFT OUTER JOIN DPRCP on v51f_608_Registrar_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 36
use wca
SELECT *
FROM v51f_608_Security_Description_Change
LEFT OUTER JOIN DPRCP on v51f_608_Security_Description_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 37
use wca
SELECT *
FROM v52f_608_Assimilation
LEFT OUTER JOIN DPRCP on v52f_608_Assimilation.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 38
use wca
SELECT *
FROM v52f_608_Listing_Status_Change
LEFT OUTER JOIN DPRCP on v52f_608_Listing_Status_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 39
use wca
SELECT *
FROM v52f_608_Local_Code_Change
LEFT OUTER JOIN DPRCP on v52f_608_Local_Code_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 40
use wca
SELECT * 
FROM v52f_608_New_Listing
LEFT OUTER JOIN DPRCP on v52f_608_New_Listing.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 42
use wca
SELECT * 
FROM v50f_608_Announcement
LEFT OUTER JOIN DPRCP on v50f_608_Announcement.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 43
use wca
SELECT * 
FROM v51f_608_Parvalue_Redenomination 
LEFT OUTER JOIN DPRCP on v51f_608_Parvalue_Redenomination.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 44
USE WCA
SELECT * 
FROM v51f_608_Currency_Redenomination 
LEFT OUTER JOIN DPRCP on v51f_608_Currency_Redenomination.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND ((RelatedEvent <> 'Clean' and RelatedEvent <> 'Correction') or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 45
USE WCA
SELECT * 
FROM v53f_608_Return_of_Capital 
LEFT OUTER JOIN DPRCP on v53f_608_Return_of_Capital.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

--# 46
USE WCA
SELECT * 
FROM v51f_608_Primary_Exchange_Change
LEFT OUTER JOIN DPRCP on v51f_608_Primary_Exchange_Change.SecID = DPRCP.Secid
WHERE CHANGED >= (select max(acttime)-0.05 from tbl_Opslog)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR SecStatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
ORDER BY EventID, ExCountry, Exchange, isin

