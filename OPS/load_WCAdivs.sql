print ""
go
print "STEP-1 delete table ahead"
go
print ""
go
use smf4
if exists (select * from sysobjects 
where name = 'WCADtemp')
delete from WCADtemp
go

print ""
go
print "STEP-2 Generate next WCA UK Dividend batch"
go
print ""
go

use wca
insert into smf4.dbo.WCADtemp
select * 
from v54f_620_dividend
where 
changed = (select max(acttime) from tbl_Opslog)
and exchgcd='GBLSE'
and optionid is not null
and optionid=1
and divtype = 'C'
and (sectycd = 'DR' or cntryofincorp<>'GB')

go


print ""
go
print "STEP-3 delete all unconfirmed EDICAOPTION records"
go
print ""
go
use smf4
delete edicaoption
from edicaoption
inner join edicadetail on edicaoption.securityid = edicadetail.securityid
                      and edicaoption.cadid = edicadetail.cadid
where exists 
(select
WCADtemp.eventid as CADID
from WCADtemp
where
edicadetail.CADID = WCADtemp.eventid
and edicadetail.SsnYear='WCAD'
and edicadetail.Status<>'S'
)
and edicaoption.distsedol='WCAD'



go

print ""
go
print "STEP-4 delete all unconfirmed EDICADETAIL records where not SENT"
go
print ""
go
use smf4
delete from edicadetail
where exists 
(select
WCADtemp.eventid as CADID
from WCADtemp
where
edicadetail.CADID = WCADtemp.eventid
and edicadetail.SsnYear='WCAD'
and edicadetail.Status<>'S'
)
go

print ""
go
print "STEP-5 insert EDICADETAIL records where key not found"
go
print ""
go
use smf4
insert into edicadetail
(Actflag, Actdate, CADID, SecurityID, AcceptDate, Closedate,
ExDate, PayDate, OsTaxrate, OsTaxCurr,
TaxRateInd, Confirmation, credate, ssnyear, DivType, DivFreq, Status, CAtype, PeriodStartDate, PeriodEnddate)
select
case when WCADtemp.Actflag = 'D' then WCADtemp.Actflag
     else WCADtemp.Actflag end as ActFlag1,
getdate(),
WCADtemp.eventid,
security.securityID,
WCADtemp.OptElectionDate,
WCADtemp.RecDate as Closedate,
WCADtemp.ExDate,
WCADtemp.PayDate,
WCADtemp.Taxrate,
WCADtemp.CurenCD,
'G' as TaxRateInd,
'C' as Confirmation,
WCADtemp.Created,
'WCAD' as ssnyear,
case when WCADtemp.Marker = 'FNL' then 'FINAL'
     when WCADtemp.Marker = 'INT' then 'INTERIM'
     when WCADtemp.Marker = 'SPL' then 'SPECIAL'
     else '' end as Divtype,
case when WCADtemp.Frequency = 'ANL' then 'ANNUAL'
     when WCADtemp.Frequency = 'SMA' then 'SEMI-ANNUAL'
     when WCADtemp.Frequency = 'QTR' then 'QUARTELY'
     when WCADtemp.Frequency = 'MNT' then 'MONTHLY'
     when WCADtemp.Frequency = 'SPL' then 'QUARTELY'
     else 'UNSPECIFIED' end as DivFreq,
CASE when WCADtemp.SectyCD = 'DR' or WCADtemp.Paydate is null then 'P' Else 'P' END as Status,
'DD' as CAType,
WCADtemp.FYEDate,
PeriodEnddate
from WCADtemp
inner join security on WCADtemp.sedol = security.sedol
where
not exists
(select edicadetail.securityID from edicadetail
where edicadetail.securityID = security.securityID
and edicadetail.CADID = WCADtemp.eventid
--and edicadetail.paydate <> WCADtemp.paydate
)
go

print ""
go
print "STEP-6 insert EDICAOPTION records where key not found"
go
print ""
go
use smf4
insert into smf4.dbo.edicaoption
(Actflag, Actdate, CADID, SecurityID, CashDistCallRate,
 OptionCurr, credate, distsedol, Ratetype, OptionType, Comment)
select 
case when WCADtemp.Actflag = 'D' then WCADtemp.Actflag
     else WCADtemp.Actflag end as ActFlag1,
getdate(),
WCADtemp.eventid,
security.securityID,
WCADtemp.GrossDividend,
WCADtemp.CurenCD,
WCADtemp.Created,
'WCAD' as distsedol,
'C' as Ratetype,
'DD' as optiontype,
case when WCADtemp.approxflag = 'T' then 'APPROXIMATE DIVIDEND' else '' end as Comment
from WCADtemp
inner join smf4.dbo.security on WCADtemp.sedol = security.sedol
where
not exists
(select edicaoption.cadid from edicaoption
where edicaoption.securityID = security.securityID
and edicaoption.CADID = WCADtemp.eventid
)
go
