--filepath=o:\datafeed\wca\611\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.611
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
USE WCA
SELECT * 
FROM v54f_610_Dividend

WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3) AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol
