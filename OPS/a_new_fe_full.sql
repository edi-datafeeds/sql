--filepath=o:\smfsql\fe\
--filenameprefix=
--filename=YYYYMMDD
--filenamealt=SELECT DATE_FORMAT( CURDATE() - INTERVAL 0 DAY, '%Y%m%d')
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=
--archivepath=
--fieldheaders=
--filetidy=
--fwoffsets=
--incremental=
--shownulls=

--# 1
SELECT
smf4.Security.Sedol,
smf4.Security.Isin,
smf4.Issuer.Issuername,
smf4.Security.Longdesc,
smf4.Issuer.Cinccode,
smf4.Issuer.ICBIndustry as Induscode,
smf4.Security.PVCurrenCD as Currcode,
smf4.Security.Actflag,
smf4.Security.Actdate,
smf4.Market.TIDM as Tidisplay
FROM smf4.Security 
INNER JOIN smf4.Issuer ON smf4.Security.IssuerID = smf4.Issuer.IssuerID
left outer join smf4.Market on smf4.Security.securityID = smf4.Market.securityID
                           and smf4.Market.MIC = 'XLON'
WHERE smf4.Security.Actflag <> 'D'
AND (smf4.Security.Statusflag <> 'D'
   OR smf4.Security.Actdate BETWEEN date_sub(now(), interval 90 day) AND now())
AND smf4.Security.SecType <> 'CB'
AND smf4.Security.SecType <> 'CC'
AND smf4.Security.SecType <> 'CM'
AND smf4.Security.SecType <> 'CN';
