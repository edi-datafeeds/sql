--filepath=o:\datafeed\wca\616\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.616
--suffix=
--fileheadertext=FULL_EDI_WCA_616_yyyymmdd
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\616\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


	
--# 1
use WCA
SELECT 
issur.Issuername,
issur.CntryofIncorp,
scmst.Acttime,
scmst.Actflag,
scmst.IssID,
scmst.SecID,
scmst.PrimaryExchgCD,
scmst.Securitydesc,
scmst.CurenCD,
scmst.Parvalue,
scmst.SectyCD,
scmst.Uscode,
scmst.Isin,
Ischg.IssOldname,
Ischg.IssNewname,
icc.oldisin,
icc.newisin,
icc.olduscode,
icc.newuscode
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN ischg ON scmst.IssID = Ischg.IssID
LEFT OUTER JOIN icc ON scmst.SecID = icc.SecID
where scmst.statusflag <> 'I'
and scmst.acttime>= (select max(acttime) from tbl_Opslog)
and scmst.actflag <> 'D'
