--filepath=o:\Datafeed\Finhols\F10\
--filenameprefix=OPENHRS_
--filename=yyyymmdd
--filenamealt=
--fileextension=.F10
--suffix=
--fileheadertext=EDI_FINHOLS_F10_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Finhols\F10\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use finhols
select
Opn_Id,
ActTime,
ActFlag,
Fin_Code,
Description,
DaysOfWeek,
OpenHrs,
CloseHrs,
Notes
from PubHol_Open_Hrs
where
FIN_CODE like '%BANK%'
and acttime>=(select max(FromDate) from FEEDLOG where FeedFreq = 'MORNING')
and acttime<(select max(ToDate) from FEEDLOG where FeedFreq = 'MORNING')
