--filepath=o:\Datafeed\Finhols\f03_markit\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.F03
--suffix=
--fileheadertext=EDI_FINHOLS_BESPOKE_F03_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use finhols
SELECT 
PubHol_Holidays.Hol_ID,
PubHol_Country.Country_Desc As CountryName , 
PubHol_Institution.DESCRIPTION As InstitutionName, 
PubHol_Exch_Map.ExchgCD,
case when PubHol_Holidays.FIN_CODE like '%BANK' then upper('BNK')
     when PubHol_Holidays.FIN_CODE like '%BANKS' then upper('BNK')
     when PubHol_Holidays.FIN_CODE like '%BANKCL' then upper('BCL')
     when PubHol_Holidays.FIN_CODE like '%CL' then upper('XCL')
     ELSE upper('EXC')
     end as InstTypeCD,
PubHol_Institution.Gmt_Offset, 
PubHol_Institution.SET_DET, 
CONVERT(char(10), PubHol_Holidays.hol_date, 6) AS HolidayDate, 
PubHol_Holidays.Hol_Type, 
PubHol_Holidays.HOL_NAME As HolidayName, 
PubHol_Open_Hrs.DaysOfWeek As OpeningDays, 
PubHol_Open_Hrs.OpenHrs, 
PubHol_Open_Hrs.CloseHrs 
FROM ((PubHol_Country 
INNER JOIN PubHol_Institution ON PubHol_Country.COUNTRY = PubHol_Institution.COUNTRY) 
INNER JOIN PubHol_Holidays ON PubHol_Institution.Fin_Code = PubHol_Holidays.FIN_CODE) 
INNER JOIN PubHol_Open_Hrs ON PubHol_Holidays.FIN_CODE = PubHol_Open_Hrs.Fin_Code
inner join PubHol_Exch_Map on PubHol_Holidays.FIN_CODE = PubHol_Exch_Map.Fin_Code
WHERE
exchgcd <> '' 
and exchgcd is not null
and PubHol_Holidays.hol_date  between '2008/08/01' and '2010/08/01'
and (PubHol_Institution.COUNTRY = 'AT'
or PubHol_Institution.COUNTRY = 'BE'
or PubHol_Institution.COUNTRY = 'BG'
or PubHol_Institution.COUNTRY = 'CH'
or PubHol_Institution.COUNTRY = 'CY'
or PubHol_Institution.COUNTRY = 'CZ'
or PubHol_Institution.COUNTRY = 'DE'
or PubHol_Institution.COUNTRY = 'DK'
or PubHol_Institution.COUNTRY = 'EE'
or PubHol_Institution.COUNTRY = 'ES'
or PubHol_Institution.COUNTRY = 'FI'
or PubHol_Institution.COUNTRY = 'FR'
or PubHol_Institution.COUNTRY = 'UK'
or PubHol_Institution.COUNTRY = 'GR'
or PubHol_Institution.COUNTRY = 'HU'
or PubHol_Institution.COUNTRY = 'IE'
or PubHol_Institution.COUNTRY = 'IS'
or PubHol_Institution.COUNTRY = 'IT'
or PubHol_Institution.COUNTRY = 'LT'
or PubHol_Institution.COUNTRY = 'LU'
or PubHol_Institution.COUNTRY = 'LV'
or PubHol_Institution.COUNTRY = 'MT'
or PubHol_Institution.COUNTRY = 'NL'
or PubHol_Institution.COUNTRY = 'NO'
or PubHol_Institution.COUNTRY = 'PL'
or PubHol_Institution.COUNTRY = 'PT'
or PubHol_Institution.COUNTRY = 'RO'
or PubHol_Institution.COUNTRY = 'SE'
or PubHol_Institution.COUNTRY = 'SI'
or PubHol_Institution.COUNTRY = 'SK')
Order By PubHol_Country.Country_Desc, PubHol_Institution.DESCRIPTION ASC, PubHol_Holidays.hol_date ASC
