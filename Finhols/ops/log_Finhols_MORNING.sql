use finhols2
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'MORNING')
set @tdate = getdate()

if @fdate<@tdate-0.8
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'MORNING', @fdate, @tdate)
    print "Log record creation successful"
  end
else
  print "Already run in last 16 hours !!!"
  go
  
