--filepath=o:\Datafeed\Finhols\F10\
--filenameprefix=HOLIDAYS_
--filename=yyyymmdd
--filenamealt=
--fileextension=.F10
--suffix=
--fileheadertext=EDI_FINHOLS_F10_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Finhols\F10\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use finhols
select
Hol_Id,
convert(varchar(30),hol_date, 111)
hol_date, 
FIN_CODE,
Hol_Type,
NOTES,
HOL_NAME,
ActTime,
ActFlag
from PubHol_Holidays
where
FIN_CODE like '%BANK%'
and acttime>=(select max(FromDate) from FEEDLOG where FeedFreq = 'MORNING')
and acttime<(select max(ToDate) from FEEDLOG where FeedFreq = 'MORNING')
