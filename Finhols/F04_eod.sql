--filepath=o:\Datafeed\Finhols\f04\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.F04
--suffix=
--fileheadertext=EDI_FINHOLS_F04_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Finhols\F04\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use finhols
select
pubhol_holidays.acttime,
pubhol_holidays.actflag,
pubhol_country.country_desc as countryname,
pubhol_institution.description as institutionname,
pubhol_holidays.fin_code,
case when pubhol_holidays.fin_code like '%bank' then upper('bnk')
     when pubhol_holidays.fin_code like '%banks' then upper('bnk')
     when pubhol_holidays.fin_code like '%bankcl' then upper('bcl')
     when pubhol_holidays.fin_code like '%cl' then upper('xcl')
     else upper('exc')
     end as insttypecd,
pubhol_institution.gmt_offset,
pubhol_institution.set_det,
pubhol_holidays.hol_date as holidaydate,
pubhol_holidays.hol_type,
case when pubhol_holidays.hol_name is null or upper(pubhol_holidays.hol_name)='NULL' then '' else pubhol_holidays.hol_name end as holidayname,
case when pubhol_open_hrs.daysofweek is null or upper(pubhol_open_hrs.daysofweek)='NULL' then '' else pubhol_open_hrs.daysofweek end as openingdays,
case when pubhol_open_hrs.openhrs is null or upper(pubhol_open_hrs.openhrs)='NULL' then '' else pubhol_open_hrs.openhrs end as openhrs,
case when pubhol_open_hrs.closehrs is null or upper(pubhol_open_hrs.closehrs)='NULL' then '' else pubhol_holidays.hol_name end as closehrs,
pubhol_open_hrs.notes as openingnotes,
pubhol_holidays.notes as holidaynotes,
pubhol_institution.notes as institutionnotes
from pubhol_country
inner join pubhol_institution on pubhol_country.country = pubhol_institution.country
inner join pubhol_holidays on pubhol_institution.fin_code = pubhol_holidays.fin_code
left outer join pubhol_open_hrs on pubhol_holidays.fin_code = pubhol_open_hrs.fin_code
where 
(pubhol_holidays.acttime > (select max(wca.dbo.tbl_opslog.Feeddate)-0.2 from wca.dbo.tbl_opslog where seq = 3)
  or pubhol_institution.acttime > (select max(wca.dbo.tbl_opslog.Feeddate)-0.2 from wca.dbo.tbl_opslog where seq = 3)
  or pubhol_open_hrs.acttime > (select max(wca.dbo.tbl_opslog.Feeddate)-0.2 from wca.dbo.tbl_opslog where seq = 3))
