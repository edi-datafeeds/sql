--filepath=o:\FINHOLS\ALIGN\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.hol
--suffix=
--fileheadertext=EDI_Finhols_
--fileheaderdate=YYYYMMDD
--datadateformat=YYYY/MM/DD
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=
--archivepath=
--fieldheaders=
--filetidy=
--fwoffsets=
--sevent=
--shownulls=


--# 1
use finhols
select
hol_date as HolidayDate,
pubhol_holidays.notes as HolidayNotes,
Hol_Name,
Country,
[Description],
pubhol_institution.notes as instit_notes
from pubhol_holidays
inner join pubhol_institution on pubhol_holidays.Fin_Code = pubhol_institution.fin_code
inner join PubHol_Exch_Map on pubhol_holidays.fin_Code = PubHol_Exch_Map.Fin_Code
inner join wca.dbo.exchg on PubHol_Exch_Map.exchgcd = wca.dbo.exchg.exchgcd
where hol_Date between getdate() and getdate()+100
and PubHol_Exch_Map.ExchgCD<>''
and pubhol_holidays.actflag<>'D'
ORDER BY hol_date
