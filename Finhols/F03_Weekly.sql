--filepath=o:\Datafeed\Finhols\F03\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.F03
--suffix=
--fileheadertext=EDI_FINHOLS_F03_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use finhols
SELECT 
PubHol_Country.Country_Desc As CountryName, 
PubHol_Institution.DESCRIPTION As InstitutionName, 
PubHol_Holidays.FIN_CODE,
case when PubHol_Holidays.FIN_CODE like '%BANK' then upper('BNK')
     when PubHol_Holidays.FIN_CODE like '%BANKS' then upper('BNK')
     when PubHol_Holidays.FIN_CODE like '%BANKCL' then upper('BCL')
     when PubHol_Holidays.FIN_CODE like '%CL' then upper('XCL')
     ELSE upper('EXC')
     end as InstTypeCD,
PubHol_Institution.Gmt_Offset, 
PubHol_Institution.SET_DET, 
CONVERT(char(10), PubHol_Holidays.hol_date, 6) AS HolidayDate, 
PubHol_Holidays.Hol_Type, 
PubHol_Holidays.HOL_NAME As HolidayName, 
PubHol_Open_Hrs.DaysOfWeek As OpeningDays, 
PubHol_Open_Hrs.OpenHrs, 
PubHol_Open_Hrs.CloseHrs, 
PubHol_Open_Hrs.Notes As OpeningNotes, 
PubHol_Holidays.NOTES AS HolidayNotes, 
PubHol_Institution.NOTES AS InstitutionNOTES
FROM ((PubHol_Country 
INNER JOIN PubHol_Institution ON PubHol_Country.COUNTRY = PubHol_Institution.COUNTRY) 
INNER JOIN PubHol_Holidays ON PubHol_Institution.Fin_Code = PubHol_Holidays.FIN_CODE) 
INNER JOIN PubHol_Open_Hrs ON PubHol_Holidays.FIN_CODE = PubHol_Open_Hrs.Fin_Code
WHERE (PubHol_Holidays.hol_date  between getdate()+2 and getdate()+29)
and (pubhol_holidays.actflag<>'D' or pubhol_holidays.acttime>getdate()-8)
Order By PubHol_Country.Country_Desc, PubHol_Institution.DESCRIPTION ASC, PubHol_Holidays.hol_date ASC
