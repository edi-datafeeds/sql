use portfolio
if exists (select * from sysobjects where name = 'CPOPT_Count')
 drop table CPOPT_Count
use wca
select cpopt.secid, count(secid) as ct
into portfolio.dbo.CPOPT_Count
from cpopt
GROUP BY cpopt.secid
order by count(secid)
go

use portfolio
ALTER TABLE CPOPT_Count ALTER COLUMN  secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_Count] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_Count] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 

	  use WFI
	  if exists (select * from sysobjects where name = 'schedule')
	    drop table schedule
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from schedule)
--	delete from schedule
--	where CpoptID in
--	(select CpoptID from wca.dbo.CPOPT
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.schedule

SELECT 
Case when callput = 'C' then 'Call' when callput = 'P' then 'Put' else CallPut end as Call_or_Put,
case when CPOPT.cptype='KO' then 'KO'
     when portfolio.dbo.CPOPT_Count.ct>1 then 'BM'
     when portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is not null then 'US'
     when portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is null then 'EU'
     else CPOPT.CPType end as Type,
case when MandatoryOptional='M' then 'Mandatory'
     when MandatoryOptional='O' then 'Optional'
     else MandatoryOptional end as Status, 
cpopt.Currency as Schedule_Currency, 
cpopt.Price as Schedule_Price,
cpopt.PriceAsPercent as Price_as_Percent,
case when CPOPT.cptype='KO' then 'Knock-out' 
     when (callput='C' and portfolio.dbo.CPOPT_Count.ct>1) 
     or (callput='C' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is null) then 'Callable Date' 
     when (callput='P' and portfolio.dbo.CPOPT_Count.ct>1) 
     or (callput='P' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is null) then 'Puttable Date' 
     when callput='C' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is not null  then 'Callable from' 
     when callput='P' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is not null  then 'Puttable from' 
     else CPOPT.CPType end as Date_1_Note,
FromDate as Date_1,
ToDate as To_Date,
NoticeFrom as Notice_From,
NoticeTo as Notice_To,
MinNoticeDays as Min_Notice_Days,
MaxNoticeDays as Max_Notice_Days,
'WCA.dbo.CPOPT|CpoptID|Notes' as Link_Notes,
CPOPT.Actflag,
CPOPT.Acttime,
CPOPT.AnnounceDate,
CPOPT.CpoptID,
CPOPT.SecID

	  into wfi.dbo.schedule

from CPOPT
left outer join portfolio.dbo.CPOPT_Count on cpopt.secid = portfolio.dbo.CPOPT_Count.secid

--	WHERE
--	CPOPT.Acttime > (select max(wfi.dbo.schedule.acttime) from wfi.dbo.schedule)

	  WHERE
	  cpopt.cptype<>''
	  and cpopt.cptype is not null

GO

	  use WFI 
	  ALTER TABLE schedule ALTER COLUMN  CpoptID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[schedule] WITH NOCHECK ADD 
	   CONSTRAINT [pk_schedule_CpoptID] PRIMARY KEY ([CpoptID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_schedule_SecID] ON [dbo].[schedule]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_schedule_Acttime] ON [dbo].[schedule]([Acttime] desc) ON [PRIMARY]
	  GO
