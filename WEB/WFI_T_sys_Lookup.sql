use WFI
if exists (select * from sysobjects where name = 'sys_lookup')
  drop table sys_lookup
GO

use WCA
SELECT
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
into WFI.dbo.sys_lookup
FROM LOOKUPEXTRA

union

SELECT
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD

union

SELECT
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY

union

--SELECT
--wfi.dbo.sys_exchg.Actflag,
--wfi.dbo.sys_exchg.Acttime,
--upper('EXCHANGE') as TypeGroup,
--wfi.dbo.sys_exchg.exchgCD as Code,
--wfi.dbo.sys_exchg.exchgname as Lookup
--from wfi.dbo.exchg

--union

SELECT
exchg.Actflag,
exchg.Acttime,
upper('MICCODE') as TypeGroup,
exchg.MIC as Code,
exchg.exchgname as Lookup
from exchg
where MIC <> '' and MIC is not null

union

--SELECT
--sys_exchg.Actflag,
--sys_exchg.Acttime,
--upper('MICMAP') as TypeGroup,
--sys_exchg.exchgCD as Code,
--sys_exchg.MIC as Lookup
--from sys_exchg

--union

SELECT
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
cast(Indus.IndusID as varchar(10)) as Code,
Indus.IndusName as Lookup
from INDUS

union

SELECT
Cntry.Actflag,
Cntry.Acttime,
upper('CNTRY') as TypeGroup,
Cntry.CntryCD as Code,
Cntry.Country as Lookup
from CNTRY

union

SELECT
Curen.Actflag,
Curen.Acttime,
upper('CUREN') as TypeGroup,
Curen.CurenCD as Code,
Curen.Currency as Lookup
from CUREN

union

SELECT
Event.Actflag,
Event.Acttime,
upper('EVENT') as TypeGroup,
Event.EventType as Code,
Event.EventName as Lookup
from EVENT

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2

use WFI 
ALTER TABLE sys_lookup ALTER COLUMN  TypeGroup char(10) NOT NULL
ALTER TABLE sys_lookup ALTER COLUMN  Code char(10) NOT NULL
ALTER TABLE sys_lookup ALTER COLUMN  Lookup varchar(70) NOT NULL
ALTER TABLE sys_lookup ALTER COLUMN  Actflag char(1) NOT NULL
GO 
use WFI 
ALTER TABLE [DBO].[sys_lookup] WITH NOCHECK ADD 
 CONSTRAINT [pk_sys_lookup] PRIMARY KEY ([Typegroup],[Code])  ON [PRIMARY]
GO

