"schedule","select
Case when callput = 'C' then 'Call' when callput = 'P' then 'Put' else CallPut end as Call_or_Put,
CPType as Call_Style,
case when MandatoryOptional='M' then 'Mandatory'
     when MandatoryOptional='O' then 'Optional'
     else MandatoryOptional end as Status, 
cpopt.Currency as Schedule_Currency, 
cpopt.Price as Schedule_Price,
cpopt.PriceAsPercent as Price_as_Percent,
FromDate as From_Date,
ToDate as To_Date,
NoticeFrom as Notice_From,
NoticeTo as Notice_To,
MinNoticeDays as Minimum_Notice_Days,
MaxNoticeDays as Maximum_Notice_Days,
'WCA.dbo.CPOPT|CpoptID|Notes' as Link_Notes,
CPOPT.Actflag,
CPOPT.Acttime,
CPOPT.AnnounceDate,
CPOPT.CpoptID as EventID,
scmst.secid
from CPOPT
inner join scmst on cpopt.secid = scmst.secid
"
