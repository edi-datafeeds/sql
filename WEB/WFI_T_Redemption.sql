	  use WFI
	  if exists (select * from sysobjects where name = 'redemption')
	    drop table redemption
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from redemption)
--	delete from redemption
--	where RedemID in
--	(select RedemID from wca.dbo.REDEM
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.redemption

SELECT 
case when REDEM.RedemDate = '1800/01/01' then null else REDEM.RedemDate end as Redemption_Date, 
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
case when REDEMTYPE.Lookup is null then REDEM.Redemtype else REDEMTYPE.Lookup end as Type,
case when indef.defaulttype<>'' then 'In default' else '' end as RedemDefault,
Poolfactor as Pool_Factor,
RedemPercent as Percentage_Redeemed,
REDEM.CurenCD as Redemption_Currency,
REDEM.AmountRedeemed as Amount_Redeemed,
REDEM.RedemPrice as Redemption_Price,
REDEM.Priceaspercent as Price_as_Percent,
REDEM.RedemPremium as Redemption_Premium,
REDEM.Premiumaspercent as Premium_as_Percent,
RD.Recdate as Record_Date,
INDEF.Notes as Default_Notes,
'WCA.dbo.REDEM|RedemID|RedemNotes' as Link_Notes,
REDEM.Actflag,
REDEM.Acttime,
REDEM.AnnounceDate,
REDEM.RedemID as EventID,
REDEM.SecID

	  into wfi.dbo.redemption

from REDEM
inner join bond on redem.secid = bond.secid
left outer join rd on redem.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDEM.Redemtype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup
left outer join indef on redem.secid = indef.secid and redem.redemdate = indef.defaultdate and 'REDDEFA' = indef.defaulttype

--	WHERE
--	REDEM.Acttime > (select max(wfi.dbo.redemption.acttime) from wfi.dbo.redemption)
--	and BOND.BondType <> 'PRF'
--	and redemtype<>'TENDER'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and REDEM.Actflag<>'D'
	  and redemtype<>'TENDER'

GO

	  use WFI 
	  ALTER TABLE redemption ALTER COLUMN  RedemID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[redemption] WITH NOCHECK ADD 
	   CONSTRAINT [pk_redemption_RedemID] PRIMARY KEY ([RedemID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_redemption_SecID] ON [dbo].[redemption]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_redemption_Acttime] ON [dbo].[redemption]([Acttime] desc) ON [PRIMARY]
	  GO

	  use WFI
	  if exists (select * from sysobjects where name = 'redemption_terms')
	    drop table redemption_terms
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from redemption_terms)
--	delete from redemption_terms
--	where EVENTID in
--	(select RedmtID from wca.dbo.REDMT
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.redemption_terms

SELECT 
RedemptionDate as Redemption_Date,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
case when REDEMTYPE.Lookup is null then REDMT.Redemptiontype else REDEMTYPE.Lookup end as Type,
REDMT.CurenCD as Redemption_Currency,
REDMT.RedemptionPrice as Redemption_Price,
REDMT.Priceaspercent as Price_as_Percent,
REDMT.RedemptionPremium as Redemption_Premium,
REDMT.Premiumaspercent as Premium_as_Percent,
REDMT.RedemptionAmount as Redemption_Amount,
REDMT.RedemInPercent as Redemption_in_Percent,
'WCA.dbo.REDMT|RedmtID|RedemtNotes' as Link_Notes,
REDMT.Actflag,
REDMT.Acttime,
REDMT.AnnounceDate,
REDMT.RedmtID as EventID,
REDMT.SecID

	  into wfi.dbo.redemption_terms

from REDMT
inner join bond on redmt.secid = bond.secid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDMT.redemptiontype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup

--	WHERE
--	REDMT.Acttime > (select max(wfi.dbo.redemption_terms.acttime) from wfi.dbo.redemption_terms)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and REDMT.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE redemption_terms ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[redemption_terms] WITH NOCHECK ADD 
	   CONSTRAINT [pk_redemption_terms_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_redemption_terms_SecID] ON [dbo].[redemption_terms]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_redemption_terms_Acttime] ON [dbo].[redemption_terms]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'conversion_terms')
	    drop table conversion_terms
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion_terms)
--	delete from conversion_terms
--	where EVENTID in
--	(select ConvtID from wca.dbo.CONVT
--	where acttime>@StartDate)
--	GO
