	  use WFI
	  if exists (select * from sysobjects where name = 'redemption')
	    drop table redemption
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from redemption)
--	delete from redemption
--	where RedemID in
--	(select RedemID from wca.dbo.REDEM
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.redemption

SELECT 
case when REDEM.RedemDate = '1800/01/01' then null else REDEM.RedemDate end as Redemption_Date, 
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
case when REDEMTYPE.Lookup is null then REDEM.Redemtype else REDEMTYPE.Lookup end as Type,
case when indef.defaulttype<>'' then 'In default' else '' end as RedemDefault,
Poolfactor as Pool_Factor,
RedemPercent as Percentage_Redeemed,
REDEM.CurenCD as Redemption_Currency,
REDEM.AmountRedeemed as Amount_Redeemed,
REDEM.RedemPrice as Redemption_Price,
REDEM.Priceaspercent as Price_as_Percent,
REDEM.RedemPremium as Redemption_Premium,
REDEM.Premiumaspercent as Premium_as_Percent,
RD.Recdate as Record_Date,
'WCA.dbo.INDEF|IndefID|Notes' as Link_Default_Notes,
'WCA.dbo.REDEM|RedemID|RedemNotes' as Link_Notes,
REDEM.Actflag,
REDEM.Acttime,
REDEM.AnnounceDate,
REDEM.RedemID as EventID,
REDEM.SecID

	  into wfi.dbo.redemption

from REDEM
inner join bond on redem.secid = bond.secid
left outer join rd on redem.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDEM.Redemtype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup
left outer join indef on redem.secid = indef.secid and redem.redemdate = indef.defaultdate and 'REDDEFA' = indef.defaulttype

--	WHERE
--	REDEM.Acttime > (select max(wfi.dbo.redemption.acttime) from wfi.dbo.redemption)
--	and BOND.BondType <> 'PRF'
--	and redemtype<>'TENDER'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and REDEM.Actflag<>'D'
	  and redemtype<>'TENDER'

GO

	  use WFI 
	  ALTER TABLE redemption ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[redemption] WITH NOCHECK ADD 
	   CONSTRAINT [pk_redemption_EventID] PRIMARY KEY ([EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_redemption_SecID] ON [dbo].[redemption]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_redemption_Acttime] ON [dbo].[redemption]([Acttime] desc) ON [PRIMARY]
	  GO

	  use WFI
	  if exists (select * from sysobjects where name = 'redemption_terms')
	    drop table redemption_terms
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from redemption_terms)
--	delete from redemption_terms
--	where EVENTID in
--	(select RedmtID from wca.dbo.REDMT
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.redemption_terms

SELECT 
RedemptionDate as Redemption_Date,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
case when REDEMTYPE.Lookup is null then REDMT.Redemptiontype else REDEMTYPE.Lookup end as Type,
REDMT.CurenCD as Redemption_Currency,
REDMT.RedemptionPrice as Redemption_Price,
REDMT.Priceaspercent as Price_as_Percent,
REDMT.RedemptionPremium as Redemption_Premium,
REDMT.Premiumaspercent as Premium_as_Percent,
REDMT.RedemptionAmount as Redemption_Amount,
REDMT.RedemInPercent as Redemption_in_Percent,
'WCA.dbo.REDMT|RedmtID|RedemtNotes' as Link_Notes,
REDMT.Actflag,
REDMT.Acttime,
REDMT.AnnounceDate,
REDMT.RedmtID as EventID,
REDMT.SecID

	  into wfi.dbo.redemption_terms

from REDMT
inner join bond on redmt.secid = bond.secid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDMT.redemptiontype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup

--	WHERE
--	REDMT.Acttime > (select max(wfi.dbo.redemption_terms.acttime) from wfi.dbo.redemption_terms)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and REDMT.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE redemption_terms ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[redemption_terms] WITH NOCHECK ADD 
	   CONSTRAINT [pk_redemption_terms_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_redemption_terms_SecID] ON [dbo].[redemption_terms]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_redemption_terms_Acttime] ON [dbo].[redemption_terms]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'conversion_terms')
	    drop table conversion_terms
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion_terms)
--	delete from conversion_terms
--	where EVENTID in
--	(select ConvtID from wca.dbo.CONVT
--	where acttime>@StartDate)
--	GO
	  use WFI
	  if exists (select * from sysobjects where name = 'reference')
	    drop table reference
	  go

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from reference)
--	delete from reference
--	where SecID in
--	(select SecID from wca.dbo.BOND
--	where acttime>@StartDate)
--	go

use wca
--	insert into wfi.dbo.reference
SELECT 
case when BOND.Municipal='Y' then 'Yes' when BOND.Municipal='N' then 'No' ELSE '' end as Municipal,
case when BOND.PrivatePlacement='Y' then 'Yes' when BOND.PrivatePlacement='N' then 'No' ELSE '' end as Private_Placement,
case when BOND.Syndicated='Y' then 'Yes' when BOND.Syndicated='N' then 'No' ELSE '' end as Syndicated,
case when BondType.Lookup is null then bond.bondtype else BondType.Lookup end as Security_Type, 
case when STRUCTCD.Lookup is null then SCMST.StructCD else STRUCTCD.Lookup end as Debt_Structure,
SCMST.Regs144a as Regulation_Code,
BOND.CurenCD as Current_Currency,
case when HOLDING.Lookup is null then SCMST.Holding else HOLDING.Lookup end as Holding,
BOND.ParValue as Nominal_Value, 
BOND.Denomination1 as Denom_1,
BOND.Denomination2 as Denom_2,
BOND.Denomination3 as Denom_3,
BOND.Denomination4 as Denom_4,
BOND.Denomination5 as Denom_5,
BOND.Denomination6 as Denom_6,
BOND.Denomination7 as Denom_7,
BOND.MinimumDenomination as Min_Denom, 
BOND.DenominationMultiple as Denom_Multiple, 
BOND.IssueDate as Issue_Date, 
BOND.IssueCurrency as Issue_Currency, 
BOND.IssuePrice as Issue_Price, 
BOND.PriceAsPercent as Price_as_Percent,
BOND.IssueAmount as Issue_Amount_in_Millions, 
BOND.IssueAmountDate as Issue_Amount_Date, 
BOND.Series, 
BOND.Class, 
case when BOND.SeniorJunior='S' then 'Senior' when BOND.SeniorJunior='J' then 'Junior' when BOND.SeniorJunior='M' then 'Mezzanine' ELSE '' end as Financing_Structure,
BOND.OutstandingAmount as Outstanding_In_Millions, 
case when BOND.OutstandingAmountDate = '1800/01/01' then null else BOND.OutstandingAmountDate end as Outstanding_Change_Date, 
case when INTTYPE.Lookup is null then bond.InterestBasis else INTTYPE.Lookup end as Interest_Basis, 
case when FRNTYPE.Lookup is null then bond.FRNType else FRNTYPE.Lookup end as FRN_Type,  
BOND.InterestCurrency as Interest_Currency, 
case when FRNINDXBEN.Lookup is null then bond.FRNIndexBenchmark else FRNINDXBEN.Lookup end as FRN_Index_Benchmark,  
BOND.Markup as Margin,
BOND.Rounding, 
BOND.InterestRate as Interest_Rate, 
BOND.MinimumInterestRate as Min_Interest_Rate, 
BOND.MaximumInterestRate as Max_Interest_Rate, 
BOND.IntCommencementDate as Interest_Commencement_Date, 
BOND.FirstCouponDate as First_Coupon_Date, 
case when BOND.Guaranteed='T' then 'Yes' ELSE 'No' end as Guaranteed,
case when BOND.SecuredBy='Y' then 'Yes' when BOND.SecuredBy='N' then 'No' ELSE '' end as Secured_By,
case when BOND.WarrantAttached='Y' then 'Yes' when BOND.WarrantAttached='N' then 'No' ELSE '' end as Warrant_Attached,
case when BOND.Subordinate='Y' then 'Yes' when BOND.Subordinate='N' then 'No' ELSE '' end as Subordinate,
case when ASSETBKD.Lookup is null then bond.SecurityCharge else ASSETBKD.Lookup end as Security_Charge, 
case when INTACCRUAL.Lookup is null then bond.InterestAccrualConvention else INTACCRUAL.Lookup end as Interest_Accrual_Convention,
case when INTBDC.Lookup is null then bond.IntBusDayConv else INTBDC.Lookup end as Interest_Biz_Day_Convention,  
ConventionMethod as Convention_Method,  
case when STRIP.Lookup is null then bond.Strip else STRIP.Lookup end as Strip, 
BOND.StripInterestNumber as Strip_Interest_Number,
case when FREQ.Lookup is null then bond.InterestPaymentFrequency else FREQ.Lookup end as Interest_Payment_Freq,  
case when BOND.VarIntPayDate='Y' then 'Yes' when BOND.VarIntPayDate='N' then 'No' ELSE '' end as Variable_Interest_Paydate,
BOND.FrnIntAdjFreq as FRN_Interest_Adjustment_Freq, 
case when (len(InterestPayDate1) <> 4 or InterestPayDate1 is NULL or InterestPayDate1 = '' ) then ''
when ((cast(substring(InterestPayDate1,3,2) as int) < 1) or (cast(substring(InterestPayDate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate1,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate1,1,2)
end as Interest_PayDate1,
case when (len(InterestPayDate2) <> 4 or InterestPayDate2 is NULL or InterestPayDate2 = '' ) then ''
when ((cast(substring(InterestPayDate2,3,2) as int) < 1) or (cast(substring(InterestPayDate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate2,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate2,1,2)
end as Interest_PayDate2,
case when (len(InterestPayDate3) <> 4 or InterestPayDate3 is NULL or InterestPayDate3 = '' ) then ''
when ((cast(substring(InterestPayDate3,3,2) as int) < 1) or (cast(substring(InterestPayDate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate3,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate3,1,2)
end as Interest_PayDate3,
case when (len(InterestPayDate4) <> 4 or InterestPayDate4 is NULL or InterestPayDate4 = '' ) then ''
when ((cast(substring(InterestPayDate4,3,2) as int) < 1) or (cast(substring(InterestPayDate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate4,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate4,1,2)
end as Interest_PayDate4,
case when BOND.Perpetual='I' then 'Irredeemable' when BOND.Perpetual='P' then 'Perpetual' when BOND.Perpetual='U' then 'Undated' ELSE '' end as Perpetual,
case when DEBTSTATUS.Lookup is null then bond.MaturityStructure else DEBTSTATUS.Lookup end as Maturity_Structure,  
BOND.MaturityDate as Maturity_Date, 
case when BOND.MaturityExtendible='Y' then 'Yes' when BOND.MaturityExtendible='N' then 'No' ELSE '' end as Maturity_Extendible,
BOND.MaturityCurrency as Maturity_Currency,
BOND.MatPrice as Maturity_Price,
BOND.MatPriceAsPercent as Maturity_Price_as_Percent,
case when MATBNHMRK.Lookup is null then bond.MaturityBenchmark else MATBNHMRK.Lookup end as Maturity_Benchmark,  
case when MATBDC.Lookup is null then bond.MatBusDayConv else MATBDC.Lookup end as Maturity_Biz_Day_Convention,  
case when BOND.Cumulative='Y' then 'Yes' when BOND.Cumulative='N' then 'No' ELSE '' end as Cumulative,
case when BOND.Callable='Y' then 'Yes' when BOND.Callable='N' then 'No' ELSE '' end as Callable,
case when BOND.Puttable='Y' then 'Yes' when BOND.Puttable='N' then 'No' ELSE '' end as Puttable,
case when BOND.SinkingFund='Y' then 'Yes' when BOND.SinkingFund='N' then 'No' ELSE '' end as Sinking_Fund,
case when PAYOUT.Lookup is null then bond.PayOutMode else PAYOUT.Lookup end as Payout_Mode,
BOND.DebtMarket as Debt_Grouping, 
case when BOND.OnTap='T' then 'Yes' ELSE 'No' end as On_Tap,
BOND.MaximumTapAmount as Max_Tap_Amount, 
BOND.TapExpiryDate as Tap_Expiry_Date, 
case when issuedate>'2001/02/28' and issueamountdate>'2002/03/01' 
     and InterestBasis<>'ZC' and cntryofincorp in (select cntryofincorp from EUSD) then 'Yes'
     else 'No' end as EUSD_Applicable,
BOND.DomesticTaxRate as Domestic_Tax_Rate, 
BOND.NonResidentTaxRate as Non_Resident_Tax_Rate, 
BOND.TaxRules as Tax_Rules, 
case when bond.Bondsrc is null or bond.Bondsrc='' or bond.Bondsrc='PP' or bond.Bondsrc='SR' then '' else 'Yes' end as Backup_Document, 
'BOND|SecID|GoverningLaw' as Link_Governing_Law,
'BOND|SecID|Notes' as Link_Notes,
BOND.Actflag,
BOND.Acttime,
BOND.AnnounceDate,
BOND.SecId

	  into wfi.dbo.reference

FROM BOND
inner join scmst on BOND.SecID = SCMST.SecID
inner join issur on scmst.issid = issur.issid
left outer join wfi.dbo.sys_lookup as HOLDING on SCMST.Holding = HOLDING.Code
                      and 'HOLDING' = HOLDING.TypeGroup
left outer join wfi.dbo.sys_lookup as STRUCTCD on SCMST.StructCD = STRUCTCD.Code
                      and 'STRUCTCD' = STRUCTCD.TypeGroup
left outer join wfi.dbo.sys_lookup as BONDTYPE on BOND.Bondtype = BONDTYPE.Code
                      and 'BONDTYPE' = BONDTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FREQ on BOND.InterestPaymentFrequency = FREQ.Code
                      and 'FREQ' = FREQ.TypeGroup
left outer join wfi.dbo.sys_lookup as INTTYPE on BOND.InterestBasis = INTTYPE.Code
                      and 'INTTYPE' = INTTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as INTACCRUAL on BOND.InterestAccrualConvention = INTACCRUAL.Code
                      and 'INTACCRUAL' = INTACCRUAL.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNTYPE on BOND.FRNType = FRNTYPE.Code
                      and 'FRNTYPE' = FRNTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNINDXBEN on BOND.FRNIndexBenchmark = FRNINDXBEN.Code
                      and 'FRNINDXBEN' = FRNINDXBEN.TypeGroup
left outer join wfi.dbo.sys_lookup as ASSETBKD on BOND.SecurityCharge = ASSETBKD.Code
                      and 'ASSETBKD' = ASSETBKD.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBDC on BOND.IntBusDayConv = MATBDC.Code
                      and 'MATBDC' = MATBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as INTBDC on BOND.IntBusDayConv = INTBDC.Code
                      and 'INTBDC' = INTBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as DEBTSTATUS on BOND.MaturityStructure = DEBTSTATUS.Code
                      and 'DEBTSTATUS' = DEBTSTATUS.TypeGroup
left outer join wfi.dbo.sys_lookup as STRIP on BOND.Strip = STRIP.Code
                      and 'STRIP' = STRIP.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBNHMRK on BOND.MaturityBenchmark = MATBNHMRK.Code
                      and 'MATBNHMRK' = MATBNHMRK.TypeGroup
left outer join wfi.dbo.sys_lookup as PAYOUT on BOND.PayoutMode = PAYOUT.Code
                      and 'PAYOUT' = PAYOUT.TypeGroup

--	WHERE
--	BOND.Acttime > (select max(wfi.dbo.reference.acttime) from wfi.dbo.reference)
--	  and ((MaturityDate>getdate()-1 or MaturityDate is null)
--	  or (MaturityDate<getdate()-1 and SCMST.statusflag<>'I'))

	  WHERE
	  ((MaturityDate>getdate()-1 or MaturityDate is null)
	  or (MaturityDate<getdate()-1 and SCMST.statusflag<>'I'))
	  and BOND.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE reference ALTER COLUMN  SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[reference] WITH NOCHECK ADD 
	   CONSTRAINT [pk_reference_SecID] PRIMARY KEY ([SecID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_reference_Maturity] ON [dbo].[reference]([Maturity_Date]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_reference_Acttime] ON [dbo].[reference]([Acttime] desc) ON [PRIMARY]
	  GO
	  use WFI
	  if exists (select * from sysobjects where name = 'static_change')
	      drop table static_change
	  GO
	  use wca
	  SELECT distinct
	  'Security Name' as Static_Change,
	  SCCHG.SecOldName as Previous_Value,
	  SCCHG.SecNewName as Current_Value,
	  SCCHG.DateofChange as Effective_Date,
	  case when SCCHG.Eventtype is null then '' when RELEVENT.Lookup is null then SCCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
	  SCCHG.Actflag,
	  SCCHG.Acttime,
	  SCCHG.AnnounceDate,
	  SCCHG.ScchgID as EventID,
	  SCCHG.SecID
	  into WFI.dbo.static_change
	  from SCCHG
	  inner join bond on scchg.secid = bond.secid
	  left outer join WFI.dbo.sys_lookup as RELEVENT on SCCHG.EventType = RELEVENT.Code
	                        and 'EVENT' = RELEVENT.TypeGroup
	  WHERE
	  SecOldName <> SecNewName
	  GO

	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  SecID int NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  EventID int NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  static_change varchar(32) NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  Reason varchar(32) NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE [DBO].[static_change] WITH NOCHECK ADD 
	    CONSTRAINT [pk_static_change] PRIMARY KEY ( [SecID], [static_change],[EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_static_change_Acttime] ON [dbo].[static_change]([Acttime] desc) ON [PRIMARY] 
	  GO
	  use WFI 
	  CREATE  INDEX [ix_static_change_Effective] ON [dbo].[static_change]([Effective_Date]) ON [PRIMARY] 
	  GO

--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Security Name')
--	delete from static_change
--	where
--	static_change = 'Security Name'
--	and EventID in
--	(select ScchgID from wca.dbo.SCCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'ISIN')
--	delete from static_change
--	where
--	(static_change = 'ISIN'
--	or static_change = 'UsCode'
--	or static_change = 'CommonCode')
--	and EventID in
--	(select IccID from wca.dbo.ICC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Sedol')
--	delete from static_change
--	where
--	static_change = 'Sedol'
--	and EventID in
--	(select SdchgID from wca.dbo.SDCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Incorporation')
--	delete from static_change
--	where
--	static_change = 'Incorporation'
--	and EventID in
--	(select InchgID from wca.dbo.INCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Company Name')
--	delete from static_change
--	where
--	static_change = 'Company Name'
--	and EventID in
--	(select IschgID from wca.dbo.ISCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Listing Status')
--	delete from static_change
--	where
--	static_change = 'Listing Status'
--	and EventID in
--	(select LstatID from wca.dbo.LSTAT
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Local Code')
--	delete from static_change
--	where
--	static_change = 'Local Code'
--	and EventID in
--	(select LccID from wca.dbo.LCC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Interest Basis')
--	delete from static_change
--	where
--	static_change = 'Interest Basis'
--	and EventID in
--	(select IntbcID from wca.dbo.INTBC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = '')
--	delete from static_change
--	where
--	(static_change = 'Current Currency'
--	or static_change = 'Interest Currency'
--	or static_change = 'Maturity Currency'
--	or static_change = 'Interest BDC'
--	or static_change = 'Maturity BDC'
--	or static_change = 'Interest Type'
--	or static_change = 'Debt Type')
--	and EventID in
--	(select BschgID from wca.dbo.BSCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = '')
--	delete from static_change
--	where
--	(static_change = 'FRN Type'
--	or static_change = 'RFN_Index_Benchmark'
--	or static_change = 'FRN_Adjustment_Freq'
--	or static_change = 'FRN_Margin'
--	or static_change = 'FRN_Min_IR'
--	or static_change = 'FRN_Max_IR'
--	or static_change = 'FRN_Rounding')
--	and EventID in
--	(select FrnfxID from wca.dbo.FRNFX
--	where acttime>=@StartDate)
--	GO
--	
--	use wca
--	insert into wfi.dbo.static_change
--	SELECT distinct
--	'Security Name' as Static_Change,
--	SCCHG.SecOldName as Previous_Value,
--	SCCHG.SecNewName as Current_Value,
--	SCCHG.DateofChange as Effective_Date,
--	case when RELEVENT.Lookup is null then SCCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
--	SCCHG.Actflag,
--	SCCHG.Acttime,
--	SCCHG.AnnounceDate,
--	SCCHG.ScchgID as EventID,
--	SCCHG.SecID
--	from SCCHG 
--	inner join bond on scchg.secid = bond.secid
--	left outer join WFI.dbo.sys_lookup as RELEVENT on SCCHG.EventType = RELEVENT.Code
--	                   and 'EVENT' = RELEVENT.TypeGroup
--	WHERE
--	inner join bond on scchg.secid = bond.secid
--	and SecOldName <> SecNewName
--	and SCCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
--	                              then '2000/01/01'
--	                              else max(wfi.dbo.static_change.Acttime)
--	                              end
--	                  from wfi.dbo.static_change 
--	                  where wfi.dbo.static_change.static_change = 'Security Name')
--	GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'ISIN' as Static_Change,
icc.OldISIN as Previous_Value,
icc.NewISIN as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on icc.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldIsin <> NewIsin
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'ISIN')
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'US Code' as Static_Change,
icc.OldUsCode as Previous_Value,
icc.NewUsCode as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on ICC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldUsCode <> NewUsCode
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'UsCode')
GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Common Code' as Static_Change,
icc.OldCommonCode as Previous_Value,
icc.NewCommonCode as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on ICC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldCommonCode <> NewCommonCode
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'CommonCode')

GO
 

use wca
insert into wfi.dbo.static_change
SELECT distinct
'SEDOL' as Static_Change,
SDCHG.OldSedol as Previous_Value,
SDCHG.NewSedol as Current_Value,
SDCHG.Effectivedate as Effective_Date,
case when SDCHG.Eventtype is null then '' when RELEVENT.Lookup is null then SDCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
SDCHG.Actflag,
SDCHG.Acttime,
SDCHG.AnnounceDate,
SDCHG.SDCHGID as EventID,
SDCHG.SecID
from SDCHG
inner join bond on sdchg.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on SDCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldSedol <> NewSedol
and SDCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Sedol')

GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Incorporation' as Static_Change,
case when OLDCNTRY.Lookup is null then INCHG.OldCntryCD else OLDCNTRY.Lookup end as Previous_Value,
case when NEWCNTRY.Lookup is null then INCHG.NewCntryCD else NEWCNTRY.Lookup end as Current_Value,
INCHG.InchgDate as Effective_Date,
case when INCHG.Eventtype is null then '' when RELEVENT.Lookup is null then INCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
INCHG.Actflag,
INCHG.Acttime,
INCHG.AnnounceDate,
INCHG.INCHGID as EventID,
SCMST.SecID
from INCHG
INNER JOIN SCMST ON INCHG.ISSID = SCMST.ISSID
inner join bond on scmst.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on INCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCNTRY on INCHG.OldCntryCD = OLDCNTRY.Code
                      and 'CNTRY' = OLDCntry.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWCNTRY on INCHG.NewCntryCD = NEWCNTRY.Code
                      and 'CNTRY' = NEWCntry.TypeGroup
where
bond.Actflag<>'D'
and OldCntryCD <> NewCntryCD
and INCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Incorporation ')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'Company Name' as Static_Change,
ISCHG.IssOldName as Previous_Value,
ISCHG.IssNewName as Current_Value,
ISCHG.NameChangeDate as Effective_Date,
case when ISCHG.Eventtype is null then '' when RELEVENT.Lookup is null then ISCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ISCHG.Actflag,
ISCHG.Acttime,
ISCHG.AnnounceDate,
ISCHG.ISCHGID as EventID,
SCMST.SecID
from ISCHG
INNER JOIN SCMST ON ISCHG.ISSID = SCMST.ISSID
inner join bond on scmst.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ISCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and IssOldName <> IssNewName
and ISCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Company Name')

GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Local Code' as Static_Change,
LCC.OldLocalcode as Previous_Value,
LCC.Newlocalcode as Current_Value,
LCC.EffectiveDate as Effective_Date,
case when LCC.Eventtype is null then '' when RELEVENT.Lookup is null then LCC.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
LCC.Actflag,
LCC.Acttime,
LCC.AnnounceDate,
LCC.LCCID as EventID,
LCC.SecID
from LCC
inner join bond on lcc.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on LCC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldLocalcode <> NewLocalcode
and LCC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Local Code')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest Type' as Static_Change,
case when OLDINTTYPE.Lookup is null then INTBC.OldIntBasis else OLDINTTYPE.Lookup end as Previous_Value,
case when NEWINTTYPE.Lookup is null then INTBC.NewIntBasis else NEWINTTYPE.Lookup end as Current_Value,
INTBC.EffectiveDate as Effective_Date,
'Correction' as Reason,
INTBC.Actflag,
INTBC.Acttime,
INTBC.AnnounceDate,
INTBC.INTBCID as EventID,
INTBC.SecID
from INTBC
inner join bond on INTBC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as OLDINTTYPE on INTBC.OldIntBasis = OLDINTTYPE.Code
                      and 'INTTYPE' = OLDINTTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTTYPE on INTBC.NewIntBasis = NEWINTTYPE.Code
                      and 'INTTYPE' = NEWINTTYPE.TypeGroup
where
bond.Actflag<>'D'
and INTBC.OldIntBasis <> INTBC.NewIntBasis
and INTBC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                                 from wfi.dbo.static_change 
				 where wfi.dbo.static_change.static_change = 'Interest Type')
  
GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest Currency' as Static_Change,
BSCHG.OldInterestCurrency as Previous_Value,
BSCHG.NewInterestCurrency as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldInterestCurrency <> BSCHG.NewInterestCurrency
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Interest Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Maturity Currency' as Static_Change,
BSCHG.OldMaturityCurrency as Previous_Value,
BSCHG.NewMaturityCurrency as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldMaturityCurrency <> BSCHG.NewMaturityCurrency
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Maturity Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Current Currency' as Static_Change,
BSCHG.OldCurenCD as Previous_Value,
BSCHG.NewCurenCD as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldCurenCD <> BSCHG.NewCurenCD
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Current Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Debt Type' as Static_Change,
case when OLDBONDTYPE.Lookup is null then BSCHG.OldBondType else OLDBONDTYPE.Lookup end as Previous_Value,
case when NEWBONDTYPE.Lookup is null then BSCHG.NewBondType else NEWBONDTYPE.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDBONDTYPE on BSCHG.OldBondType = OLDBONDTYPE.Code
                      and 'BONDTYPE' = OLDBONDTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWBONDTYPE on BSCHG.NewBondType = NEWBONDTYPE.Code
                      and 'BONDTYPE' = NEWBONDTYPE.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldBondType <> BSCHG.NewBondType
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Debt Type')

GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest BDC' as Static_Change,
case when OLDINTBDC.Lookup is null then BSCHG.OldIntBusDayConv else OLDINTBDC.Lookup end as Previous_Value,
case when NEWINTBDC.Lookup is null then BSCHG.NewIntBusDayConv else NEWINTBDC.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDINTBDC on BSCHG.OldIntBusDayConv = OLDINTBDC.Code
                      and 'INTBDC' = OLDINTBDC.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTBDC on BSCHG.NewIntBusDayConv = NEWINTBDC.Code
                      and 'INTBDC' = NEWINTBDC.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldIntBusDayConv <> BSCHG.NewIntBusDayConv
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Interest BDC')

GO
 
use wca
insert into wfi.dbo.static_change
SELECT distinct
'Maturity BDC' as Static_Change,
case when OLDMATBDC.Lookup is null then BSCHG.OldMatBusDayConv else OLDMATBDC.Lookup end as Previous_Value,
case when NEWMATBDC.Lookup is null then BSCHG.NewMatBusDayConv else NEWMATBDC.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDMATBDC on BSCHG.OldMatBusDayConv = OLDMATBDC.Code
                      and 'INTBDC' = OLDMATBDC.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWMATBDC on BSCHG.NewMatBusDayConv = NEWMATBDC.Code
                      and 'INTBDC' = NEWMATBDC.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldMatBusDayConv  <> BSCHG.NewMatBusDayConv 
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Maturity BDC')
GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Rounding' as Static_Change,
FRNFX.OldRounding as Previous_Value,
FRNFX.NewRounding as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldRounding <> FRNFX.NewRounding
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Rounding')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Minimum IR' as Static_Change,
FRNFX.OldMinimumInterestRate as Previous_Value,
FRNFX.NewMinimumInterestRate as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMinimumInterestRate <> FRNFX.NewMinimumInterestRate
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Minimum IR')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Maximum IR' as Static_Change,
FRNFX.OldMaximumInterestRate as Previous_Value,
FRNFX.NewMaximumInterestRate as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMaximumInterestRate <> FRNFX.NewMaximumInterestRate
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Maximum IR')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Margin' as Static_Change,
FRNFX.OldMarkup as Previous_Value,
FRNFX.NewMarkup as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMarkup <> FRNFX.NewMarkup
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Margin')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN IR Adjustment Frequency' as Static_Change,
case when OLDFREQ.Lookup is null then FRNFX.OldFRNIntAdjFreq else OLDFREQ.Lookup end as Previous_Value,
case when NEWFREQ.Lookup is null then FRNFX.NewFRNIntAdjFreq else NEWFREQ.Lookup end as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDFREQ on FRNFX.OldFRNIntAdjFreq = OLDFREQ.Code
                      and 'FREQ' = OLDFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWFREQ on FRNFX.NewFRNIntAdjFreq = NEWFREQ.Code
                      and 'FREQ' = NEWFREQ.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNIntAdjFreq <> FRNFX.NewFRNIntAdjFreq
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  IR Adjustment Frequency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Index Benchmark' as Static_Change,
case when OLDINDXBEN.Lookup is null then FRNFX.OldFRNIndexBenchmark else OLDINDXBEN.Lookup end as Current_Value,
case when NEWINDXBEN.Lookup is null then FRNFX.NewFRNIndexBenchmark else NEWINDXBEN.Lookup end as Previous_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDINDXBEN on FRNFX.OldFRNIndexBenchmark = OLDINDXBEN.Code
                      and 'FRNINDXBEN' = OLDINDXBEN.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINDXBEN on FRNFX.NewFRNIndexBenchmark = NEWINDXBEN.Code
                      and 'FRNINDXBEN' = NEWINDXBEN.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNIndexBenchmark <> FRNFX.NewFRNIndexBenchmark
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Index Benchmark')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Type' as Static_Change,
case when OLDTYPE.Lookup is null then FRNFX.OldFRNType else OLDTYPE.Lookup end as Previous_Value,
case when NEWTYPE.Lookup is null then FRNFX.NewFRNType else NEWTYPE.Lookup end as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDTYPE on FRNFX.OldFRNType = OLDTYPE.Code
                      and 'FRNTYPE' = OLDTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWTYPE on FRNFX.NewFRNType = NEWTYPE.Code
                      and 'FRNTYPE' = NEWTYPE.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNType <> FRNFX.NewFRNType
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN Type')

GO
use WFI
if exists (select * from sysobjects where name = 'sys_lookup')
  drop table sys_lookup
GO

use WCA
SELECT
upper(Actflag) as Actflag,
lookupextra.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookupextra.Lookup
into WFI.dbo.sys_lookup
FROM LOOKUPEXTRA

union

SELECT
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD

union

SELECT
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY

union

--SELECT
--wfi.dbo.sys_exchg.Actflag,
--wfi.dbo.sys_exchg.Acttime,
--upper('EXCHANGE') as TypeGroup,
--wfi.dbo.sys_exchg.exchgCD as Code,
--wfi.dbo.sys_exchg.exchgname as Lookup
--from wfi.dbo.exchg

--union

SELECT
exchg.Actflag,
exchg.Acttime,
upper('MICCODE') as TypeGroup,
exchg.MIC as Code,
exchg.exchgname as Lookup
from exchg
where MIC <> '' and MIC is not null

union

--SELECT
--sys_exchg.Actflag,
--sys_exchg.Acttime,
--upper('MICMAP') as TypeGroup,
--sys_exchg.exchgCD as Code,
--sys_exchg.MIC as Lookup
--from sys_exchg

--union

SELECT
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
cast(Indus.IndusID as varchar(10)) as Code,
Indus.IndusName as Lookup
from INDUS

union

SELECT
Cntry.Actflag,
Cntry.Acttime,
upper('CNTRY') as TypeGroup,
Cntry.CntryCD as Code,
Cntry.Country as Lookup
from CNTRY

union

SELECT
Curen.Actflag,
Curen.Acttime,
upper('CUREN') as TypeGroup,
Curen.CurenCD as Code,
Curen.Currency as Lookup
from CUREN

union

SELECT
Event.Actflag,
Event.Acttime,
upper('EVENT') as TypeGroup,
Event.EventType as Code,
Event.EventName as Lookup
from EVENT

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DRTYPE') as TypeGroup,
sectygrp.sectycd as Code,
sectygrp.securitydescriptor as Lookup
from sectygrp
where secgrpid = 2

use WFI 
ALTER TABLE sys_lookup ALTER COLUMN  TypeGroup char(10) NOT NULL
ALTER TABLE sys_lookup ALTER COLUMN  Code char(10) NOT NULL
ALTER TABLE sys_lookup ALTER COLUMN  Lookup varchar(70) NOT NULL
ALTER TABLE sys_lookup ALTER COLUMN  Actflag char(1) NOT NULL
GO 
use WFI 
ALTER TABLE [DBO].[sys_lookup] WITH NOCHECK ADD 
 CONSTRAINT [pk_sys_lookup] PRIMARY KEY ([Typegroup],[Code])  ON [PRIMARY]
GO

	  use WFI
	  if exists (select * from sysobjects where name = 'matured')
	    drop table matured
	  go

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from matured)
--	delete from matured
--	where SecID in
--	(select SecID from wca.dbo.BOND
--	where acttime>@StartDate)
--	go

 
use wca
--	insert into wfi.dbo.matured
SELECT 
case when BOND.Municipal='Y' then 'Yes' when BOND.Municipal='N' then 'No' ELSE '' end as Municipal,
case when BOND.PrivatePlacement='Y' then 'Yes' when BOND.PrivatePlacement='N' then 'No' ELSE '' end as Private_Placement,
case when BOND.Syndicated='Y' then 'Yes' when BOND.Syndicated='N' then 'No' ELSE '' end as Syndicated,
case when BondType.Lookup is null then bond.bondtype else BondType.Lookup end as Security_Type, 
case when STRUCTCD.Lookup is null then SCMST.StructCD else STRUCTCD.Lookup end as Debt_Structure,
SCMST.Regs144a as Regulation_Code,
BOND.CurenCD as Current_Currency,
case when HOLDING.Lookup is null then SCMST.Holding else HOLDING.Lookup end as Holding,
BOND.ParValue as Nominal_Value, 
BOND.Denomination1 as Denom_1,
BOND.Denomination2 as Denom_2,
BOND.Denomination3 as Denom_3,
BOND.Denomination4 as Denom_4,
BOND.Denomination5 as Denom_5,
BOND.Denomination6 as Denom_6,
BOND.Denomination7 as Denom_7,
BOND.MinimumDenomination as Min_Denom, 
BOND.DenominationMultiple as Denom_Multiple, 
BOND.IssueDate as Issue_Date, 
BOND.IssueCurrency as Issue_Currency, 
BOND.IssuePrice as Issue_Price, 
BOND.PriceAsPercent as Price_as_Percent,
BOND.IssueAmount as Issue_Amount_in_Millions, 
BOND.IssueAmountDate as Issue_Amount_Date, 
BOND.Series, 
BOND.Class, 
case when BOND.SeniorJunior='S' then 'Senior' when BOND.SeniorJunior='J' then 'Junior' when BOND.SeniorJunior='M' then 'Mezzanine' ELSE '' end as Financing_Structure,
BOND.OutstandingAmount as Outstanding_In_Millions, 
case when BOND.OutstandingAmountDate = '1800/01/01' then null else BOND.OutstandingAmountDate end as Outstanding_Change_Date, 
case when INTTYPE.Lookup is null then bond.InterestBasis else INTTYPE.Lookup end as Interest_Basis, 
case when FRNTYPE.Lookup is null then bond.FRNType else FRNTYPE.Lookup end as FRN_Type,  
BOND.InterestCurrency as Interest_Currency, 
case when FRNINDXBEN.Lookup is null then bond.FRNIndexBenchmark else FRNINDXBEN.Lookup end as FRN_Index_Benchmark,  
BOND.Markup as Margin,
BOND.Rounding, 
BOND.InterestRate as Interest_Rate, 
BOND.MinimumInterestRate as Min_Interest_Rate, 
BOND.MaximumInterestRate as Max_Interest_Rate, 
BOND.IntCommencementDate as Interest_Commencement_Date, 
BOND.FirstCouponDate as First_Coupon_Date, 
case when BOND.Guaranteed='T' then 'Yes' ELSE 'No' end as Guaranteed,
case when BOND.SecuredBy='Y' then 'Yes' when BOND.SecuredBy='N' then 'No' ELSE '' end as Secured_By,
case when BOND.WarrantAttached='Y' then 'Yes' when BOND.WarrantAttached='N' then 'No' ELSE '' end as Warrant_Attached,
case when BOND.Subordinate='Y' then 'Yes' when BOND.Subordinate='N' then 'No' ELSE '' end as Subordinate,
case when ASSETBKD.Lookup is null then bond.SecurityCharge else ASSETBKD.Lookup end as Security_Charge, 
case when INTACCRUAL.Lookup is null then bond.InterestAccrualConvention else INTACCRUAL.Lookup end as Interest_Accrual_Convention,
case when INTBDC.Lookup is null then bond.IntBusDayConv else INTBDC.Lookup end as Interest_Biz_Day_Convention,  
ConventionMethod as Convention_Method,  
case when STRIP.Lookup is null then bond.Strip else STRIP.Lookup end as Strip, 
BOND.StripInterestNumber as Strip_Interest_Number,
case when FREQ.Lookup is null then bond.InterestPaymentFrequency else FREQ.Lookup end as Interest_Payment_Freq,  
case when BOND.VarIntPayDate='Y' then 'Yes' when BOND.VarIntPayDate='N' then 'No' ELSE '' end as Variable_Interest_Paydate,
BOND.FrnIntAdjFreq as FRN_Interest_Adjustment_Freq, 
case when (len(InterestPayDate1) <> 4 or InterestPayDate1 is NULL or InterestPayDate1 = '' ) then ''
when ((cast(substring(InterestPayDate1,3,2) as int) < 1) or (cast(substring(InterestPayDate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate1,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate1,1,2)
end as Interest_PayDate1,
case when (len(InterestPayDate2) <> 4 or InterestPayDate2 is NULL or InterestPayDate2 = '' ) then ''
when ((cast(substring(InterestPayDate2,3,2) as int) < 1) or (cast(substring(InterestPayDate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate2,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate2,1,2)
end as Interest_PayDate2,
case when (len(InterestPayDate3) <> 4 or InterestPayDate3 is NULL or InterestPayDate3 = '' ) then ''
when ((cast(substring(InterestPayDate3,3,2) as int) < 1) or (cast(substring(InterestPayDate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate3,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate3,1,2)
end as Interest_PayDate3,
case when (len(InterestPayDate4) <> 4 or InterestPayDate4 is NULL or InterestPayDate4 = '' ) then ''
when ((cast(substring(InterestPayDate4,3,2) as int) < 1) or (cast(substring(InterestPayDate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate4,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate4,1,2)
end as Interest_PayDate4,
case when BOND.Perpetual='I' then 'Irredeemable' when BOND.Perpetual='P' then 'Perpetual' when BOND.Perpetual='U' then 'Undated' ELSE '' end as Perpetual,
case when DEBTSTATUS.Lookup is null then bond.MaturityStructure else DEBTSTATUS.Lookup end as Maturity_Structure,  
BOND.MaturityDate as Maturity_Date, 
case when BOND.MaturityExtendible='Y' then 'Yes' when BOND.MaturityExtendible='N' then 'No' ELSE '' end as Maturity_Extendible,
BOND.MaturityCurrency as Maturity_Currency,
BOND.MatPrice as Maturity_Price,
BOND.MatPriceAsPercent as Maturity_Price_as_Percent,
case when MATBNHMRK.Lookup is null then bond.MaturityBenchmark else MATBNHMRK.Lookup end as Maturity_Benchmark,  
case when MATBDC.Lookup is null then bond.MatBusDayConv else MATBDC.Lookup end as Maturity_Biz_Day_Convention,  
case when BOND.Cumulative='Y' then 'Yes' when BOND.Cumulative='N' then 'No' ELSE '' end as Cumulative,
case when BOND.Callable='Y' then 'Yes' when BOND.Callable='N' then 'No' ELSE '' end as Callable,
case when BOND.Puttable='Y' then 'Yes' when BOND.Puttable='N' then 'No' ELSE '' end as Puttable,
case when BOND.SinkingFund='Y' then 'Yes' when BOND.SinkingFund='N' then 'No' ELSE '' end as Sinking_Fund,
case when PAYOUT.Lookup is null then bond.PayOutMode else PAYOUT.Lookup end as Payout_Mode,
BOND.DebtMarket as Debt_Grouping, 
case when BOND.OnTap='T' then 'Yes' ELSE 'No' end as On_Tap,
BOND.MaximumTapAmount as Max_Tap_Amount, 
BOND.TapExpiryDate as Tap_Expiry_Date, 
case when issuedate>'2001/02/28' and issueamountdate>'2002/03/01' 
     and InterestBasis<>'ZC' and cntryofincorp in (select cntryofincorp from EUSD) then 'Yes'
     else 'No' end as EUSD_Applicable,
BOND.DomesticTaxRate as Domestic_Tax_Rate, 
BOND.NonResidentTaxRate as Non_Resident_Tax_Rate, 
BOND.TaxRules as Tax_Rules, 
case when bond.Bondsrc is null or bond.Bondsrc='' or bond.Bondsrc='PP' or bond.Bondsrc='SR' then '' else 'Yes' end as Backup_Document, 
'BOND|SecID|GoverningLaw' as Link_Governing_Law,
'BOND|SecID|Notes' as Link_Notes,
BOND.Actflag,
BOND.Acttime,
BOND.AnnounceDate,
BOND.SecId

	  into wfi.dbo.matured

FROM BOND
inner join scmst on BOND.SecID = SCMST.SecID
inner join issur on scmst.issid = issur.issid
left outer join wfi.dbo.sys_lookup as HOLDING on SCMST.Holding = HOLDING.Code
                      and 'HOLDING' = HOLDING.TypeGroup
left outer join wfi.dbo.sys_lookup as STRUCTCD on SCMST.StructCD = STRUCTCD.Code
                      and 'STRUCTCD' = STRUCTCD.TypeGroup
left outer join wfi.dbo.sys_lookup as BONDTYPE on BOND.Bondtype = BONDTYPE.Code
                      and 'BONDTYPE' = BONDTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FREQ on BOND.InterestPaymentFrequency = FREQ.Code
                      and 'FREQ' = FREQ.TypeGroup
left outer join wfi.dbo.sys_lookup as INTTYPE on BOND.InterestBasis = INTTYPE.Code
                      and 'INTTYPE' = INTTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as INTACCRUAL on BOND.InterestAccrualConvention = INTACCRUAL.Code
                      and 'INTACCRUAL' = INTACCRUAL.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNTYPE on BOND.FRNType = FRNTYPE.Code
                      and 'FRNTYPE' = FRNTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNINDXBEN on BOND.FRNIndexBenchmark = FRNINDXBEN.Code
                      and 'FRNINDXBEN' = FRNINDXBEN.TypeGroup
left outer join wfi.dbo.sys_lookup as ASSETBKD on BOND.SecurityCharge = ASSETBKD.Code
                      and 'ASSETBKD' = ASSETBKD.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBDC on BOND.IntBusDayConv = MATBDC.Code
                      and 'MATBDC' = MATBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as INTBDC on BOND.IntBusDayConv = INTBDC.Code
                      and 'INTBDC' = INTBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as DEBTSTATUS on BOND.MaturityStructure = DEBTSTATUS.Code
                      and 'DEBTSTATUS' = DEBTSTATUS.TypeGroup
left outer join wfi.dbo.sys_lookup as STRIP on BOND.Strip = STRIP.Code
                      and 'STRIP' = STRIP.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBNHMRK on BOND.MaturityBenchmark = MATBNHMRK.Code
                      and 'MATBNHMRK' = MATBNHMRK.TypeGroup
left outer join wfi.dbo.sys_lookup as PAYOUT on BOND.PayoutMode = PAYOUT.Code
                      and 'PAYOUT' = PAYOUT.TypeGroup

--	WHERE
--	BOND.Acttime > (select max(wfi.dbo.matured.acttime) from wfi.dbo.matured)
--	not ((MaturityDate>getdate()-1 or MaturityDate is null)
--	or (MaturityDate<getdate()-1 and SCMST.statusflag<>'I'))

	  WHERE
	  not ((MaturityDate>getdate()-1 or MaturityDate is null)
	  or (MaturityDate<getdate()-1 and SCMST.statusflag<>'I'))
	  and BOND.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE matured ALTER COLUMN  SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[matured] WITH NOCHECK ADD 
	   CONSTRAINT [pk_matured_SecID] PRIMARY KEY ([SecID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_matured_Maturity] ON [dbo].[matured]([Maturity_Date]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_matured_Acttime] ON [dbo].[matured]([Acttime] desc) ON [PRIMARY]
	  GO
	  use WFI
	  if exists (select * from sysobjects where name = 'outstanding_amount')
	    drop table outstanding_amount
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from outstanding_amount)
--	delete from outstanding_amount
--	where EVENTID in
--	(select BOCHGID from wca.dbo.BOCHG
--	where acttime>@StartDate)


use wca

--	insert into wfi.dbo.outstanding_amount

SELECT 
case when RELEVENT.Lookup is null then BOCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BOCHG.EffectiveDate as Effective_Date,
BOCHG.OldOutValue as Old_OS_Value_in_Millions,
BOCHG.NewOutValue as New_OS_Value_in_Millions,
BOCHG.OldOutDate as Old_Outstanding_Date,
BOCHG.NewOutDate as New_Outstanding_Date,
'WCA.dbo.BOCHG|BOCHGID|BOCHGNotes' as Link_Notes,
BOCHG.Actflag,
BOCHG.Acttime,
BOCHG.AnnounceDate,
BOCHG.BOCHGID as EventID,
BOCHG.SecID

	  into wfi.dbo.outstanding_amount

from BOCHG
left outer join WFI.dbo.sys_lookup as RELEVENT on BOCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup

--	WHERE
--	BOCHG.Acttime > (select max(wfi.dbo.outstanding_amount.acttime) from wfi.dbo.outstanding_amount)

	  WHERE
	  BOCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE outstanding_amount ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[outstanding_amount] WITH NOCHECK ADD 
	   CONSTRAINT [pk_outstanding_amount_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_outstanding_amount_SecID] ON [dbo].[outstanding_amount]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_outstanding_amount_Acttime] ON [dbo].[outstanding_amount]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'maturity_change')
	    drop table maturity_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from maturity_change)
--	delete from maturity_change
--	where EVENTID in
--	(select MtchgID from wca.dbo.MTCHG
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.maturity_change

SELECT 
case when RELEVENT.Lookup is null then MTCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
MTCHG.NotificationDate as Notification_Date,
MTCHG.OldMaturityDate as Old_Maturity_Date,
MTCHG.NewMaturityDate as New_Maturity_Date,
case when OLDMATBNHMRK.Lookup is null then MTCHG.OldMaturityBenchmark else OLDMATBNHMRK.Lookup end as Old_Maturity_Benchmark,
case when NEWMATBNHMRK.Lookup is null then MTCHG.NewMaturityBenchmark else NEWMATBNHMRK.Lookup end as New_Maturity_Benchmark,
'WCA.dbo.MTCHG|MTCHGID|Notes' as Link_Notes,
MTCHG.Actflag,
MTCHG.Acttime,
MTCHG.AnnounceDate,
MTCHG.MTCHGID as EventID,
MTCHG.SecID

	  into wfi.dbo.maturity_change

from MTCHG
inner join bond on mtchg.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on MTCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDMATBNHMRK on MTCHG.OldMaturityBenchmark = OLDMATBNHMRK.Code
                      and 'MATBNHMRK' = OLDMATBNHMRK.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWMATBNHMRK on MTCHG.NewMaturityBenchmark = NEWMATBNHMRK.Code
                      and 'MATBNHMRK' = NEWMATBNHMRK.TypeGroup

--	WHERE
--	MTCHG.Acttime > (select max(wfi.dbo.maturity_change.acttime) from wfi.dbo.maturity_change)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and MTCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE maturity_change ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[maturity_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_maturity_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_maturity_change_SecID] ON [dbo].[maturity_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_maturity_change_Acttime] ON [dbo].[maturity_change]([Acttime] desc) ON [PRIMARY]
	  GO



use wca

--	insert into wfi.dbo.conversion_terms

SELECT 
CONVT.FromDate as From_Date,
CONVT.ToDate as To_Date,
CONVT.RatioNew as New_Ratio,
CONVT.RatioOld as Old_Ratio,
CONVT.CurenCD as Conversion_Currency,
CONVT.Price as Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONVT.ResSecID,
RESSCMST.ISIN as Resultant_ISIN,
case when SECTYCD.Lookup is null then CONVT.SectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONVT.Fractions else FRACTIONS.Lookup end as Fractions,
CONVT.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
CONVT.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONVT|ConvtID|ConvtNotes' as Link_Notes,
CONVT.Actflag,
CONVT.Acttime,
CONVT.AnnounceDate,
CONVT.ConvtID as EventID,
CONVT.SecID

	  into wfi.dbo.conversion_terms

from CONVT
inner join bond on convt.secid = bond.secid
left outer join scmst as resscmst on convt.ressecid = resscmst.secid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONVT.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONVT.SectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONVT.Acttime > (select max(wfi.dbo.conversion_terms.acttime) from wfi.dbo.conversion_terms)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONVT.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE conversion_terms ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion_terms] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_terms_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_SecID] ON [dbo].[conversion_terms]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_Acttime] ON [dbo].[conversion_terms]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'conversion')
	    drop table conversion
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion)
--	delete from conversion
--	where EVENTID in
--	(select ConvID from wca.dbo.CONV
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.conversion

SELECT 
CONV.FromDate as From_Date,
CONV.ToDate as To_Date,
CONV.RatioNew as New_Ratio,
CONV.RatioOld as Old_Ratio,
CONV.CurenCD as Conversion_Currency,
CONV.Price as Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONV.ResSecID AS Resultant_SecID,
RESSCMST.ISIN as Resultant_ISIN,
case when SECTYCD.Lookup is null then CONV.ResSectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONV.Fractions else FRACTIONS.Lookup end as Fractions,
CONV.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
CONV.ConvType as Conversion_Type,
RD.Recdate as Record_Date,
CONV.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONV|ConvID|CONVNotes' as Link_Notes,
CONV.Actflag,
CONV.Acttime,
CONV.AnnounceDate,
CONV.ConvID as EventID,
CONV.SecID

	  into wfi.dbo.conversion

from CONV
inner join bond on conv.secid = bond.secid
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join rd on CONV.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONV.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONV.ResSectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONV.Acttime > (select max(wfi.dbo.conversion.acttime) from wfi.dbo.conversion)
--	and BOND.BondType <> 'PRF'
--	and Convtype<>'TENDER'


	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONV.Actflag<>'D'
	  and Convtype<>'TENDER'

GO


	  use WFI 
	  ALTER TABLE conversion ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_SecID] ON [dbo].[conversion]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_Acttime] ON [dbo].[conversion]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'reconvention')
	    drop table Reconvention
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Reconvention)
--	delete from Reconvention
--	where EVENTID in
--	(select RconvID from wca.dbo.RCONV
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Reconvention

SELECT 
RCONV.EffectiveDate as Effective_Date,
case when OLDINTACCRUAL.Lookup is null then RCONV.OldInterestAccrualConvention else OLDINTACCRUAL.Lookup end as Old_Accrual_Convention,
case when NEWINTACCRUAL.Lookup is null then RCONV.NewInterestAccrualConvention else NEWINTACCRUAL.Lookup end as New_Accrual_Convention,
case when OLDCMETHOD.Lookup is null then RCONV.OldConvMethod else OLDCMETHOD.Lookup end as Old_Convention_Method,
case when NEWCMETHOD.Lookup is null then RCONV.NewConvMethod else NEWCMETHOD.Lookup end as New_Convention_Method,
case when RELEVENT.Lookup is null then RCONV.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.RCONV|RconvID|Notes' as Link_Notes,
RCONV.Actflag,
RCONV.Acttime,
RCONV.AnnounceDate,
RCONV.RconvID as EventID,
RCONV.SecID

	  into wfi.dbo.Reconvention

from RCONV
left outer join WFI.dbo.sys_lookup as OLDINTACCRUAL on RCONV.OldInterestAccrualConvention = OLDINTACCRUAL.Code
                      and 'INTACCRUAL' = OLDINTACCRUAL.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTACCRUAL on RCONV.NewInterestAccrualConvention = NEWINTACCRUAL.Code
                      and 'INTACCRUAL' = NEWINTACCRUAL.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCMETHOD on RCONV.OldConvMethod = OLDCMETHOD.Code
                      and 'CMETHOD' = OLDCMETHOD.TypeGroup                        
left outer join WFI.dbo.sys_lookup as NEWCMETHOD on RCONV.NewConvMethod = NEWCMETHOD.Code
                      and 'CMETHOD' = NEWCMETHOD.TypeGroup                      
left outer join WFI.dbo.sys_lookup as RELEVENT on RCONV.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                                                          

--	WHERE
--	RCONV.Acttime > (select max(wfi.dbo.Reconvention.acttime) from wfi.dbo.Reconvention)

	  WHERE
	  RCONV.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE Reconvention ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Reconvention] WITH NOCHECK ADD 
	   CONSTRAINT [pk_Reconvention_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_Reconvention_SecID] ON [dbo].[Reconvention]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_Reconvention_Acttime] ON [dbo].[Reconvention]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'redenomination')
	    drop table Redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Redenomination)
--	delete from Redenomination
--	where EVENTID in
--	(select RdnomID from wca.dbo.RDNOM
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Redenomination

SELECT 
RDNOM.EffectiveDate as Effective_Date,
RDNOM.OldDenomination1 as Old_Denom1,
RDNOM.OldDenomination2 as Old_Denom2,
RDNOM.OldDenomination3 as Old_Denom3,
RDNOM.OldDenomination4 as Old_Denom4,
RDNOM.OldDenomination5 as Old_Denom5,
RDNOM.OldDenomination6 as Old_Denom6,
RDNOM.OldDenomination7 as Old_Denom7,
RDNOM.OldMinimumDenomination as Old_Min_Denom,
RDNOM.OldDenominationMultiple as Old_Denom_Multiple,
RDNOM.NewDenomination1 as New_Denom1,
RDNOM.NewDenomination2 as New_Denom2,
RDNOM.NewDenomination3 as New_Denom3,
RDNOM.NewDenomination4 as New_Denom4,
RDNOM.NewDenomination5 as New_Denom5,
RDNOM.NewDenomination6 as New_Denom6,
RDNOM.NewDenomination7 as New_Denom7,
RDNOM.NewMinimumDenomination as New_Min_Denom,
RDNOM.NewDenominationMultiple as New_Denom_Multiple,
'WCA.dbo.RDNOM|RdnomID|Notes' as Link_Notes,
RDNOM.Actflag,
RDNOM.Acttime,
RDNOM.AnnounceDate,
RDNOM.RdnomID as EventID,
RDNOM.SecID

	  into wfi.dbo.Redenomination

from RDNOM
inner join bond on rdnom.secid = bond.secid

--	WHERE
--	RDNOM.Acttime > (select max(wfi.dbo.Redenomination.acttime) from wfi.dbo.Redenomination)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and RDNOM.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE Redenomination ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_ReDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_ReDenom_SecID] ON [dbo].[Redenomination]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_ReDenom_Acttime] ON [dbo].[Redenomination]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'int_freq_change')
	    drop table int_freq_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from int_freq_change)
--	delete from int_freq_change
--	where EVENTID in
--	(select IfchgId from wca.dbo.IFCHG
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.int_freq_change

SELECT 
IFCHG.NotificationDate as Notification_Date,
case when OLDFREQ.Lookup is null then IFCHG.OldIntPayFrqncy else OLDFREQ.Lookup end as Old_Interest_Pay_Freq,
case when (len(oldintpaydate1) <> 4 or oldintpaydate1 is NULL or oldintpaydate1 = '' ) then ''
when ((cast(substring(oldintpaydate1,3,2) as int) < 1) or (cast(substring(oldintpaydate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate1,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate1,1,2)
end as Old_Pay_Date_1,
case when (len(oldintpaydate2) <> 4 or oldintpaydate2 is NULL or oldintpaydate2 = '' ) then ''
when ((cast(substring(oldintpaydate2,3,2) as int) < 1) or (cast(substring(oldintpaydate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate2,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate2,1,2)
end as Old_Pay_Date_2,
case when (len(oldintpaydate3) <> 4 or oldintpaydate3 is NULL or oldintpaydate3 = '' ) then ''
when ((cast(substring(oldintpaydate3,3,2) as int) < 1) or (cast(substring(oldintpaydate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate3,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate3,1,2)
end as Old_Pay_Date_3,
case when (len(oldintpaydate4) <> 4 or oldintpaydate4 is NULL or oldintpaydate4 = '' ) then ''
when ((cast(substring(oldintpaydate4,3,2) as int) < 1) or (cast(substring(oldintpaydate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate4,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate4,1,2)
end as Old_Pay_Date_4,
case when NEWFREQ.Lookup is null then IFCHG.NewIntPayFrqncy else NEWFREQ.Lookup end as New_Interest_Pay_Freq,
case when (len(newintpaydate1) <> 4 or newintpaydate1 is NULL or newintpaydate1 = '' ) then ''
when ((cast(substring(newintpaydate1,3,2) as int) < 1) or (cast(substring(newintpaydate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate1,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate1,1,2)
end as New_Pay_Date_1,
case when (len(newintpaydate2) <> 4 or newintpaydate2 is NULL or newintpaydate2 = '' ) then ''
when ((cast(substring(newintpaydate2,3,2) as int) < 1) or (cast(substring(newintpaydate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate2,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate2,1,2)
end as New_Pay_Date_2,
case when (len(newintpaydate3) <> 4 or newintpaydate3 is NULL or newintpaydate3 = '' ) then ''
when ((cast(substring(newintpaydate3,3,2) as int) < 1) or (cast(substring(newintpaydate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate3,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate3,1,2)
end as New_Pay_Date_3,
case when (len(newintpaydate4) <> 4 or newintpaydate4 is NULL or newintpaydate4 = '' ) then ''
when ((cast(substring(newintpaydate4,3,2) as int) < 1) or (cast(substring(newintpaydate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate4,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate4,1,2)
end as New_Pay_Date_4,
case when RELEVENT.Lookup is null then IFCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
IFCHG.OldVarIntPayDate as Old_Var_Interest_Pay_Date,
IFCHG.NewVarIntPayDate as New_Var_Interest_Pay_Date,
IFCHG.Actflag,
IFCHG.Acttime,
IFCHG.AnnounceDate,
IFCHG.IfchgId as EventID,
IFCHG.SecID

	  into wfi.dbo.int_freq_change

from IFCHG                                                      
left outer join WFI.dbo.sys_lookup as OLDFREQ on IFCHG.OldIntPayFrqncy = OLDFREQ.Code
                      and 'FREQ' = OLDFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWFREQ on IFCHG.NewIntPayFrqncy = NEWFREQ.Code
                      and 'FREQ' = NEWFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as RELEVENT on IFCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                       

--	WHERE
--	IFCHG.Acttime > (select max(wfi.dbo.int_freq_change.acttime) from wfi.dbo.int_freq_change)

	  WHERE
	  IFCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE int_freq_change ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[int_freq_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_int_freq_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_int_freq_change_SecID] ON [dbo].[int_freq_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_int_freq_change_Acttime] ON [dbo].[int_freq_change]([Acttime] desc) ON [PRIMARY]
	  GO



	  use WFI
	  if exists (select * from sysobjects where name = 'int_rate_change')
	    drop table int_rate_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from int_rate_change)
--	delete from int_rate_change
--	where EVENTID in
--	(select IRChgId from wca.dbo.IRCHG
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.int_rate_change

SELECT 
IRCHG.EffectiveDate as Effective_Date,
IRCHG.OldInterestRate as Old_Interest_Rate,
IRCHG.NewInterestRate as New_Interest_Rate,
case when RELEVENT.Lookup is null then IRCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.IRCHG|IRChgId|Notes' as Link_Notes,
IRCHG.Actflag,
IRCHG.Acttime,
IRCHG.AnnounceDate,
IRCHG.IRChgId as EventID,
IRCHG.SecID

	  into wfi.dbo.int_rate_change

from IRCHG                                                      
left outer join WFI.dbo.sys_lookup as RELEVENT on IRCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                       

--	WHERE
--	IRCHG.Acttime > (select max(wfi.dbo.int_rate_change.acttime) from wfi.dbo.int_rate_change)

	  WHERE
	  IRCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE int_rate_change ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[int_rate_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_int_rate_change_EVENTIDD] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_int_rate_change_SecID] ON [dbo].[int_rate_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_int_rate_change_Acttime] ON [dbo].[int_rate_change]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'consent')
	    drop table consent
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from consent)
--	delete from consent
--	where EVENTID in
--	(select RdID from wca.dbo.COSNT
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.consent

SELECT 
RD.recdate as Record_Date,
EXDT.exdate as Ex_Date,
EXDT.paydate as Pay_Date,
COSNT.ExpiryDate as Expiry_Date,
COSNT.ExpiryTime as Expiry_Time,
SUBSTRING(cosnt.TimeZone, 1,3) AS Time_Zone,
case when YNBLANK.Lookup is null then COSNT.CollateralRelease else YNBLANK.Lookup end as Collateral_Release,
COSNT.Currency as Redemption_Currency,
COSNT.Fee as Fee,
'WCA.dbo.COSNT|RdID|Notes' as Link_Notes,
COSNT.Actflag,
COSNT.Acttime,
COSNT.AnnounceDate,
COSNT.RdID as EventID,
RD.SecID 

	  into wfi.dbo.consent

from COSNT   
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCMST.PrimaryExchgCD = EXDT.ExchgCD AND 'COSNT' = EXDT.EventType
left outer join WFI.dbo.sys_lookup as YNBLANK on COSNT.CollateralRelease = YNBLANK.Code
                      and 'YNBLANK' = YNBLANK.TypeGroup
                      
--	WHERE
--	COSNT.Acttime > (select max(wfi.dbo.consent.acttime) from wfi.dbo.consent)

	  WHERE
	  COSNT.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE consent ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[consent] WITH NOCHECK ADD 
	   CONSTRAINT [pk_consent_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_liquidation_SecID] ON [dbo].[consent]([SecID]) ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_consent_Acttime] ON [dbo].[consent]([Acttime] desc) ON [PRIMARY]
	  GO



	  use WFI
	  if exists (select * from sysobjects where name = 'Exchange_Offer')
	    drop table Exchange_Offer
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Exchange_Offer)
--	delete from Exchange_Offer
--	where EVENTID in
--	(select ConvID from wca.dbo.CONV
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.Exchange_Offer

SELECT 
CONV.FromDate as From_Date,
CONV.ToDate as To_Date,
CONV.RatioNew as New_Ratio,
CONV.RatioOld as Old_Ratio,
CONV.CurenCD as Conversion_Currency,
CONV.Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONV.ResSecID AS Resultant_SecID,
RESSCMST.ISIN as Resultant_ISIN,
case when SECTYCD.Lookup is null then CONV.ResSectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONV.Fractions else FRACTIONS.Lookup end as Fractions,
CONV.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
RD.Recdate as Record_Date,
CONV.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONV|ConvID|CONVNotes' as Link_Notes,
CONV.Actflag,
CONV.Acttime,
CONV.AnnounceDate,
CONV.ConvID as EventID,
CONV.SecID

	  into wfi.dbo.Exchange_Offer

from CONV
inner join bond on conv.secid = bond.secid
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join rd on CONV.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONV.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONV.ResSectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONV.Acttime > (select max(wfi.dbo.Exchange_Offer.acttime) from wfi.dbo.Exchange_Offer)
--	and BOND.BondType <> 'PRF'
--	and Convtype='TENDER'


	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONV.Actflag<>'D'
	  and Convtype='TENDER'

GO

	  use WFI 
	  ALTER TABLE Exchange_Offer ALTER COLUMN EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Exchange_Offer] WITH NOCHECK ADD 
	   CONSTRAINT [pk_Exchange_Offer_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_Exchange_Offer_SecID] ON [dbo].[Exchange_Offer]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_Exchange_Offer_Acttime] ON [dbo].[Exchange_Offer]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'Tender')
	    drop table Tender
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Tender)
--	delete from Tender
--	where RedemID in
--	(select RedemID from wca.dbo.REDEM
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Tender

SELECT 
case when REDEM.RedemDate = '1800/01/01' then null else REDEM.RedemDate end as Redemption_Date, 
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
Poolfactor as Pool_Factor,
RedemPercent as Tender_Percent,
REDEM.CurenCD as Tender_Currency,
REDEM.AmountRedeemed as Tender_Amount,
REDEM.RedemPrice as Tender_Price,
REDEM.Priceaspercent as Price_as_Percent,
REDEM.RedemPremium as Tender_Premium,
REDEM.Premiumaspercent as Premium_as_Percent,
'WCA.dbo.REDEM|RedemID|RedemNotes' as Link_Notes,
REDEM.Actflag,
REDEM.Acttime,
REDEM.AnnounceDate,
REDEM.RedemID as EventID,
REDEM.SecID

	  into wfi.dbo.Tender

from REDEM
inner join bond on redem.secid = bond.secid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDEM.Redemtype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup

--	WHERE
--	REDEM.Acttime > (select max(wfi.dbo.Tender.acttime) from wfi.dbo.Tender)
--	and BOND.BondType <> 'PRF'
--	and redemtype='TENDER'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and REDEM.Actflag<>'D'
	  and redemtype='TENDER'

GO

	  use WFI 
	  ALTER TABLE Tender ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Tender] WITH NOCHECK ADD 
	   CONSTRAINT [pk_Tender_EventID] PRIMARY KEY ([EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_Tender_SecID] ON [dbo].[Tender]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_Tender_Acttime] ON [dbo].[Tender]([Acttime] desc) ON [PRIMARY]
	  GO



	  use WFI
	  if exists (select * from sysobjects where name = 'conversion_terms_change')
	    drop table conversion_terms_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion_terms_change)
--	delete from conversion_terms_change
--	where EVENTID in
--	(select CTCHGID from wca.dbo.CTCHG
--	where acttime>@StartDate)
--	GO


use wca

--	insert into wfi.dbo.conversion_terms_change

SELECT 
CTCHG.EffectiveDate as Effective_Date,
CTCHG.OldFromDate as Old_From_Date,
CTCHG.NewFromDate as New_From_Date,
CTCHG.OldToDate as Old_To_Date,
CTCHG.NewToDate as New_To_Date,
CTCHG.OldResultantRatio as Old_Resultant_Ratio,
CTCHG.NewResultantRatio as New_Resultant_Ratio,
CTCHG.OldSecurityRatio as Old_Security_Ratio,
CTCHG.NewSecurityRatio as New_Security_Ratio,
CTCHG.OldCurrency as Old_Currency,
CTCHG.NewCurrency as New_Currency,
CTCHG.OldConversionPrice as Old_ConversionPrice,
CTCHG.NewConversionPrice as New_ConversionPrice,
CTCHG.OldResSecID as Old_Resultant_SecID,
OLDRES.ISIN as Old_Resultant_ISIN,
CTCHG.NewResSecID as New_Resultant_SecID,
NEWRES.ISIN as New_Resultant_ISIN,
case when SECTYCD.Lookup is null then CTCHG.ResSectyCD else SECTYCD.Lookup end as Resultant_Type,
CTCHG.OldFXrate as Old_Foreign_Exchange_Rate,
CTCHG.NewFXrate as New_Foreign_Exchange_Rate,
CTCHG.OldPriceaspercent as Old_Price_as_Percent,
CTCHG.NewPriceaspercent as New_Price_as_Percent,
case when RELEVENT.Lookup is null then CTCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
CTCHG.RelEventID as Related_EventID,
CTCHG.ConvtID as Conversion_Terms_ID,
'WCA.dbo.CTCHG|CTCHGID|CTCHGNotes' as Link_Notes,
CTCHG.Actflag,
CTCHG.Acttime,
CTCHG.AnnounceDate,
CTCHG.CTCHGID as EventID,
CTCHG.SecID

	  into wfi.dbo.conversion_terms_change

from CTCHG
inner join bond on CTCHG.secid = bond.secid
left outer join scmst as oldres on CTCHG.oldressecid = oldres.secid
left outer join scmst as newres on CTCHG.newressecid = newres.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on CTCHG.Eventtype = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CTCHG.ResSectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CTCHG.Acttime > (select max(wfi.dbo.conversion_terms_change.acttime) from wfi.dbo.conversion_terms_change)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CTCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE conversion_terms_change ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion_terms_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_terms_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_change_SecID] ON [dbo].[conversion_terms_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_change_Acttime] ON [dbo].[conversion_terms_change]([Acttime] desc) ON [PRIMARY]
	  GO

	  use WFI
	  if exists (select * from sysobjects where name = 'takeover')
	    drop table takeover
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from takeover)
--	delete from takeover
--	where EVENTID in
--	(select TkovrID from wca.dbo.TKOVR
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.takeover

SELECT 
RD.recdate as Record_Date,
TKOVR.OfferorName as Offeror_Name,
case when TKOVR.Hostile='T' then 'Yes'
     when TKOVR.Hostile='F' then 'No'
     else TKOVR.Hostile end as Hostile, 
TKOVR.OpenDate as Open_Date,
TKOVR.CloseDate as Close_Date,
case when TKOVRSTAT.Lookup is null then TKOVR.TkovrStatus else TKOVRSTAT.Lookup end as Takeover_Status,
TKOVR.PreOfferQty as Pre_Offer_Quantity,
TKOVR.PreOfferPercent as Pre_Offer_Percent,
TKOVR.TargetQuantity as Target_Quantity,
TKOVR.TargetPercent as Target_Percent,
TKOVR.UnconditionalDate as Unconditional_Date,
TKOVR.CmAcqDate as Compulsory_Acquisition_Date,
TKOVR.MinAcpQty as Min_Acceptance_Quantity,
TKOVR.MaxAcpQty as Max_Acceptance_Quantity,
case when TKOVR.MiniTkovr='T' then 'Yes'
     when TKOVR.MiniTkovr='F' then 'No'
     else TKOVR.MiniTkovr end as Mini_Takeover, 
'WCA.dbo.TKOVR|TkovrID|TkovrNotes' as Link_Notes,
TKOVR.Actflag,
TKOVR.Acttime,
TKOVR.AnnounceDate,
TKOVR.TkovrID as EventID,
TKOVR.SecID 

	  into wfi.dbo.takeover

from TKOVR   
left outer join RD ON TKOVR.RdID = RD.RdID
inner join BOND on TKOVR.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as TKOVRSTAT on TKOVR.TkovrStatus = TKOVRSTAT.Code
                      and 'TKOVRSTAT' = TKOVRSTAT.TypeGroup

--	WHERE
--	TKOVR.Acttime > (select max(wfi.dbo.takeover.acttime) from wfi.dbo.takeover)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and TKOVR.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE takeover ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[takeover] WITH NOCHECK ADD 
	   CONSTRAINT [pk_takeover_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_takeover_Acttime] ON [dbo].[takeover]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'cur_redenomination')
	    drop table cur_redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from cur_redenomination)
--	delete from cur_redenomination
--	where EVENTID in
--	(select CurrdID from wca.dbo.CURRD
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.cur_redenomination

SELECT 
CURRD.EffectiveDate as Effective_Date,
CURRD.OldCurenCD as Old_Redemption_Currency,
CURRD.NewCurenCD as New_Redemption_Currency,
CURRD.OldParValue as Old_Par_Value,
CURRD.NewParValue as New_Par_Value,
case when RELEVENT.Lookup is null then CURRD.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.CURRD|CurrdID|CurRdNotes' as Link_Notes,
CURRD.Actflag,
CURRD.Acttime,
CURRD.AnnounceDate,
CURRD.CurrdID as EventID,
CURRD.SecID 

	  into wfi.dbo.cur_redenomination

from CURRD   
inner join BOND on CURRD.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on CURRD.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
                      
--	WHERE
--	CURRD.Acttime > (select max(wfi.dbo.cur_redenomination.acttime) from wfi.dbo.cur_redenomination)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CURRD.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE cur_redenomination ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[cur_redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_cur_reDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_cur_reDenom_Acttime] ON [dbo].[cur_redenomination]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'buy_back')
	    drop table buy_back
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from buy_back)
--	delete from buy_back
--	where EVENTID in
--	(select BBID from wca.dbo.BB
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.buy_back

SELECT 
RD.recdate as Record_Date,
case when ONOFFFLAG.Lookup is null then BB.OnOffFlag else ONOFFFLAG.Lookup end as On_Off_Market,
BB.StartDate as Start_Date,
BB.EndDate as End_Date,
BB.MinAcpQty as Min_Acceptance_Quantity,
BB.MaxAcpQty as Max_Acceptance_Quantity,
BB.BBMinPct as Min_Percent,
BB.BBMaxPct as Max_Percent,
'WCA.dbo.BB|BBID|BBNotes' as Link_Notes,
BB.Actflag,
BB.Acttime,
BB.AnnounceDate,
BB.BBID as EventID,
BB.SecID  

	  into wfi.dbo.buy_back

from BB   
left outer join RD ON BB.RdID = RD.RdID
inner join BOND on BB.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as ONOFFFLAG on BB.OnOffFlag = ONOFFFLAG.Code
                      and 'ONOFFFLAG' = ONOFFFLAG.TypeGroup
                                       
                      
--	WHERE
--	BB.Acttime > (select max(wfi.dbo.buy_back.acttime) from wfi.dbo.buy_back)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and BB.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE buy_back ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[buy_back] WITH NOCHECK ADD 
	   CONSTRAINT [pk_buy_back_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_buy_back_Acttime] ON [dbo].[buy_back]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'pv_redenomination')
	    drop table pv_redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from pv_redenomination)
--	delete from pv_redenomination
--	where EVENTID in
--	(select PvRdID from wca.dbo.PVRD
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.pv_redenomination

SELECT 
PVRD.EffectiveDate as Effective_Date,
PVRD.CurenCD as Redemption_Currency,
PVRD.OldParValue as Old_Par_Value,
PVRD.NewParValue as New_Par_Value,
case when RELEVENT.Lookup is null then PVRD.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.PVRD|PvRdID|PvRdNotes' as Link_Notes,
PVRD.Actflag,
PVRD.Acttime,
PVRD.AnnounceDate,
PVRD.PvRdID as EventID,
PVRD.SecID  

	  into wfi.dbo.pv_redenomination

from PVRD   
inner join BOND on PVRD.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on PVRD.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                      
                                       
                      
--	WHERE
--	PVRD.Acttime > (select max(wfi.dbo.pv_redenomination.acttime) from wfi.dbo.pv_redenomination)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and PVRD.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE pv_redenomination ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[pv_redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_pv_reDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_pv_reDenom_Acttime] ON [dbo].[pv_redenomination]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'announcement')
	    drop table announcement
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from announcement)
--	delete from announcement
--	where EVENTID in
--	(select AnnID from wca.dbo.ANN
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.announcement

SELECT 
case when RELEVENT.Lookup is null then ANN.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ANN.NotificationDate as Notification_Date,
'WCA.dbo.ANN|AnnID|AnnNotes' as Link_Notes,
ANN.Actflag,
ANN.Acttime,
ANN.AnnounceDate,
ANN.AnnID as EventID,
SCMST.SecID  

	  into wfi.dbo.announcement

from ANN   
inner join SCMST on ANN.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on ANN.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup  
                
                                                             
--	WHERE
--	ANN.Acttime > (select max(wfi.dbo.announcement.acttime) from wfi.dbo.announcement)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and ANN.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE announcement ALTER COLUMN EVENTID int NOT NULL
	  ALTER TABLE announcement ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[announcement] WITH NOCHECK ADD 
	   CONSTRAINT [pk_announcement] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_announcement_Acttime] ON [dbo].[announcement]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'liquidation')
	    drop table liquidation
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from liquidation)
--	delete from liquidation
--	where EVENTID in
--	(select LiqID from wca.dbo.LIQ
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.liquidation

SELECT 
LIQ.Liquidator as Liquidator,
LIQ.LiqAdd1 as Address1,
LIQ.LiqAdd2 as Address2,
LIQ.LiqAdd3 as Address3,
LIQ.LiqAdd4 as Address4,
LIQ.LiqAdd5 as Address5,
LIQ.LiqAdd6 as Address6,
LIQ.LiqCity as City,
case when CNTRY.Lookup is null then LIQ.LiqCntryCD else CNTRY.Lookup end as Cntry,
LIQ.LiqTel as Telephone,
LIQ.LiqFax as Fax,
LIQ.LiqEmail as Email,
LIQ.RdDate as Record_Date,
'WCA.dbo.LIQ|LiqID|LiquidationTerms' as Link_Notes,
LIQ.Actflag,
LIQ.Acttime,
LIQ.AnnounceDate,
LIQ.LiqID as EventID,
SCMST.SecID  

	  into wfi.dbo.liquidation

from LIQ   
inner join SCMST on LIQ.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as CNTRY on LIQ.LiqCntryCD = CNTRY.Code
                      and 'CNTRY' = CNTRY.TypeGroup     
                
                                                             
--	WHERE
--	LIQ.Acttime > (select max(wfi.dbo.liquidation.acttime) from wfi.dbo.liquidation)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and LIQ.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE liquidation ALTER COLUMN EVENTID int NOT NULL
	  ALTER TABLE liquidation ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[liquidation] WITH NOCHECK ADD 
	   CONSTRAINT [pk_liquidation_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_liquidation_Acttime] ON [dbo].[liquidation]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'bankruptcy')
	    drop table bankruptcy
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from bankruptcy)
--	delete from bankruptcy
--	where EVENTID in
--	(select BkrpID from wca.dbo.BKRP
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.bankruptcy

SELECT 
BKRP.NotificationDate as Notification_Date,
BKRP.FilingDate as Filing_Date,
'WCA.dbo.BKRP|BkrpID|BkrpNotes' as Link_Notes,
BKRP.Actflag,
BKRP.Acttime,
BKRP.AnnounceDate,
BKRP.BkrpID as EventID,
SCMST.SecID  

	  into wfi.dbo.bankruptcy

from BKRP   
inner join SCMST on BKRP.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID

                                                             
--	WHERE
--	BKRP.Acttime > (select max(wfi.dbo.bankruptcy.acttime) from wfi.dbo.bankruptcy)

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and BKRP.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE bankruptcy ALTER COLUMN  EventID int NOT NULL
	  ALTER TABLE bankruptcy ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[bankruptcy] WITH NOCHECK ADD 
	   CONSTRAINT [pk_bankruptcy_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_bankruptcy_Acttime] ON [dbo].[bankruptcy]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'legal_action')
	    drop table legal_action
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from legal_action)
--	delete from legal_action
--	where EVENTID in
--	(select LawstID from wca.dbo.LAWST
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Legal_Action

SELECT 
LAWST.EffectiveDate as Effective_Date,
CASE WHEN LAWST.LAType= 'CLSACT' THEN 'Class Action' ELSE 'Other' END as Legal_Action_Type,
LAWST.Regdate as Registration_Date,
'WCA.dbo.LAWST|LawstID|LawstNotes' as Link_Notes,
LAWST.Actflag,
LAWST.Acttime,
LAWST.AnnounceDate,
LAWST.LawstID as EventID,
SCMST.SecID  

	  into wfi.dbo.legal_action

from LAWST   
inner join SCMST on LAWST.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID

--	WHERE
--	LAWST.Acttime > (select max(wfi.dbo.legal_action.acttime) from wfi.dbo.legal_action)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and LAWST.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE legal_action ALTER COLUMN  EventID int NOT NULL
	  ALTER TABLE legal_action ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[legal_action] WITH NOCHECK ADD 
	   CONSTRAINT [pk_legal_action_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_legal_action_Acttime] ON [dbo].[legal_action]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'bondholder_meeting')
	    drop table bondholder_meeting
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from bondholder_meeting)
--	delete from bondholder_meeting
--	where AGMID in
--	(select AGMID from wca.dbo.AGM
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.bondholder_meeting

SELECT 
AGM.AGMDate as AGM_Date,
case when AGMEGM.Lookup is null then AGM.AGMEGM else AGMEGM.Lookup end as Meeting_Type,
--case when AGM.BondSecID  
AGM.AGMNo as AGM_Number,
AGM.FYEDate as Fin_Year_End_Date,
AGM.AGMTime as AGM_Time,
AGM.ADD1 as Address1,
AGM.ADD2 as Address2,
AGM.ADD3 as Address3,
AGM.ADD4 as Address4,
AGM.ADD5 as Address5,
AGM.ADD6 as Address6,
AGM.City as City,
case when CNTRY.Lookup is null then AGM.CntryCD else CNTRY.Lookup end as Cntry,
AGM.Actflag,
AGM.Acttime,
AGM.AnnounceDate,
AGM.AGMID as EventID,
AGM.BondSecID as SecID  

	  into wfi.dbo.bondholder_meeting

from AGM  
inner join BOND on AGM.BondSecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as AGMEGM on AGM.AGMEGM = AGMEGM.Code
                      and 'AGMEGM' = AGMEGM.TypeGroup  
left outer join WFI.dbo.sys_lookup as CNTRY on AGM.CntryCD = CNTRY.Code
                      and 'CNTRY' = CNTRY.TypeGroup                      
                                       
                      
--	WHERE
--	AGM.Acttime > (select max(wfi.dbo.bondholder_meeting.acttime) from wfi.dbo.bondholder_meeting)
--	AND BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and BondSecID is not null and BondSecID>0

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and BondSecID is not null and BondSecID>0
	  and AGM.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE bondholder_meeting ALTER COLUMN EventID int NOT NULL
	  ALTER TABLE bondholder_meeting ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[bondholder_meeting] WITH NOCHECK ADD 
	   CONSTRAINT [pk_bondholder_meeting_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_bondholder_meeting_Acttime] ON [dbo].[bondholder_meeting]([Acttime] desc) ON [PRIMARY]
	  GO

