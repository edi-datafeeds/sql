"center","select
CNTR.CntryCD as Center_Country,
CNTR.CentreName as Center_Name,
case when BDC.bdcappliedto='I' then 'Interest' when BDC.bdcappliedto='M' then 'Maturity' else BDC.bdcappliedto end as Applied_To,
'BDC|BdcID|Notes' as Link_Notes,
BDC.Actflag,
BDC.Acttime,
BDC.AnnounceDate,
BDC.BdcID as EventID,
scmst.secid
from BDC
left outer join CNTR on BDC.CntrID = CNTR.CntrID
inner join scmst on BDC.secid = scmst.secid
"
