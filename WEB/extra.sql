	  use WFI
	  if exists (select * from sysobjects where name = 'selling_restrictions')
	    drop table selling_restrictions
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from selling_restrictions)
--	delete from selling_restrictions
--	where EVENTID in
--	(select SELRTID from wca.dbo.SELRT
--	where acttime>@StartDate)


use wca

--	insert into wfi.dbo.selling_restrictions

*SelrtId	Integer	32 bit	10		Unique Internal Counter for this table
SecID	Integer	32 bit	10		Security Linking Key
ISIN	Char	Char String	12		ISIN from SCMST for reference purposes
CntryCD	Char	Char String	2	CNTRY	Country of Exchange
Restriction	Memo	Text No Carriage Return	1073741823		Notes applicable.

SELECT 
case when RELEVENT.Lookup is null then SELRT.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
SELRT.EffectiveDate as Effective_Date,
SELRT.OldOutValue as Old_OS_Value_in_Millions,
SELRT.NewOutValue as New_OS_Value_in_Millions,
SELRT.OldOutDate as Old_Outstanding_Date,
SELRT.NewOutDate as New_Outstanding_Date,
'WCA.dbo.SELRT|SELRTID|SELRTNotes' as Link_Notes,
SELRT.Actflag,
SELRT.Acttime,
SELRT.AnnounceDate,
SELRT.SELRTID as EventID,
SELRT.SecID

	  into wfi.dbo.selling_restrictions

from SELRT
left outer join WFI.dbo.sys_lookup as RELEVENT on SELRT.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup

--	WHERE
--	SELRT.Acttime > (select max(wfi.dbo.selling_restrictions.acttime) from wfi.dbo.selling_restrictions)

	  WHERE
	  SELRT.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE selling_restrictions ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[selling_restrictions] WITH NOCHECK ADD 
	   CONSTRAINT [pk_selling_restrictions_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_selling_restrictions_SecID] ON [dbo].[selling_restrictions]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_selling_restrictions_Acttime] ON [dbo].[selling_restrictions]([Acttime] desc) ON [PRIMARY]
	  GO



----------					
Series WFI - SCAGY - Security Agency -					

Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description
TableName	VarChar	Char String	10	TABLENAME	Table Name
Actflag	Char	Char String	1	ACTION	Record Level Action Status
AnnounceDate	Date	yyyy/mm/dd	10		Creation date of record at EDI
Acttime	Date	yyyy/mm/dd	10		Last Changed date of record at EDI
*ScagyID	Integer	32 bit	10		
SecID	Integer	32 bit	10		Link to SCMST TABLE
ISIN	Char	Char String	12		ISIN from SCMST for reference purposes
Relationship	VarChar	Char String	10	RELATION	
AgncyID	Integer	32 bit	10		Agency link
GuaranteeType	VarChar	Char String	10		In case of Guarantors the type of Guarantee provided. Credit Enhancements, Letter of Credit, Guarantors etc.
SpStartDate	Date	yyyy/mm/dd	10		Credit Enhancements, Securities offered or Guarantees made etc. by an agency to a Fixed Income security could have term i.e. they could come into existence during the life of the bond and expire before maturity.   Not all relationships will have the surety period.
SpEndDate	Date	yyyy/mm/dd	10		Credit Enhancements, Securities offered or Guarantees made etc. by an agency to a Fixed Income security could have term i.e. they could come into existence during the life of the bond and expire before maturity.   Not all relationships will have the surety period.



----------					
Series WFI - SACHG - Security Agency Change - 					

Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description
TableName	VarChar	Char String	10	TABLENAME	Table Name
Actflag	Char	Char String	1	ACTION	Record Level Action Status
AnnounceDate	Date	yyyy/mm/dd	10		Creation date of record at EDI
Acttime	Date	yyyy/mm/dd	10		Last Changed date of record at EDI
*SachgID	Integer	32 bit	10		
ScagyID	Integer	32 bit	10		
SecID	Integer	32 bit	10		Link to SCMST TABLE
ISIN	Char	Char String	12		ISIN from SCMST for reference purposes
EffectiveDate	Date	yyyy/mm/dd	10		Effective Date
OldRelationship	VarChar	Char String	10		
NewRelationship	VarChar	Char String	10		
OldAgncyID	Integer	32 bit	10		
NewAgncyID	Integer	32 bit	10		
OldSpStartDate	Date	yyyy/mm/dd	10		Credit Enhancements, Securities offered or Guarantees made etc. by an agency to a Fixed Income security could have term i.e. they could come into existence during the life of the bond and expire before maturity.   Not all relationships will have the surety period.
NewSpStartDate	Date	yyyy/mm/dd	10		Credit Enhancements, Securities offered or Guarantees made etc. by an agency to a Fixed Income security could have term i.e. they could come into existence during the life of the bond and expire before maturity.   Not all relationships will have the surety period.
OldSpEndDate	Date	yyyy/mm/dd	10		Credit Enhancements, Securities offered or Guarantees made etc. by an agency to a Fixed Income security could have term i.e. they could come into existence during the life of the bond and expire before maturity.   Not all relationships will have the surety period.
NewSpEndDate	Date	yyyy/mm/dd	10		Credit Enhancements, Securities offered or Guarantees made etc. by an agency to a Fixed Income security could have term i.e. they could come into existence during the life of the bond and expire before maturity.   Not all relationships will have the surety period.
OldGuaranteeType	VarChar	Char String	10		In case of Guarantors the type of Guarantee provided. Credit Enhancements, Letter of Credit, Guarantors etc.
NewGuaranteeType	VarChar	Char String	10		In case of Guarantors the type of Guarantee provided. Credit Enhancements, Letter of Credit, Guarantors etc.



----------					
Series WFI - AGNCY - Agency -					

Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description
TableName	VarChar	Char String	10	TABLENAME	Table Name
Actflag	Char	Char String	1	ACTION	Record Level Action Status
AnnounceDate	Date	yyyy/mm/dd	10		Creation date of record at EDI
Acttime	Date	yyyy/mm/dd	10		Last Changed date of record at EDI
*AgncyID	Integer	32 bit	10		
RegistrarName	VarChar	Char String	70		
Add1	VarChar	Char String	50		
Add2	VarChar	Char String	50		
Add3	VarChar	Char String	50		
Add4	VarChar	Char String	50		
Add5	VarChar	Char String	50		
Add6	VarChar	Char String	50		
City	VarChar	Char String	50		
CntryCD	Char	Char String	2	CNTRY	ISO Country of Agency
WebSite	VarChar	Char String	50		
Contact1	VarChar	Char String	50	
Tel1	VarChar	Char String	50	
Fax1	VarChar	Char String	50	
email1	VarChar	Char String	50	
Contact2	VarChar	Char String	50	
Tel2	VarChar	Char String	50	
Fax2	VarChar	Char String	50	
email2	VarChar	Char String	50	
Depository	Char	Char String	1	BOOLEAN


