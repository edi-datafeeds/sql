print ""
GO
print " Data Generation evf_Announcement, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Announcement')
	     drop table evf_Announcement

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Announcement)
--	delete from evf_Announcement
--	where EventID in
--	(select AnnID from wca.dbo.ANN
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Announcement

select distinct
v10s_ANN.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(EventID as char(8))) as Caref,
EventID,
v10s_ANN.AnnounceDate as Created,
v10s_ANN.Acttime as Changed,
v10s_ANN.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
CASE WHEN (v10s_ANN.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ANN.EventType) > 0) THEN '[' + v10s_ANN.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ANN.NotificationDate,
'M' as Choice,
'ANN|AnnID|AnnNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Announcement

FROM v10s_ANN
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_Ann.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ANN.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_ANN.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
	  '2002/01/01' AND  '2008/01/01'
------and v10s_ANN.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_ANN.NotificationDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_ANN.Acttime > (select max(wcaarch.dbo.evf_Announcement.changed) from wcaarch.dbo.evf_Announcement)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_ANN.NotificationDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print " Key Generation evf_Announcement ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Announcement ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Announcement] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Announcement] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Announcement] ON [dbo].[evf_Announcement]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Announcement] ON [dbo].[evf_Announcement]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Announcement] ON [dbo].[evf_Announcement]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Bankruptcy, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Bankruptcy')
	   drop table evf_Bankruptcy

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Bankruptcy)
--	delete from evf_Bankruptcy
--	where EventID in
--	(select BkrpID from wca.dbo.BKRP
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Bankruptcy

select distinct
v10s_BKRP.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_BKRP.EventID,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_BKRP.NotificationDate,
v10s_BKRP.FilingDate,
'M' as Choice,
'BKRP|BkrpID|BkrpNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Bankruptcy

FROM v10s_BKRP
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_BKRP.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_BKRP.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_BKRP.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_BKRP.NotificationDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_BKRP.Acttime > (select max(wcaarch.dbo.evf_Bankruptcy.changed) from wcaarch.dbo.evf_Bankruptcy)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_BKRP.NotificationDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Bankruptcy ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Bankruptcy ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [evf_Bankruptcy] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Bankruptcy] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Bankruptcy] ON [dbo].[evf_Bankruptcy]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Bankruptcy] ON [dbo].[evf_Bankruptcy]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Bankruptcy] ON [dbo].[evf_Bankruptcy]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Company_Meeting, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Company_Meeting')
	   drop table evf_Company_Meeting

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Company_Meeting)
--	delete from evf_Company_Meeting
--	where EventID in
--	(select AgmID from wca.dbo.AGM
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Company_Meeting

select distinct
v10s_AGM.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_AGM.EventID,
v10s_AGM.AnnounceDate as Created,
v10s_AGM.Acttime as Changed,
v10s_AGM.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_AGM.AGMDate,
v10s_AGM.AGMEGM,
v10s_AGM.AGMNO,
v10s_AGM.FYEDate as FinYearEndDate,
v10s_AGM.AGMTime,
v10s_AGM.Add1 as Address1,
v10s_AGM.Add2 as Address2,
v10s_AGM.Add3 as Address3,
v10s_AGM.Add4 as Address4,
v10s_AGM.Add5 as Address5,
v10s_AGM.Add6 as Address6,
v10s_AGM.City,
CASE WHEN (v10s_AGM.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_AGM.CntryCD) > 0) THEN '[' + v10s_AGM.CntryCD +'] not found' ELSE Cntry.Country END as Country,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Company_Meeting

FROM v10s_AGM
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_AGM.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN CNTRY ON v10s_AGM.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_AGM.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_AGM.ActFlag<>'D'
	  and v10s_AGM.AGMEGM<>'BHM'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_AGM.AGMDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_AGM.Acttime > (select max(wcaarch.dbo.evf_Company_Meeting.changed) from wcaarch.dbo.evf_Company_Meeting)
--	and v10s_AGM.AGMEGM<>'BHM'
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_AGM.AGMDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Company_Meeting ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Company_Meeting ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Company_Meeting] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Company_Meeting] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Company_Meeting] ON [dbo].[evf_Company_Meeting]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Company_Meeting] ON [dbo].[evf_Company_Meeting]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Company_Meeting] ON [dbo].[evf_Company_Meeting]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Financial_Year_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Financial_Year_Change')
	   drop table evf_Financial_Year_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Financial_Year_Change)
--	delete from evf_Financial_Year_Change
--	where EventID in
--	(select FychgID from wca.dbo.FYCHG
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Financial_Year_Change

select distinct
v10s_FYCHG.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_FYCHG.EventID,
v10s_FYCHG.AnnounceDate as Created,
v10s_FYCHG.Acttime as Changed,
v10s_FYCHG.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_FYCHG.NotificationDate,
v10s_FYCHG.OldFYStartDate as OldFinYearStart,
v10s_FYCHG.OldFYEndDate as OldFinYearEnd,
v10s_FYCHG.NewFYStartDate as NewFinYearStart,
v10s_FYCHG.NewFYEndDate as NewFinYearEnd,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Financial_Year_Change

FROM v10s_FYCHG
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_FYCHG.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_FYCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_FYCHG.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_FYCHG.NotificationDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_FYCHG.Acttime > (select max(wcaarch.dbo.evf_Financial_Year_Change.changed) from wcaarch.dbo.evf_Financial_Year_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_FYCHG.NotificationDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Financial_Year_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Financial_Year_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Financial_Year_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Financial_Year_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Financial_Year_Change] ON [dbo].[evf_Financial_Year_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Financial_Year_Change] ON [dbo].[evf_Financial_Year_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Financial_Year_Change] ON [dbo].[evf_Financial_Year_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Incorporation_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Incorporation_Change')
	   drop table evf_Incorporation_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Incorporation_Change)
--	delete from evf_Incorporation_Change
--	where EventID in
--	(select InchgID from wca.dbo.INCHG
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Incorporation_Change

select distinct
v10s_INCHG.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_INCHG.EventID,
v10s_INCHG.AnnounceDate as Created,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_INCHG.InChgDate as EffectiveDate,
CASE WHEN (v10s_INCHG.OldCntryCD is null) THEN OldCntry.Country WHEN (OldCntry.Country is null) and (LEN(v10s_INCHG.OldCntryCD) > 0) THEN '[' + v10s_INCHG.OldCntryCD +'] not found' ELSE OldCntry.Country END as OldCountry,
CASE WHEN (v10s_INCHG.NewCntryCD is null) THEN NewCntry.Country WHEN (NewCntry.Country is null) and (LEN(v10s_INCHG.NewCntryCD) > 0) THEN '[' + v10s_INCHG.NewCntryCD +'] not found' ELSE NewCntry.Country END as NewCountry,
CASE WHEN (v10s_INCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_INCHG.EventType) > 0) THEN '[' + v10s_INCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Incorporation_Change

FROM v10s_INCHG
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_INCHG.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_INCHG.EventType = EVENT.EventType
LEFT OUTER JOIN CNTRY as oldCNTRY ON v10s_INCHG.OldCntryCD = oldCNTRY.CntryCD
LEFT OUTER JOIN CNTRY as newCNTRY ON v10s_INCHG.NewCntryCD = newCNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_INCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_INCHG.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_INCHG.InChgDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_INCHG.Acttime > (select max(wcaarch.dbo.evf_Incorporation_Change.changed) from wcaarch.dbo.evf_Incorporation_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_INCHG.InChgDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Incorporation_Change ,please  wait ....."
	  GO
	  
	  use wcaarch
	  ALTER TABLE evf_Incorporation_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Incorporation_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Incorporation_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Incorporation_Change] ON [dbo].[evf_Incorporation_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Incorporation_Change] ON [dbo].[evf_Incorporation_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Incorporation_Change] ON [dbo].[evf_Incorporation_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Issuer_Name_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Issuer_Name_Change')
	   drop table evf_Issuer_Name_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Issuer_Name_Change)
--	delete from evf_Issuer_Name_Change
--	where EventID in
--	(select IschgID from wca.dbo.ISCHG
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Issuer_Name_Change)
--	delete from evf_Issuer_Name_Change
--	where EventID in
--	(select RelEventID from wca.dbo.ICC
--	where acttime>@Startdate
--	and Eventtype ='ISCHG')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Issuer_Name_Change)
--	delete from evf_Issuer_Name_Change
--	where EventID in
--	(select RelEventID from wca.dbo.SDCHG
--	where acttime>@Startdate
--	and Eventtype ='ISCHG')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Issuer_Name_Change)
--	delete from evf_Issuer_Name_Change
--	where EventID in
--	(select RelEventID from wca.dbo.LCC
--	where acttime>@Startdate
--	and Eventtype ='ISCHG')

GO

use wca

--	insert into wcaarch.dbo.evf_Issuer_Name_Change

select distinct
v10s_ISCHG.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_ISCHG.EventID,
v10s_ISCHG.AnnounceDate as Created,
v10s_ISCHG.Acttime as Changed,
v10s_ISCHG.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_WCA2_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_wca2_dSCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_WCA2_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_wca2_dSCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
CASE WHEN (v10s_ISCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ISCHG.EventType) > 0) THEN '[' + v10s_ISCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ISCHG.NameChangeDate,
v10s_ISCHG.IssOldName,
v10s_ISCHG.IssNewName,
v10s_ISCHG.LegalName,
'M' as Choice,
'ISCHG|IschgID|IschgNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Issuer_Name_Change

FROM v10s_ISCHG
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_ISCHG.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
------and ICC.ActFlag<>'D'
       AND v20c_WCA2_SCMST.secid = ICC.secid
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_wca2_dSCEXH.secid = SDCHG.secid
       AND v20c_wca2_dSCEXH.ExCountry = SDCHG.CntryCD 
       AND v20c_wca2_dSCEXH.RegCountry = SDCHG.RcntryCD
------       and SDCHG.ActFlag<>'D'
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_wca2_dSCEXH.ExchgCD = LCC.ExchgCD
       AND v20c_WCA2_SCMST.secid = LCC.secid
------       and LCC.ActFlag<>'D'
LEFT OUTER JOIN EVENT ON v10s_ISCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_ISCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
	  or ICC.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or SDCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or LCC.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and v10s_ISCHG.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_ISCHG.NameChangeDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_ISCHG.Acttime > (select max(wcaarch.dbo.evf_Issuer_Name_Change.changed) from wcaarch.dbo.evf_Issuer_Name_Change)
--	or ICC.Acttime > (select max(wcaarch.dbo.evf_Issuer_Name_Change.changed) from wcaarch.dbo.evf_Issuer_Name_Change)
--	or SDCHG.Acttime > (select max(wcaarch.dbo.evf_Issuer_Name_Change.changed) from wcaarch.dbo.evf_Issuer_Name_Change)
--	or LCC.Acttime > (select max(wcaarch.dbo.evf_Issuer_Name_Change.changed) from wcaarch.dbo.evf_Issuer_Name_Change))
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_ISCHG.NameChangeDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Issuer_Name_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Issuer_Name_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Issuer_Name_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Issuer_Name_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Issuer_Name_Change] ON [dbo].[evf_Issuer_Name_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Issuer_Name_Change] ON [dbo].[evf_Issuer_Name_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Issuer_Name_Change] ON [dbo].[evf_Issuer_Name_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Class_Action, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Class_Action')
	   drop table evf_Class_Action

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Class_Action)
--	delete from evf_Class_Action
--	where EventID in
--	(select LawstID from wca.dbo.LAWST
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Class_Action

select distinct
v10s_CLACT.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_CLACT.EventID,
v10s_CLACT.AnnounceDate as Created,
v10s_CLACT.Acttime as Changed,
v10s_CLACT.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_CLACT.EffectiveDate,
'M' as Choice,
'LAWST|LawstID|LawstNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Class_Action

FROM v10s_CLACT
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_CLACT.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_CLACT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_CLACT.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_CLACT.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_CLACT.Acttime > (select max(wcaarch.dbo.evf_Class_Action.changed) from wcaarch.dbo.evf_Class_Action)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_CLACT.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Class_Action ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Class_Action ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Class_Action] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Class_Action] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Class_Action] ON [dbo].[evf_Class_Action]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Class_Action] ON [dbo].[evf_Class_Action]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Class_Action] ON [dbo].[evf_Class_Action]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Liquidation, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Liquidation')
	   drop table evf_Liquidation

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Liquidation)
--	delete from evf_Liquidation
--	where EventID in
--	(select LiqID from wca.dbo.LIQ
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Liquidation)
--	delete from evf_Liquidation
--	where EventID in
--	(select EventID from wca.dbo.MPAY
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Liquidation

select distinct
v10s_LIQ.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('0'+rtrim(cast(serialid as char(2))),len(serialid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(MPAY.EventID as char(8)))
else cast(Seqnum as char(1))+'0101'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_LIQ.EventID as char(8)))
end as Caref,
v10s_LIQ.EventID,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as [Changed],
v10s_LIQ.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
MPAY.Paytype,
MPAY.ActFlag as ActionMPAY,
MPAY.Paydate as LiquidationDate,
'Price' AS RateType,
MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_LIQ.Liquidator,
v10s_LIQ.LiqAdd1 as Address1,
v10s_LIQ.LiqAdd2 as Address2,
v10s_LIQ.LiqAdd3 as Address3,
v10s_LIQ.LiqAdd4 as Address4,
v10s_LIQ.LiqAdd5 as Address5,
v10s_LIQ.LiqAdd6 as Address6,
v10s_LIQ.LiqCity as City,
CASE WHEN (v10s_LIQ.LiqCntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_LIQ.LiqCntryCD) > 0) THEN '[' + v10s_LIQ.LiqCntryCD +'] not found' ELSE Cntry.Country END as Country,
v10s_LIQ.LiqTel as Telephone,
v10s_LIQ.LiqFax as Fax,
v10s_LIQ.LiqEmail as Email,
'M' as Choice,
'LIQ|LiqID|LiquidationTerms' as Link_Terms,
'n/a' as Ratio,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Liquidation

FROM v10s_LIQ
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.IssID = v10s_LIQ.IssID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN CNTRY ON v10s_LIQ.LiqCntryCD = CNTRY.CntryCD
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD

	  WHERE
	  v10s_LIQ.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_LIQ.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_LIQ.AnnounceDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_LIQ.Acttime > (select max(wcaarch.dbo.evf_Liquidation.changed) from wcaarch.dbo.evf_Liquidation)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_LIQ.AnnounceDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Liquidation ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Liquidation ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Liquidation] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Liquidation] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Liquidation] ON [dbo].[evf_Liquidation]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Liquidation] ON [dbo].[evf_Liquidation]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Liquidation] ON [dbo].[evf_Liquidation]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_International_Code_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_International_Code_Change')
	   drop table evf_International_Code_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_International_Code_Change)
--	delete from evf_International_Code_Change
--	where EventID in
--	(select IccID from wca.dbo.ICC
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_International_Code_Change

select distinct
v10s_ICC.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_ICC.EventID,
v10s_ICC.AnnounceDate as Created,
v10s_ICC.Acttime as Changed,
v10s_ICC.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_ICC.EffectiveDate,
v10s_ICC.OldISIN,
v10s_ICC.NewISIN,
v10s_ICC.OldUSCode,
v10s_ICC.NewUSCode,
CASE WHEN (v10s_ICC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ICC.EventType) > 0) THEN '[' + v10s_ICC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_International_Code_Change

FROM v10s_ICC
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_ICC.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ICC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_ICC.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_ICC.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_ICC.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_ICC.Acttime > (select max(wcaarch.dbo.evf_International_Code_Change.changed) from wcaarch.dbo.evf_International_Code_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_ICC.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_International_Code_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_International_Code_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_International_Code_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_International_Code_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_International_Code_Change] ON [dbo].[evf_International_Code_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_International_Code_Change] ON [dbo].[evf_International_Code_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_International_Code_Change] ON [dbo].[evf_International_Code_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Security_Description_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Security_Description_Change')
	   drop table evf_Security_Description_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Security_Description_Change)
--	delete from evf_Security_Description_Change
--	where EventID in
--	(select ScchgID from wca.dbo.SCCHG
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Security_Description_Change

select distinct
v10s_SCCHG.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_SCCHG.EventID,
v10s_SCCHG.AnnounceDate as Created,
v10s_SCCHG.Acttime as Changed,
v10s_SCCHG.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_SCCHG.DateofChange,
CASE WHEN (v10s_SCCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SCCHG.EventType) > 0) THEN '[' + v10s_SCCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SCCHG.SecOldName as OldName,
v10s_SCCHG.SecNewName as NewName,
'M' as Choice,
'SCCHG|ScchgID|ScChgNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Security_Description_Change

FROM v10s_SCCHG
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_SCCHG.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_SCCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_SCCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_SCCHG.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_SCCHG.DateofChange
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_SCCHG.Acttime > (select max(wcaarch.dbo.evf_Security_Description_Change.changed) from wcaarch.dbo.evf_Security_Description_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_SCCHG.DateofChange
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Security_Description_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Security_Description_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Security_Description_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Security_Description_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Security_Description_Change] ON [dbo].[evf_Security_Description_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Security_Description_Change] ON [dbo].[evf_Security_Description_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Security_Description_Change] ON [dbo].[evf_Security_Description_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Listing_Status_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Listing_Status_Change')
	   drop table evf_Listing_Status_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Listing_Status_Change)
--	delete from evf_Listing_Status_Change
--	where EventID in
--	(select LstatID from wca.dbo.LSTAT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Listing_Status_Change

select distinct
v10s_LSTAT.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_LSTAT.EventID,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
CASE WHEN (v10s_LSTAT.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LSTAT.EventType) > 0) THEN '[' + v10s_LSTAT.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LSTAT.LStatStatus as ListStatChangedTo,
v10s_LSTAT.NotificationDate,
v10s_LSTAT.EffectiveDate,
'M' as Choice,
'LSTAT|LstatID|Reason' as Link_Reason,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Listing_Status_Change

FROM v10s_LSTAT
INNER JOIN v20c_wca2_dSCEXH ON v10s_LSTAT.SecID = v20c_wca2_dSCEXH.SecID AND v10s_LSTAT.ExchgCD = v20c_wca2_dSCEXH.ExchgCD
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_LSTAT.SecID
LEFT OUTER JOIN EVENT ON v10s_LSTAT.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_LSTAT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_LSTAT.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_LSTAT.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_LSTAT.Acttime > (select max(wcaarch.dbo.evf_Listing_Status_Change.changed) from wcaarch.dbo.evf_Listing_Status_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_LSTAT.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Listing_Status_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Listing_Status_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Listing_Status_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Listing_Status_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Listing_Status_Change] ON [dbo].[evf_Listing_Status_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Listing_Status_Change] ON [dbo].[evf_Listing_Status_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Listing_Status_Change] ON [dbo].[evf_Listing_Status_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Local_Code_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Local_Code_Change')
	   drop table evf_Local_Code_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Local_Code_Change)
--	delete from evf_Local_Code_Change
--	where EventID in
--	(select LccID from wca.dbo.LCC
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Local_Code_Change

select distinct
v10s_LCC.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_LCC.EventID,
v10s_LCC.AnnounceDate as Created,
v10s_LCC.Acttime as Changed,
v10s_LCC.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
CASE WHEN (v10s_LCC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LCC.EventType) > 0) THEN '[' + v10s_LCC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LCC.EffectiveDate,
v10s_LCC.OldLocalCode,
v10s_LCC.NewLocalCode,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Local_Code_Change

FROM v10s_LCC
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_LCC.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID AND v10s_LCC.ExchgCD = v20c_wca2_dSCEXH.ExchgCD
LEFT OUTER JOIN EVENT ON v10s_LCC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_LCC.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_LCC.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_LCC.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_LCC.Acttime > (select max(wcaarch.dbo.evf_Local_Code_Change.changed) from wcaarch.dbo.evf_Local_Code_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_LCC.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Local_Code_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Local_Code_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Local_Code_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Local_Code_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Local_Code_Change] ON [dbo].[evf_Local_Code_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Local_Code_Change] ON [dbo].[evf_Local_Code_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Local_Code_Change] ON [dbo].[evf_Local_Code_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Lot_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Lot_Change')
	   drop table evf_Lot_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Lot_Change)
--	delete from evf_Lot_Change
--	where EventID in
--	(select LtchgID from wca.dbo.LTCHG
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Lot_Change

select distinct
v10s_LTCHG.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_LTCHG.EventID,
v10s_LTCHG.AnnounceDate as Created,
v10s_LTCHG.Acttime as Changed,
v10s_LTCHG.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_LTCHG.EffectiveDate,
v10s_LTCHG.OldLot as OldLotSize,
v10s_LTCHG.NewLot as NewLotSize,
v10s_LTCHG.OldMinTrdQty as OldMinTradingQuant,
v10s_LTCHG.NewMinTrdgQty as NewMinTradingQuant,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Lot_Change

FROM v10s_LTCHG
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_LTCHG.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID AND v10s_LTCHG.ExchgCD = v20c_wca2_dSCEXH.ExchgCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_LTCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_LTCHG.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_LTCHG.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_LTCHG.Acttime > (select max(wcaarch.dbo.evf_Lot_Change.changed) from wcaarch.dbo.evf_Lot_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_LTCHG.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Lot_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Lot_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Lot_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Lot_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Lot_Change] ON [dbo].[evf_Lot_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Lot_Change] ON [dbo].[evf_Lot_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Lot_Change] ON [dbo].[evf_Lot_Change]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_New_Listing, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_New_Listing')
	   drop table evf_New_Listing

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_New_Listing)
--	delete from evf_New_Listing
--	where EventID in
--	(select ScexhID from wca.dbo.NLIST
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_New_Listing

select distinct
v10s_NLIST1.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_NLIST1.EventID,
v10s_NLIST1.AnnounceDate as Created,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_New_Listing

FROM v10s_NLIST1
INNER JOIN v20c_wca2_dSCEXH ON v10s_NLIST1.EventID = v20c_wca2_dSCEXH.ScexhID
INNER JOIN v20c_WCA2_SCMST ON v20c_wca2_dSCEXH.SecID = v20c_WCA2_SCMST.SecID
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_NLIST1.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_NLIST1.ActFlag<>'D'

--	WHERE
--	v10s_NLIST1.Acttime > (select max(wcaarch.dbo.evf_New_Listing.changed) from wcaarch.dbo.evf_New_Listing)

GO

	  print ""
	  GO
	  print "Key Generation evf_New_Listing ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_New_Listing ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_New_Listing] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_New_Listing] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_New_Listing] ON [dbo].[evf_New_Listing]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_New_Listing] ON [dbo].[evf_New_Listing]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_New_Listing] ON [dbo].[evf_New_Listing]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Sedol_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Sedol_Change')
	   drop table evf_Sedol_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Sedol_Change)
--	delete from evf_Sedol_Change
--	where EventID in
--	(select SdchgID from wca.dbo.SDCHG
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Sedol_Change

select distinct
v10s_SDCHG.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_SDCHG.EventID,
v10s_SDCHG.AnnounceDate as Created,
v10s_SDCHG.Acttime as Changed,
v10s_SDCHG.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
CASE WHEN (v10s_SDCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SDCHG.EventType) > 0) THEN '[' + v10s_SDCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SDCHG.CntryCD as CntryofSedol,
v10s_SDCHG.EffectiveDate,
v10s_SDCHG.OldSEDOL,
v10s_SDCHG.NewSEDOL,
v10s_SDCHG.CntryCD as OldCountry,
v10s_SDCHG.NewCntryCD as NewCountry,
v10s_SDCHG.RCntryCD as OldRegCountry,
v10s_SDCHG.NewRCntryCD as NewRegCountry,
'M' as Choice,
'SDCHG|SdchgID|SdChgNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Sedol_Change

FROM v10s_SDCHG
INNER JOIN v20c_WCA2_SCMST ON v10s_SDCHG.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID AND v10s_SDCHG.CntryCD = v20c_wca2_dSCEXH.ExCountry
LEFT OUTER JOIN EVENT ON v10s_SDCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_SDCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01' 
------and v10s_SDCHG.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_SDCHG.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_SDCHG.Acttime > (select max(wcaarch.dbo.evf_Sedol_Change.changed) from wcaarch.dbo.evf_Sedol_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_SDCHG.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Sedol_Change ,please  wait ....."
	  GO
	  
	  use wcaarch 
	  ALTER TABLE evf_Sedol_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Sedol_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Sedol_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Sedol_Change] ON [dbo].[evf_Sedol_Change]([secid]) ON [PRIMARY] 
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Sedol_Change] ON [dbo].[evf_Sedol_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Sedol_Change] ON [dbo].[evf_Sedol_Change]([changed]) ON [PRIMARY]
	  GO


--------------------------------------------------------------------------------------------------------------------------------------------------------------------	--	--	--	--	--	--	--		--	use wcaarch 
print ""
GO
print "Data Generation evf_Certificate_Exchange, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Certificate_Exchange')
	   drop table evf_Certificate_Exchange

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Certificate_Exchange)
--	delete from evf_Certificate_Exchange
--	where EventID in
--	(select CtxID from wca.dbo.CTX
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Certificate_Exchange

select distinct
v10s_CTX.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_CTX.EventID,
v10s_CTX.AnnounceDate as Created,
v10s_CTX.Acttime as Changed,
v10s_CTX.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_CTX.StartDate,
v10s_CTX.EndDate,
CASE WHEN (v10s_CTX.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_CTX.EventType) > 0) THEN '[' + v10s_CTX.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_CTX.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (v10s_CTX.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_CTX.SectyCD) > 0) THEN '[' + v10s_CTX.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
'CTX|CtxID|CtxNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Certificate_Exchange

FROM v10s_CTX
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_CTX.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_CTX.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_CTX.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON v10s_CTX.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN EVENT ON v10s_CTX.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE v10s_CTX.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_CTX.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_CTX.StartDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_CTX.Acttime > (select max(wcaarch.dbo.evf_Certificate_Exchange.changed) from wcaarch.dbo.evf_Certificate_Exchange)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_CTX.StartDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Certificate_Exchange ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Certificate_Exchange ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Certificate_Exchange] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Certificate_Exchange] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Certificate_Exchange] ON [dbo].[evf_Certificate_Exchange]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Certificate_Exchange] ON [dbo].[evf_Certificate_Exchange]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Certificate_Exchange] ON [dbo].[evf_Certificate_Exchange]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Currency_Redenomination, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Currency_Redenomination')
	   drop table evf_Currency_Redenomination

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Currency_Redenomination)
--	delete from evf_Currency_Redenomination
--	where EventID in
--	(select CurrdID from wca.dbo.CURRD
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Currency_Redenomination

select distinct
v10s_CURRD.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_CURRD.EventID,
v10s_CURRD.AnnounceDate as Created,
v10s_CURRD.Acttime as Changed,
v10s_CURRD.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_CURRD.EffectiveDate,
CASE WHEN (v10s_CURRD.OldCurenCD is null) THEN oldCUREN.Currency WHEN (oldCUREN.Currency is null) and (LEN(v10s_CURRD.OldCurenCD) > 0) THEN '[' + v10s_CURRD.OldCurenCD +'] not found' ELSE oldCUREN.Currency END as OldCurrency,
CASE WHEN (v10s_CURRD.NewCurenCD is null) THEN newCUREN.Currency WHEN (newCUREN.Currency is null) and (LEN(v10s_CURRD.NewCurenCD) > 0) THEN '[' + v10s_CURRD.NewCurenCD +'] not found' ELSE newCUREN.Currency END as NewCurrency,
v10s_CURRD.OldParValue,
v10s_CURRD.NewParValue,
CASE WHEN (v10s_CURRD.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_CURRD.EventType) > 0) THEN '[' + v10s_CURRD.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
'CURRD|CurrdID|CurrdNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Currency_Redenomination

FROM v10s_CURRD
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_CURRD.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_CURRD.EventType = EVENT.EventType
LEFT OUTER JOIN CUREN as oldCUREN ON v10s_CURRD.OldCurenCD = oldCUREN.CurenCD
LEFT OUTER JOIN CUREN as newCUREN ON v10s_CURRD.NewCurenCD = newCUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE v10s_CURRD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_CURRD.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_CURRD.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_CURRD.Acttime > (select max(wcaarch.dbo.evf_Currency_Redenomination.changed) from wcaarch.dbo.evf_Currency_Redenomination)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_CURRD.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Currency_Redenomination ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Currency_Redenomination ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Currency_Redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Currency_Redenomination] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Currency_Redenomination] ON [dbo].[evf_Currency_Redenomination]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Currency_Redenomination] ON [dbo].[evf_Currency_Redenomination]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Currency_Redenomination] ON [dbo].[evf_Currency_Redenomination]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Parvalue_Redenomination, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Parvalue_Redenomination')
	   drop table evf_Parvalue_Redenomination

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Parvalue_Redenomination)
--	delete from evf_Parvalue_Redenomination
--	where EventID in
--	(select PvrdID from wca.dbo.PVRD
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Parvalue_Redenomination

select distinct
v10s_PVRD.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_PVRD.EventID,
v10s_PVRD.AnnounceDate as Created,
v10s_PVRD.Acttime as Changed,
v10s_PVRD.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
CASE WHEN (v10s_PVRD.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_PVRD.EventType) > 0) THEN '[' + v10s_PVRD.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_PVRD.EffectiveDate,
CASE WHEN (v10s_PVRD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PVRD.CurenCD) > 0) THEN '[' + v10s_PVRD.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PVRD.OldParValue,
v10s_PVRD.NewParValue,
'M' as Choice,
'PVRD|PvrdID|PvrdNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Parvalue_Redenomination

FROM v10s_PVRD
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_PVRD.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_PVRD.EventType = EVENT.EventType
LEFT OUTER JOIN CUREN ON v10s_PVRD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE v10s_PVRD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_PVRD.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_PVRD.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_PVRD.Acttime > (select max(wcaarch.dbo.evf_Parvalue_Redenomination.changed) from wcaarch.dbo.evf_Parvalue_Redenomination)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_PVRD.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Parvalue_Redenomination ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Parvalue_Redenomination ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Parvalue_Redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Parvalue_Redenomination] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Parvalue_Redenomination] ON [dbo].[evf_Parvalue_Redenomination]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Parvalue_Redenomination] ON [dbo].[evf_Parvalue_Redenomination]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Parvalue_Redenomination] ON [dbo].[evf_Parvalue_Redenomination]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Preference_Conversion, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Preference_Conversion')
	   drop table evf_Preference_Conversion

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Preference_Conversion)
--	delete from evf_Preference_Conversion
--	where EventID in
--	(select ConvID from wca.dbo.CONV
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Preference_Conversion

select distinct
v10s_PRFCONV.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_PRFCONV.ConvID as EventID,
v10s_PRFCONV.AnnounceDate as Created,
v10s_PRFCONV.Acttime as Changed,
v10s_PRFCONV.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_PRFCONV.RatioNew +':'+v10s_PRFCONV.RatioOld as Ratio,
'Price' AS RateType,
v10s_PRFCONV.Price as Rate,
CASE WHEN (v10s_PRFCONV.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRFCONV.CurenCD) > 0) THEN '[' + v10s_PRFCONV.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRFCONV.FromDate,
v10s_PRFCONV.ToDate,
v10s_PRFCONV.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (v10s_PRFCONV.ResSectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_PRFCONV.ResSectyCD) > 0) THEN '[' + v10s_PRFCONV.ResSectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
CASE WHEN v10s_PRFCONV.MandOptFlag = 'M' THEN 'M' WHEN v10s_PRFCONV.MandOptFlag = 'V' THEN 'V'ELSE '' END as Choice,
CASE WHEN v10s_PRFCONV.PartFinalFlag='P' THEN 'Part' WHEN v10s_PRFCONV.PartFinalFlag='F' THEN 'Final' ELSE '' END as PartFinal,
'CONV|ConvID|ConvNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Preference_Conversion

FROM v10s_PRFCONV
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_PRFCONV.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_PRFCONV.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_PRFCONV.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON v10s_PRFCONV.ResSectyCD = SECTY.SectyCD
LEFT OUTER JOIN CUREN ON v10s_PRFCONV.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE v10s_PRFCONV.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_PRFCONV.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_PRFCONV.FromDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_PRFCONV.Acttime > (select max(wcaarch.dbo.evf_Preference_Conversion.changed) from wcaarch.dbo.evf_Preference_Conversion)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_PRFCONV.FromDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Preference_Conversion ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Preference_Conversion ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Preference_Conversion] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Preference_Conversion] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Preference_Conversion] ON [dbo].[evf_Preference_Conversion]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Preference_Conversion] ON [dbo].[evf_Preference_Conversion]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Preference_Conversion] ON [dbo].[evf_Preference_Conversion]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Preference_Redemption, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Preference_Redemption')
	   drop table evf_Preference_Redemption

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Preference_Redemption)
--	delete from evf_Preference_Redemption
--	where EventID in
--	(select RedemID from wca.dbo.REDEM
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Preference_Redemption

select distinct
v10s_PRFREDEM.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_PRFREDEM.EventID,
v10s_PRFREDEM.AnnounceDate as Created,
v10s_PRFREDEM.Acttime as Changed,
v10s_PRFREDEM.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
'Price' AS RateType,
v10s_PRFREDEM.RedemPrice as Rate,
CASE WHEN (v10s_PRFREDEM.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRFREDEM.CurenCD) > 0) THEN '[' + v10s_PRFREDEM.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRFREDEM.RedemDate as RedemptionDate,
CASE WHEN v10s_PRFREDEM.PartFinal='P' THEN 'Part' WHEN v10s_PRFREDEM.PartFinal='F' THEN 'Final' ELSE '' END as PartFinal,
CASE WHEN v10s_PRFREDEM.MandOptFlag = 'M' THEN 'M' WHEN v10s_PRFREDEM.MandOptFlag = 'V' THEN 'V'ELSE '' END as Choice,
'Redem|RedemID|RedemNotes' as Link_Notes,
'n/a' as Ratio,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Preference_Redemption

FROM v10s_PRFREDEM
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_PRFREDEM.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN CUREN ON v10s_PRFREDEM.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE v10s_PRFREDEM.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_PRFREDEM.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_PRFREDEM.RedemDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_PRFREDEM.Acttime > (select max(wcaarch.dbo.evf_Preference_Redemption.changed) from wcaarch.dbo.evf_Preference_Redemption)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_PRFREDEM.RedemDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Preference_Redemption ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Preference_Redemption ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Preference_Redemption] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Preference_Redemption] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Preference_Redemption] ON [dbo].[evf_Preference_Redemption]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Preference_Redemption] ON [dbo].[evf_Preference_Redemption]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Preference_Redemption] ON [dbo].[evf_Preference_Redemption]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Security_Reclassification, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Security_Reclassification')
	   drop table evf_Security_Reclassification

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Security_Reclassification)
--	delete from evf_Security_Reclassification
--	where EventID in
--	(select SecrcID from wca.dbo.SECRC
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Security_Reclassification

select distinct
v10s_SECRC.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_SECRC.EventID,
v10s_SECRC.AnnounceDate as Created,
v10s_SECRC.Acttime as Changed,
v10s_SECRC.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
CASE WHEN (v10s_SECRC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SECRC.EventType) > 0) THEN '[' + v10s_SECRC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SECRC.EffectiveDate,
CASE WHEN (v10s_SECRC.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_SECRC.SectyCD) > 0) THEN '[' + v10s_SECRC.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_SECRC.RatioNew +':'+v10s_SECRC.RatioOld as Ratio,
v10s_SECRC.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
'M' as Choice,
'SECRC|SecrcID|SecrcNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Security_Reclassification

FROM v10s_SECRC
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_SECRC.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_SECRC.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_SECRC.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON v10s_SECRC.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN EVENT ON v10s_SECRC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE v10s_SECRC.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_SECRC.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_SECRC.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_SECRC.Acttime > (select max(wcaarch.dbo.evf_Security_Reclassification.changed) from wcaarch.dbo.evf_Security_Reclassification)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_SECRC.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Data Generation evf_Security_Reclassification ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Security_Reclassification ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Security_Reclassification] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Security_Reclassification] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Security_Reclassification] ON [dbo].[evf_Security_Reclassification]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Security_Reclassification] ON [dbo].[evf_Security_Reclassification]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Security_Reclassification] ON [dbo].[evf_Security_Reclassification]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Assimilation, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Assimilation')
	   drop table evf_Assimilation

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Assimilation)
--	delete from evf_Assimilation
--	where EventID in
--	(select AssmID from wca.dbo.ASSM
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Assimilation

select distinct
v10s_ASSM.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_ASSM.EventID,
v10s_ASSM.AnnounceDate as Created,
v10s_ASSM.Acttime as Changed,
v10s_ASSM.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_ASSM.AssimilationDate,
CASE WHEN (v10s_ASSM.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_ASSM.SectyCD) > 0) THEN '[' + v10s_ASSM.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_ASSM.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
'M' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Assimilation

FROM v10s_ASSM
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_ASSM.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_ASSM.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_ASSM.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON v10s_ASSM.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_ASSM.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_ASSM.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_ASSM.AssimilationDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_ASSM.Acttime > (select max(wcaarch.dbo.evf_Security_Reclassification.changed) from wcaarch.dbo.evf_Security_Reclassification)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_ASSM.AssimilationDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Assimilation ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Assimilation ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Assimilation] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Assimilation] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Assimilation] ON [dbo].[evf_Assimilation]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Assimilation] ON [dbo].[evf_Assimilation]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Assimilation] ON [dbo].[evf_Assimilation]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Buy_Back, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Buy_Back')
	   drop table evf_Buy_Back

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Buy_Back)
--	delete from evf_Buy_Back
--	where EventID in
--	(select BbID from wca.dbo.BB
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Buy_Back)
--	delete from evf_Buy_Back
--	where EventID in
--	(select EventID from wca.dbo.MPAY
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Buy_Back)
--	delete from evf_Buy_Back
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Buy_Back

select distinct
v10s_BB.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('0'+rtrim(cast(serialid as char(2))),len(serialid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(MPAY.EventID as char(8)))
else cast(Seqnum as char(1))+'0101'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_BB.EventID as char(8)))
end as Caref,
v10s_BB.EventID,
v10s_BB.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_BB.Acttime) and (MPAY.Acttime > RD.Acttime) THEN MPAY.Acttime WHEN RD.Acttime > v10s_BB.Acttime THEN RD.Acttime ELSE v10s_BB.Acttime END as [Changed],
v10s_BB.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.ActFlag) > 0) THEN '[' + MPAY.ActFlag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.ActFlag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.ActFlag+']' ELSE irPaytype.Lookup+'['+MPAY.ActFlag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
'MinMaxPrice' AS RateType,
MPAY.MinPrice +':'+MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
MPAY.MinQlyQty as MinQualifyingQuant,
MPAY.MaxQlyQty as MaxQualifyingQuant,
MPAY.PayDate,
MPAY.MinOfrQty as MinOfferQuant,
MPAY.MaxOfrqty as MaxOfferQuant,
MPAY.TndrStrkPrice as TenderStrikePrice,
MPAY.TndrStrkStep as TenderStrikeStep,
CASE WHEN (v10s_BB.OnOffFlag is null) THEN irOnOffMarket.Lookup WHEN (irOnOffMarket.Lookup is null) and (LEN(v10s_BB.OnOffFlag) > 0) THEN '[' + v10s_BB.OnOffFlag +'] not found' ELSE irOnOffMarket.Lookup END as OnOffMarket,
v10s_BB.StartDate,
v10s_BB.EndDate,
v10s_BB.MinAcpQty as MinAcceptanceQuant,
v10s_BB.MaxAcpQty as MAxAcceptanceQuant,
v10s_BB.BBMinPct as MinPercent,
v10s_BB.BBMaxPct as MaxPercent,
'V' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'BB|BbID|BBNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Buy_Back

FROM v10s_BB
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_BB.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN RD ON v10s_BB.RdID = RD.RdID
LEFT OUTER JOIN MPAY ON v10s_BB.EventID = MPAY.EventID AND v10s_BB.SEvent = MPAY.SEvent
LEFT OUTER JOIN irOnOffMarket ON v10s_BB.OnOffFlag = irOnOffMarket.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.ActFlag = irACTIONMPAY.Code
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_BB.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or MPAY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_BB.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>v10s_BB.EndDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_BB.Acttime > (select max(wcaarch.dbo.evf_Buy_Back.changed) from wcaarch.dbo.evf_Buy_Back)
--	or MPAY.Acttime > (select max(wcaarch.dbo.evf_Buy_Back.changed) from wcaarch.dbo.evf_Buy_Back)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Buy_Back.changed) from wcaarch.dbo.evf_Buy_Back))
--	and (v20c_wca2_dSCEXH.effectivedate>v10s_BB.EndDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Buy_Back ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Buy_Back ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Buy_Back] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Buy_Back] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Buy_Back] ON [dbo].[evf_Buy_Back]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Buy_Back] ON [dbo].[evf_Buy_Back]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Buy_Back] ON [dbo].[evf_Buy_Back]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Call, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Call')
	   drop table evf_Call

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Call)
--	delete from evf_Call
--	where EventID in
--	(select CallID from wca.dbo.CALL
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Call

select distinct
v10s_CALL.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_CALL.EventID,
v10s_CALL.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CALL.Acttime THEN RD.Acttime ELSE v10s_CALL.Acttime END as [Changed],
v10s_CALL.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
v10s_CALL.CallNumber,
CASE WHEN (v10s_CALL.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_CALL.CurenCD) > 0) THEN '[' + v10s_CALL.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_CALL.ToFaceValue,
v10s_CALL.ToPremium,
v10s_CALL.DueDate,
'V' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'CALL|CallID|CallNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Call

FROM v10s_CALL
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_CALL.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN RD ON v10s_CALL.RdID = RD.RdID
LEFT OUTER JOIN CUREN ON v10s_CALL.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_CALL.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_CALL.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_CALL.DueDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_CALL.Acttime > (select max(wcaarch.dbo.evf_Call.changed) from wcaarch.dbo.evf_Call)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Call.changed) from wcaarch.dbo.evf_Call))
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_CALL.DueDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Call ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Call ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Call] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Call] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Call] ON [dbo].[evf_Call]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Call] ON [dbo].[evf_Call]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Call] ON [dbo].[evf_Call]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Capital_Reduction, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Capital_Reduction')
	   drop table evf_Capital_Reduction

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Capital_Reduction)
--	delete from evf_Capital_Reduction
--	where EventID in
--	(select CaprdID from wca.dbo.CAPRD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Capital_Reduction)
--	delete from evf_Capital_Reduction
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Capital_Reduction)
--	delete from evf_Capital_Reduction
--	where EventID in
--	(select RelEventID from wca.dbo.ICC
--	where acttime>@Startdate
--	and Eventtype ='CAPRD')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Capital_Reduction)
--	delete from evf_Capital_Reduction
--	where EventID in
--	(select RelEventID from wca.dbo.SDCHG
--	where acttime>@Startdate
--	and Eventtype ='CAPRD')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Capital_Reduction)
--	delete from evf_Capital_Reduction
--	where EventID in
--	(select RelEventID from wca.dbo.LCC
--	where acttime>@Startdate
--	and Eventtype ='CAPRD')

GO

use wca

--	insert into wcaarch.dbo.evf_Capital_Reduction

select distinct
v10s_CAPRD.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_CAPRD.EventID,
v10s_CAPRD.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CAPRD.Acttime THEN RD.Acttime ELSE v10s_CAPRD.Acttime END as [Changed],
v10s_CAPRD.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_WCA2_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_wca2_dSCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_WCA2_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_wca2_dSCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN (v10s_CAPRD.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_CAPRD.Fractions) > 0) THEN '[' + v10s_CAPRD.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_CAPRD.EffectiveDate,
v10s_CAPRD.NewRatio +':'+v10s_CAPRD.OldRatio as Ratio,
v10s_CAPRD.PayDate,
v10s_CAPRD.OldParValue,
v10s_CAPRD.NewParValue,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'CAPRD|CaprdID|CaprdNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as RateType,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Capital_Reduction

FROM v10s_CAPRD
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_CAPRD.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN RD ON v10s_CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
       and ICC.ActFlag<>'D'
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_wca2_dSCEXH.secid = SDCHG.secid
       AND v20c_wca2_dSCEXH.ExCountry = SDCHG.CntryCD 
       AND v20c_wca2_dSCEXH.RegCountry = SDCHG.RcntryCD
       and SDCHG.ActFlag<>'D'
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_wca2_dSCEXH.ExchgCD = LCC.ExchgCD
       and LCC.ActFlag<>'D'
LEFT OUTER JOIN irFRACTIONS ON v10s_CAPRD.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_CAPRD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or ICC.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or SDCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or LCC.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_CAPRD.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_CAPRD.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_CAPRD.Acttime > (select max(wcaarch.dbo.evf_Capital_Reduction.changed) from wcaarch.dbo.evf_Capital_Reduction)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Capital_Reduction.changed) from wcaarch.dbo.evf_Capital_Reduction)
--	or ICC.Acttime > (select max(wcaarch.dbo.evf_Capital_Reduction.changed) from wcaarch.dbo.evf_Capital_Reduction)
--	or SDCHG.Acttime > (select max(wcaarch.dbo.evf_Capital_Reduction.changed) from wcaarch.dbo.evf_Capital_Reduction)
--	or LCC.Acttime > (select max(wcaarch.dbo.evf_Capital_Reduction.changed) from wcaarch.dbo.evf_Capital_Reduction))
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_CAPRD.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Capital_Reduction ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Capital_Reduction ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Capital_Reduction] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Capital_Reduction] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Capital_Reduction] ON [dbo].[evf_Capital_Reduction]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Capital_Reduction] ON [dbo].[evf_Capital_Reduction]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Capital_Reduction] ON [dbo].[evf_Capital_Reduction]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Return_of_Capital, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Return_of_Capital')
	   drop table evf_Return_of_Capital

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Return_of_Capital)
--	delete from evf_Return_of_Capital
--	where EventID in
--	(select RcapID from wca.dbo.RCAP
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Return_of_Capital)
--	delete from evf_Return_of_Capital
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Return_of_Capital

select distinct
v10s_RCAP.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_RCAP.EventID,
v10s_RCAP.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_RCAP.Acttime THEN RD.Acttime ELSE v10s_RCAP.Acttime END as [Changed],
v10s_RCAP.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
'CashBack' AS RateType,
v10s_RCAP.CashBak as Rate,
CASE WHEN (v10s_RCAP.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_RCAP.CurenCD) > 0) THEN '[' + v10s_RCAP.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_RCAP.EffectiveDate,
v10s_RCAP.CSPYDate,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'RCAP|RcapID|RCapNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratio

	  into wcaarch.dbo.evf_Return_of_Capital

FROM v10s_RCAP
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_RCAP.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
LEFT OUTER JOIN CUREN ON v10s_RCAP.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_RCAP.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_RCAP.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_RCAP.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_RCAP.Acttime > (select max(wcaarch.dbo.evf_Return_of_Capital.changed) from wcaarch.dbo.evf_Return_of_Capital)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Return_of_Capital.changed) from wcaarch.dbo.evf_Return_of_Capital))
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_RCAP.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Return_of_Capital ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Return_of_Capital ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Return_of_Capital] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Return_of_Capital] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Return_of_Capital] ON [dbo].[evf_Return_of_Capital]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Return_of_Capital] ON [dbo].[evf_Return_of_Capital]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Return_of_Capital] ON [dbo].[evf_Return_of_Capital]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Takeover, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Takeover')
	   drop table evf_Takeover

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Takeover)
--	delete from evf_Takeover
--	where EventID in
--	(select TkovrID from wca.dbo.TKOVR
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Takeover)
--	delete from evf_Takeover
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Takeover)
--	delete from evf_Takeover
--	where EventID in
--	(select EventID from wca.dbo.MPAY
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Takeover

select distinct
v10s_TKOVR.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('0'+rtrim(cast(serialid as char(2))),len(serialid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(MPAY.EventID as char(8)))
else cast(Seqnum as char(1))+'0101'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_TKOVR.EventID as char(8)))
end as Caref,
v10s_TKOVR.EventID,
v10s_TKOVR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_TKOVR.Acttime) and (MPAY.Acttime > RD.Acttime) THEN MPAY.Acttime WHEN RD.Acttime > v10s_TKOVR.Acttime THEN RD.Acttime ELSE v10s_TKOVR.Acttime END as [Changed],
v10s_TKOVR.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.ActFlag) > 0) THEN '[' + MPAY.ActFlag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.ActFlag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.ActFlag+']' ELSE irPaytype.Lookup+'['+MPAY.ActFlag+']' END as Paytype,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
'MinPriceMaxPrice' AS RateType,
MPAY.MinPrice +':'+MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
MPAY.PayDate,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.MinQlyQty as MinQualifyingQuant,
MPAY.MaxQlyQty as MaxQualifyingQuant,
MPAY.MinOfrQty as MinOfferQuant,
MPAY.MaxOfrqty as MaxOfferQuant,
MPAY.TndrStrkPrice as TenderStrikePrice,
MPAY.TndrStrkStep as TenderStrikeStep,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
CASE WHEN (v10s_TKOVR.Hostile is null) THEN irHostile.Lookup WHEN (irHostile.Lookup is null) and (LEN(v10s_TKOVR.Hostile) > 0) THEN '[' + v10s_TKOVR.Hostile +'] not found' ELSE irHostile.Lookup END as TakeoverType,
CASE WHEN (v10s_TKOVR.TkovrStatus is null) THEN irTkovrStat.Lookup WHEN (irTkovrStat.Lookup is null) and (LEN(v10s_TKOVR.TkovrStatus) > 0) THEN '[' + v10s_TKOVR.TkovrStatus +'] not found' ELSE irTkovrStat.Lookup END as TakeoverStatus,
v10s_TKOVR.OfferorIssID,
v10s_TKOVR.OfferorName,
v10s_TKOVR.OpenDate,
v10s_TKOVR.CloseDate,
v10s_TKOVR.PreOfferQty,
v10s_TKOVR.PreOfferPercent,
v10s_TKOVR.TargetQuantity,
v10s_TKOVR.TargetPercent,
v10s_TKOVR.UnconditionalDate,
v10s_TKOVR.CmAcqDate as CompulsoryAcqDate,
v10s_TKOVR.MinAcpQty as MinAcceptanceQuant,
v10s_TKOVR.MaxAcpQty as MaxAcceptanceQuant,
CASE WHEN (v10s_TKOVR.CmAcqDate is null) THEN 'V' ELSE 'M' END as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'TKOVR|TkovrID|TkovrNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Takeover

FROM v10s_TKOVR
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_TKOVR.SecID
inner JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN RD ON v10s_TKOVR.RdID = RD.RdID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID AND v10s_TKOVR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON MPAY.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irHOSTILE ON v10s_TKOVR.Hostile = irHOSTILE.Code
LEFT OUTER JOIN irTKOVRSTAT ON v10s_TKOVR.TkovrStatus = irTKOVRSTAT.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.ActFlag = irACTIONMPAY.Code
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_TKOVR.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or MPAY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_TKOVR.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_TKOVR.AnnounceDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_TKOVR.Acttime > (select max(wcaarch.dbo.evf_Takeover.changed) from wcaarch.dbo.evf_Takeover)
--	or MPAY.Acttime > (select max(wcaarch.dbo.evf_Takeover.changed) from wcaarch.dbo.evf_Takeover)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Takeover.changed) from wcaarch.dbo.evf_Takeover))
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_TKOVR.AnnounceDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Takeover ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Takeover ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Takeover] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Takeover] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Takeover] ON [dbo].[evf_Takeover]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Takeover] ON [dbo].[evf_Takeover]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Takeover] ON [dbo].[evf_Takeover]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Arrangement, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Arrangement')
	   drop table evf_Arrangement

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Arrangement)
--	delete from evf_Arrangement
--	where EventID in
--	(select RdID from wca.dbo.ARR
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Arrangement)
--	delete from evf_Arrangement
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Arrangement)
--	delete from evf_Arrangement
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Arrangement)
--	delete from evf_Arrangement
--	where EventID in
--	(select RelEventID from wca.dbo.ICC
--	where acttime>@Startdate
--	and Eventtype ='ARR')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Arrangement)
--	delete from evf_Arrangement
--	where EventID in
--	(select RelEventID from wca.dbo.SDCHG
--	where acttime>@Startdate
--	and Eventtype ='ARR')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Arrangement)
--	delete from evf_Arrangement
--	where EventID in
--	(select RelEventID from wca.dbo.LCC
--	where acttime>@Startdate
--	and Eventtype ='ARR')

GO

use wca

--	insert into wcaarch.dbo.evf_Arrangement

select distinct
v10s_ARR.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_ARR.EventID,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v10s_ARR.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_WCA2_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_wca2_dSCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_WCA2_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_wca2_dSCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
' ' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'ARR|RdID|ArrNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Arrangement

FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
       and ICC.ActFlag<>'D'
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_wca2_dSCEXH.secid = SDCHG.secid
       AND v20c_wca2_dSCEXH.ExCountry = SDCHG.CntryCD 
       AND v20c_wca2_dSCEXH.RegCountry = SDCHG.RcntryCD
       and SDCHG.ActFlag<>'D'
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_wca2_dSCEXH.ExchgCD = LCC.ExchgCD
       and LCC.ActFlag<>'D'
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_ARR.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_ARR.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_ARR.Acttime > (select max(wcaarch.dbo.evf_Arrangement.changed) from wcaarch.dbo.evf_Arrangement)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Arrangement.changed) from wcaarch.dbo.evf_Arrangement)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Arrangement.changed) from wcaarch.dbo.evf_Arrangement)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Arrangement.changed) from wcaarch.dbo.evf_Arrangement))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Arrangement ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Arrangement ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Arrangement] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Arrangement] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Arrangement] ON [dbo].[evf_Arrangement]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Arrangement] ON [dbo].[evf_Arrangement]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Arrangement] ON [dbo].[evf_Arrangement]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Bonus, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Bonus')
	   drop table evf_Bonus

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Bonus)
--	delete from evf_Bonus
--	where EventID in
--	(select RdID from wca.dbo.BON
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Bonus)
--	delete from evf_Bonus
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Bonus)
--	delete from evf_Bonus
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Bonus

select distinct
v10s_BON.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_BON.EventID,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
v10s_BON.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_BON.RatioNew +':'+v10s_BON.RatioOld as Ratio,
CASE WHEN (v10s_BON.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_BON.Fractions) > 0) THEN '[' + v10s_BON.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_BON.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (v10s_BON.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_BON.SectyCD) > 0) THEN '[' + v10s_BON.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_BON.LapsedPremium,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'BON|RdID|BonNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Bonus

FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_BON.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_BON.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_BON.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SECTY ON v10s_BON.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_BON.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_BON.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_BON.Acttime > (select max(wcaarch.dbo.evf_Bonus.changed) from wcaarch.dbo.evf_Bonus)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Bonus.changed) from wcaarch.dbo.evf_Bonus)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Bonus.changed) from wcaarch.dbo.evf_Bonus)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Bonus.changed) from wcaarch.dbo.evf_Bonus))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Bonus ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Bonus ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Bonus] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Bonus] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Bonus] ON [dbo].[evf_Bonus]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Bonus] ON [dbo].[evf_Bonus]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Bonus] ON [dbo].[evf_Bonus]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Bonus_Rights, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Bonus_Rights')
	   drop table evf_Bonus_Rights

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Bonus_Rights)
--	delete from evf_Bonus_Rights
--	where EventID in
--	(select RdID from wca.dbo.BR
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Bonus_Rights)
--	delete from evf_Bonus_Rights
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Bonus_Rights)
--	delete from evf_Bonus_Rights
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Bonus_Rights

select distinct
v10s_BR.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_BR.EventID,
v10s_BR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BR.Acttime) THEN PEXDT.Acttime ELSE v10s_BR.Acttime END as [Changed],
v10s_BR.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_BR.RatioNew +':'+v10s_BR.RatioOld as Ratio,
'IssuePrice' AS RateType,
v10s_BR.IssuePrice as Rate,
CASE WHEN (v10s_BR.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_BR.CurenCD) > 0) THEN '[' + v10s_BR.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_BR.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_BR.SectyCD) > 0) THEN '[' + v10s_BR.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
CASE WHEN (v10s_BR.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_BR.Fractions) > 0) THEN '[' + v10s_BR.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_BR.StartSubscription,
v10s_BR.EndSubscription,
v10s_BR.SplitDate,
v10s_BR.StartTrade,
v10s_BR.EndTrade,
v10s_BR.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
v10s_BR.TraSecID as TradeableSecID,
v09b_SCMST.ISIN as TradeableIsin,
CASE WHEN v10s_BR.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
' ' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'BR|RdID|BrNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Bonus_Rights

FROM v10s_BR
INNER JOIN RD ON RD.RdID = v10s_BR.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'BR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_BR.ResSecID = SCMST.SecID
LEFT OUTER JOIN v09b_SCMST ON v10s_BR.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_BR.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_BR.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SECTY ON v10s_BR.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN CUREN ON v10s_BR.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_BR.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_BR.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_BR.Acttime > (select max(wcaarch.dbo.evf_Bonus_Rights.changed) from wcaarch.dbo.evf_Bonus_Rights)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Bonus_Rights.changed) from wcaarch.dbo.evf_Bonus_Rights)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Bonus_Rights.changed) from wcaarch.dbo.evf_Bonus_Rights)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Bonus_Rights.changed) from wcaarch.dbo.evf_Bonus_Rights))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Bonus_Rights ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Bonus_Rights ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Bonus_Rights] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Bonus_Rights] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Bonus_Rights] ON [dbo].[evf_Bonus_Rights]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Bonus_Rights] ON [dbo].[evf_Bonus_Rights]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Bonus_Rights] ON [dbo].[evf_Bonus_Rights]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Consolidation, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Consolidation')
	   drop table evf_Consolidation

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Consolidation)
--	delete from evf_Consolidation
--	where EventID in
--	(select RdID from wca.dbo.CONSD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Consolidation)
--	delete from evf_Consolidation
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Consolidation)
--	delete from evf_Consolidation
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Consolidation)
--	delete from evf_Consolidation
--	where EventID in
--	(select RelEventID from wca.dbo.ICC
--	where acttime>@Startdate
--	and Eventtype ='CONSD')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Consolidation)
--	delete from evf_Consolidation
--	where EventID in
--	(select RelEventID from wca.dbo.SDCHG
--	where acttime>@Startdate
--	and Eventtype ='CONSD')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Consolidation)
--	delete from evf_Consolidation
--	where EventID in
--	(select RelEventID from wca.dbo.LCC
--	where acttime>@Startdate
--	and Eventtype ='CONSD')

GO

use wca

--	insert into wcaarch.dbo.evf_Consolidation

select distinct
v10s_CONSD.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_CONSD.EventID,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
v10s_CONSD.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_WCA2_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_wca2_dSCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_WCA2_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_wca2_dSCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_CONSD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_CONSD.CurenCD) > 0) THEN '[' + v10s_CONSD.CurenCD +'] not found' ELSE CUREN.Currency END as CONSDCurrency,
v10s_CONSD.OldParValue,
v10s_CONSD.NewParValue,
v10s_CONSD.NewRatio +':'+v10s_CONSD.OldRatio as Ratio,
CASE WHEN (v10s_CONSD.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_CONSD.Fractions) > 0) THEN '[' + v10s_CONSD.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'CONSD|RdID|ConsdNotes' as Link_Notes,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Consolidation

FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
       and ICC.ActFlag<>'D'
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_wca2_dSCEXH.secid = SDCHG.secid
       AND v20c_wca2_dSCEXH.ExCountry = SDCHG.CntryCD 
       AND v20c_wca2_dSCEXH.RegCountry = SDCHG.RcntryCD
       and SDCHG.ActFlag<>'D'
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_wca2_dSCEXH.ExchgCD = LCC.ExchgCD
       and LCC.ActFlag<>'D'
LEFT OUTER JOIN irFRACTIONS ON v10s_CONSD.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_CONSD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_CONSD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_CONSD.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_CONSD.Acttime > (select max(wcaarch.dbo.evf_Consolidation.changed) from wcaarch.dbo.evf_Consolidation)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Consolidation.changed) from wcaarch.dbo.evf_Consolidation)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Consolidation.changed) from wcaarch.dbo.evf_Consolidation)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Consolidation.changed) from wcaarch.dbo.evf_Consolidation))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Consolidation ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Consolidation ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Consolidation] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Consolidation] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Consolidation] ON [dbo].[evf_Consolidation]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Consolidation] ON [dbo].[evf_Consolidation]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Consolidation] ON [dbo].[evf_Consolidation]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Demerger, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Demerger')
	   drop table evf_Demerger

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Demerger)
--	delete from evf_Demerger
--	where EventID in
--	(select RdID from wca.dbo.DMRGR
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Demerger)
--	delete from evf_Demerger
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Demerger)
--	delete from evf_Demerger
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Demerger)
--	delete from evf_Demerger
--	where EventID in
--	(select EventID from wca.dbo.MPAY
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Demerger

select distinct
v10s_DMRGR.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('0'+rtrim(cast(serialid as char(2))),len(serialid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(MPAY.EventID as char(8)))
else cast(Seqnum as char(1))+'0101'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_DMRGR.EventID as char(8)))
end as Caref,
v10s_DMRGR.EventID,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_DMRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime Or EXDT.Acttime is Null) and (MPAY.Acttime > PEXDT.Acttime Or PEXDT.Acttime is Null) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime Or EXDT.Acttime is Null) and (RD.Acttime > PEXDT.Acttime Or PEXDT.Acttime is Null) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime Or PEXDT.Acttime is Null) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
v10s_DMRGR.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.ActFlag) > 0) THEN '[' + MPAY.ActFlag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.ActFlag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.ActFlag+']' ELSE irPaytype.Lookup+'['+MPAY.ActFlag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_DMRGR.EffectiveDate,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'DMRGR|RdID|DmrgrNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as RateType,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Demerger

FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DMRGR.EventID = MPAY.EventID AND v10s_DMRGR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON MPAY.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.ActFlag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_DMRGR.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or MPAY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_DMRGR.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_DMRGR.Acttime > (select max(wcaarch.dbo.evf_Demerger.changed) from wcaarch.dbo.evf_Demerger)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Demerger.changed) from wcaarch.dbo.evf_Demerger)
--	or MPAY.Acttime > (select max(wcaarch.dbo.evf_Demerger.changed) from wcaarch.dbo.evf_Demerger)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Demerger.changed) from wcaarch.dbo.evf_Demerger)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Demerger.changed) from wcaarch.dbo.evf_Demerger))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Demerger ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Demerger ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Demerger] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Demerger] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Demerger] ON [dbo].[evf_Demerger]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Demerger] ON [dbo].[evf_Demerger]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Demerger] ON [dbo].[evf_Demerger]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Distribution, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Distribution')
	   drop table evf_Distribution

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Distribution)
--	delete from evf_Distribution
--	where EventID in
--	(select RdID from wca.dbo.DIST
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Distribution)
--	delete from evf_Distribution
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Distribution)
--	delete from evf_Distribution
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Distribution)
--	delete from evf_Distribution
--	where EventID in
--	(select EventID from wca.dbo.MPAY
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Distribution

select distinct
v10s_DIST.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('0'+rtrim(cast(serialid as char(2))),len(serialid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(MPAY.EventID as char(8)))
else cast(Seqnum as char(1))+'0101'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_DIST.EventID as char(8)))
end as Caref,
v10s_DIST.EventID,
v10s_DIST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIST.Acttime) THEN PEXDT.Acttime ELSE v10s_DIST.Acttime END as [Changed],
v10s_DIST.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.ActFlag) > 0) THEN '[' + MPAY.ActFlag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.ActFlag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.ActFlag+']' ELSE irPaytype.Lookup+'['+MPAY.ActFlag+']' END as Paytype,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'DIST|RdID|DistNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Distribution

FROM v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DIST.EventID = MPAY.EventID AND v10s_DIST.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON MPAY.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.ActFlag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_DIST.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or MPAY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_DIST.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_DIST.Acttime > (select max(wcaarch.dbo.evf_Distribution.changed) from wcaarch.dbo.evf_Distribution)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Distribution.changed) from wcaarch.dbo.evf_Distribution)
--	or MPAY.Acttime > (select max(wcaarch.dbo.evf_Distribution.changed) from wcaarch.dbo.evf_Distribution)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Distribution.changed) from wcaarch.dbo.evf_Distribution)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Distribution.changed) from wcaarch.dbo.evf_Distribution))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Distribution ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Distribution ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Distribution] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Distribution] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Distribution] ON [dbo].[evf_Distribution]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Distribution] ON [dbo].[evf_Distribution]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Distribution] ON [dbo].[evf_Distribution]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Divestment, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Divestment')
	   drop table evf_Divestment

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Divestment)
--	delete from evf_Divestment
--	where EventID in
--	(select RdID from wca.dbo.DVST
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Divestment)
--	delete from evf_Divestment
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Divestment)
--	delete from evf_Divestment
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Divestment

select distinct
v10s_DVST.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_DVST.EventID,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v10s_DVST.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_DVST.RatioNew +':'+v10s_DVST.RatioOld as Ratio,
'Min:MaxPrice' AS RateType,
v10s_DVST.MinPrice +':'+v10s_DVST.MaxPrice as Rate,
CASE WHEN (v10s_DVST.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_DVST.CurenCD) > 0) THEN '[' + v10s_DVST.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_DVST.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_DVST.Fractions) > 0) THEN '[' + v10s_DVST.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_DVST.StartSubscription,
v10s_DVST.EndSubscription,
v10s_DVST.TndrStrkPrice as TenderStrikePrice,
v10s_DVST.TndrPriceStep as TenderPriceStep,
v10s_DVST.MinQlyQty as MinQualifyingQuant,
v10s_DVST.MaxQlyQty as MaxQualifyingQuant,
v10s_DVST.MinAcpQty as MinAcceptanceQuant,
v10s_DVST.MaxAcpQty as MaxAcceptanceQuant,
v10s_DVST.TraSecID as TradeableSecID,
v09b_SCMST.Isin as TradeableIsin,
v10s_DVST.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (v10s_DVST.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_DVST.SectyCD) > 0) THEN '[' + v10s_DVST.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'DVST|RdID|DvstNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Divestment

FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_DVST.ResSecID = SCMST.SecID
LEFT OUTER JOIN v09b_SCMST ON v10s_DVST.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_DVST.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON v10s_DVST.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON v10s_DVST.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_DVST.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_DVST.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_DVST.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_DVST.Acttime > (select max(wcaarch.dbo.evf_Divestment.changed) from wcaarch.dbo.evf_Divestment)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Divestment.changed) from wcaarch.dbo.evf_Divestment)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Divestment.changed) from wcaarch.dbo.evf_Divestment)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Divestment.changed) from wcaarch.dbo.evf_Divestment))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Divestment ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Divestment ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Divestment] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Divestment] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Divestment] ON [dbo].[evf_Divestment]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Divestment] ON [dbo].[evf_Divestment]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Divestment] ON [dbo].[evf_Divestment]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Entitlement, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Entitlement')
	   drop table evf_Entitlement

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Entitlement)
--	delete from evf_Entitlement
--	where EventID in
--	(select RdID from wca.dbo.ENT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Entitlement)
--	delete from evf_Entitlement
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Entitlement)
--	delete from evf_Entitlement
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Entitlement

select distinct
v10s_ENT.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_ENT.EventID,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
v10s_ENT.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_ENT.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_ENT.SectyCD) > 0) THEN '[' + v10s_ENT.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_ENT.RatioNew +':'+v10s_ENT.RatioOld as Ratio,
'IssuePrice' AS RateType,
v10s_ENT.EntIssuePrice as Rate,
CASE WHEN (v10s_ENT.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_ENT.CurenCD) > 0) THEN '[' + v10s_ENT.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_ENT.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_ENT.Fractions) > 0) THEN '[' + v10s_ENT.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_ENT.EntIssuePrice as IssuePrice,
v10s_ENT.StartSubscription,
v10s_ENT.EndSubscription,
v10s_ENT.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN v10s_ENT.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'ENT|RdID|EntNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Entitlement

FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_ENT.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_ENT.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_ENT.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_ENT.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_ENT.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_ENT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_ENT.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_ENT.Acttime > (select max(wcaarch.dbo.evf_Entitlement.changed) from wcaarch.dbo.evf_Entitlement)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Entitlement.changed) from wcaarch.dbo.evf_Entitlement)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Entitlement.changed) from wcaarch.dbo.evf_Entitlement)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Entitlement.changed) from wcaarch.dbo.evf_Entitlement))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Entitlement ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Entitlement ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Entitlement] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Entitlement] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Entitlement] ON [dbo].[evf_Entitlement]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Entitlement] ON [dbo].[evf_Entitlement]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Entitlement] ON [dbo].[evf_Entitlement]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Subdivision, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Subdivision')
	   drop table evf_Subdivision

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Subdivision)
--	delete from evf_Subdivision
--	where EventID in
--	(select RdID from wca.dbo.SD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Subdivision)
--	delete from evf_Subdivision
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Subdivision)
--	delete from evf_Subdivision
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Subdivision)
--	delete from evf_Subdivision
--	where EventID in
--	(select RelEventID from wca.dbo.ICC
--	where acttime>@Startdate
--	and Eventtype ='SD')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Subdivision)
--	delete from evf_Subdivision
--	where EventID in
--	(select RelEventID from wca.dbo.SDCHG
--	where acttime>@Startdate
--	and Eventtype ='SD')
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Subdivision)
--	delete from evf_Subdivision
--	where EventID in
--	(select RelEventID from wca.dbo.LCC
--	where acttime>@Startdate
--	and Eventtype ='SD')

GO

use wca

--	insert into wcaarch.dbo.evf_Subdivision

select distinct
v10s_SD.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_SD.EventID,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
v10s_SD.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_WCA2_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_wca2_dSCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_WCA2_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_wca2_dSCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_SD.NewRatio +':'+v10s_SD.OldRatio as Ratio,
CASE WHEN (v10s_SD.OldCurenCD is null) THEN oldCUREN.Currency WHEN (oldCUREN.Currency is null) and (LEN(v10s_SD.OldCurenCD) > 0) THEN '[' + v10s_SD.OldCurenCD +'] not found' ELSE oldCUREN.Currency END as OldCurrency,
CASE WHEN (v10s_SD.NewCurenCD is null) THEN newCUREN.Currency WHEN (newCUREN.Currency is null) and (LEN(v10s_SD.NewCurenCD) > 0) THEN '[' + v10s_SD.NewCurenCD +'] not found' ELSE newCUREN.Currency END as NewCurrency,
v10s_SD.OldParValue,
v10s_SD.NewParValue,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'SD|RdID|SdNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Subdivision

FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
       and ICC.ActFlag<>'D'
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_wca2_dSCEXH.secid = SDCHG.secid
       AND v20c_wca2_dSCEXH.ExCountry = SDCHG.CntryCD 
       AND v20c_wca2_dSCEXH.RegCountry = SDCHG.RcntryCD
       and SDCHG.ActFlag<>'D'
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_wca2_dSCEXH.ExchgCD = LCC.ExchgCD
       and LCC.ActFlag<>'D'
LEFT OUTER JOIN CUREN as oldCUREN ON v10s_SD.OldCurenCD = oldCUREN.CurenCD
LEFT OUTER JOIN CUREN as newCUREN ON v10s_SD.NewCurenCD = newCUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_SD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_SD.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_SD.Acttime > (select max(wcaarch.dbo.evf_Subdivision.changed) from wcaarch.dbo.evf_Subdivision)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Subdivision.changed) from wcaarch.dbo.evf_Subdivision)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Subdivision.changed) from wcaarch.dbo.evf_Subdivision)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Subdivision.changed) from wcaarch.dbo.evf_Subdivision))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Subdivision ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Subdivision ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Subdivision] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Subdivision] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Subdivision] ON [dbo].[evf_Subdivision]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Subdivision] ON [dbo].[evf_Subdivision]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Subdivision] ON [dbo].[evf_Subdivision]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Merger, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Merger')
	   drop table evf_Merger

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Merger)
--	delete from evf_Merger
--	where EventID in
--	(select RdID from wca.dbo.MRGR
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Merger)
--	delete from evf_Merger
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Merger)
--	delete from evf_Merger
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Merger)
--	delete from evf_Merger
--	where EventID in
--	(select EventID from wca.dbo.MPAY
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Merger

select distinct
v10s_MRGR.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('0'+rtrim(cast(serialid as char(2))),len(serialid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(MPAY.EventID as char(8)))
else cast(Seqnum as char(1))+'0101'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_MRGR.EventID as char(8)))
end as Caref,
v10s_MRGR.EventID,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v10s_MRGR.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.ActFlag) > 0) THEN '[' + MPAY.ActFlag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.ActFlag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.ActFlag+']' ELSE irPaytype.Lookup+'['+MPAY.ActFlag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.MinPrice,
v10s_MRGR.EffectiveDate,
v10s_MRGR.AppointedDate,
CASE WHEN (v10s_MRGR.MrgrStatus is null) THEN irMrgrStat.Lookup WHEN (irMrgrStat.Lookup is null) and (LEN(v10s_MRGR.MrgrStatus) > 0) THEN '[' + v10s_MRGR.MrgrStatus +'] not found' ELSE irMrgrStat.Lookup END as MergerStatus,
'M' as Choice,
'MRGR|RdID|Companies' as Link_Companies,
'MRGR|RdID|ApprovalStatus' as Link_ApprovalStatus,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'MRGR|RdID|MrgrTerms' as Link_Terms,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Merger

FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID AND v10s_MRGR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON MPAY.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irMRGRSTAT ON v10s_MRGR.MrgrStatus = irMRGRSTAT.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.ActFlag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_MRGR.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or MPAY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_MRGR.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_MRGR.Acttime > (select max(wcaarch.dbo.evf_Merger.changed) from wcaarch.dbo.evf_Merger)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Merger.changed) from wcaarch.dbo.evf_Merger)
--	or MPAY.Acttime > (select max(wcaarch.dbo.evf_Merger.changed) from wcaarch.dbo.evf_Merger)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Merger.changed) from wcaarch.dbo.evf_Merger)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Merger.changed) from wcaarch.dbo.evf_Merger))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Merger ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Merger ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Merger] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Merger] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Merger] ON [dbo].[evf_Merger]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Merger] ON [dbo].[evf_Merger]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Merger] ON [dbo].[evf_Merger]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Rights, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Rights')
	   drop table evf_Rights

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Rights)
--	delete from evf_Rights
--	where EventID in
--	(select RdID from wca.dbo.RTS
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Rights)
--	delete from evf_Rights
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Rights)
--	delete from evf_Rights
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Rights

select distinct
v10s_RTS.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_RTS.EventID,
v10s_RTS.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_RTS.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_RTS.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_RTS.Acttime) THEN PEXDT.Acttime ELSE v10s_RTS.Acttime END as [Changed],
v10s_RTS.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_RTS.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_RTS.SectyCD) > 0) THEN '[' + v10s_RTS.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_RTS.RatioNew +':'+v10s_RTS.RatioOld as Ratio,
'IssuePrice' as RateType,
v10s_RTS.IssuePrice as Rate,
CASE WHEN (v10s_RTS.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_RTS.CurenCD) > 0) THEN '[' + v10s_RTS.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_RTS.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_RTS.Fractions) > 0) THEN '[' + v10s_RTS.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_RTS.StartSubscription,
v10s_RTS.EndSubscription,
v10s_RTS.SplitDate,
v10s_RTS.StartTrade,
v10s_RTS.EndTrade,
v10s_RTS.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
v10s_RTS.TraSecID as TradeableSecID,
v09b_SCMST.ISIN as TradeableIsin,
v10s_RTS.LapsedPremium,
CASE WHEN v10s_RTS.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
'V' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'RTS|RdID|RtsNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Rights

FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'RTS' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'RTS' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_RTS.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_RTS.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN v09b_SCMST ON v10s_RTS.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN irFRACTIONS ON v10s_RTS.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_RTS.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_RTS.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_RTS.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_RTS.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_RTS.Acttime > (select max(wcaarch.dbo.evf_Rights.changed) from wcaarch.dbo.evf_Rights)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Rights.changed) from wcaarch.dbo.evf_Rights)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Rights.changed) from wcaarch.dbo.evf_Rights)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Rights.changed) from wcaarch.dbo.evf_Rights))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Rights ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Rights ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Rights] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Rights] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Rights] ON [dbo].[evf_Rights]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Rights] ON [dbo].[evf_Rights]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Rights] ON [dbo].[evf_Rights]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Preferential_Offer, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Preferential_Offer')
	   drop table evf_Preferential_Offer

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Preferential_Offer)
--	delete from evf_Preferential_Offer
--	where EventID in
--	(select RdID from wca.dbo.PRF
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Preferential_Offer)
--	delete from evf_Preferential_Offer
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Preferential_Offer)
--	delete from evf_Preferential_Offer
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Preferential_Offer

select distinct
v10s_PRF.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_PRF.EventID,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
v10s_PRF.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_PRF.OffereeIssID,
v10s_PRF.OffereeName,
CASE WHEN (v10s_PRF.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_PRF.SectyCD) > 0) THEN '[' + v10s_PRF.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
CASE WHEN (v10s_PRF.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_PRF.Fractions) > 0) THEN '[' + v10s_PRF.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_PRF.RatioNew +':'+v10s_PRF.RatioOld as Ratio,
'MinPrice:MaxPrice' AS RateType,
v10s_PRF.MinPrice +':'+v10s_PRF.MaxPrice as Rate,
CASE WHEN (v10s_PRF.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRF.CurenCD) > 0) THEN '[' + v10s_PRF.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRF.StartSubscription,
v10s_PRF.EndSubscription,
v10s_PRF.TndrStrkPrice,
v10s_PRF.TndrPriceStep,
v10s_PRF.MinQlyQty as MinQualifyingQuant,
v10s_PRF.MaxQlyQty as MaxQualifyingQuant,
v10s_PRF.MinAcpQty as MinAcceptanceQuant,
v10s_PRF.MaxAcpQty as MaxAcceptanceQuant,
v10s_PRF.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
'V' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'PRF|RdID|PrfNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Preferential_Offer

FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_PRF.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_PRF.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_PRF.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_PRF.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_PRF.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_PRF.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_PRF.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_PRF.Acttime > (select max(wcaarch.dbo.evf_Preferential_Offer.changed) from wcaarch.dbo.evf_Preferential_Offer)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Preferential_Offer.changed) from wcaarch.dbo.evf_Preferential_Offer)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Preferential_Offer.changed) from wcaarch.dbo.evf_Preferential_Offer)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Preferential_Offer.changed) from wcaarch.dbo.evf_Preferential_Offer))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Preferential_Offer ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Preferential_Offer ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Preferential_Offer] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Preferential_Offer] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Preferential_Offer] ON [dbo].[evf_Preferential_Offer]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Preferential_Offer] ON [dbo].[evf_Preferential_Offer]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Preferential_Offer] ON [dbo].[evf_Preferential_Offer]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Purchase_Offer, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Purchase_Offer')
	   drop table evf_Purchase_Offer

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Purchase_Offer)
--	delete from evf_Purchase_Offer
--	where EventID in
--	(select RdID from wca.dbo.PO
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Purchase_Offer)
--	delete from evf_Purchase_Offer
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Purchase_Offer)
--	delete from evf_Purchase_Offer
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Purchase_Offer

select distinct
v10s_PO.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_PO.EventID,
v10s_PO.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PO.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PO.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PO.Acttime) THEN PEXDT.Acttime ELSE v10s_PO.Acttime END as [Changed],
v10s_PO.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
'MinMaxPrice' AS RateType,
v10s_PO.MinPrice +':'+v10s_PO.MaxPrice as Rate,
CASE WHEN (v10s_PO.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PO.CurenCD) > 0) THEN '[' + v10s_PO.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PO.NegotiatedPrice,
v10s_PO.OfferOpens,
v10s_PO.OfferCloses,
v10s_PO.POMinPercent as MinPercent,
v10s_PO.POMaxPercent as MaxPercent,
v10s_PO.MinOfrQty as MinOfferQuant,
v10s_PO.MaxOfrqty as MaxOfferQuant,
v10s_PO.TndrStrkPrice as TenderStrikePrice,
v10s_PO.TndrPriceStep as TenderPriceStep,
v10s_PO.MinQlyQty as MinQualifyingQuant,
v10s_PO.MaxQlyQty as MaxQualifyingQuant,
v10s_PO.MinAcpQty as MinAcceptanceQuant,
v10s_PO.MaxAcpQty as MaxAcceptanceQuant,
'V' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'PO|RdID|PoNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratio

	  into wcaarch.dbo.evf_Purchase_Offer

FROM v10s_PO
INNER JOIN RD ON RD.RdID = v10s_PO.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'PO' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PO' = PEXDT.EventType
LEFT OUTER JOIN CUREN ON v10s_PO.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_PO.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_PO.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_PO.Acttime > (select max(wcaarch.dbo.evf_Purchase_Offer.changed) from wcaarch.dbo.evf_Purchase_Offer)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Purchase_Offer.changed) from wcaarch.dbo.evf_Purchase_Offer)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Purchase_Offer.changed) from wcaarch.dbo.evf_Purchase_Offer)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Purchase_Offer.changed) from wcaarch.dbo.evf_Purchase_Offer))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Purchase_Offer ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Purchase_Offer ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Purchase_Offer] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Purchase_Offer] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Purchase_Offer] ON [dbo].[evf_Purchase_Offer]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Purchase_Offer] ON [dbo].[evf_Purchase_Offer]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Purchase_Offer] ON [dbo].[evf_Purchase_Offer]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Dividend_Reinvestment_Plan, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Dividend_Reinvestment_Plan')
	   drop table evf_Dividend_Reinvestment_Plan

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend_Reinvestment_Plan)
--	delete from evf_Dividend_Reinvestment_Plan
--	where EventID in
--	(select DivID from wca.dbo.DRIP
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend_Reinvestment_Plan)
--	delete from evf_Dividend_Reinvestment_Plan
--	where EventID in
--	(select DivID from wca.dbo.DIVPY
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend_Reinvestment_Plan)
--	delete from evf_Dividend_Reinvestment_Plan
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend_Reinvestment_Plan)
--	delete from evf_Dividend_Reinvestment_Plan
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Dividend_Reinvestment_Plan

select distinct
v10s_DRIP.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_DRIP.EventID,
v10s_DRIP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DRIP.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DRIP.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DRIP.Acttime) THEN PEXDT.Acttime ELSE v10s_DRIP.Acttime END as [Changed],
case when DRIP.ActFlag<>'D' and DRIP.ActFlag<>'C' then v10s_DRIP.ActFlag else DRIP.ActFlag end as ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
DRIP.DripPayDate,
CASE WHEN v10s_DRIP.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_DRIP.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_DRIP.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_DRIP.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_DRIP.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_DRIP.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_DRIP.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_DRIP.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_DRIP.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_DRIP.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_DRIP.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_DRIP.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_DRIP.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_DRIP.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_DRIP.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_DRIP.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN (DRIP.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(DRIP.CntryCD) > 0) THEN '[' + DRIP.CntryCD +'] not found' ELSE Cntry.Country END as Country,
DRIP.DripLastdate,
DRIP.DripReinvPrice,
'V' as Choice,
'RD|RdID|RdNotes' as Link_RecordDateNotes,
'DIV|DivID|DivNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Dividend_Reinvestment_Plan

FROM v10s_DRIP
INNER JOIN DRIP ON v10s_DRIP.EventID = DRIP.DivID
INNER JOIN RD ON RD.RdID = v10s_DRIP.RdID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID and v20c_wca2_dSCEXH.ExCountry = DRIP.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN CNTRY ON DRIP.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_DRIP.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or DRIP.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_DRIP.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_DRIP.Acttime > (select max(wcaarch.dbo.evf_Dividend_Reinvestment_Plan.changed) from wcaarch.dbo.evf_Dividend_Reinvestment_Plan)
--	or DRIP.Acttime > (select max(wcaarch.dbo.evf_Dividend_Reinvestment_Plan.changed) from wcaarch.dbo.evf_Dividend_Reinvestment_Plan)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Dividend_Reinvestment_Plan.changed) from wcaarch.dbo.evf_Dividend_Reinvestment_Plan)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Dividend_Reinvestment_Plan.changed) from wcaarch.dbo.evf_Dividend_Reinvestment_Plan)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Dividend_Reinvestment_Plan.changed) from wcaarch.dbo.evf_Dividend_Reinvestment_Plan))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Dividend_Reinvestment_Plan ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Dividend_Reinvestment_Plan ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Dividend_Reinvestment_Plan] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Dividend_Reinvestment_Plan] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Dividend_Reinvestment_Plan] ON [dbo].[evf_Dividend_Reinvestment_Plan]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Dividend_Reinvestment_Plan] ON [dbo].[evf_Dividend_Reinvestment_Plan]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Dividend_Reinvestment_Plan] ON [dbo].[evf_Dividend_Reinvestment_Plan]([changed]) ON [PRIMARY]
	  GO


print ""
GO
print "Data Generation evf_Franking, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Franking')
	   drop table evf_Franking

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Franking)
--	delete from evf_Franking
--	where EventID in
--	(select DivID from wca.dbo.DIV
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Franking)
--	delete from evf_Franking
--	where EventID in
--	(select DivID from wca.dbo.FRANK
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Franking)
--	delete from evf_Franking
--	where EventID in
--	(select DivID from wca.dbo.DIVPY
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Franking)
--	delete from evf_Franking
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Franking)
--	delete from evf_Franking
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Franking

select distinct
v10s_FRANK.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_FRANK.EventID,
v10s_FRANK.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_FRANK.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_FRANK.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_FRANK.Acttime) THEN PEXDT.Acttime ELSE v10s_FRANK.Acttime END as [Changed],
case when FRANK.ActFlag<>'D' and FRANK.ActFlag<>'C' then v10s_FRANK.ActFlag else FRANK.ActFlag end as ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_FRANK.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_FRANK.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_FRANK.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_FRANK.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_FRANK.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_FRANK.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_FRANK.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_FRANK.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_FRANK.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_FRANK.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_FRANK.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_FRANK.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_FRANK.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_FRANK.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_FRANK.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_FRANK.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN (FRANK.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(FRANK.CntryCD) > 0) THEN '[' + FRANK.CntryCD +'] not found' ELSE Cntry.Country END as Country,
CASE WHEN FRANK.Frankflag = 'F' THEN 'Fully Franked'
     WHEN FRANK.Frankflag = 'P' THEN 'Partially Franked'
     WHEN FRANK.Frankflag = 'U' THEN 'Unfranked'
     WHEN FRANK.Frankflag = 'N' THEN 'Not known'
ELSE '' END as Franking,
CASE WHEN FRANK.Frankdiv<>'' THEN FRANK.Frankdiv
     WHEN FRANK.Frankflag = 'F' THEN DIVPY.GrossDividend
ELSE '' END as AmountFranked,
CASE WHEN FRANK.UnFrankdiv<>'' THEN FRANK.UnFrankdiv
     WHEN FRANK.Frankflag = 'U' THEN DIVPY.GrossDividend
ELSE '' END as AmountUnfranked,
'M' as Choice,
'RD|RdID|RdNotes' as Link_RecordDateNotes,
'DIV|DivID|DivNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Franking

FROM v10s_FRANK
INNER JOIN FRANK ON v10s_FRANK.EventID = FRANK.DivID
INNER JOIN RD ON RD.RdID = v10s_FRANK.RdID
LEFT OUTER JOIN DIVPY ON v10s_FRANK.EventID = DIVPY.DivID and FRANK.CntryCD = substring(DIVPY.CurenCD,1,2) and 'S' <> DIVPY.Divtype and 'B' <> DIVPY.Divtype
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID and v20c_wca2_dSCEXH.ExCountry = FRANK.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN CNTRY ON FRANK.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_FRANK.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or FRANK.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or DIVPY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_FRANK.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_FRANK.Acttime > (select max(wcaarch.dbo.evf_Franking.changed) from wcaarch.dbo.evf_Franking)
--	or FRANK.Acttime > (select max(wcaarch.dbo.evf_Franking.changed) from wcaarch.dbo.evf_Franking)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Franking.changed) from wcaarch.dbo.evf_Franking)
--	or DIVPY.Acttime > (select max(wcaarch.dbo.evf_Franking.changed) from wcaarch.dbo.evf_Franking)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Franking.changed) from wcaarch.dbo.evf_Franking)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Franking.changed) from wcaarch.dbo.evf_Franking))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Franking ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Franking ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Franking] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Franking] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Franking] ON [dbo].[evf_Franking]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Franking] ON [dbo].[evf_Franking]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Franking] ON [dbo].[evf_Franking]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Security_Swap, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Security_Swap')
	   drop table evf_Security_Swap

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Security_Swap)
--	delete from evf_Security_Swap
--	where EventID in
--	(select RdID from wca.dbo.SCSWP
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Security_Swap)
--	delete from evf_Security_Swap
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Security_Swap)
--	delete from evf_Security_Swap
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Security_Swap

select distinct
v10s_SCSWP.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_SCSWP.EventID,
v10s_SCSWP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SCSWP.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SCSWP.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SCSWP.Acttime) THEN PEXDT.Acttime ELSE v10s_SCSWP.Acttime END as [Changed],
v10s_SCSWP.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_SCSWP.NewRatio +':'+v10s_SCSWP.OldRatio as Ratio,
CASE WHEN (v10s_SCSWP.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_SCSWP.Fractions) > 0) THEN '[' + v10s_SCSWP.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_SCSWP.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
CASE WHEN (v10s_SCSWP.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_SCSWP.SectyCD) > 0) THEN '[' + v10s_SCSWP.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'SCSWP|RdID|ScswpNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Security_Swap

FROM v10s_SCSWP
INNER JOIN RD ON RD.RdID = v10s_SCSWP.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'SCSWP' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SCSWP' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_SCSWP.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON v10s_SCSWP.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON v10s_SCSWP.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON v10s_SCSWP.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_SCSWP.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_SCSWP.ActFlag<>'D')

--	WHERE
--	(v10s_SCSWP.Acttime > (select max(wcaarch.dbo.evf_Security_Swap.changed) from wcaarch.dbo.evf_Security_Swap)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Security_Swap.changed) from wcaarch.dbo.evf_Security_Swap)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Security_Swap.changed) from wcaarch.dbo.evf_Security_Swap)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Security_Swap.changed) from wcaarch.dbo.evf_Security_Swap))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Security_Swap ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Security_Swap ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Security_Swap] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Security_Swap] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Security_Swap] ON [dbo].[evf_Security_Swap]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Security_Swap] ON [dbo].[evf_Security_Swap]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Security_Swap] ON [dbo].[evf_Security_Swap]([changed]) ON [PRIMARY]
	  GO

print ""
GO
print "Data Generation evf_Odd_Lot_Offer, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Odd_Lot_Offer')
	  drop table evf_Odd_Lot_Offer

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Odd_Lot_Offer)
--	delete from evf_Odd_Lot_Offer
--	where EventID in
--	(select RdID from wca.dbo.ODDLT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Odd_Lot_Offer)
--	delete from evf_Odd_Lot_Offer
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Odd_Lot_Offer)
--	delete from evf_Odd_Lot_Offer
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Odd_Lot_Offer)
--	delete from evf_Odd_Lot_Offer
--	where EventID in
--	(select EventID from wca.dbo.MPAY
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Odd_Lot_Offer

select distinct
v10s_ODDLT.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('0'+rtrim(cast(serialid as char(2))),len(serialid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(MPAY.EventID as char(8)))
else cast(Seqnum as char(1))+'0101'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_ODDLT.EventID as char(8)))
end as Caref,
v10s_ODDLT.EventID,
v10s_ODDLT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ODDLT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ODDLT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ODDLT.Acttime) THEN PEXDT.Acttime ELSE v10s_ODDLT.Acttime END as [Changed],
v10s_ODDLT.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.ActFlag) > 0) THEN '[' + MPAY.ActFlag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.ActFlag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.ActFlag+']' ELSE irPaytype.Lookup+'['+MPAY.ActFlag+']' END as Paytype,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
'M' as Choice,
v10s_ODDLT.Startdate,
v10s_ODDLT.Enddate,
v10s_ODDLT.MinAcpQty,
v10s_ODDLT.MaxAcpQty,
v10s_ODDLT.BuyIn,
v10s_ODDLT.BuyInCurenCD,
v10s_ODDLT.BuyInPrice,
'RD|RdID|RDNotes' as Link_RecordDateNotes,
'ODDLT|RdID|Notes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency

	  into wcaarch.dbo.evf_Odd_Lot_Offer

FROM v10s_ODDLT
INNER JOIN RD ON RD.RdID = v10s_ODDLT.EventID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'ODDLT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ODDLT' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_ODDLT.EventID = MPAY.EventID AND v10s_ODDLT.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON MPAY.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.ActFlag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (v10s_ODDLT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or MPAY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
------and (v10s_ODDLT.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(v10s_ODDLT.Acttime > (select max(wcaarch.dbo.evf_Odd_Lot_Offer.changed) from wcaarch.dbo.evf_Odd_Lot_Offer)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Odd_Lot_Offer.changed) from wcaarch.dbo.evf_Odd_Lot_Offer)
--	or MPAY.Acttime > (select max(wcaarch.dbo.evf_Odd_Lot_Offer.changed) from wcaarch.dbo.evf_Odd_Lot_Offer)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Odd_Lot_Offer.changed) from wcaarch.dbo.evf_Odd_Lot_Offer)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Odd_Lot_Offer.changed) from wcaarch.dbo.evf_Odd_Lot_Offer))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Odd_Lot_Offer ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Odd_Lot_Offer ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Odd_Lot_Offer] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Odd_Lot_Offer] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Odd_Lot_Offer] ON [dbo].[evf_Odd_Lot_Offer]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Odd_Lot_Offer] ON [dbo].[evf_Odd_Lot_Offer]([issid]) ON [PRIMARY]
	  GO
 --	use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Odd_Lot_Offer] ON [dbo].[evf_Odd_Lot_Offer]([changed]) ON [PRIMARY]
	  GO

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

print ""
GO
print "Data Generation evf_Dividend, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Dividend')
	   drop table evf_Dividend

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend)
--	delete from evf_Dividend
--	where EventID in
--	(select DivID from wca.dbo.DIV
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend)
--	delete from evf_Dividend
--	where EventID in
--	(select DivID from wca.dbo.DIVPY
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend)
--	delete from evf_Dividend
--	where RdID in
--	(select RdID from wca.dbo.RD
--	where acttime>@Startdate)
--	GO
--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Dividend)
--	delete from evf_Dividend
--	where RdID in
--	(select RdID from wca.dbo.EXDT
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Dividend

select distinct
v10s_DIV.Levent,
case when OptionID is not null
then cast(Seqnum as char(1))
+substring('0'+rtrim(cast(optionid as char(2))),len(optionid),2)
+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(v10s_DIV.EventID as char(8)))
else cast(Seqnum as char(1))+'01'+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)
+rtrim(cast(v10s_DIV.EventID as char(8)))
end as Caref,
v10s_DIV.EventID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (DIVPY.Acttime is not null) and (DIVPY.Acttime > v10s_DIV.Acttime) and (DIVPY.Acttime > RD.Acttime) and (DIVPY.Acttime > EXDT.Acttime) and (DIVPY.Acttime > PEXDT.Acttime) THEN DIVPY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
v10s_DIV.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.ParValue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
v20c_WCA2_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
RD.RecDate,
RD.RegistrationDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.ExDate is not null THEN '' WHEN PEXDT.ExDate is not null THEN 'P' ELSE '' END as Pex,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN EXDT.PayDate is not null THEN '' WHEN PEXDT.PayDate is not null THEN 'P' ELSE '' END as Ppy,
v10s_DIV.FYEDate as FinYearEndDate,
CASE WHEN v10s_DIV.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_DIV.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_DIV.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_DIV.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_DIV.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_DIV.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_DIV.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_DIV.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_DIV.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_DIV.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_DIV.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_DIV.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_DIV.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_DIV.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN v10s_DIV.NilDividend= 'T' THEN 'Yes' ELSE '' END as NilDividend,
DIVPY.OptionID as OptionKey,
CASE WHEN DIVPY.DefaultOpt = 'T' THEN 'Yes' ELSE '' END as DefaultOption,
DIVPY.OptElectionDate as OptionElectionDate,
CASE WHEN (DIVPY.ActFlag is null) THEN irActionDIVPY.Lookup WHEN (irActionDIVPY.Lookup is null) and (LEN(DIVPY.ActFlag) > 0) THEN '[' + DIVPY.ActFlag +'] not found' ELSE irActionDIVPY.Lookup END as OptionRecordFlag,
CASE WHEN v10s_DIV.NilDividend= 'Y' THEN 'NilDividend'
     WHEN DIVPY.DivType= 'B' THEN 'Cash & Stock'
     WHEN DIVPY.DivType= 'S' THEN 'Stock'
     WHEN DIVPY.DivType= 'C' THEN 'Cash'
     ELSE 'Unspecified' END as DividendType,
case when DIVPY.RecindCashDiv='T' then '0' else DIVPY.GrossDividend end as GrossDividend,
case when DIVPY.RecindCashDiv='T' then '0' else DIVPY.NetDividend end as NetDividend,
CASE WHEN (DIVPY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(DIVPY.CurenCD) > 0) THEN '[' + DIVPY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN 1=1 THEN ' ' ELSE ' ' END as DivInPercent,
CASE WHEN DIVPY.RecindCashDiv= 'T' THEN 'Yes: Gross was: '+DIVPY.GrossDividend + ' Net was: '+ DIVPY.NetDividend + ' as on Changed date' ELSE '' END as CashDivRecinded,
DIVPY.TaxRate,
CASE WHEN DIVPY.Approxflag= 'T' THEN 'Yes' ELSE '' END as ApproximateDividend,
DIVPY.USDRateToCurrency,
CASE WHEN (DIVPY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(DIVPY.Fractions) > 0) THEN '[' + DIVPY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
DIVPY.Coupon,
DIVPY.CouponID,
DIVPY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
ressedol.Sedol as ResultantSedol,
DIVPY.RatioNew +':'+DIVPY.RatioOld as Ratio,
EXDT.Paydate2 as StockPayDate,
'M' as Choice,
'RD|RdID|RdNotes' as Link_RecordDateNotes,
'DIV|DivID|DivNotes' as Link_Notes,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
RD.RdID

	  into wcaarch.dbo.evf_Dividend

FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_WCA2_SCMST ON RD.SecID = v20c_WCA2_SCMST.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_wca2_dSCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_WCA2_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN DIVPY ON v10s_DIV.EventID = DIVPY.DivID
LEFT OUTER JOIN SCMST ON DIVPY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedol as ressedol ON DIVPY.ResSecID = ressedol.SecID
                   and v20c_wca2_dSCEXH.RegCountry = ressedol.RCntryCD
                   and v20c_wca2_dSCEXH.ExCountry = ressedol.CntryCD
LEFT OUTER JOIN irFRACTIONS ON DIVPY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irACTION as irACTIONDIVPY ON DIVPY.ActFlag = irACTIONDIVPY.Code
LEFT OUTER JOIN CUREN ON DIVPY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  (DIVPY.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or v10s_DIV.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or RD.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or EXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
	  or PEXDT.Acttime BETWEEN '2002/01/01' AND  '2008/07/01')
	  and (SectyGrp.secgrpid = 1 Or  SectyGrp.secgrpid = 2)
------and v10s_DIV.ActFlag<>'D'
	  and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	(DIVPY.Acttime > (select max(wcaarch.dbo.evf_Dividend.changed) from wcaarch.dbo.evf_Dividend)
--	or v10s_DIV.Acttime > (select max(wcaarch.dbo.evf_Dividend.changed) from wcaarch.dbo.evf_Dividend)
--	or RD.Acttime > (select max(wcaarch.dbo.evf_Dividend.changed) from wcaarch.dbo.evf_Dividend)
--	or EXDT.Acttime > (select max(wcaarch.dbo.evf_Dividend.changed) from wcaarch.dbo.evf_Dividend)
--	or PEXDT.Acttime > (select max(wcaarch.dbo.evf_Dividend.changed) from wcaarch.dbo.evf_Dividend))
--	and (v20c_wca2_dSCEXH.effectivedate>=EXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate>=PEXDT.exdate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Dividend ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Dividend ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Dividend] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Dividend] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Dividend] ON [dbo].[evf_Dividend]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Dividend] ON [dbo].[evf_Dividend]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Dividend] ON [dbo].[evf_Dividend]([changed]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_paydate_evf_Dividend] ON [dbo].[evf_Dividend]([paydate]) ON [PRIMARY]
	  GO



print ""
GO
print "Data Generation evf_Depositary_Receipt_Change, please wait..."
GO

	  use wcaarch
	  if exists (select * from sysobjects where name = 'evf_Depositary_Receipt_Change')
	   drop table evf_Depositary_Receipt_Change

--	use wcaarch
--	Declare @StartDate datetime
--	set @StartDate = (select max(changed) from evf_Depositary_Receipt_Change)
--	delete from evf_Depositary_Receipt_Change
--	where EventID in
--	(select DrchgID from wca.dbo.DRCHG
--	where acttime>@Startdate)

GO

use wca

--	insert into wcaarch.dbo.evf_Depositary_Receipt_Change

select distinct
v10s_DRCHG.Levent,
cast(Seqnum as char(1))+substring('000000'+rtrim(cast(scexhid as char(7))),len(scexhid),7)+rtrim(cast(EventID as char(8))) as Caref,
v10s_DRCHG.EventID,
v10s_DRCHG.AnnounceDate as Created,
v10s_DRCHG.Acttime as Changed,
v10s_DRCHG.ActFlag,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.IssuerName,
v20c_WCA2_SCMST.SecurityDesc,
v20c_WCA2_SCMST.Parvalue,
v20c_WCA2_SCMST.PVCurrency,
v20c_WCA2_SCMST.ISIN,
v20c_wca2_dSCEXH.Sedol,
v20c_WCA2_SCMST.USCode,
v20c_WCA2_SCMST.StatusFlag,
v20c_WCA2_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_wca2_dSCEXH.ExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD IS NULL OR v20c_WCA2_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_WCA2_SCMST.PrimaryExchgCD=v20c_wca2_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_wca2_dSCEXH.ExchgCD,
v20c_wca2_dSCEXH.MIC,
v20c_wca2_dSCEXH.ExCountry,
v20c_wca2_dSCEXH.RegCountry,
v20c_wca2_dSCEXH.Localcode,
v20c_wca2_dSCEXH.ListStatus,
v20c_wca2_dSCEXH.Listdate,
v10s_DRCHG.EffectiveDate,
v10s_DRCHG.OldDRratio +':'+v10s_DRCHG.OldUNratio as OldRatio,
v10s_DRCHG.NewDRratio +':'+v10s_DRCHG.NewUNratio as NewRatio,
v10s_DRCHG.OldUNSecID,
v10s_DRCHG.NewUNSecID,
CASE WHEN (v10s_DRCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_DRCHG.EventType) > 0) THEN '[' + v10s_DRCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_DRCHG.OldDepbank,
v10s_DRCHG.NewDepBank,
v10s_DRCHG.OldDRtype,
v10s_DRCHG.NewDRtype,
v10s_DRCHG.OldLevel,
v10s_DRCHG.NewLevel,
'DRCHG|DrchgID|DrchgNotes' as Link_Notes,
' ' as Choice,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID

	  into wcaarch.dbo.evf_Depositary_Receipt_Change

FROM v10s_DRCHG
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = v10s_DRCHG.SecID
INNER JOIN v20c_wca2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_wca2_dSCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_DRCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_WCA2_SCMST.SectyCD = SectyGrp.SectyCD

	  WHERE
	  v10s_DRCHG.Acttime BETWEEN '2002/01/01' AND  '2008/07/01'
------and (v10s_DRCHG.ActFlag<>'D')
	  and (v20c_wca2_dSCEXH.effectivedate>=v10s_DRCHG.EffectiveDate
	      or v20c_wca2_dSCEXH.effectivedate is null)

--	WHERE
--	v10s_DRCHG.Acttime > (select max(wcaarch.dbo.evf_Depositary_Receipt_Change.changed) from wcaarch.dbo.evf_Depositary_Receipt_Change)
--	and (v20c_wca2_dSCEXH.effectivedate>=v10s_DRCHG.EffectiveDate
--	or v20c_wca2_dSCEXH.effectivedate is null)

GO

	  print ""
	  GO
	  print "Key Generation evf_Depositary_Receipt_Change ,please  wait ....."
	  GO 
	  
	  use wcaarch 
	  ALTER TABLE evf_Depositary_Receipt_Change ALTER COLUMN caref bigint NOT NULL 
	  GO 
	  use wcaarch 
	  ALTER TABLE [DBO].[evf_Depositary_Receipt_Change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_caref_evf_Depositary_Receipt_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_secid_evf_Depositary_Receipt_Change] ON [dbo].[evf_Depositary_Receipt_Change]([secid]) ON [PRIMARY] 
	  GO 
	  use wcaarch 
	  CREATE  INDEX [ix_issid_evf_Depositary_Receipt_Change] ON [dbo].[evf_Depositary_Receipt_Change]([issid]) ON [PRIMARY]
	  GO
	  use wcaarch 
	  CREATE  INDEX [ix_changed_evf_Depositary_Receipt_Change] ON [dbo].[evf_Depositary_Receipt_Change]([changed]) ON [PRIMARY]
	  GO

--------------------------------------------------------------------------------------------------------------------------------------------------------------------	--	--	--	--	--	--	--		--	use wcaarch 

