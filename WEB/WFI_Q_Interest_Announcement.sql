"interest_announcement","select
RD.Recdate as Record_Date,
EXDT.Exdate as Ex_Date,
EXDT.Paydate as Pay_Date,
INTPY.CurenCD as Announced_Interest_Currency,
INTPY.IntRate as Interest_Rate,
INTPY.GrossInterest as Gross_Interest,
INTPY.NetInterest as Net_Interest,
INTPY.RescindInterest as Rescinded_Interest,
[INT].InterestFromDate as Interest_From_Date,
[INT].InterestToDate as Interest_To_Date,
[INT].Days,
case when indef.defaulttype<>'' then 'Full default' else '' end as Interest_Default,
INTPY.DomesticTaxRate as Domestic_Tax_Rate,
INTPY.AgencyFees as Agency_Fees,
INTPY.CouponNo as Coupon_Number,
INTPY.OptElectionDate as Election_Date,
INTPY.AnlCoupRate as Annual_Coupon_Rate,
INTPY.BCurenCD as Announced_Current_Currency,
INTPY.BParValue as Announced_Nominal_Value,
'INT|RdID|IntNotes' as Link_Notes,
CASE WHEN [INT].Actflag = 'D' or [INT].actflag='C' or intpy.actflag is null THEN [INT].Actflag ELSE INTPY.Actflag END as Actflag,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > [INT].Acttime) and (RD.Acttime > EXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > [INT].Acttime) THEN EXDT.Acttime ELSE [INT].Acttime END as Acttime,
[INT].AnnounceDate,
[INT].RdID as EventID,
scmst.secid
from [INT]
INNER JOIN RD ON [INT].RdID = RD.RdID
inner join scmst on RD.secid = scmst.secid
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID 
     AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
     AND EXDT.Actflag<>'D'
LEFT OUTER JOIN INTPY ON INT.RdID = INTPY.RdID
left outer join indef on scmst.secid = indef.secid and exdt.paydate = indef.defaultdate and 'INTDEFA' = indef.defaulttype
"
