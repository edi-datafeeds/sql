"Agency","select
agncy.RegistrarName as Agency_Name,
agncy.CntryCD as Agency_Country,
case when RELATION.Lookup is null then scagy.Relationship else RELATION.Lookup end as Relationship,
case when GTYPE.Lookup is null then scagy.GuaranteeType else GTYPE.Lookup end as Guarantee_Type,  
scagy.SpStartDate as Surety_Start_Date,
scagy.SpEndDate as Surety_End_Date,
scagy.Actflag,
scagy.Acttime,
scagy.AnnounceDate,
scagy.ScagyID as EventID,
scmst.secid
from scagy
left outer join agncy on scagy.agncyid = agncy.agncyid
inner join scmst on scagy.secid = scmst.secid
left outer join wfi.dbo.sys_lookup as RELATION on scagy.relationship = RELATION.Code
                      and 'RELATION' = RELATION.TypeGroup
left outer join wfi.dbo.sys_lookup as GTYPE on scagy.GuaranteeType = GTYPE.Code
                      and 'GTYPE' = GTYPE.TypeGroup
"
