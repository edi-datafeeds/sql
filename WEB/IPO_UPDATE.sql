  	use IPO
  	Declare @StartDate datetime
  	set @StartDate = (select max(modified) from IPO)
  	delete from IPO
  	where [ID] in
  	(select IpoID from wca.dbo.IPO

  	inner join wca.dbo.scmst on wca.dbo.ipo.secid = wca.dbo.scmst.secid
  	inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
  	left outer join wca.dbo.scexh on wca.dbo.ipo.secid = wca.dbo.scexh.secid
  	left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
  	left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid and wca.dbo.exchg.cntrycd = wca.dbo.sedol.cntrycd

  	where
  	(
  	wca.dbo.ipo.acttime >= (select max(acttime) from wca.dbo.tbl_opslog)
  	or (wca.dbo.scmst.acttime >= (select max(acttime) from wca.dbo.tbl_opslog)
  	     and getdate()-60<wca.dbo.ipo.AnnounceDate)
  	or (wca.dbo.scexh.acttime >= (select max(acttime) from wca.dbo.tbl_opslog)
  	     and getdate()-60<wca.dbo.ipo.AnnounceDate)
  	or (wca.dbo.sedol.acttime >= (select max(acttime) from wca.dbo.tbl_opslog)
  	     and getdate()-60<wca.dbo.ipo.AnnounceDate)
  	)
  	and
  	(
  	wca.dbo.scmst.primaryexchgcd = wca.dbo.scexh.exchgcd
  	or wca.dbo.scexh.exchgcd is null
  	))

  	GO

use ipo

  	insert into ipo.dbo.ipo

SELECT
wca.dbo.ipo.AnnounceDate as Created,
getdate() as Modified,
wca.dbo.ipo.IpoID as ID,
substring(wca.dbo.ipo.IPOStatus,1,1) as Status,
wca.dbo.ipo.Actflag,
wca.dbo.issur.Issuername,
wca.dbo.issur.CntryofIncorp as CntryInc,
wca.dbo.Scexh.ExchgCd,
wca.dbo.scmst.ISIN,
wca.dbo.sedol.Sedol,
wca.dbo.scmst.USCode,
wca.dbo.scexh.Localcode as Symbol,
wca.dbo.scmst.SecurityDesc as SecDescription,
wca.dbo.scmst.CurenCD as PVCurrency,
wca.dbo.scmst.ParValue,
wca.dbo.scmst.SecTyCD as Sectype,
wca.dbo.scexh.Lot as LotSize,
wca.dbo.ipo.TOFCurrency,
case when wca.dbo.ipo.TotalOfferSize<>'' then cast(cast(wca.dbo.ipo.TotalOfferSize as decimal(18,5))*1000000 as bigint) else null end as TotalOfferSize,
wca.dbo.ipo.SubPeriodFrom,
wca.dbo.ipo.SubperiodTo,
wca.dbo.ipo.MinSharesOffered,
wca.dbo.ipo.MaxSharesOffered,
case when wca.dbo.ipo.SharesOutstanding = '' or wca.dbo.ipo.SharesOutstanding = '0' then wca.dbo.scmst.SharesOutstanding else wca.dbo.ipo.SharesOutstanding end as SharesOutstanding,
wca.dbo.ipo.TOFCurrency as Currency,
case when wca.dbo.ipo.SharePriceLowest<>'' then cast(cast(wca.dbo.ipo.SharePriceLowest as decimal(15,5)) as money) else null end as SharePriceLowest,
case when wca.dbo.ipo.SharePriceHighest<>'' then cast(cast(wca.dbo.ipo.SharePriceHighest as decimal(15,5)) as money) else null end as SharePriceHighest,
case when wca.dbo.ipo.ProposedPrice<>'' then cast(cast(wca.dbo.ipo.ProposedPrice as decimal(15,5)) as money) else null end as ProposedPrice,
case when wca.dbo.ipo.InitialPrice<>'' then cast(cast(wca.dbo.ipo.InitialPrice as decimal(15,5)) as money) else null end as InitialPrice,
0 as InitialTradedVolume,
wca.dbo.ipo.Underwriter,
wca.dbo.ipo.DealType,
wca.dbo.ipo.LawFirm,
wca.dbo.ipo.TransferAgent,
'' as IndusCode,
wca.dbo.ipo.FirstTradingDate,
'' as SecLevel,
wca.dbo.ipo.Notes,
999 as userId,
'Y' as Release
from wca.dbo.IPO
inner join wca.dbo.scmst on wca.dbo.ipo.secid = wca.dbo.scmst.secid
inner join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
left outer join wca.dbo.scexh on wca.dbo.ipo.secid = wca.dbo.scexh.secid
                          and wca.dbo.scmst.primaryexchgcd = wca.dbo.scexh.exchgcd 
                          and 'D' <> wca.dbo.scexh.actflag
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd

left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid
                      and wca.dbo.exchg.cntrycd = wca.dbo.sedol.cntrycd
                      and  wca.dbo.scmst.curencd = wca.dbo.sedol.curencd
                      and 'D' <> wca.dbo.sedol.actflag

  	where
  	(
  	wca.dbo.ipo.acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_opslog)
  	or (wca.dbo.scmst.acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_opslog)
  	     and getdate()-60<wca.dbo.ipo.AnnounceDate)
  	or (wca.dbo.scexh.acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_opslog)
  	     and getdate()-60<wca.dbo.ipo.AnnounceDate)
  	or (wca.dbo.sedol.acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_opslog)
  	     and getdate()-60<wca.dbo.ipo.AnnounceDate)
  	)
  	and
  	(
         wca.dbo.scmst.primaryexchgcd = wca.dbo.scexh.exchgcd
         or wca.dbo.scexh.exchgcd is null
  	)
