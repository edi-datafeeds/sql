"Agency_Change","select
sachg.EffectiveDate as Effective_Date,
OLDAGNCY.RegistrarName as Old_Agency_Name,
NEWAGNCY.RegistrarName as New_Agency_Name,
OLDAGNCY.CntryCD as Old_Agency_Country,
NEWAGNCY.CntryCD as New_Agency_Country,
sachg.OldSpStartDate as Old_Surety_Start_Date,
sachg.NewSpStartDate as New_Surety_Start_Date,
sachg.OldSpEndDate as Old_Surety_End_Date,
sachg.NewSpEndDate as New_Surety_End_Date,
case when OLDRELATION.Lookup is null then sachg.OldRelationship else OLDRELATION.Lookup end as Old_Relationship,
case when NEWRELATION.Lookup is null then sachg.NewRelationship else NEWRELATION.Lookup end as New_Relationship,
case when OLDGTYPE.Lookup is null then sachg.OldGuaranteeType else OLDGTYPE.Lookup end as Old_Guarantee_Type,  
case when NEWGTYPE.Lookup is null then sachg.NewGuaranteeType else NEWGTYPE.Lookup end as New_Guarantee_Type,  
sachg.Actflag,
sachg.Acttime,
sachg.AnnounceDate,
sachg.sachgID as EventID,
scmst.secid
from sachg
inner join scagy on sachg.scagyid = scagy.scagyid
inner join scmst on scagy.secid = scmst.secid
left outer join agncy as OLDAGNCY on sachg.OldAgncyID = OLDAGNCY.agncyid
left outer join agncy as NEWAGNCY on sachg.OldAgncyID = NEWAGNCY.agncyid
left outer join wfi.dbo.sys_lookup as OLDRELATION on sachg.oldrelationship = OLDRELATION.Code
                      and 'RELATION' = OLDRELATION.TypeGroup
left outer join wfi.dbo.sys_lookup as NEWRELATION on sachg.newrelationship = NEWRELATION.Code
                      and 'RELATION' = NEWRELATION.TypeGroup
left outer join wfi.dbo.sys_lookup as OLDGTYPE on sachg.OldGuaranteeType = OLDGTYPE.Code
                      and 'GTYPE' = OLDGTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as NEWGTYPE on sachg.NewGuaranteeType = NEWGTYPE.Code
                      and 'GTYPE' = NEWGTYPE.TypeGroup
"
