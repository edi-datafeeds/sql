	  use WFI
	  if exists (select * from sysobjects where name = 'static_change')
	      drop table static_change
	  GO
	  use wca
	  SELECT distinct
	  'Security Name' as Static_Change,
	  SCCHG.SecOldName as Previous_Value,
	  SCCHG.SecNewName as Current_Value,
	  SCCHG.DateofChange as Effective_Date,
	  case when SCCHG.Eventtype is null then '' when RELEVENT.Lookup is null then SCCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
	  SCCHG.Actflag,
	  SCCHG.Acttime,
	  SCCHG.AnnounceDate,
	  SCCHG.ScchgID as EventID,
	  SCCHG.SecID
	  into WFI.dbo.static_change
	  from SCCHG
	  inner join bond on scchg.secid = bond.secid
	  left outer join WFI.dbo.sys_lookup as RELEVENT on SCCHG.EventType = RELEVENT.Code
	                        and 'EVENT' = RELEVENT.TypeGroup
	  WHERE
	  SecOldName <> SecNewName
	  GO

	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  SecID int NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  EventID int NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  static_change varchar(32) NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  Reason varchar(32) NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE [DBO].[static_change] WITH NOCHECK ADD 
	    CONSTRAINT [pk_static_change] PRIMARY KEY ( [SecID], [static_change],[EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_static_change_Acttime] ON [dbo].[static_change]([Acttime] desc) ON [PRIMARY] 
	  GO
	  use WFI 
	  CREATE  INDEX [ix_static_change_Effective] ON [dbo].[static_change]([Effective_Date]) ON [PRIMARY] 
	  GO

--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Security Name')
--	delete from static_change
--	where
--	static_change = 'Security Name'
--	and EventID in
--	(select ScchgID from wca.dbo.SCCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'ISIN')
--	delete from static_change
--	where
--	(static_change = 'ISIN'
--	or static_change = 'UsCode'
--	or static_change = 'CommonCode')
--	and EventID in
--	(select IccID from wca.dbo.ICC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Sedol')
--	delete from static_change
--	where
--	static_change = 'Sedol'
--	and EventID in
--	(select SdchgID from wca.dbo.SDCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Incorporation')
--	delete from static_change
--	where
--	static_change = 'Incorporation'
--	and EventID in
--	(select InchgID from wca.dbo.INCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Company Name')
--	delete from static_change
--	where
--	static_change = 'Company Name'
--	and EventID in
--	(select IschgID from wca.dbo.ISCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Listing Status')
--	delete from static_change
--	where
--	static_change = 'Listing Status'
--	and EventID in
--	(select LstatID from wca.dbo.LSTAT
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Local Code')
--	delete from static_change
--	where
--	static_change = 'Local Code'
--	and EventID in
--	(select LccID from wca.dbo.LCC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Interest Basis')
--	delete from static_change
--	where
--	static_change = 'Interest Basis'
--	and EventID in
--	(select IntbcID from wca.dbo.INTBC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = '')
--	delete from static_change
--	where
--	(static_change = 'Current Currency'
--	or static_change = 'Interest Currency'
--	or static_change = 'Maturity Currency'
--	or static_change = 'Interest BDC'
--	or static_change = 'Maturity BDC'
--	or static_change = 'Interest Type'
--	or static_change = 'Debt Type')
--	and EventID in
--	(select BschgID from wca.dbo.BSCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = '')
--	delete from static_change
--	where
--	(static_change = 'FRN Type'
--	or static_change = 'RFN_Index_Benchmark'
--	or static_change = 'FRN_Adjustment_Freq'
--	or static_change = 'FRN_Margin'
--	or static_change = 'FRN_Min_IR'
--	or static_change = 'FRN_Max_IR'
--	or static_change = 'FRN_Rounding')
--	and EventID in
--	(select FrnfxID from wca.dbo.FRNFX
--	where acttime>=@StartDate)
--	GO
--	
--	use wca
--	insert into wfi.dbo.static_change
--	SELECT distinct
--	'Security Name' as Static_Change,
--	SCCHG.SecOldName as Previous_Value,
--	SCCHG.SecNewName as Current_Value,
--	SCCHG.DateofChange as Effective_Date,
--	case when RELEVENT.Lookup is null then SCCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
--	SCCHG.Actflag,
--	SCCHG.Acttime,
--	SCCHG.AnnounceDate,
--	SCCHG.ScchgID as EventID,
--	SCCHG.SecID
--	from SCCHG 
--	inner join bond on scchg.secid = bond.secid
--	left outer join WFI.dbo.sys_lookup as RELEVENT on SCCHG.EventType = RELEVENT.Code
--	                   and 'EVENT' = RELEVENT.TypeGroup
--	WHERE
--	inner join bond on scchg.secid = bond.secid
--	and SecOldName <> SecNewName
--	and SCCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
--	                              then '2000/01/01'
--	                              else max(wfi.dbo.static_change.Acttime)
--	                              end
--	                  from wfi.dbo.static_change 
--	                  where wfi.dbo.static_change.static_change = 'Security Name')
--	GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'ISIN' as Static_Change,
icc.OldISIN as Previous_Value,
icc.NewISIN as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on icc.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldIsin <> NewIsin
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'ISIN')
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'US Code' as Static_Change,
icc.OldUsCode as Previous_Value,
icc.NewUsCode as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on ICC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldUsCode <> NewUsCode
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'UsCode')
GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Common Code' as Static_Change,
icc.OldCommonCode as Previous_Value,
icc.NewCommonCode as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on ICC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldCommonCode <> NewCommonCode
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'CommonCode')

GO
 

use wca
insert into wfi.dbo.static_change
SELECT distinct
'SEDOL' as Static_Change,
SDCHG.OldSedol as Previous_Value,
SDCHG.NewSedol as Current_Value,
SDCHG.Effectivedate as Effective_Date,
case when SDCHG.Eventtype is null then '' when RELEVENT.Lookup is null then SDCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
SDCHG.Actflag,
SDCHG.Acttime,
SDCHG.AnnounceDate,
SDCHG.SDCHGID as EventID,
SDCHG.SecID
from SDCHG
inner join bond on sdchg.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on SDCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldSedol <> NewSedol
and SDCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Sedol')

GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Incorporation' as Static_Change,
case when OLDCNTRY.Lookup is null then INCHG.OldCntryCD else OLDCNTRY.Lookup end as Previous_Value,
case when NEWCNTRY.Lookup is null then INCHG.NewCntryCD else NEWCNTRY.Lookup end as Current_Value,
INCHG.InchgDate as Effective_Date,
case when INCHG.Eventtype is null then '' when RELEVENT.Lookup is null then INCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
INCHG.Actflag,
INCHG.Acttime,
INCHG.AnnounceDate,
INCHG.INCHGID as EventID,
SCMST.SecID
from INCHG
INNER JOIN SCMST ON INCHG.ISSID = SCMST.ISSID
inner join bond on scmst.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on INCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCNTRY on INCHG.OldCntryCD = OLDCNTRY.Code
                      and 'CNTRY' = OLDCntry.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWCNTRY on INCHG.NewCntryCD = NEWCNTRY.Code
                      and 'CNTRY' = NEWCntry.TypeGroup
where
bond.Actflag<>'D'
and OldCntryCD <> NewCntryCD
and INCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Incorporation ')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'Company Name' as Static_Change,
ISCHG.IssOldName as Previous_Value,
ISCHG.IssNewName as Current_Value,
ISCHG.NameChangeDate as Effective_Date,
case when ISCHG.Eventtype is null then '' when RELEVENT.Lookup is null then ISCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ISCHG.Actflag,
ISCHG.Acttime,
ISCHG.AnnounceDate,
ISCHG.ISCHGID as EventID,
SCMST.SecID
from ISCHG
INNER JOIN SCMST ON ISCHG.ISSID = SCMST.ISSID
inner join bond on scmst.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ISCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and IssOldName <> IssNewName
and ISCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Company Name')

GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Local Code' as Static_Change,
LCC.OldLocalcode as Previous_Value,
LCC.Newlocalcode as Current_Value,
LCC.EffectiveDate as Effective_Date,
case when LCC.Eventtype is null then '' when RELEVENT.Lookup is null then LCC.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
LCC.Actflag,
LCC.Acttime,
LCC.AnnounceDate,
LCC.LCCID as EventID,
LCC.SecID
from LCC
inner join bond on lcc.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on LCC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldLocalcode <> NewLocalcode
and LCC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Local Code')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest Type' as Static_Change,
case when OLDINTTYPE.Lookup is null then INTBC.OldIntBasis else OLDINTTYPE.Lookup end as Previous_Value,
case when NEWINTTYPE.Lookup is null then INTBC.NewIntBasis else NEWINTTYPE.Lookup end as Current_Value,
INTBC.EffectiveDate as Effective_Date,
'Correction' as Reason,
INTBC.Actflag,
INTBC.Acttime,
INTBC.AnnounceDate,
INTBC.INTBCID as EventID,
INTBC.SecID
from INTBC
inner join bond on INTBC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as OLDINTTYPE on INTBC.OldIntBasis = OLDINTTYPE.Code
                      and 'INTTYPE' = OLDINTTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTTYPE on INTBC.NewIntBasis = NEWINTTYPE.Code
                      and 'INTTYPE' = NEWINTTYPE.TypeGroup
where
bond.Actflag<>'D'
and INTBC.OldIntBasis <> INTBC.NewIntBasis
and INTBC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                                 from wfi.dbo.static_change 
				 where wfi.dbo.static_change.static_change = 'Interest Type')
  
GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest Currency' as Static_Change,
BSCHG.OldInterestCurrency as Previous_Value,
BSCHG.NewInterestCurrency as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldInterestCurrency <> BSCHG.NewInterestCurrency
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Interest Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Maturity Currency' as Static_Change,
BSCHG.OldMaturityCurrency as Previous_Value,
BSCHG.NewMaturityCurrency as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldMaturityCurrency <> BSCHG.NewMaturityCurrency
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Maturity Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Current Currency' as Static_Change,
BSCHG.OldCurenCD as Previous_Value,
BSCHG.NewCurenCD as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldCurenCD <> BSCHG.NewCurenCD
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Current Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Debt Type' as Static_Change,
case when OLDBONDTYPE.Lookup is null then BSCHG.OldBondType else OLDBONDTYPE.Lookup end as Previous_Value,
case when NEWBONDTYPE.Lookup is null then BSCHG.NewBondType else NEWBONDTYPE.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDBONDTYPE on BSCHG.OldBondType = OLDBONDTYPE.Code
                      and 'BONDTYPE' = OLDBONDTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWBONDTYPE on BSCHG.NewBondType = NEWBONDTYPE.Code
                      and 'BONDTYPE' = NEWBONDTYPE.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldBondType <> BSCHG.NewBondType
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Debt Type')

GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest BDC' as Static_Change,
case when OLDINTBDC.Lookup is null then BSCHG.OldIntBusDayConv else OLDINTBDC.Lookup end as Previous_Value,
case when NEWINTBDC.Lookup is null then BSCHG.NewIntBusDayConv else NEWINTBDC.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDINTBDC on BSCHG.OldIntBusDayConv = OLDINTBDC.Code
                      and 'INTBDC' = OLDINTBDC.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTBDC on BSCHG.NewIntBusDayConv = NEWINTBDC.Code
                      and 'INTBDC' = NEWINTBDC.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldIntBusDayConv <> BSCHG.NewIntBusDayConv
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Interest BDC')

GO
 
use wca
insert into wfi.dbo.static_change
SELECT distinct
'Maturity BDC' as Static_Change,
case when OLDMATBDC.Lookup is null then BSCHG.OldMatBusDayConv else OLDMATBDC.Lookup end as Previous_Value,
case when NEWMATBDC.Lookup is null then BSCHG.NewMatBusDayConv else NEWMATBDC.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDMATBDC on BSCHG.OldMatBusDayConv = OLDMATBDC.Code
                      and 'INTBDC' = OLDMATBDC.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWMATBDC on BSCHG.NewMatBusDayConv = NEWMATBDC.Code
                      and 'INTBDC' = NEWMATBDC.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldMatBusDayConv  <> BSCHG.NewMatBusDayConv 
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Maturity BDC')
GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Rounding' as Static_Change,
FRNFX.OldRounding as Previous_Value,
FRNFX.NewRounding as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldRounding <> FRNFX.NewRounding
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Rounding')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Minimum IR' as Static_Change,
FRNFX.OldMinimumInterestRate as Previous_Value,
FRNFX.NewMinimumInterestRate as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMinimumInterestRate <> FRNFX.NewMinimumInterestRate
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Minimum IR')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Maximum IR' as Static_Change,
FRNFX.OldMaximumInterestRate as Previous_Value,
FRNFX.NewMaximumInterestRate as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMaximumInterestRate <> FRNFX.NewMaximumInterestRate
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Maximum IR')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Margin' as Static_Change,
FRNFX.OldMarkup as Previous_Value,
FRNFX.NewMarkup as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMarkup <> FRNFX.NewMarkup
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Margin')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN IR Adjustment Frequency' as Static_Change,
case when OLDFREQ.Lookup is null then FRNFX.OldFRNIntAdjFreq else OLDFREQ.Lookup end as Previous_Value,
case when NEWFREQ.Lookup is null then FRNFX.NewFRNIntAdjFreq else NEWFREQ.Lookup end as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDFREQ on FRNFX.OldFRNIntAdjFreq = OLDFREQ.Code
                      and 'FREQ' = OLDFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWFREQ on FRNFX.NewFRNIntAdjFreq = NEWFREQ.Code
                      and 'FREQ' = NEWFREQ.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNIntAdjFreq <> FRNFX.NewFRNIntAdjFreq
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  IR Adjustment Frequency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Index Benchmark' as Static_Change,
case when OLDINDXBEN.Lookup is null then FRNFX.OldFRNIndexBenchmark else OLDINDXBEN.Lookup end as Current_Value,
case when NEWINDXBEN.Lookup is null then FRNFX.NewFRNIndexBenchmark else NEWINDXBEN.Lookup end as Previous_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDINDXBEN on FRNFX.OldFRNIndexBenchmark = OLDINDXBEN.Code
                      and 'FRNINDXBEN' = OLDINDXBEN.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINDXBEN on FRNFX.NewFRNIndexBenchmark = NEWINDXBEN.Code
                      and 'FRNINDXBEN' = NEWINDXBEN.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNIndexBenchmark <> FRNFX.NewFRNIndexBenchmark
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Index Benchmark')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Type' as Static_Change,
case when OLDTYPE.Lookup is null then FRNFX.OldFRNType else OLDTYPE.Lookup end as Previous_Value,
case when NEWTYPE.Lookup is null then FRNFX.NewFRNType else NEWTYPE.Lookup end as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDTYPE on FRNFX.OldFRNType = OLDTYPE.Code
                      and 'FRNTYPE' = OLDTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWTYPE on FRNFX.NewFRNType = NEWTYPE.Code
                      and 'FRNTYPE' = NEWTYPE.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNType <> FRNFX.NewFRNType
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN Type')

GO
