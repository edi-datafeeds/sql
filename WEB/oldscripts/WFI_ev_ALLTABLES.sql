-- Out of date
print ""
go
print " DELETING from outstanding_amount, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'outstanding_amount')
	    drop table outstanding_amount
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from outstanding_amount)
--	delete from outstanding_amount
--	where EVENTID in
--	(select BOCHGID from wca.dbo.BOCHG
--	where acttime>@StartDate)

print ""
go
print " UPDATING outstanding_amount, please wait..."
go
 
use wca

--	insert into wfi.dbo.outstanding_amount

SELECT 
case when RELEVENT.Lookup is null then BOCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BOCHG.EffectiveDate as Effective_Date,
BOCHG.OldOutValue as Old_Outstanding_Value,
BOCHG.NewOutValue as New_Outstanding_Value,
BOCHG.OldOutDate as Old_Outstanding_Date,
BOCHG.NewOutDate as New_Outstanding_Date,
'WCA.dbo.BOCHG|BOCHGID|BOCHGNotes' as Link_Notes,
BOCHG.Actflag,
BOCHG.Acttime,
BOCHG.AnnounceDate,
BOCHG.BOCHGID as EventID,
BOCHG.SecID

	  into wfi.dbo.outstanding_amount

from BOCHG
left outer join WFI.dbo.sys_lookup as RELEVENT on BOCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup

--	WHERE
--	BOCHG.Acttime > (select max(wfi.dbo.outstanding_amount.acttime) from wfi.dbo.outstanding_amount)

	  WHERE
	  BOCHG.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Outstanding Amount keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE outstanding_amount ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[outstanding_amount] WITH NOCHECK ADD 
	   CONSTRAINT [pk_outstanding_amount_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_outstanding_amount_SecID] ON [dbo].[outstanding_amount]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_outstanding_amount_Acttime] ON [dbo].[outstanding_amount]([Acttime] desc) ON [PRIMARY]
	  GO


print ""
go
print " DELETING from maturity_change,  please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'maturity_change')
	    drop table maturity_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from maturity_change)
--	delete from maturity_change
--	where EVENTID in
--	(select MtchgID from wca.dbo.MTCHG
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING maturity_change, please wait..."
go
 
use wca

--	insert into wfi.dbo.maturity_change

SELECT 
case when RELEVENT.Lookup is null then MTCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
MTCHG.NotificationDate as Notification_Date,
MTCHG.OldMaturityDate as Old_Maturity_Date,
MTCHG.NewMaturityDate as New_Maturity_Date,
case when OLDMATBNHMRK.Lookup is null then MTCHG.OldMaturityBenchmark else OLDMATBNHMRK.Lookup end as Old_Maturity_Benchmark,
case when NEWMATBNHMRK.Lookup is null then MTCHG.NewMaturityBenchmark else NEWMATBNHMRK.Lookup end as New_Maturity_Benchmark,
'WCA.dbo.MTCHG|MTCHGID|Notes' as Link_Notes,
MTCHG.Actflag,
MTCHG.Acttime,
MTCHG.AnnounceDate,
MTCHG.MTCHGID as EventID,
MTCHG.SecID

	  into wfi.dbo.maturity_change

from MTCHG
inner join bond on mtchg.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on MTCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDMATBNHMRK on MTCHG.OldMaturityBenchmark = OLDMATBNHMRK.Code
                      and 'MATBNHMRK' = OLDMATBNHMRK.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWMATBNHMRK on MTCHG.NewMaturityBenchmark = NEWMATBNHMRK.Code
                      and 'MATBNHMRK' = NEWMATBNHMRK.TypeGroup

--	WHERE
--	MTCHG.Acttime > (select max(wfi.dbo.maturity_change.acttime) from wfi.dbo.maturity_change)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and MTCHG.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING maturity change keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE maturity_change ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[maturity_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_maturity_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_maturity_change_SecID] ON [dbo].[maturity_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_maturity_change_Acttime] ON [dbo].[maturity_change]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from redemption terms, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'redemption_terms')
	    drop table redemption_terms
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from redemption_terms)
--	delete from redemption_terms
--	where EVENTID in
--	(select RedmtID from wca.dbo.REDMT
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING redemption terms, please wait..."
go
 
use wca

--	insert into wfi.dbo.redemption_terms

SELECT 
RedemptionDate as Redemption_Date,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
case when REDEMTYPE.Lookup is null then REDMT.Redemptiontype else REDEMTYPE.Lookup end as Type,
case when CUREN.Lookup is null then REDMT.CurenCD else CUREN.Lookup end as Redemption_Currency,
REDMT.RedemptionPrice as Redemption_Price,
REDMT.Priceaspercent as Price_as_Percent,
REDMT.RedemptionPremium as Redemption_Premium,
REDMT.Premiumaspercent as Premium_as_Percent,
REDMT.RedemptionAmount as Redemption_Amount,
REDMT.RedemInPercent as Redemption_in_Percent,
'WCA.dbo.REDMT|RedmtID|RedemtNotes' as Link_Notes,
REDMT.Actflag,
REDMT.Acttime,
REDMT.AnnounceDate,
REDMT.RedmtID as EventID,
REDMT.SecID

	  into wfi.dbo.redemption_terms

from REDMT
inner join bond on redmt.secid = bond.secid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDMT.redemptiontype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as CUREN on REDMT.CurenCD = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup

--	WHERE
--	REDMT.Acttime > (select max(wfi.dbo.redemption_terms.acttime) from wfi.dbo.redemption_terms)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and REDMT.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING redemption terms keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE redemption_terms ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[redemption_terms] WITH NOCHECK ADD 
	   CONSTRAINT [pk_redemption_terms_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_redemption_terms_SecID] ON [dbo].[redemption_terms]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_redemption_terms_Acttime] ON [dbo].[redemption_terms]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from conversion terms, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'conversion_terms')
	    drop table conversion_terms
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion_terms)
--	delete from conversion_terms
--	where EVENTID in
--	(select ConvtID from wca.dbo.CONVT
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING conversion terms, please wait..."
go
 
use wca

--	insert into wfi.dbo.conversion_terms

SELECT 
CONVT.FromDate as From_Date,
CONVT.ToDate as To_Date,
CONVT.RatioNew as New_Ratio,
CONVT.RatioOld as Old_Ratio,
case when CUREN.Lookup is null then CONVT.CurenCD else CUREN.Lookup end as Conversion_Currency,
CONVT.Price as Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONVT.ResSecID,
case when SECTYCD.Lookup is null then CONVT.SectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONVT.Fractions else FRACTIONS.Lookup end as Fractions,
CONVT.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
CONVT.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONVT|ConvtID|ConvtNotes' as Link_Notes,
CONVT.Actflag,
CONVT.Acttime,
CONVT.AnnounceDate,
CONVT.ConvtID as EventID,
CONVT.SecID

	  into wfi.dbo.conversion_terms

from CONVT
inner join bond on convt.secid = bond.secid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONVT.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as CUREN on CONVT.CurenCD = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONVT.SectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONVT.Acttime > (select max(wfi.dbo.conversion_terms.acttime) from wfi.dbo.conversion_terms)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONVT.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING conversion terms keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE conversion_terms ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion_terms] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_terms_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_SecID] ON [dbo].[conversion_terms]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_Acttime] ON [dbo].[conversion_terms]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from conversion, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'conversion')
	    drop table conversion
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion)
--	delete from conversion
--	where EVENTID in
--	(select ConvID from wca.dbo.CONV
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING conversion, please wait..."
go
 
use wca

--	insert into wfi.dbo.conversion

SELECT 
CONV.FromDate as From_Date,
CONV.ToDate as To_Date,
CONV.RatioNew as New_Ratio,
CONV.RatioOld as Old_Ratio,
case when CUREN.Lookup is null then CONV.CurenCD else CUREN.Lookup end as Conversion_Currency,
CONV.Price as Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONV.ResSecID,
case when SECTYCD.Lookup is null then CONV.ResSectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONV.Fractions else FRACTIONS.Lookup end as Fractions,
CONV.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
CONV.ConvType as Conversion_Type,
RD.Recdate as Record_Date,
CONV.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONV|ConvID|CONVNotes' as Link_Notes,
CONV.Actflag,
CONV.Acttime,
CONV.AnnounceDate,
CONV.ConvID as EventID,
CONV.SecID

	  into wfi.dbo.conversion

from CONV
inner join bond on conv.secid = bond.secid
left outer join rd on CONV.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONV.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as CUREN on CONV.CurenCD = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONV.ResSectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONV.Acttime > (select max(wfi.dbo.conversion.acttime) from wfi.dbo.conversion)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONV.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING conversion keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE conversion ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_SecID] ON [dbo].[conversion]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_Acttime] ON [dbo].[conversion]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Reconvention, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'reconvention')
	    drop table Reconvention
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Reconvention)
--	delete from Reconvention
--	where EVENTID in
--	(select RconvID from wca.dbo.RCONV
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Reconvention, please wait..."
go
 
use wca

--	insert into wfi.dbo.Reconvention

SELECT 
RCONV.EffectiveDate as Effective_Date,
case when OLDINTACCRUAL.Lookup is null then RCONV.OldInterestAccrualConvention else OLDINTACCRUAL.Lookup end as Old_Interest_Accrual_Convention,
case when NEWINTACCRUAL.Lookup is null then RCONV.NewInterestAccrualConvention else NEWINTACCRUAL.Lookup end as New_Interest_Accrual_Convention,
case when OLDCMETHOD.Lookup is null then RCONV.OldConvMethod else OLDCMETHOD.Lookup end as Old_Convention_Method,
case when NEWCMETHOD.Lookup is null then RCONV.NewConvMethod else NEWCMETHOD.Lookup end as New_Convention_Method,
case when RELEVENT.Lookup is null then RCONV.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.RCONV|RconvID|Notes' as Link_Notes,
RCONV.Actflag,
RCONV.Acttime,
RCONV.AnnounceDate,
RCONV.RconvID as EventID,
RCONV.SecID

	  into wfi.dbo.Reconvention

from RCONV
left outer join WFI.dbo.sys_lookup as OLDINTACCRUAL on RCONV.OldInterestAccrualConvention = OLDINTACCRUAL.Code
                      and 'INTACCRUAL' = OLDINTACCRUAL.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTACCRUAL on RCONV.NewInterestAccrualConvention = NEWINTACCRUAL.Code
                      and 'INTACCRUAL' = NEWINTACCRUAL.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCMETHOD on RCONV.OldConvMethod = OLDCMETHOD.Code
                      and 'CMETHOD' = OLDCMETHOD.TypeGroup                        
left outer join WFI.dbo.sys_lookup as NEWCMETHOD on RCONV.NewConvMethod = NEWCMETHOD.Code
                      and 'CMETHOD' = NEWCMETHOD.TypeGroup                      
left outer join WFI.dbo.sys_lookup as RELEVENT on RCONV.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                                                          

--	WHERE
--	RCONV.Acttime > (select max(wfi.dbo.Reconvention.acttime) from wfi.dbo.Reconvention)

	  WHERE
	  RCONV.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Reconvention keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE Reconvention ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Reconvention] WITH NOCHECK ADD 
	   CONSTRAINT [pk_Reconvention_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_Reconvention_SecID] ON [dbo].[Reconvention]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_Reconvention_Acttime] ON [dbo].[Reconvention]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Redenomination, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'redenomination')
	    drop table Redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Redenomination)
--	delete from Redenomination
--	where EVENTID in
--	(select RdnomID from wca.dbo.RDNOM
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Redenomination, please wait..."
go
 
use wca

--	insert into wfi.dbo.Redenomination

SELECT 
RDNOM.EffectiveDate as Effective_Date,
RDNOM.OldDenomination1 as Old_Denom1,
RDNOM.OldDenomination2 as Old_Denom2,
RDNOM.OldDenomination3 as Old_Denom3,
RDNOM.OldDenomination4 as Old_Denom4,
RDNOM.OldDenomination5 as Old_Denom5,
RDNOM.OldDenomination6 as Old_Denom6,
RDNOM.OldDenomination7 as Old_Denom7,
RDNOM.OldMinimumDenomination as Old_Min_Denom,
RDNOM.OldDenominationMultiple as Old_Denom_Multiple,
RDNOM.NewDenomination1 as New_Denom1,
RDNOM.NewDenomination2 as New_Denom2,
RDNOM.NewDenomination3 as New_Denom3,
RDNOM.NewDenomination4 as New_Denom4,
RDNOM.NewDenomination5 as New_Denom5,
RDNOM.NewDenomination6 as New_Denom6,
RDNOM.NewDenomination7 as New_Denom7,
RDNOM.NewMinimumDenomination as New_Min_Denom,
RDNOM.NewDenominationMultiple as New_Denom_Multiple,
'WCA.dbo.RDNOM|RdnomID|Notes' as Link_Notes,
RDNOM.Actflag,
RDNOM.Acttime,
RDNOM.AnnounceDate,
RDNOM.RdnomID as EventID,
RDNOM.SecID

	  into wfi.dbo.Redenomination

from RDNOM
inner join bond on rdnom.secid = bond.secid

--	WHERE
--	RDNOM.Acttime > (select max(wfi.dbo.Redenomination.acttime) from wfi.dbo.Redenomination)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and RDNOM.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Redenomination keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE Redenomination ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_ReDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_ReDenom_SecID] ON [dbo].[Redenomination]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_ReDenom_Acttime] ON [dbo].[Redenomination]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Interest Frequency Change, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'int_freq_change')
	    drop table int_freq_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from int_freq_change)
--	delete from int_freq_change
--	where EVENTID in
--	(select IfchgId from wca.dbo.IFCHG
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Interest Frequency Change, please wait..."
go
 
use wca

--	insert into wfi.dbo.int_freq_change

SELECT 
IFCHG.NotificationDate as Notification_Date,
case when OLDFREQ.Lookup is null then IFCHG.OldIntPayFrqncy else OLDFREQ.Lookup end as Old_Interest_Pay_Freq,
case when (len(oldintpaydate1) <> 4 or oldintpaydate1 is NULL or oldintpaydate1 = '' ) then ''
when ((cast(substring(oldintpaydate1,3,2) as int) < 1) or (cast(substring(oldintpaydate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate1,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate1,1,2)
end as Old_Pay_Date_1,
case when (len(oldintpaydate2) <> 4 or oldintpaydate2 is NULL or oldintpaydate2 = '' ) then ''
when ((cast(substring(oldintpaydate2,3,2) as int) < 1) or (cast(substring(oldintpaydate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate2,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate2,1,2)
end as Old_Pay_Date_2,
case when (len(oldintpaydate3) <> 4 or oldintpaydate3 is NULL or oldintpaydate3 = '' ) then ''
when ((cast(substring(oldintpaydate3,3,2) as int) < 1) or (cast(substring(oldintpaydate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate3,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate3,1,2)
end as Old_Pay_Date_3,
case when (len(oldintpaydate4) <> 4 or oldintpaydate4 is NULL or oldintpaydate4 = '' ) then ''
when ((cast(substring(oldintpaydate4,3,2) as int) < 1) or (cast(substring(oldintpaydate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate4,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate4,1,2)
end as Old_Pay_Date_4,
case when NEWFREQ.Lookup is null then IFCHG.NewIntPayFrqncy else NEWFREQ.Lookup end as New_Interest_Pay_Freq,
case when (len(newintpaydate1) <> 4 or newintpaydate1 is NULL or newintpaydate1 = '' ) then ''
when ((cast(substring(newintpaydate1,3,2) as int) < 1) or (cast(substring(newintpaydate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate1,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate1,1,2)
end as New_Pay_Date_1,
case when (len(newintpaydate2) <> 4 or newintpaydate2 is NULL or newintpaydate2 = '' ) then ''
when ((cast(substring(newintpaydate2,3,2) as int) < 1) or (cast(substring(newintpaydate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate2,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate2,1,2)
end as New_Pay_Date_2,
case when (len(newintpaydate3) <> 4 or newintpaydate3 is NULL or newintpaydate3 = '' ) then ''
when ((cast(substring(newintpaydate3,3,2) as int) < 1) or (cast(substring(newintpaydate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate3,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate3,1,2)
end as New_Pay_Date_3,
case when (len(newintpaydate4) <> 4 or newintpaydate4 is NULL or newintpaydate4 = '' ) then ''
when ((cast(substring(newintpaydate4,3,2) as int) < 1) or (cast(substring(newintpaydate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate4,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate4,1,2)
end as New_Pay_Date_4,
case when RELEVENT.Lookup is null then IFCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
IFCHG.OldVarIntPayDate as Old_Var_Interest_Pay_Date,
IFCHG.NewVarIntPayDate as New_Var_Interest_Pay_Date,
IFCHG.Actflag,
IFCHG.Acttime,
IFCHG.AnnounceDate,
IFCHG.IfchgId as EventID,
IFCHG.SecID

	  into wfi.dbo.int_freq_change

from IFCHG                                                      
left outer join WFI.dbo.sys_lookup as OLDFREQ on IFCHG.OldIntPayFrqncy = OLDFREQ.Code
                      and 'FREQ' = OLDFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWFREQ on IFCHG.NewIntPayFrqncy = NEWFREQ.Code
                      and 'FREQ' = NEWFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as RELEVENT on IFCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                       

--	WHERE
--	IFCHG.Acttime > (select max(wfi.dbo.int_freq_change.acttime) from wfi.dbo.int_freq_change)

	  WHERE
	  IFCHG.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Interest Frequency Change, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE int_freq_change ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[int_freq_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_int_freq_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_int_freq_change_SecID] ON [dbo].[int_freq_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_int_freq_change_Acttime] ON [dbo].[int_freq_change]([Acttime] desc) ON [PRIMARY]
	  GO


print ""
go
print ""
go
print " DELETING from Interest Rate Change, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'int_rate_change')
	    drop table int_rate_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from int_rate_change)
--	delete from int_rate_change
--	where EVENTID in
--	(select IRChgId from wca.dbo.IRCHG
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Interest Rate Change, please wait..."
go
 
use wca

--	insert into wfi.dbo.int_rate_change

SELECT 
IRCHG.EffectiveDate as Effective_Date,
IRCHG.OldInterestRate as Old_Interest_Rate,
IRCHG.NewInterestRate as New_Interest_Rate,
case when RELEVENT.Lookup is null then IRCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.IRCHG|IRChgId|Notes' as Link_Notes,
IRCHG.Actflag,
IRCHG.Acttime,
IRCHG.AnnounceDate,
IRCHG.IRChgId as EventID,
IRCHG.SecID

	  into wfi.dbo.int_rate_change

from IRCHG                                                      
left outer join WFI.dbo.sys_lookup as RELEVENT on IRCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                       

--	WHERE
--	IRCHG.Acttime > (select max(wfi.dbo.int_rate_change.acttime) from wfi.dbo.int_rate_change)

	  WHERE
	  IRCHG.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Interest Rate Change, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE int_rate_change ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[int_rate_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_int_rate_change_EVENTIDD] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_int_rate_change_SecID] ON [dbo].[int_rate_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_int_rate_change_Acttime] ON [dbo].[int_rate_change]([Acttime] desc) ON [PRIMARY]
	  GO


print ""
go
print ""
go
print " DELETING from Consent, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'consent')
	    drop table consent
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from consent)
--	delete from consent
--	where EVENTID in
--	(select RdID from wca.dbo.COSNT
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Consent, please wait..."
go
 
use wca

--	insert into wfi.dbo.consent

SELECT 
RD.recdate as Record_Date,
EXDT.exdate as Ex_Date,
EXDT.paydate as Pay_Date,
COSNT.ExpiryDate as Expiry_Date,
COSNT.ExpiryTime as Expiry_Time,
SUBSTRING(cosnt.TimeZone, 1,3) AS Time_Zone,
case when YNBLANK.Lookup is null then COSNT.CollateralRelease else YNBLANK.Lookup end as Collateral_Release,
case when CUREN.Lookup is null then COSNT.Currency else CUREN.Lookup end as Redemption_Currency,
COSNT.Fee as Fee,
'WCA.dbo.COSNT|RdID|Notes' as Link_Notes,
COSNT.Actflag,
COSNT.Acttime,
COSNT.AnnounceDate,
COSNT.RdID as EventID,
RD.SecID 

	  into wfi.dbo.consent

from COSNT   
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCMST.PrimaryExchgCD = EXDT.ExchgCD AND 'COSNT' = EXDT.EventType
left outer join WFI.dbo.sys_lookup as YNBLANK on COSNT.CollateralRelease = YNBLANK.Code
                      and 'YNBLANK' = YNBLANK.TypeGroup
left outer join WFI.dbo.sys_lookup as CUREN on COSNT.Currency = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup
                      
--	WHERE
--	COSNT.Acttime > (select max(wfi.dbo.consent.acttime) from wfi.dbo.consent)

	  WHERE
	  COSNT.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Consent, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE consent ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[consent] WITH NOCHECK ADD 
	   CONSTRAINT [pk_consent_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_liquidation_SecID] ON [dbo].[consent]([SecID]) ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_consent_Acttime] ON [dbo].[consent]([Acttime] desc) ON [PRIMARY]
	  GO
print ""
go
print ""
go
print " DELETING from Takeover, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'takeover')
	    drop table takeover
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from takeover)
--	delete from takeover
--	where EVENTID in
--	(select TkovrID from wca.dbo.TKOVR
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Takeover, please wait..."
go
 
use wca

--	insert into wfi.dbo.takeover

SELECT 
RD.recdate as Record_Date,
TKOVR.OfferorName as Offeror_Name,
case when TKOVR.Hostile='T' then 'Yes'
     when TKOVR.Hostile='F' then 'No'
     else TKOVR.Hostile end as Hostile, 
TKOVR.OpenDate as Open_Date,
TKOVR.CloseDate as Close_Date,
case when TKOVRSTAT.Lookup is null then TKOVR.TkovrStatus else TKOVRSTAT.Lookup end as Takeover_Status,
TKOVR.PreOfferQty as Pre_Offer_Quantity,
TKOVR.PreOfferPercent as Pre_Offer_Percent,
TKOVR.TargetQuantity as Target_Quantity,
TKOVR.TargetPercent as Target_Percent,
TKOVR.UnconditionalDate as Unconditional_Date,
TKOVR.CmAcqDate as CompulsoryAcqDate,
TKOVR.MinAcpQty as MinAcceptanceQuant,
TKOVR.MaxAcpQty as MaxAcceptanceQuant,
case when TKOVR.MiniTkovr='T' then 'Yes'
     when TKOVR.MiniTkovr='F' then 'No'
     else TKOVR.MiniTkovr end as Mini_Takeover, 
'WCA.dbo.TKOVR|TkovrID|TkovrNotes' as Link_Notes,
TKOVR.Actflag,
TKOVR.Acttime,
TKOVR.AnnounceDate,
TKOVR.TkovrID as EventID,
TKOVR.SecID 

	  into wfi.dbo.takeover

from TKOVR   
left outer join RD ON TKOVR.RdID = RD.RdID
inner join BOND on TKOVR.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as TKOVRSTAT on TKOVR.TkovrStatus = TKOVRSTAT.Code
                      and 'TKOVRSTAT' = TKOVRSTAT.TypeGroup

--	WHERE
--	TKOVR.Acttime > (select max(wfi.dbo.takeover.acttime) from wfi.dbo.takeover)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and TKOVR.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Takeover, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE takeover ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[takeover] WITH NOCHECK ADD 
	   CONSTRAINT [pk_takeover_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_takeover_Acttime] ON [dbo].[takeover]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Currency Redenomination, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'cur_redenomination')
	    drop table cur_redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from cur_redenomination)
--	delete from cur_redenomination
--	where EVENTID in
--	(select CurrdID from wca.dbo.CURRD
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Currency Redenomination, please wait..."
go
 
use wca

--	insert into wfi.dbo.cur_redenomination

SELECT 
CURRD.EffectiveDate as Effective_Date,
case when OLDCUREN.Lookup is null then CURRD.OldCurenCD else OLDCUREN.Lookup end as Old_Redemption_Currency,
case when NEWCUREN.Lookup is null then CURRD.NewCurenCD else NEWCUREN.Lookup end as New_Redemption_Currency,
CURRD.OldParValue as Old_Par_Value,
CURRD.NewParValue as New_Par_Value,
case when RELEVENT.Lookup is null then CURRD.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.CURRD|CurrdID|CurRdNotes' as Link_Notes,
CURRD.Actflag,
CURRD.Acttime,
CURRD.AnnounceDate,
CURRD.CurrdID as EventID,
CURRD.SecID 

	  into wfi.dbo.cur_redenomination

from CURRD   
inner join BOND on CURRD.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on CURRD.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCUREN on CURRD.OldCurenCD = OLDCUREN.Code
                      and 'CUREN' = OLDCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWCUREN on CURRD.NewCurenCD = NEWCUREN.Code
                      and 'CUREN' = NEWCUREN.TypeGroup                      
                      
--	WHERE
--	CURRD.Acttime > (select max(wfi.dbo.cur_redenomination.acttime) from wfi.dbo.cur_redenomination)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CURRD.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Currency Redenomination, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE cur_redenomination ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[cur_redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_cur_reDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_cur_reDenom_Acttime] ON [dbo].[cur_redenomination]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Buy back, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'buy_back')
	    drop table buy_back
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from buy_back)
--	delete from buy_back
--	where EVENTID in
--	(select BBID from wca.dbo.BB
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Buy back, please wait..."
go
 
use wca

--	insert into wfi.dbo.buy_back

SELECT 
RD.recdate as Record_Date,
case when ONOFFMARKET.Lookup is null then BB.OnOffFlag else ONOFFMARKET.Lookup end as On_Off_Market,
BB.StartDate as Start_Date,
BB.EndDate as End_Date,
BB.MinAcpQty as Min_Acceptance_Quantity,
BB.MaxAcpQty as Max_Acceptance_Quantity,
BB.BBMinPct as Min_Percent,
BB.BBMaxPct as Max_Percent,
'WCA.dbo.BB|BBID|BBNotes' as Link_Notes,
BB.Actflag,
BB.Acttime,
BB.AnnounceDate,
BB.BBID as EventID,
BB.SecID  

	  into wfi.dbo.buy_back

from BB   
left outer join RD ON BB.RdID = RD.RdID
inner join BOND on BB.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as ONOFFMARKET on BB.OnOffFlag = ONOFFMARKET.Code
                      and 'ONOFFMARKET' = ONOFFMARKET.TypeGroup
                                       
                      
--	WHERE
--	BB.Acttime > (select max(wfi.dbo.buy_back.acttime) from wfi.dbo.buy_back)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and BB.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Buy back, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE buy_back ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[buy_back] WITH NOCHECK ADD 
	   CONSTRAINT [pk_buy_back_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_buy_back_Acttime] ON [dbo].[buy_back]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Parvalue Redenomination, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'pv_redenomination')
	    drop table pv_redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from pv_redenomination)
--	delete from pv_redenomination
--	where EVENTID in
--	(select PvRdID from wca.dbo.PVRD
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Parvalue Redenomination, please wait..."
go
 
use wca

--	insert into wfi.dbo.pv_redenomination

SELECT 
PVRD.EffectiveDate as Effective_Date,
case when CUREN.Lookup is null then PVRD.CurenCD else CUREN.Lookup end as Redemption_Currency,
PVRD.OldParValue as Old_Par_Value,
PVRD.NewParValue as New_Par_Value,
case when RELEVENT.Lookup is null then PVRD.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.PVRD|PvRdID|PvRdNotes' as Link_Notes,
PVRD.Actflag,
PVRD.Acttime,
PVRD.AnnounceDate,
PVRD.PvRdID as EventID,
PVRD.SecID  

	  into wfi.dbo.pv_redenomination

from PVRD   
inner join BOND on PVRD.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as CUREN on PVRD.CurenCD = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup  
left outer join WFI.dbo.sys_lookup as RELEVENT on PVRD.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                      
                                       
                      
--	WHERE
--	PVRD.Acttime > (select max(wfi.dbo.pv_redenomination.acttime) from wfi.dbo.pv_redenomination)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and PVRD.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Parvalue Redenomination, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE pv_redenomination ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[pv_redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_pv_reDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_pv_reDenom_Acttime] ON [dbo].[pv_redenomination]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Announcement, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'announcement')
	    drop table announcement
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from announcement)
--	delete from announcement
--	where EVENTID in
--	(select AnnID from wca.dbo.ANN
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Announcement, please wait..."
go
 
use wca

--	insert into wfi.dbo.announcement

SELECT 
case when RELEVENT.Lookup is null then ANN.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ANN.NotificationDate as Notification_Date,
'WCA.dbo.ANN|AnnID|AnnNotes' as Link_Notes,
ANN.Actflag,
ANN.Acttime,
ANN.AnnounceDate,
ANN.AnnID as EventID,
SCMST.SecID  

	  into wfi.dbo.announcement

from ANN   
inner join SCMST on ANN.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on ANN.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup  
                
                                                             
--	WHERE
--	ANN.Acttime > (select max(wfi.dbo.announcement.acttime) from wfi.dbo.announcement)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and ANN.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Announcement, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE announcement ALTER COLUMN EVENTID int NOT NULL
	  ALTER TABLE announcement ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[announcement] WITH NOCHECK ADD 
	   CONSTRAINT [pk_announcement] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_announcement_Acttime] ON [dbo].[announcement]([Acttime] desc) ON [PRIMARY]
	  GO


print ""
go
print ""
go
print " DELETING from Liquidation, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'liquidation')
	    drop table liquidation
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from liquidation)
--	delete from liquidation
--	where EVENTID in
--	(select LiqID from wca.dbo.LIQ
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Liquidation, please wait..."
go
 
use wca

--	insert into wfi.dbo.liquidation

SELECT 
LIQ.Liquidator as Liquidator,
LIQ.LiqAdd1 as Address1,
LIQ.LiqAdd2 as Address2,
LIQ.LiqAdd3 as Address3,
LIQ.LiqAdd4 as Address4,
LIQ.LiqAdd5 as Address5,
LIQ.LiqAdd6 as Address6,
LIQ.LiqCity as City,
case when CNTRY.Lookup is null then LIQ.LiqCntryCD else CNTRY.Lookup end as Country,
LIQ.LiqTel as Telephone,
LIQ.LiqFax as Fax,
LIQ.LiqEmail as Email,
LIQ.RdDate as Record_Date,
'WCA.dbo.LIQ|LiqID|LiquidationTerms' as Link_Notes,
LIQ.Actflag,
LIQ.Acttime,
LIQ.AnnounceDate,
LIQ.LiqID as EventID,
SCMST.SecID  

	  into wfi.dbo.liquidation

from LIQ   
inner join SCMST on LIQ.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as CNTRY on LIQ.LiqCntryCD = CNTRY.Code
                      and 'CNTRY' = CNTRY.TypeGroup     
                
                                                             
--	WHERE
--	LIQ.Acttime > (select max(wfi.dbo.liquidation.acttime) from wfi.dbo.liquidation)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and LIQ.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Liquidation, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE liquidation ALTER COLUMN EVENTID int NOT NULL
	  ALTER TABLE liquidation ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[liquidation] WITH NOCHECK ADD 
	   CONSTRAINT [pk_liquidation_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_liquidation_Acttime] ON [dbo].[liquidation]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Bankruptcy, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'bankruptcy')
	    drop table bankruptcy
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from bankruptcy)
--	delete from bankruptcy
--	where EVENTID in
--	(select BkrpID from wca.dbo.BKRP
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Bankruptcy, please wait..."
go
 
use wca

--	insert into wfi.dbo.bankruptcy

SELECT 
BKRP.NotificationDate as Notification_Date,
BKRP.FilingDate as Filing_Date,
'WCA.dbo.BKRP|BkrpID|BkrpNotes' as Link_Notes,
BKRP.Actflag,
BKRP.Acttime,
BKRP.AnnounceDate,
BKRP.BkrpID as EventID,
SCMST.SecID  

	  into wfi.dbo.bankruptcy

from BKRP   
inner join SCMST on BKRP.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID

                                                             
--	WHERE
--	BKRP.Acttime > (select max(wfi.dbo.bankruptcy.acttime) from wfi.dbo.bankruptcy)

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and BKRP.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Bankruptcy, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE bankruptcy ALTER COLUMN  EventID int NOT NULL
	  ALTER TABLE bankruptcy ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[bankruptcy] WITH NOCHECK ADD 
	   CONSTRAINT [pk_bankruptcy_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_bankruptcy_Acttime] ON [dbo].[bankruptcy]([Acttime] desc) ON [PRIMARY]
	  GO


print ""
go
print ""
go
print " DELETING from Legal Action, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'legal_action')
	    drop table legal_action
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from legal_action)
--	delete from legal_action
--	where EVENTID in
--	(select LawstID from wca.dbo.LAWST
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Legal Action, please wait..."
go
 
use wca

--	insert into wfi.dbo.Legal_Action

SELECT 
LAWST.EffectiveDate as Effective_Date,
CASE WHEN LAWST.LAType= 'CLSACT' THEN 'Class Action' ELSE 'Other' END as Legal_Action_Type,
LAWST.Regdate as Registration_Date,
'WCA.dbo.LAWST|LawstID|LawstNotes' as Link_Notes,
LAWST.Actflag,
LAWST.Acttime,
LAWST.AnnounceDate,
LAWST.LawstID as EventID,
SCMST.SecID  

	  into wfi.dbo.legal_action

from LAWST   
inner join SCMST on LAWST.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID

--	WHERE
--	LAWST.Acttime > (select max(wfi.dbo.legal_action.acttime) from wfi.dbo.legal_action)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and LAWST.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Legal Action, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE legal_action ALTER COLUMN  EventID int NOT NULL
	  ALTER TABLE legal_action ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[legal_action] WITH NOCHECK ADD 
	   CONSTRAINT [pk_legal_action_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_legal_action_Acttime] ON [dbo].[legal_action]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print ""
go
print " DELETING from Bondholder Meeting, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'bondholder_meeting')
	    drop table bondholder_meeting
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from bondholder_meeting)
--	delete from bondholder_meeting
--	where AGMID in
--	(select AGMID from wca.dbo.AGM
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING Bondholder Meeting, please wait..."
go
 
use wca

--	insert into wfi.dbo.bondholder_meeting

SELECT 
AGM.AGMDate as AGM_Date,
case when AGMEGM.Lookup is null then AGM.AGMEGM else AGMEGM.Lookup end as Meeting_Type,
--case when AGM.BondSecID  
AGM.AGMNo as AGM_Number,
AGM.FYEDate as Fin_Year_End_Date,
AGM.AGMTime as AGM_Time,
AGM.ADD1 as Address1,
AGM.ADD2 as Address2,
AGM.ADD3 as Address3,
AGM.ADD4 as Address4,
AGM.ADD5 as Address5,
AGM.ADD6 as Address6,
AGM.City as City,
case when CNTRY.Lookup is null then AGM.CntryCD else CNTRY.Lookup end as Country,
AGM.Actflag,
AGM.Acttime,
AGM.AnnounceDate,
AGM.AGMID as EventID,
AGM.BondSecID as SecID  

	  into wfi.dbo.bondholder_meeting

from AGM  
inner join BOND on AGM.BondSecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as AGMEGM on AGM.AGMEGM = AGMEGM.Code
                      and 'AGMEGM' = AGMEGM.TypeGroup  
left outer join WFI.dbo.sys_lookup as CNTRY on AGM.CntryCD = CNTRY.Code
                      and 'CNTRY' = CNTRY.TypeGroup                      
                                       
                      
--	WHERE
--	AGM.Acttime > (select max(wfi.dbo.bondholder_meeting.acttime) from wfi.dbo.bondholder_meeting)
--	AND BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and BondSecID is not null and BondSecID>0

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and BondSecID is not null and BondSecID>0
	  and AGM.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING Bondholder Meeting, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE bondholder_meeting ALTER COLUMN EventID int NOT NULL
	  ALTER TABLE bondholder_meeting ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[bondholder_meeting] WITH NOCHECK ADD 
	   CONSTRAINT [pk_bondholder_meeting_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_bondholder_meeting_Acttime] ON [dbo].[bondholder_meeting]([Acttime] desc) ON [PRIMARY]
	  GO

print ""
go
print " DELETING from redemption, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'redemption')
	    drop table redemption
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from redemption)
--	delete from redemption
--	where RedemID in
--	(select RedemID from wca.dbo.REDEM
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING redemption, please wait..."
go
 
use wca

--	insert into wfi.dbo.redemption

SELECT 
RedemDate as Redemption_Date,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
case when REDEMTYPE.Lookup is null then REDEM.Redemtype else REDEMTYPE.Lookup end as Type,
Poolfactor as Pool_factor,
RedemPercent as Percentage_Redeemed,
case when CUREN.Lookup is null then REDEM.CurenCD else CUREN.Lookup end as Redemption_Currency,
REDEM.RedemPrice as Redemption_Price,
REDEM.Priceaspercent as Price_as_Percent,
REDEM.RedemPremium as Redemption_Premium,
REDEM.Premiumaspercent as Premium_as_Percent,
RD.Recdate as Record_Date,
'WCA.dbo.REDEM|RedemID|RedemNotes' as Link_Notes,
REDEM.Actflag,
REDEM.Acttime,
REDEM.AnnounceDate,
REDEM.RedemID,
REDEM.SecID

	  into wfi.dbo.redemption

from REDEM
inner join bond on redem.secid = bond.secid
left outer join rd on redem.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDEM.Redemtype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as CUREN on REDEM.CurenCD = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup

--	WHERE
--	REDEM.Acttime > (select max(wfi.dbo.redemption.acttime) from wfi.dbo.redemption)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and REDEM.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING redemption keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE redemption ALTER COLUMN  RedemID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[redemption] WITH NOCHECK ADD 
	   CONSTRAINT [pk_redemption_RedemID] PRIMARY KEY ([RedemID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_redemption_SecID] ON [dbo].[redemption]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_redemption_Acttime] ON [dbo].[redemption]([Acttime] desc) ON [PRIMARY]
	  GO
print ""
go
print " DELETING from reference, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'reference')
	    drop table reference
	  go

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from reference)
--	delete from reference
--	where SecID in
--	(select SecID from wca.dbo.BOND
--	where acttime>@StartDate)
--	go

print ""
go
print " UPDATING reference, please wait..."
go
 
use wca
--	insert into wfi.dbo.reference
SELECT 
case when BondType.Lookup is null then bond.bondtype else BondType.Lookup end as Security_Type, 
case when STRUCTCD.Lookup is null then SCMST.StructCD else STRUCTCD.Lookup end as Debt_Structure,
SCMST.Regs144a as Regulation_Code,
case when CURCUREN.Lookup is null then BOND.CurenCD else CURCUREN.Lookup end as Current_Currency,
case when HOLDING.Lookup is null then SCMST.Holding else HOLDING.Lookup end as Holding,
BOND.ParValue as Nominal_Value, 
BOND.Denomination1 as Denom_1,
BOND.Denomination2 as Denom_2,
BOND.Denomination3 as Denom_3,
BOND.Denomination4 as Denom_4,
BOND.Denomination5 as Denom_5,
BOND.Denomination6 as Denom_6,
BOND.Denomination7 as Denom_7,
BOND.MinimumDenomination as Min_Denom, 
BOND.DenominationMultiple as Denom_Multiple, 
BOND.IssueDate as Issue_Date, 
case when ISSCUREN.Lookup is null then BOND.IssueCurrency else ISSCUREN.Lookup end as Issue_Currency, 
BOND.IssuePrice as Issue_Price, 
BOND.PriceAsPercent as Price_as_Percent,
BOND.IssueAmount as Issue_Amount_in_Millions, 
BOND.IssueAmountDate as Issue_Amount_Date, 
BOND.Series, 
BOND.Class, 
case when BOND.SeniorJunior='S' then 'Senior' when BOND.SeniorJunior='J' then 'Junior' when BOND.SeniorJunior='M' then 'Mezzanine' ELSE '' end as Financing_Structure,
BOND.OutstandingAmount as Outstanding_Amount, 
BOND.OutstandingAmountDate as Outstanding_Amount_Date, 
case when INTTYPE.Lookup is null then bond.InterestBasis else INTTYPE.Lookup end as Interest_Basis, 
case when FRNTYPE.Lookup is null then bond.FRNType else FRNTYPE.Lookup end as FRN_Type,  
case when INTCUREN.Lookup is null then BOND.InterestCurrency else INTCUREN.Lookup end as Interest_Currency, 
case when FRNINDXBEN.Lookup is null then bond.FRNIndexBenchmark else FRNINDXBEN.Lookup end as FRN_Index_Benchmark,  
BOND.Markup as Margin,
BOND.Rounding, 
BOND.InterestRate as Interest_Rate, 
BOND.MinimumInterestRate as Min_Interest_Rate, 
BOND.MaximumInterestRate as Max_Interest_Rate, 
BOND.IntCommencementDate as Interest_Commencement_Date, 
BOND.FirstCouponDate as First_Coupon_Date, 
case when BOND.Guaranteed='T' then 'Yes' ELSE 'No' end as Guaranteed,
case when BOND.SecuredBy='Y' then 'Yes' when BOND.SecuredBy='N' then 'No' ELSE '' end as Secured_By,
case when BOND.WarrantAttached='Y' then 'Yes' when BOND.WarrantAttached='N' then 'No' ELSE '' end as Warrant_Attached,
case when BOND.Subordinate='Y' then 'Yes' when BOND.Subordinate='N' then 'No' ELSE '' end as Subordinate,
case when ASSETBKD.Lookup is null then bond.SecurityCharge else ASSETBKD.Lookup end as Security_Charge, 
case when INTACCRUAL.Lookup is null then bond.InterestAccrualConvention else INTACCRUAL.Lookup end as Interest_Accrual_Convention,
case when INTBDC.Lookup is null then bond.IntBusDayConv else INTBDC.Lookup end as Interest_Biz_Day_Convention,  
ConventionMethod as Convention_Method,  
case when STRIP.Lookup is null then bond.Strip else STRIP.Lookup end as Strip, 
BOND.StripInterestNumber as Strip_Interest_Number,
case when FREQ.Lookup is null then bond.InterestPaymentFrequency else FREQ.Lookup end as Interest_Payment_Freq,  
case when BOND.VarIntPayDate='Y' then 'Yes' when BOND.VarIntPayDate='N' then 'No' ELSE '' end as Variable_Interest_Paydate,
BOND.FrnIntAdjFreq as FRN_Interest_Adjustment_Freq, 
case when (len(InterestPayDate1) <> 4 or InterestPayDate1 is NULL or InterestPayDate1 = '' ) then ''
when ((cast(substring(InterestPayDate1,3,2) as int) < 1) or (cast(substring(InterestPayDate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate1,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate1,1,2)
end as Interest_PayDate1,
case when (len(InterestPayDate2) <> 4 or InterestPayDate2 is NULL or InterestPayDate2 = '' ) then ''
when ((cast(substring(InterestPayDate2,3,2) as int) < 1) or (cast(substring(InterestPayDate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate2,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate2,1,2)
end as Interest_PayDate2,
case when (len(InterestPayDate3) <> 4 or InterestPayDate3 is NULL or InterestPayDate3 = '' ) then ''
when ((cast(substring(InterestPayDate3,3,2) as int) < 1) or (cast(substring(InterestPayDate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate3,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate3,1,2)
end as Interest_PayDate3,
case when (len(InterestPayDate4) <> 4 or InterestPayDate4 is NULL or InterestPayDate4 = '' ) then ''
when ((cast(substring(InterestPayDate4,3,2) as int) < 1) or (cast(substring(InterestPayDate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate4,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate4,1,2)
end as Interest_PayDate4,
case when BOND.Perpetual='I' then 'Irredeemable' when BOND.Perpetual='P' then 'Perpetual' when BOND.Perpetual='U' then 'Undated' ELSE '' end as Perpetual,
case when DEBTSTATUS.Lookup is null then bond.MaturityStructure else DEBTSTATUS.Lookup end as Maturity_Structure,  
BOND.MaturityDate as Maturity_Date, 
case when BOND.MaturityExtendible='Y' then 'Yes' when BOND.MaturityExtendible='N' then 'No' ELSE '' end as Maturity_Extendible,
case when MATCUREN.Lookup is null then BOND.MaturityCurrency else MATCUREN.Lookup end as Maturity_Currency, 
BOND.MatPrice as Maturity_Price,
BOND.MatPriceAsPercent as Maturity_Price_as_Percent,
case when MATBNHMRK.Lookup is null then bond.MaturityBenchmark else MATBNHMRK.Lookup end as Maturity_Benchmark,  
case when MATBDC.Lookup is null then bond.MatBusDayConv else MATBDC.Lookup end as Maturity_Biz_Day_Convention,  
case when BOND.Cumulative='Y' then 'Yes' when BOND.Cumulative='N' then 'No' ELSE '' end as Cumulative,
case when BOND.Callable='Y' then 'Yes' when BOND.Callable='N' then 'No' ELSE '' end as Callable,
case when BOND.Puttable='Y' then 'Yes' when BOND.Puttable='N' then 'No' ELSE '' end as Puttable,
case when BOND.SinkingFund='Y' then 'Yes' when BOND.SinkingFund='N' then 'No' ELSE '' end as Sinking_Fund,
case when PAYOUT.Lookup is null then bond.PayOutMode else PAYOUT.Lookup end as Payout_Mode,
BOND.DebtMarket as Debt_Grouping, 
case when BOND.OnTap='T' then 'Yes' ELSE 'No' end as On_Tap,
BOND.MaximumTapAmount as Max_Tap_Amount, 
BOND.TapExpiryDate as Tap_Expiry_Date, 
BOND.DomesticTaxRate as Domestic_Tax_Rate, 
BOND.NonResidentTaxRate as Non_Resident_Tax_Rate, 
BOND.TaxRules as Tax_Rules, 
case when bond.Bondsrc is null or bond.Bondsrc='' or bond.Bondsrc='PP' or bond.Bondsrc='SR' then '' else 'Yes' end as Backup_Document, 
'BOND|SecID|GoverningLaw' as Link_Governing_Law,
'BOND|SecID|Notes' as Link_Notes,
BOND.Actflag,
BOND.Acttime,
BOND.AnnounceDate,
BOND.SecId

	  into wfi.dbo.reference

FROM BOND
inner join scmst on BOND.SecID = SCMST.SecID
left outer join wfi.dbo.sys_lookup as HOLDING on SCMST.Holding = HOLDING.Code
                      and 'HOLDING' = HOLDING.TypeGroup
left outer join wfi.dbo.sys_lookup as STRUCTCD on SCMST.StructCD = STRUCTCD.Code
                      and 'STRUCTCD' = STRUCTCD.TypeGroup
left outer join wfi.dbo.sys_lookup as BONDTYPE on BOND.Bondtype = BONDTYPE.Code
                      and 'BONDTYPE' = BONDTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FREQ on BOND.InterestPaymentFrequency = FREQ.Code
                      and 'FREQ' = FREQ.TypeGroup
left outer join wfi.dbo.sys_lookup as INTTYPE on BOND.InterestBasis = INTTYPE.Code
                      and 'INTTYPE' = INTTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as INTACCRUAL on BOND.InterestAccrualConvention = INTACCRUAL.Code
                      and 'INTACCRUAL' = INTACCRUAL.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNTYPE on BOND.FRNType = FRNTYPE.Code
                      and 'FRNTYPE' = FRNTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNINDXBEN on BOND.FRNIndexBenchmark = FRNINDXBEN.Code
                      and 'FRNINDXBEN' = FRNINDXBEN.TypeGroup
left outer join wfi.dbo.sys_lookup as ASSETBKD on BOND.SecurityCharge = ASSETBKD.Code
                      and 'ASSETBKD' = ASSETBKD.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBDC on BOND.IntBusDayConv = MATBDC.Code
                      and 'MATBDC' = MATBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as INTBDC on BOND.IntBusDayConv = INTBDC.Code
                      and 'INTBDC' = INTBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as DEBTSTATUS on BOND.MaturityStructure = DEBTSTATUS.Code
                      and 'DEBTSTATUS' = DEBTSTATUS.TypeGroup
left outer join wfi.dbo.sys_lookup as STRIP on BOND.Strip = STRIP.Code
                      and 'STRIP' = STRIP.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBNHMRK on BOND.MaturityBenchmark = MATBNHMRK.Code
                      and 'MATBNHMRK' = MATBNHMRK.TypeGroup
left outer join wfi.dbo.sys_lookup as PAYOUT on BOND.PayoutMode = PAYOUT.Code
                      and 'PAYOUT' = PAYOUT.TypeGroup
left outer join WFI.dbo.sys_lookup as CURCUREN on BOND.CurenCD = CURCUREN.Code
                      and 'CUREN' = CURCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as ISSCUREN on BOND.IssueCurrency = ISSCUREN.Code
                      and 'CUREN' = ISSCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as INTCUREN on BOND.InterestCurrency = INTCUREN.Code
                      and 'CUREN' = INTCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as MATCUREN on BOND.MaturityCurrency = MATCUREN.Code
                      and 'CUREN' = MATCUREN.TypeGroup

--	WHERE
--	BOND.Acttime > (select max(wfi.dbo.reference.acttime) from wfi.dbo.reference)

	  WHERE
	  BOND.Actflag<>'D'

GO

	  print ""
	  go
	  print " CREATING reference keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE reference ALTER COLUMN  SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[reference] WITH NOCHECK ADD 
	   CONSTRAINT [pk_reference_SecID] PRIMARY KEY ([SecID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_reference_Maturity] ON [dbo].[reference]([Maturity_Date]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_reference_Acttime] ON [dbo].[reference]([Acttime] desc) ON [PRIMARY]
	  GO

use portfolio
if exists (select * from sysobjects where name = 'CPOPT_Count')
 drop table CPOPT_Count
use wca
select cpopt.secid, count(secid) as ct
into portfolio.dbo.CPOPT_Count
from cpopt
GROUP BY cpopt.secid
order by count(secid)
go

print ""
go

print " INDEXING portfolio.dbo.CPOPT_Count ,please  wait ....."
GO 
use portfolio
ALTER TABLE CPOPT_Count ALTER COLUMN  secid bigint NOT NULL
GO 
use portfolio
ALTER TABLE [DBO].[CPOPT_Count] WITH NOCHECK ADD 
 CONSTRAINT [pk_secid_CPOPT_Count] PRIMARY KEY ([secid])  ON [PRIMARY]
GO 

print ""
go
print " DELETING from schedule, please wait..."
go

	  use WFI
	  if exists (select * from sysobjects where name = 'schedule')
	    drop table schedule
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from schedule)
--	delete from schedule
--	where CpoptID in
--	(select CpoptID from wca.dbo.CPOPT
--	where acttime>@StartDate)
--	GO

print ""
go
print " UPDATING schedule, please wait..."
go
 
use wca

--	insert into wfi.dbo.schedule

SELECT 
Case when callput = 'C' then 'Call' when callput = 'P' then 'Put' else CallPut end as Call_or_Put,
case when CPOPT.cptype='KO' then 'KO'
     when portfolio.dbo.CPOPT_Count.ct>1 then 'BM'
     when portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is not null then 'US'
     when portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is null then 'EU'
     else CPOPT.CPType end as Type,
case when MandatoryOptional='M' then 'Mandatory'
     when MandatoryOptional='O' then 'Optional'
     else MandatoryOptional end as Status, 
CUREN.Lookup as schedule_Currency, 
cpopt.Price as schedule_Price,
cpopt.PriceAsPercent as Price_as_Percent,
case when CPOPT.cptype='KO' then 'Knock-out' 
     when (callput='C' and portfolio.dbo.CPOPT_Count.ct>1) 
     or (callput='C' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is null) then 'Callable Date' 
     when (callput='P' and portfolio.dbo.CPOPT_Count.ct>1) 
     or (callput='P' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is null) then 'Puttable Date' 
     when callput='C' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is not null  then 'Callable from' 
     when callput='P' and portfolio.dbo.CPOPT_Count.ct=1 and cpopt.todate is not null  then 'Puttable from' 
     else CPOPT.CPType end as Date_1_Note,
FromDate as Date_1,
ToDate as To_Date,
NoticeFrom as Notice_From,
NoticeTo as Notice_To,
MinNoticeDays as Min_Notice_Days,
MaxNoticeDays as Max_Notice_Days,
'WCA.dbo.CPOPT|CpoptID|Notes' as Link_Notes,
CPOPT.Actflag,
CPOPT.Acttime,
CPOPT.AnnounceDate,
CPOPT.CpoptID,
CPOPT.SecID

	  into wfi.dbo.schedule

from CPOPT
left outer join portfolio.dbo.CPOPT_Count on cpopt.secid = portfolio.dbo.CPOPT_Count.secid
left outer join WFI.dbo.sys_lookup as CUREN on CPOPT.Currency = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup

--	WHERE
--	CPOPT.Acttime > (select max(wfi.dbo.schedule.acttime) from wfi.dbo.schedule)

	  WHERE
	  cpopt.cptype<>''
	  and cpopt.cptype is not null

GO

	  print ""
	  go
	  print " CREATING schedule keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE schedule ALTER COLUMN  CpoptID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[schedule] WITH NOCHECK ADD 
	   CONSTRAINT [pk_schedule_CpoptID] PRIMARY KEY ([CpoptID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_schedule_SecID] ON [dbo].[schedule]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_schedule_Acttime] ON [dbo].[schedule]([Acttime] desc) ON [PRIMARY]
	  GO
	  use WFI
	  if exists (select * from sysobjects where name = 'static_change')
	      drop table static_change
	  GO
	  use wca
	  SELECT distinct
	  'Security Name' as static_change,
	  SCCHG.SecOldName as Previous_Value,
	  SCCHG.SecNewName as Current_Value,
	  SCCHG.DateofChange as Effective_Date,
	  case when SCCHG.Eventtype is null then '' when RELEVENT.Lookup is null then SCCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
	  SCCHG.Actflag,
	  SCCHG.Acttime,
	  SCCHG.AnnounceDate,
	  SCCHG.ScchgID as EventID,
	  SCCHG.SecID
	  into WFI.dbo.static_change
	  from SCCHG
	  inner join bond on scchg.secid = bond.secid
	  left outer join WFI.dbo.sys_lookup as RELEVENT on SCCHG.EventType = RELEVENT.Code
	                        and 'EVENT' = RELEVENT.TypeGroup
	  WHERE
	  SecOldName <> SecNewName
	  GO

	  print " CREATING Static_Data keys, please  wait ....."
	  GO 
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  SecID int NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  EventID int NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  static_change varchar(32) NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE static_change ALTER COLUMN  Reason varchar(32) NOT NULL
	  GO
	  use WFI 
	  ALTER TABLE [DBO].[static_change] WITH NOCHECK ADD 
	    CONSTRAINT [pk_static_change] PRIMARY KEY ( [SecID], [static_change],[EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_static_change_Acttime] ON [dbo].[static_change]([Acttime] desc) ON [PRIMARY] 
	  GO
	  use WFI 
	  CREATE  INDEX [ix_static_change_Effective] ON [dbo].[static_change]([Effective_Date]) ON [PRIMARY] 
	  GO

--	print ""
--	GO
--	print " DELETING incremental Static_Data, please wait..."
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Security Name')
--	delete from static_change
--	where
--	static_change = 'Security Name'
--	and EventID in
--	(select ScchgID from wca.dbo.SCCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'ISIN')
--	delete from static_change
--	where
--	(static_change = 'ISIN'
--	or static_change = 'UsCode'
--	or static_change = 'CommonCode')
--	and EventID in
--	(select IccID from wca.dbo.ICC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Sedol')
--	delete from static_change
--	where
--	static_change = 'Sedol'
--	and EventID in
--	(select SdchgID from wca.dbo.SDCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Incorporation')
--	delete from static_change
--	where
--	static_change = 'Incorporation'
--	and EventID in
--	(select InchgID from wca.dbo.INCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Company Name')
--	delete from static_change
--	where
--	static_change = 'Company Name'
--	and EventID in
--	(select IschgID from wca.dbo.ISCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Listing Status')
--	delete from static_change
--	where
--	static_change = 'Listing Status'
--	and EventID in
--	(select LstatID from wca.dbo.LSTAT
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Local Code')
--	delete from static_change
--	where
--	static_change = 'Local Code'
--	and EventID in
--	(select LccID from wca.dbo.LCC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = 'Interest Basis')
--	delete from static_change
--	where
--	static_change = 'Interest Basis'
--	and EventID in
--	(select IntbcID from wca.dbo.INTBC
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = '')
--	delete from static_change
--	where
--	(static_change = 'Current Currency'
--	or static_change = 'Interest Currency'
--	or static_change = 'Maturity Currency'
--	or static_change = 'Interest BDC'
--	or static_change = 'Maturity BDC'
--	or static_change = 'Interest Type'
--	or static_change = 'Debt Type')
--	and EventID in
--	(select BschgID from wca.dbo.BSCHG
--	where acttime>=@StartDate)
--	GO
--	use wfi
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from static_change where static_change = '')
--	delete from static_change
--	where
--	(static_change = 'FRN Type'
--	or static_change = 'RFN_Index_Benchmark'
--	or static_change = 'FRN_Adjustment_Freq'
--	or static_change = 'FRN_Margin'
--	or static_change = 'FRN_Min_IR'
--	or static_change = 'FRN_Max_IR'
--	or static_change = 'FRN_Rounding')
--	and EventID in
--	(select FrnfxID from wca.dbo.FRNFX
--	where acttime>=@StartDate)
--	GO
--	
--	use wca
--	insert into wfi.dbo.static_change
--	SELECT distinct
--	'Security Name' as static_change,
--	SCCHG.SecOldName as Previous_Value,
--	SCCHG.SecNewName as Current_Value,
--	SCCHG.DateofChange as Effective_Date,
--	case when RELEVENT.Lookup is null then SCCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
--	SCCHG.Actflag,
--	SCCHG.Acttime,
--	SCCHG.AnnounceDate,
--	SCCHG.ScchgID as EventID,
--	SCCHG.SecID
--	from SCCHG 
--	inner join bond on scchg.secid = bond.secid
--	left outer join WFI.dbo.sys_lookup as RELEVENT on SCCHG.EventType = RELEVENT.Code
--	                   and 'EVENT' = RELEVENT.TypeGroup
--	WHERE
--	inner join bond on scchg.secid = bond.secid
--	and SecOldName <> SecNewName
--	and SCCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
--	                              then '2000/01/01'
--	                              else max(wfi.dbo.static_change.Acttime)
--	                              end
--	                  from wfi.dbo.static_change 
--	                  where wfi.dbo.static_change.static_change = 'Security Name')
--	GO

print ""
GO
--	
print "static_change - Loading ISIN Change , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'ISIN' as static_change,
icc.OldISIN as Previous_Value,
icc.NewISIN as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on icc.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldIsin <> NewIsin
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'ISIN')
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'UsCode' as static_change,
icc.OldUsCode as Previous_Value,
icc.NewUsCode as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on ICC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldUsCode <> NewUsCode
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'UsCode')
GO

print "static_change - Loading CommonCode Change , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'CommonCode' as static_change,
icc.OldCommonCode as Previous_Value,
icc.NewCommonCode as Current_Value,
icc.Effectivedate as Effective_Date,
case when icc.Eventtype is null then '' when RELEVENT.Lookup is null then icc.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ICC.Actflag,
ICC.Acttime,
ICC.AnnounceDate,
ICC.IccID as EventID,
ICC.SecID
from ICC
inner join bond on ICC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ICC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldCommonCode <> NewCommonCode
and ICC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'CommonCode')

GO
 
print ""
GO
print "static_change - Loading Sedol Change , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Sedol' as static_change,
SDCHG.OldSedol as Previous_Value,
SDCHG.NewSedol as Current_Value,
SDCHG.Effectivedate as Effective_Date,
case when SDCHG.Eventtype is null then '' when RELEVENT.Lookup is null then SDCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
SDCHG.Actflag,
SDCHG.Acttime,
SDCHG.AnnounceDate,
SDCHG.SDCHGID as EventID,
SDCHG.SecID
from SDCHG
inner join bond on sdchg.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on SDCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldSedol <> NewSedol
and SDCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Sedol')

GO

print ""
GO
print "static_change - Loading Incorporation Change , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Incorporation' as static_change,
case when OLDCNTRY.Lookup is null then INCHG.OldCntryCD else OLDCNTRY.Lookup end as Previous_Value,
case when NEWCNTRY.Lookup is null then INCHG.NewCntryCD else NEWCNTRY.Lookup end as Current_Value,
INCHG.InchgDate as Effective_Date,
case when INCHG.Eventtype is null then '' when RELEVENT.Lookup is null then INCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
INCHG.Actflag,
INCHG.Acttime,
INCHG.AnnounceDate,
INCHG.INCHGID as EventID,
SCMST.SecID
from INCHG
INNER JOIN SCMST ON INCHG.ISSID = SCMST.ISSID
inner join bond on scmst.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on INCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCNTRY on INCHG.OldCntryCD = OLDCNTRY.Code
                      and 'CNTRY' = OLDCntry.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWCNTRY on INCHG.NewCntryCD = NEWCNTRY.Code
                      and 'CNTRY' = NEWCntry.TypeGroup
where
bond.Actflag<>'D'
and OldCntryCD <> NewCntryCD
and INCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Incorporation ')

GO


print "static_change - Loading Company Name Change , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Company Name' as static_change,
ISCHG.IssOldName as Previous_Value,
ISCHG.IssNewName as Current_Value,
ISCHG.NameChangeDate as Effective_Date,
case when ISCHG.Eventtype is null then '' when RELEVENT.Lookup is null then ISCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ISCHG.Actflag,
ISCHG.Acttime,
ISCHG.AnnounceDate,
ISCHG.ISCHGID as EventID,
SCMST.SecID
from ISCHG
INNER JOIN SCMST ON ISCHG.ISSID = SCMST.ISSID
inner join bond on scmst.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on ISCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and IssOldName <> IssNewName
and ISCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Company Name')

GO

print "static_change - Loading Local Code Change , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Local Code' as static_change,
LCC.OldLocalcode as Previous_Value,
LCC.Newlocalcode as Current_Value,
LCC.EffectiveDate as Effective_Date,
case when LCC.Eventtype is null then '' when RELEVENT.Lookup is null then LCC.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
LCC.Actflag,
LCC.Acttime,
LCC.AnnounceDate,
LCC.LCCID as EventID,
LCC.SecID
from LCC
inner join bond on lcc.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on LCC.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and OldLocalcode <> NewLocalcode
and LCC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Local Code')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest Type' as static_change,
case when OLDINTTYPE.Lookup is null then INTBC.OldIntBasis else OLDINTTYPE.Lookup end as Previous_Value,
case when NEWINTTYPE.Lookup is null then INTBC.NewIntBasis else NEWINTTYPE.Lookup end as Current_Value,
INTBC.EffectiveDate as Effective_Date,
'Correction' as Reason,
INTBC.Actflag,
INTBC.Acttime,
INTBC.AnnounceDate,
INTBC.INTBCID as EventID,
INTBC.SecID
from INTBC
inner join bond on INTBC.secid = bond.secid
left outer join WFI.dbo.sys_lookup as OLDINTTYPE on INTBC.OldIntBasis = OLDINTTYPE.Code
                      and 'INTTYPE' = OLDINTTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTTYPE on INTBC.NewIntBasis = NEWINTTYPE.Code
                      and 'INTTYPE' = NEWINTTYPE.TypeGroup
where
bond.Actflag<>'D'
and INTBC.OldIntBasis <> INTBC.NewIntBasis
and INTBC.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                                 from wfi.dbo.static_change 
				 where wfi.dbo.static_change.static_change = 'Interest Type')
  
GO


print "static_change - Bond Static Change , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest Currency' as static_change,
case when OLDCUREN.Lookup is null then BSCHG.OldInterestCurrency else OLDCUREN.Lookup end as Previous_Value,
case when NEWCUREN.Lookup is null then BSCHG.NewInterestCurrency else NEWCUREN.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCUREN on BSCHG.OldInterestCurrency = OLDCUREN.Code
                      and 'CUREN' = OLDCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWCUREN on BSCHG.NewInterestCurrency = NEWCUREN.Code
                      and 'CUREN' = NEWCUREN.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldInterestCurrency <> BSCHG.NewInterestCurrency
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Interest Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Maturity Currency' as static_change,
case when OLDCUREN.Lookup is null then BSCHG.OldMaturityCurrency else OLDCUREN.Lookup end as Previous_Value,
case when NEWCUREN.Lookup is null then BSCHG.NewMaturityCurrency else NEWCUREN.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCUREN on BSCHG.OldMaturityCurrency = OLDCUREN.Code
                      and 'CUREN' = OLDCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWCUREN on BSCHG.NewMaturityCurrency = NEWCUREN.Code
                      and 'CUREN' = NEWCUREN.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldMaturityCurrency <> BSCHG.NewMaturityCurrency
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Maturity Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Current Currency' as static_change,
case when OLDCUREN.Lookup is null then BSCHG.OldCurenCD else OLDCUREN.Lookup end as Previous_Value,
case when NEWCUREN.Lookup is null then BSCHG.NewCurenCD else NEWCUREN.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCUREN on BSCHG.OldCurenCD = OLDCUREN.Code
                      and 'CUREN' = OLDCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWCUREN on BSCHG.NewCurenCD = NEWCUREN.Code
                      and 'CUREN' = NEWCUREN.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldCurenCD <> BSCHG.NewCurenCD
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Current Currency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'Debt Type' as static_change,
case when OLDBONDTYPE.Lookup is null then BSCHG.OldBondType else OLDBONDTYPE.Lookup end as Previous_Value,
case when NEWBONDTYPE.Lookup is null then BSCHG.NewBondType else NEWBONDTYPE.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDBONDTYPE on BSCHG.OldBondType = OLDBONDTYPE.Code
                      and 'BONDTYPE' = OLDBONDTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWBONDTYPE on BSCHG.NewBondType = NEWBONDTYPE.Code
                      and 'BONDTYPE' = NEWBONDTYPE.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldBondType <> BSCHG.NewBondType
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Debt Type')

GO


use wca
insert into wfi.dbo.static_change
SELECT distinct
'Interest BDC' as static_change,
case when OLDINTBDC.Lookup is null then BSCHG.OldIntBusDayConv else OLDINTBDC.Lookup end as Previous_Value,
case when NEWINTBDC.Lookup is null then BSCHG.NewIntBusDayConv else NEWINTBDC.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDINTBDC on BSCHG.OldIntBusDayConv = OLDINTBDC.Code
                      and 'INTBDC' = OLDINTBDC.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTBDC on BSCHG.NewIntBusDayConv = NEWINTBDC.Code
                      and 'INTBDC' = NEWINTBDC.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldIntBusDayConv <> BSCHG.NewIntBusDayConv
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Interest BDC')

GO
 
use wca
insert into wfi.dbo.static_change
SELECT distinct
'Maturity BDC' as static_change,
case when OLDMATBDC.Lookup is null then BSCHG.OldMatBusDayConv else OLDMATBDC.Lookup end as Previous_Value,
case when NEWMATBDC.Lookup is null then BSCHG.NewMatBusDayConv else NEWMATBDC.Lookup end as Current_Value,
BSCHG.NotificationDate as Effective_Date,
case when BSCHG.Eventtype is null then '' when RELEVENT.Lookup is null then BSCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BSCHG.Actflag,
BSCHG.Acttime,
BSCHG.AnnounceDate,
BSCHG.BSCHGID as EventID,
BSCHG.SecID
from BSCHG
inner join bond on BSCHG.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on BSCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDMATBDC on BSCHG.OldMatBusDayConv = OLDMATBDC.Code
                      and 'INTBDC' = OLDMATBDC.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWMATBDC on BSCHG.NewMatBusDayConv = NEWMATBDC.Code
                      and 'INTBDC' = NEWMATBDC.TypeGroup
where
bond.Actflag<>'D'
and BSCHG.OldMatBusDayConv  <> BSCHG.NewMatBusDayConv 
and BSCHG.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'Maturity BDC')
GO


print "static_change - FRN Fixings , please wait..."
GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Rounding' as static_change,
FRNFX.OldRounding as Previous_Value,
FRNFX.NewRounding as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldRounding <> FRNFX.NewRounding
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Rounding')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Minimum IR' as static_change,
FRNFX.OldMinimumInterestRate as Previous_Value,
FRNFX.NewMinimumInterestRate as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMinimumInterestRate <> FRNFX.NewMinimumInterestRate
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Minimum IR')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Maximum IR' as static_change,
FRNFX.OldMaximumInterestRate as Previous_Value,
FRNFX.NewMaximumInterestRate as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMaximumInterestRate <> FRNFX.NewMaximumInterestRate
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Maximum IR')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Margin' as static_change,
FRNFX.OldMarkup as Previous_Value,
FRNFX.NewMarkup as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldMarkup <> FRNFX.NewMarkup
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Margin')

GO



use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN  IR Adjustment Frequency' as static_change,
case when OLDFREQ.Lookup is null then FRNFX.OldFRNIntAdjFreq else OLDFREQ.Lookup end as Previous_Value,
case when NEWFREQ.Lookup is null then FRNFX.NewFRNIntAdjFreq else NEWFREQ.Lookup end as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDFREQ on FRNFX.OldFRNIntAdjFreq = OLDFREQ.Code
                      and 'FREQ' = OLDFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWFREQ on FRNFX.NewFRNIntAdjFreq = NEWFREQ.Code
                      and 'FREQ' = NEWFREQ.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNIntAdjFreq <> FRNFX.NewFRNIntAdjFreq
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  IR Adjustment Frequency')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Index Benchmark' as static_change,
case when OLDINDXBEN.Lookup is null then FRNFX.OldFRNIndexBenchmark else OLDINDXBEN.Lookup end as Current_Value,
case when NEWINDXBEN.Lookup is null then FRNFX.NewFRNIndexBenchmark else NEWINDXBEN.Lookup end as Previous_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDINDXBEN on FRNFX.OldFRNIndexBenchmark = OLDINDXBEN.Code
                      and 'FRNINDXBEN' = OLDINDXBEN.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINDXBEN on FRNFX.NewFRNIndexBenchmark = NEWINDXBEN.Code
                      and 'FRNINDXBEN' = NEWINDXBEN.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNIndexBenchmark <> FRNFX.NewFRNIndexBenchmark
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN  Index Benchmark')

GO

use wca
insert into wfi.dbo.static_change
SELECT distinct
'FRN Type' as static_change,
case when OLDTYPE.Lookup is null then FRNFX.OldFRNType else OLDTYPE.Lookup end as Previous_Value,
case when NEWTYPE.Lookup is null then FRNFX.NewFRNType else NEWTYPE.Lookup end as Current_Value,
FRNFX.EffectiveDate as Effective_Date,
case when FRNFX.Eventtype is null then '' when RELEVENT.Lookup is null then FRNFX.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
FRNFX.Actflag,
FRNFX.Acttime,
FRNFX.AnnounceDate,
FRNFX.FRNFXID as EventID,
FRNFX.SecID
from FRNFX
inner join bond on FRNFX.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on FRNFX.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDTYPE on FRNFX.OldFRNType = OLDTYPE.Code
                      and 'FRNTYPE' = OLDTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWTYPE on FRNFX.NewFRNType = NEWTYPE.Code
                      and 'FRNTYPE' = NEWTYPE.TypeGroup
where
bond.Actflag<>'D'
and FRNFX.OldFRNType <> FRNFX.NewFRNType
and FRNFX.Acttime > (select case when max(wfi.dbo.static_change.Acttime) is null
                                 then '2000/01/01'
                                 else max(wfi.dbo.static_change.Acttime)
                                 end
                     from wfi.dbo.static_change 
                     where wfi.dbo.static_change.static_change = 'FRN Type')

GO
