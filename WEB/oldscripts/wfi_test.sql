print ""
go
print " DELETING from tender, please wait..."
go

--  use WFI
--  if exists (select * from sysobjects where name = 'tender')
--    drop table tender
--  GO

  	use WFI
  	Declare @StartDate datetime
  	set @StartDate = (select max(acttime) from tender)
  	delete from tender
  	where RedemID in
  	(select RedemID from wca.dbo.REDEM
  	where acttime>@StartDate)
  	GO

print ""
go
print " UPDATING tender, please wait..."
go
 
use wca

  	insert into wfi.dbo.tender

SELECT 
case when REDEM.RedemDate = '1800/01/01' then null else REDEM.RedemDate end as tender_Date, 
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
case when REDEMTYPE.Lookup is null then REDEM.Redemtype else REDEMTYPE.Lookup end as Type,
Poolfactor as Pool_Factor,
RedemPercent as Percentage_Redeemed,
case when CUREN.Lookup is null then REDEM.CurenCD else CUREN.Lookup end as tender_Currency,
REDEM.RedemPrice as tender_Price,
REDEM.Priceaspercent as Price_as_Percent,
REDEM.RedemPremium as tender_Premium,
REDEM.Premiumaspercent as Premium_as_Percent,
RD.Recdate as Record_Date,
'WCA.dbo.REDEM|RedemID|RedemNotes' as Link_Notes,
REDEM.Actflag,
REDEM.Acttime,
REDEM.AnnounceDate,
REDEM.RedemID,
REDEM.SecID

--  into wfi.dbo.tender

from REDEM
inner join bond on redem.secid = bond.secid
left outer join rd on redem.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDEM.Redemtype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup
left outer join WFI.dbo.sys_lookup as CUREN on REDEM.CurenCD = CUREN.Code
                      and 'CUREN' = CUREN.TypeGroup

  	WHERE
  	REDEM.Acttime > (select max(wfi.dbo.tender.acttime) from wfi.dbo.tender)
  	and BOND.BondType <> 'PRF'

--  WHERE
--  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and REDEM.Actflag<>'D'

GO

--  print ""
--  go
--  print " CREATING tender keys, please  wait ....."
--  GO 
--  use WFI 
--  ALTER TABLE tender ALTER COLUMN  RedemID int NOT NULL
--  GO 
--  use WFI 
--  ALTER TABLE [DBO].[tender] WITH NOCHECK ADD 
--   CONSTRAINT [pk_tender_RedemID] PRIMARY KEY ([RedemID])  ON [PRIMARY]
--  GO
--  use WFI 
--  CREATE  INDEX [ix_tender_SecID] ON [dbo].[tender]([SecID]) ON [PRIMARY] 
--  GO 
--  use WFI 
--  CREATE  INDEX [ix_tender_Acttime] ON [dbo].[tender]([Acttime] desc) ON [PRIMARY]
--  GO
