print ""
go
print " DELETING from reference, please wait..."
go

--  use WFI
--  if exists (select * from sysobjects where name = 'reference')
--    drop table reference
--  go

  	use WFI
  	Declare @StartDate datetime
  	set @StartDate = (select max(acttime) from reference)
  	delete from reference
  	where SecID in
  	(select SecID from wca.dbo.BOND
  	where acttime>@StartDate)
  	go

print ""
go
print " UPDATING reference, please wait..."
go
 
use wca
  	insert into wfi.dbo.reference
SELECT 
case when BondType.Lookup is null then bond.bondtype else BondType.Lookup end as Security_Type, 
case when STRUCTCD.Lookup is null then SCMST.StructCD else STRUCTCD.Lookup end as Debt_Structure,
SCMST.Regs144a as Regulation_Code,
case when CURCUREN.Lookup is null then BOND.CurenCD else CURCUREN.Lookup end as Current_Currency,
case when HOLDING.Lookup is null then SCMST.Holding else HOLDING.Lookup end as Holding,
BOND.ParValue as Nominal_Value, 
BOND.Denomination1 as Denom_1,
BOND.Denomination2 as Denom_2,
BOND.Denomination3 as Denom_3,
BOND.Denomination4 as Denom_4,
BOND.Denomination5 as Denom_5,
BOND.Denomination6 as Denom_6,
BOND.Denomination7 as Denom_7,
BOND.MinimumDenomination as Min_Denom, 
BOND.DenominationMultiple as Denom_Multiple, 
BOND.IssueDate as Issue_Date, 
case when ISSCUREN.Lookup is null then BOND.IssueCurrency else ISSCUREN.Lookup end as Issue_Currency, 
BOND.IssuePrice as Issue_Price, 
BOND.PriceAsPercent as Price_as_Percent,
BOND.IssueAmount as Issue_Amount_in_Millions, 
BOND.IssueAmountDate as Issue_Amount_Date, 
BOND.Series, 
BOND.Class, 
case when BOND.SeniorJunior='S' then 'Senior' when BOND.SeniorJunior='J' then 'Junior' when BOND.SeniorJunior='M' then 'Mezzanine' ELSE '' end as Financing_Structure,
BOND.OutstandingAmount as Outstanding_Amount, 
case when BOND.OutstandingAmountDate = '1800/01/01' then null else BOND.OutstandingAmountDate end as Outstanding_Amount_Date, 
case when INTTYPE.Lookup is null then bond.InterestBasis else INTTYPE.Lookup end as Interest_Basis, 
case when FRNTYPE.Lookup is null then bond.FRNType else FRNTYPE.Lookup end as FRN_Type,  
case when INTCUREN.Lookup is null then BOND.InterestCurrency else INTCUREN.Lookup end as Interest_Currency, 
case when FRNINDXBEN.Lookup is null then bond.FRNIndexBenchmark else FRNINDXBEN.Lookup end as FRN_Index_Benchmark,  
BOND.Markup as Margin,
BOND.Rounding, 
BOND.InterestRate as Interest_Rate, 
BOND.MinimumInterestRate as Min_Interest_Rate, 
BOND.MaximumInterestRate as Max_Interest_Rate, 
BOND.IntCommencementDate as Interest_Commencement_Date, 
BOND.FirstCouponDate as First_Coupon_Date, 
case when BOND.Guaranteed='T' then 'Yes' ELSE 'No' end as Guaranteed,
case when BOND.SecuredBy='Y' then 'Yes' when BOND.SecuredBy='N' then 'No' ELSE '' end as Secured_By,
case when BOND.WarrantAttached='Y' then 'Yes' when BOND.WarrantAttached='N' then 'No' ELSE '' end as Warrant_Attached,
case when BOND.Subordinate='Y' then 'Yes' when BOND.Subordinate='N' then 'No' ELSE '' end as Subordinate,
case when ASSETBKD.Lookup is null then bond.SecurityCharge else ASSETBKD.Lookup end as Security_Charge, 
case when INTACCRUAL.Lookup is null then bond.InterestAccrualConvention else INTACCRUAL.Lookup end as Interest_Accrual_Convention,
case when INTBDC.Lookup is null then bond.IntBusDayConv else INTBDC.Lookup end as Interest_Biz_Day_Convention,  
ConventionMethod as Convention_Method,  
case when STRIP.Lookup is null then bond.Strip else STRIP.Lookup end as Strip, 
BOND.StripInterestNumber as Strip_Interest_Number,
case when FREQ.Lookup is null then bond.InterestPaymentFrequency else FREQ.Lookup end as Interest_Payment_Freq,  
case when BOND.VarIntPayDate='Y' then 'Yes' when BOND.VarIntPayDate='N' then 'No' ELSE '' end as Variable_Interest_Paydate,
BOND.FrnIntAdjFreq as FRN_Interest_Adjustment_Freq, 
case when (len(InterestPayDate1) <> 4 or InterestPayDate1 is NULL or InterestPayDate1 = '' ) then ''
when ((cast(substring(InterestPayDate1,3,2) as int) < 1) or (cast(substring(InterestPayDate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate1,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate1,1,2)
end as Interest_PayDate1,
case when (len(InterestPayDate2) <> 4 or InterestPayDate2 is NULL or InterestPayDate2 = '' ) then ''
when ((cast(substring(InterestPayDate2,3,2) as int) < 1) or (cast(substring(InterestPayDate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate2,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate2,1,2)
end as Interest_PayDate2,
case when (len(InterestPayDate3) <> 4 or InterestPayDate3 is NULL or InterestPayDate3 = '' ) then ''
when ((cast(substring(InterestPayDate3,3,2) as int) < 1) or (cast(substring(InterestPayDate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate3,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate3,1,2)
end as Interest_PayDate3,
case when (len(InterestPayDate4) <> 4 or InterestPayDate4 is NULL or InterestPayDate4 = '' ) then ''
when ((cast(substring(InterestPayDate4,3,2) as int) < 1) or (cast(substring(InterestPayDate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(InterestPayDate4,3,2) + '/01') as datetime),107),1,4) + substring(InterestPayDate4,1,2)
end as Interest_PayDate4,
case when BOND.Perpetual='I' then 'Irredeemable' when BOND.Perpetual='P' then 'Perpetual' when BOND.Perpetual='U' then 'Undated' ELSE '' end as Perpetual,
case when DEBTSTATUS.Lookup is null then bond.MaturityStructure else DEBTSTATUS.Lookup end as Maturity_Structure,  
BOND.MaturityDate as Maturity_Date, 
case when BOND.MaturityExtendible='Y' then 'Yes' when BOND.MaturityExtendible='N' then 'No' ELSE '' end as Maturity_Extendible,
case when MATCUREN.Lookup is null then BOND.MaturityCurrency else MATCUREN.Lookup end as Maturity_Currency, 
BOND.MatPrice as Maturity_Price,
BOND.MatPriceAsPercent as Maturity_Price_as_Percent,
case when MATBNHMRK.Lookup is null then bond.MaturityBenchmark else MATBNHMRK.Lookup end as Maturity_Benchmark,  
case when MATBDC.Lookup is null then bond.MatBusDayConv else MATBDC.Lookup end as Maturity_Biz_Day_Convention,  
case when BOND.Cumulative='Y' then 'Yes' when BOND.Cumulative='N' then 'No' ELSE '' end as Cumulative,
case when BOND.Callable='Y' then 'Yes' when BOND.Callable='N' then 'No' ELSE '' end as Callable,
case when BOND.Puttable='Y' then 'Yes' when BOND.Puttable='N' then 'No' ELSE '' end as Puttable,
case when BOND.SinkingFund='Y' then 'Yes' when BOND.SinkingFund='N' then 'No' ELSE '' end as Sinking_Fund,
case when PAYOUT.Lookup is null then bond.PayOutMode else PAYOUT.Lookup end as Payout_Mode,
BOND.DebtMarket as Debt_Grouping, 
case when BOND.OnTap='T' then 'Yes' ELSE 'No' end as On_Tap,
BOND.MaximumTapAmount as Max_Tap_Amount, 
BOND.TapExpiryDate as Tap_Expiry_Date, 
case when issuedate>'2001/02/28' and issueamountdate>'2002/03/01' 
     and InterestBasis<>'ZC' and cntryofincorp in (select cntryofincorp from EUSD) then 'Yes'
     else 'No' end as EUSD_Applicable,
BOND.DomesticTaxRate as Domestic_Tax_Rate, 
BOND.NonResidentTaxRate as Non_Resident_Tax_Rate, 
BOND.TaxRules as Tax_Rules, 
case when bond.Bondsrc is null or bond.Bondsrc='' or bond.Bondsrc='PP' or bond.Bondsrc='SR' then '' else 'Yes' end as Backup_Document, 
'BOND|SecID|GoverningLaw' as Link_Governing_Law,
'BOND|SecID|Notes' as Link_Notes,
BOND.Actflag,
BOND.Acttime,
BOND.AnnounceDate,
BOND.SecId

--  into wfi.dbo.reference

FROM BOND
inner join scmst on BOND.SecID = SCMST.SecID
inner join issur on scmst.issid = issur.issid
left outer join wfi.dbo.sys_lookup as HOLDING on SCMST.Holding = HOLDING.Code
                      and 'HOLDING' = HOLDING.TypeGroup
left outer join wfi.dbo.sys_lookup as STRUCTCD on SCMST.StructCD = STRUCTCD.Code
                      and 'STRUCTCD' = STRUCTCD.TypeGroup
left outer join wfi.dbo.sys_lookup as BONDTYPE on BOND.Bondtype = BONDTYPE.Code
                      and 'BONDTYPE' = BONDTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FREQ on BOND.InterestPaymentFrequency = FREQ.Code
                      and 'FREQ' = FREQ.TypeGroup
left outer join wfi.dbo.sys_lookup as INTTYPE on BOND.InterestBasis = INTTYPE.Code
                      and 'INTTYPE' = INTTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as INTACCRUAL on BOND.InterestAccrualConvention = INTACCRUAL.Code
                      and 'INTACCRUAL' = INTACCRUAL.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNTYPE on BOND.FRNType = FRNTYPE.Code
                      and 'FRNTYPE' = FRNTYPE.TypeGroup
left outer join wfi.dbo.sys_lookup as FRNINDXBEN on BOND.FRNIndexBenchmark = FRNINDXBEN.Code
                      and 'FRNINDXBEN' = FRNINDXBEN.TypeGroup
left outer join wfi.dbo.sys_lookup as ASSETBKD on BOND.SecurityCharge = ASSETBKD.Code
                      and 'ASSETBKD' = ASSETBKD.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBDC on BOND.IntBusDayConv = MATBDC.Code
                      and 'MATBDC' = MATBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as INTBDC on BOND.IntBusDayConv = INTBDC.Code
                      and 'INTBDC' = INTBDC.TypeGroup
left outer join wfi.dbo.sys_lookup as DEBTSTATUS on BOND.MaturityStructure = DEBTSTATUS.Code
                      and 'DEBTSTATUS' = DEBTSTATUS.TypeGroup
left outer join wfi.dbo.sys_lookup as STRIP on BOND.Strip = STRIP.Code
                      and 'STRIP' = STRIP.TypeGroup
left outer join wfi.dbo.sys_lookup as MATBNHMRK on BOND.MaturityBenchmark = MATBNHMRK.Code
                      and 'MATBNHMRK' = MATBNHMRK.TypeGroup
left outer join wfi.dbo.sys_lookup as PAYOUT on BOND.PayoutMode = PAYOUT.Code
                      and 'PAYOUT' = PAYOUT.TypeGroup
left outer join WFI.dbo.sys_lookup as CURCUREN on BOND.CurenCD = CURCUREN.Code
                      and 'CUREN' = CURCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as ISSCUREN on BOND.IssueCurrency = ISSCUREN.Code
                      and 'CUREN' = ISSCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as INTCUREN on BOND.InterestCurrency = INTCUREN.Code
                      and 'CUREN' = INTCUREN.TypeGroup
left outer join WFI.dbo.sys_lookup as MATCUREN on BOND.MaturityCurrency = MATCUREN.Code
                      and 'CUREN' = MATCUREN.TypeGroup

  	WHERE
  	BOND.Acttime > (select max(wfi.dbo.reference.acttime) from wfi.dbo.reference)

--  WHERE
--  BOND.Actflag<>'D'

GO

--  print ""
--  go
--  print " CREATING reference keys, please  wait ....."
--  GO 
--  use WFI 
--  ALTER TABLE reference ALTER COLUMN  SecID int NOT NULL
--  GO 
--  use WFI 
--  ALTER TABLE [DBO].[reference] WITH NOCHECK ADD 
--   CONSTRAINT [pk_reference_SecID] PRIMARY KEY ([SecID])  ON [PRIMARY]
--  GO
--  use WFI 
--  CREATE  INDEX [ix_reference_Maturity] ON [dbo].[reference]([Maturity_Date]) ON [PRIMARY] 
--  GO 
--  use WFI 
--  CREATE  INDEX [ix_reference_Acttime] ON [dbo].[reference]([Acttime] desc) ON [PRIMARY]
--  GO
