	  use WFI
	  if exists (select * from sysobjects where name = 'takeover')
	    drop table takeover
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from takeover)
--	delete from takeover
--	where EVENTID in
--	(select TkovrID from wca.dbo.TKOVR
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.takeover

SELECT 
RD.recdate as Record_Date,
TKOVR.OfferorName as Offeror_Name,
case when TKOVR.Hostile='T' then 'Yes'
     when TKOVR.Hostile='F' then 'No'
     else TKOVR.Hostile end as Hostile, 
TKOVR.OpenDate as Open_Date,
TKOVR.CloseDate as Close_Date,
case when TKOVRSTAT.Lookup is null then TKOVR.TkovrStatus else TKOVRSTAT.Lookup end as Takeover_Status,
TKOVR.PreOfferQty as Pre_Offer_Quantity,
TKOVR.PreOfferPercent as Pre_Offer_Percent,
TKOVR.TargetQuantity as Target_Quantity,
TKOVR.TargetPercent as Target_Percent,
TKOVR.UnconditionalDate as Unconditional_Date,
TKOVR.CmAcqDate as Compulsory_Acquisition_Date,
TKOVR.MinAcpQty as Min_Acceptance_Quantity,
TKOVR.MaxAcpQty as Max_Acceptance_Quantity,
case when TKOVR.MiniTkovr='T' then 'Yes'
     when TKOVR.MiniTkovr='F' then 'No'
     else TKOVR.MiniTkovr end as Mini_Takeover, 
'WCA.dbo.TKOVR|TkovrID|TkovrNotes' as Link_Notes,
TKOVR.Actflag,
TKOVR.Acttime,
TKOVR.AnnounceDate,
TKOVR.TkovrID as EventID,
TKOVR.SecID 

	  into wfi.dbo.takeover

from TKOVR   
left outer join RD ON TKOVR.RdID = RD.RdID
inner join BOND on TKOVR.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as TKOVRSTAT on TKOVR.TkovrStatus = TKOVRSTAT.Code
                      and 'TKOVRSTAT' = TKOVRSTAT.TypeGroup

--	WHERE
--	TKOVR.Acttime > (select max(wfi.dbo.takeover.acttime) from wfi.dbo.takeover)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and TKOVR.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE takeover ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[takeover] WITH NOCHECK ADD 
	   CONSTRAINT [pk_takeover_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_takeover_Acttime] ON [dbo].[takeover]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'cur_redenomination')
	    drop table cur_redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from cur_redenomination)
--	delete from cur_redenomination
--	where EVENTID in
--	(select CurrdID from wca.dbo.CURRD
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.cur_redenomination

SELECT 
CURRD.EffectiveDate as Effective_Date,
CURRD.OldCurenCD as Old_Redemption_Currency,
CURRD.NewCurenCD as New_Redemption_Currency,
CURRD.OldParValue as Old_Par_Value,
CURRD.NewParValue as New_Par_Value,
case when RELEVENT.Lookup is null then CURRD.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.CURRD|CurrdID|CurRdNotes' as Link_Notes,
CURRD.Actflag,
CURRD.Acttime,
CURRD.AnnounceDate,
CURRD.CurrdID as EventID,
CURRD.SecID 

	  into wfi.dbo.cur_redenomination

from CURRD   
inner join BOND on CURRD.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on CURRD.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
                      
--	WHERE
--	CURRD.Acttime > (select max(wfi.dbo.cur_redenomination.acttime) from wfi.dbo.cur_redenomination)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CURRD.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE cur_redenomination ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[cur_redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_cur_reDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_cur_reDenom_Acttime] ON [dbo].[cur_redenomination]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'buy_back')
	    drop table buy_back
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from buy_back)
--	delete from buy_back
--	where EVENTID in
--	(select BBID from wca.dbo.BB
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.buy_back

SELECT 
RD.recdate as Record_Date,
case when ONOFFFLAG.Lookup is null then BB.OnOffFlag else ONOFFFLAG.Lookup end as On_Off_Market,
BB.StartDate as Start_Date,
BB.EndDate as End_Date,
BB.MinAcpQty as Min_Acceptance_Quantity,
BB.MaxAcpQty as Max_Acceptance_Quantity,
BB.BBMinPct as Min_Percent,
BB.BBMaxPct as Max_Percent,
'WCA.dbo.BB|BBID|BBNotes' as Link_Notes,
BB.Actflag,
BB.Acttime,
BB.AnnounceDate,
BB.BBID as EventID,
BB.SecID  

	  into wfi.dbo.buy_back

from BB   
left outer join RD ON BB.RdID = RD.RdID
inner join BOND on BB.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as ONOFFFLAG on BB.OnOffFlag = ONOFFFLAG.Code
                      and 'ONOFFFLAG' = ONOFFFLAG.TypeGroup
                                       
                      
--	WHERE
--	BB.Acttime > (select max(wfi.dbo.buy_back.acttime) from wfi.dbo.buy_back)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and BB.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE buy_back ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[buy_back] WITH NOCHECK ADD 
	   CONSTRAINT [pk_buy_back_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_buy_back_Acttime] ON [dbo].[buy_back]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'pv_redenomination')
	    drop table pv_redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from pv_redenomination)
--	delete from pv_redenomination
--	where EVENTID in
--	(select PvRdID from wca.dbo.PVRD
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.pv_redenomination

SELECT 
PVRD.EffectiveDate as Effective_Date,
PVRD.CurenCD as Redemption_Currency,
PVRD.OldParValue as Old_Par_Value,
PVRD.NewParValue as New_Par_Value,
case when RELEVENT.Lookup is null then PVRD.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.PVRD|PvRdID|PvRdNotes' as Link_Notes,
PVRD.Actflag,
PVRD.Acttime,
PVRD.AnnounceDate,
PVRD.PvRdID as EventID,
PVRD.SecID  

	  into wfi.dbo.pv_redenomination

from PVRD   
inner join BOND on PVRD.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on PVRD.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                      
                                       
                      
--	WHERE
--	PVRD.Acttime > (select max(wfi.dbo.pv_redenomination.acttime) from wfi.dbo.pv_redenomination)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and PVRD.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE pv_redenomination ALTER COLUMN EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[pv_redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_pv_reDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_pv_reDenom_Acttime] ON [dbo].[pv_redenomination]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'announcement')
	    drop table announcement
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from announcement)
--	delete from announcement
--	where EVENTID in
--	(select AnnID from wca.dbo.ANN
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.announcement

SELECT 
case when RELEVENT.Lookup is null then ANN.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
ANN.NotificationDate as Notification_Date,
'WCA.dbo.ANN|AnnID|AnnNotes' as Link_Notes,
ANN.Actflag,
ANN.Acttime,
ANN.AnnounceDate,
ANN.AnnID as EventID,
SCMST.SecID  

	  into wfi.dbo.announcement

from ANN   
inner join SCMST on ANN.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as RELEVENT on ANN.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup  
                
                                                             
--	WHERE
--	ANN.Acttime > (select max(wfi.dbo.announcement.acttime) from wfi.dbo.announcement)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and ANN.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE announcement ALTER COLUMN EVENTID int NOT NULL
	  ALTER TABLE announcement ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[announcement] WITH NOCHECK ADD 
	   CONSTRAINT [pk_announcement] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_announcement_Acttime] ON [dbo].[announcement]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'liquidation')
	    drop table liquidation
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from liquidation)
--	delete from liquidation
--	where EVENTID in
--	(select LiqID from wca.dbo.LIQ
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.liquidation

SELECT 
LIQ.Liquidator as Liquidator,
LIQ.LiqAdd1 as Address1,
LIQ.LiqAdd2 as Address2,
LIQ.LiqAdd3 as Address3,
LIQ.LiqAdd4 as Address4,
LIQ.LiqAdd5 as Address5,
LIQ.LiqAdd6 as Address6,
LIQ.LiqCity as City,
case when CNTRY.Lookup is null then LIQ.LiqCntryCD else CNTRY.Lookup end as Cntry,
LIQ.LiqTel as Telephone,
LIQ.LiqFax as Fax,
LIQ.LiqEmail as Email,
LIQ.RdDate as Record_Date,
'WCA.dbo.LIQ|LiqID|LiquidationTerms' as Link_Notes,
LIQ.Actflag,
LIQ.Acttime,
LIQ.AnnounceDate,
LIQ.LiqID as EventID,
SCMST.SecID  

	  into wfi.dbo.liquidation

from LIQ   
inner join SCMST on LIQ.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as CNTRY on LIQ.LiqCntryCD = CNTRY.Code
                      and 'CNTRY' = CNTRY.TypeGroup     
                
                                                             
--	WHERE
--	LIQ.Acttime > (select max(wfi.dbo.liquidation.acttime) from wfi.dbo.liquidation)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and LIQ.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE liquidation ALTER COLUMN EVENTID int NOT NULL
	  ALTER TABLE liquidation ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[liquidation] WITH NOCHECK ADD 
	   CONSTRAINT [pk_liquidation_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_liquidation_Acttime] ON [dbo].[liquidation]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'bankruptcy')
	    drop table bankruptcy
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from bankruptcy)
--	delete from bankruptcy
--	where EVENTID in
--	(select BkrpID from wca.dbo.BKRP
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.bankruptcy

SELECT 
BKRP.NotificationDate as Notification_Date,
BKRP.FilingDate as Filing_Date,
'WCA.dbo.BKRP|BkrpID|BkrpNotes' as Link_Notes,
BKRP.Actflag,
BKRP.Acttime,
BKRP.AnnounceDate,
BKRP.BkrpID as EventID,
SCMST.SecID  

	  into wfi.dbo.bankruptcy

from BKRP   
inner join SCMST on BKRP.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID

                                                             
--	WHERE
--	BKRP.Acttime > (select max(wfi.dbo.bankruptcy.acttime) from wfi.dbo.bankruptcy)

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and BKRP.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE bankruptcy ALTER COLUMN  EventID int NOT NULL
	  ALTER TABLE bankruptcy ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[bankruptcy] WITH NOCHECK ADD 
	   CONSTRAINT [pk_bankruptcy_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_bankruptcy_Acttime] ON [dbo].[bankruptcy]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'legal_action')
	    drop table legal_action
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from legal_action)
--	delete from legal_action
--	where EVENTID in
--	(select LawstID from wca.dbo.LAWST
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Legal_Action

SELECT 
LAWST.EffectiveDate as Effective_Date,
CASE WHEN LAWST.LAType= 'CLSACT' THEN 'Class Action' ELSE 'Other' END as Legal_Action_Type,
LAWST.Regdate as Registration_Date,
'WCA.dbo.LAWST|LawstID|LawstNotes' as Link_Notes,
LAWST.Actflag,
LAWST.Acttime,
LAWST.AnnounceDate,
LAWST.LawstID as EventID,
SCMST.SecID  

	  into wfi.dbo.legal_action

from LAWST   
inner join SCMST on LAWST.IssID = SCMST.IssID
inner join BOND on SCMST.SecID = BOND.SecID

--	WHERE
--	LAWST.Acttime > (select max(wfi.dbo.legal_action.acttime) from wfi.dbo.legal_action)
--	AND BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and LAWST.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE legal_action ALTER COLUMN  EventID int NOT NULL
	  ALTER TABLE legal_action ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[legal_action] WITH NOCHECK ADD 
	   CONSTRAINT [pk_legal_action_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_legal_action_Acttime] ON [dbo].[legal_action]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'bondholder_meeting')
	    drop table bondholder_meeting
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from bondholder_meeting)
--	delete from bondholder_meeting
--	where AGMID in
--	(select AGMID from wca.dbo.AGM
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.bondholder_meeting

SELECT 
AGM.AGMDate as AGM_Date,
case when AGMEGM.Lookup is null then AGM.AGMEGM else AGMEGM.Lookup end as Meeting_Type,
--case when AGM.BondSecID  
AGM.AGMNo as AGM_Number,
AGM.FYEDate as Fin_Year_End_Date,
AGM.AGMTime as AGM_Time,
AGM.ADD1 as Address1,
AGM.ADD2 as Address2,
AGM.ADD3 as Address3,
AGM.ADD4 as Address4,
AGM.ADD5 as Address5,
AGM.ADD6 as Address6,
AGM.City as City,
case when CNTRY.Lookup is null then AGM.CntryCD else CNTRY.Lookup end as Cntry,
AGM.Actflag,
AGM.Acttime,
AGM.AnnounceDate,
AGM.AGMID as EventID,
AGM.BondSecID as SecID  

	  into wfi.dbo.bondholder_meeting

from AGM  
inner join BOND on AGM.BondSecID = BOND.SecID
left outer join WFI.dbo.sys_lookup as AGMEGM on AGM.AGMEGM = AGMEGM.Code
                      and 'AGMEGM' = AGMEGM.TypeGroup  
left outer join WFI.dbo.sys_lookup as CNTRY on AGM.CntryCD = CNTRY.Code
                      and 'CNTRY' = CNTRY.TypeGroup                      
                                       
                      
--	WHERE
--	AGM.Acttime > (select max(wfi.dbo.bondholder_meeting.acttime) from wfi.dbo.bondholder_meeting)
--	AND BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and BondSecID is not null and BondSecID>0

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and BondSecID is not null and BondSecID>0
	  and AGM.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE bondholder_meeting ALTER COLUMN EventID int NOT NULL
	  ALTER TABLE bondholder_meeting ALTER COLUMN SecID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[bondholder_meeting] WITH NOCHECK ADD 
	   CONSTRAINT [pk_bondholder_meeting_EventID] PRIMARY KEY ([SecID],[EventID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_bondholder_meeting_Acttime] ON [dbo].[bondholder_meeting]([Acttime] desc) ON [PRIMARY]
	  GO

