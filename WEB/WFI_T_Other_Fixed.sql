	  use WFI
	  if exists (select * from sysobjects where name = 'outstanding_amount')
	    drop table outstanding_amount
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from outstanding_amount)
--	delete from outstanding_amount
--	where EVENTID in
--	(select BOCHGID from wca.dbo.BOCHG
--	where acttime>@StartDate)


use wca

--	insert into wfi.dbo.outstanding_amount

SELECT 
case when RELEVENT.Lookup is null then BOCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
BOCHG.EffectiveDate as Effective_Date,
BOCHG.OldOutValue as Old_OS_Value_in_Millions,
BOCHG.NewOutValue as New_OS_Value_in_Millions,
BOCHG.OldOutDate as Old_Outstanding_Date,
BOCHG.NewOutDate as New_Outstanding_Date,
'WCA.dbo.BOCHG|BOCHGID|BOCHGNotes' as Link_Notes,
BOCHG.Actflag,
BOCHG.Acttime,
BOCHG.AnnounceDate,
BOCHG.BOCHGID as EventID,
BOCHG.SecID

	  into wfi.dbo.outstanding_amount

from BOCHG
left outer join WFI.dbo.sys_lookup as RELEVENT on BOCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup

--	WHERE
--	BOCHG.Acttime > (select max(wfi.dbo.outstanding_amount.acttime) from wfi.dbo.outstanding_amount)

	  WHERE
	  BOCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE outstanding_amount ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[outstanding_amount] WITH NOCHECK ADD 
	   CONSTRAINT [pk_outstanding_amount_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_outstanding_amount_SecID] ON [dbo].[outstanding_amount]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_outstanding_amount_Acttime] ON [dbo].[outstanding_amount]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'maturity_change')
	    drop table maturity_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from maturity_change)
--	delete from maturity_change
--	where EVENTID in
--	(select MtchgID from wca.dbo.MTCHG
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.maturity_change

SELECT 
case when RELEVENT.Lookup is null then MTCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
MTCHG.NotificationDate as Notification_Date,
MTCHG.OldMaturityDate as Old_Maturity_Date,
MTCHG.NewMaturityDate as New_Maturity_Date,
case when OLDMATBNHMRK.Lookup is null then MTCHG.OldMaturityBenchmark else OLDMATBNHMRK.Lookup end as Old_Maturity_Benchmark,
case when NEWMATBNHMRK.Lookup is null then MTCHG.NewMaturityBenchmark else NEWMATBNHMRK.Lookup end as New_Maturity_Benchmark,
'WCA.dbo.MTCHG|MTCHGID|Notes' as Link_Notes,
MTCHG.Actflag,
MTCHG.Acttime,
MTCHG.AnnounceDate,
MTCHG.MTCHGID as EventID,
MTCHG.SecID

	  into wfi.dbo.maturity_change

from MTCHG
inner join bond on mtchg.secid = bond.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on MTCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDMATBNHMRK on MTCHG.OldMaturityBenchmark = OLDMATBNHMRK.Code
                      and 'MATBNHMRK' = OLDMATBNHMRK.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWMATBNHMRK on MTCHG.NewMaturityBenchmark = NEWMATBNHMRK.Code
                      and 'MATBNHMRK' = NEWMATBNHMRK.TypeGroup

--	WHERE
--	MTCHG.Acttime > (select max(wfi.dbo.maturity_change.acttime) from wfi.dbo.maturity_change)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and MTCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE maturity_change ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[maturity_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_maturity_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_maturity_change_SecID] ON [dbo].[maturity_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_maturity_change_Acttime] ON [dbo].[maturity_change]([Acttime] desc) ON [PRIMARY]
	  GO



use wca

--	insert into wfi.dbo.conversion_terms

SELECT 
CONVT.FromDate as From_Date,
CONVT.ToDate as To_Date,
CONVT.RatioNew as New_Ratio,
CONVT.RatioOld as Old_Ratio,
CONVT.CurenCD as Conversion_Currency,
CONVT.Price as Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONVT.ResSecID,
RESSCMST.ISIN as Resultant_ISIN,
case when SECTYCD.Lookup is null then CONVT.SectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONVT.Fractions else FRACTIONS.Lookup end as Fractions,
CONVT.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
CONVT.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONVT|ConvtID|ConvtNotes' as Link_Notes,
CONVT.Actflag,
CONVT.Acttime,
CONVT.AnnounceDate,
CONVT.ConvtID as EventID,
CONVT.SecID

	  into wfi.dbo.conversion_terms

from CONVT
inner join bond on convt.secid = bond.secid
left outer join scmst as resscmst on convt.ressecid = resscmst.secid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONVT.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONVT.SectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONVT.Acttime > (select max(wfi.dbo.conversion_terms.acttime) from wfi.dbo.conversion_terms)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONVT.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE conversion_terms ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion_terms] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_terms_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_SecID] ON [dbo].[conversion_terms]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_Acttime] ON [dbo].[conversion_terms]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'conversion')
	    drop table conversion
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion)
--	delete from conversion
--	where EVENTID in
--	(select ConvID from wca.dbo.CONV
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.conversion

SELECT 
CONV.FromDate as From_Date,
CONV.ToDate as To_Date,
CONV.RatioNew as New_Ratio,
CONV.RatioOld as Old_Ratio,
CONV.CurenCD as Conversion_Currency,
CONV.Price as Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONV.ResSecID AS Resultant_SecID,
RESSCMST.ISIN as Resultant_ISIN,
case when SECTYCD.Lookup is null then CONV.ResSectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONV.Fractions else FRACTIONS.Lookup end as Fractions,
CONV.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
CONV.ConvType as Conversion_Type,
RD.Recdate as Record_Date,
CONV.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONV|ConvID|CONVNotes' as Link_Notes,
CONV.Actflag,
CONV.Acttime,
CONV.AnnounceDate,
CONV.ConvID as EventID,
CONV.SecID

	  into wfi.dbo.conversion

from CONV
inner join bond on conv.secid = bond.secid
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join rd on CONV.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONV.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONV.ResSectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONV.Acttime > (select max(wfi.dbo.conversion.acttime) from wfi.dbo.conversion)
--	and BOND.BondType <> 'PRF'
--	and Convtype<>'TENDER'


	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONV.Actflag<>'D'
	  and Convtype<>'TENDER'

GO


	  use WFI 
	  ALTER TABLE conversion ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_SecID] ON [dbo].[conversion]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_Acttime] ON [dbo].[conversion]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'reconvention')
	    drop table Reconvention
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Reconvention)
--	delete from Reconvention
--	where EVENTID in
--	(select RconvID from wca.dbo.RCONV
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Reconvention

SELECT 
RCONV.EffectiveDate as Effective_Date,
case when OLDINTACCRUAL.Lookup is null then RCONV.OldInterestAccrualConvention else OLDINTACCRUAL.Lookup end as Old_Accrual_Convention,
case when NEWINTACCRUAL.Lookup is null then RCONV.NewInterestAccrualConvention else NEWINTACCRUAL.Lookup end as New_Accrual_Convention,
case when OLDCMETHOD.Lookup is null then RCONV.OldConvMethod else OLDCMETHOD.Lookup end as Old_Convention_Method,
case when NEWCMETHOD.Lookup is null then RCONV.NewConvMethod else NEWCMETHOD.Lookup end as New_Convention_Method,
case when RELEVENT.Lookup is null then RCONV.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.RCONV|RconvID|Notes' as Link_Notes,
RCONV.Actflag,
RCONV.Acttime,
RCONV.AnnounceDate,
RCONV.RconvID as EventID,
RCONV.SecID

	  into wfi.dbo.Reconvention

from RCONV
left outer join WFI.dbo.sys_lookup as OLDINTACCRUAL on RCONV.OldInterestAccrualConvention = OLDINTACCRUAL.Code
                      and 'INTACCRUAL' = OLDINTACCRUAL.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWINTACCRUAL on RCONV.NewInterestAccrualConvention = NEWINTACCRUAL.Code
                      and 'INTACCRUAL' = NEWINTACCRUAL.TypeGroup
left outer join WFI.dbo.sys_lookup as OLDCMETHOD on RCONV.OldConvMethod = OLDCMETHOD.Code
                      and 'CMETHOD' = OLDCMETHOD.TypeGroup                        
left outer join WFI.dbo.sys_lookup as NEWCMETHOD on RCONV.NewConvMethod = NEWCMETHOD.Code
                      and 'CMETHOD' = NEWCMETHOD.TypeGroup                      
left outer join WFI.dbo.sys_lookup as RELEVENT on RCONV.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                                                          

--	WHERE
--	RCONV.Acttime > (select max(wfi.dbo.Reconvention.acttime) from wfi.dbo.Reconvention)

	  WHERE
	  RCONV.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE Reconvention ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Reconvention] WITH NOCHECK ADD 
	   CONSTRAINT [pk_Reconvention_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_Reconvention_SecID] ON [dbo].[Reconvention]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_Reconvention_Acttime] ON [dbo].[Reconvention]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'redenomination')
	    drop table Redenomination
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Redenomination)
--	delete from Redenomination
--	where EVENTID in
--	(select RdnomID from wca.dbo.RDNOM
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Redenomination

SELECT 
RDNOM.EffectiveDate as Effective_Date,
RDNOM.OldDenomination1 as Old_Denom1,
RDNOM.OldDenomination2 as Old_Denom2,
RDNOM.OldDenomination3 as Old_Denom3,
RDNOM.OldDenomination4 as Old_Denom4,
RDNOM.OldDenomination5 as Old_Denom5,
RDNOM.OldDenomination6 as Old_Denom6,
RDNOM.OldDenomination7 as Old_Denom7,
RDNOM.OldMinimumDenomination as Old_Min_Denom,
RDNOM.OldDenominationMultiple as Old_Denom_Multiple,
RDNOM.NewDenomination1 as New_Denom1,
RDNOM.NewDenomination2 as New_Denom2,
RDNOM.NewDenomination3 as New_Denom3,
RDNOM.NewDenomination4 as New_Denom4,
RDNOM.NewDenomination5 as New_Denom5,
RDNOM.NewDenomination6 as New_Denom6,
RDNOM.NewDenomination7 as New_Denom7,
RDNOM.NewMinimumDenomination as New_Min_Denom,
RDNOM.NewDenominationMultiple as New_Denom_Multiple,
'WCA.dbo.RDNOM|RdnomID|Notes' as Link_Notes,
RDNOM.Actflag,
RDNOM.Acttime,
RDNOM.AnnounceDate,
RDNOM.RdnomID as EventID,
RDNOM.SecID

	  into wfi.dbo.Redenomination

from RDNOM
inner join bond on rdnom.secid = bond.secid

--	WHERE
--	RDNOM.Acttime > (select max(wfi.dbo.Redenomination.acttime) from wfi.dbo.Redenomination)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and RDNOM.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE Redenomination ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Redenomination] WITH NOCHECK ADD 
	   CONSTRAINT [pk_ReDenom_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_ReDenom_SecID] ON [dbo].[Redenomination]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_ReDenom_Acttime] ON [dbo].[Redenomination]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'int_freq_change')
	    drop table int_freq_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from int_freq_change)
--	delete from int_freq_change
--	where EVENTID in
--	(select IfchgId from wca.dbo.IFCHG
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.int_freq_change

SELECT 
IFCHG.NotificationDate as Notification_Date,
case when OLDFREQ.Lookup is null then IFCHG.OldIntPayFrqncy else OLDFREQ.Lookup end as Old_Interest_Pay_Freq,
case when (len(oldintpaydate1) <> 4 or oldintpaydate1 is NULL or oldintpaydate1 = '' ) then ''
when ((cast(substring(oldintpaydate1,3,2) as int) < 1) or (cast(substring(oldintpaydate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate1,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate1,1,2)
end as Old_Pay_Date_1,
case when (len(oldintpaydate2) <> 4 or oldintpaydate2 is NULL or oldintpaydate2 = '' ) then ''
when ((cast(substring(oldintpaydate2,3,2) as int) < 1) or (cast(substring(oldintpaydate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate2,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate2,1,2)
end as Old_Pay_Date_2,
case when (len(oldintpaydate3) <> 4 or oldintpaydate3 is NULL or oldintpaydate3 = '' ) then ''
when ((cast(substring(oldintpaydate3,3,2) as int) < 1) or (cast(substring(oldintpaydate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate3,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate3,1,2)
end as Old_Pay_Date_3,
case when (len(oldintpaydate4) <> 4 or oldintpaydate4 is NULL or oldintpaydate4 = '' ) then ''
when ((cast(substring(oldintpaydate4,3,2) as int) < 1) or (cast(substring(oldintpaydate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(oldintpaydate4,3,2) + '/01') as datetime),107),1,4) + substring(oldintpaydate4,1,2)
end as Old_Pay_Date_4,
case when NEWFREQ.Lookup is null then IFCHG.NewIntPayFrqncy else NEWFREQ.Lookup end as New_Interest_Pay_Freq,
case when (len(newintpaydate1) <> 4 or newintpaydate1 is NULL or newintpaydate1 = '' ) then ''
when ((cast(substring(newintpaydate1,3,2) as int) < 1) or (cast(substring(newintpaydate1,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate1,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate1,1,2)
end as New_Pay_Date_1,
case when (len(newintpaydate2) <> 4 or newintpaydate2 is NULL or newintpaydate2 = '' ) then ''
when ((cast(substring(newintpaydate2,3,2) as int) < 1) or (cast(substring(newintpaydate2,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate2,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate2,1,2)
end as New_Pay_Date_2,
case when (len(newintpaydate3) <> 4 or newintpaydate3 is NULL or newintpaydate3 = '' ) then ''
when ((cast(substring(newintpaydate3,3,2) as int) < 1) or (cast(substring(newintpaydate3,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate3,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate3,1,2)
end as New_Pay_Date_3,
case when (len(newintpaydate4) <> 4 or newintpaydate4 is NULL or newintpaydate4 = '' ) then ''
when ((cast(substring(newintpaydate4,3,2) as int) < 1) or (cast(substring(newintpaydate4,3,2) as int) > 12)) then 'Bad MM'
else substring(convert(varchar(20),cast(('2009/' + substring(newintpaydate4,3,2) + '/01') as datetime),107),1,4) + substring(newintpaydate4,1,2)
end as New_Pay_Date_4,
case when RELEVENT.Lookup is null then IFCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
IFCHG.OldVarIntPayDate as Old_Var_Interest_Pay_Date,
IFCHG.NewVarIntPayDate as New_Var_Interest_Pay_Date,
IFCHG.Actflag,
IFCHG.Acttime,
IFCHG.AnnounceDate,
IFCHG.IfchgId as EventID,
IFCHG.SecID

	  into wfi.dbo.int_freq_change

from IFCHG                                                      
left outer join WFI.dbo.sys_lookup as OLDFREQ on IFCHG.OldIntPayFrqncy = OLDFREQ.Code
                      and 'FREQ' = OLDFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as NEWFREQ on IFCHG.NewIntPayFrqncy = NEWFREQ.Code
                      and 'FREQ' = NEWFREQ.TypeGroup
left outer join WFI.dbo.sys_lookup as RELEVENT on IFCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                       

--	WHERE
--	IFCHG.Acttime > (select max(wfi.dbo.int_freq_change.acttime) from wfi.dbo.int_freq_change)

	  WHERE
	  IFCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE int_freq_change ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[int_freq_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_int_freq_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_int_freq_change_SecID] ON [dbo].[int_freq_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_int_freq_change_Acttime] ON [dbo].[int_freq_change]([Acttime] desc) ON [PRIMARY]
	  GO



	  use WFI
	  if exists (select * from sysobjects where name = 'int_rate_change')
	    drop table int_rate_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from int_rate_change)
--	delete from int_rate_change
--	where EVENTID in
--	(select IRChgId from wca.dbo.IRCHG
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.int_rate_change

SELECT 
IRCHG.EffectiveDate as Effective_Date,
IRCHG.OldInterestRate as Old_Interest_Rate,
IRCHG.NewInterestRate as New_Interest_Rate,
case when RELEVENT.Lookup is null then IRCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
'WCA.dbo.IRCHG|IRChgId|Notes' as Link_Notes,
IRCHG.Actflag,
IRCHG.Acttime,
IRCHG.AnnounceDate,
IRCHG.IRChgId as EventID,
IRCHG.SecID

	  into wfi.dbo.int_rate_change

from IRCHG                                                      
left outer join WFI.dbo.sys_lookup as RELEVENT on IRCHG.EventType = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup                       

--	WHERE
--	IRCHG.Acttime > (select max(wfi.dbo.int_rate_change.acttime) from wfi.dbo.int_rate_change)

	  WHERE
	  IRCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE int_rate_change ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[int_rate_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_int_rate_change_EVENTIDD] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_int_rate_change_SecID] ON [dbo].[int_rate_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_int_rate_change_Acttime] ON [dbo].[int_rate_change]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'consent')
	    drop table consent
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from consent)
--	delete from consent
--	where EVENTID in
--	(select RdID from wca.dbo.COSNT
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.consent

SELECT 
RD.recdate as Record_Date,
EXDT.exdate as Ex_Date,
EXDT.paydate as Pay_Date,
COSNT.ExpiryDate as Expiry_Date,
COSNT.ExpiryTime as Expiry_Time,
SUBSTRING(cosnt.TimeZone, 1,3) AS Time_Zone,
case when YNBLANK.Lookup is null then COSNT.CollateralRelease else YNBLANK.Lookup end as Collateral_Release,
COSNT.Currency as Redemption_Currency,
COSNT.Fee as Fee,
'WCA.dbo.COSNT|RdID|Notes' as Link_Notes,
COSNT.Actflag,
COSNT.Acttime,
COSNT.AnnounceDate,
COSNT.RdID as EventID,
RD.SecID 

	  into wfi.dbo.consent

from COSNT   
INNER JOIN RD ON COSNT.RdID = RD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCMST.PrimaryExchgCD = EXDT.ExchgCD AND 'COSNT' = EXDT.EventType
left outer join WFI.dbo.sys_lookup as YNBLANK on COSNT.CollateralRelease = YNBLANK.Code
                      and 'YNBLANK' = YNBLANK.TypeGroup
                      
--	WHERE
--	COSNT.Acttime > (select max(wfi.dbo.consent.acttime) from wfi.dbo.consent)

	  WHERE
	  COSNT.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE consent ALTER COLUMN  EVENTID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[consent] WITH NOCHECK ADD 
	   CONSTRAINT [pk_consent_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO	  
	  use WFI 
	  CREATE  INDEX [ix_liquidation_SecID] ON [dbo].[consent]([SecID]) ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_consent_Acttime] ON [dbo].[consent]([Acttime] desc) ON [PRIMARY]
	  GO



	  use WFI
	  if exists (select * from sysobjects where name = 'Exchange_Offer')
	    drop table Exchange_Offer
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Exchange_Offer)
--	delete from Exchange_Offer
--	where EVENTID in
--	(select ConvID from wca.dbo.CONV
--	where acttime>@StartDate)
--	GO

use wca

--	insert into wfi.dbo.Exchange_Offer

SELECT 
CONV.FromDate as From_Date,
CONV.ToDate as To_Date,
CONV.RatioNew as New_Ratio,
CONV.RatioOld as Old_Ratio,
CONV.CurenCD as Conversion_Currency,
CONV.Price,
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
CONV.ResSecID AS Resultant_SecID,
RESSCMST.ISIN as Resultant_ISIN,
case when SECTYCD.Lookup is null then CONV.ResSectyCD else SECTYCD.Lookup end as Resultant_Type,
case when FRACTIONS.Lookup is null then CONV.Fractions else FRACTIONS.Lookup end as Fractions,
CONV.FXrate as Foreign_Exchange_Rate,
case when Partfinalflag='P' then 'Part'
     when Partfinalflag='F' then 'Final'
     else Partfinalflag end as Part_Final_Flag, 
RD.Recdate as Record_Date,
CONV.Priceaspercent as Price_as_Percent,
'WCA.dbo.CONV|ConvID|CONVNotes' as Link_Notes,
CONV.Actflag,
CONV.Acttime,
CONV.AnnounceDate,
CONV.ConvID as EventID,
CONV.SecID

	  into wfi.dbo.Exchange_Offer

from CONV
inner join bond on conv.secid = bond.secid
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join rd on CONV.rdid = rd.rdid
left outer join WFI.dbo.sys_lookup as FRACTIONS on CONV.Fractions = FRACTIONS.Code
                      and 'FRACTIONS' = FRACTIONS.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CONV.ResSectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CONV.Acttime > (select max(wfi.dbo.Exchange_Offer.acttime) from wfi.dbo.Exchange_Offer)
--	and BOND.BondType <> 'PRF'
--	and Convtype='TENDER'


	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CONV.Actflag<>'D'
	  and Convtype='TENDER'

GO

	  use WFI 
	  ALTER TABLE Exchange_Offer ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Exchange_Offer] WITH NOCHECK ADD 
	   CONSTRAINT [pk_Exchange_Offer_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_Exchange_Offer_SecID] ON [dbo].[Exchange_Offer]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_Exchange_Offer_Acttime] ON [dbo].[Exchange_Offer]([Acttime] desc) ON [PRIMARY]
	  GO


	  use WFI
	  if exists (select * from sysobjects where name = 'Tender')
	    drop table Tender
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from Tender)
--	delete from Tender
--	where RedemID in
--	(select RedemID from wca.dbo.REDEM
--	where acttime>@StartDate)
--	GO

 
use wca

--	insert into wfi.dbo.Tender

SELECT 
case when REDEM.RedemDate = '1800/01/01' then null else REDEM.RedemDate end as Redemption_Date, 
case when MandOptFlag='M' then 'Mandatory'
     when MandOptFlag='O' then 'Optional'
     else MandOptFlag end as Status, 
case when Partfinal='P' then 'Part'
     when Partfinal='F' then 'Final'
     else Partfinal end as Part_or_Final, 
Poolfactor as Pool_Factor,
RedemPercent as Tender_Percent,
REDEM.CurenCD as Tender_Currency,
REDEM.AmountRedeemed as Tender_Amount,
REDEM.RedemPrice as Tender_Price,
REDEM.Priceaspercent as Price_as_Percent,
REDEM.RedemPremium as Tender_Premium,
REDEM.Premiumaspercent as Premium_as_Percent,
'WCA.dbo.REDEM|RedemID|RedemNotes' as Link_Notes,
REDEM.Actflag,
REDEM.Acttime,
REDEM.AnnounceDate,
REDEM.RedemID as EventID,
REDEM.SecID

	  into wfi.dbo.Tender

from REDEM
inner join bond on redem.secid = bond.secid
left outer join WFI.dbo.sys_lookup as REDEMTYPE on REDEM.Redemtype = REDEMTYPE.Code
                      and 'REDEMTYPE' = REDEMTYPE.TypeGroup

--	WHERE
--	REDEM.Acttime > (select max(wfi.dbo.Tender.acttime) from wfi.dbo.Tender)
--	and BOND.BondType <> 'PRF'
--	and redemtype='TENDER'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D' and REDEM.Actflag<>'D'
	  and redemtype='TENDER'

GO

	  use WFI 
	  ALTER TABLE Tender ALTER COLUMN  RedemID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[Tender] WITH NOCHECK ADD 
	   CONSTRAINT [pk_Tender_RedemID] PRIMARY KEY ([RedemID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_Tender_SecID] ON [dbo].[Tender]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_Tender_Acttime] ON [dbo].[Tender]([Acttime] desc) ON [PRIMARY]
	  GO



	  use WFI
	  if exists (select * from sysobjects where name = 'conversion_terms_change')
	    drop table conversion_terms_change
	  GO

--	use WFI
--	Declare @StartDate datetime
--	set @StartDate = (select max(acttime) from conversion_terms_change)
--	delete from conversion_terms_change
--	where EVENTID in
--	(select CTCHGID from wca.dbo.CTCHG
--	where acttime>@StartDate)
--	GO


use wca

--	insert into wfi.dbo.conversion_terms_change

SELECT 
CTCHG.EffectiveDate as Effective_Date,
CTCHG.OldFromDate as Old_From_Date,
CTCHG.NewFromDate as New_From_Date,
CTCHG.OldToDate as Old_To_Date,
CTCHG.NewToDate as New_To_Date,
CTCHG.OldResultantRatio as Old_Resultant_Ratio,
CTCHG.NewResultantRatio as New_Resultant_Ratio,
CTCHG.OldSecurityRatio as Old_Security_Ratio,
CTCHG.NewSecurityRatio as New_Security_Ratio,
CTCHG.OldCurrency as Old_Currency,
CTCHG.NewCurrency as New_Currency,
CTCHG.OldConversionPrice as Old_ConversionPrice,
CTCHG.NewConversionPrice as New_ConversionPrice,
CTCHG.OldResSecID as Old_Resultant_SecID,
OLDRES.ISIN as Old_Resultant_ISIN,
CTCHG.NewResSecID as New_Resultant_SecID,
NEWRES.ISIN as New_Resultant_ISIN,
case when SECTYCD.Lookup is null then CTCHG.ResSectyCD else SECTYCD.Lookup end as Resultant_Type,
CTCHG.OldFXrate as Old_Foreign_Exchange_Rate,
CTCHG.NewFXrate as New_Foreign_Exchange_Rate,
CTCHG.OldPriceaspercent as Old_Price_as_Percent,
CTCHG.NewPriceaspercent as New_Price_as_Percent,
case when RELEVENT.Lookup is null then CTCHG.EventType when RELEVENT.Lookup = 'Clean' then 'Batch Correction' else RELEVENT.Lookup end as Reason,
CTCHG.RelEventID as Related_EventID,
CTCHG.ConvtID as Conversion_Terms_ID,
'WCA.dbo.CTCHG|CTCHGID|CTCHGNotes' as Link_Notes,
CTCHG.Actflag,
CTCHG.Acttime,
CTCHG.AnnounceDate,
CTCHG.CTCHGID as EventID,
CTCHG.SecID

	  into wfi.dbo.conversion_terms_change

from CTCHG
inner join bond on CTCHG.secid = bond.secid
left outer join scmst as oldres on CTCHG.oldressecid = oldres.secid
left outer join scmst as newres on CTCHG.newressecid = newres.secid
left outer join WFI.dbo.sys_lookup as RELEVENT on CTCHG.Eventtype = RELEVENT.Code
                      and 'EVENT' = RELEVENT.TypeGroup
left outer join WFI.dbo.sys_lookup as SECTYCD on CTCHG.ResSectyCD = SECTYCD.Code
                      and 'SECTYCD' = SECTYCD.TypeGroup                      
                      

--	WHERE
--	CTCHG.Acttime > (select max(wfi.dbo.conversion_terms_change.acttime) from wfi.dbo.conversion_terms_change)
--	and BOND.BondType <> 'PRF'

	  WHERE
	  BOND.BondType <> 'PRF' and BOND.Actflag<>'D'
	  and CTCHG.Actflag<>'D'

GO

	  use WFI 
	  ALTER TABLE conversion_terms_change ALTER COLUMN  EventID int NOT NULL
	  GO 
	  use WFI 
	  ALTER TABLE [DBO].[conversion_terms_change] WITH NOCHECK ADD 
	   CONSTRAINT [pk_conversion_terms_change_EVENTID] PRIMARY KEY ([EVENTID])  ON [PRIMARY]
	  GO
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_change_SecID] ON [dbo].[conversion_terms_change]([SecID]) ON [PRIMARY] 
	  GO 
	  use WFI 
	  CREATE  INDEX [ix_conversion_terms_change_Acttime] ON [dbo].[conversion_terms_change]([Acttime] desc) ON [PRIMARY]
	  GO

