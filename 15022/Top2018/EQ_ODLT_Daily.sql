--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--ODLT_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\newedge_268\
--FileTidy=N
--sEvent=ODDLT
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=7990
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
'' as bbgcntry,
'' as BBGCUREN,
Changed,
'select  Notes FROM ODDLT WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
maintab.paydate as PAYLINK,
Startdate as OPENLINK,
Enddate as CLOSELINK,
MaxAcpQty as MAQLINK,
case when 1=1 then (select '{1:F01SAMPLEXXXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Exdate is not null
              and (SR.CAMV<>'VOLU' or SR.TFB<>':35B:'+mainTFB2)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '163' END as CAref1,
maintab.EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ODLT' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB2 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.MPAY.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartSeqE,
'' as Notes, 
'-}$'
From v_EQ_ODLT as maintab
left outer join wca.dbo.MPAY on maintab.eventid = wca.dbo.MPAY.eventid and 'ODDLT' = wca.dbo.MPAY.sevent
                                             and 'D'=wca.dbo.MPAY.paytype and 'D'<>wca.dbo.MPAY.actflag
left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'ODLT'=SR.CAEVMAIN
WHERE
mainTFB2<>''
and changed>(select max(feeddate)-300 as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
order by caref2 desc, caref1, caref3
