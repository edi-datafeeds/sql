--SEMETABLE=client.dbo.seme_daiwa_272
--FileName=edi_YYYYMMDD
--INCR_CAPRD_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where feedate='2015/05/15'
--archive=off
--ArchivePath=n:\15022\daiwa\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  CapRdNotes As Notes FROM CAPRD WHERE CAPRDID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'148'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INCR'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN' THEN '/GB/' + Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT/'+NewParValue as commasub,
':16S:CADETL',
CASE WHEN rcapid is not null THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN rcapid is not null THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22F::CAOP//CASH' ELSE '' END,
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
CASE WHEN rcapid is not null THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16R:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN CSPYDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     WHEN rcapid is not null THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN CashBak<>'' AND CashBak IS NOT NULL
     THEN ':92F::GRSS//'+CurenCD+substring(CashBak,1,15)
     WHEN rcapid is not null THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as Commasub,
CASE WHEN rcapid is not null THEN ':16S:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16S:CAOPTN' ELSE '' END,
'' as Notes, 
'-}$'
From v_EQ_DECR_CAPRD
WHERE 
mainTFB1<>''
and ((isin in (select code from client.dbo.pfisin where accid=272 and actflag='I')
and Effectivedate > getdate())
OR
(isin in (select code from client.dbo.pfisin where accid=272 and actflag='U')
and changed>'2018/05/16' and changed<'2018/05/17'))
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and rtrim(OldParValue)<>'' and rtrim(NewParValue)<>''
and rtrim(OldParValue)<>'0' and rtrim(NewParValue)<>'0'
and cast(rtrim(OldParValue) as float(16,7))<cast(rtrim(NewParValue) as float(16,7))
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3
