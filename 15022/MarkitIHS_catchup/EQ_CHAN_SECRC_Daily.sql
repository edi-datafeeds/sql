--SEMETABLE=client.dbo.seme_markitihs
--FileName=edi_YYYYMMDD
--CHAN_SECRC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as MaxDate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed,
case when 1=1 then (select '{1:F01IHSMARKTXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'191'+EXCHGID as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN isin <>'' THEN 'ISIN '+isin ELSE '' END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN Effectivedate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Effectivedate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//TERM',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newsedol <>''
     THEN ':35B:/GB/'+newsedol
     WHEN resTFB1 <> ''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//EXCH/SECM',
':16S:FIA',
':92D::NEWO//1,/1,',
CASE WHEN Effectivedate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , Effectivedate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_CHAN_SECRC as maintab
WHERE
sedol<>''
and opol<>'SHSC' and opol<>'XSSC'
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and maintab.secid<>maintab.ressecid
and CalcListdate<=maintab.announcedate
and CalcDelistdate>=maintab.announcedate
order by caref2 desc, caref1, caref3
