--SEMETABLE=client.dbo.seme_fidelity
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_REDM
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select distinct
v_FI_REDM.changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then rtrim(char(32)) else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(v_FI_REDM.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '410'+EXCHGID+cast(Seqnum as char(1))
     ELSE '410'
     END as CAref1,
v_FI_REDM.EventID as CAREF2,
v_FI_REDM.SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_REDM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_REDM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_REDM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_REDM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//BIDS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB2,
case when substring(mainTFB2,2,2)='GB' and isin<>'' then 'ISIN ' + isin else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN v_FI_REDM.InterestRate <> '' THEN ':92A::INTR//'+v_FI_REDM.InterestRate ELSE '' END as COMMASUB,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
/*':70E::OFFO//'+substring(Issuername,1,35) as TIDYTEXT,*/
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_FI_REDM.CurenCD <>''
     THEN ':11A::OPTN//' + v_FI_REDM.CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN v_FI_REDM.TenderCloseDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , v_FI_REDM.TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_FI_REDM.TenderOpenDate<>'' AND v_FI_REDM.TenderCloseDate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_FI_REDM.TenderOpenDate,112)+'/'
          +CONVERT ( varchar , v_FI_REDM.TenderCloseDate,112)
     WHEN v_FI_REDM.TenderOpenDate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_FI_REDM.TenderOpenDate,112)+'/UKWN'
     WHEN v_FI_REDM.TenderCloseDate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_FI_REDM.TenderCloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_REDM.RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , v_FI_REDM.RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN v_FI_REDM.RedemPrice <> '' and v_FI_REDM.RedemPremium <> ''
     THEN ':90B::OFFR//ACTU/' + v_FI_REDM.CurenCD 
          + cast(cast(((cast(v_FI_REDM.RedemPrice as decimal(14,5))/cast(nominalvalue as decimal(14,5))
          +cast(v_FI_REDM.RedemPremium as decimal(14,5))/cast(nominalvalue as decimal(14,5)))*1000) as money)  as varchar (30))
     WHEN v_FI_REDM.RedemPrice <> ''
     THEN ':90B::OFFR//ACTU/' + v_FI_REDM.CurenCD 
          + cast(cast(((cast(v_FI_REDM.RedemPrice as decimal(14,5))/cast(nominalvalue as decimal(14,5))
          /cast(nominalvalue as decimal(14,5)))*1000) as money)  as varchar (30))
     WHEN v_FI_REDM.RedemPercent <>''
     THEN ':90A::OFFR//PRCT/' + v_FI_REDM.RedemPercent
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//CASH',
CASE WHEN Redem.CurenCD <>''
     THEN ':11A::OPTN//' + Redem.CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN Redem.TenderCloseDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , Redem.TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN Redem.TenderOpenDate<>'' AND Redem.TenderCloseDate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Redem.TenderOpenDate,112)+'/'
          +CONVERT ( varchar , Redem.TenderCloseDate,112)
     WHEN Redem.TenderOpenDate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Redem.TenderOpenDate,112)+'/UKWN'
     WHEN Redem.TenderCloseDate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Redem.TenderCloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Redem.RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , Redem.RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN Redem.RedemPrice <> '' and Redem.RedemPremium <> ''
     THEN ':90B::OFFR//ACTU/' + Redem.CurenCD 
          + cast(cast(((cast(Redem.RedemPrice as decimal(14,5))/cast(nominalvalue as decimal(14,5))
          +cast(Redem.RedemPremium as decimal(14,5))/cast(nominalvalue as decimal(14,5)))*1000) as money)  as varchar (30))
     WHEN Redem.RedemPrice <> ''
     THEN ':90B::OFFR//ACTU/' + Redem.CurenCD 
          + cast(cast(((cast(Redem.RedemPrice as decimal(14,5))/cast(nominalvalue as decimal(14,5))
          /cast(nominalvalue as decimal(14,5)))*1000) as money)  as varchar (30))
     WHEN Redem.RedemPercent <>''
     THEN ':90A::OFFR//PRCT/' + Redem.RedemPercent
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//003',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
':16S:CAOPTN',
':16R:ADDINFO',
':70E::TXNR//LISTCNTRY'
'INCORP:'+CntryofIncorp as dummy,
'DEBTTYPE:'+Isstype as dummy,
'' as Notes, 
':16S:ADDINFO',
'-}$'
From v_FI_REDM
left outer join redem on v_FI_REDM.secid=redem.secid
                     and 'BBRD'=redem.RedemType
                     and redem.tenderopendate=v_FI_REDM.tenderopendate
WHERE
mainTFB1<>''
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and v_FI_REDM.redemdate <>''
and v_FI_REDM.redemdate <> '1800/01/01'
and v_FI_REDM.redemtype='BBED'
and v_FI_REDM.DutchAuction<>'T'
and v_FI_REDM.TenderOfferor=''
order by caref2 desc, caref1, caref3
