--SEMETABLE=client.dbo.seme_fidelity
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_EXTM
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select distinct
v_FI_EXTM.changed,
'select case when len(cast(Notes as varchar(24)))=22 then rtrim(char(32)) else Notes end As Notes FROM MTCHG WHERE MtchgID = '+ cast(v_FI_EXTM.EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '415'+EXCHGID+cast(Seqnum as char(1))
     ELSE '415'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_EXTM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_EXTM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_EXTM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_EXTM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXTM',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB2,
case when substring(mainTFB2,2,2)='GB' and isin<>'' then 'ISIN ' + isin else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_EXTM.NewMaturityDate is not null and v_FI_EXTM.NewMaturityDate <>''
     THEN ':98A::MATU//'+CONVERT ( varchar , v_FI_EXTM.NewMaturityDate,112)
     ELSE ':98A::MATU//UKWN'
     END,
':16S:CADETL',
'' as Notes,  
'-}$'
From v_FI_EXTM
WHERE
mainTFB1<>''
and changed>(select max(feeddate)  from wca.dbo.tbl_opslog where seq=3)

order by caref2 desc, caref1, caref3
