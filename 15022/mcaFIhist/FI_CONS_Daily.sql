--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_CONS
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
v_FI_CONS.changed,
'select case when len(cast(Notes as varchar(24)))=22 then rtrim(char(32)) else Notes end As Notes FROM COSNT WHERE RDID = '+ cast(v_FI_CONS.EventID as char(16)) as ChkNotes,
'{1:F01MMCAUS22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '416'+EXCHGID+cast(Seqnum as char(1))
     ELSE '416'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_CONS.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_CONS.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_CONS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_CONS.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_CONS.RecDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , v_FI_CONS.RecDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN v_FI_CONS.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_FI_CONS.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONY',
':17B::DFLT//N',
':17B::WTHD//Y',
CASE WHEN v_FI_CONS.Expirydate <>''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_FI_CONS.Expirydate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
':17B::WTHD//Y',
CASE WHEN v_FI_CONS.Expirydate <>''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_FI_CONS.Expirydate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
':16S:CAOPTN',
/* '' as Notes, */ 
'-}$'
From v_FI_CONS
WHERE
mainTFB1<>''
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
and isnull(maintab.Expirydate,'2001/01/01')>'2019/06/13'

order by caref2 desc, caref1, caref3
