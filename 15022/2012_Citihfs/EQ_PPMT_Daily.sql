--SEMETABLE=client.dbo.seme_citihfs
--FileName=15022_EQ_YYYYMMDD
--PPMT_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_citihfs\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=

--# 1
use wca
select distinct top 20
Changed,
'' as ISOHDR,
':16R:GENL',
'111'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PPMT',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP,
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DueDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , DueDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD
     END,
':17B::DFLT//N',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN DueDate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , DueDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN ToFacevalue <> ''
     THEN ':90B::PRPP//ACTU/'+curenCD+rtrim(cast(ToFacevalue as char(15)))
     ELSE ':90E::PRPP//UKWN' END as commasub,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//LAPS',
':17B::DFLT//Y',
':16S:CAOPTN',
'-}$'
From v_EQ_PPMT
WHERE 
((changed>='2011/01/01'
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Duedate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
    ))
and maintfb2<>''
and (mcd<>'' and mcd is not null)
and Duedate is not null
and CalcListdate<=Duedate
and CalcDelistdate>=Duedate
order by caref2 desc, caref1, caref3