--SEMETABLE=client.dbo.seme_mirae
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_DFLT_INT
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=3
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2
--SUPPADDTAGS=N

--# 1
use wca
select distinct
changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then rtrim(char(32)) else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01XMIRAE22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '400'+EXCHGID+cast(Seqnum as char(1))
     ELSE '400'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DFLT',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB3,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>'' and localcode is not null
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' or MCD='SECM' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD<>'' and PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD and ExchgCD<>'' THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
case when debtcurrency='USD' then ':22F::MICO//A006' else '' end,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN InterestFromdate <>'' AND InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/'
          +CONVERT ( varchar , InterestToDate,112)
     WHEN InterestFromDate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/UKWN'
     WHEN InterestToDate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , InterestToDate,112)
     ELSE ':69J::INPE//UKWN'
     END,
CASE WHEN AnlCoupRate  <>''
     THEN ':92A::INTR//' + AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD  <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+rtrim(cast(parvalue as char (20))) ELSE '' END as COMMASUB,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 
     THEN ':36B::MILT//FAMT/'+ltrim(cast(denominationmultiple as char(20))) ELSE '' END as COMMASUB,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN InterestPaymentFrequency = 'ANL'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + AnlCoupRate
     WHEN InterestPaymentFrequency = 'SMA'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN InterestPaymentFrequency = 'QTR'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN InterestPaymentFrequency = 'MNT'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_INTR as maintab
WHERE
mainTFB3<>''
and maintab.cntryofincorp<>'US'
and changed>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and indefpay <>''
and indefpay = 'F'
order by caref2 desc, caref1, caref3
