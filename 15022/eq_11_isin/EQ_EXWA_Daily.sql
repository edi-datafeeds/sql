--FilePath=o:\datafeed\15022\eq_11_isin\
--FileName=YYYYMMDD_MT564
--EXWA_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\eq_11_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
Changed,
/*'select  WartmNotes As Notes FROM WARTM WHERE SECID = '+ cast(SecID as char(16)) as ChkNotes, */ 
'' as ISOHDR,
':16R:GENL',
'300'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXWA',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD
     END,
':17B::DFLT//N',
CASE WHEN ExpirationDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , ExpirationDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN ExpirationDate <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , ExpirationDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN ToDate <>''
       AND FromDate <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/'
          +CONVERT ( varchar , ToDate,112)
     WHEN FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/UKWN'
     WHEN ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN exerTFB1<>''
     THEN ':35B:' + exerTFB1
     ELSE ':35B:' + 'UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew <>''
          AND RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(RatioNew as char (15)))+
                    '/'+rtrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN FromDate <>'' AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN FromDate <>'' AND FromDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , FromDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN StrikePrice <> ''
     THEN ':90B::EXER//ACTU/'+CurenCD+rtrim(cast(StrikePrice as char(15)))
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_EXWA
WHERE 
mainTFB1<>''
and changed>(select max(feeddate) from tbl_opslog where seq=3)
and (mcd<>'' and mcd is not null)
and ExpirationDate is not null
and CalcListdate<=ExpirationDate
and CalcDelistdate>=ExpirationDate
and EventID is not null
and ExpirationDate>getdate()-122
order by caref2 desc, caref1, caref3