--FilePath=o:\datafeed\15022\eq_11_isin\
--FileName=YYYYMMDD_MT564
--CAPG_RCAP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\eq_11_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
Changed,
/*'select  RcapNotes As Notes FROM RCAP WHERE RCAPID = '+ cast(EventID as char(16)) as ChkNotes, */ 
'' as ISOHDR,
':16R:GENL',
'165'+EXCHGID+'1' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CAPG',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' +MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN RecDate <>'' and RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN EffectiveDate <>'' and effectivedate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN CSPYDate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN CashBak <>'' AND CashBak IS NOT NULL
     THEN ':92F::GRSS//'+CurenCD+
          +substring(CashBak,1,15)
     ELSE ':92K::GRSS//UKWN'
     END as Commasub,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_CAPG_RCAP
WHERE 
mainTFB1<>''
and changed>(select max(feeddate) from tbl_opslog where seq=3)
and (mcd<>'' and mcd is not null)
and Effectivedate is not null
and CalcListdate<=Effectivedate
and CalcDelistdate>=Effectivedate
and effectivedate>getdate()-122
order by caref2 desc, caref1, caref3

