--SEMETABLE=client.dbo.seme_markit
--FileName=edi_YYYYMMDD
--BIDS_PREC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as MaxDate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\smartstream\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed,
'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'115'+EXCHGID as CAref1,
maintab.eventid as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN isin <>'' THEN 'ISIN '+isin ELSE '' END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN StartDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN maintab.MaxQlyQty<>''
     THEN ':36B::FOLQ//UNIT/'+substring(maintab.MaxQlyQty,1,15) + ','
     ELSE '' END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
':11A::OPTN//USD',
':17B::DFLT//N',
CASE WHEN Enddate<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , Enddate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN Enddate<>'' AND Startdate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Startdate,112)+'/'
          +CONVERT ( varchar , Enddate,112)
     WHEN Startdate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Startdate,112)+'/UKWN'
     WHEN Enddate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Enddate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN maintab.MaxAcpQty <>''
     THEN ':36B::FOLQ//UNIT/'+substring(maintab.MaxAcpQty,1,15) + ','
     ELSE ':36C::FOLQ//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.PayDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , maintab.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':90E::OFFR//UKWN',
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From wca2.dbo.t_EQ_BIDS as maintab
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'BB' = wca.dbo.mpay.sevent
                                         and 'D'<>wca.dbo.mpay.actflag
WHERE
sedol<>''
and opol<>'SHSC' and opol<>'XSSC'
and wca.dbo.mpay.eventid is null
and CalcListdate<=maintab.announcedate
and CalcDelistdate>=maintab.announcedate
order by caref2 desc, caref1, caref3
