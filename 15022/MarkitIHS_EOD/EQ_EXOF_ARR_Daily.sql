--SEMETABLE=client.dbo.seme_markit
--FileName=edi_YYYYMMDD
--EXOF_ARR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as MaxDate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--sEvent=ARR
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'' as bbgcntry,
'' as BBGCUREN,
'select  ArrNotes As Notes FROM Arr WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'112' as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN isin <>'' THEN 'ISIN '+isin ELSE '' END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.announcedate,112) ELSE '' END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Exdate, 112)
     ELSE ':98B::EFFD//UKWN'
     END as DateEx,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.MPAY.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as WholeWordNotes,
'-}$'
From v_EQ_OTHR_ARR as maintab
left outer join wca.dbo.MPAY on maintab.eventid = wca.dbo.MPAY.eventid and 'ARR' = wca.dbo.MPAY.sevent
                                             and 'D'=wca.dbo.MPAY.paytype and 'D'<>wca.dbo.MPAY.actflag
WHERE
mainTFB1<>''
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and maintab.changed>(select max(acttime_new)-0.05 from wca.dbo.tbl_opslog)
order by caref2 desc, caref1, caref3
