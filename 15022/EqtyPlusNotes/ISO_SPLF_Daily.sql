--FilePath=O:\Datafeed\ISO\15022\DAILY\
--FilePrefix=
--FileName=YYYYMMDD_15022SPLF
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\ISO\15022\DAILY\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select   
v54f_ISO_SPLF.Changed,
'' as ISOHDR,
':16R:GENL',
'120'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_SPLF.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_SPLF.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_SPLF.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_SPLF.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLF',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
CASE WHEN v54f_ISO_SPLF.OldSid <> '' and v54f_ISO_SPLF.OldSidName = 'ISIN'
     THEN ':35B:' + v54f_ISO_SPLF.OldSidname +' '+ v54f_ISO_SPLF.OldSid
     WHEN v54f_ISO_SPLF.OldSid <> '' and v54f_ISO_SPLF.SidName = '/GB/'
     THEN ':35B:' + v54f_ISO_SPLF.OldSidname +' '+ v54f_ISO_SPLF.OldSid
     ELSE ':35B:' + v54f_ISO_SPLF.Sidname +' '+ v54f_ISO_SPLF.Sid
     END,
substring(v54f_ISO_SPLF.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_SPLF.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_SPLF.Sedol <> '' and v54f_ISO_SPLF.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_SPLF.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_SPLF.Localcode <> ''
     THEN '/TS/' + v54f_ISO_SPLF.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_SPLF.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_SPLF.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_SPLF.Exdate <>'' AND v54f_ISO_SPLF.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_SPLF.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN v54f_ISO_SPLF.Recdate <>'' AND v54f_ISO_SPLF.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v54f_ISO_SPLF.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END as DateRec,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v54f_ISO_SPLF.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v54f_ISO_SPLF.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v54f_ISO_SPLF.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v54f_ISO_SPLF.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v54f_ISO_SPLF.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v54f_ISO_SPLF.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_SPLF.NewSid <>''
     THEN ':35B:' + v54f_ISO_SPLF.NewSidname +' '+ v54f_ISO_SPLF.NewSid
     ELSE ':35B:' + v54f_ISO_SPLF.Sidname +' '+ v54f_ISO_SPLF.Sid
     END,
CASE WHEN v54f_ISO_SPLF.RatioNew <>''
            AND v54f_ISO_SPLF.RatioNew IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(v54f_ISO_SPLF.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_SPLF.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN v54f_ISO_SPLF.Paydate <>'' 
               AND v54f_ISO_SPLF.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_SPLF.paydate,112)
     ELSE ':98B::PAYD//UKWN' END as DatePay,
':16S:SECMOVE',
':16S:CAOPTN',
CASE WHEN v54f_ISO_SPLF.OldParValue <> '' or v54f_ISO_SPLF.NewParValue <> ''
     THEN ':16R:ADDINFO'
     ELSE '' END,
CASE WHEN v54f_ISO_SPLF.OldParValue <> ''
     THEN ':70E::ADTX//' + 'OldParValue '+v54f_ISO_SPLF.OldParValue
     ELSE ''
     END, 
CASE WHEN v54f_ISO_SPLF.NewParValue <> ''
     THEN ':70E::ADTX//' + 'NewParValue '+v54f_ISO_SPLF.NewParValue
     ELSE ''
     END, 
CASE WHEN v54f_ISO_SPLF.OldParValue <> '' or v54f_ISO_SPLF.NewParValue <> ''
     THEN ':16S:ADDINFO'
     ELSE '' END,
'-}$'
From v54f_ISO_SPLF
WHERE 
(SID <> '' or OldSID <> '')
and changed>(select max(feeddate) from tbl_opslog where seq=3)
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
