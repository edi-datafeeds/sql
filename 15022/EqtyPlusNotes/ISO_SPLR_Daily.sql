--FilePath=O:\Datafeed\ISO\15022\DAILY\
--FilePrefix=
--FileName=YYYYMMDD_15022SPLR
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\ISO\15022\DAILY\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select   
v54f_ISO_SPLR.Changed,
'' as ISOHDR,
':16R:GENL',
'121'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_SPLR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_SPLR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_SPLR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_SPLR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
CASE WHEN v54f_ISO_SPLR.OldSid <> '' and v54f_ISO_SPLR.OldSidName = 'ISIN'
     THEN ':35B:' + v54f_ISO_SPLR.OldSidname +' '+ v54f_ISO_SPLR.OldSid
     WHEN v54f_ISO_SPLR.OldSid <> '' and v54f_ISO_SPLR.SidName = '/GB/'
     THEN ':35B:' + v54f_ISO_SPLR.OldSidname +' '+ v54f_ISO_SPLR.OldSid
     ELSE ':35B:' + v54f_ISO_SPLR.Sidname +' '+ v54f_ISO_SPLR.Sid
     END,
substring(v54f_ISO_SPLR.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_SPLR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_SPLR.Sedol <> '' and v54f_ISO_SPLR.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_SPLR.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_SPLR.Localcode <> ''
     THEN '/TS/' + v54f_ISO_SPLR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_SPLR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_SPLR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_SPLR.Exdate <>'' AND v54f_ISO_SPLR.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_SPLR.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v54f_ISO_SPLR.Recdate <>'' AND v54f_ISO_SPLR.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v54f_ISO_SPLR.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v54f_ISO_SPLR.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v54f_ISO_SPLR.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v54f_ISO_SPLR.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v54f_ISO_SPLR.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v54f_ISO_SPLR.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v54f_ISO_SPLR.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_SPLR.NewSid <>''
     THEN ':35B:' + v54f_ISO_SPLR.NewSidname +' '+ v54f_ISO_SPLR.NewSid
     ELSE ':35B:' + v54f_ISO_SPLR.Sidname +' '+ v54f_ISO_SPLR.Sid
     END,
CASE WHEN v54f_ISO_SPLR.RatioNew <>''
            AND v54f_ISO_SPLR.RatioNew IS NOT NULL
     THEN ':92D::NEWO//'+ltrim(cast(v54f_ISO_SPLR.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_SPLR.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
CASE WHEN v54f_ISO_SPLR.Paydate <>'' 
               AND v54f_ISO_SPLR.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_SPLR.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
CASE WHEN v54f_ISO_SPLR.OldParValue <> '' or v54f_ISO_SPLR.NewParValue <> ''
     THEN ':16R:ADDINFO'
     ELSE '' END,
CASE WHEN v54f_ISO_SPLR.OldParValue <> ''
     THEN ':70E::ADTX//' + 'OldParValue '+v54f_ISO_SPLR.OldParValue
     ELSE ''
     END, 
CASE WHEN v54f_ISO_SPLR.NewParValue <> ''
     THEN ':70E::ADTX//' + 'NewParValue '+v54f_ISO_SPLR.NewParValue
     ELSE ''
     END, 
CASE WHEN v54f_ISO_SPLR.OldParValue <> '' or v54f_ISO_SPLR.NewParValue <> ''
     THEN ':16S:ADDINFO'
     ELSE '' END,
'-}$'
From v54f_ISO_SPLR
WHERE 
(SID <> '' or OldSID <> '')
and changed>(select max(feeddate) from tbl_opslog where seq=3)
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
