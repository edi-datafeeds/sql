--SEMETABLE=client.dbo.seme_daiwa
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_DFLT_INT
--FileNameNotes=15022_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=350
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then rtrim(char(32)) else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01BICSAM22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '400'+EXCHGID
     ELSE '400'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DFLT',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else '' 
     end,
case when len(Issuername)<36
     then Issuername
     else substring(Issuername,24,35)
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end,
substring(bonddesc3,1,35),
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when issuedate<>'' then ':98A::ISSU//'+CONVERT(varchar, issuedate,112) else '' end,
case when debtcurrency='USD' then ':22F::MICO//A006' else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN InterestFromdate <>'' AND InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/'
          +CONVERT ( varchar , InterestToDate,112)
     WHEN InterestFromDate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/UKWN'
     WHEN InterestToDate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , InterestToDate,112)
     ELSE ':69J::INPE//UKWN'
     END,
CASE WHEN AnlCoupRate  <>''
     THEN ':92A::INTR//' + AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD  <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN MinimumDenomination<>'' THEN ':36B::MIEX//FAMT/'+MinimumDenomination ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN Nilint='T'
     THEN ':92K::INTP//NILP'
     WHEN InterestPaymentFrequency = 'ANL'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + AnlCoupRate
     WHEN InterestPaymentFrequency = 'SMA'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN InterestPaymentFrequency = 'QTR'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN InterestPaymentFrequency = 'MNT'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
CASE WHEN maintab.GrossInterest<>'' and maintab.GrossInterest<>'0'
     THEN ':92F::GRSS//'+ maintab.CurenCD
     ELSE ''
     END as NOCRLF1,
CASE WHEN maintab.GrossInterest<>'' and maintab.GrossInterest<>'0'
     THEN cast(cast(maintab.GrossInterest as float) as varchar)
     ELSE ''
     END as COMMASUB,
CASE WHEN maintab.GrossInterest='' or maintab.GrossInterest<>'0'
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}'
From v_FI_INTR as maintab
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=384 and actflag='I'))
and Recdate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=384 and actflag='U'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq = 2)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and indefpay <>''
and indefpay = 'F'
order by caref2 desc, caref1, caref3
