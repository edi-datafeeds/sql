--SEMETABLE=client.dbo.seme_daiwa
--FileName=edi_YYYYMMDD
--DVCA_INS_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=7900
--messterminator=-}
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(DivNotes as varchar(255)))>40 then DivNotes else rtrim(char(32)) end As Notes FROM DIV WHERE DIVID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
paydate as MPAYLINK,
paydate2 as MPAYLINK,
maintab.cntrycd as MPAYLINK3,
case when 1=1 then (select '{1:F01EDILTD2LAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EXOSUS33AXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'9'+EXCHGID + cast(divpy.optionserialno as char(1)) as CAREF1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C' or divpy.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D' or divpy.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVCA' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(Localcode,'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='XJPX' THEN ':94B::PLIS//XTKS' WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT(varchar , DeclarationDate,112)
     ELSE ''
     END,
CASE WHEN maintab.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT(varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN maintab.DuebillsRedemDate IS NOT NULL
     THEN ':98A::MCTD//'+CONVERT(varchar , DuebillsRedemDate,112)
     ELSE ''
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and maintab.RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT(varchar, RegistrationDate,112)
     WHEN maintab.Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT(varchar, recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN maintab.Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN maintab.Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN maintab.Marker='INS'
     THEN ':22F::DIVI//SPEC'
     WHEN maintab.Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN maintab.Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN maintab.Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN divpy.curencd<>'' AND divpy.curencd IS NOT NULL
     THEN ':11A::OPTN//' + divpy.curencd ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN divpy.InstallmentPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT (varchar, divpy.InstallmentPaydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE 
     WHEN divpy.approxflag='T'
     THEN ''
     WHEN divpy.Netdividend<>'' AND divpy.Netdividend IS NOT NULL
     THEN ':92F::NETT//'+ divpy.curencd+ltrim(cast(divpy.Netdividend as char(15)))
     WHEN divpy.Grossdividend<>'' AND  divpy.Grossdividend IS NOT NULL
     THEN ''
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE 
     WHEN divpy.approxflag='T' and divpy.Grossdividend <> '' AND  divpy.Grossdividend IS NOT NULL
     THEN ':92F::PDIV//'+ divpy.curencd+ltrim(cast(divpy.Grossdividend as char(15)))
     WHEN divpy.Grossdividend<>'' AND  divpy.Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ divpy.curencd+ltrim(cast(divpy.Grossdividend as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}'
From v_EQ_DV_CA_SE_OP as maintab
left outer join v10s_divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid and 'D'<>mainoptn.actflag and 'C'<>mainoptn.actflag
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag and 'C'<>divopt3.actflag
left outer join divpy  on maintab.eventid = divpy.divid and 'C'=divpy.divtype
WHERE
mainTFB1<>''
and divpy.optionserialno<>1
and (((maintab.isin in (select code from client.dbo.pfisin where accid=397 and actflag='I'))
and (isnull(exdate,'2001/01/01') > getdate()-1 or isnull(paydate,'2001/01/01') > getdate()-1))
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=397 and actflag='U'))
and changed>=(select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq = 2)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and divpy.optionserialno<>1
and not (divperiodcd='ISC' or marker='ISC')
and mainoptn.divid is not null 
and mainoptn.divtype='C'
and divopt2.divid is null 
and divopt3.divid is null
order by caref2 desc, caref1, caref3
