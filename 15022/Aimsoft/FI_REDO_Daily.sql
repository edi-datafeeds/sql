--SEMETABLE=client.dbo.seme_capdublin
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_REDO
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct top 20
Changed,
'select case when len(cast(CurrdNotes as varchar(24)))=22 then rtrim(char(32)) else CurrdNotes end As Notes FROM CURRD WHERE CurrdID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NEQ}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '325'+EXCHGID+cast(Seqnum as char(1))
     ELSE '325'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN mainTFB2 <>''
     THEN ':35B:' + mainTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
CASE WHEN NewCurenCD <> ''
     THEN ':11A::DENO//'+NewCurenCD
     END,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
CASE WHEN NewCurenCD=Current_Currency and MinimumDenomination<>''
    THEN ':36B::MINO'+MinimumDenomination
    END,
CASE WHEN newparvalue <>''
            AND oldparvalue <>''
     THEN ':92D::NEWO//'+rtrim(cast(newparvalue as char (15)))+
                    '/'+rtrim(cast(oldparvalue as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_REDO as maintab
WHERE
mainTFB1<>''
and NewCurenCD <>''
and Actflag  <>''
/* and (((maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=996 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=996 and actflag='I'))
and EffectiveDate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=996 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=996 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=996 and actflag='U'))
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))) */
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by caref2 desc, caref1, caref3
