--SEMETABLE=client.dbo.seme_i4
--FileName=edi_YYYYMMDD
--PRIO_EQ564_YYYYMMDD
--FileNameNotes=edi_YYYYMMDD_MT568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select PrfNotes As Notes FROM PRF WHERE PrfID = '+ cast(maintab.PrfID as char(16)) as ChkNotes, 
'{1:F01EDII4XXXXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Exdate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '110' END as CAref1, */
'110' as CAref1,
maintab.PrfID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//GENL',
':22F::CAEV//PRIO' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(maintab.cfi)=6 THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASE',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD

     ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN maintab.EndSubscription<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN maintab.EndSubscription<>''
       AND maintab.StartSubscription<>''
      THEN ':69A::PWAL//'
          +CONVERT ( varchar , maintab.StartSubscription,112)+'/'
          +CONVERT ( varchar , maintab.EndSubscription,112)
     WHEN maintab.StartSubscription<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , maintab.StartSubscription,112)+'/UKWN'
     WHEN maintab.EndSubscription<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , maintab.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN isnull(resisin,'')<>'' THEN ':35B:ISIN ' + resisin
     else '' end,
case when isnull(resisin,'')='' then ':35B:/US/999999999' else '' end,
case when isnull(resisin,'')='' then 'NOT AVAILABLE AT PRESENT' else '' end,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN maintab.RatioNew<>'' AND maintab.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(maintab.RatioNew as char (15)))+
                     '/'+ltrim(cast(maintab.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN maintab.MaxPrice<>''
     THEN ':90B::PRPP//ACTU/'+maintab.curencd+rtrim(cast(maintab.MaxPrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN maintab.MaxPrice<>''
     THEN ':90B::PRPP//ACTU/'+maintab.CurenCD+
          +substring(maintab.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes70F,
'-}$'
From v_EQ_PRIO_PRF_PrfID as maintab
/* left outer join client.dbo.seme_i4 as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'PRIO'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and maintab.changed>'2020/06/17 01:00:00'
and maintab.changed<'2020/06/17 07:00:00'
order by caref2 desc, caref1, caref3
