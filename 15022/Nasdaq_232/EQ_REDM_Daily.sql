--SEMETABLE=client.dbo.seme_nasdaq
--FileName=edi_YYYYMMDD
--REDM_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1

use wca
select
Changed,
'select RedemNotes As Notes FROM REDEM WHERE RedemID = '+ cast(EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'410'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDM',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='XJPX' THEN ':94B::PLIS//XTKS' WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN RedemPercent <> ''
     THEN ':90B::REDM//PRCT/' + CONVERT ( varchar , RedemPercent,112)
     WHEN RedemPrice is not null
     THEN ':90B::REDM//ACTU/' + CurenCD + CONVERT ( varchar , RedemPrice,112)
     ELSE ':90F::REDM//PRCT/UKWN'
     END as COMMASUB,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN RedemDate <>'' AND RedemDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN RedemPercent <>''
     THEN ':90A::OFFR//PRCT/'+RedemPercent
     ELSE ':90A::OFFR//PRCT/100,'
     END as commasub,
':16S:CASHMOVE',
':16S:CAOPTN',
/* '' as Notes, */ 
'-}$'
From v_EQ_REDM
WHERE
mcd<>'' and mcd is not null
and sedol in (select code from client.dbo.pfsedolmic where accid=232 and actflag<>'D' and (mic=mcd or mic not in (select mic from exchg) or (mic='XTKS' and mcd='XJPX')))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and redemdate <>''
and redemdate is not null
and redemdate <> '1800/01/01'
order by caref2 desc, caref1, caref3
