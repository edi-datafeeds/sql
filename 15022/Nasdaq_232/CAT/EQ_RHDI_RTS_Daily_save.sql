--SEMETABLE=client.dbo.seme_nasdaq
--FileName=edi_YYYYMMDD
--RHDI_RTS_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select  RtsNotes As Notes FROM RTS WHERE RDID = '+ cast(EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'160'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHDI',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN Recdate <>'' AND Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':22F::SELL//RENO',
':22F::RHDI//EXRI',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
':17B::DFLT//Y',
CASE WHEN EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN EndTrade <> '' 
       AND StartTrade <> '' 
     THEN ':69A::REVO//'
          +CONVERT ( varchar , StartTrade,112) + '/'
          +CONVERT ( varchar , EndTrade,112)
     WHEN StartTrade <> '' 
     THEN ':69C::REVO//'
          +CONVERT ( varchar , StartTrade,112) + '/UKWN'
     WHEN EndTrade <> '' 
     THEN ':69E::REVO//UKWN/'
          +CONVERT ( varchar , EndTrade,112)
     ELSE ':69J::REVO//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN traTFB2<>'' and traTFB2<>'UKWN'
     THEN ':35B:' + traTFB2
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew <>''
               AND RatioOld <>''
     AND RatioNew is not null
               AND RatioOld is not null
     THEN ':92D::ADEX//'+ltrim(cast(RatioNew as char (15)))+
                    '/'+ltrim(cast(RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN StartTrade <>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From v_EQ_RHDI_RTS
WHERE 
mcd<>'' and mcd is not null
and changed>'2018/04/13' and changed<'2018/04/14' and datepart(hh,changed)>11
and (sedol in (select code from client.dbo.pfsedolmic where accid = 232 and actflag<>'D' and (mic=mcd or mic not in (select mic from exchg))))
order by caref2 desc, caref1, caref3