--SEMETABLE=client.dbo.seme_nasdaq
--FileName=edi_YYYYMMDD
--EXOF_CTX_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'127'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
CASE WHEN EndDate <>'' AND EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndDate <>'' AND EndDate IS NOT NULL
       AND StartDate <>'' AND StartDate IS NOT NULL
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/'
          +CONVERT ( varchar , EndDate,112)
     WHEN StartDate <>'' AND StartDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/UKWN'
     WHEN EndDate <>'' AND EndDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB2<>'UKWN' and resTFB2<>''
     THEN ':35B:' + resTFB2
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':92D::NEWO//1,/1,',
':92K::NEWO//UKWN',
CASE WHEN StartDate <>'' AND StartDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From v_EQ_EXOF_CTX
WHERE 
mcd<>'' and mcd is not null
and changed>'2018/04/13' and changed<'2018/04/14' and datepart(hh,changed)>11
and (sedol in (select code from client.dbo.pfsedolmic where accid = 232 and actflag<>'D' and (mic=mcd or mic not in (select mic from exchg))))
order by caref2 desc, caref1, caref3