--SEMETABLE=client.dbo.seme_nasdaq
--FileName=edi_YYYYMMDD
--DECR_CAPRD_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select  CapRdNotes As Notes FROM CAPRD WHERE CAPRDID = '+ cast(EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'148'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='XJPX' THEN ':94B::PLIS//XTKS' WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN effectivedate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT/'+NewParValue as commasub,
':16S:CADETL',
CASE WHEN rcapid is not null THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN rcapid is not null THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22F::CAOP//CASH' ELSE '' END,
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
CASE WHEN rcapid is not null THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16R:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN CSPYDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     WHEN rcapid is not null THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN CashBak<>'' AND CashBak IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+CurenCD+substring(CashBak,1,15)
     WHEN rcapid is not null THEN ':90E::OFFR//UKWN'
     ELSE ''
     END as Commasub,
CASE WHEN rcapid is not null THEN ':16S:CASHMOVE' ELSE '' END,
CASE WHEN rcapid is not null THEN ':16S:CAOPTN' ELSE '' END,
/* '' as Notes, */
'-}$'
From v_EQ_DECR_CAPRD
WHERE 
mcd<>'' and mcd is not null
and sedol in (select code from client.dbo.pfsedolmic where accid=232 and actflag<>'D' and (mic=mcd or mic not in (select mic from exchg) or (mic='XTKS' and mcd='XJPX')))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and rtrim(OldParValue)<>'' and rtrim(NewParValue)<>''
and rtrim(OldParValue)<>'0' and rtrim(NewParValue)<>'0'
and cast(rtrim(OldParValue) as float(16,7))>cast(rtrim(NewParValue) as float(16,7))and (mcd<>'' and mcd is not null)
order by caref2 desc, caref1, caref3
