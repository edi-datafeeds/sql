--SEMETABLE=client.dbo.seme_nasdaq
--FileName=edi_YYYYMMDD
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--sEvent=DIV
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select    
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
cntrycd as MPAYLINK3,
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
':23G:NEWM' AS MessageStatus,
':22F::CAEV//DV' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END, 
':16S:CADETL',
'' as StartseqE,
/* '' as Notes, */
'-}$'
From v_EQ_DV_CA_SE_OP
left outer join wca.dbo.divpy as divopt1 on v_EQ_DV_CA_SE_OP.eventid = divopt1.divid and 1=divopt1.optionid
left outer join wca.dbo.divpy as divopt2 on v_EQ_DV_CA_SE_OP.eventid = divopt2.divid and 2=divopt2.optionid
left outer join wca.dbo.divpy as divopt3 on v_EQ_DV_CA_SE_OP.eventid = divopt3.divid and 3=divopt3.optionid
WHERE 
mcd<>'' and mcd is not null
and ((changed>'2016/05/02 01:00:00' and changed<'2016/05/02 17:00:00') or changed>'2016/05/20 01:00:00')
and (sedol in (select code from client.dbo.pfsedolmic where accid = 232 and actflag<>'D' and (mic=mcd or mic not in (select mic from exchg))))
and not (v_EQ_DV_CA_SE_OP.divperiodcd='ISC' and divopt2.divid is null and divopt3.divid is null)
and (paydate>getdate()-183 or paydate is null)
and not (divopt1.divid is null and divopt2.divid is null and divopt3.divid is null)
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=v_EQ_DV_CA_SE_OP.AnnounceDate))
order by caref2 desc, caref1, caref3
