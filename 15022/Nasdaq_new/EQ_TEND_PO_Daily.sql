--SEMETABLE=client.dbo.seme_nasdaq
--FileName=edi_YYYYMMDD
--TEND_PO_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select  PONotes As Notes FROM PO WHERE RDID = '+ cast(EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'132'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN OfferOpens <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , OfferOpens,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN OfferCloses <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , OfferCloses,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN OfferCloses <> ''
       AND OfferOpens <> ''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , OfferOpens,112)+'/'
          +CONVERT ( varchar , OfferCloses,112)
     WHEN OfferOpens <> ''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , OfferOpens,112)+'/UKWN'
     WHEN OfferCloses <> ''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , OfferCloses,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN TndrStrkPrice <> '' 
     THEN ':90B::OFFR//ACTU/'+CurenCD+
          +substring(TndrStrkPrice,1,15)
     WHEN MaxPrice <>'' 
     THEN ':90B::OFFR//ACTU/'+CurenCD+
          +substring(MaxPrice,1,15)     
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From wca2.dbo.i_EQ_TEND_PO 
WHERE 
mcd<>'' and mcd is not null
and (sedol in (select code from client.dbo.pfsedolmic where accid = 232 and actflag<>'D' and (mic=mcd or mic not in (select mic from exchg))))
order by caref2 desc, caref1, caref3