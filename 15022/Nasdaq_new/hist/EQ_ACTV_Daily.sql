--SEMETABLE=client.dbo.seme_nasdaq
--FileName=edi_YYYYMMDD
--ACTV_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\Upload\Acc\232\feed\
--FileTidy=n
--TFBNUMBER=2
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select  Reason As Notes FROM LSTAT WHERE LSTATID = '+ cast(EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'150'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ACTV',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN MCD <>'' and Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     ELSE ':94B::PLIS//SECM'
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN notificationdate<>'' and NotificationDate is not null
     THEN ':98A::ANOU//'+CONVERT ( varchar , NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN effectivedate<>'' and EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
/* '' as Notes, */
'-}$'
From v_EQ_ACTV
WHERE 
sedol<>''
and exchgid<>'' and exchgid is not null
and Effectivedate>getdate()-8
and (sedol in (select code from client.dbo.pfsedolmic where accid = 232 and actflag='I' and (mic=mcd or mic not in (select mic from exchg))))
and LStatStatus = 'R'
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
