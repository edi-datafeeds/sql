--SEMETABLE=client.dbo.seme_freetrade
--FileName=edi_YYYYMMDD
--TEND_TKOVR_PREC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\investec\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'131'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.eventid as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN' THEN '/GB/' + Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN Opendate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::WUCO//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN UnconditionalDate IS NOT NULL and UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+replace(substring(maintab.Offerorname,1,23),'�','e')
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
':11A::OPTN//USD',
':17B::DFLT//N',
CASE WHEN Closedate<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , Closedate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN Closedate<>'' AND Opendate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Opendate,112)+'/'
          +CONVERT ( varchar , Closedate,112)
     WHEN Opendate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Opendate,112)+'/UKWN'
     WHEN Closedate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Closedate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN MaxAcpQty <>''
     THEN ':36B::FOLQ//UNIT/'+MaxAcpQty+','
     ELSE ':36C::FOLQ//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
':98B::PAYD//UKWN',
':90E::OFFR//UKWN',
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_EQ_TEND_TKOVR as maintab
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'TKOVR' = wca.dbo.mpay.sevent
                                         and 'D'<>wca.dbo.mpay.actflag
WHERE
maintab.isin<>'' and maintab.isin is not null
and (maintab.closedate is not null 
     and maintab.opendate is not null
     and maintab.minitkovr<>'T')
and ((maintab.isin in (select code from client.dbo.pfisin where accid=376 and actflag='I')
and Closedate > getdate())
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=376 and actflag='U')
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and wca.dbo.mpay.eventid is null
order by caref2 desc, caref1, caref3
