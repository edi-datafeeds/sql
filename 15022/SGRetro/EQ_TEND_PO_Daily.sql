--SEMETABLE=client.dbo.seme_sharegain
--FileName=edi_YYYYMMDD
--TEND_PO_EQ564_YYYYMMDD
--FileNameNotes=edi_YYYYMMDD_MT568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq=3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  PONotes As Notes FROM PO WHERE RDID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01SHAREGAINXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.OfferCloses>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.OfferCloses is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '132' END as CAref1, */
'132' as CAref1,
EventID as CAREF2,
bbemain.BbeID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//GENL',
':22F::CAEV//TEND' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
'/FG/'+bbemain.bbgexhid,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
'/TS/' + replace(replace(replace(bbemain.bbgexhtk,'%','PCT'),'&','and'),'_',' ') as TIDYTEXT,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN OfferOpens <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , OfferOpens,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN maintab.CurenCD <> '' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN OfferCloses <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , OfferCloses,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN OfferCloses <> ''
       AND OfferOpens <> ''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , OfferOpens,112)+'/'
          +CONVERT ( varchar , OfferCloses,112)
     WHEN OfferOpens <> ''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , OfferOpens,112)+'/UKWN'
     WHEN OfferCloses <> ''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , OfferCloses,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN TndrStrkPrice <> '' 
     THEN ':90B::OFFR//ACTU/'+maintab.CurenCD+
          +substring(TndrStrkPrice,1,15)
     WHEN MaxPrice <>'' 
     THEN ':90B::OFFR//ACTU/'+maintab.CurenCD+
          +substring(MaxPrice,1,15)     
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes70F,
'-}$'
From v_EQ_TEND_PO as maintab
inner join wca.dbo.bbe as bbemain on maintab.secid=bbemain.secid and maintab.exchgcd=bbemain.exchgcd and 'D'<>bbemain.actflag
/* left outer join client.dbo.seme_sharegain as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'TEND'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and ((bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='I')
      and maintab.OfferCloses > getdate())
OR
(bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='U')
and maintab.changed>'2020/06/16' and maintab.changed<'2020/06/17'))
order by caref2 desc, caref1, caref3