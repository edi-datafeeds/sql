--SEMETABLE=client.dbo.seme_sharegain
--FileName=edi_YYYYMMDD
--CHAN_EQ564_YYYYMMDD
--FileNameNotes=edi_YYYYMMDD_MT568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq=3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select IschgNotes As Notes FROM ISCHG WHERE ISCHGID = '+ cast(EventID as char(16)) as ChkNotes, 
'{1:F01SHAREGAINXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.NameChangeDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.NameChangeDate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '153' END as CAref1, */
'153' as CAref1,
EventID as CAREF2,
bbemain.BbeID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//GENL',
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
'/FG/'+bbemain.bbgexhid,
substring(IssOldname,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
'/TS/' + replace(replace(replace(bbemain.bbgexhtk,'%','PCT'),'&','and'),'_',' ') as TIDYTEXT,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.announcedate,112) ELSE '' END,
CASE WHEN NameChangeDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//NAME',
':70E::NAME//' + substring(replace(IssNewname,'&','and'),1,35) as TIDYTEXT,
':16S:CADETL',
CASE WHEN newisin <>'' THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN newisin <>'' THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN newisin <>'' THEN ':22F::CAOP//SECU' ELSE '' END,
CASE WHEN newisin <>'' THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16R:SECMOVE' ELSE '' END,
CASE WHEN newisin <>'' THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN newisin <>'' THEN ':35B:ISIN ' + newisin ELSE '' END,
CASE WHEN newisin <>'' THEN ':16R:FIA' ELSE '' END,
CASE WHEN newisin <>'' THEN ':94B::PLIS//EXCH/SECM' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16S:FIA' ELSE '' END,
CASE WHEN newisin <>'' THEN ':92D::NEWO//1,/1,' ELSE '' END,
CASE WHEN newisin <>'' and icceffectivedate is not null 
     THEN ':98A::PAYD//'+CONVERT ( varchar , icceffectivedate,112)
     WHEN newisin <>'' and icceffectivedate is null 
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN newisin <>'' THEN ':16S:SECMOVE' ELSE '' END,
CASE WHEN newisin <>'' THEN ':16S:CAOPTN' ELSE '' END,
'' as Notes70F,
'-}$'
From v_EQ_CHAN_NAME as maintab
inner join wca.dbo.bbe as bbemain on maintab.secid=bbemain.secid and maintab.exchgcd=bbemain.exchgcd and 'D'<>bbemain.actflag
/* left outer join client.dbo.seme_sharegain as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CHAN'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and ((bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='I')
      and (maintab.NameChangeDate > getdate() or maintab.icceffectivedate > getdate()))
OR
(bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='U')
and maintab.changed>'2020/06/16' and maintab.changed<'2020/06/17'))
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
