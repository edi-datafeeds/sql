--SEMETABLE=client.dbo.seme_sharegain
--FileName=edi_YYYYMMDD
--CHAN_LTCHG_EQ564_YYYYMMDD
--FileNameNotes=edi_YYYYMMDD_MT568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq=3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'{1:F01SHAREGAINXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.EffectiveDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.EffectiveDate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '199' END as CAref1, */
'199' as CAref1,
EventID as CAREF2,
bbemain.BbeID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//GENL',
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
'/FG/'+bbemain.bbgexhid,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
'/TS/' + replace(replace(replace(bbemain.bbgexhtk,'%','PCT'),'&','and'),'_',' ') as TIDYTEXT,
'/TS/' + replace(bbemain.bbgexhtk,'%','PCT'),substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.announcedate,112) ELSE '' END,
CASE WHEN EffectiveDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//TERM',
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//RE/TRADABLE UNIT CHANGE',
'OLD 1UNIT/'+cast(maintab.oldlot as varchar(20))+'SHS',
'NEW 1UNIT/'+cast(maintab.newlot as varchar(20))+'SHS',
':16S:ADDINFO',
'' as Notes70F,
'-}$'
From v_EQ_CHAN_LTCHG as maintab
inner join wca.dbo.bbe as bbemain on maintab.secid=bbemain.secid and maintab.exchgcd=bbemain.exchgcd and 'D'<>bbemain.actflag
/* left outer join client.dbo.seme_sharegain as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CHAN'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and ((bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='I')
      and maintab.effectivedate > getdate())
OR
(bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='U')
and maintab.changed>'2020/06/16' and maintab.changed<'2020/06/17'))
and maintab.oldlot is not null
and maintab.newlot is not null
and maintab.eventtype<>'CLEAN'
and maintab.eventtype<>'CORR'
and maintab.oldlot<>maintab.newlot
order by caref2 desc, caref1, caref3
