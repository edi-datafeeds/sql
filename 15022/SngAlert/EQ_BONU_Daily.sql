--SEMETABLE=client.dbo.seme_sngalert
--FileName=edi_YYYYMMDD
--BONU_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select distinct
Changed,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'114' as CAref1,
EventID as CAREF2,
maintab.Sedolid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BONU' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/'+upper(lse.opol) END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
CASE when pvcurrency is null then '' WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate, 112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.secid<>maintab.ressecid and resTFB1<>''
     THEN ':35B:' + resTFB1
     WHEN maintab.secid<>maintab.ressecid and resTFB1=''
     THEN ':35B:UKWN'
     ELSE ':35B:' + mainTFB2
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew<>''
          AND RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(RatioNew as char (15)))+
                    '/'+ltrim(cast(RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_BONU as maintab
inner join smf4.dbo.security as lse on maintab.sedol = lse.sedol
WHERE 
((maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='I')
and exdate between getdate()-10 and getdate()+1000)
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='U')
and (changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
or exdate between getdate()-1 and getdate())))
and not (sectycd='DR' and lapsedpremium<>'')
and CalcListdate<=maintab.announcedate
and CalcDelistdate>=maintab.announcedate
order by caref2 desc, caref1, caref3
