--SEMETABLE=client.dbo.seme_sngalert
--FileName=edi_YYYYMMDD
--ODLT_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=ODDLT
--TFBNUMBER=1
--INCREMENTAL=

--# 1
use wca
select distinct
Changed,
'select distinct * from V10s_MPAY'
+ ' where (Actflag='+char(39)+'I'+char(39)+ ' or Actflag='+char(39)+'U'+char(39)
+') and eventid=' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent='+ char(39) + 'ODDLT'+ char(39)
+ ' and Paytype<>'+ char(39) + 'D'+ char(39)
+ ' Order By OptionID,SerialID' as MPAYlink,
maintab.paydate as PAYLINK,
Startdate as OPENLINK,
Enddate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'163' as CAref1,
maintab.EventID as CAREF2,
maintab.Sedolid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ODLT' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/'+upper(lse.opol) END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
CASE when pvcurrency is null then '' WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.mpay.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartSeqE,
'-}$'
From v_EQ_ODLT as maintab
inner join smf4.dbo.security as lse on maintab.sedol = lse.sedol and maintab.mcd = lse.opol
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'ODDLT' = wca.dbo.mpay.sevent
                                             and 'D'=wca.dbo.mpay.paytype and 'D'<>wca.dbo.mpay.actflag
WHERE 
(maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='I')
and exdate between getdate()-10 and getdate()+1000)
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='U')
and (changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
or exdate between getdate()-1 and getdate()))
order by caref2 desc, caref1, caref3
