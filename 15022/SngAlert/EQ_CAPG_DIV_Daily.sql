--SEMETABLE=client.dbo.seme_sngalert
--FileName=edi_YYYYMMDD
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\2012_Citibank_i\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(DivNotes as varchar(255)))>40 then DivNotes else rtrim(char(32)) end As Notes FROM DIV WHERE DIVID = '+ cast(EventID as char(16)) as ChkNotes, 
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'167' as CAref1,
EventID as CAREF2,
maintab.Sedolid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D' or mainoptn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C' or mainoptn.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEP//DISN',
':22F::CAEV//CAPG' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/'+upper(lse.opol) END,
CASE WHEN maintab.cfi <> '' THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
CASE when maintab.pvcurrency is null then '' WHEN maintab.pvcurrency<>'' THEN ':11A::DENO//' + maintab.pvcurrency ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     ELSE ''
     END,
CASE WHEN ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':22F::DIVI//SPEC',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
CASE WHEN mainoptn.divtype ='S' 
     THEN ':22F::CAOP//SECU'
     ELSE ':22F::CAOP//CASH'
     END,
CASE WHEN mainoptn.CurenCD<>'' AND mainoptn.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + mainoptn.CurenCD
     ELSE ''
     END,
CASE WHEN mainoptn.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN mainoptn.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN mainoptn.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN mainoptn.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN mainoptn.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN mainoptn.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN mainoptn.defaultopt ='T'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN mainoptn.divtype ='S'
     THEN ':16R:SECMOVE'
     ELSE ':16R:CASHMOVE'
     END,
':22H::CRDB//CRED',
CASE WHEN mainoptn.divtype ='C' and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT (varchar,  paydate, 112)
     WHEN mainoptn.divtype ='C'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN maintab.marker='CGL' and mainoptn.divtype ='C' and mainoptn.Grossdividend<>'' AND mainoptn.Grossdividend IS NOT NULL
     THEN ':92J::GRSS//LTCG/'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))+ '/ACTU'
     WHEN maintab.marker='CGS' and mainoptn.divtype ='C' and mainoptn.Grossdividend<>'' AND mainoptn.Grossdividend IS NOT NULL
     THEN ':92J::GRSS//STCG/'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))+ '/ACTU'
     WHEN mainoptn.divtype ='C' and mainoptn.Grossdividend<>'' AND mainoptn.Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))
     WHEN mainoptn.divtype ='C'
     THEN ':92K::GRSS//UKWN' 
     ELSE '' 
     END as COMMASUB,
CASE WHEN mainoptn.divtype ='S' and resSCMST.isin<>''
     THEN ':35B:ISIN ' + resSCMST.isin
     WHEN mainoptn.divtype ='S'
     THEN ':35B:UKWN'
     ELSE ''
     END,
CASE WHEN mainoptn.divtype ='S'
     THEN ':16R:FIA'
     ELSE ''
     END,
CASE WHEN mainoptn.divtype ='S'
     THEN ':94B::PLIS//SECM'
     ELSE ''
     END,
CASE WHEN mainoptn.divtype ='S'
     THEN ':16S:FIA'
     ELSE ''
     END,
CASE WHEN mainoptn.divtype ='S' and mainoptn.RatioNew<>'' AND mainoptn.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(mainoptn.RatioNew as char (15)))+
                    '/'+ltrim(cast(mainoptn.RatioOld as char (15))) 
     WHEN mainoptn.divtype ='S'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN mainoptn.divtype ='S' and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     WHEN mainoptn.divtype ='S'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN mainoptn.divtype = 'S'
     THEN ':16S:SECMOVE'
     ELSE ':16S:CASHMOVE'
     END,
':16S:CAOPTN',
CASE WHEN divopt2.divtype = 'C' or  divopt2.divtype = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'C' or  divopt2.divtype = 'S'
     THEN ':13A::CAON//002'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'S' 
     THEN ':22F::CAOP//SECU'
     WHEN divopt2.divtype = 'C' 
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN divopt2.CurenCD<>'' AND divopt2.CurenCD IS NOT NULL and divopt2.divtype <> 'B'
     THEN ':11A::OPTN//' + divopt2.CurenCD
     ELSE ''
     END,
CASE WHEN divopt2.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN divopt2.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN divopt2.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN divopt2.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN divopt2.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN divopt2.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN (divopt2.divtype = 'C' or  divopt2.divtype = 'S') and divopt2.defaultopt = 'T'
     THEN ':17B::DFLT//Y'
     WHEN (divopt2.divtype = 'C' or  divopt2.divtype = 'S') and divopt2.defaultopt = 'F'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'S'
     THEN ':16R:SECMOVE'
     WHEN divopt2.divtype = 'C'
     THEN ':16R:CASHMOVE'
     ELSE ''
     END,
CASE WHEN divopt2.divid is not null and divopt2.divtype <> 'B'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'C' and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT (varchar,  paydate, 112)
     WHEN divopt2.divtype ='C'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN maintab.marker='CGL' and divopt2.divtype ='C' and divopt2.Grossdividend<>'' AND divopt2.Grossdividend IS NOT NULL
     THEN ':92J::GRSS//LTCG/'+ divopt2.CurenCD+ltrim(cast(divopt2.Grossdividend as char(15)))+ '/ACTU'
     WHEN maintab.marker='CGS' and divopt2.divtype ='C' and divopt2.Grossdividend<>'' AND divopt2.Grossdividend IS NOT NULL
     THEN ':92J::GRSS//STCG/'+ divopt2.CurenCD+ltrim(cast(divopt2.Grossdividend as char(15)))+ '/ACTU'
     WHEN divopt2.divtype ='C' and divopt2.Grossdividend<>'' AND divopt2.Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ mainoptn.CurenCD+ltrim(cast(mainoptn.Grossdividend as char(15)))
     WHEN divopt2.divtype ='C'
     THEN ':92K::GRSS//UKWN' 
     ELSE '' 
     END as COMMASUB,
CASE WHEN divopt2.divtype = 'S' and opt2resSCMST.isin<>''
     THEN ':35B:ISIN ' + opt2resSCMST.isin
     WHEN divopt2.divtype = 'S'
     THEN ':35B:UKWN'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'S'
     THEN ':16R:FIA'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'S'
     THEN ':94B::PLIS//SECM'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'S'
     THEN ':16S:FIA'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'S' and divopt2.RatioNew<>'' AND divopt2.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(divopt2.RatioNew as char (15)))+
                    '/'+ltrim(cast(divopt2.RatioOld as char (15))) 
     WHEN divopt2.divtype = 'S'
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN divopt2.divtype = 'S' and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     WHEN divopt2.divtype = 'S'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN divopt2.divtype = 'S'
     THEN ':16S:SECMOVE'
     WHEN divopt2.divtype = 'C'
     THEN ':16S:CASHMOVE'
     ELSE ''
     END,
CASE WHEN divopt2.divid is not null and divopt2.divtype <> 'B'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* '' as Notes, */ 
'-}$'
From v_EQ_DV_CA_SE_OP as maintab
inner join smf4.dbo.security as lse on maintab.sedol = lse.sedol
left outer join v10s_divpy as mainoptn on maintab.eventid = mainoptn.divid
                   and 1=mainoptn.optionid
left outer join scmst as ResSCMST ON mainoptn.ResSecID = ResSCMST.SecID
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid and 2=divopt2.optionid and 'C'<>divopt2.actflag and 'D'<>divopt2.actflag
left outer join scmst as opt2ResSCMST ON divopt2.ResSecID = opt2ResSCMST.SecID
left outer join wca.dbo.drip on maintab.eventid=wca.dbo.drip.divid
WHERE 
((maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='I')
and exdate between getdate()-10 and getdate()+1000)
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='U')
and (changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
or exdate between getdate()-1 and getdate())))
and mainoptn.divid is not null 
and (marker='CG' or marker='CGL' or marker='CGS')
and (paydate>getdate()-183 or paydate is null)
order by caref2 desc, caref1, caref3
