--SEMETABLE=client.dbo.seme_sngalert
--FileName=edi_YYYYMMDD
--DVCA_CHOS_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\Citibank\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DIV
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
maintab.frankdiv as FLFR,
maintab.unfrankdiv as UNFR,
case when maintab.StructCD<>'XREIT' then '' else maintab.StructCD end as structcd,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'100' as CAref1,
EventID as CAREF2,
maintab.Sedolid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVOP' as CAEV,
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/'+upper(lse.opol) END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     ELSE ''
     END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
'' as StartseqE,
':16R:CAOPTN',
':13A::CAON//00'
     +cast((select max(optionid)+1 from wca.dbo.divpy where wca.dbo.divpy.divid=maintab.EventID) as char(1)),
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB2,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN maintab.DripReinvPrice <>''
     THEN ':90B::PRPP//ACTU/'+maintab.DripCurenCD+ltrim(cast(maintab.DripReinvPrice as char(15)))
     ELSE ':90E::PRPP//UKWN' end as COMMASUB,
CASE WHEN maintab.DripPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT(varchar, maintab.DripPaydate,112)
     ELSE ':98B::PAYD//UKWN' end,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */ 
'-}$'
From v_EQ_DV_CA_SE_OP as maintab
inner join smf4.dbo.security as lse on maintab.sedol = lse.sedol
left outer join v10s_divpy as divopt1 on maintab.eventid = divopt1.divid 
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag
                   and 1=divopt1.optionid and 'C'<>divopt1.actflag
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid 
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid 
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
left outer join v10s_divpy as divopt4 on maintab.eventid = divopt4.divid 
                   and 4=divopt4.optionid and 'D'<>divopt4.actflag
                   and 4=divopt4.optionid and 'C'<>divopt4.actflag
left outer join v10s_divpy as divopt5 on maintab.eventid = divopt5.divid 
                   and 5=divopt5.optionid and 'D'<>divopt5.actflag
                   and 5=divopt5.optionid and 'C'<>divopt5.actflag
left outer join v10s_divpy as divopt6 on maintab.eventid = divopt6.divid 
                   and 6=divopt6.optionid and 'D'<>divopt6.actflag
                   and 6=divopt6.optionid and 'C'<>divopt6.actflag
left outer join v10s_divpy as divopt7 on maintab.eventid = divopt7.divid 
                   and 7=divopt7.optionid and 'D'<>divopt7.actflag
                   and 7=divopt7.optionid and 'C'<>divopt7.actflag
left outer join v10s_divpy as divopt8 on maintab.eventid = divopt8.divid 
                   and 8=divopt8.optionid and 'D'<>divopt8.actflag
                   and 8=divopt8.optionid and 'C'<>divopt8.actflag
WHERE 
((maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='I')
and exdate between getdate()-10 and getdate()+1000)
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=262 and actflag='U')
and (changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
or exdate between getdate()-1 and getdate())))
and not (maintab.dripcntrycd='CA' or maintab.dripcntrycd='GB' or maintab.dripcntrycd='ZA')
and maintab.dripcntrycd<>''
and not ((divopt1.divtype = 'C' or divopt1.actflag is null)
and (divopt2.divtype = 'C' or divopt2.actflag is null)
and (divopt3.divtype = 'C' or divopt3.actflag is null)
and (divopt4.divtype = 'C' or divopt4.actflag is null)
and (divopt5.divtype = 'C' or divopt5.actflag is null)
and (divopt6.divtype = 'C' or divopt6.actflag is null)
and (divopt7.divtype = 'C' or divopt7.actflag is null)
and (divopt8.divtype = 'C' or divopt8.actflag is null) 
)
and ((divopt1.actflag is not null and divopt2.actflag is not null)
  or (divopt1.actflag is not null and divopt3.actflag is not null)      
  or (divopt2.actflag is not null and divopt3.actflag is not null)
  or divopt1.divtype = 'B' or divopt2.divtype = 'B')
and not (divperiodcd='ISC' or marker='ISC' or marker='CG' or marker='CGL' or marker='CGS')
order by caref2 desc, caref1, caref3
