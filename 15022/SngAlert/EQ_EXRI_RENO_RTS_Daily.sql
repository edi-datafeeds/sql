--SEMETABLE=client.dbo.seme_sngalert
--FileName=edi_YYYYMMDD
--EXRI_RTS_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select getdate() as maxdate
--archive=off
--ArchivePath=n:\15022\2012_citibank_i\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct
Changed,
'select case when len(cast(RTSNotes as varchar(255)))>40 then RTSNotes else rtrim(char(32)) end As Notes FROM RTS WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
'118' as CAref1,
EventID as CAREF2,
maintab.Sedolid as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXRI' as CAEV,
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
':16R:LINK',
'160' as CAref4,
':16S:LINK',
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and maintab.sedol<>'' then '/GB/' + maintab.Sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
CASE WHEN lse.opol is null or lse.opol ='' or lse.opol='XXXX' or lse.opol='XFMQ' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/'+upper(lse.opol) END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
CASE when pvcurrency is null then '' WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN EndSubscription<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndSubscription<>''
       AND StartSubscription<>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/'
          +CONVERT ( varchar , EndSubscription,112)
     WHEN StartSubscription<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/UKWN'
     WHEN EndSubscription<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>'UKWN' and resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB2
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN EndTrade<>'' 
       AND StartTrade<>'' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/'
          +CONVERT ( varchar , EndTrade,112)
     WHEN StartTrade<>'' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/UKWN'
     WHEN EndTrade<>'' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
CASE WHEN NewExerciseRatio <>'' AND OldExerciseRatio <>'' and NewExerciseRatio <>'0' AND OldExerciseRatio <>'0'
     THEN ':92D::NEWO//'+rtrim(cast(NewExerciseRatio as char (15)))+
                     '/'+rtrim(cast(OldExerciseRatio as char (15))) 
     WHEN RatioNew <>'' AND RatioOld <>'' and RatioNew <>'0' AND RatioOld <>'0'
     THEN ':92D::NEWO//1,/1,'
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN StartTrade<>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN StartTrade<>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN IssuePrice<>''
     THEN ':90B::PRPP//ACTU/'+CurenCD+ltrim(cast(IssuePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//LAPS',
':17B::DFLT//Y',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From v_EQ_EXRI_RENO_RTS as maintab
inner join smf4.dbo.security as lse on maintab.sedol = lse.sedol
WHERE 
(maintab.parentsecid in (select secid from client.dbo.pfsedol inner join wca.dbo.sedol on code=sedol where accid=262 and client.dbo.pfsedol.actflag='I')
and EndSubscription between getdate()-10 and getdate()+1000)
OR
(maintab.parentsecid in (select secid from client.dbo.pfsedol inner join wca.dbo.sedol on code=sedol where accid=262 and client.dbo.pfsedol.actflag='U')
and (changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
or EndSubscription between getdate()-1 and getdate()))
order by caref2 desc, caref1, caref3

