--SEMETABLE=client.dbo.seme_maitland
--FileName=edi_YYYYMMDD
--ACTv_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\maitland\
--FileTidy=n
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select distinct
Changed,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'147' as CAref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ACTV',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':98B::ANOU//UKWN',
CASE WHEN EffectiveDate <>'' and effectivedate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'-}$'
From v_EQ_ACTV_NLIST
WHERE 
mainTFB1<>''
and changed>'2016/12/12 16:00:00' and changed<'2016/12/12 22:00:00'
and primaryexchgcd=exchgcd
and exchgid<>'' and exchgid is not null

     and (changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
          and (sedol in (select code from client.dbo.pfsedol where accid = 217 and actflag<>'D')
          or isin in (select code from client.dbo.pfisin where accid = 217 and actflag<>'D')
          or uscode in (select code from client.dbo.pfuscode where accid = 217 and actflag<>'D')))

order by caref2 desc, caref1, caref3
