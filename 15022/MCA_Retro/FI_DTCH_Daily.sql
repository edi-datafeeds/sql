--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_DTCH
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then rtrim(char(32)) else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '410'+EXCHGID+cast(Seqnum as char(1))
     ELSE '410'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//DTCH',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN InterestRate <> '' THEN ':92A::INTR//'+InterestRate ELSE '' END as COMMASUB,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DutchMaxPrice <> '' 
     THEN ':90B::MAXP//ACTU/'+CurenCD+
          +substring(DutchMaxPrice,1,15)
     ELSE ':90E::MAXP//UKWN'
     END as COMMASUB,
CASE WHEN DutchMinPrice <> '' 
     THEN ':90B::MINP//ACTU/'+CurenCD+
          +substring(DutchMinPrice,1,15)
     ELSE ':90E::MINP//UKWN'
     END as COMMASUB,
CASE WHEN TenderOfferor<>''
     then ':70E::OFFO//'+upper(replace(substring(TenderOfferor,1,35),'�','e'))
     ELSE ''
     END as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD 
     ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN TenderCloseDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN TenderOpenDate<>'' AND TenderCloseDate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , TenderOpenDate,112)+'/'
          +CONVERT ( varchar , TenderCloseDate,112)
     WHEN TenderOpenDate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , TenderOpenDate,112)+'/UKWN'
     WHEN TenderCloseDate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , TenderCloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CAOPTN',
'' as Notes,  
'-}$'
From v_FI_REDM as maintab
WHERE
mainTFB1<>''
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and maintab.changed>'2018/03/12 01:00:00' and maintab.changed<'2018/03/12 11:00:00'
and redemdate >getdate()-30
and redemdate <> '1800/01/01'
and (redemtype='BB' or redemtype='BBED' or redemtype='BBRD')
and DutchAuction='T'
and TenderOfferor=''
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
order by caref2 desc, caref1, caref3
