--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_REDO
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
v_FI_REDO.changed,
'select case when len(cast(CurrdNotes as varchar(24)))=22 then rtrim(char(32)) else CurrdNotes end As Notes FROM CURRD WHERE CurrdID = '+ cast(v_FI_REDO.EventID as char(16)) as ChkNotes,
'{1:F01MMCAUS22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '325'+EXCHGID+cast(Seqnum as char(1))
     ELSE '325'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_REDO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_FI_REDO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_REDO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_REDO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN oldparvalue<>'' THEN ':36B::MIEX//FAMT/'+oldparvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_REDO.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_REDO.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT'+newparvalue,
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v_FI_REDO.mainTFB2 <>''
     THEN ':35B:' + mainTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':11A::DENO//'+NewCurenCD,
':16S:FIA',
CASE WHEN v_FI_REDO.NewCurenCD=v_FI_REDO.Current_Currency and v_FI_REDO.MinimumDenomination<>''
    THEN ':36B::MINO'+v_FI_REDO.MinimumDenomination
    ELSE ''
    END,
':16S:SECMOVE',
':16S:CAOPTN',
/* '' as Notes, */ 
'-}$'
From v_FI_REDO
WHERE
mainTFB1<>''
and not (cntrycd='CA' or cntrycd='US')
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
and OldParvalue <>''
and NewParvalue <>''
and OldCurenCD <>''
and NewCurenCD <>''
and Actflag  <>''
order by caref2 desc, caref1, caref3
