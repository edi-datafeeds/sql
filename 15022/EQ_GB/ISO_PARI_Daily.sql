--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_PARI
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v52f_ISO_PARI.Changed,
'' as ISOHDR,
':16R:GENL',
'124'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v52f_ISO_PARI.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v52f_ISO_PARI.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v52f_ISO_PARI.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v52f_ISO_PARI.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PARI',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v52f_ISO_PARI.Sidname +' '+ v52f_ISO_PARI.Sid,
substring(v52f_ISO_PARI.Issuername,1,35) as TIDYTEXT,
substring(v52f_ISO_PARI.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v52f_ISO_PARI.Sedol <> '' and v52f_ISO_PARI.Sidname <> '/GB/'
     THEN '/GB/' + v52f_ISO_PARI.Sedol
     ELSE ''
     END,
CASE WHEN v52f_ISO_PARI.Localcode <> ''
     THEN '/TS/' + v52f_ISO_PARI.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v52f_ISO_PARI.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v52f_ISO_PARI.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v52f_ISO_PARI.AssimilationDate <>'' and v52f_ISO_PARI.AssimilationDate is not null
     THEN ':98A::PPDT//'+CONVERT ( varchar , v52f_ISO_PARI.AssimilationDate,112)
     ELSE ':98B::PPDT//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v52f_ISO_PARI.Ressid <> ''
     THEN ':35B:' + v52f_ISO_PARI.Ressidname +' '+ v52f_ISO_PARI.Ressid
     END,
/* CASE WHEN v52f_ISO_PARI.RatioNew <>''
            AND v52f_ISO_PARI.RatioNew IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(v52f_ISO_PARI.RatioNew as char (15)))+
                    '/'+rtrim(cast(v52f_ISO_PARI.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB, 
next line is a placebo, needs a ratioold,rationew added to parent */
':92K::NEWO//UKWN',
CASE WHEN v52f_ISO_PARI.AssimilationDate <>'' and v52f_ISO_PARI.AssimilationDate is not null
     THEN ':98A::PPDT//'+CONVERT ( varchar , v52f_ISO_PARI.AssimilationDate,112)
     ELSE ':98B::PPDT//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v52f_ISO_PARI
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
