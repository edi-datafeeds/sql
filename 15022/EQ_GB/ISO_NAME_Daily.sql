--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_NAME
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v50f_ISO_NAME.Changed,
'select  IschgNotes As Notes FROM ISCHG WHERE ISCHGID = '+ cast(v50f_ISO_NAME.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'153'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v50f_ISO_NAME.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v50f_ISO_NAME.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v50f_ISO_NAME.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v50f_ISO_NAME.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//NAME',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v50f_ISO_NAME.Sidname +' '+ v50f_ISO_NAME.Sid,
substring(v50f_ISO_NAME.IssOldname,1,35) as TIDYTEXT,
substring(v50f_ISO_NAME.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v50f_ISO_NAME.Sedol <> '' and v50f_ISO_NAME.Sidname <> '/GB/'
     THEN '/GB/' + v50f_ISO_NAME.Sedol
     ELSE ''
     END,
CASE WHEN v50f_ISO_NAME.Localcode <> ''
     THEN '/TS/' + v50f_ISO_NAME.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v50f_ISO_NAME.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v50f_ISO_NAME.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v50f_ISO_NAME.NameChangeDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v50f_ISO_NAME.NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':70E::NAME//' + substring(v50f_ISO_NAME.IssNewname,1,35) as TIDYTEXT,
':16S:CADETL',
'' as Notes,
'-}$'
From v50f_ISO_NAME
WHERE 
SID <> ''
and upper(eventtype)<>'CLEAN'
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
