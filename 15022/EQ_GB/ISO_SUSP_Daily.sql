--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_SUSP
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v52f_ISO_ACTV.Changed,
'select Reason As Notes FROM LSTAT WHERE LSTATID = '+ cast(v52f_ISO_ACTV.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'152'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v52f_ISO_ACTV.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v52f_ISO_ACTV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v52f_ISO_ACTV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v52f_ISO_ACTV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SUSP',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v52f_ISO_ACTV.Sidname +' '+ v52f_ISO_ACTV.Sid,
substring(v52f_ISO_ACTV.Issuername,1,35) as TIDYTEXT,
substring(v52f_ISO_ACTV.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v52f_ISO_ACTV.Sedol <> '' and v52f_ISO_ACTV.Sidname <> '/GB/'
     THEN '/GB/' + v52f_ISO_ACTV.Sedol
     ELSE ''
     END,
CASE WHEN v52f_ISO_ACTV.Localcode <> ''
     THEN '/TS/' + v52f_ISO_ACTV.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v52f_ISO_ACTV.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v52f_ISO_ACTV.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v52f_ISO_ACTV.NotificationDate <>'' and v52f_ISO_ACTV.NotificationDate is not null
     THEN ':98A::ANOU//'+CONVERT ( varchar , v52f_ISO_ACTV.NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN v52f_ISO_ACTV.EffectiveDate <>'' and v52f_ISO_ACTV.EffectiveDate is not null
     THEN ':98A::TSDT//'+CONVERT ( varchar , v52f_ISO_ACTV.EffectiveDate,112)
     ELSE ':98B::TSDT//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From v52f_ISO_ACTV
WHERE 
SID <> ''
and LStatStatus = 'S'
and upper(eventtype)<>'CLEAN'
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
