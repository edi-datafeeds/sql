--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_SPLR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v53f_ISO_DECR_CAPRD.Changed,
'select  CAPRDNotes As Notes FROM CAPRD WHERE CaprdID = '+ cast(v53f_ISO_DECR_CAPRD.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'123'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v53f_ISO_DECR_CAPRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v53f_ISO_DECR_CAPRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v53f_ISO_DECR_CAPRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v53f_ISO_DECR_CAPRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
CASE WHEN v53f_ISO_DECR_CAPRD.OldSid <> '' and v53f_ISO_DECR_CAPRD.OldSidName = 'ISIN'
     THEN ':35B:' + v53f_ISO_DECR_CAPRD.OldSidname +' '+ v53f_ISO_DECR_CAPRD.OldSid
     WHEN v53f_ISO_DECR_CAPRD.OldSid <> '' and v53f_ISO_DECR_CAPRD.SidName = '/GB/'
     THEN ':35B:' + v53f_ISO_DECR_CAPRD.OldSidname +' '+ v53f_ISO_DECR_CAPRD.OldSid
     ELSE ':35B:' + v53f_ISO_DECR_CAPRD.Sidname +' '+ v53f_ISO_DECR_CAPRD.Sid
     END,
substring(v53f_ISO_DECR_CAPRD.Issuername,1,35) as TIDYTEXT,
substring(v53f_ISO_DECR_CAPRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v53f_ISO_DECR_CAPRD.Sedol <> '' and v53f_ISO_DECR_CAPRD.Sidname <> '/GB/'
     THEN '/GB/' + v53f_ISO_DECR_CAPRD.Sedol
     ELSE ''
     END,
CASE WHEN v53f_ISO_DECR_CAPRD.Localcode <> ''
     THEN '/TS/' + v53f_ISO_DECR_CAPRD.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v53f_ISO_DECR_CAPRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v53f_ISO_DECR_CAPRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v53f_ISO_DECR_CAPRD.Recdate <>'' AND v53f_ISO_DECR_CAPRD.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v53f_ISO_DECR_CAPRD.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v53f_ISO_DECR_CAPRD.PayDate <>'' and v53f_ISO_DECR_CAPRD.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v53f_ISO_DECR_CAPRD.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':70E::TXNR//',
'Old Par Value ' + v53f_ISO_DECR_CAPRD.OldParValue,
'New Par Value ' + v53f_ISO_DECR_CAPRD.NewParValue,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v53f_ISO_DECR_CAPRD.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v53f_ISO_DECR_CAPRD.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v53f_ISO_DECR_CAPRD.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v53f_ISO_DECR_CAPRD.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v53f_ISO_DECR_CAPRD.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v53f_ISO_DECR_CAPRD.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v53f_ISO_DECR_CAPRD.NewSid <>''
     THEN ':35B:' + v53f_ISO_DECR_CAPRD.NewSidname +' '+ v53f_ISO_DECR_CAPRD.NewSid
     ELSE ':35B:' + v53f_ISO_DECR_CAPRD.Sidname +' '+ v53f_ISO_DECR_CAPRD.Sid
     END,
CASE WHEN v53f_ISO_DECR_CAPRD.NewRatio <>''
            AND v53f_ISO_DECR_CAPRD.NewRatio IS NOT NULL
     THEN ':92D::NEWO//'+ltrim(cast(v53f_ISO_DECR_CAPRD.NewRatio as char (15)))+
                    '/'+ltrim(cast(v53f_ISO_DECR_CAPRD.OldRatio as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v53f_ISO_DECR_CAPRD
WHERE 
(SID <> '' or OldSID <> '')
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
and v53f_ISO_DECR_CAPRD.OldRatio<>''
and v53f_ISO_DECR_CAPRD.NewRatio<>''
and cast(rtrim(v53f_ISO_DECR_CAPRD.OldRatio) as float(15,7))>
      cast(rtrim(v53f_ISO_DECR_CAPRD.NewRatio) as float(15,7))
order by caref1, caref2 desc, caref3
