--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_LIQU
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=LIQ

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select    
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + rtrim(cast(v50f_ISO_LIQU.EventID as char(10))) + ' and sEvent = '+ char(39) + 'LIQ'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
v50f_ISO_LIQU.Changed,
'select  LiquidationTerms As Notes FROM LIQ WHERE LIQID = '+ cast(v50f_ISO_LIQU.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'113'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v50f_ISO_LIQU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v50f_ISO_LIQU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v50f_ISO_LIQU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v50f_ISO_LIQU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v50f_ISO_LIQU.Sidname +' '+ v50f_ISO_LIQU.Sid,
substring(v50f_ISO_LIQU.Issuername,1,35) as TIDYTEXT,
substring(v50f_ISO_LIQU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v50f_ISO_LIQU.Sedol <> '' and v50f_ISO_LIQU.Sidname <> '/GB/'
     THEN '/GB/' + v50f_ISO_LIQU.Sedol
     ELSE ''
     END,
CASE WHEN v50f_ISO_LIQU.Localcode <> ''
     THEN '/TS/' + v50f_ISO_LIQU.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v50f_ISO_LIQU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v50f_ISO_LIQU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v50f_ISO_LIQU.RdDate <>'' AND v50f_ISO_LIQU.RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v50f_ISO_LIQU.RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v50f_ISO_LIQU
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
