--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_MEET
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v50f_ISO_MEET.Changed,
'' as ISOHDR,
':16R:GENL',
'120'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v50f_ISO_MEET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v50f_ISO_MEET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v50f_ISO_MEET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v50f_ISO_MEET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN v50f_ISO_MEET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN v50f_ISO_MEET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN v50f_ISO_MEET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN v50f_ISO_MEET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN v50f_ISO_MEET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v50f_ISO_MEET.Sidname +' '+ v50f_ISO_MEET.Sid,
substring(v50f_ISO_MEET.Issuername,1,35) as TIDYTEXT,
substring(v50f_ISO_MEET.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v50f_ISO_MEET.Sedol <> '' and v50f_ISO_MEET.Sidname <> '/GB/'
     THEN '/GB/' + v50f_ISO_MEET.Sedol
     ELSE ''
     END,
CASE WHEN v50f_ISO_MEET.Localcode <> ''
     THEN '/TS/' + v50f_ISO_MEET.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v50f_ISO_MEET.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v50f_ISO_MEET.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v50f_ISO_MEET.AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , v50f_ISO_MEET.AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//' + substring(rtrim(v50f_ISO_MEET.Add1),1,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Add1),36,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Add2),1,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Add2),36,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Add3),1,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Add4),1,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Add5),1,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Add6),1,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.City),1,35) as TIDYTEXT,
substring(rtrim(v50f_ISO_MEET.Country),1,35) as TIDYTEXT,
':16S:ADDINFO',
'-}$'
From v50f_ISO_MEET
WHERE 
(v50f_ISO_MEET.AGMEGM = 'AG'
or v50f_ISO_MEET.AGMEGM = 'CG'
or v50f_ISO_MEET.AGMEGM = 'EG'
or v50f_ISO_MEET.AGMEGM = 'GM'
or v50f_ISO_MEET.AGMEGM = 'SG')
and SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref2 desc, caref1, caref3
