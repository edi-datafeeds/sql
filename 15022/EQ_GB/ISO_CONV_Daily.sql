--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_CONV
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v51f_ISO_CONV.Changed,
'select  ConvNotes As Notes FROM CONV WHERE ConvID = '+ cast(v51f_ISO_CONV.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'125'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v51f_ISO_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v51f_ISO_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v51f_ISO_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v51f_ISO_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN v51f_ISO_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':25D::PROC//COMP', 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v51f_ISO_CONV.Sidname +' '+ v51f_ISO_CONV.Sid,
substring(v51f_ISO_CONV.Issuername,1,35) as TIDYTEXT,
substring(v51f_ISO_CONV.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v51f_ISO_CONV.Sedol <> '' and v51f_ISO_CONV.Sidname <> '/GB/'
     THEN '/GB/' + v51f_ISO_CONV.Sedol
     ELSE ''
     END,
CASE WHEN v51f_ISO_CONV.Localcode <> ''
     THEN '/TS/' + v51f_ISO_CONV.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v51f_ISO_CONV.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v51f_ISO_CONV.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v51f_ISO_CONV.FromDate <>'' AND v51f_ISO_CONV.FromDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v51f_ISO_CONV.FromDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN v51f_ISO_CONV.ToDate <>'' AND v51f_ISO_CONV.ToDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , v51f_ISO_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v51f_ISO_CONV.EventID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONV',
CASE WHEN v51f_ISO_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN v51f_ISO_CONV.ToDate <>'' AND v51f_ISO_CONV.ToDate IS NOT NULL
       AND v51f_ISO_CONV.FromDate <>'' AND v51f_ISO_CONV.FromDate IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , v51f_ISO_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , v51f_ISO_CONV.ToDate,112)
     WHEN v51f_ISO_CONV.FromDate <>'' AND v51f_ISO_CONV.FromDate IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , v51f_ISO_CONV.FromDate,112)+'/UKWN'
     WHEN v51f_ISO_CONV.ToDate <>'' AND v51f_ISO_CONV.ToDate IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , v51f_ISO_CONV.ToDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN v51f_ISO_CONV.Price <>'' AND v51f_ISO_CONV.Price IS NOT NULL
     THEN ':90B::EXER//ACTU/'+v51f_ISO_CONV.CurenCD+
          +substring(v51f_ISO_CONV.Price,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v51f_ISO_CONV.ResSID <> ''
     THEN ':35B:' + v51f_ISO_CONV.resSIDname +' '+ v51f_ISO_CONV.ResSID
     END,
CASE WHEN v51f_ISO_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v51f_ISO_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v51f_ISO_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v51f_ISO_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v51f_ISO_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v51f_ISO_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v51f_ISO_CONV.RatioNew <>''
            AND v51f_ISO_CONV.RatioNew IS NOT NULL
          AND v51f_ISO_CONV.RatioOld <>''
            AND v51f_ISO_CONV.RatioOld IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(v51f_ISO_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(v51f_ISO_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
/*Currently no suitable paydate
CASE WHEN v51f_ISO_CONV.Paydate <>'' 
             AND v51f_ISO_CONV.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v51f_ISO_CONV.Paydate,112)
     WHEN v51f_ISO_CONV.paydate <>'' 
             AND v51f_ISO_CONV.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v51f_ISO_CONV.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v51f_ISO_CONV
WHERE 
SID <> ''
and Actflag <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
