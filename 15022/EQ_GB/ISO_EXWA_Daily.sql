--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXWA
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v51f_ISO_EXWA.Changed,
'select  WartmNotes As Notes FROM WARTM WHERE SECID = '+ cast(v51f_ISO_EXWA.SecID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'300'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v51f_ISO_EXWA.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v51f_ISO_EXWA.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v51f_ISO_EXWA.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v51f_ISO_EXWA.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXWA',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v51f_ISO_EXWA.Sidname +' '+ v51f_ISO_EXWA.Sid,
substring(v51f_ISO_EXWA.Issuername,1,35) as TIDYTEXT,
substring(v51f_ISO_EXWA.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v51f_ISO_EXWA.Sedol <> '' and v51f_ISO_EXWA.Sidname <> '/GB/'
     THEN '/GB/' + v51f_ISO_EXWA.Sedol
     ELSE ''
     END,
CASE WHEN v51f_ISO_EXWA.Localcode <> ''
     THEN '/TS/' + v51f_ISO_EXWA.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v51f_ISO_EXWA.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v51f_ISO_EXWA.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v51f_ISO_EXWA.ExpirationDate <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v51f_ISO_EXWA.ExpirationDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN v51f_ISO_EXWA.ExpirationDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v51f_ISO_EXWA.ExpirationDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v51f_ISO_EXWA.ToDate <>'' AND v51f_ISO_EXWA.ToDate IS NOT NULL
       AND v51f_ISO_EXWA.FromDate <>'' AND v51f_ISO_EXWA.FromDate IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , v51f_ISO_EXWA.FromDate,112)+'/'
          +CONVERT ( varchar , v51f_ISO_EXWA.ToDate,112)
     WHEN v51f_ISO_EXWA.FromDate <>'' AND v51f_ISO_EXWA.FromDate IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , v51f_ISO_EXWA.FromDate,112)+'/UKWN'
     WHEN v51f_ISO_EXWA.ToDate <>'' AND v51f_ISO_EXWA.ToDate IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , v51f_ISO_EXWA.ToDate,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN v51f_ISO_EXWA.PricePerShare <> ''
     THEN ':90B::MRKT//ACTU/'+CurenCD+rtrim(cast(v51f_ISO_EXWA.PricePerShare as char(15)))
     ELSE ':90E::MRKT//UKWN'
     END as COMMASUB,
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN v51f_ISO_EXWA.CurenCD <> '' 
     THEN ':11A::OPTN//' + v51f_ISO_EXWA.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v51f_ISO_EXWA.StrikePrice <> ''
     THEN ':90B::EXER//ACTU/'+CurenCD+rtrim(cast(v51f_ISO_EXWA.StrikePrice as char(15)))
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v51f_ISO_EXWA.ExerSid <> ''
     THEN ':35B:' + v51f_ISO_EXWA.ExerSidname +' '+ v51f_ISO_EXWA.ExerSid
     END,
CASE WHEN v51f_ISO_EXWA.RatioNew <>''
            AND v51f_ISO_EXWA.RatioNew IS NOT NULL
          AND v51f_ISO_EXWA.RatioOld <>''
            AND v51f_ISO_EXWA.RatioOld IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(v51f_ISO_EXWA.RatioNew as char (15)))+
                    '/'+rtrim(cast(v51f_ISO_EXWA.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
/* CASE WHEN v51f_ISO_EXWA.Paydate <>'' 
               AND v51f_ISO_EXWA.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v51f_ISO_EXWA.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END, */
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v51f_ISO_EXWA
WHERE 
SID <> ''
and EventID is not null
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
