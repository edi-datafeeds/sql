--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXRI
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v54f_ISO_RHTSt.Changed,
'select  RTSNotes As Notes FROM RTS WHERE RDID = '+ cast(v54f_ISO_RHTSt.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'118'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_RHTSt.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_RHTSt.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_RHTSt.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_RHTSt.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXRI',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
/*':20C::RELA//117'+v54f_ISO_RHTSt.EXCHGID+cast(v54f_ISO_RHTSt.Seqnum as char(1))+ cast(v54f_ISO_RHTSt.EventID as char(16)) as CAref,*/
':16S:GENL',
':16R:USECU',
CASE WHEN rtrim(v54f_ISO_RHTSt.TraSid) <> ''
     THEN ':35B:' + v54f_ISO_RHTSt.TraSidname +' '+ v54f_ISO_RHTSt.TraSid
     END,
substring(v54f_ISO_RHTSt.tIssuername,1,35),
substring(v54f_ISO_RHTSt.tSecuritydesc,1,35) as tsecdesc,
CASE WHEN v54f_ISO_RHTSt.Sedol <> '' and v54f_ISO_RHTSt.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_RHTSt.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_RHTSt.Localcode <> ''
     THEN '/TS/' + v54f_ISO_RHTSt.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_RHTSt.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_RHTSt.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_RHTSt.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v54f_ISO_RHTSt.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSt.EndTrade <> '' 
       AND v54f_ISO_RHTSt.StartTrade <> '' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartTrade,112) + '/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndTrade,112)
     WHEN v54f_ISO_RHTSt.StartTrade <> '' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartTrade,112) + '//UKWN'
     WHEN v54f_ISO_RHTSt.EndTrade <> '' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v54f_ISO_RHTSt.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN v54f_ISO_RHTSt.CurenCD <> '' 
     THEN ':11A::OPTN//' + v54f_ISO_RHTSt.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v54f_ISO_RHTSt.EndSubscription <>'' AND v54f_ISO_RHTSt.EndSubscription IS NOT NULL
       AND v54f_ISO_RHTSt.StartSubscription <>'' AND v54f_ISO_RHTSt.StartSubscription IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartSubscription,112)+'/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndSubscription,112)
     WHEN v54f_ISO_RHTSt.StartSubscription <>'' AND v54f_ISO_RHTSt.StartSubscription IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartSubscription,112)+'/UKWN'
     WHEN v54f_ISO_RHTSt.EndSubscription <>'' AND v54f_ISO_RHTSt.EndSubscription IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndSubscription,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSt.Paydate <>'' 
               AND v54f_ISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN v54f_ISO_RHTSt.IssuePrice <> ''
     THEN ':90B::SUPR//ACTU/'+CurenCD+ltrim(cast(v54f_ISO_RHTSt.IssuePrice as char(15)))
     ELSE ':90E::SUPR//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_RHTSt.ResSid <> ''
     THEN ':35B:' + v54f_ISO_RHTSt.ResSidname +' '+ v54f_ISO_RHTSt.ResSid
     END,
CASE WHEN v54f_ISO_RHTSt.RatioNew <>''
               AND v54f_ISO_RHTSt.RatioOld <>''
     AND v54f_ISO_RHTSt.RatioNew is not null
               AND v54f_ISO_RHTSt.RatioOld is not null
     THEN ':92D::NWRT//'+ltrim(cast(v54f_ISO_RHTSt.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_RHTSt.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
CASE WHEN v54f_ISO_RHTSt.Paydate <>'' 
               AND v54f_ISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* EXERCISE OPTION END */
/* SELL RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SLLE',
':17B::DFLT//N',
CASE WHEN v54f_ISO_RHTSt.Paydate <>'' 
               AND v54f_ISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* SELL RIGHTS OPTION END */
/* LAPSE RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//003',
':22F::CAOP//LAPS',
':17B::DFLT//N',
CASE WHEN v54f_ISO_RHTSt.Paydate <>'' 
               AND v54f_ISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* LAPSE RIGHTS OPTION END */
'' as Notes,
'-}$'
From v54f_ISO_RHTSt
WHERE 
traSID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
