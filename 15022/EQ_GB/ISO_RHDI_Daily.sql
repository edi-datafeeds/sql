--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_RHDI
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v54f_ISO_RHTSt.Changed,
'select  RtsNotes As Notes FROM RTS WHERE RDID = '+ cast(v54f_ISO_RHTSt.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'117'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_RHTSt.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_RHTSt.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_RHTSt.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_RHTSt.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHDI',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
/*':20C::RELA//118'+v54f_ISO_RHTSt.EXCHGID+cast(v54f_ISO_RHTSt.Seqnum as char(1))+ cast(v54f_ISO_RHTSt.EventID as char(16)) as CAref,*/
':16S:GENL',
':16R:USECU',
':35B:' + v54f_ISO_RHTSt.Sidname +' '+ v54f_ISO_RHTSt.Sid,
substring(v54f_ISO_RHTSt.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_RHTSt.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_RHTSt.Sedol <> '' and v54f_ISO_RHTSt.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_RHTSt.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_RHTSt.Localcode <> ''
     THEN '/TS/' + v54f_ISO_RHTSt.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_RHTSt.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_RHTSt.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_RHTSt.Exdate <>'' AND v54f_ISO_RHTSt.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_RHTSt.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSt.Recdate <>'' AND v54f_ISO_RHTSt.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v54f_ISO_RHTSt.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSt.EndTrade <> '' 
       AND v54f_ISO_RHTSt.StartTrade <> '' 
     THEN ':69A::REVO//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartTrade,112) + '/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndTrade,112)
     WHEN v54f_ISO_RHTSt.StartTrade <> '' 
     THEN ':69C::REVO//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartTrade,112) + '/UKWN'
     WHEN v54f_ISO_RHTSt.EndTrade <> '' 
     THEN ':69E::REVO//UKWN/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndTrade,112)
     ELSE ':69J::REVO//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v54f_ISO_RHTSt.RdID as char(16)) as Notes, */ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':22F::RHDI//EXRI',
CASE WHEN v54f_ISO_RHTSt.Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN v54f_ISO_RHTSt.Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN v54f_ISO_RHTSt.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v54f_ISO_RHTSt.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v54f_ISO_RHTSt.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v54f_ISO_RHTSt.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
':17B::DFLT//Y',
CASE WHEN v54f_ISO_RHTSt.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v54f_ISO_RHTSt.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSt.EndTrade <> '' 
       AND v54f_ISO_RHTSt.StartTrade <> '' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartTrade,112) + '/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndTrade,112)
     WHEN v54f_ISO_RHTSt.StartTrade <> '' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , v54f_ISO_RHTSt.StartTrade,112) + '//UKWN'
     WHEN v54f_ISO_RHTSt.EndTrade <> '' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , v54f_ISO_RHTSt.EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_RHTSt.TraSid <> ''
     THEN ':35B:' + v54f_ISO_RHTSt.TraSidname +' '+ v54f_ISO_RHTSt.TraSid
     END,
CASE WHEN v54f_ISO_RHTSt.RatioNew <>''
            AND v54f_ISO_RHTSt.RatioNew IS NOT NULL
          AND v54f_ISO_RHTSt.RatioOld <>''
            AND v54f_ISO_RHTSt.RatioOld IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(v54f_ISO_RHTSt.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_RHTSt.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN v54f_ISO_RHTSt.Paydate <>'' 
               AND v54f_ISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v54f_ISO_RHTSt
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
