--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_TEND
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=Y
--sEvent=TKOVR

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' 
+ rtrim(cast(v53f_ISO_TEND_TKOVR.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) 
+ ' Order By OptionID, SerialID' as MPAYlink,
v53f_ISO_TEND_TKOVR.Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(v53f_ISO_TEND_TKOVR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'131'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v53f_ISO_TEND_TKOVR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v53f_ISO_TEND_TKOVR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v53f_ISO_TEND_TKOVR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v53f_ISO_TEND_TKOVR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//VOLU', 
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v53f_ISO_TEND_TKOVR.Sidname +' '+ v53f_ISO_TEND_TKOVR.Sid,
substring(v53f_ISO_TEND_TKOVR.Issuername,1,35) as TIDYTEXT,
substring(v53f_ISO_TEND_TKOVR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v53f_ISO_TEND_TKOVR.Sedol <> '' and v53f_ISO_TEND_TKOVR.Sidname <> '/GB/'
     THEN '/GB/' + v53f_ISO_TEND_TKOVR.Sedol
     ELSE ''
     END,
CASE WHEN v53f_ISO_TEND_TKOVR.Localcode <> ''
     THEN '/TS/' + v53f_ISO_TEND_TKOVR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v53f_ISO_TEND_TKOVR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v53f_ISO_TEND_TKOVR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v53f_ISO_TEND_TKOVR.Opendate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v53f_ISO_TEND_TKOVR.opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN v53f_ISO_TEND_TKOVR.CmAcqdate <> ''
     THEN ':98A::WUCO//'+CONVERT ( varchar , v53f_ISO_TEND_TKOVR.CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN v53f_ISO_TEND_TKOVR.CloseDate <> ''
       AND v53f_ISO_TEND_TKOVR.OpenDate <> ''
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , v53f_ISO_TEND_TKOVR.OpenDate,112)+'/'
          +CONVERT ( varchar , v53f_ISO_TEND_TKOVR.CloseDate,112)
     WHEN v53f_ISO_TEND_TKOVR.OpenDate <> ''
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , v53f_ISO_TEND_TKOVR.OpenDate,112)+'/UKWN'
     WHEN v53f_ISO_TEND_TKOVR.CloseDate <> ''
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , v53f_ISO_TEND_TKOVR.CloseDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN v53f_ISO_TEND_TKOVR.CloseDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v53f_ISO_TEND_TKOVR.CloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v53f_ISO_TEND_TKOVR.UnconditionalDate <>'' and v53f_ISO_TEND_TKOVR.UnconditionalDate > getdate()
     THEN ':22F::TAKO//COND'
     ELSE ':22F::TAKO//UNCO'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v53f_ISO_TEND_TKOVR
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
