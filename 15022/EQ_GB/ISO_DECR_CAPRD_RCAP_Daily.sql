--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DECR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v53f_ISO_DECR_CAPRD.Changed,
'select  CapRdNotes As Notes FROM CAPRD WHERE CAPRDID = '+ cast(v53f_ISO_DECR_CAPRD.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'148'+v53f_ISO_DECR_CAPRD.EXCHGID+cast(v53f_ISO_DECR_CAPRD.Seqnum as char(1)) as CAref1,
v53f_ISO_DECR_CAPRD.EventID as CAREF2,
v53f_ISO_DECR_CAPRD.SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v53f_ISO_DECR_CAPRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v53f_ISO_DECR_CAPRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v53f_ISO_DECR_CAPRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v53f_ISO_DECR_CAPRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
CASE WHEN v53f_ISO_DECR_CAPRD.OldSid <> ''
     THEN ':35B:' + v53f_ISO_DECR_CAPRD.OldSidname + v53f_ISO_DECR_CAPRD.OldSid
     ELSE ':35B:' + v53f_ISO_DECR_CAPRD.Sidname + v53f_ISO_DECR_CAPRD.Sid
     END,
substring(v53f_ISO_DECR_CAPRD.Issuername,1,35) as TIDYTEXT,
substring(v53f_ISO_DECR_CAPRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v53f_ISO_DECR_CAPRD.Sedol <> '' and v53f_ISO_DECR_CAPRD.Sidname <> '/GB/'
     THEN '/GB/' + v53f_ISO_DECR_CAPRD.Sedol
     ELSE ''
     END,
CASE WHEN v53f_ISO_DECR_CAPRD.Localcode <> ''
     THEN '/TS/' + v53f_ISO_DECR_CAPRD.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v53f_ISO_DECR_CAPRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v53f_ISO_DECR_CAPRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v53f_ISO_DECR_CAPRD.RecDate <>'' and v53f_ISO_DECR_CAPRD.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , v53f_ISO_DECR_CAPRD.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v53f_ISO_DECR_CAPRD.EffectiveDate <>'' and v53f_ISO_DECR_CAPRD.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , v53f_ISO_DECR_CAPRD.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN RCAP.CSPYDate <>'' and RCAP.CSPYDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , RCAP.CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':70E::TXNR//',
'Old Par Value ' + v53f_ISO_DECR_CAPRD.OldParValue,
'New Par Value ' + v53f_ISO_DECR_CAPRD.NewParValue,
':16S:CADETL',
CASE WHEN  RCAP.CashBak <>'' AND RCAP.CashBak IS NOT NULL
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN  RCAP.CashBak <>'' AND RCAP.CashBak IS NOT NULL
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN  RCAP.CashBak <>'' AND RCAP.CashBak IS NOT NULL
     THEN ':11A::OPTN//' + RCAP.CurenCD
     END,
CASE WHEN  RCAP.CashBak <>'' AND RCAP.CashBak IS NOT NULL
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN  RCAP.CashBak <>'' AND RCAP.CashBak IS NOT NULL
     THEN ':17B::DFLT//Y'
     ELSE ''
     END,
CASE WHEN v53f_ISO_DECR_CAPRD.NewSid <> ''
     THEN ':35B:' + v53f_ISO_DECR_CAPRD.NewSidname + v53f_ISO_DECR_CAPRD.NewSid
     ELSE ''
     END,
CASE WHEN RCAP.CashBak <>'' AND RCAP.CashBak IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+RCAP.CurenCD+
          +substring(RCAP.CashBak,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as Commasub,
CASE WHEN  RCAP.CashBak <>'' AND RCAP.CashBak IS NOT NULL
     THEN ':16S:CAOPTN'
     ELSE ''
     END,'' as Notes,
'-}$'
From v53f_ISO_DECR_CAPRD
INNER JOIN RCAP on v53f_ISO_DECR_CAPRD.SecID = RCAP.SecID
                     and v53f_ISO_DECR_CAPRD.EffectiveDate = RCAP.EffectiveDate
WHERE 
(v53f_ISO_DECR_CAPRD.SID <> '' or v53f_ISO_DECR_CAPRD.oldSID <> '')
and v53f_ISO_DECR_CAPRD.changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
and RCAP.SecID is not null
order by caref1, caref2 desc, caref3
