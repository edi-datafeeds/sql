--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_CLSA
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v50f_ISO_CLSA.Changed,
'select  LawstNotes As Notes FROM LAWST WHERE LAWSTID = '+ cast(v50f_ISO_CLSA.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'149'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v50f_ISO_CLSA.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v50f_ISO_CLSA.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v50f_ISO_CLSA.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v50f_ISO_CLSA.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CLSA',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v50f_ISO_CLSA.Sidname +' '+ v50f_ISO_CLSA.Sid,
substring(v50f_ISO_CLSA.Issuername,1,35) as TIDYTEXT,
substring(v50f_ISO_CLSA.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v50f_ISO_CLSA.Sedol <> '' and v50f_ISO_CLSA.Sidname <> '/GB/'
     THEN '/GB/' + v50f_ISO_CLSA.Sedol
     ELSE ''
     END,
CASE WHEN v50f_ISO_CLSA.Localcode <> ''
     THEN '/TS/' + v50f_ISO_CLSA.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v50f_ISO_CLSA.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v50f_ISO_CLSA.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v50f_ISO_CLSA.EffectiveDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v50f_ISO_CLSA.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From v50f_ISO_CLSA
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
