--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_PPMT
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v53f_ISO_PPMT.Changed,
'select  CallNotes As Notes FROM CALL WHERE CALLID = '+ cast(v53f_ISO_PPMT.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'111'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v53f_ISO_PPMT.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v53f_ISO_PPMT.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v53f_ISO_PPMT.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v53f_ISO_PPMT.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PPMT',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v53f_ISO_PPMT.Sidname +' '+ v53f_ISO_PPMT.Sid,
substring(v53f_ISO_PPMT.Issuername,1,35) as TIDYTEXT,
substring(v53f_ISO_PPMT.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v53f_ISO_PPMT.Sedol <> '' and v53f_ISO_PPMT.Sidname <> '/GB/'
     THEN '/GB/' + v53f_ISO_PPMT.Sedol
     ELSE ''
     END,
CASE WHEN v53f_ISO_PPMT.Localcode <> ''
     THEN '/TS/' + v53f_ISO_PPMT.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v53f_ISO_PPMT.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v53f_ISO_PPMT.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v53f_ISO_PPMT.DueDate <>'' AND v53f_ISO_PPMT.DueDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v53f_ISO_PPMT.DueDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v53f_ISO_PPMT.CurenCD <> '' AND v53f_ISO_PPMT.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + v53f_ISO_PPMT.CurenCD
     END,
':17B::DFLT//Y',
CASE WHEN v53f_ISO_PPMT.DueDate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v53f_ISO_PPMT.DueDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN v53f_ISO_PPMT.ToFacevalue <> '' AND v53f_ISO_PPMT.ToFacevalue is not null
     THEN ':90B::PRPP//ACTU/'+curenCD+rtrim(cast(v53f_ISO_PPMT.ToFacevalue as char(15)))
     ELSE ':90E::PRPP//UKWN' END,
':16S:CAOPTN',
'' as Notes,
'-}$'
From v53f_ISO_PPMT
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
