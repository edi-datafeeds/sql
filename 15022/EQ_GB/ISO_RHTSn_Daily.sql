--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_RHTS
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v54f_ISO_RHTSn.Changed,
'select  ENTNotes As Notes FROM ENT WHERE RDID = '+ cast(v54f_ISO_RHTSn.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'116'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_RHTSn.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_RHTSn.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_RHTSn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_RHTSn.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHTS',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v54f_ISO_RHTSn.Sidname +' '+ v54f_ISO_RHTSn.Sid,
substring(v54f_ISO_RHTSn.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_RHTSn.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_RHTSn.Sedol <> '' and v54f_ISO_RHTSn.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_RHTSn.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_RHTSn.Localcode <> ''
     THEN '/TS/' + v54f_ISO_RHTSn.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_RHTSn.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_RHTSn.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_RHTSn.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v54f_ISO_RHTSn.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSn.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v54f_ISO_RHTSn.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSn.EndSubscription <>'' AND v54f_ISO_RHTSn.EndSubscription IS NOT NULL
       AND v54f_ISO_RHTSn.StartSubscription <>'' AND v54f_ISO_RHTSn.StartSubscription IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , v54f_ISO_RHTSn.StartSubscription,112)+'/'
          +CONVERT ( varchar , v54f_ISO_RHTSn.EndSubscription,112)
     WHEN v54f_ISO_RHTSn.StartSubscription <>'' AND v54f_ISO_RHTSn.StartSubscription IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , v54f_ISO_RHTSn.StartSubscription,112)+'/UKWN'
     WHEN v54f_ISO_RHTSn.EndSubscription <>'' AND v54f_ISO_RHTSn.EndSubscription IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , v54f_ISO_RHTSn.EndSubscription,112)
     ELSE ':69J::IACC//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v54f_ISO_RHTSn.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN v54f_ISO_RHTSn.CurenCD <> '' 
     THEN ':11A::OPTN//' + v54f_ISO_RHTSn.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v54f_ISO_RHTSn.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v54f_ISO_RHTSn.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSn.Paydate <> '' and v54f_ISO_RHTSn.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_RHTSn.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN v54f_ISO_RHTSn.EntIssuePrice <> ''
     THEN ':90B::SUPR//ACTU/'+CurenCD+ltrim(cast(v54f_ISO_RHTSn.EntIssuePrice as char(15)))
     ELSE ':90E::SUPR//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_RHTSn.ResSid <> ''
     THEN ':35B:' + v54f_ISO_RHTSn.ResSidname +' '+ v54f_ISO_RHTSn.ResSid
     END,
CASE WHEN v54f_ISO_RHTSn.RatioNew <>''
            AND v54f_ISO_RHTSn.RatioNew IS NOT NULL
          AND v54f_ISO_RHTSn.RatioOld <>''
            AND v54f_ISO_RHTSn.RatioOld IS NOT NULL
     THEN ':92D::NWRT//'+ltrim(cast(v54f_ISO_RHTSn.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_RHTSn.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v54f_ISO_RHTSn
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
