--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DECR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v53f_ISO_DECR_RCAP.Changed,
'select  RcapNotes As Notes FROM RCAP WHERE RCAPID = '+ cast(v53f_ISO_DECR_RCAP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'146'+v53f_ISO_DECR_RCAP.EXCHGID+cast(v53f_ISO_DECR_RCAP.Seqnum as char(1)) as CAref1,
v53f_ISO_DECR_RCAP.EventID as CAREF2,
v53f_ISO_DECR_RCAP.SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v53f_ISO_DECR_RCAP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v53f_ISO_DECR_RCAP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v53f_ISO_DECR_RCAP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v53f_ISO_DECR_RCAP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v53f_ISO_DECR_RCAP.Sidname +' '+ v53f_ISO_DECR_RCAP.Sid,
substring(v53f_ISO_DECR_RCAP.Issuername,1,35) as TIDYTEXT,
substring(v53f_ISO_DECR_RCAP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v53f_ISO_DECR_RCAP.Sedol <> '' and v53f_ISO_DECR_RCAP.Sidname <> '/GB/'
     THEN '/GB/' + v53f_ISO_DECR_RCAP.Sedol
     ELSE ''
     END,
CASE WHEN v53f_ISO_DECR_RCAP.Localcode <> ''
     THEN '/TS/' + v53f_ISO_DECR_RCAP.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v53f_ISO_DECR_RCAP.MCD <>''
     THEN ':94B::PLIS//EXCH/' +v53f_ISO_DECR_RCAP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v53f_ISO_DECR_RCAP.RecDate <>'' and v53f_ISO_DECR_RCAP.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , v53f_ISO_DECR_RCAP.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v53f_ISO_DECR_RCAP.EffectiveDate <>'' and v53f_ISO_DECR_RCAP.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , v53f_ISO_DECR_RCAP.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN v53f_ISO_DECR_RCAP.CSPYDate <>'' and v53f_ISO_DECR_RCAP.CSPYDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v53f_ISO_DECR_RCAP.CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v53f_ISO_DECR_RCAP.CurenCD <> '' 
     THEN ':11A::OPTN//' + v53f_ISO_DECR_RCAP.CurenCD
     END,
':17B::DFLT//Y',
CASE WHEN v53f_ISO_DECR_RCAP.CashBak <>'' AND v53f_ISO_DECR_RCAP.CashBak IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+v53f_ISO_DECR_RCAP.CurenCD+
          +substring(v53f_ISO_DECR_RCAP.CashBak,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as Commasub,
':16S:CAOPTN',
'' as Notes,
'-}$'
From v53f_ISO_DECR_RCAP
LEFT OUTER JOIN CAPRD on v53f_ISO_DECR_RCAP.SecID = CAPRD.SecID
                    and v53f_ISO_DECR_RCAP.EffectiveDate = CAPRD.EffectiveDate
WHERE 
v53f_ISO_DECR_RCAP.SID <> ''
and v53f_ISO_DECR_RCAP.changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and CAPRD.Secid is null
and v53f_ISO_DECR_RCAP.cntrycd = 'GB'
order by caref1, caref2 desc, caref3
