--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DECR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v51f_ISO_INCR_DECR_PVRD.Changed,
'select  PVRDNotes As Notes FROM PVRD WHERE PVRDID = '+ cast(v51f_ISO_INCR_DECR_PVRD.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'155'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v51f_ISO_INCR_DECR_PVRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v51f_ISO_INCR_DECR_PVRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v51f_ISO_INCR_DECR_PVRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v51f_ISO_INCR_DECR_PVRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v51f_ISO_INCR_DECR_PVRD.Sidname +' '+ v51f_ISO_INCR_DECR_PVRD.Sid,
substring(v51f_ISO_INCR_DECR_PVRD.Issuername,1,35) as TIDYTEXT,
substring(v51f_ISO_INCR_DECR_PVRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v51f_ISO_INCR_DECR_PVRD.Sedol <> '' and v51f_ISO_INCR_DECR_PVRD.Sidname <> '/GB/'
     THEN '/GB/' + v51f_ISO_INCR_DECR_PVRD.Sedol
     ELSE ''
     END,
CASE WHEN v51f_ISO_INCR_DECR_PVRD.Localcode <> ''
     THEN '/TS/' + v51f_ISO_INCR_DECR_PVRD.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v51f_ISO_INCR_DECR_PVRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v51f_ISO_INCR_DECR_PVRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v51f_ISO_INCR_DECR_PVRD.EffectiveDate <>'' and v51f_ISO_INCR_DECR_PVRD.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , v51f_ISO_INCR_DECR_PVRD.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':70E::TXNR//',
'Old Par Value ' +v51f_ISO_INCR_DECR_PVRD.OldParValue,
'New Par Value ' +v51f_ISO_INCR_DECR_PVRD.NewParValue,
':16S:CADETL',
'' as Notes,
'-}$'
From v51f_ISO_INCR_DECR_PVRD
WHERE 
(SID <> '' or Sid <> '')
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and v51f_ISO_INCR_DECR_PVRD.OldParValue<>'0'
and v51f_ISO_INCR_DECR_PVRD.NewParValue<>'0'
and cntrycd = 'GB'
and cast(rtrim(v51f_ISO_INCR_DECR_PVRD.OldParValue) as float(14,5))>
      cast(rtrim(v51f_ISO_INCR_DECR_PVRD.NewParValue) as float(14,5))
order by caref1, caref2 desc, caref3
