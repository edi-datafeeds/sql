--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_BIDS
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v53f_ISO_BIDS.Changed,
'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(v53f_ISO_BIDS.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'115'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v53f_ISO_BIDS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v53f_ISO_BIDS.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN v53f_ISO_BIDS.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v53f_ISO_BIDS.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v53f_ISO_BIDS.Sidname +' '+ v53f_ISO_BIDS.Sid,
substring(v53f_ISO_BIDS.Issuername,1,35) as TIDYTEXT,
substring(v53f_ISO_BIDS.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v53f_ISO_BIDS.Sedol <> '' and v53f_ISO_BIDS.Sidname <> '/GB/'
     THEN '/GB/' + v53f_ISO_BIDS.Sedol
     ELSE ''
     END,
CASE WHEN v53f_ISO_BIDS.Localcode <> ''
     THEN '/TS/' + v53f_ISO_BIDS.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v53f_ISO_BIDS.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v53f_ISO_BIDS.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v53f_ISO_BIDS.StartDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v53f_ISO_BIDS.StartDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN v53f_ISO_BIDS.EndDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v53f_ISO_BIDS.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v53f_ISO_BIDS.EndDate <>'' AND v53f_ISO_BIDS.EndDate IS NOT NULL
       AND v53f_ISO_BIDS.StartDate <>'' AND v53f_ISO_BIDS.StartDate IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , v53f_ISO_BIDS.StartDate,112)+'/'
          +CONVERT ( varchar , v53f_ISO_BIDS.EndDate,112)
     WHEN v53f_ISO_BIDS.StartDate <>'' AND v53f_ISO_BIDS.StartDate IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , v53f_ISO_BIDS.StartDate,112)+'/UKWN'
     WHEN v53f_ISO_BIDS.EndDate <>'' AND v53f_ISO_BIDS.EndDate IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , v53f_ISO_BIDS.EndDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN v53f_ISO_BIDS.MaxQlyQty <> ''
     THEN ':36B::QTSO//'+substring(v53f_ISO_BIDS.MaxQlyQty,1,15) + ','
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v53f_ISO_BIDS.CurenCD <> '' AND v53f_ISO_BIDS.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + v53f_ISO_BIDS.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v53f_ISO_BIDS.paydate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v53f_ISO_BIDS.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN v53f_ISO_BIDS.MaxPrice <> '' AND v53f_ISO_BIDS.MaxPrice IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+v53f_ISO_BIDS.CurenCD+
          +substring(v53f_ISO_BIDS.MaxPrice,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
'' as Notes,
'-}$'
From v53f_ISO_BIDS
WHERE 
SID <> ''
and Actflag <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3