--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXOF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v54f_ISO_OTHR_DVST.Changed,
'select  DvstNotes As Notes FROM DVST WHERE RDID = '+ cast(v54f_ISO_OTHR_DVST.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'128'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_OTHR_DVST.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v54f_ISO_OTHR_DVST.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_OTHR_DVST.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_OTHR_DVST.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v54f_ISO_OTHR_DVST.Sidname +' '+ v54f_ISO_OTHR_DVST.Sid,
substring(v54f_ISO_OTHR_DVST.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_OTHR_DVST.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_OTHR_DVST.Sedol <> '' and v54f_ISO_OTHR_DVST.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_OTHR_DVST.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_OTHR_DVST.Localcode <> ''
     THEN '/TS/' + v54f_ISO_OTHR_DVST.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_OTHR_DVST.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_OTHR_DVST.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_OTHR_DVST.Exdate <> ''
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_OTHR_DVST.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v54f_ISO_OTHR_DVST.EndSubscription <>'' AND v54f_ISO_OTHR_DVST.EndSubscription IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , v54f_ISO_OTHR_DVST.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v54f_ISO_OTHR_DVST.EndSubscription <>'' AND v54f_ISO_OTHR_DVST.EndSubscription IS NOT NULL
       AND v54f_ISO_OTHR_DVST.StartSubscription <>'' AND v54f_ISO_OTHR_DVST.StartSubscription IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , v54f_ISO_OTHR_DVST.StartSubscription,112)+'/'
          +CONVERT ( varchar , v54f_ISO_OTHR_DVST.EndSubscription,112)
     WHEN v54f_ISO_OTHR_DVST.StartSubscription <>'' AND v54f_ISO_OTHR_DVST.StartSubscription IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , v54f_ISO_OTHR_DVST.StartSubscription,112)+'/UKWN'
     WHEN v54f_ISO_OTHR_DVST.EndSubscription <>'' AND v54f_ISO_OTHR_DVST.EndSubscription IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , v54f_ISO_OTHR_DVST.EndSubscription,112)
     ELSE ':69J::OFFR//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASE',
CASE WHEN v54f_ISO_OTHR_DVST.CurenCD <> '' AND v54f_ISO_OTHR_DVST.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + v54f_ISO_OTHR_DVST.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v54f_ISO_OTHR_DVST.MaxPrice <>'' AND v54f_ISO_OTHR_DVST.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+v54f_ISO_OTHR_DVST.CurenCD+
          +substring(v54f_ISO_OTHR_DVST.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_OTHR_DVST.ResSID <> ''
     THEN ':35B:' + v54f_ISO_OTHR_DVST.resSIDname +' '+ v54f_ISO_OTHR_DVST.ResSID
     END,
':92D::NEWO//1,/1,',
CASE WHEN v54f_ISO_OTHR_DVST.Paydate <>'' 
             AND v54f_ISO_OTHR_DVST.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_OTHR_DVST.Paydate,112)
     WHEN v54f_ISO_OTHR_DVST.paydate <>'' 
             AND v54f_ISO_OTHR_DVST.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_OTHR_DVST.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN v54f_ISO_OTHR_DVST.ResSID <> ''
     THEN ':35B:' + v54f_ISO_OTHR_DVST.ResSIDname +' '+ v54f_ISO_OTHR_DVST.ResSID
     END,
CASE WHEN v54f_ISO_OTHR_DVST.Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN v54f_ISO_OTHR_DVST.Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN v54f_ISO_OTHR_DVST.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v54f_ISO_OTHR_DVST.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v54f_ISO_OTHR_DVST.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v54f_ISO_OTHR_DVST.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
CASE WHEN v54f_ISO_OTHR_DVST.RatioNew <>''
               AND v54f_ISO_OTHR_DVST.RatioOld <>''
     AND v54f_ISO_OTHR_DVST.RatioNew is not null
               AND v54f_ISO_OTHR_DVST.RatioOld is not null
     THEN ':92D::ADEX//'+ltrim(cast(v54f_ISO_OTHR_DVST.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_OTHR_DVST.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
from v54f_ISO_OTHR_DVST
WHERE 
SID <> ''
and Actflag <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
