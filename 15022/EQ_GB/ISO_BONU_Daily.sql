--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_BONU
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select    
v54f_ISO_BONU.Changed,
'select  BonNotes As Notes FROM BON WHERE RDID = '+ cast(v54f_ISO_BONU.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'114'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v54f_ISO_BONU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_BONU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_BONU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_BONU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BONU',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v54f_ISO_BONU.Sidname +' '+ v54f_ISO_BONU.Sid,
substring(v54f_ISO_BONU.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_BONU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_BONU.Sedol <> '' and v54f_ISO_BONU.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_BONU.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_BONU.Localcode <> ''
     THEN '/TS/' + v54f_ISO_BONU.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_BONU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_BONU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_BONU.Exdate <>'' AND v54f_ISO_BONU.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_BONU.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN v54f_ISO_BONU.Recdate <>'' AND v54f_ISO_BONU.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v54f_ISO_BONU.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END as DateRec,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v54f_ISO_BONU.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v54f_ISO_BONU.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v54f_ISO_BONU.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v54f_ISO_BONU.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v54f_ISO_BONU.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v54f_ISO_BONU.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_BONU.Ressid <> ''
     THEN ':35B:' + v54f_ISO_BONU.Ressidname +' '+ v54f_ISO_BONU.Ressid
     WHEN (v54f_ISO_BONU.Ressecid = '')
     THEN ':35B:' + v54f_ISO_BONU.Sidname +' '+ v54f_ISO_BONU.Sid
     WHEN v54f_ISO_BONU.SecID = v54f_ISO_BONU.Ressecid
     THEN ':35B:' + v54f_ISO_BONU.Sidname +' '+ v54f_ISO_BONU.Sid
     END,
CASE WHEN v54f_ISO_BONU.RatioNew <>''
            AND v54f_ISO_BONU.RatioNew IS NOT NULL
          AND v54f_ISO_BONU.RatioOld <>''
            AND v54f_ISO_BONU.RatioOld IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(v54f_ISO_BONU.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_BONU.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN v54f_ISO_BONU.Paydate <>'' 
               AND v54f_ISO_BONU.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_BONU.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v54f_ISO_BONU
WHERE 
SID <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
