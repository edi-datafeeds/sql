--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXOF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v51f_ISO_EXOF.Changed,
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(v51f_ISO_EXOF.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'127'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v51f_ISO_EXOF.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v51f_ISO_EXOF.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v51f_ISO_EXOF.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v51f_ISO_EXOF.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v51f_ISO_EXOF.Sidname +' '+ v51f_ISO_EXOF.Sid,
substring(v51f_ISO_EXOF.Issuername,1,35) as TIDYTEXT,
substring(v51f_ISO_EXOF.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v51f_ISO_EXOF.Sedol <> '' and v51f_ISO_EXOF.Sidname <> '/GB/'
     THEN '/GB/' + v51f_ISO_EXOF.Sedol
     ELSE ''
     END,
CASE WHEN v51f_ISO_EXOF.Localcode <> ''
     THEN '/TS/' + v51f_ISO_EXOF.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v51f_ISO_EXOF.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v51f_ISO_EXOF.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v51f_ISO_EXOF.EndDate <>'' AND v51f_ISO_EXOF.EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , v51f_ISO_EXOF.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v51f_ISO_EXOF.EndDate <>'' AND v51f_ISO_EXOF.EndDate IS NOT NULL
       AND v51f_ISO_EXOF.StartDate <>'' AND v51f_ISO_EXOF.StartDate IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , v51f_ISO_EXOF.StartDate,112)+'/'
          +CONVERT ( varchar , v51f_ISO_EXOF.EndDate,112)
     WHEN v51f_ISO_EXOF.StartDate <>'' AND v51f_ISO_EXOF.StartDate IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , v51f_ISO_EXOF.StartDate,112)+'/UKWN'
     WHEN v51f_ISO_EXOF.EndDate <>'' AND v51f_ISO_EXOF.EndDate IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , v51f_ISO_EXOF.EndDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v51f_ISO_EXOF.ResSID <> ''
     THEN ':35B:' + v51f_ISO_EXOF.resSIDname +' '+ v51f_ISO_EXOF.ResSID
     END,
':92D::NEWO//1,/1,',
/* Currently no suitable paydate
CASE WHEN v51f_ISO_EXOF.Paydate <>'' 
             AND v51f_ISO_EXOF.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v51f_ISO_EXOF.Paydate,112)
     WHEN v51f_ISO_EXOF.paydate <>'' 
             AND v51f_ISO_EXOF.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v51f_ISO_EXOF.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v51f_ISO_EXOF
WHERE 
SID <> ''
and Actflag <> ''
and upper(eventtype)<>'CLEAN'
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3
