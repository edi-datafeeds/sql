--FilePath=O:\Datafeed\15022\EQ_GB\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_PRIO
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--Archive=on
--ArchivePath=n:\15022\EQ_GB\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select    
v54f_ISO_PRIO.Changed,
'select  PrfNotes As Notes FROM PRF WHERE RDID = '+ cast(v54f_ISO_PRIO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'110'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_PRIO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v54f_ISO_PRIO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_PRIO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_PRIO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + v54f_ISO_PRIO.Sidname +' '+ v54f_ISO_PRIO.Sid,
substring(v54f_ISO_PRIO.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_PRIO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_PRIO.Sedol <> '' and v54f_ISO_PRIO.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_PRIO.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_PRIO.Localcode <> ''
     THEN '/TS/' + v54f_ISO_PRIO.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_PRIO.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_PRIO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_PRIO.Exdate <>'' AND v54f_ISO_PRIO.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_PRIO.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN v54f_ISO_PRIO.Recdate <>'' AND v54f_ISO_PRIO.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v54f_ISO_PRIO.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
 /* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v54f_ISO_PRIO.EventID as char(16)) as Notes, */ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
':17B::DFLT//N',
CASE WHEN v54f_ISO_PRIO.CurenCD <> '' AND v54f_ISO_PRIO.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + v54f_ISO_PRIO.CurenCD
     END,
CASE WHEN v54f_ISO_PRIO.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v54f_ISO_PRIO.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v54f_ISO_PRIO.EndSubscription <>'' AND v54f_ISO_PRIO.EndSubscription IS NOT NULL
       AND v54f_ISO_PRIO.StartSubscription <>'' AND v54f_ISO_PRIO.StartSubscription IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , v54f_ISO_PRIO.StartSubscription,112)+'/'
          +CONVERT ( varchar , v54f_ISO_PRIO.EndSubscription,112)
     WHEN v54f_ISO_PRIO.StartSubscription <>'' AND v54f_ISO_PRIO.StartSubscription IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , v54f_ISO_PRIO.StartSubscription,112)+'/UKWN'
     WHEN v54f_ISO_PRIO.EndSubscription <>'' AND v54f_ISO_PRIO.EndSubscription IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , v54f_ISO_PRIO.EndSubscription,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN v54f_ISO_PRIO.MaxPrice <>'' AND v54f_ISO_PRIO.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+v54f_ISO_PRIO.CurenCD+
          +substring(v54f_ISO_PRIO.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN v54f_ISO_PRIO.ResSID <> ''
     THEN ':35B:' + v54f_ISO_PRIO.resSIDname +' '+ v54f_ISO_PRIO.ResSID
     END,
CASE WHEN v54f_ISO_PRIO.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v54f_ISO_PRIO.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v54f_ISO_PRIO.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v54f_ISO_PRIO.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v54f_ISO_PRIO.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v54f_ISO_PRIO.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v54f_ISO_PRIO.RatioNew <>''
            AND v54f_ISO_PRIO.RatioNew IS NOT NULL
          AND v54f_ISO_PRIO.RatioOld <>''
            AND v54f_ISO_PRIO.RatioOld IS NOT NULL
     THEN ':92D::NEWO//'+ltrim(cast(v54f_ISO_PRIO.RatioNew as char (15)))+
                    '/'+ltrim(cast(v54f_ISO_PRIO.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN v54f_ISO_PRIO.paydate2 <>'' 
             AND v54f_ISO_PRIO.paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_PRIO.paydate2,112)
     WHEN v54f_ISO_PRIO.paydate <>'' 
             AND v54f_ISO_PRIO.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_PRIO.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v54f_ISO_PRIO
WHERE 
SID <> ''
and Actflag <> ''
and changed >= (select max(acttime) from tbl_Opslog where seq = 1)
and cntrycd = 'GB'
order by caref1, caref2 desc, caref3

