--SEMETABLE=client.dbo.seme_fnz
--FileName=edi_YYYYMMDD_mt564
--XMET_AGM_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_FNZ_Isin\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=


--# 1
use wca
select
Changed,
'' as ISOHDR,
':16R:GENL',
'161'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//' + substring(rtrim(wca2.dbo.t_EQ_MEET.Add1),1,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Add1),36,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Add2),1,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Add2),36,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Add3),1,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Add4),1,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Add5),1,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Add6),1,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.City),1,35) as TIDYTEXT,
substring(rtrim(wca2.dbo.t_EQ_MEET.Country),1,35) as TIDYTEXT,
':16S:ADDINFO',
'-}$'
From wca2.dbo.t_EQ_MEET
WHERE 
mainTFB1<>''
and (mcd<>'' and mcd is not null)
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (AGMDate>getdate()-183
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
*/
and (AGMEGM = 'EG'
or AGMEGM = 'SG')
order by caref2 desc, caref1, caref3
