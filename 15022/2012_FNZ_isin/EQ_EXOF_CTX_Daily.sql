--SEMETABLE=client.dbo.seme_fnz
--FileName=edi_YYYYMMDD_mt564
--EXOF_CTX_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_EOD.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_FNZ_Isin\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'127'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
CASE WHEN EndDate <>'' AND EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndDate <>'' AND EndDate IS NOT NULL
       AND StartDate <>'' AND StartDate IS NOT NULL
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/'
          +CONVERT ( varchar , EndDate,112)
     WHEN StartDate <>'' AND StartDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/UKWN'
     WHEN EndDate <>'' AND EndDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>'UKWN' and resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
/*':92D::NEWO//1,/1,',*/
':92K::NEWO//UKWN',
CASE WHEN StartDate <>'' AND StartDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From wca2.dbo.t_EQ_EXOF_CTX
WHERE 
mainTFB1<>''
/**/and (mcd<>'' and mcd is not null)
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (EndDate>getdate()-183
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
*/
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3