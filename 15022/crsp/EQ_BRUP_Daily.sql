--FilePath=o:\DataFeed\15022\crsp\
--FilePrefix=
--BRUP
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_BRUP.Changed,
'select  BkrpNotes As Notes FROM BKRP WHERE BKRPID = '+ cast(v_EQ_BRUP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'119'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_BRUP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_BRUP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_BRUP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_BRUP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BRUP',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_BRUP.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_BRUP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_BRUP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_BRUP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_BRUP.NotificationDate <>''
     THEN ':98A::ANOU//'+CONVERT ( varchar , v_EQ_BRUP.NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN v_EQ_BRUP.FilingDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_BRUP.FilingDate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From v_EQ_BRUP
WHERE 
uscode<>''
and (CalcListdate<=NotificationDate or (NotificationDate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=NotificationDate or (NotificationDate is null and CalcDelistdate>=AnnounceDate))
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3