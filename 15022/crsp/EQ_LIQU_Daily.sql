--FilePath=o:\DataFeed\15022\crsp\
--FilePrefix=
--LIQU
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\crsp\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=LIQ
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
'select distinct * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + rtrim(cast(v_EQ_LIQU.EventID as char(10))) + ' and sEvent = '+ char(39) + 'LIQ'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
v_EQ_LIQU.Changed,
'select  LiquidationTerms As Notes FROM LIQ WHERE LIQID = '+ cast(v_EQ_LIQU.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'113'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_LIQU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_LIQU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_LIQU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_LIQU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_LIQU.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_LIQU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_LIQU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_LIQU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_LIQU.RdDate <>'' AND v_EQ_LIQU.RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_LIQU.RdDate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_LIQU
WHERE 
uscode<>''
and (CalcListdate<=RdDate or (RdDate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=RdDate or (RdDate is null and CalcDelistdate>=AnnounceDate))
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3