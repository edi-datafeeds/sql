--FilePath=o:\DataFeed\15022\crsp\
--FilePrefix=
--RHTS
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_RHTS_ENT.Changed,
'select  ENTNotes As Notes FROM ENT WHERE RDID = '+ cast(v_EQ_RHTS_ENT.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'116'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_RHTS_ENT.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_RHTS_ENT.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_RHTS_ENT.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_RHTS_ENT.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHTS',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_RHTS_ENT.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_RHTS_ENT.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_RHTS_ENT.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_RHTS_ENT.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <>''
AND v_EQ_RHTS_ENT.EndSubscription IS NOT NULL
       AND v_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.StartSubscription,112)+'/'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     WHEN v_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.StartSubscription,112)+'/UKWN'
     WHEN v_EQ_RHTS_ENT.EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_RHTS_ENT.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN v_EQ_RHTS_ENT.CurenCD <> '' 
     THEN ':11A::OPTN//' + v_EQ_RHTS_ENT.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN v_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.Paydate <> '' and v_EQ_RHTS_ENT.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_RHTS_ENT.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN v_EQ_RHTS_ENT.EntIssuePrice <> ''
     THEN ':90B::PRPP//ACTU/'+CurenCD+ltrim(cast(v_EQ_RHTS_ENT.EntIssuePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB3<>''
     THEN ':35B:' + resTFB3
     ELSE ':35B:/US/' + uscode
     END,
CASE WHEN v_EQ_RHTS_ENT.RatioNew <>''
          AND v_EQ_RHTS_ENT.RatioOld <>''
     THEN ':92D::NWRT//'+ltrim(cast(v_EQ_RHTS_ENT.RatioNew as char (15)))+
                    '/'+ltrim(cast(v_EQ_RHTS_ENT.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_RHTS_ENT
WHERE 
uscode<>''
and (CalcListdate<=Exdate or (Exdate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=AnnounceDate))
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3