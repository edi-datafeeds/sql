--FilePath=o:\DataFeed\15022\crsp\
--FilePrefix=
--SPLR
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_SPLR.Changed,
'select  CONSDNotes As Notes FROM CONSD WHERE RdID = '+ cast(v_EQ_SPLR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'121'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_SPLR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_SPLR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_SPLR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_SPLR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_SPLR.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_SPLR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_SPLR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_SPLR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_SPLR.Exdate <>'' AND v_EQ_SPLR.Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_SPLR.Exdate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
CASE WHEN v_EQ_SPLR.Recdate <>'' AND v_EQ_SPLR.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_SPLR.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v_EQ_SPLR.NewParValue <> ''
     THEN ':70E::TXNR//'
     ELSE '' END,
CASE WHEN v_EQ_SPLR.OldParValue <> ''
     THEN 'Old Par Value ' + v_EQ_SPLR.OldParValue
     ELSE ''
     END,
CASE WHEN v_EQ_SPLR.NewParValue <> ''
     THEN 'New Par Value ' + v_EQ_SPLR.NewParValue
     ELSE ''
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_EQ_SPLR.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_SPLR.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_SPLR.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_SPLR.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_SPLR.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_SPLR.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newTFB3<>''
     THEN ':35B:' + newTFB3
     ELSE ':35B:/US/' + uscode
     END,
CASE WHEN v_EQ_SPLR.RatioNew <>''
            AND v_EQ_SPLR.RatioOld <>''
     THEN ':92D::NEWO//'+ltrim(cast(v_EQ_SPLR.RatioNew as char (15)))+
                    '/'+ltrim(cast(v_EQ_SPLR.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
CASE WHEN v_EQ_SPLR.Paydate <>'' 
               AND v_EQ_SPLR.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_SPLR.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_SPLR
WHERE 
uscode<>''
and (CalcListdate<=Exdate or (Exdate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=AnnounceDate))
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3