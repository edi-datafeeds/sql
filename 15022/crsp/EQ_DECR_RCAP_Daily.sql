--FilePath=o:\DataFeed\15022\crsp\
--FilePrefix=
--DECR
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_DECR_RCAP.Changed,
'select  RcapNotes As Notes FROM RCAP WHERE RCAPID = '+ cast(v_EQ_DECR_RCAP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'146'+v_EQ_DECR_RCAP.EXCHGID+cast(v_EQ_DECR_RCAP.Seqnum as char(1)) as CAref1,
v_EQ_DECR_RCAP.EventID as CAREF2,
v_EQ_DECR_RCAP.SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_DECR_RCAP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_DECR_RCAP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_DECR_RCAP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_DECR_RCAP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_DECR_RCAP.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_DECR_RCAP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_DECR_RCAP.MCD <>''
     THEN ':94B::PLIS//EXCH/' +v_EQ_DECR_RCAP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_DECR_RCAP.RecDate <>'' and v_EQ_DECR_RCAP.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_DECR_RCAP.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v_EQ_DECR_RCAP.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_DECR_RCAP.EffectiveDate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
CASE WHEN v_EQ_DECR_RCAP.CSPYDate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DECR_RCAP.CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_EQ_DECR_RCAP.CurenCD <> '' 
     THEN ':11A::OPTN//' + v_EQ_DECR_RCAP.CurenCD
     END,
':17B::DFLT//Y',
CASE WHEN v_EQ_DECR_RCAP.CashBak <>'' AND v_EQ_DECR_RCAP.CashBak IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+v_EQ_DECR_RCAP.CurenCD+
          +substring(v_EQ_DECR_RCAP.CashBak,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as Commasub,
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_DECR_RCAP
LEFT OUTER JOIN wca.dbo.CAPRD on v_EQ_DECR_RCAP.SecID = wca.dbo.CAPRD.SecID
                    and v_EQ_DECR_RCAP.EffectiveDate = wca.dbo.CAPRD.EffectiveDate
WHERE 
uscode<>''
and wca.dbo.CAPRD.Secid is null
and (CalcListdate<=v_EQ_DECR_RCAP.effectivedate or (v_EQ_DECR_RCAP.effectivedate is null and CalcListdate<=v_EQ_DECR_RCAP.AnnounceDate))
and (CalcDelistdate>=v_EQ_DECR_RCAP.effectivedate or (v_EQ_DECR_RCAP.effectivedate is null and CalcDelistdate>=v_EQ_DECR_RCAP.AnnounceDate))
AND (v_EQ_DECR_RCAP.secid not in (select secid from portfolio.dbo.PFDAY))
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd
      
order by caref2 desc, caref1, caref3