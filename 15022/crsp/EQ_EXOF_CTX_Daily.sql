--FilePath=o:\DataFeed\15022\crsp\
--FilePrefix=
--EXOF
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_EXOF_CTX.Changed,
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(v_EQ_EXOF_CTX.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'127'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_EXOF_CTX.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_EXOF_CTX.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_EXOF_CTX.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_EXOF_CTX.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_EXOF_CTX.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_EXOF_CTX.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_EXOF_CTX.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_EXOF_CTX.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_EXOF_CTX.EndDate <>'' AND v_EQ_EXOF_CTX.EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_EXOF_CTX.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_EXOF_CTX.EndDate <>'' AND v_EQ_EXOF_CTX.EndDate IS NOT NULL
       AND v_EQ_EXOF_CTX.StartDate <>'' AND v_EQ_EXOF_CTX.StartDate IS NOT NULL
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_EXOF_CTX.StartDate,112)+'/'
          +CONVERT ( varchar , v_EQ_EXOF_CTX.EndDate,112)
     WHEN v_EQ_EXOF_CTX.StartDate <>'' AND v_EQ_EXOF_CTX.StartDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_EXOF_CTX.StartDate,112)+'/UKWN'
     WHEN v_EQ_EXOF_CTX.EndDate <>'' AND v_EQ_EXOF_CTX.EndDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_EXOF_CTX.EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB3<>''
     THEN ':35B:' + resTFB3
     ELSE ':35B:ISIN UKWN'
     END,
':92D::NEWO//1,/1,',
/* Currently no suitable paydate
CASE WHEN v_EQ_EXOF_CTX.Paydate <>'' 
             AND v_EQ_EXOF_CTX.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_EXOF_CTX.Paydate,112)
     WHEN v_EQ_EXOF_CTX.paydate <>'' 
             AND v_EQ_EXOF_CTX.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_EXOF_CTX.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_EXOF_CTX
WHERE 
uscode<>''
and upper(eventtype)<>'CLEAN'
and (CalcListdate<=StartDate or (StartDate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=StartDate or (StartDate is null and CalcDelistdate>=AnnounceDate))
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3