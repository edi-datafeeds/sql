--FilePath=o:\DataFeed\15022\crsp\
--FilePrefix=
--BONU
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_BONU.Changed,
'select  BonNotes As Notes FROM BON WHERE RDID = '+ cast(v_EQ_BONU.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'114'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_EQ_BONU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_BONU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_BONU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_BONU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BONU',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_BONU.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_BONU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_BONU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_BONU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_BONU.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_EQ_BONU.Exdate,112)
     ELSE ':98A::XDTE//UKWN'
     END as DateEx,
CASE WHEN v_EQ_BONU.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_BONU.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END as DateRec,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_EQ_BONU.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_BONU.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_BONU.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_BONU.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_BONU.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_BONU.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB3<>''
     THEN ':35B:' + resTFB3
     ELSE ':35B:/US/' + uscode
     END,
CASE WHEN v_EQ_BONU.RatioNew <>''
          AND v_EQ_BONU.RatioOld <>''
     THEN ':92D::ADEX//'+ltrim(cast(v_EQ_BONU.RatioNew as char (15)))+
                    '/'+ltrim(cast(v_EQ_BONU.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN v_EQ_BONU.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_BONU.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_BONU
WHERE 
uscode<>''
and (CalcListdate<=Exdate or (Exdate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=AnnounceDate))
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3