--SEMETABLE=client.dbo.seme_accenture
--FileName=aptp_tlmca_ca_camkt_YYYYMMDD
--PARI_FI564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=350
--TEXTCLEAN=2
--SUPPADDTAGS=N

--# 1
use wca
select distinct
Changed,
case when 1=1 then (select '{1:F01SAMPLEXXXXXO0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.ExpiryDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.ExpiryDate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '219' END as CAref1, */
'150' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PARI' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN cast(parvalue as float)>1 and parvalue like '%.00000%' 
     THEN ':36B::MINO//FAMT/'+rtrim(replace(ParValue,'.00000','.'))
     WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue
     ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExpiryDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , ExpiryDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN len(resTFB1)>10
     THEN ':35B:' + resTFB1
     ELSE ':35B:/US/999999999'
     END,
CASE WHEN len(resTFB1)<10
     THEN 'NOT AVAILABLE AT PRESENT'
     ELSE ''
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN OldOutValue <>'' AND NewOutValue <>''
     THEN ':92D::NEWO//'+rtrim(cast(NewOutValue as char (15)))+
                    '/'+rtrim(cast(OldOutValue as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
CASE WHEN ExpiryDate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , ExpiryDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_FI_PARI_2 as maintab
WHERE
maintab.sedol<>'' and maintab.sedol is not null
and ((maintab.sedol in (select code from client.dbo.pfsedol where accid=274 and actflag='I')
and ExpiryDate > getdate())
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=274 and actflag='U')
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)))
order by caref2 desc, caref1, caref3
