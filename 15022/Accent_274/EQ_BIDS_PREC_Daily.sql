--SEMETABLE=client.dbo.seme_accenture
--FileName=aptp_tlmca_ca_camkt_YYYYMMDD
--BIDS_PREC_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=350
--TEXTCLEAN=2

--# 1
use wca
select distinct
case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end as Changed,
'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Enddate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Enddate is not null
              and SR.TFB<>':35B:/GB/'+maintab.Sedol
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '115' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,112) + replace(convert ( varchar, case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE '' END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN StartDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN maintab.MaxQlyQty<>''
     THEN ':36B::QTSO//UNIT/'+substring(maintab.MaxQlyQty,1,15) + ','
     ELSE '' END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
':11A::OPTN//USD',
':17B::DFLT//N',
CASE WHEN Enddate<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , Enddate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN Enddate<>'' AND Startdate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Startdate,112)+'/'
          +CONVERT ( varchar , Enddate,112)
     WHEN Startdate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Startdate,112)+'/UKWN'
     WHEN Enddate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Enddate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN MaxAcpQty <>''
     THEN ':36B::FOLQ//UNIT/'+MaxAcpQty+','
     ELSE ':36C::FOLQ//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.PayDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , maintab.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':90E::OFFR//UKWN',
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}'
From v_EQ_BIDS as maintab
left outer join client.dbo.seme_accenture as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'BIDS'=SR.CAEVMAIN
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'BB' = wca.dbo.mpay.sevent
                                             and 'D'<>wca.dbo.mpay.actflag
WHERE
maintab.sedol<>'' and maintab.sedol is not null
and ((maintab.sedol in (select code from client.dbo.pfsedol where accid=274 and actflag='I')
and enddate > getdate())
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=274 and actflag='U')
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)))
and wca.dbo.mpay.eventid is null
order by caref2 desc, caref1, caref3
