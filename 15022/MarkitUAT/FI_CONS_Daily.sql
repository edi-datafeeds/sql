--SEMETABLE=client.dbo.seme_markituat
--FileName=edi_UAT_YYYYMMDD
-- YYYYMMDD_FI564_CONS
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=250000
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.changed,
'select case when len(cast(Notes as varchar(24)))=22 then rtrim(char(32)) else Notes end As Notes FROM COSNT WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01MMCAUAT2AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NEQ}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '416'+EXCHGID+cast(Seqnum as char(1))
     ELSE '416'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':98B::ANOU//UKWN',
CASE WHEN maintab.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , maintab.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':22F::CONS//CTRM',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//CONY',
':17B::DFLT//N',
CASE WHEN MinimumDenomination<>'' THEN ':36B::MIEX//FAMT/'+rtrim(cast(MinimumDenomination as char (20))) ELSE '' END as COMMASUB,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN 1=2 AND Expirydate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/'
          +CONVERT ( varchar , Expirydate,112)
     WHEN 1=2
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/UKWN'
     WHEN Expirydate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Expirydate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//003',
':22F::CAOP//CONN',
':17B::DFLT//N',
CASE WHEN MinimumDenomination<>'' THEN ':36B::MIEX//FAMT/'+rtrim(cast(MinimumDenomination as char (20))) ELSE '' END as COMMASUB,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN 1=2 AND Expirydate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/'
          +CONVERT ( varchar , Expirydate,112)
     WHEN 1=2
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/UKWN'
     WHEN Expirydate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Expirydate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CAOPTN',
'' as RawNotes, 
'-}$'
From v_FI_CONS as maintab
WHERE
mainTFB1<>''
and changed>'2019/05/13 02:00:00' and changed<'2019/05/13 07:40:00'
and (maintab.primaryexchgcd=exchgcd or maintab.primaryexchgcd='')
and cntrycd='GB'
order by caref2 desc, caref1, caref3
