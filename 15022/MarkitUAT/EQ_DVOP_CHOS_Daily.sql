--SEMETABLE=client.dbo.seme_markituat
--FileName=edi_UAT_YYYYMMDD
--DVCA_CHOS_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\smartstream\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DIV
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=250000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'' as bbgcntry,
'' as BBGCUREN,
'select case when len(cast(DivNotes as varchar(255)))>40 then DivNotes else rtrim(char(32)) end As Notes FROM DIV WHERE DIVID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
maintab.frankdiv as FLFR,
maintab.unfrankdiv as UNFR,
case when maintab.StructCD<>'XREIT' then '' else maintab.StructCD end as structcd,
case when 1=1 then (select '{1:F01MMCAUAT2AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NEQ}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN (maintab.exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.exdate is not null
       OR maintab.recdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.recdate is not null)
              and (SR.CAMV<>'CHOS' or SR.TFB<>':35B:'+mainTFB1)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '105' END as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DVOP' as CAEV,
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     ELSE ''
     END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN DuebillsRedemDate IS NOT NULL
     THEN ':98A::MCTD//'+CONVERT ( varchar , DuebillsRedemDate,112)
     ELSE ''
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
'' as StartseqE,
'' as RawNotes, 
'-}$'
From v_EQ_DV_CA_SE_OP as maintab
left outer join v10s_divpy as divopt1 on maintab.eventid = divopt1.divid 
                   and 1=divopt1.optionid and 'D'<>divopt1.actflag
                   and 1=divopt1.optionid and 'C'<>divopt1.actflag
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid 
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid 
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
left outer join v10s_divpy as divopt4 on maintab.eventid = divopt4.divid 
                   and 4=divopt4.optionid and 'D'<>divopt4.actflag
                   and 4=divopt4.optionid and 'C'<>divopt4.actflag
left outer join v10s_divpy as divopt5 on maintab.eventid = divopt5.divid 
                   and 5=divopt5.optionid and 'D'<>divopt5.actflag
                   and 5=divopt5.optionid and 'C'<>divopt5.actflag
left outer join v10s_divpy as divopt6 on maintab.eventid = divopt6.divid 
                   and 6=divopt6.optionid and 'D'<>divopt6.actflag
                   and 6=divopt6.optionid and 'C'<>divopt6.actflag
left outer join v10s_divpy as divopt7 on maintab.eventid = divopt7.divid 
                   and 7=divopt7.optionid and 'D'<>divopt7.actflag
                   and 7=divopt7.optionid and 'C'<>divopt7.actflag
left outer join v10s_divpy as divopt8 on maintab.eventid = divopt8.divid 
                   and 8=divopt8.optionid and 'D'<>divopt8.actflag
                   and 8=divopt8.optionid and 'C'<>divopt8.actflag
left outer join client.dbo.seme_markituat as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'DVOP'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and changed>'2019/05/13 02:00:00' and changed<'2019/05/13 07:40:00'
and (maintab.primaryexchgcd=exchgcd or maintab.primaryexchgcd='')
and maintab.cntrycd='GB'
and (maintab.dripcntrycd='' or maintab.cntrycd='US')
and not ((divopt1.divtype = 'C' or divopt1.actflag is null)
and (divopt2.divtype = 'C' or divopt2.actflag is null)
and (divopt3.divtype = 'C' or divopt3.actflag is null)
and (divopt4.divtype = 'C' or divopt4.actflag is null)
and (divopt5.divtype = 'C' or divopt5.actflag is null)
and (divopt6.divtype = 'C' or divopt6.actflag is null)
and (divopt7.divtype = 'C' or divopt7.actflag is null)
and (divopt8.divtype = 'C' or divopt8.actflag is null) 
)
and ((divopt1.actflag is not null and divopt2.actflag is not null)
  or (divopt1.actflag is not null and divopt3.actflag is not null)      
  or (divopt2.actflag is not null and divopt3.actflag is not null))
and (paydate>getdate()-183 or paydate is null)
order by caref2 desc, caref1, caref3
