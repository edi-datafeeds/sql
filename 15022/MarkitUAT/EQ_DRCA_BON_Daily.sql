--SEMETABLE=client.dbo.seme_markituat
--FileName=edi_UAT_YYYYMMDD
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=250000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(bonnotes as varchar(255)))>40 then bonnotes else rtrim(char(32)) end As Notes FROM BON WHERE BonID = '+ cast(maintab.BonID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01MMCAUAT2AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NEQ}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.paydate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.paydate is not null
              and (SR.CAMV<>'MAND' or SR.TFB<>':35B:'+mainTFB1)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '107' END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN  maintab.Actflag = 'D' or maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'C' or maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN  maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEP//DISN',
':22F::CAEV//DRCA' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
':11A::OPTN//USD',
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  Paydate <>'' AND Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
/* Depfees unknown */
':92K::CHAR//UKWN',
':90B::OFFR//ACTU/USD'+ltrim(cast(LapsedPremium as char(15))) as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as RawNotes, 
'-}$'
From v_EQ_BONU as maintab
left outer join client.dbo.seme_markituat as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'BONU'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and changed>'2019/05/13 02:00:00' and changed<'2019/05/13 07:40:00'
and (maintab.primaryexchgcd=exchgcd or maintab.primaryexchgcd='')
and maintab.cntrycd='GB'
and sectycd='DR' and lapsedpremium<>''
and (paydate>getdate()-183 or paydate is null)
order by caref2 desc, caref1, caref3
