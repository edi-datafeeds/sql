--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=DECR
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
v_EQ_DECR_CAPRD.Changed,
'select CapRdNotes As Notes FROM CAPRD WHERE CAPRDID = '+ cast(v_EQ_DECR_CAPRD.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'148'+v_EQ_DECR_CAPRD.EXCHGID+cast(v_EQ_DECR_CAPRD.Seqnum as char(1)) as CAref1,
v_EQ_DECR_CAPRD.EventID as CAREF2,
v_EQ_DECR_CAPRD.SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_DECR_CAPRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_DECR_CAPRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_DECR_CAPRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_DECR_CAPRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_DECR_CAPRD.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_DECR_CAPRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_DECR_CAPRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_DECR_CAPRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN  v_EQ_DECR_CAPRD.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_DECR_CAPRD.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v_EQ_DECR_CAPRD.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_DECR_CAPRD.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/*CASE WHEN v_EQ_DECR_CAPRD.OldParValue <> '' or v_EQ_DECR_CAPRD.NewParValue <> ''
     THEN ':70E::TXNR//'
     ELSE '' END,
CASE WHEN v_EQ_DECR_CAPRD.OldParValue <> ''
     THEN 'Old Par Value ' +v_EQ_DECR_CAPRD.OldParValue
     ELSE ''
     END, 
CASE WHEN v_EQ_DECR_CAPRD.NewParValue <> ''
     THEN 'New Par Value ' +v_EQ_DECR_CAPRD.NewParValue
     ELSE ''
     END, */
':16S:CADETL',
/*CASE WHEN  wca.dbo.RCAP.CashBak <>'' AND wca.dbo.RCAP.CashBak IS NOT NULL
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN  wca.dbo.RCAP.CashBak <>'' AND wca.dbo.RCAP.CashBak IS NOT NULL
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN  wca.dbo.RCAP.CashBak <>'' AND wca.dbo.RCAP.CashBak IS NOT NULL
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN  wca.dbo.RCAP.CashBak <>'' AND wca.dbo.RCAP.CashBak IS NOT NULL
     THEN ':11A::OPTN//' + wca.dbo.RCAP.CurenCD
     END,
CASE WHEN  wca.dbo.RCAP.CashBak <>'' AND wca.dbo.RCAP.CashBak IS NOT NULL
     THEN ':17B::DFLT//Y'
     ELSE ''
     END,
CASE WHEN wca.dbo.RCAP.CashBak <>'' AND wca.dbo.RCAP.CashBak IS NOT NULL and newTFB1<>'UKWN'
     THEN ':35B:' + newTFB1
     ELSE ''
     END,
CASE WHEN wca.dbo.RCAP.CashBak <>'' AND wca.dbo.RCAP.CashBak IS NOT NULL
                  and wca.dbo.RCAP.CurenCD<>'' and wca.dbo.RCAP.CurenCD is not null
     THEN ':90B::OFFR//ACTU/'+wca.dbo.RCAP.CurenCD+
          +substring(wca.dbo.RCAP.CashBak,1,15)
     WHEN wca.dbo.RCAP.CashBak ='' AND wca.dbo.RCAP.CashBak IS NULL
     THEN ':90E::OFFR//UKWN'
     ELSE ''
     END as Commasub,*/
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN  wca.dbo.RCAP.CurenCD<>''
     THEN ':11A::OPTN//' + wca.dbo.RCAP.CurenCD
     ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN newTFB1<>'UKWN'
     THEN ':35B:' + newTFB1
     ELSE ''
     END,
CASE WHEN wca.dbo.RCAP.CurenCD<>'' and wca.dbo.RCAP.CashBak<>''
     THEN ':90B::OFFR//ACTU/'+wca.dbo.RCAP.CurenCD+
          +substring(wca.dbo.RCAP.CashBak,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as Commasub,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN wca.dbo.RCAP.CSPYDate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , wca.dbo.RCAP.CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_DECR_CAPRD
INNER JOIN wca.dbo.RCAP on v_EQ_DECR_CAPRD.SecID = wca.dbo.RCAP.SecID
                     and v_EQ_DECR_CAPRD.EffectiveDate = wca.dbo.RCAP.EffectiveDate
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and v_EQ_DECR_CAPRD.actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (v_EQ_DECR_CAPRD.sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or v_EQ_DECR_CAPRD.isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or v_EQ_DECR_CAPRD.uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (v_EQ_DECR_CAPRD.effectivedate>getdate()-8
     and (v_EQ_DECR_CAPRD.sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or v_EQ_DECR_CAPRD.isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or v_EQ_DECR_CAPRD.uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and wca.dbo.RCAP.SecID is not null
order by caref2 desc, caref1, caref3
