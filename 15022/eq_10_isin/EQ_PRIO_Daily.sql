--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--PRIO
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
v_EQ_PRIO.Changed,
'select  PrfNotes As Notes FROM PRF WHERE RDID = '+ cast(v_EQ_PRIO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'110'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_PRIO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_PRIO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_PRIO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_PRIO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO',
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_PRIO.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_PRIO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_PRIO.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_PRIO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_PRIO.Exdate <>'' AND v_EQ_PRIO.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_EQ_PRIO.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN v_EQ_PRIO.Recdate <>'' AND v_EQ_PRIO.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_PRIO.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
 /* RDNotes populated by rendering programme */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_PRIO.EventID as char(16)) as Notes, */ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
':17B::DFLT//N',
CASE WHEN v_EQ_PRIO.CurenCD <> '' AND v_EQ_PRIO.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + v_EQ_PRIO.CurenCD
     END,
CASE WHEN v_EQ_PRIO.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , v_EQ_PRIO.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN v_EQ_PRIO.EndSubscription <>''
       AND v_EQ_PRIO.StartSubscription <>''
      THEN ':69A::PWAL//'
          +CONVERT ( varchar , v_EQ_PRIO.StartSubscription,112)+'/'
          +CONVERT ( varchar , v_EQ_PRIO.EndSubscription,112)
     WHEN v_EQ_PRIO.StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , v_EQ_PRIO.StartSubscription,112)+'/UKWN'
     WHEN v_EQ_PRIO.EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , v_EQ_PRIO.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN v_EQ_PRIO.MaxPrice <>''
     THEN ':90B::PRPP//ACTU/'+v_EQ_PRIO.CurenCD+
          +substring(v_EQ_PRIO.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN v_EQ_PRIO.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_PRIO.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_PRIO.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_PRIO.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_PRIO.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_PRIO.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN v_EQ_PRIO.RatioNew <>''
          AND v_EQ_PRIO.RatioOld <>''
     THEN ':92D::NEWO//'+ltrim(cast(v_EQ_PRIO.RatioNew as char (15)))+
                    '/'+ltrim(cast(v_EQ_PRIO.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN v_EQ_PRIO.paydate2 <>'' 
             AND v_EQ_PRIO.paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_PRIO.paydate2,112)
     WHEN v_EQ_PRIO.paydate <>'' 
             AND v_EQ_PRIO.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_PRIO.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_PRIO
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3