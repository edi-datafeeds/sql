--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--PPMT
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
v_EQ_PPMT.Changed,
'select  CallNotes As Notes FROM CALL WHERE CALLID = '+ cast(v_EQ_PPMT.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'111'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_PPMT.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_PPMT.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_PPMT.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_PPMT.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PPMT',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_PPMT.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_PPMT.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_PPMT.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_PPMT.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_PPMT.DueDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_PPMT.DueDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_EQ_PPMT.CurenCD <> ''
     THEN ':11A::OPTN//' + v_EQ_PPMT.CurenCD
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN v_EQ_PPMT.DueDate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_PPMT.DueDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN v_EQ_PPMT.ToFacevalue <> ''
     THEN ':90B::PRPP//ACTU/'+curenCD+rtrim(cast(v_EQ_PPMT.ToFacevalue as char(15)))
     ELSE ':90E::PRPP//UKWN' END,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_PPMT
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Duedate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3