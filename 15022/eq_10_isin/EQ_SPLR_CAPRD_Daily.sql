--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--SPLR
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
v_EQ_DECR_CAPRD.Changed,
'select  CAPRDNotes As Notes FROM CAPRD WHERE CaprdID = '+ cast(v_EQ_DECR_CAPRD.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'123'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_DECR_CAPRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_DECR_CAPRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_DECR_CAPRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_DECR_CAPRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLR',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_DECR_CAPRD.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_DECR_CAPRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_DECR_CAPRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_DECR_CAPRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_DECR_CAPRD.Recdate <>'' AND v_EQ_DECR_CAPRD.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_DECR_CAPRD.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
/* CASE WHEN v_EQ_DECR_CAPRD.OldParValue <> '' or v_EQ_DECR_CAPRD.NewParValue <> ''
     THEN ':70E::TXNR//'
     ELSE '' END,
CASE WHEN v_EQ_DECR_CAPRD.OldParValue <> ''
     THEN 'Old Par Value ' +v_EQ_DECR_CAPRD.OldParValue
     ELSE ''
     END, 
CASE WHEN v_EQ_DECR_CAPRD.NewParValue <> ''
     THEN 'New Par Value ' +v_EQ_DECR_CAPRD.NewParValue
     ELSE ''
     END, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_EQ_DECR_CAPRD.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_DECR_CAPRD.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_DECR_CAPRD.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_DECR_CAPRD.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_DECR_CAPRD.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_DECR_CAPRD.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newTFB1<>'UKWN'
     THEN ':35B:' + newTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN v_EQ_DECR_CAPRD.NewRatio <>''
            AND v_EQ_DECR_CAPRD.OldRatio <>''
     THEN ':92D::NEWO//'+ltrim(cast(v_EQ_DECR_CAPRD.NewRatio as char (15)))+
                    '/'+ltrim(cast(v_EQ_DECR_CAPRD.OldRatio as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
CASE WHEN v_EQ_DECR_CAPRD.PayDate <>'' and v_EQ_DECR_CAPRD.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DECR_CAPRD.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_DECR_CAPRD
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Effectivedate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and OldRatio<>''
and NewRatio<>''
and cast(rtrim(OldRatio) as float(15,7))>
      cast(rtrim(NewRatio) as float(15,7))
order by caref2 desc, caref1, caref3