--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--DVSE
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
v_EQ_DVOP.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(v_EQ_DVOP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_DVOP.Actflag_1 = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVSE',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_DVOP.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_DVOP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_DVOP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_DVOP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_DVOP.Exdate <>'' AND v_EQ_DVOP.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v_EQ_DVOP.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v_EQ_DVOP.Recdate <>'' AND v_EQ_DVOP.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
/* RDNotes populated by rendering programme */
/*/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_DVOP.RdID as char(16)) as Notes, */*/ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_EQ_DVOP.Fractions_1 = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_DVOP.Fractions_1 = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_DVOP.Fractions_1 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_DVOP.Fractions_1 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_DVOP.Fractions_1 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_DVOP.Fractions_1 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1_1 <> ''
           THEN ':35B:' + resTFB1_1
           ELSE ':35B:' + mainTFB1
           END,
CASE WHEN v_EQ_DVOP.RatioNew_1 <>''
            AND v_EQ_DVOP.RatioNew_1 IS NOT NULL
            AND v_EQ_DVOP.RatioOld_1 <>''
            AND v_EQ_DVOP.RatioOld_1 IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(v_EQ_DVOP.RatioNew_1 as char (15)))+
                    '/'+ltrim(cast(v_EQ_DVOP.RatioOld_1 as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN v_EQ_DVOP.Paydate <>'' 
               AND v_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_DVOP.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_DVOP
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and actflag <> 'C'
and actflag_1 <> 'D'
and actflag_1 <> 'C'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and DIVType_1 = 'S'
AND (OpID_2 = '' or Actflag_2='C' or Actflag_2='D')
AND (OpID_3 = '' or Actflag_3='C' or Actflag_3='D')
AND OpID_4 = ''
AND OpID_5 = ''
AND OpID_6 = ''
and not ((actflag_1='D' or actflag_1='C') and (actflag_2='D' or actflag_2='C'))
and not ((actflag_1='D' or actflag_1='C') and (actflag_3='D' or actflag_3='C'))
order by caref2 desc, caref1, caref3