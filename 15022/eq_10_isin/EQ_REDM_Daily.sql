--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--REDM
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1

use wca
select top 5
v_EQ_REDM.Changed,
'select RedemNotes As Notes FROM REDEM WHERE RedemID = '+ cast(v_EQ_REDM.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'410'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_EQ_REDM.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_REDM.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_REDM.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_REDM.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDM',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_REDM.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_REDM.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_REDM.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + v_EQ_REDM.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_REDM.RedemDate is not null AND v_EQ_REDM.RedemDate IS NOT NULL
     THEN ':98A::REDM//'+CONVERT ( varchar , v_EQ_REDM.RedemDate,112)
     ELSE ':98A::REDM//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v_EQ_REDM.CurenCD  is not null
     THEN ':11A::OPTN//' + v_EQ_REDM.CurenCD
     ELSE ':11A::OPTN//UKWN'
     END,
':17B::DFLT//Y',
CASE WHEN v_EQ_REDM.RedemPercent  is not null
     THEN ':90B::REDM//PRCT/' + CONVERT ( varchar , v_EQ_REDM.RedemPercent,112)
     WHEN v_EQ_REDM.RedemPrice is not null
     THEN ':90B::REDM//ACTU/' + v_EQ_REDM.CurenCD + CONVERT ( varchar , v_EQ_REDM.RedemPrice,112)
     ELSE ':90F::REDM//PRCT/UKWN'
     END as COMMASUB,
':16S:CAOPTN',
/* Notes populated by rendering programme */
'' as Notes, 
'-}$'
From v_EQ_REDM
WHERE
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Redemdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and redemdate is not null
and redemdate <> '1800/01/01'
order by caref2 desc, caref1, caref3
