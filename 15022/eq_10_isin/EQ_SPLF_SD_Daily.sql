--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--SPLF
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
v_EQ_SPLF.Changed,
'select  SDNotes As Notes FROM SD WHERE RdID = '+ cast(v_EQ_SPLF.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'120'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_SPLF.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_SPLF.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_SPLF.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_SPLF.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLF',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_SPLF.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_SPLF.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_SPLF.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_SPLF.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_SPLF.Exdate <>'' AND v_EQ_SPLF.Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_SPLF.Exdate,112)
     ELSE ':98B::EFFD//UKWN'
     END as DateEx,
CASE WHEN v_EQ_SPLF.Recdate <>'' AND v_EQ_SPLF.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_SPLF.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
/* CASE WHEN v_EQ_SPLF.OldParValue <> '' or v_EQ_SPLF.NewParValue <> ''
     THEN ':70E::TXNR//'
     ELSE '' END,
CASE WHEN v_EQ_SPLF.OldParValue <> ''
     THEN 'Old Par Value ' + v_EQ_SPLF.OldParValue
     ELSE ''
     END,
CASE WHEN v_EQ_SPLF.NewParValue <> ''
     THEN 'New Par Value ' + v_EQ_SPLF.NewParValue
     ELSE ''
     END, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN v_EQ_SPLF.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN v_EQ_SPLF.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN v_EQ_SPLF.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN v_EQ_SPLF.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN v_EQ_SPLF.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN v_EQ_SPLF.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newTFB1<>'UKWN'
     THEN ':35B:' + newTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN v_EQ_SPLF.RatioNew <>''
          AND v_EQ_SPLF.RatioOld <>''
     THEN ':92D::NEWO//'+ltrim(cast(v_EQ_SPLF.RatioNew as char (15)))+
                    '/'+ltrim(cast(v_EQ_SPLF.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN v_EQ_SPLF.Paydate <>'' 
               AND v_EQ_SPLF.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_SPLF.paydate,112)
     ELSE ':98B::PAYD//UKWN' END as DatePay,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_SPLF
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3