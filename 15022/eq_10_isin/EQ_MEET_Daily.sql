--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--MEET
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
v_EQ_MEET.Changed,
'' as ISOHDR,
':16R:GENL',
'161'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_MEET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_MEET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_MEET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_MEET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN v_EQ_MEET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN v_EQ_MEET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN v_EQ_MEET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN v_EQ_MEET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN v_EQ_MEET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(v_EQ_MEET.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_MEET.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_MEET.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_MEET.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_MEET.AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , v_EQ_MEET.AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX// ' + substring(rtrim(v_EQ_MEET.Add1),1,22) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add1),23,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add2),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add2),36,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add3),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add4),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add5),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add6),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.City),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Country),1,35) as TIDYTEXT,
':16S:ADDINFO',
'-}$'
From v_EQ_MEET
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (AGMDate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    )
and v_EQ_MEET.AGMEGM = 'AG'

order by caref2 desc, caref1, caref3
