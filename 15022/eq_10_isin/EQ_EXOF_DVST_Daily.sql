UNFINISHED
--FilePath=c:\Datafeed\15022\eq_sample\
--FilePrefix=
--EXOF
--FileName=15022_EQ_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=off
--ArchivePath=n:\15022\citihfs\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select top 5
Changed,
'select  DvstNotes As Notes FROM DVST WHERE RDID = '+ cast(v_EQ_EXOF_DVST.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'128'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <> ''
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':16S:CADETL',
CASE WHEN MaxPrice <>'' or (RatioNew <>'' and RatioOld <>'')
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN MaxPrice <>'' or (RatioNew <>'' and RatioOld <>'')
     THEN '':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN MaxPrice <>'' and RatioNew <>'' and RatioOld <>''
     THEN ':22F::CAOP//CASE'
     WHEN RatioNew <>'' and RatioOld <>''
     THEN ':22F::CAOP//SECU'
     WHEN MaxPrice <>''
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN MaxPrice <>'' or (RatioNew <>'' and RatioOld <>'')
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN EndSubscription <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN MaxPrice <>'' and CurenCD <> '' 
     THEN ':11A::OPTN//' + CurenCD
     ELSE ''
     END,
CASE WHEN MaxPrice <>'' and CurenCD <> ''
     THEN ':90B::PRPP//ACTU/'+CurenCD+
          +substring(MaxPrice,1,15)
     ELSE ''
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
':92D::NEWO//1,/1,',
CASE WHEN Paydate <>'' 
             AND Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , Paydate,112)
     WHEN paydate <>'' 
             AND paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
CASE WHEN RatioNew <>''
               AND RatioOld <>''
     THEN ':92D::ADEX//'+ltrim(cast(RatioNew as char (15)))+
                    '/'+ltrim(cast(RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
from v_EQ_EXOF_DVST
WHERE 
mainTFB1<>''
and exchgcd=primaryexchgcd
and actflag <> 'D'
and (mcd<>'' and mcd is not null)
AND (changed>='2010/01/01'
and EndSubscription =''
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-8
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
**/
    ) 
order by caref2 desc, caref1, caref3
