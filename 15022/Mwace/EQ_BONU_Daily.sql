--SEMETABLE=client.dbo.seme_mwace
--FileName=edi_YYYYMMDD
--BONU_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\mwace\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=0
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(BonNotes as varchar(255)))>40 then BonNotes else rtrim(char(32)) end As Notes FROM BON WHERE BONID = '+ cast(maintab.BonID as char(16)) as ChkNotes,
'{1:F01EDIXXXXXXISO0300000054}{2:O5641825151030EDIXXXXXXISO03000000541510301825N}{4:' as ISOHDR,
':16R:GENL',
'114'+EXCHGID+cast(maintab.Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BONU'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.Sedol <> '' and substring(maintab.mainTFB1,1,4) = 'ISIN' THEN '/GB/' + maintab.Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,     
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN maintab.PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN maintab.PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(maintab.cfi)=6 THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,     
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,     
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN maintab.RatioNew<>'' AND maintab.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(maintab.RatioNew as char (15)))+
                    '/'+ltrim(cast(maintab.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,     
':16S:SECMOVE',
case when bon2.bonid is not null then ':16R:SECMOVE' ELSE '' end,
case when bon2.bonid is not null then ':22H::CRDB//CRED' ELSE '' end,
case WHEN bon2.bonid is not null and bon2scmst.isin is not null and bon2scmst.isin<>''
     THEN ':35B:ISIN ' + bon2scmst.isin
     WHEN bon2.bonid is not null
     THEN ':35B:UKWN'
     ELSE ''
     END,     
case when bon2.bonid is not null then ':16R:FIA' ELSE '' end,
CASE 
     WHEN bon2.bonid is not null
     THEN ':94B::PLIS//SECM'
     ELSE ''
     END,     
case when bon2.bonid is not null then ':16S:FIA' ELSE '' end,
CASE WHEN bon2.bonid is not null and bon2.RatioNew<>'' AND bon2.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(bon2.RatioNew as char (15)))+
                    '/'+ltrim(cast(bon2.RatioOld as char (15))) 
     WHEN bon2.bonid is not null
     THEN ':92K::ADEX//UKWN' 
     ELSE ''
     END as COMMASUB,
CASE WHEN bon2.bonid is not null and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     WHEN bon2.bonid is not null
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,     
case when bon2.bonid is not null then ':16S:SECMOVE' ELSE '' end,
':16S:CAOPTN',
'' as MT564Notes, 
'-}$'
From wca2.dbo.t_EQ_BONU as maintab
left outer join serialseq on maintab.eventid = serialseq.rdid and 2=serialseq.seqnum and 'BON'=serialseq.eventcd
left outer join bon as bon2 on serialseq.uniqueid = bon2.bonid
left outer join scmst as bon2scmst on bon2.ressecid = bon2scmst.secid
WHERE
mainTFB1<>''
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and exchgid<>'' and exchgid is not null
and not (maintab.sectycd='DR' and maintab.lapsedpremium<>'')
order by caref2 desc, caref1, caref3
