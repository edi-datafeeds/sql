--SEMETABLE=client.dbo.seme_mwace
--FileName=edi_YYYYMMDD
--TEND_TKOVR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=TKOVR
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=0
--TEXTCLEAN=2

--
--# 1
use wca
select distinct
'' as bbgcntry,
'' as BBGCUREN,
maintab.Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
null as PAYLINK,
Opendate as OPENLINK,
maintab.Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01EDIXXXXXXISO0300000054}{2:O5641825151030EDIXXXXXXISO03000000541510301825N}{4:' as ISOHDR,
':16R:GENL',
'130'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MRGR' as CAEV,
case when (select count(distinct subpay.optionid) from wca.dbo.mpay as subpay
           where subpay.eventid=maintab.eventid and sevent='TKOVR' 
           and subpay.actflag<>'C' and subpay.actflag<>'D' and subpay.paytype<>'D')>1 then ':22F::CAMV//CHOS' else ':22F::CAMV//MAND' end as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN' THEN '/GB/' + Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,     
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , CmAcqdate,112)
     WHEN Closedate IS NOT NULL and TkovrStatus='C'
     THEN ':98A::EFFD//'+CONVERT ( varchar , Closedate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,     
case when wca.dbo.mpay.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     END,     
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+replace(substring(maintab.Offerorname,1,23),'�','e')
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as MT564Notes, 
'-}$'
From wca2.dbo.t_EQ_TEND_TKOVR as maintab
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'TKOVR' = wca.dbo.mpay.sevent
                                             and 'D'=wca.dbo.mpay.paytype and 'D'<>wca.dbo.mpay.actflag
left outer join client.dbo.seme_mwace as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'CANC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'MRGR'=SR.CAEVMAIN
WHERE 
mainTFB1<>''
and ((maintab.opendate is null or maintab.cmAcqdate is not null)
  and (maintab.closedate is not null or maintab.cmAcqdate is not null)
  and maintab.minitkovr<>'T'
  or (SR.corp is not null and maintab.Actflag='D'))
and opol<>'SHSC' and opol<>'XSSC'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3
