--SEMETABLE=client.dbo.seme_solactive
--FileName=edi_YYYYMMDD
--CLSA_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\Upload\acc\246\feed\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select
Changed,
'select case when len(cast(LawstNotes as varchar(255)))>40 then LawstNotes else rtrim(char(32)) end As Notes FROM LAWST WHERE LAWSTID = '+ cast(EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'149'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CLSA',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN EffectiveDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From wca2.dbo.i_EQ_CLSA
WHERE 
mcd<>'' and mcd is not null
and sedol in (select code from client.dbo.pfsedol where accid = 246 and actflag<>'D')
and LAtype='CLSACT'
and LAtype is not null
order by caref2 desc, caref1, caref3
