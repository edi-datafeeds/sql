--SEMETABLE=client.dbo.seme_opensense_393
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_CLSA
--FileNameNotes=15022_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(LawstNotes as varchar(24)))=22 then rtrim(char(32)) else LawstNotes end As Notes FROM LAWST WHERE LAWSTID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01BICSAM22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '349'+EXCHGID+cast(Seqnum as char(1))
     ELSE '349'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CLSA',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else '' 
     end,
case when len(Issuername)<36
     then Issuername
     else substring(Issuername,36,35)
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end,
substring(bonddesc3,1,35),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Effectivedate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes, 
'-}$'
From v_FI_CLSA as maintab 
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=384 and actflag='I'))
      and (effectivedate > getdate() or changed > getdate()-92))
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=384 and actflag='U'))
and changed>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
order by caref2 desc, caref1, caref3
