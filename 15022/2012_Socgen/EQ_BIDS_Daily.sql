--SEMETABLE=client.dbo.seme_socgen
--FileName=edi_YYYYMMDD_MT564
--BIDS_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_Socgen\
--FileTidy=N
--sEvent=BB
--TFBNUMBER=2
--INCREMENTAL=OFF

--# 1
use wca
select
'select distinct V10s_MPAY.* from V10s_MPAY'
+ ' where (v10s_MPAY.actflag='+char(39)+'I'+char(39)+ ' or v10S_MPAY.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'BB'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
Changed,
/*'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(EventID as char(16)) as ChkNotes, */
null as PAYLINK,
Startdate as OPENLINK,
Enddate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
'115'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN StartDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN MaxQlyQty<>''
     THEN ':36B::QTSO//UNIT/'+substring(MaxQlyQty,1,15) + ','
     ELSE '' END,
':16S:CADETL',
':16R:CAOPTN' as StartSeqE,
'-}$'
From v_EQ_BIDS
WHERE 
mainTFB2<>''
and (mcd<>'' and mcd is not null)
and Enddate is not null
and CalcListdate<=Enddate
and CalcDelistdate>=Enddate
AND (((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
      or (select max(acttime) from mpay where mpay.sevent='BB' and mpay.eventid=v_EQ_BIDS.eventid)>=
      (select max(feeddate) from wca.dbo.tbl_opslog where seq=3))
     and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='U')
                    ))
     OR 
    (Enddate>getdate()
     and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='I')
          )
    ))
order by caref2 desc, caref1, caref3
