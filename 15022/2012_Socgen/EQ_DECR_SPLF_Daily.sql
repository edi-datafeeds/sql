--SEMETABLE=client.dbo.seme_socgen
--FileName=edi_YYYYMMDD_MT564
--DECR_SPLF_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_Socgen\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF

--# 1
use wca
select
Changed,
/*'select  SDNotes As Notes FROM SD WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes, */
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
'148'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::EFFD//UKWN'
     END as DateEx,
CASE WHEN Recdate <>'' AND Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':36B::NEWD//UNIT/'+NewParValue as commasub,
':16S:CADETL',

'-}$'
From v_EQ_SPLF
WHERE 
mainTFB2<>''
and (mcd<>'' and mcd is not null)
and exdate is not null
and CalcListdate<=exdate
and CalcDelistdate>=exdate
and rtrim(OldParValue)<>'' and rtrim(NewParValue)<>''
and rtrim(OldParValue)<>'0' and rtrim(NewParValue)<>'0'
and cast(rtrim(OldParValue) as float(16,7))>cast(rtrim(NewParValue) as float(16,7))
AND ((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
     and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='U')
                    ))
     OR 
    (Exdate>getdate()
     and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='I')
          )
    ))
order by caref2 desc, caref1, caref3
