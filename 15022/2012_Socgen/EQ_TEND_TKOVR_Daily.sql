--SEMETABLE=client.dbo.seme_socgen
--FileName=edi_YYYYMMDD_MT564
--TEND_TKOVR_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_Socgen\
--FileTidy=N
--sEvent=TKOVR
--TFBNUMBER=2
--INCREMENTAL=OFF

--# 1
use wca
select
'select distinct V10s_MPAY.* from V10s_MPAY'
+ ' where (v10s_MPAY.actflag='+char(39)+'I'+char(39) +' or v10S_MPAY.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'TKOVR'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
Changed,
/*'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(EventID as char(16)) as ChkNotes, */ 
null as PAYLINK,
Opendate as OPENLINK,
Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
'131'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
CASE WHEN CmAcqdate <> ''
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Opendate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN CmAcqdate <> ''
     THEN ':98A::WUCO//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN UnconditionalDate <>'' and UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'-}$'
From v_EQ_TEND_TKOVR
WHERE 
mainTFB2<>''
and closedate is not null
and CalcListdate<=closedate
and CalcDelistdate+60>=closedate
and (mcd<>'' and mcd is not null)
AND (((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
      or (select max(acttime) from mpay where mpay.sevent='TKOVR' and mpay.eventid=v_EQ_TEND_TKOVR.eventid)>=
      (select max(feeddate) from wca.dbo.tbl_opslog where seq=3))
     and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='U')
                    ))
     OR 
    (closedate>getdate()
     and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='I')
          )
    ))     
order by caref2 desc, caref1, caref3