--SEMETABLE=client.dbo.seme_socgen
--FileName=edi_YYYYMMDD_MT564
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_Socgen\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF


--# 1
use wca
select
Changed,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
cntrycd as MPAYLINK3,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN  maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN  maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEP//DISN',
':22F::CAEV//DRCA',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CG'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='INS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='MEM'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='SUP'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='TEI'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='ARR'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ONE'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='UN'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='VAR'
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN divopt1.CurenCD <> '' AND divopt1.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + divopt1.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  Paydate <>'' AND Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN  divopt1.depfees <>'' and divopt1.depfees <>'0'
     THEN ':92F::CHAR//USD'+ltrim(cast(divopt1.depfees as char(15)))
     ELSE ':92K::CHAR//UKWN'
     END as COMMASUB,
CASE WHEN divopt1.Grossdividend <> '' AND divopt1.Grossdividend IS NOT NULL
     THEN ':90B::OFFR//ACTU/'+ divopt1.CurenCD+ltrim(cast(divopt1.Grossdividend as char(15)))
     ELSE ''
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
from v_EQ_DV_CA_SE_OP as maintab
left outer join divpy as divopt1 on maintab.eventid = divopt1.divid and 1=divopt1.optionid and 'D'<>divopt1.actflag
left outer join divpy as divopt2 on maintab.eventid = divopt2.divid and 2=divopt2.optionid and 'D'<>divopt2.actflag
left outer join divpy as divopt3 on maintab.eventid = divopt3.divid and 3=divopt3.optionid and 'D'<>divopt3.actflag
WHERE 
mainTFB2<>''
and (mcd<>'' and mcd is not null)
and divopt2.divid is null
and divopt3.divid is null
and divopt1.divid is not null
and divopt1.divtype='C'
and divopt1.grossdividend<>''
and maintab.divperiodcd<>'ISC'
and maintab.sectycd='DR'
and (paydate>getdate()-183 or paydate is null)
and Exdate is not null
and CalcListdate<=Exdate
and CalcDelistdate>=Exdate
AND (((changed>=(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
     or Dripchanged>=(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
     or Divpychanged>=(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3))
    and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='U')
          ))
     OR 
    (exdate>getdate()
     and (sedol in (select code from client.dbo.pfsedol where accid = 135 and actflag='I')
          )))
order by caref2 desc, caref1, caref3
