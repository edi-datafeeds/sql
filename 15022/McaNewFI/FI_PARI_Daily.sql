--SEMETABLE=client.dbo.seme_sample
--FileName=edi_2018_FI_YYYYMMDD
--PARI_FI564_YYYYMMDD
--FileNameNotes=2018_FI_MT568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=1000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(Notes as varchar(24)))=22 then rtrim(char(32)) else Notes end As Notes FROM TRNCH WHERE TRNCHID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01MMCAUS22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'124' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PARI' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExpiryDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , ExpiryDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN len(resTFB1)>10
     THEN ':35B:' + resTFB1
     ELSE ':35B:/US/999999999'
     END,
CASE WHEN len(resTFB1)<10
     THEN 'NOT AVAILABLE AT PRESENT'
     ELSE ''
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN OldOutValue <>'' AND NewOutValue <>''
     THEN ':92D::NEWO//'+rtrim(cast(NewOutValue as char (15)))+
                    '/'+rtrim(cast(OldOutValue as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
CASE WHEN ExpiryDate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , ExpiryDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_FI_PARI_2 as maintab
WHERE
mainTFB1<>''
and changed>'2018/09/01'
order by caref2 desc, caref1, caref3
