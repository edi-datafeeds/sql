--SEMETABLE=client.dbo.seme_smartstream
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_REDO
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
wca2.dbo.i_FI_REDO.Changed,
'select case when len(cast(CurrdNotes as varchar(24)))=22 then rtrim(char(32)) else CurrdNotes end As Notes FROM CURRD WHERE CurrdID = '+ cast(wca2.dbo.i_FI_REDO.EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '325'+EXCHGID+cast(Seqnum as char(1))
     ELSE '325'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN wca2.dbo.i_FI_REDO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN wca2.dbo.i_FI_REDO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN wca2.dbo.i_FI_REDO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN wca2.dbo.i_FI_REDO.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN wca2.dbo.i_FI_REDO.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , wca2.dbo.i_FI_REDO.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN wca2.dbo.i_FI_REDO.mainTFB2 <>''
     THEN ':35B:' + mainTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
CASE WHEN NewCurenCD <> ''
     THEN ':11A::DENO//'+NewCurenCD
     ELSE ''
     END,
':16S:FIA',
CASE WHEN wca2.dbo.i_FI_REDO.NewCurenCD=wca2.dbo.i_FI_REDO.Current_Currency and wca2.dbo.i_FI_REDO.MinimumDenomination<>''
     THEN ':36B::MINO'+wca2.dbo.i_FI_REDO.MinimumDenomination
     ELSE ''
     END,
CASE WHEN wca2.dbo.i_FI_REDO.newparvalue <>''
            AND wca2.dbo.i_FI_REDO.oldparvalue <>''
     THEN ':92D::NEWO//'+rtrim(cast(wca2.dbo.i_FI_REDO.newparvalue as char (15)))+
                    '/'+rtrim(cast(wca2.dbo.i_FI_REDO.oldparvalue as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From wca2.dbo.i_FI_REDO
WHERE
mainTFB1<>''
and NewCurenCD <>''
and Actflag  <>''
order by caref2 desc, caref1, caref3
