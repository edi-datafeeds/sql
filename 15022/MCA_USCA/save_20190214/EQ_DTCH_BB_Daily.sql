--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_USCA_YYYYMMDD
--DTCH_BB_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\smartstream\
--FileTidy=N
--sEvent=BB
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
'select distinct V10s_MPAY.* from V10s_MPAY'
+ ' where (v10s_MPAY.actflag='+char(39)+'I'+char(39)+ ' or v10S_MPAY.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'BB'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
maintab.Changed,
'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
null as PAYLINK,
Startdate as OPENLINK,
Enddate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Enddate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Enddate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '133' END as CAref1,
maintab.EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DTCH'as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN MaxPrice <> '' 
     THEN ':90B::MAXP//ACTU/'+CurenCD+
          +substring(MaxPrice,1,15)
     ELSE ':90E::MAXP//UKWN'
     END as COMMASUB,
CASE WHEN MinPrice <> '' 
     THEN ':90B::MINP//ACTU/'+CurenCD+
          +substring(MinPrice,1,15)
     ELSE ':90E::MINP//UKWN'
     END as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//NOAC',
CASE WHEN CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN EndDate <>''
     THEN ':98A::EXPI//' + CONVERT(varchar , EndDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN StartDate<>'' AND EndDate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/'
          +CONVERT ( varchar , EndDate,112)
     WHEN StartDate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/UKWN'
     WHEN EndDate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//CASH',
CASE WHEN CurenCD<>'' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN EndDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN WithdrawalRights='T' 
            and WithdrawalFromdate <> '' AND WithdrawalTodate <> '' 
     THEN ':69A::REVO//'
          +CONVERT ( varchar , WithdrawalFromdate,112) + '/'
          +CONVERT ( varchar , WithdrawalTodate,112)
     WHEN WithdrawalFromdate<>''
     THEN ':69C::REVO//'
          +CONVERT ( varchar , WithdrawalFromdate,112)+'/UKWN'
     WHEN WithdrawalTodate <> '' 
     THEN ':69E::REVO//UKWN/'
          +CONVERT ( varchar , WithdrawalTodate,112)
     ELSE ':69J::REVO//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN PayDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes, 
'-}$'
From wca2.dbo.i_EQ_BIDS as maintab
left outer join client.dbo.seme_markitmca as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'DTCH'=SR.CAEVMAIN
WHERE 
mainTFB1<>''
and maintab.DutchAuction='T'
and maintab.primaryexchgcd=maintab.exchgcd
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
order by caref2 desc, caref1, caref3
