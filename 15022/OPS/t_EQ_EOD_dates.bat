-- print " Generating t_EQ_CHAN_NAME, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_CHAN_NAME')
 drop table t_EQ_CHAN_NAME
use wca
select * 
into wca2.dbo.t_EQ_CHAN_NAME
from v_EQ_CHAN_NAME
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_CAPG_RCAP, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_CAPG_RCAP')
 drop table t_EQ_CAPG_RCAP
use wca
select * 
into wca2.dbo.t_EQ_CAPG_RCAP
from v_EQ_CAPG_RCAP
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go

-- print " Generating t_EQ_DRIP_2011, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_DRIP_2011')
 drop table t_EQ_DRIP_2011
use wca
select * 
into wca2.dbo.t_EQ_DRIP_2011
from v_EQ_DRIP_2011
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go

-- print " Generating t_EQ_DV_CA_SE_OP, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_DV_CA_SE_OP')
 drop table t_EQ_DV_CA_SE_OP
use wca
select * 
into wca2.dbo.t_EQ_DV_CA_SE_OP
from v_EQ_DV_CA_SE_OP
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go

-- print " Generating t_EQ_PRIO_DVST, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_PRIO_DVST')
 drop table t_EQ_PRIO_DVST
use wca
select * 
into wca2.dbo.t_EQ_PRIO_DVST
from v_EQ_PRIO_DVST
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go


-- print " Generating t_EQ_PRIO_ENT, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_PRIO_ENT')
 drop table t_EQ_PRIO_ENT
use wca
select * 
into wca2.dbo.t_EQ_PRIO_ENT
from v_EQ_PRIO_ENT
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go


-- print " Generating t_EQ_PRIO_PRF, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_PRIO_PRF')
 drop table t_EQ_PRIO_PRF
use wca
select * 
into wca2.dbo.t_EQ_PRIO_PRF
from v_EQ_PRIO_PRF
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go


-- print " Generating t_EQ_RHDI_RTS, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_RHDI_RTS')
 drop table t_EQ_RHDI_RTS
use wca
select * 
into wca2.dbo.t_EQ_RHDI_RTS
from v_EQ_RHDI_RTS
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go


-- print " Generating t_EQ_ACTV, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_ACTV')
 drop table t_EQ_ACTV
use wca
select * 
into wca2.dbo.t_EQ_ACTV
from v_EQ_ACTV
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go

-- print " Generating t_EQ_ACTV_NLIST, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_ACTV_NLIST')
 drop table t_EQ_ACTV_NLIST
use wca
select * 
into wca2.dbo.t_EQ_ACTV_NLIST
from v_EQ_ACTV
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go

-- print " Generating t_EQ_BIDS, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_BIDS')
 drop table t_EQ_BIDS
use wca
select * 
into wca2.dbo.t_EQ_BIDS
from v_EQ_BIDS
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_BONU, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_BONU')
 drop table t_EQ_BONU
use wca
select * 
into wca2.dbo.t_EQ_BONU
from v_EQ_BONU
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_BRUP, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_BRUP')
 drop table t_EQ_BRUP
use wca
select * 
into wca2.dbo.t_EQ_BRUP
from v_EQ_BRUP
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_DECR_CAPRD, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_DECR_CAPRD')
 drop table t_EQ_DECR_CAPRD
use wca
select * 
into wca2.dbo.t_EQ_DECR_CAPRD
from v_EQ_DECR_CAPRD
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_CLSA, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_CLSA')
 drop table t_EQ_CLSA
use wca
select * 
into wca2.dbo.t_EQ_CLSA
from v_EQ_CLSA
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_CONV, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_CONV')
 drop table t_EQ_CONV
use wca
select * 
into wca2.dbo.t_EQ_CONV
from v_EQ_CONV
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_DECR_RCAP, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_DECR_RCAP')
 drop table t_EQ_DECR_RCAP
use wca
select * 
into wca2.dbo.t_EQ_DECR_RCAP
from v_EQ_DECR_RCAP
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_DVCA_DVSE, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_DVCA_DVSE')
 drop table t_EQ_DVCA_DVSE
use wca
select * 
into wca2.dbo.t_EQ_DVCA_DVSE
from v_EQ_DVCA_DVSE
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_DVOP, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_DVOP')
 drop table t_EQ_DVOP
use wca
select * 
into wca2.dbo.t_EQ_DVOP
from v_EQ_DVOP
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_EXOF_CTX, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_EXOF_CTX')
 drop table t_EQ_EXOF_CTX
use wca
select * 
into wca2.dbo.t_EQ_EXOF_CTX
from v_EQ_EXOF_CTX
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_EXOF_DVST, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_EXOF_DVST')
 drop table t_EQ_EXOF_DVST
use wca
select * 
into wca2.dbo.t_EQ_EXOF_DVST
from v_EQ_EXOF_DVST
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_EXOF_SCSWP, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_EXOF_SCSWP')
 drop table t_EQ_EXOF_SCSWP
use wca
select * 
into wca2.dbo.t_EQ_EXOF_SCSWP
from v_EQ_EXOF_SCSWP
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_EXRI_RTS, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_EXRI_RTS')
 drop table t_EQ_EXRI_RTS
use wca
select * 
into wca2.dbo.t_EQ_EXRI_RTS
from v_EQ_EXRI_RTS
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_EXWA, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_EXWA')
 drop table t_EQ_EXWA
use wca
select * 
into wca2.dbo.t_EQ_EXWA
from v_EQ_EXWA
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_INCR_DECR_PVRD, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_INCR_DECR_PVRD')
 drop table t_EQ_INCR_DECR_PVRD
use wca
select * 
into wca2.dbo.t_EQ_INCR_DECR_PVRD
from v_EQ_INCR_DECR_PVRD
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_LIQU, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_LIQU')
 drop table t_EQ_LIQU
use wca
select * 
into wca2.dbo.t_EQ_LIQU
from v_EQ_LIQU
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_MEET, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_MEET')
 drop table t_EQ_MEET
use wca
select * 
into wca2.dbo.t_EQ_MEET
from v_EQ_MEET
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_MRGR, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_MRGR')
 drop table t_EQ_MRGR
use wca
select * 
into wca2.dbo.t_EQ_MRGR
from v_EQ_MRGR
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_ODLT, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_ODLT')
 drop table t_EQ_ODLT
use wca
select * 
into wca2.dbo.t_EQ_ODLT
from v_EQ_ODLT
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_OTHR_ARR, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_OTHR_ARR')
 drop table t_EQ_OTHR_ARR
use wca
select * 
into wca2.dbo.t_EQ_OTHR_ARR
from v_EQ_OTHR_ARR
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_PARI, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_PARI')
 drop table t_EQ_PARI
use wca
select * 
into wca2.dbo.t_EQ_PARI
from v_EQ_PARI
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_PPMT, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_PPMT')
 drop table t_EQ_PPMT
use wca
select * 
into wca2.dbo.t_EQ_PPMT
from v_EQ_PPMT
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_PRIO, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_PRIO')
 drop table t_EQ_PRIO
use wca
select * 
into wca2.dbo.t_EQ_PRIO
from v_EQ_PRIO
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_REDM, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_REDM')
 drop table t_EQ_REDM
use wca
select * 
into wca2.dbo.t_EQ_REDM
from v_EQ_REDM
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_REDO, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_REDO')
 drop table t_EQ_REDO
use wca
select * 
into wca2.dbo.t_EQ_REDO
from v_EQ_REDO
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_RHTS_ENT, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_RHTS_ENT')
 drop table t_EQ_RHTS_ENT
use wca
select * 
into wca2.dbo.t_EQ_RHTS_ENT
from v_EQ_RHTS_ENT
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_RHTS_RTS, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_RHTS_RTS')
 drop table t_EQ_RHTS_RTS
use wca
select * 
into wca2.dbo.t_EQ_RHTS_RTS
from v_EQ_RHTS_RTS
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_SOFF_DIST, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_SOFF_DIST')
 drop table t_EQ_SOFF_DIST
use wca
select * 
into wca2.dbo.t_EQ_SOFF_DIST
from v_EQ_SOFF_DIST
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_SOFF_DMRGR, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_SOFF_DMRGR')
 drop table t_EQ_SOFF_DMRGR
use wca
select * 
into wca2.dbo.t_EQ_SOFF_DMRGR
from v_EQ_SOFF_DMRGR
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_SPLF, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_SPLF')
 drop table t_EQ_SPLF
use wca
select * 
into wca2.dbo.t_EQ_SPLF
from v_EQ_SPLF
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_SPLR, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_SPLR')
 drop table t_EQ_SPLR
use wca
select * 
into wca2.dbo.t_EQ_SPLR
from v_EQ_SPLR
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_TEND_PO, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_TEND_PO')
 drop table t_EQ_TEND_PO
use wca
select * 
into wca2.dbo.t_EQ_TEND_PO
from v_EQ_TEND_PO
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go



-- print " Generating t_EQ_TEND_TKOVR, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_TEND_TKOVR')
 drop table t_EQ_TEND_TKOVR
use wca
select * 
into wca2.dbo.t_EQ_TEND_TKOVR
from v_EQ_TEND_TKOVR
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go


-- print " Generating t_EQ_EXRI_RENO_RTS, please wait..."
go
use wca
Declare @StartDate datetime
Declare @EndDate datetime
  	-- set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
set @StartDate = '2016/12/14'
set @EndDate = '2016/12/15'

use wca2
if exists (select * from sysobjects where name = 't_EQ_EXRI_RENO_RTS')
 drop table t_EQ_EXRI_RENO_RTS
use wca
select * 
into wca2.dbo.t_EQ_EXRI_RENO_RTS
from v_EQ_EXRI_RENO_RTS
where 
  	-- changed >= @Startdate
changed between @Startdate and @Enddate
go

