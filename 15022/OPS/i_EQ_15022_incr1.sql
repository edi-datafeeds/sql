-- print " Generating i_EQ_CHAN_NAME, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_CHAN_NAME')
 drop table i_EQ_CHAN_NAME
use wca
select * 
into wca2.dbo.i_EQ_CHAN_NAME
from v_EQ_CHAN_NAME
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)


go
-- print " Generating i_EQ_CAPG_RCAP, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_CAPG_RCAP')
 drop table i_EQ_CAPG_RCAP
use wca
select * 
into wca2.dbo.i_EQ_CAPG_RCAP
from v_EQ_CAPG_RCAP
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go

-- print " Generating i_EQ_DRIP_2011, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_DRIP_2011')
 drop table i_EQ_DRIP_2011
use wca
select * 
into wca2.dbo.i_EQ_DRIP_2011
from v_EQ_DRIP_2011
where 
(changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)
    or Dripchanged>=(select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
    or Divpychanged>=(select max(acttime)-0.05 from wca.dbo.tbl_Opslog))

go

-- print " Generating i_EQ_DV_CA_SE_OP, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_DV_CA_SE_OP')
 drop table i_EQ_DV_CA_SE_OP
use wca
select *
into wca2.dbo.i_EQ_DV_CA_SE_OP
from v_EQ_DV_CA_SE_OP
where 
(changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)
      or (select max(acttime) from divpy where divid=eventid)>=
      (select max(acttime)-0.05 from wca.dbo.tbl_opslog))
go


-- print " Generating i_EQ_PRIO_DVST, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_PRIO_DVST')
 drop table i_EQ_PRIO_DVST
use wca
select * 
into wca2.dbo.i_EQ_PRIO_DVST
from v_EQ_PRIO_DVST
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go


-- print " Generating i_EQ_PRIO_ENT, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_PRIO_ENT')
 drop table i_EQ_PRIO_ENT
use wca
select * 
into wca2.dbo.i_EQ_PRIO_ENT
from v_EQ_PRIO_ENT
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go


-- print " Generating i_EQ_PRIO_PRF, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_PRIO_PRF')
 drop table i_EQ_PRIO_PRF
use wca
select * 
into wca2.dbo.i_EQ_PRIO_PRF
from v_EQ_PRIO_PRF
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go


-- print " Generating i_EQ_RHDI_RTS, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_RHDI_RTS')
 drop table i_EQ_RHDI_RTS
use wca
select * 
into wca2.dbo.i_EQ_RHDI_RTS
from v_EQ_RHDI_RTS
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go


-- print " Generating i_EQ_ACTV, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_ACTV')
 drop table i_EQ_ACTV
use wca
select * 
into wca2.dbo.i_EQ_ACTV
from v_EQ_ACTV
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go

-- print " Generating i_EQ_ACTV_NLIST, please wait..."
--go
--use wca2
--if exists (select * from sysobjects where name = 'i_EQ_ACTV_NLIST')
-- drop table i_EQ_ACTV_NLIST
--use wca
--select * 
--into wca2.dbo.i_EQ_ACTV_NLIST
--from v_EQ_ACTV
--where 
--changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

--go

-- print " Generating i_EQ_BIDS, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_BIDS')
 drop table i_EQ_BIDS
use wca
select * 
into wca2.dbo.i_EQ_BIDS
from v_EQ_BIDS
where 
((changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)
      or (select max(acttime) from wca.dbo.mpay where mpay.sevent='BB' and wca.dbo.mpay.eventid=wca.dbo.v_EQ_BIDS.eventid)>=
      (select max(acttime)-0.05 from wca.dbo.tbl_Opslog))
and Actflag <> '')
-- or changed > '2012/11/12 16:00:00' and changed<'2012/11/13 00:00:00'
go


-- print " Generating i_EQ_BONU, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_BONU')
 drop table i_EQ_BONU
use wca
select * 
into wca2.dbo.i_EQ_BONU
from v_EQ_BONU
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_BRUP, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_BRUP')
 drop table i_EQ_BRUP
use wca
select * 
into wca2.dbo.i_EQ_BRUP
from v_EQ_BRUP
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_DECR_CAPRD, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_DECR_CAPRD')
 drop table i_EQ_DECR_CAPRD
use wca
select * 
into wca2.dbo.i_EQ_DECR_CAPRD
from v_EQ_DECR_CAPRD
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_CLSA, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_CLSA')
 drop table i_EQ_CLSA
use wca
select * 
into wca2.dbo.i_EQ_CLSA
from v_EQ_CLSA
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_CONV, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_CONV')
 drop table i_EQ_CONV
use wca
select * 
into wca2.dbo.i_EQ_CONV
from v_EQ_CONV
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_EXOF_CTX, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_EXOF_CTX')
 drop table i_EQ_EXOF_CTX
use wca
select * 
into wca2.dbo.i_EQ_EXOF_CTX
from v_EQ_EXOF_CTX
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_EXOF_DVST, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_EXOF_DVST')
 drop table i_EQ_EXOF_DVST
use wca
select * 
into wca2.dbo.i_EQ_EXOF_DVST
from v_EQ_EXOF_DVST
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_EXRI_RTS, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_EXRI_RTS')
 drop table i_EQ_EXRI_RTS
use wca
select * 
into wca2.dbo.i_EQ_EXRI_RTS
from v_EQ_EXRI_RTS
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_EXWA, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_EXWA')
 drop table i_EQ_EXWA
use wca
select * 
into wca2.dbo.i_EQ_EXWA
from v_EQ_EXWA
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_INCR_DECR_PVRD, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_INCR_DECR_PVRD')
 drop table i_EQ_INCR_DECR_PVRD
use wca
select * 
into wca2.dbo.i_EQ_INCR_DECR_PVRD
from v_EQ_INCR_DECR_PVRD
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_LIQU, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_LIQU')
 drop table i_EQ_LIQU
use wca
select * 
into wca2.dbo.i_EQ_LIQU
from v_EQ_LIQU
where
(changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)
   or (select max(acttime) from wca.dbo.mpay where wca.dbo.mpay.sevent='LIQ' and mpay.eventid=wca.dbo.v_EQ_LIQU.eventid)>=
     (select max(acttime)-0.05 from wca.dbo.tbl_Opslog))

go



-- print " Generating i_EQ_MEET, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_MEET')
 drop table i_EQ_MEET
use wca
select * 
into wca2.dbo.i_EQ_MEET
from v_EQ_MEET
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_MRGR, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_MRGR')
 drop table i_EQ_MRGR
use wca
select * 
into wca2.dbo.i_EQ_MRGR
from v_EQ_MRGR
where 
(changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)
   or (select max(acttime) from wca.dbo.mpay where wca.dbo.mpay.eventid=wca.dbo.v_EQ_MRGR.eventid)>=
     (select max(acttime)-0.05 from wca.dbo.tbl_Opslog))

go



-- print " Generating i_EQ_ODLT, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_ODLT')
 drop table i_EQ_ODLT
use wca
select * 
into wca2.dbo.i_EQ_ODLT
from v_EQ_ODLT
where 
(changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)
      or (select max(acttime) from wca.dbo.mpay where wca.dbo.mpay.sevent='ODDLT' and mpay.eventid=wca.dbo.v_EQ_ODLT.eventid)>=
      (select max(acttime)-0.05 from wca.dbo.tbl_Opslog))
go



-- print " Generating i_EQ_PARI, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_PARI')
 drop table i_EQ_PARI
use wca
select * 
into wca2.dbo.i_EQ_PARI
from v_EQ_PARI
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_PPMT, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_PPMT')
 drop table i_EQ_PPMT
use wca
select * 
into wca2.dbo.i_EQ_PPMT
from v_EQ_PPMT
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_REDM, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_REDM')
 drop table i_EQ_REDM
use wca
select * 
into wca2.dbo.i_EQ_REDM
from v_EQ_REDM
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_REDO, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_REDO')
 drop table i_EQ_REDO
use wca
select * 
into wca2.dbo.i_EQ_REDO
from v_EQ_REDO
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_SOFF_DIST, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_SOFF_DIST')
 drop table i_EQ_SOFF_DIST
use wca
select * 
into wca2.dbo.i_EQ_SOFF_DIST
from v_EQ_SOFF_DIST
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2
   or (select max(acttime) from wca.dbo.mpay where wca.dbo.mpay.sevent='DIST' and mpay.eventid=wca.dbo.v_EQ_SOFF_DIST.eventid)>=
      (select max(acttime)-0.05 from wca.dbo.tbl_Opslog))

go



-- print " Generating i_EQ_SOFF_DMRGR, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_SOFF_DMRGR')
 drop table i_EQ_SOFF_DMRGR
use wca
select * 
into wca2.dbo.i_EQ_SOFF_DMRGR
from v_EQ_SOFF_DMRGR
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2
    or (select max(acttime) from wca.dbo.mpay where wca.dbo.mpay.sevent='DMRGR' and mpay.eventid=wca.dbo.v_EQ_SOFF_DMRGR.eventid)>=
      (select max(acttime)-0.05 from wca.dbo.tbl_Opslog))

go



-- print " Generating i_EQ_SPLF, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_SPLF')
 drop table i_EQ_SPLF
use wca
select * 
into wca2.dbo.i_EQ_SPLF
from v_EQ_SPLF
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_SPLR, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_SPLR')
 drop table i_EQ_SPLR
use wca
select * 
into wca2.dbo.i_EQ_SPLR
from v_EQ_SPLR
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_TEND_PO, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_TEND_PO')
 drop table i_EQ_TEND_PO
use wca
select * 
into wca2.dbo.i_EQ_TEND_PO
from v_EQ_TEND_PO
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)

go



-- print " Generating i_EQ_TEND_TKOVR, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_EQ_TEND_TKOVR')
 drop table i_EQ_TEND_TKOVR
use wca
select * 
into wca2.dbo.i_EQ_TEND_TKOVR
from v_EQ_TEND_TKOVR
where 
(changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=1) and changed < (select max(acttime)-0.05 from wca.dbo.tbl_Opslog where seq=2)
   or (select max(acttime) from wca.dbo.mpay where wca.dbo.mpay.sevent='TKOVR' and mpay.eventid=wca.dbo.v_EQ_TEND_TKOVR.eventid)>=
    (select max(acttime)-0.05 from wca.dbo.tbl_Opslog))

go
