use wca2
if exists (select * from sysobjects where name = 'i_FI_ACTV')
 drop table i_FI_ACTV
use wca
select * 
into wca2.dbo.i_FI_ACTV
from v_FI_ACTV
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go

use wca2
if exists (select * from sysobjects where name = 'i_FI_ACTV_NLIST')
 drop table i_FI_ACTV_NLIST
use wca
select * 
into wca2.dbo.i_FI_ACTV_NLIST
from v_FI_ACTV_NLIST
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go

-- print " Generating i_FI_BRUP, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_BRUP')
 drop table i_FI_BRUP
use wca
select * 
into wca2.dbo.i_FI_BRUP
from v_FI_BRUP
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go


-- print " Generating i_FI_CHAN_NAME, please wait..."
use wca2
if exists (select * from sysobjects where name = 'i_FI_CHAN_NAME')
 drop table i_FI_CHAN_NAME
use wca
select * 
into wca2.dbo.i_FI_CHAN_NAME
from v_FI_CHAN_NAME
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go


-- print " Generating i_FI_CLSA, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_CLSA')
 drop table i_FI_CLSA
use wca
select * 
into wca2.dbo.i_FI_CLSA
from v_FI_CLSA
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go


-- print " Generating i_FI_CONS, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_CONS')
 drop table i_FI_CONS
use wca
select * 
into wca2.dbo.i_FI_CONS
from v_FI_CONS
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go


-- print " Generating i_FI_CONV, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_CONV')
 drop table i_FI_CONV
use wca
select * 
into wca2.dbo.i_FI_CONV
from v_FI_CONV
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go




-- print " Generating i_FI_EXTM, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_EXTM')
 drop table i_FI_EXTM
use wca
select * 
into wca2.dbo.i_FI_EXTM
from v_FI_EXTM
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go



-- print " Generating i_FI_LIQU, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_LIQU')
 drop table i_FI_LIQU
use wca
select * 
into wca2.dbo.i_FI_LIQU
from v_FI_LIQU
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go



-- print " Generating i_FI_XMET, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_XMET')
 drop table i_FI_XMET
use wca
select * 
into wca2.dbo.i_FI_XMET
from v_FI_XMET
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go



-- print " Generating i_FI_REDM, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_REDM')
 drop table i_FI_REDM
use wca
select * 
into wca2.dbo.i_FI_REDM
from v_FI_REDM
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go



-- print " Generating i_FI_REDO, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_REDO')
 drop table i_FI_REDO
use wca
select * 
into wca2.dbo.i_FI_REDO
from v_FI_REDO
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go



-- print " Generating i_FI_XMET, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_XMET')
 drop table i_FI_XMET
use wca
select * 
into wca2.dbo.i_FI_XMET
from v_FI_XMET
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go



-- print " Generating i_FI_INTR, please wait..."
go
use wca2
if exists (select * from sysobjects where name = 'i_FI_INTR')
 drop table i_FI_INTR
use wca
select * 
into wca2.dbo.i_FI_INTR
from v_FI_INTR
where 
changed >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)

go

