--SEMETABLE=client.dbo.seme_exosfinancial
--FileName=edi_YYYYMMDD
--BONU_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
resisin,
'select case when len(cast(BonNotes as varchar(255)))>40 then BonNotes else rtrim(char(32)) end As Notes FROM BON WHERE BONID = '+ cast(maintab.BonID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01EDILTD2LAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EXOSUS33AXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
case when bbeexc.bbgexhid is not null then '114'+EXCHGID + cast(bbeexc.seqnum as char(1))
     when bbeexc.bbgexhid is null then '114'+EXCHGID + '1'
     end as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BONU'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when bbeexc.bbgexhid<>'' then '/FG/'+bbeexc.bbgexhid else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN bbeexc.bbgexhtk<>''
     THEN '/TS/' + replace(replace(replace(bbeexc.bbgexhtk,'%','PCT'),'&','and'),'_',' ')
     WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(maintab.cfi)=6 THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN DuebillsRedemDate IS NOT NULL
     THEN ':98A::MCTD//'+CONVERT ( varchar , DuebillsRedemDate,112)
     ELSE ''
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resisin<>'' THEN ':35B:ISIN ' + resisin
     when resisin='' and isnull(bberes.bbgexhid,'')<>'' then ':35B:/FG/'+bberes.bbgexhid
     else '' end,
case when resisin<>'' and isnull(bberes.bbgexhid,'')<>'' then '/FG/'+bberes.bbgexhid else '' end,
case when resisin='' and isnull(bberes.bbgexhid,'')='' then ':35B:/US/999999999' else '' end,
case when resisin='' and isnull(bberes.bbgexhid,'')='' then 'NOT AVAILABLE AT PRESENT' else '' end,
':16R:FIA',
CASE WHEN isnull(bberes.bbgexhid,'')<>'' and MCD<>'' THEN ':94B::PLIS//EXCH/' + MCD ELSE ':94B::PLIS//SECM' END,
':16S:FIA',
CASE WHEN maintab.RatioNew<>'' AND maintab.RatioOld<>''
     THEN ':92D::ADEX//'+rtrim(cast(maintab.RatioNew as char (15)))+
                     '/'+rtrim(cast(maintab.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
case when bon2.bonid is not null then ':16R:SECMOVE' ELSE '' end,
case when bon2.bonid is not null then ':22H::CRDB//CRED' ELSE '' end,
CASE WHEN isnull(bon2scmst.isin,'')<>'' THEN ':35B:ISIN ' + bon2scmst.isin else '' END,
case when bon2.bonid is not null and isnull(bberes2.bbgexhid,'')<>'' then '/FG/'+bberes2.bbgexhid else '' end,
case when bon2.bonid is not null and isnull(bon2scmst.isin,'')='' and isnull(bberes2.bbgexhid,'')='' then ':35B:/US/999999999' else '' end,
case when bon2.bonid is not null and isnull(bon2scmst.isin,'')='' and isnull(bberes2.bbgexhid,'')='' then 'NOT AVAILABLE AT PRESENT' else '' end,
case when bon2.bonid is not null then ':16R:FIA' ELSE '' end,
CASE 
     WHEN bon2.bonid is not null and isnull(bberes2.bbgexhid,'')<>''
     THEN ':94B::PLIS//EXCH/' + MCD
     WHEN bon2.bonid is not null and isnull(bberes2.bbgexhid,'')=''
     THEN ':94B::PLIS//SECM'
     ELSE ''
     END,
case when bon2.bonid is not null then ':16S:FIA' ELSE '' end,
CASE WHEN bon2.bonid is not null and bon2.RatioNew<>'' AND bon2.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(bon2.RatioNew as char (15)))+
                    '/'+ltrim(cast(bon2.RatioOld as char (15))) 
     WHEN bon2.bonid is not null
     THEN ':92K::ADEX//UKWN' 
     ELSE ''
     END as COMMASUB,
CASE WHEN bon2.bonid is not null and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     WHEN bon2.bonid is not null
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
case when bon2.bonid is not null then ':16S:SECMOVE' ELSE '' end,
':16S:CAOPTN',
'' as Notes, 
'-}$'
From v_EQ_BONU as maintab
left outer join serialseq on maintab.eventid = serialseq.rdid and 2=serialseq.seqnum and 'BON'=serialseq.eventcd
left outer join bon as bon2 on serialseq.uniqueid = bon2.bonid
left outer join scmst as bon2scmst on bon2.ressecid = bon2scmst.secid
left outer join figiseq as bbeexc on maintab.secid=bbeexc.secid and maintab.exchgcd=bbeexc.exchgcd
left outer join figiseq as bberes on maintab.ressecid=bberes.secid and maintab.exchgcd=bberes.exchgcd and bbeexc.curencd=bberes.curencd
left outer join figiseq as bberes2 on bon2.ressecid=bberes2.secid and maintab.exchgcd=bberes2.exchgcd and bbeexc.curencd=bberes2.curencd
WHERE
mainTFB1<>''
and changed>'2018/01/08' and changed<'2018/01/13' and mainTFB1<>''
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and maintab.exchgid<>'' and exchgid is not null
and maintab.exchgcd=maintab.primaryexchgcd
and maintab.Actflag<>'D'
and not (maintab.sectycd='DR' and maintab.lapsedpremium<>'')
order by caref2 desc, caref1, caref3
