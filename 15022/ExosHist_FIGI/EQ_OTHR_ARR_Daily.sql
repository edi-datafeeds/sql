--SEMETABLE=client.dbo.seme_exosfinancial
--FileName=edi_YYYYMMDD
--OTHR_ARR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=ARR
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=100008000
--TEXTCLEAN=2

--# 1
use wca
select distinct
'select distinct V10s_mpay.* from V10s_MPAY'
+ ' where (v10s_mpay.actflag='+char(39)+'I'+char(39)+ ' or v10S_mpay.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'ARR'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
Changed,
'select  ArrNotes As Notes FROM Arr WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01EDILTD2LAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EXOSUS33AXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
case when bbeexc.bbgexhid is not null then '112'+EXCHGID + cast(bbeexc.seqnum as char(1))
     when bbeexc.bbgexhid is null then '112'+EXCHGID + '1'
     end as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when bbeexc.bbgexhid<>'' then '/FG/'+bbeexc.bbgexhid else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN bbeexc.bbgexhtk<>''
     THEN '/TS/' + replace(replace(replace(bbeexc.bbgexhtk,'%','PCT'),'&','and'),'_',' ')
     WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <>'' and ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN RecDate <>'' and RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN DuebillsRedemDate IS NOT NULL
     THEN ':98A::MCTD//'+CONVERT ( varchar , DuebillsRedemDate,112)
     ELSE ':98B::MCTD//UKWN'
     END,
case when wca.dbo.mpay.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_OTHR_ARR as maintab
left outer join figiseq as bbeexc on maintab.secid=bbeexc.secid and maintab.exchgcd=bbeexc.exchgcd
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'ARR' = wca.dbo.mpay.sevent
                                             and 'D'=wca.dbo.mpay.paytype and 'D'<>wca.dbo.mpay.actflag
WHERE 
changed>'2018/01/08' and changed<'2018/01/13' and mainTFB1<>''
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3
