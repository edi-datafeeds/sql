--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_REDM
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
maintab.Changed,
CASE WHEN EXCHGID is not null
     THEN '420'+ EXCHGID+cast(Seqnum as char(1))
     ELSE '420'
     END as CAref1,
maintab.SecID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
':23G:NEWM' MessageStatus,
':22F::CAEV//REDM',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , getdate(),112) + replace(convert ( varchar, getdate(), 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN maintab.PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN maintab.PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(maintab.cfi)=6 THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when maintab.MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, maintab.MaturityDate,112) else '' end,
CASE WHEN maintab.InterestRate <> '' THEN ':92A::INTR//'+maintab.InterestRate ELSE '' END as COMMASUB,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.RedemDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , maintab.RedemDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN  maintab.CurenCD <>''
     THEN ':11A::OPTN//' +  maintab.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , maintab.RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN maintab.RedemPrice <> '' and maintab.RedemPrice <> '0' and maintab.nominalvalue<>'0' and maintab.nominalvalue<>'' and maintab.CurenCD<>''
     THEN ':90B::OFFR//ACTU/' + maintab.CurenCD 
          + maintab.RedemPrice
     WHEN maintab.RedemPercent <>''
     THEN ':90A::OFFR//PRCT/' + maintab.RedemPercent
     WHEN maintab.nominalvalue <>'' and maintab.RedemPrice<>''
          and maintab.nominalvalue <>'0' and maintab.RedemPrice<>'0'
     THEN ':90A::OFFR//PRCT/' + cast(cast(cast(maintab.RedemPrice as decimal(14,5))*100
                                 /cast(maintab.nominalvalue as decimal(14,5)) as money) as varchar (30))
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
CASE WHEN maintab.parvalue<>'' THEN ':36B::MIEX//FAMT/'+maintab.parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From v_FI_MATU as maintab
left outer join redem on maintab.secid=redem.secid and '2017/01/09'=redem.redemdate
WHERE
redem.redemid is null
and (maintab.statusflag<>'I')
and maintab.maturitydate='2017/01/09'
and mainTFB1<>''
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
order by caref2 desc, caref1, caref3
