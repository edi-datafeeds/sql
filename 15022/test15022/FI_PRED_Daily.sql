--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_USCA_YYYYMMDD
--YYYYMMDD_FI564_PRED
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then rtrim(char(32)) else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '410'+EXCHGID+cast(Seqnum as char(1))
     ELSE '410'
     END as CAref1,
EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRED',
case when maintab.mandoptflag = 'M' then ':22F::CAMV//MAND' else ':22F::CAMV//VOLU' end,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
CASE WHEN redem.poolfactor is not null
     THEN ':25D::PROC//COMP'
     ELSE ':25D::PROC//PREC'
     END as PROCCOMP,
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':36B::MINO//FAMT/1000,',
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
CASE WHEN InterestRate <> '' THEN ':92A::INTR//'+InterestRate ELSE '' END as COMMASUB,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.RedemDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , maintab.RedemDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN redem.poolfactor is not null
     THEN ':92A::PRFC//'+cast(cast(redem.poolfactor as float) as varchar)
     ELSE ''
     END as COMMASUB,
':92A::NWFC//'+cast(cast(maintab.poolfactor as float) as varchar) as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN maintab.CurenCD  <>''
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar, maintab.RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
maintab.redemprice,
maintab.redempremium,
maintab.nominalvalue,
':90A::OFFR//PRCT/' as NOCRLF,
prctvalue as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,  
'-}$'
From v_FI_REDM as maintab
left outer join redem on maintab.secid=redem.secid
WHERE
mainTFB1<>''
and prctvalue is not null
and changed>(select getdate()-1)
and maintab.primaryexchgcd=maintab.exchgcd
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and maintab.redemtype<>'BB' and maintab.redemtype<>'BBED' and maintab.redemtype<>'BBRD'
and (maintab.indefpay='' or maintab.indefpay = 'P')
and maintab.partfinal = 'P'
and maintab.redemdate <>''
and maintab.redemdate <> '1800/01/01'
and not (maintab.redemtype='MAT' or maturitydate=maintab.redemdate)
and maintab.poolfactor <> ''
and maintab.redemtype <> 'PUT'
and maintab.mandoptflag<>''
and (redem.redemid is null or redem.redemid=
(select top 1 redemid from redem as subredem
where
maintab.secid=subredem.secid
and 'D'<>subredem.actflag
and ''<>subredem.poolfactor
and '0'<>subredem.poolfactor
and maintab.redemdate>subredem.redemdate
order by subredem.redemdate desc))
order by caref2 desc, caref1, caref3
