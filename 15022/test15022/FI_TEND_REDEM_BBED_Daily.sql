--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_REDM
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7980
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then rtrim(char(32)) else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01SAMPLEXXXXXO0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Redemdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Redemdate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '201' END as CAref1, */
'150' as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//TEND' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
 ':98C::PREP//'+CONVERT(varchar , maintab.changed,112) + replace(CONVERT(varchar, maintab.changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + maintab.mainTFB1 as TFB,
case when len(maintab.Issuername)>35
     then substring(maintab.Issuername,1,35)
     else maintab.Issuername 
     end as TIDYTEXT,
case when len(maintab.Issuername)>35
     then replace(substring(maintab.Issuername,36,35),'&','and')
     else ''
     end,
case when maintab.bonddesc1<>''
     then substring(maintab.bonddesc1+' '+maintab.bonddesc2,1,35)
     else substring(maintab.bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(maintab.bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN maintab.MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + maintab.MCD END,
CASE WHEN len(maintab.cfi)=6 THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
case when maintab.debtcurrency<>'' then ':11A::DENO//'+maintab.debtcurrency else '' end,
case when maintab.MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, maintab.MaturityDate,112) else '' end,
CASE WHEN cast(maintab.parvalue as float)>1 and maintab.parvalue like '%.00000%' 
     THEN ':36B::MINO//FAMT/'+rtrim(replace(maintab.ParValue,'.00000','.'))
     WHEN maintab.ParValue<>'' THEN ':36B::MINO//FAMT/'+maintab.ParValue
     ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT(varchar , maintab.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN maintab.TenderOfferor<>''
     then ':70E::OFFO//'+upper(replace(substring(maintab.TenderOfferor,1,35),'�','e'))
     ELSE ''
     END as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN maintab.CurenCD <>''
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN maintab.TenderCloseDate is not null
     THEN ':98A::MKDT//' + CONVERT(varchar , maintab.TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN maintab.TenderOpenDate is not null AND maintab.TenderCloseDate is not null
     THEN ':69A::PWAL//'
          +CONVERT(varchar , maintab.TenderOpenDate,112)+'/'
          +CONVERT(varchar , maintab.TenderCloseDate,112)
     WHEN maintab.TenderOpenDate is not null
     THEN ':69C::PWAL//'
          +CONVERT(varchar , maintab.TenderOpenDate,112)+'/UKWN'
     WHEN maintab.TenderCloseDate is not null
     THEN ':69E::PWAL//UKWN/'
          +CONVERT(varchar , maintab.TenderCloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.RedemDate is not null
     THEN ':98A::PAYD//' + CONVERT(varchar , maintab.RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
case when maintab.actuvalue is not null then ':90B::OFFR//ACTU/'+maintab.curencd else '' end as NOCRLF1, 
case when maintab.actuvalue is null or maintab.actuvalue ='' then ':90E::OFFR//UKWN' else '' end,
case when maintab.actuvalue is not null then cast(maintab.actuvalue as varchar) else '' end as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//CASH',
CASE WHEN bbrd.CurenCD <>''
     THEN ':11A::OPTN//' + bbrd.CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN bbrd.TenderCloseDate is not null
     THEN ':98A::MKDT//' + CONVERT(varchar , bbrd.TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN bbrd.TenderOpenDate is not null AND bbrd.TenderCloseDate is not null
     THEN ':69A::PWAL//'
          +CONVERT(varchar , bbrd.TenderOpenDate,112)+'/'
          +CONVERT(varchar , bbrd.TenderCloseDate,112)
     WHEN bbrd.TenderOpenDate is not null
     THEN ':69C::PWAL//'
          +CONVERT(varchar , bbrd.TenderOpenDate,112)+'/UKWN'
     WHEN bbrd.TenderCloseDate is not null
     THEN ':69E::PWAL//UKWN/'
          +CONVERT(varchar , bbrd.TenderCloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN bbrd.RedemDate is not null
     THEN ':98A::PAYD//' + CONVERT(varchar , bbrd.RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
case when bbrd.actuvalue is not null then ':90B::OFFR//ACTU/'+bbrd.curencd else '' end as NOCRLF1, 
case when bbrd.actuvalue is null or bbrd.actuvalue ='' then ':90E::OFFR//UKWN' else '' end,
case when bbrd.actuvalue is not null then cast(bbrd.actuvalue as varchar) else '' end as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//999',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN bbrd.TenderCloseDate is not null
     THEN ':98A::MKDT//' + CONVERT(varchar , bbrd.TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_FI_REDM_BBRD as maintab
inner join wca.dbo.v_FI_REDM_BBRD as bbrd on maintab.secid=bbrd.secid and isnull(maintab.tenderopendate,'') = isnull(bbrd.tenderopendate,'') and 'BBRD'=bbrd.redemtype and bbrd.actflag<>'D'
/* left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'BIDS'=SR.CAEVMAIN */
WHERE
maintab.mainTFB1<>''
and maintab.secid=3919678
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and maintab.Actflag <> 'D'
and maintab.redemtype='BBED'
and maintab.DutchAuction<>'T'
order by caref2 desc, caref1, caref3
