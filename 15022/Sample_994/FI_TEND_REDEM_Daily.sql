--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_REDM
--FileNameNotes=edi_YYYYMMDD_MT568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(RedemNotes as varchar(255)))<30 then rtrim(char(32)) else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01SAMPLEXXXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Redemdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Redemdate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '209' END as CAref1, */
'150' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//TEND' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN cast(parvalue as float)>1 and parvalue like '%.00000%' 
     THEN ':36B::MINO//FAMT/'+rtrim(replace(ParValue,'.00000','.'))
     WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue
     ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN TenderOfferor<>''
     then ':70E::OFFO//'+upper(replace(substring(TenderOfferor,1,35),'�','e'))
     ELSE ''
     END as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN MinimumDenomination<>'' THEN ':36B::MIEX//FAMT/'+rtrim(replace(MinimumDenomination,'.000000','.')) ELSE '' END as COMMASUB,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
CASE WHEN TenderCloseDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN TenderOpenDate<>'' AND TenderCloseDate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , TenderOpenDate,112)+'/'
          +CONVERT ( varchar , TenderCloseDate,112)
     WHEN TenderOpenDate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , TenderOpenDate,112)+'/UKWN'
     WHEN TenderCloseDate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , TenderCloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':90E::OFFR//UKWN',
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//999',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN TenderCloseDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16S:CAOPTN',
'' as Notes70F,
'-}$'
From v_FI_REDM as maintab
/* left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'TEND'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and ((maintab.isin in (select code from client.dbo.pfisin where accid=994 and actflag='I')
      and maintab.redemdate > getdate())
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=994 and actflag='U')
and maintab.changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and redemdate <> '1800/01/01'
and (redemtype='BB')
and DutchAuction<>'T'
order by caref2 desc, caref1, caref3
