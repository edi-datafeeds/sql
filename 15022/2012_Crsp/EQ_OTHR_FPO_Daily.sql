--SEMETABLE=client.dbo.seme_crsp
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--OTHR_EQ564_YYYYMMDD
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--archive=off
--ArchivePath=n:\15022\crsp\
--FileTidy=N
--TFBNUMBER=3
--INCREMENTAL=
--NOTESLIMIT=8000


--# 1
use wca
select distinct
Changed,
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'166'+EXCHGID+'1' as CAREF1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB3,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>'' THEN ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN pvcurrency <>'' THEN ':11A::DENO//' + pvcurrency END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN FPOAnnounceDate<>'' and FPOAnnounceDate is not null
     THEN ':98A::ANNO//'+CONVERT ( varchar , FPOAnnounceDate,112)
     ELSE ':98B::ANNO//UKWN'
     END,
CASE WHEN PriceAnnounceDate<>'' and PriceAnnounceDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , PriceAnnounceDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN mainTFB3<>''
     THEN ':35B:' + mainTFB3
     ELSE ':35B:' + 'UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN SharesIssued <>''
          AND StratSharesIssued <>''
     THEN ':92D::NEWO//'+rtrim(cast(SharesIssued as char (15)))+
                    '/'+rtrim(cast(StratSharesIssued as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN SharesIssueDate <>'' AND SharesIssueDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , SharesIssueDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
':70E::ADTX//',
'Disclosure Form: '+rtrim(FormType)+';',
'Offer Amount: '+SharesOffered+';',
'SharesOffered: '+SharesOffered+';',
'StratSaleOffered: '+StratSaleOffered+';',
'Old Shares Offered: '+StratSaleOffered+';',
'Over Allotment: '+GreenOffered+';',
'Shares Priced Amount: '+SharesIssued+';',
'Old Shares Priced: '+StratSharesIssued+';',
'Offer Price: '+Price+';',
'CurenCD: '+CurenCD+';',
'FpoValue: '+FpoValue+';',
case when SharesIssuedate is null then 'SharesIssuedate:' else 'SharesIssuedate: '+CONVERT ( varchar , SharesIssuedate,112)+';' end,
case when GreenExercisedate is null then 'GreenExercisedate:' else 'GreenExercisedate: '+CONVERT ( varchar , GreenExercisedate,112)+';' end,
'GreenSharesIssued: '+GreenSharesIssued+';',
':16S:ADDINFO',
'-}$'
From v_EQ_OTHR_FPO
WHERE 
FPOAnnounceDate is not null
and uscode<>'' and uscode is not null
and (mcd<>'' and mcd is not null)
and changed > (select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd
order by caref2 desc, caref1, caref3
