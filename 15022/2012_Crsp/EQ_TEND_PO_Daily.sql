--SEMETABLE=client.dbo.seme_crsp
--FileName=YYYYMMDD_EQ564
--TEND_PO_EQ564_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_Crsp\
--FileTidy=N
--TFBNUMBER=3
--INCREMENTAL=
--NOTESLIMIT=8000

--# 1
use wca
select
Changed,
'select  PONotes As Notes FROM PO WHERE RDID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'132'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB3,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN OfferOpens <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , OfferOpens,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> '' AND CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + CurenCD
     END,
':17B::DFLT//N',
CASE WHEN OfferCloses <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , OfferCloses,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN OfferCloses <> ''
       AND OfferOpens <> ''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , OfferOpens,112)+'/'
          +CONVERT ( varchar , OfferCloses,112)
     WHEN OfferOpens <> ''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , OfferOpens,112)+'/UKWN'
     WHEN OfferCloses <> ''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , OfferCloses,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN TndrStrkPrice <> '' 
     THEN ':90B::OFFR//ACTU/'+CurenCD+
          +substring(TndrStrkPrice,1,15)
     WHEN MaxPrice <>'' 
     THEN ':90B::OFFR//ACTU/'+CurenCD+
          +substring(MaxPrice,1,15)     
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From wca2.dbo.t_EQ_TEND_PO 
WHERE 
uscode<>'' and uscode is not null
and (mcd<>'' and mcd is not null)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (OfferCloses>getdate()-183
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
*/
order by caref2 desc, caref1, caref3