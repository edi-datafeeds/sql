--SEMETABLE=client.dbo.seme_crsp
--FileName=YYYYMMDD_EQ564
--MRGR_EQ564_YYYYMMDD
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=on
--ArchivePath=n:\15022\2012_Crsp\
--FileTidy=N
--sEvent=MRGR
--TFBNUMBER=3
--INCREMENTAL=
--NOTESLIMIT=8000

--# 1
use wca
select
'select distinct * from V10s_MPAY'
+ ' where (v10s_MPAY.actflag='+char(39)+'I'+char(39)+ ' or v10S_MPAY.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'MRGR'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
Changed,
'select  MrgrTerms As Notes FROM MRGR WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes, 
paydate as PAYLINK,
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'126'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MRGR',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB3,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN EffectiveDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN RecDate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From wca2.dbo.t_EQ_MRGR
WHERE 
uscode<>'' and uscode is not null
and (mcd<>'' and mcd is not null)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd
/*
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='U')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='U')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='U')))
     OR 
    (Exdate>getdate()-183
     and (sedol in (select code from portfolio.dbo.fsedol where accid = 190 and actflag='I')
          or isin in (select code from portfolio.dbo.fisin where accid = 190 and actflag='I')
          or uscode in (select code from portfolio.dbo.fuscode where accid = 190 and actflag='I'))
*/
order by caref2 desc, caref1, caref3

