--SEMETABLE=client.dbo.seme_investec
--FileName=edi_YYYYMMDD
--SPLF_BON_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\investec\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=250000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(BonNotes as varchar(255)))>40 then BonNotes else rtrim(char(32)) end As Notes FROM BON WHERE BONID = '+ cast(maintab.BonID as char(16)) as ChkNotes,
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'122'+EXCHGID+cast(maintab.Seqnum as char(1)) as CAref1,
EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLF'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN' THEN '/GB/' + Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN maintab.PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN maintab.PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(maintab.cfi)=6 THEN ':12C::CLAS//'+maintab.cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.secid<>maintab.ressecid and resTFB1<>''
     THEN ':35B:' + resTFB1
     WHEN maintab.secid<>maintab.ressecid and resTFB1=''
     THEN ':35B:UKWN'
     ELSE ':35B:' + mainTFB2
     END,
':16R:FIA',
CASE 
     WHEN maintab.secid<>maintab.ressecid and resTFB1<>''
     THEN ':94B::PLIS//SECM'
     WHEN maintab.secid<>maintab.ressecid and resTFB1=''
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
':16S:FIA',
CASE WHEN maintab.RatioNew<>'' AND maintab.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(maintab.RatioNew as char (15)))+
                    '/'+ltrim(cast(maintab.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
case when bon2.bonid is not null then ':16R:SECMOVE' ELSE '' end,
case when bon2.bonid is not null then ':22H::CRDB//CRED' ELSE '' end,
case WHEN bon2.bonid is not null and bon2scmst.isin is not null and bon2scmst.isin<>''
     THEN ':35B:ISIN ' + bon2scmst.isin
     WHEN bon2.bonid is not null
     THEN ':35B:UKWN'
     ELSE ''
     END,
case when bon2.bonid is not null then ':16R:FIA' ELSE '' end,
CASE 
     WHEN bon2.bonid is not null
     THEN ':94B::PLIS//SECM'
     ELSE ''
     END,
case when bon2.bonid is not null then ':16S:FIA' ELSE '' end,
CASE WHEN bon2.bonid is not null and bon2.RatioNew<>'' AND bon2.RatioOld<>''
     THEN ':92D::ADEX//'+ltrim(cast(bon2.RatioNew as char (15)))+
                    '/'+ltrim(cast(bon2.RatioOld as char (15))) 
     WHEN bon2.bonid is not null
     THEN ':92K::ADEX//UKWN' 
     ELSE ''
     END as COMMASUB,
CASE WHEN bon2.bonid is not null and Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     WHEN bon2.bonid is not null
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
case when bon2.bonid is not null then ':16S:SECMOVE' ELSE '' end,
':16S:CAOPTN',
'' as RawNotesFull, 
'-}$'
From v_EQ_BONU as maintab
inner join serialseq as mainbon on maintab.eventid = mainbon.rdid and 1=mainbon.seqnum and 'BON'=mainbon.eventcd
left outer join serialseq on maintab.eventid = serialseq.rdid and 2=serialseq.seqnum and 'BON'=serialseq.eventcd
left outer join bon as bon2 on serialseq.uniqueid = bon2.bonid
left outer join scmst as bon2scmst on bon2.ressecid = bon2scmst.secid
WHERE
mainTFB1<>''
and ((sedol in (select code from client.dbo.pfsedol where accid=352 and actflag='I')
and Exdate > getdate()-31)
OR
(sedol in (select code from client.dbo.pfsedol where accid=352 and actflag='U')
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and substring(maintab.PrimaryExchgCD,1,2)='US'
and exchgid<>'' and exchgid is not null
and not (maintab.sectycd='DR' and maintab.lapsedpremium<>'')
order by caref2 desc, caref1, caref3
