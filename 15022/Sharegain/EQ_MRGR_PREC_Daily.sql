--SEMETABLE=client.dbo.seme_sharegain
--FileName=edi_YYYYMMDD
--MRGR_PREC_EQ564_YYYYMMDD
--FileNameNotes=edi_YYYYMMDD_MT568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq=3 
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  MrgrTerms As Notes FROM MRGR WHERE RdID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
maintab.paydate as PAYLINK,
'{1:F01SHAREGAINXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.EffectiveDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.EffectiveDate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '126' END as CAref1, */
'126' as CAref1,
maintab.EventID as CAREF2,
bbemain.BbeID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//MRGR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//PREC' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
'/FG/'+bbemain.bbgexhid,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
'/TS/' + replace(replace(replace(bbemain.bbgexhtk,'%','PCT'),'&','and'),'_',' ') as TIDYTEXT,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.announcedate,112) ELSE '' END,
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN EffectiveDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:/US/999999999',
'NOT AVAILABLE AT PRESENT',
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':92K::ADEX//UKWN',
CASE WHEN maintab.paydate<>'' and maintab.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , maintab.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes70F,
'-}$'
From v_EQ_MRGR as maintab
inner join wca.dbo.bbe as bbemain on maintab.secid=bbemain.secid and maintab.exchgcd=bbemain.exchgcd and 'D'<>bbemain.actflag
left outer join wca.dbo.MPAY on maintab.eventid = wca.dbo.MPAY.eventid and 'MRGR' = wca.dbo.MPAY.sevent
                                        and 'D'<>wca.dbo.MPAY.actflag
/* left outer join client.dbo.seme_sharegain as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'MRGR'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and ((bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='I')
      and maintab.exdate > getdate())
OR
(bbemain.bbgexhid in (select code from client.dbo.pffigi where accid=165 and actflag='U')
and maintab.changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and wca.dbo.MPAY.eventid is null
order by caref2 desc, caref1, caref3
