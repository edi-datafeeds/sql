--SEMETABLE=client.dbo.seme_pictet
--FileName=edi_YYYYMMDD
--EXRI_RENO_RTS_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq=3
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=7000
--MESSTERMINATOR=-}
--ROWTERMINATOR=UX
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select case when len(cast(RTSNotes as varchar(255)))>40 then RTSNotes else rtrim(char(32)) end As Notes FROM RTS WHERE RtsID = '+ cast(maintab.EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01PICTGB2LXXXX7224140951}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX7224140951'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.EndTrade>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.EndTrade is not null
              and (SR.CAMV<>'CHOS' or SR.TFB<>':35B:'+mainTFB1)
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '118' END as CAref1, */
'118' as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXRI' as CAEV,
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
/* ':16R:LINK',
'160' as CAref4,
':16S:LINK', */
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(Localcode,'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN EndSubscription<>''
     THEN ':98A::RDDT//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::RDDT//UKWN'
     END,
CASE WHEN EndSubscription<>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndSubscription<>''
       AND StartSubscription<>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/'
          +CONVERT ( varchar , EndSubscription,112)
     WHEN StartSubscription<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/UKWN'
     WHEN EndSubscription<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN isnull(resisin,'')<>'' THEN ':35B:ISIN ' + resisin
     else '' end,
case when isnull(resisin,'')='' then ':35B:/US/999999999' else '' end,
case when isnull(resisin,'')='' then 'NOT AVAILABLE AT PRESENT' else '' end,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN EndTrade<>'' 
       AND StartTrade<>'' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/'
          +CONVERT ( varchar , EndTrade,112)
     WHEN StartTrade<>'' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , StartTrade,112) + '/UKWN'
     WHEN EndTrade<>'' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
CASE WHEN NewExerciseRatio <>'' AND OldExerciseRatio <>'' and NewExerciseRatio <>'0' AND OldExerciseRatio <>'0'
     THEN ':92D::NEWO//'+rtrim(cast(NewExerciseRatio as char (15)))+
                     '/'+rtrim(cast(OldExerciseRatio as char (15))) 
     WHEN RatioNew <>'' AND RatioOld <>'' and RatioNew <>'0' AND RatioOld <>'0'
     THEN ':92D::NEWO//1,/1,'
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN StartTrade<>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN StartTrade<>'' 
               AND StartTrade IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartTrade,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN IssuePrice<>''
     THEN ':90B::PRPP//ACTU/'+maintab.CurenCD+ltrim(cast(IssuePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//LAPS',
':17B::DFLT//Y',
':16S:CAOPTN',
'' as Notes70F,
'-}'
From v_EQ_EXRI_RENO_RTS_RtsID as maintab
left outer join client.dbo.seme_usbank as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'EXRI'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and (((maintab.resisin in (select code from client.dbo.pfisin where accid=149 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=149 and actflag='I')
      or maintab.ressedol in (select code from client.dbo.pfsedol where accid=149 and actflag='I'))
and EndTrade > getdate())
OR
((maintab.resisin in (select code from client.dbo.pfisin where accid=149 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=149 and actflag='U')
or maintab.ressedol in (select code from client.dbo.pfsedol where accid=149 and actflag='U'))
and maintab.changed>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)))
order by caref2 desc, caref1, caref3
