--SEMETABLE=client.dbo.seme_freetrade
--FileName=edi_YYYYMMDD
--CHAN_LTCHG_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where feeddate = '2018/01/30'
--archive=off
--ArchivePath=n:\15022\investec\
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed,
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'199'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN' THEN '/GB/' + Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN EffectiveDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//TERM',
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//RE/TRADABLE UNIT CHANGE',
'OLD 1UNIT/'+cast(maintab.oldlot as varchar(20))+'SHS',
'NEW 1UNIT/'+cast(maintab.newlot as varchar(20))+'SHS',
':16S:ADDINFO',
'-}$'
From v_EQ_CHAN_LTCHG as maintab
WHERE
mainTFB1<>''
and ((isin in (select code from client.dbo.pfisin where accid=376 and actflag='I')
and Effectivedate > getdate()-31)
OR
(isin in (select code from client.dbo.pfisin where accid=376 and actflag='U')
and changed>'2018/01/30' and changed<'2018/01/31'))
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and exchgid<>'' and exchgid is not null
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and maintab.oldlot is not null
and maintab.newlot is not null
and maintab.eventtype<>'CLEAN'
and maintab.eventtype<>'CORR'
and maintab.oldlot<>maintab.newlot
and maintab.opol=maintab.MCD
order by caref2 desc, caref1, caref3
