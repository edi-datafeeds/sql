--SEMETABLE=client.dbo.seme_freetrade
--FileName=edi_YYYYMMDD
--OTHR_ARR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where feeddate = '2018/01/30'
--archive=off
--ArchivePath=n:\15022\RAT\
--FileTidy=N
--sEvent=OTHR
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select
'select distinct V10s_MPAY.* from V10s_MPAY'
+ ' where (v10s_MPAY.actflag='+char(39)+'I'+char(39)+ ' or v10S_MPAY.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'ARR'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
Changed,
'select  ArrNotes As Notes FROM Arr WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
'{1:F01EDIGB2LXXISO0300000054}{2:I564XXXXXXXX0XXXXN}{4:' as ISOHDR,
':16R:GENL',
'112'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Sedol <> '' and substring(mainTFB1,1,4) = 'ISIN' THEN '/GB/' + Sedol ELSE '' END,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <>'' and ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , ExDate,112)
     ELSE ':98A::XDTE//UKWN'
     END,
CASE WHEN maintab.PayDate <>'' and maintab.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , maintab.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN RecDate <>'' and RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.mpay.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_OTHR_ARR as maintab
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'ARR' = wca.dbo.mpay.sevent
                                             and 'D'=wca.dbo.mpay.paytype and 'D'<>wca.dbo.mpay.actflag
WHERE 
mainTFB1<>''
and ((isin in (select code from client.dbo.pfisin where accid=376 and actflag='I')
and Exdate > getdate()-31)
OR
(isin in (select code from client.dbo.pfisin where accid=376 and actflag='U')
))
and Exdate>getdate()-31
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3
