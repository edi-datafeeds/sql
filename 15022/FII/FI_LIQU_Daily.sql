--SEMETABLE=client.dbo.seme_fii
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_LIQU
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--sEvent=LIQ
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=340
--TEXTCLEAN=2

--# 1
use wca
select distinct
'select * from V10s_MPAY'
+ ' where (v10s_MPAY.Actflag='+char(39)+'I'+char(39)+ ' or v10S_MPAY.Actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'LIQ'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
changed,
null as PAYLINK,
v_FI_LIQU.changed,
'select case when len(cast(LiquidationTerms as varchar(24)))=22 then rtrim(char(32)) else LiquidationTerms end As Notes FROM LIQ WHERE LIQID = '+ cast(v_FI_LIQU.EventID as char(16)) as ChkNotes,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '313'+EXCHGID+cast(Seqnum as char(1))
     ELSE '313'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_LIQU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_LIQU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_LIQU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_LIQU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU',
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>''
     then ':11A::DENO//'+debtcurrency
     else ''
     end,
case when MaturityDate is not null
     then ':98A::MATU//'++CONVERT(varchar, MaturityDate,112)
     else ''
     end,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_FI_LIQU.RdDate is not null and v_FI_LIQU.RdDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_FI_LIQU.RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
/* '' as Notes, */ 
'-}$'
From v_FI_LIQU
WHERE
mainTFB1<>''
/* and (primaryexchgcd=exchgcd or primaryexchgcd ='') */
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
/* and substring(primaryexchgcd,1,2)='US' */
order by caref2 desc, caref1, caref3
