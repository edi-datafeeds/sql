--SEMETABLE=client.dbo.seme_fii
--FileName=edi_YYYYMMDD
--TEND_TKOVR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568_ICR.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=TKOVR
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=340
--TEXTCLEAN=2

--# 1
use wca
select distinct
'select distinct * from V10s_MPAY'
+ ' where (Actflag='+char(39)+'I'+char(39) +' or Actflag='+char(39)+'U'+char(39)
+') and eventid=' + rtrim(cast(maintab.EventID as char(10))) 
+ ' and sEvent='+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype<>'+ char(39) + 'D'+ char(39)
+ ' Order By OptionID,SerialID' as MPAYlink,
Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(EventID as char(16)) as ChkNotes, 
null as PAYLINK,
Opendate as OPENLINK,
maintab.Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01EDIXGB2LXISO0300000054}{2:I564AAAAAAAAXBBBA2333}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Closedate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Closedate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '131' END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97A::SAFE//NONREF' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN primaryexchgcd=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN primaryexchgcd<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Opendate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::WUCO//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN UnconditionalDate IS NOT NULL and UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+replace(substring(maintab.Offerorname,1,23),'�','e')
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes, 
'-}$'
From wca2.dbo.i_EQ_TEND_TKOVR as maintab
left outer join client.dbo.seme_fii as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'TEND'=SR.CAEVMAIN
WHERE
mainTFB1<>''
/* and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='') */
and CmAcqdate is null
/* and maintab.Closedate>getdate()-8 and Closedate is not null */
order by caref2 desc, caref1, caref3
