--SEMETABLE=client.dbo.seme_exosfinancial
--FileName=edi_YYYYMMDD
--INCR_PVRD_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  PVRDNotes As Notes FROM PVRD WHERE PVRDID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01EDILTD2LAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EXOSUS33AXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
case when bbcmain.bbgcompid is not null then '154'+EXCHGID + cast(bbcmain.seqnum as char(1))
     when bbcmain.bbgcompid is null then '154'+EXCHGID + '1'
     end as CAref1,
EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INCR'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when bbcmain.bbgcompid<>'' then '/FG/'+bbcmain.bbgcompid
     when isnull(bbebond.bbgExhid,'')<>'' then '/FG/'+bbebond.bbgExhid else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN bbcmain.bbgcomptk<>''
     THEN '/TS/' + replace(replace(replace(bbcmain.bbgcomptk,'%','PCT'),'&','and'),'_',' ')
     WHEN bbebond.bbgexhtk<>''
     THEN '/TS/' + replace(replace(replace(bbebond.bbgexhtk,'%','PCT'),'     WHEN Localcode <>''','and'),'_',' ')
     WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT/'+rtrim(cast(NewParValue as char(15))) as COMMASUB,
':16S:CADETL',
'' as Notes, 
'-}$'
From v_EQ_INCR_DECR_PVRD as maintab
left outer join wca2.dbo.compseq as bbcmain on maintab.secid=bbcmain.secid and maintab.cntrycd=bbcmain.cntrycd
left outer join bbe as bbebond on maintab.secid=bbebond.secid and ''=bbebond.exchgcd and bbebond.actflag<>'D'
WHERE
mainTFB1<>''
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and rtrim(OldParValue)<>'' and rtrim(NewParValue)<>''
and rtrim(OldParValue)<>'0' and rtrim(NewParValue)<>'0'
and cast(rtrim(OldParValue) as float(16,7))<cast(rtrim(NewParValue) as float(16,7))
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3
