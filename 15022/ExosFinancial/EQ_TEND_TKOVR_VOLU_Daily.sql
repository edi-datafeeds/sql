--SEMETABLE=client.dbo.seme_exosfinancial
--FileName=edi_YYYYMMDD
--TEND_TKOVR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=TKOVR
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
case when bbcmain.cntrycd is not null then bbcmain.cntrycd else '' end as BBGCNTRY,
case when bbcmain.curencd is not null then bbcmain.curencd else '' end as BBGCUREN,
maintab.Changed,
'select TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
null as PAYLINK,
Opendate as OPENLINK,
maintab.Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
case when 1=1 then (select '{1:F01EDILTD2LAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EXOSUS33AXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
case when bbcmain.bbgcompid is not null then '131'+EXCHGID + cast(bbcmain.seqnum as char(1))
     when bbcmain.bbgcompid is null then '131'+EXCHGID + '1'
     end as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when bbcmain.bbgcompid<>'' then '/FG/'+bbcmain.bbgcompid
     when isnull(bbebond.bbgExhid,'')<>'' then '/FG/'+bbebond.bbgExhid else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN bbcmain.bbgcomptk<>''
     THEN '/TS/' + replace(replace(replace(bbcmain.bbgcomptk,'%','PCT'),'&','and'),'_',' ')
     WHEN bbebond.bbgexhtk<>''
     THEN '/TS/' + replace(replace(replace(bbebond.bbgexhtk,'%','PCT'),'     WHEN Localcode <>''','and'),'_',' ')
     WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Opendate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::WUCO//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN UnconditionalDate IS NOT NULL and UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
case when wca.dbo.mpay.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+replace(substring(maintab.Offerorname,1,23),'�','e')
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes, 
'-}$'
From v_EQ_TEND_TKOVR as maintab
left outer join wca2.dbo.compseq as bbcmain on maintab.secid=bbcmain.secid and maintab.cntrycd=bbcmain.cntrycd
left outer join bbe as bbebond on maintab.secid=bbebond.secid and ''=bbebond.exchgcd and bbebond.actflag<>'D'
left outer join wca.dbo.mpay on maintab.eventid = wca.dbo.mpay.eventid and 'TKOVR' = wca.dbo.mpay.sevent
                                             and 'D'=wca.dbo.mpay.paytype and 'D'<>wca.dbo.mpay.actflag
WHERE
mainTFB1<>''
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and (maintab.closedate is not null
     and maintab.opendate is not null
     and maintab.minitkovr<>'T')
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3
