--SEMETABLE=client.dbo.seme_exosfinancial
--FileName=edi_YYYYMMDD
--CHAN_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  IschgNotes As Notes FROM ISCHG WHERE ISCHGID = '+ cast(EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01EDILTD2LAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EXOSUS33AXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
case when bbcmain.bbgcompid is not null then '153'+EXCHGID + cast(bbcmain.seqnum as char(1))
     when bbcmain.bbgcompid is null then '153'+EXCHGID + '1'
     end as CAref1,
EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CHAN'as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when bbcmain.bbgcompid<>'' then '/FG/'+bbcmain.bbgcompid
     when isnull(bbebond.bbgExhid,'')<>'' then '/FG/'+bbebond.bbgExhid else '' end,
substring(IssOldname,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN bbcmain.bbgcomptk<>''
     THEN '/TS/' + replace(replace(replace(bbcmain.bbgcomptk,'%','PCT'),'&','and'),'_',' ')
     WHEN bbebond.bbgexhtk<>''
     THEN '/TS/' + replace(replace(replace(bbebond.bbgexhtk,'%','PCT'),'&','and'),'_',' ')
     WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN NameChangeDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':22F::CHAN//NAME',
':70E::NAME//' + substring(replace(IssNewname,'&','and'),1,35) as TIDYTEXT,
':16S:CADETL',
CASE WHEN NewISIN <>'' THEN ':16R:CAOPTN' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':13A::CAON//001' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':22F::CAOP//SECU' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':17B::DFLT//Y' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':16R:SECMOVE' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':22H::CRDB//CRED' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':35B:ISIN ' + NewISIN else '' END,
CASE WHEN NewISIN <>'' and isnull(bbcmain.bbgcompid,'')<>'' then '/FG/'+bbcmain.bbgcompid
     when NewISIN <>'' and isnull(bbebond.bbgExhid,'')<>'' then '/FG/'+bbebond.bbgExhid else '' end,
CASE WHEN NewISIN <>'' THEN ':16R:FIA' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':94B::PLIS//EXCH/' + MCD ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':16S:FIA' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':92D::NEWO//1,/1,' ELSE '' END,
CASE WHEN NewISIN <>'' and icceffectivedate is not null 
     THEN ':98A::PAYD//'+CONVERT ( varchar , icceffectivedate,112)
     WHEN NewISIN <>'' and icceffectivedate is null 
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN NewISIN <>'' THEN ':16S:SECMOVE' ELSE '' END,
CASE WHEN NewISIN <>'' THEN ':16S:CAOPTN' ELSE '' END,
'' as Notes, 
'-}$'
From v_EQ_CHAN_NAME as maintab
left outer join wca2.dbo.compseq as bbcmain on maintab.secid=bbcmain.secid and maintab.cntrycd=bbcmain.cntrycd
left outer join bbe as bbebond on maintab.secid=bbebond.secid and ''=bbebond.exchgcd and bbebond.actflag<>'D'
WHERE
mainTFB1<>''
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
