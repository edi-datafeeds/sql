--SEMETABLE=client.dbo.seme_exosfinancial
--FileName=edi_YYYYMMDD
--EXOF_SCSWP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=10000
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed,
case when 1=1 then (select '{1:F01EDILTD2LAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EXOSUS33AXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
case when bbcmain.bbgcompid is not null then '198'+EXCHGID + cast(bbcmain.seqnum as char(1))
     when bbcmain.bbgcompid is null then '198'+EXCHGID + '1'
     end as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when bbcmain.bbgcompid<>'' then '/FG/'+bbcmain.bbgcompid
     when isnull(bbebond.bbgExhid,'')<>'' then '/FG/'+bbebond.bbgExhid else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN bbcmain.bbgcomptk<>''
     THEN '/TS/' + replace(replace(replace(bbcmain.bbgcomptk,'%','PCT'),'&','and'),'_',' ')
     WHEN bbebond.bbgexhtk<>''
     THEN '/TS/' + replace(replace(replace(bbebond.bbgexhtk,'%','PCT'),'     WHEN Localcode <>''','and'),'_',' ')
     WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Exdate, 112)
     ELSE ':98B::EFFD//UKWN'
     END as DateEx,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':69J::PWAL//UKWN',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resisin<>'' THEN ':35B:ISIN ' + resisin
     when resisin='' and isnull(bbcres.bbgcompid,'')<>'' then ':35B:/FG/'+bbcres.bbgcompid
     else '' end,
case when resisin<>'' and isnull(bbcres.bbgcompid,'')<>'' then '/FG/'+bbcres.bbgcompid else '' end,
case when resisin='' and isnull(bbcres.bbgcompid,'')='' then ':35B:/US/999999999' else '' end,
case when resisin='' and isnull(bbcres.bbgcompid,'')='' then 'NOT AVAILABLE AT PRESENT' else '' end,
':16R:FIA',
CASE WHEN isnull(bbcres.bbgcompid,'')<>'' and MCD<>'' THEN ':94B::PLIS//EXCH/' + MCD ELSE ':94B::PLIS//SECM' END,
':16S:FIA',
CASE WHEN newratio <>''
          AND oldratio <>''
     THEN ':92D::NEWO//'+rtrim(cast(newratio as char (15)))+
                    '/'+rtrim(cast(oldratio as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN Paydate<>'' and Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , Paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_EXOF_SCSWP as maintab
left outer join wca2.dbo.compseq as bbcmain on maintab.secid=bbcmain.secid and maintab.cntrycd=bbcmain.cntrycd
left outer join bbe as bbebond on maintab.secid=bbebond.secid and ''=bbebond.exchgcd and bbebond.actflag<>'D'
left outer join wca2.dbo.compseq as bbcres on maintab.ResSecID=bbcres.secid and maintab.cntrycd=bbcres.cntrycd and bbcmain.curencd=bbcres.curencd
WHERE
mainTFB1<>''
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (maintab.cntrycd='CA' or maintab.cntrycd='US')
and exchgid<>'' and exchgid is not null
order by caref2 desc, caref1, caref3
