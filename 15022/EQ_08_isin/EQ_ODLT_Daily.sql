--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--ODLT
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_ODLT.Changed,
'select  Notes FROM ODDLT WHERE RDID = '+ cast(t_EQ_ODLT.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'163'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_ODLT.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_ODLT.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_ODLT.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_ODLT.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ODLT',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_ODLT.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_ODLT.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_ODLT.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_ODLT.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_ODLT.Recdate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_ODLT.Recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_ODLT.Enddate <> ''
       AND t_EQ_ODLT.Startdate <> ''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_ODLT.Startdate,112)+'/'
          +CONVERT ( varchar , t_EQ_ODLT.Enddate,112)
     WHEN t_EQ_ODLT.Startdate <> ''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_ODLT.Startdate,112)+'/UKWN'
     WHEN t_EQ_ODLT.Enddate <> ''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_ODLT.Enddate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_ODLT.MaxAcpQty is not null
     THEN ':36B::FOLQ//UNIT/'+rtrim(t_EQ_ODLT.MaxAcpQty)
     ELSE ':36C::FOLQ//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM rd WHERE RDID = '+ cast(t_EQ_ODLT.RDID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN t_EQ_ODLT.BuyInCurenCD <> ''
     THEN ':11A::OPTN//' + t_EQ_ODLT.BuyInCurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_ODLT.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_ODLT.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN t_EQ_ODLT.BuyInPrice <>'' and t_EQ_ODLT.BuyInCurenCD <>''
     THEN ':90B::OFFR//ACTU/'+t_EQ_ODLT.BuyInCurenCD+
          +substring(t_EQ_ODLT.BuyInPrice,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_ODLT
WHERE 
mainTFB1<>''
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3