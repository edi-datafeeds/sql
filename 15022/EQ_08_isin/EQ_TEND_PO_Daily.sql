--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--TEND
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_TEND_PO.Changed,
'select  PONotes As Notes FROM PO WHERE RDID = '+ cast(t_EQ_TEND_PO.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'132'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_TEND_PO.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_TEND_PO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_TEND_PO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_TEND_PO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_TEND_PO.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_TEND_PO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_TEND_PO.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_TEND_PO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_TEND_PO.OfferOpens <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_TEND_PO.OfferOpens,112)
     ELSE ':98A::EFFD//19000103'
     END,
CASE WHEN t_EQ_TEND_PO.OfferCloses <> ''
       AND t_EQ_TEND_PO.OfferOpens <> ''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferOpens,112)+'/'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferCloses,112)
     WHEN t_EQ_TEND_PO.OfferOpens <> ''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferOpens,112)+'/UKWN'
     WHEN t_EQ_TEND_PO.OfferCloses <> ''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_TEND_PO.OfferCloses,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_TEND_PO.OfferCloses <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_TEND_PO.OfferCloses,112)
     ELSE ':98B::MKDT//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM rd WHERE RDID = '+ cast(t_EQ_TEND_PO.RDID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN t_EQ_TEND_PO.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_TEND_PO.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_TEND_PO.Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_TEND_PO.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN t_EQ_TEND_PO.TndrStrkPrice <> '' 
     THEN ':90B::OFFR//ACTU/'+t_EQ_TEND_PO.CurenCD+
          +substring(t_EQ_TEND_PO.TndrStrkPrice,1,15)
     WHEN t_EQ_TEND_PO.MaxPrice <>'' 
     THEN ':90B::OFFR//ACTU/'+t_EQ_TEND_PO.CurenCD+
          +substring(t_EQ_TEND_PO.MaxPrice,1,15)     
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_TEND_PO
WHERE 
mainTFB1<>''
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3