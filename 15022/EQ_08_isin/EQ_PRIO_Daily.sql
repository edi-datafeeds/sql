--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--PRIO
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_PRIO.Changed,
'select  PrfNotes As Notes FROM PRF WHERE RDID = '+ cast(t_EQ_PRIO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'110'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_PRIO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_PRIO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_PRIO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_PRIO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_PRIO.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_PRIO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_PRIO.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_PRIO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_PRIO.Exdate <>'' AND t_EQ_PRIO.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_PRIO.Exdate,112)
     ELSE ':98A::XDTE//19000103'
     END as DateEx,
CASE WHEN t_EQ_PRIO.Recdate <>'' AND t_EQ_PRIO.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_PRIO.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
 /* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_PRIO.EventID as char(16)) as Notes, */ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
':17B::DFLT//N',
CASE WHEN t_EQ_PRIO.CurenCD <> '' AND t_EQ_PRIO.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + t_EQ_PRIO.CurenCD
     END,
CASE WHEN t_EQ_PRIO.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_PRIO.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_PRIO.EndSubscription <>''
       AND t_EQ_PRIO.StartSubscription <>''
      THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_PRIO.StartSubscription,112)+'/'
          +CONVERT ( varchar , t_EQ_PRIO.EndSubscription,112)
     WHEN t_EQ_PRIO.StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_PRIO.StartSubscription,112)+'/UKWN'
     WHEN t_EQ_PRIO.EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_PRIO.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_PRIO.MaxPrice <>''
     THEN ':90B::PRPP//ACTU/'+t_EQ_PRIO.CurenCD+
          +substring(t_EQ_PRIO.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_PRIO.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_PRIO.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_PRIO.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_PRIO.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_PRIO.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_PRIO.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN t_EQ_PRIO.RatioNew <>''
          AND t_EQ_PRIO.RatioOld <>''
     THEN ':92D::NEWO//'+ltrim(cast(t_EQ_PRIO.RatioNew as char (15)))+
                    '/'+ltrim(cast(t_EQ_PRIO.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN t_EQ_PRIO.paydate2 <>'' 
             AND t_EQ_PRIO.paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_PRIO.paydate2,112)
     WHEN t_EQ_PRIO.paydate <>'' 
             AND t_EQ_PRIO.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_PRIO.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_PRIO
WHERE 
mainTFB1<>''
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3