--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--EXOF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_EXOF_CTX.Changed,
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(t_EQ_EXOF_CTX.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'127'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_EXOF_CTX.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_EXOF_CTX.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_EXOF_CTX.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_EXOF_CTX.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_EXOF_CTX.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_EXOF_CTX.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_EXOF_CTX.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_EXOF_CTX.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_EXOF_CTX.EndDate <>'' AND t_EQ_EXOF_CTX.EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_EXOF_CTX.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_EXOF_CTX.EndDate <>'' AND t_EQ_EXOF_CTX.EndDate IS NOT NULL
       AND t_EQ_EXOF_CTX.StartDate <>'' AND t_EQ_EXOF_CTX.StartDate IS NOT NULL
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.StartDate,112)+'/'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.EndDate,112)
     WHEN t_EQ_EXOF_CTX.StartDate <>'' AND t_EQ_EXOF_CTX.StartDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.StartDate,112)+'/UKWN'
     WHEN t_EQ_EXOF_CTX.EndDate <>'' AND t_EQ_EXOF_CTX.EndDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_EXOF_CTX.EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:ISIN UKWN'
     END,
':92D::NEWO//1,/1,',
/* Currently no suitable paydate
CASE WHEN t_EQ_EXOF_CTX.Paydate <>'' 
             AND t_EQ_EXOF_CTX.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXOF_CTX.Paydate,112)
     WHEN t_EQ_EXOF_CTX.paydate <>'' 
             AND t_EQ_EXOF_CTX.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXOF_CTX.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_EXOF_CTX
WHERE 
mainTFB1<>''
and upper(eventtype)<>'CLEAN'
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3