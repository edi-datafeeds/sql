--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--CONV
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_CONV.Changed,
'select  ConvNotes As Notes FROM CONV WHERE ConvID = '+ cast(t_EQ_CONV.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'125'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN t_EQ_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_CONV.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_CONV.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_CONV.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_CONV.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_CONV.FromDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_CONV.FromDate,112)
     ELSE ':98A::EFFD//19000103'
     END,
CASE WHEN t_EQ_CONV.ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_CONV.EventID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONV',
CASE WHEN t_EQ_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN t_EQ_CONV.ToDate <>'' AND t_EQ_CONV.FromDate <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_CONV.FromDate,112)+'/'
          +CONVERT ( varchar , t_EQ_CONV.ToDate,112)
     WHEN t_EQ_CONV.FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_CONV.FromDate,112)+'/UKWN'
     WHEN t_EQ_CONV.ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_CONV.ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_CONV.Price <>''
     THEN ':90B::EXER//ACTU/'+t_EQ_CONV.CurenCD+
          +substring(t_EQ_CONV.Price,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN t_EQ_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN t_EQ_CONV.RatioNew <>'' AND t_EQ_CONV.RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(t_EQ_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
/*Currently no suitable paydate
CASE WHEN t_EQ_CONV.Paydate <>'' 
             AND t_EQ_CONV.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_CONV.Paydate,112)
     WHEN t_EQ_CONV.paydate <>'' 
             AND t_EQ_CONV.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_CONV.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,*/
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_CONV
WHERE 
mainTFB1<>''
and Actflag <> ''
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3