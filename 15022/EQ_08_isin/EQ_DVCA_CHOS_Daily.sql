--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--DVCA
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_DVOP.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(t_EQ_DVOP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE
     WHEN t_EQ_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVCA',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_DVOP.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_DVOP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_DVOP.MCD is not null
     THEN ':94B::PLIS//EXCH/' + t_EQ_DVOP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DVOP.Exdate is not null 
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_DVOP.Exdate,112)
     ELSE ':98A::XDTE//19000103'
     END,
CASE WHEN t_EQ_DVOP.Recdate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CG'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGL'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='CGS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='INS'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='MEM'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='SUP'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='TEI'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='ARR'
     THEN ':22F::DIVI//SPEC'
     WHEN DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='ONE'
     THEN ':22F::DIVI//FINL'
     WHEN DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='UN'
     THEN ':22F::DIVI//INTE'
     WHEN DivPeriodCD='VAR'
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//INTE'
     END,
/* START In case of Franking */
CASE WHEN t_EQ_DVOP.UnFrankDiv <> ''
     THEN ':92J::GRSS//UNFR/'+CurenCD_1+rtrim(cast(t_EQ_DVOP.UnFrankDiv as char(15)))
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.FrankDiv <> ''
     THEN ':92J::GRSS//FLFR/'+CurenCD_1+rtrim(cast(t_EQ_DVOP.FrankDiv as char(15)))
     END as COMMASUB,
/* END In case of Franking */     
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_DVOP.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* CASH OPTION START_1 */
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
     THEN ':13A::CAON//001'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_1 <> '' AND t_EQ_DVOP.CurenCD_1 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_1
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C' and (t_EQ_DVOP.DefaultOpt_1='T' 
         or (DefaultOpt_2='F' and DefaultOpt_3='F' and DefaultOpt_4='F' and DefaultOpt_5='F' and DefaultOpt_6='F'))
     THEN ':17B::DFLT//Y'
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
     THEN ':17B::DFLT//N'
ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
            AND  t_EQ_DVOP.Netdividend_1 <> ''
     THEN ':92F::NETT//'+CurenCD_1+ltrim(cast(t_EQ_DVOP.Netdividend_1 as char(15)))
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
            AND  t_EQ_DVOP.Netdividend_1 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
            AND t_EQ_DVOP.Grossdividend_1 <> ''
     THEN ':92F::GRSS//'+CurenCD_1+ltrim(cast(t_EQ_DVOP.Grossdividend_1 as char(15)))
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
            AND t_EQ_DVOP.Grossdividend_1 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
           AND t_EQ_DVOP.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_1 as char(15))
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
           AND t_EQ_DVOP.Taxrate_1 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_1 = 'D' OR t_EQ_DVOP.Divtype_1 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_1 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION START_2 */
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
     THEN ':13A::CAON//002'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_2 <> '' AND t_EQ_DVOP.CurenCD_2 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_2
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C' and t_EQ_DVOP.DefaultOpt_2='T' 
     THEN ':17B::DFLT//Y'
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
            AND  t_EQ_DVOP.Netdividend_2 <> ''
     THEN ':92F::NETT//'+CurenCD_2+ltrim(cast(t_EQ_DVOP.Netdividend_2 as char(15)))
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
            AND  t_EQ_DVOP.Netdividend_2 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
            AND t_EQ_DVOP.Grossdividend_2 <> ''
     THEN ':92F::GRSS//'+CurenCD_2+ltrim(cast(t_EQ_DVOP.Grossdividend_2 as char(15)))
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
            AND t_EQ_DVOP.Grossdividend_2 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
           AND t_EQ_DVOP.Taxrate_2 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_2 as char(15))
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
           AND t_EQ_DVOP.Taxrate_2 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D' OR t_EQ_DVOP.Divtype_2 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_2 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_2 */
/* CASH OPTION START_3 */
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
     THEN ':13A::CAON//003'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_3 <> '' AND t_EQ_DVOP.CurenCD_3 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_3
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C' and t_EQ_DVOP.DefaultOpt_3='T' 
     THEN ':17B::DFLT//Y'
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
            AND  t_EQ_DVOP.Netdividend_3 <> ''
     THEN ':92F::NETT//'+CurenCD_3+ltrim(cast(t_EQ_DVOP.Netdividend_3 as char(15)))
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
            AND  t_EQ_DVOP.Netdividend_3 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
            AND t_EQ_DVOP.Grossdividend_3 <> ''
     THEN ':92F::GRSS//'+CurenCD_3+ltrim(cast(t_EQ_DVOP.Grossdividend_3 as char(15)))
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
            AND t_EQ_DVOP.Grossdividend_3 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
           AND t_EQ_DVOP.Taxrate_3 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_3 as char(15))
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
           AND t_EQ_DVOP.Taxrate_3 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_3 = 'D' OR t_EQ_DVOP.Divtype_3 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_3 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_3 */
/* CASH OPTION START_4 */
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
     THEN ':13A::CAON//004'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_4 <> '' AND t_EQ_DVOP.CurenCD_4 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_4
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C' and t_EQ_DVOP.DefaultOpt_4='T' 
     THEN ':17B::DFLT//Y'
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
            AND  t_EQ_DVOP.Netdividend_4 <> ''
     THEN ':92F::NETT//'+CurenCD_4+ltrim(cast(t_EQ_DVOP.Netdividend_4 as char(15)))
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
            AND  t_EQ_DVOP.Netdividend_4 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
            AND t_EQ_DVOP.Grossdividend_4 <> ''
     THEN ':92F::GRSS//'+CurenCD_4+ltrim(cast(t_EQ_DVOP.Grossdividend_4 as char(15)))
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
            AND t_EQ_DVOP.Grossdividend_4 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
           AND t_EQ_DVOP.Taxrate_4 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_4 as char(15))
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
           AND t_EQ_DVOP.Taxrate_4 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_4 = 'D' OR t_EQ_DVOP.Divtype_4 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_4 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_4 */
/* CASH OPTION START_5 */
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
     THEN ':13A::CAON//005'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_5 <> '' AND t_EQ_DVOP.CurenCD_5 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_5
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C' and t_EQ_DVOP.DefaultOpt_5='T' 
     THEN ':17B::DFLT//Y'
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
            AND  t_EQ_DVOP.Netdividend_5 <> ''
     THEN ':92F::NETT//'+CurenCD_5+ltrim(cast(t_EQ_DVOP.Netdividend_5 as char(15)))
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
            AND  t_EQ_DVOP.Netdividend_5 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
            AND t_EQ_DVOP.Grossdividend_5 <> ''
     THEN ':92F::GRSS//'+CurenCD_5+ltrim(cast(t_EQ_DVOP.Grossdividend_5 as char(15)))
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
            AND t_EQ_DVOP.Grossdividend_5 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
           AND t_EQ_DVOP.Taxrate_5 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_5 as char(15))
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
           AND t_EQ_DVOP.Taxrate_5 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_5 = 'D' OR t_EQ_DVOP.Divtype_5 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_5 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_5 */
/* CASH OPTION START_6 */
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
     THEN ':13A::CAON//006'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 <> 'C'
     THEN ''
     WHEN t_EQ_DVOP.CurenCD_6 <> '' AND t_EQ_DVOP.CurenCD_6 <> ''
     THEN ':11A::OPTN//' + t_EQ_DVOP.CurenCD_6
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C' and t_EQ_DVOP.DefaultOpt_6='T' 
     THEN ':17B::DFLT//Y'
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
           AND t_EQ_DVOP.Paydate is not null 
              AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
            AND  t_EQ_DVOP.Netdividend_6 <> ''
     THEN ':92F::NETT//'+CurenCD_6+ltrim(cast(t_EQ_DVOP.Netdividend_6 as char(15)))
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
            AND  t_EQ_DVOP.Netdividend_6 is null
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
            AND t_EQ_DVOP.Grossdividend_6 <> ''
     THEN ':92F::GRSS//'+CurenCD_6+ltrim(cast(t_EQ_DVOP.Grossdividend_6 as char(15)))
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
            AND t_EQ_DVOP.Grossdividend_6 is null
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
           AND t_EQ_DVOP.Taxrate_6 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_6 as char(15))
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
           AND t_EQ_DVOP.Taxrate_6 is null
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Actflag_6 = 'D' OR t_EQ_DVOP.Divtype_6 = ''
     THEN ''
     WHEN t_EQ_DVOP.Divtype_6 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_6 */
'' as Notes,
'-}$'
From t_EQ_DVOP
WHERE 
mainTFB1<>''
/* make sure exclusively cash types */
and (DIVTYPE_1 = 'C' or DIVTYPE_1 = '')
and (DIVTYPE_2 = 'C' or DIVTYPE_2 = '')
and (DIVTYPE_3 = 'C' or DIVTYPE_3 = '')
and (DIVTYPE_4 = 'C' or DIVTYPE_4 = '')
and (DIVTYPE_5 = 'C' or DIVTYPE_5 = '')
and (DIVTYPE_6 = 'C' or DIVTYPE_6 = '')
/* and at least 2 are cash types */
and ((divtype_1='C' and divtype_2='C') or (divtype_1='C' and divtype_3='C')
or   (divtype_1='C' and divtype_4='C') or (divtype_1='C' and divtype_5='C')
or   (divtype_1='C' and divtype_6='C') or (divtype_2='C' and divtype_3='C')
or   (divtype_2='C' and divtype_4='C') or (divtype_2='C' and divtype_5='C'))
/* and that one corresponding pair at least are undeleted */
and ((actflag_1<>'D' and actflag_2<>'D') or (actflag_1<>'D' and actflag_3<>'D')
or   (actflag_1<>'D' and actflag_4<>'D') or (actflag_1<>'D' and actflag_5<>'D')
or   (actflag_1<>'D' and actflag_6<>'D') or (actflag_2<>'D' and actflag_3<>'D')
or   (actflag_2<>'D' and actflag_4<>'D') or (actflag_2<>'D' and actflag_5<>'D'))

and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3