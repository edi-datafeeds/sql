--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--SPLF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_DECR_CAPRD.Changed,
'select  CAPRDNotes As Notes FROM CAPRD WHERE CaprdID = '+ cast(t_EQ_DECR_CAPRD.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'122'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_DECR_CAPRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLF',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_DECR_CAPRD.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_DECR_CAPRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_DECR_CAPRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_DECR_CAPRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DECR_CAPRD.Recdate <>'' AND t_EQ_DECR_CAPRD.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_DECR_CAPRD.PayDate <>'' and t_EQ_DECR_CAPRD.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN t_EQ_DECR_CAPRD.OldParValue <> '' or t_EQ_DECR_CAPRD.NewParValue <> ''
     THEN ':70E::TXNR//'
     ELSE '' END,
CASE WHEN t_EQ_DECR_CAPRD.OldParValue <> ''
     THEN 'Old Par Value ' + t_EQ_DECR_CAPRD.OldParValue
     ELSE ''
     END, 
CASE WHEN t_EQ_DECR_CAPRD.NewParValue <> ''
     THEN 'New Par Value ' + t_EQ_DECR_CAPRD.NewParValue
     ELSE ''
     END, 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN t_EQ_DECR_CAPRD.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_DECR_CAPRD.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_DECR_CAPRD.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_DECR_CAPRD.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_DECR_CAPRD.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_DECR_CAPRD.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newTFB1<>''
     THEN ':35B:' + newTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_DECR_CAPRD.NewRatio <>''
          AND t_EQ_DECR_CAPRD.OldRatio <>''
     THEN ':92D::ADEX//'+ltrim(cast(t_EQ_DECR_CAPRD.NewRatio as char (15)))+
                    '/'+ltrim(cast(t_EQ_DECR_CAPRD.OldRatio as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_DECR_CAPRD
WHERE 
mainTFB1<>''
and OldRatio<>''
and NewRatio<>''
and cast(rtrim(OldRatio) as float(15,7))<
      cast(rtrim(NewRatio) as float(15,7))
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3