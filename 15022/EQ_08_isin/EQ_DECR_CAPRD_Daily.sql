--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--DECR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_DECR_CAPRD.Changed,
'select  CapRdNotes As Notes FROM CAPRD WHERE CAPRDID = '+ cast(t_EQ_DECR_CAPRD.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'148'+t_EQ_DECR_CAPRD.EXCHGID+cast(t_EQ_DECR_CAPRD.Seqnum as char(1)) as CAref1,
t_EQ_DECR_CAPRD.EventID as CAREF2,
t_EQ_DECR_CAPRD.SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_DECR_CAPRD.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DECR_CAPRD.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_DECR_CAPRD.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_DECR_CAPRD.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_DECR_CAPRD.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_DECR_CAPRD.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DECR_CAPRD.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_DECR_CAPRD.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.EffectiveDate,112)
     ELSE ':98A::EFFD//19000103'
     END,
CASE WHEN t_EQ_DECR_CAPRD.PayDate <>'' and t_EQ_DECR_CAPRD.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DECR_CAPRD.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN t_EQ_DECR_CAPRD.OldParValue <> '' or t_EQ_DECR_CAPRD.NewParValue <> ''
     THEN ':70E::TXNR//'
     ELSE '' END,
CASE WHEN t_EQ_DECR_CAPRD.OldParValue <> ''
     THEN 'Old Par Value ' +t_EQ_DECR_CAPRD.OldParValue
     ELSE ''
     END, 
CASE WHEN t_EQ_DECR_CAPRD.NewParValue <> ''
     THEN 'New Par Value ' +t_EQ_DECR_CAPRD.NewParValue
     ELSE ''
     END, 
':16S:CADETL',
CASE WHEN t_EQ_DECR_CAPRD.newTFB1<>''
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN t_EQ_DECR_CAPRD.newTFB1<>''
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN t_EQ_DECR_CAPRD.newTFB1<>''
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN t_EQ_DECR_CAPRD.newTFB1<>''
     THEN ':17B::DFLT//Y'
     ELSE ''
     END,
CASE WHEN newTFB1<>''
     THEN ':35B:' + newTFB1
     ELSE ''
     END,
CASE WHEN t_EQ_DECR_CAPRD.newTFB1<>''
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
'' as Notes,
'-}$'
From t_EQ_DECR_CAPRD
LEFT OUTER JOIN wca.dbo.RCAP on t_EQ_DECR_CAPRD.SecID = wca.dbo.RCAP.SecID
                    and t_EQ_DECR_CAPRD.EffectiveDate = wca.dbo.RCAP.EffectiveDate
WHERE 
mainTFB1<>''
and wca.dbo.RCAP.Secid is null
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3