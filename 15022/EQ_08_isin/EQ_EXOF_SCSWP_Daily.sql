--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--EXOF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_EXOF_SCSWP.Changed,
'select  ScswpNotes As Notes FROM SCSWP WHERE RdID = '+ cast(t_EQ_EXOF_SCSWP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'164'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_EXOF_SCSWP.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_EXOF_SCSWP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_EXOF_SCSWP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_EXOF_SCSWP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_EXOF_SCSWP.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_EXOF_SCSWP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_EXOF_SCSWP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_EXOF_SCSWP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':69J::PWAL//UKWN'
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_EXOF_SCSWP.newratio <>''
          AND t_EQ_EXOF_SCSWP.oldratio <>''
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_EXOF_SCSWP.newratio as char (15)))+
                    '/'+rtrim(cast(t_EQ_EXOF_SCSWP.oldratio as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN t_EQ_EXOF_SCSWP.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXOF_SCSWP.Paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_EXOF_SCSWP
WHERE 
mainTFB1<>''
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3