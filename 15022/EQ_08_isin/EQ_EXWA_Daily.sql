--FilePath=o:\DataFeed\15022\EQ_08_isin\
--FilePrefix=
--FileName=YYYYMMDD_EQ15022
--EXWA
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\EQ_08_isin\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_EXWA.Changed,
'select  WartmNotes As Notes FROM WARTM WHERE SECID = '+ cast(t_EQ_EXWA.SecID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'300'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_EXWA.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_EXWA.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_EXWA.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_EXWA.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXWA',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(t_EQ_EXWA.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_EXWA.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
CASE WHEN len(mainTFB1)>12 and sedol <>''
     THEN '/GB/' + Sedol
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_EXWA.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_EXWA.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_EXWA.ExpirationDate <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , t_EQ_EXWA.ExpirationDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN t_EQ_EXWA.ExpirationDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_EXWA.ExpirationDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_EXWA.ToDate <>''
       AND t_EQ_EXWA.FromDate <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_EXWA.FromDate,112)+'/'
          +CONVERT ( varchar , t_EQ_EXWA.ToDate,112)
     WHEN t_EQ_EXWA.FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_EXWA.FromDate,112)+'/UKWN'
     WHEN t_EQ_EXWA.ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_EXWA.ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_EXWA.PricePerShare <> ''
     THEN ':90B::MRKT//ACTU/'+CurenCD+rtrim(cast(t_EQ_EXWA.PricePerShare as char(15)))
     ELSE ':90E::MRKT//UKWN'
     END as COMMASUB,
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN t_EQ_EXWA.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_EXWA.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_EXWA.StrikePrice <> ''
     THEN ':90B::EXER//ACTU/'+CurenCD+rtrim(cast(t_EQ_EXWA.StrikePrice as char(15)))
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN exerTFB1<>''
     THEN ':35B:' + exerTFB1
     ELSE ':35B:' + mainTFB1
     END,
CASE WHEN t_EQ_EXWA.RatioNew <>''
          AND t_EQ_EXWA.RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_EXWA.RatioNew as char (15)))+
                    '/'+rtrim(cast(t_EQ_EXWA.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
/* CASE WHEN t_EQ_EXWA.Paydate <>'' 
               AND t_EQ_EXWA.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXWA.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END, */
':98B::PAYD//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_EXWA
WHERE 
mainTFB1<>''
and EventID is not null
and (ListStatus<>'D' or SCEXHActtime>changed-28)
order by caref2 desc, caref1, caref3