--SEMETABLE=client.dbo.seme_smithwill
--FileName=edi_YYYYMMDD
--EXOF_CTX_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=30000
--TEXTCLEAN=2
--ROWTERMINATOR=UX

--# 1
use wca
select distinct
Changed,
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01SAMPLEAAXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'127'+EXCHGID as CAref1,
/* CASE WHEN maintab.Enddate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Enddate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '127' END as CAref1, */
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(Localcode,'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
CASE WHEN EndDate <>'' AND EndDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndDate <>'' AND EndDate IS NOT NULL
       AND StartDate <>'' AND StartDate IS NOT NULL
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/'
          +CONVERT ( varchar , EndDate,112)
     WHEN StartDate <>'' AND StartDate IS NOT NULL
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartDate,112)+'/UKWN'
     WHEN EndDate <>'' AND EndDate IS NOT NULL
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN isnull(resisin,'')<>'' THEN ':35B:ISIN ' + resisin
     else '' end,
case when isnull(resisin,'')='' then ':35B:/US/999999999' else '' end,
case when isnull(resisin,'')='' then 'NOT AVAILABLE AT PRESENT' else '' end,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':92K::NEWO//UKWN',
CASE WHEN StartDate <>'' AND StartDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , StartDate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as RawNotes, 
'-}$'
From v_EQ_EXOF_CTX as maintab
WHERE
mainTFB1<>''
and (maintab.exchgcd=maintab.primaryexchgcd or maintab.primaryexchgcd='')
and (((maintab.isin in (select code from client.dbo.pfisin where accid=121 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=1000 and actflag='I'))
and Enddate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=121 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=1000 and actflag='U'))
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))  
and upper(eventtype)<>'CLEAN'
order by caref2 desc, caref1, caref3
