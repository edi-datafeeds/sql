--SEMETABLE=client.dbo.seme_smithwill
--FileName=edi_YYYYMMDD
--SOFF_DIST_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\
--FileTidy=N
--sEvent=DIST
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=30000
--TEXTCLEAN=2
--ROWTERMINATOR=UX

--# 1
use wca
select distinct
'' AS BBGCNTRY,
'' as BBGEXCHG,
'' as BBGCUREN,
Changed,
'select  DistNotes As Notes FROM DIST WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
maintab.paydate as PAYLINK,
case when 1=1 then (select '{1:F01SAMPLEAAXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'136' as CAref1,
maintab.EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SOFF' as CAEV,
case when (select count(distinct subpay.optionid) from wca.dbo.MPAY as subpay
           where subpay.eventid=maintab.eventid and sevent='DIST' 
           and subpay.actflag<>'C' and subpay.actflag<>'D' and subpay.paytype<>'D')>1 then ':22F::CAMV//CHOS' else ':22F::CAMV//MAND' end as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <> '' and  ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.MPAY.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
':16S:CADETL',
':16R:CAOPTN' as StartSeqE,
'' as RawNotes, 
'-}$'
From v_EQ_SOFF_DIST as maintab
left outer join wca.dbo.MPAY on maintab.eventid = wca.dbo.MPAY.eventid and 'DIST' = wca.dbo.MPAY.sevent
                                             and 'D'=wca.dbo.MPAY.paytype and 'D'<>wca.dbo.MPAY.actflag
WHERE
mainTFB1<>''
and (maintab.exchgcd=maintab.primaryexchgcd or maintab.primaryexchgcd='')
and (((maintab.isin in (select code from client.dbo.pfisin where accid=121 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=1000 and actflag='I'))
and exdate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=121 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=1000 and actflag='U'))
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))  
order by caref2 desc, caref1, caref3