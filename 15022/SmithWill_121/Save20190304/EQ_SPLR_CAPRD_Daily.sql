--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--SPLR_CAPRD_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\newedge_268\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select  CAPRDNotes As Notes FROM CAPRD WHERE CaprdID = '+ cast(EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01SAMPLEAAXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.effectivedate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.effectivedate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '123' END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Effectivedate <>'' AND Effectivedate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Effectivedate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN (cntrycd='HK' or cntrycd='SG' or cntrycd='TH' or cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newTFB1<>''
     THEN ':35B:' + newTFB1
     ELSE ':35B:' + mainTFB1
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN NewRatio <>''
            AND OldRatio <>''
     THEN ':92D::NEWO//'+ltrim(cast(NewRatio as char (15)))+
                    '/'+ltrim(cast(OldRatio as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
CASE WHEN PayDate <>'' and PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as RawNotes, 
'-}$'
From v_EQ_DECR_CAPRD as maintab
left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'SPLR'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=995 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=1000 and actflag='I'))

and effectivedate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=995 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=1000 and actflag='U'))
and changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and exchgcd='GBLSE'
and OldRatio<>''
and NewRatio<>''
and cast(rtrim(OldRatio) as float(15,7))>
      cast(rtrim(NewRatio) as float(15,7))
order by caref2 desc, caref1, caref3