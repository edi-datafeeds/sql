--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_CONV
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(ConvNotes as varchar(24)))<30 then rtrim(char(32)) else ConvNotes end As Notes FROM CONV WHERE ConvID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01STIFELXXXXXO0300000054}{2:I564EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Todate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Todate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '211' END as CAref1, */
'150' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//EXOF' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN cast(parvalue as float)>1 and parvalue like '%.00000%' 
     THEN ':36B::MINO//FAMT/'+rtrim(replace(ParValue,'.00000','.'))
     WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue
     ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
/*':70E::OFFO//'+substring(Issuername,1,35) as TIDYTEXT,*/
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
CASE WHEN ToDate is not null and ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN ToDate is not null and ToDate <>''
       AND FromDate is not null and FromDate <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/'
          +CONVERT ( varchar , ToDate,112)
     WHEN FromDate is not null and FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/UKWN'
     WHEN ToDate is not null and ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1 <>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:/US/999999999'
     END,
CASE WHEN resTFB1 =''
     THEN 'NOT AVAILABLE AT PRESENT'
     ELSE ''
     END,
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN Price <>''
     THEN ':90B::PRPP//ACTU/'+CurenCD+substring(Price,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
CASE WHEN RatioNew <>''
          AND RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(RatioNew as char (15)))+
                    '/'+rtrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN SettlementDate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , SettlementDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//999',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN ToDate is not null and ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16S:CAOPTN',
'' as Notes70F,
'-}$'
From v_FI_CONV as maintab
/* left outer join client.dbo.seme_sample as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'EXOF'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and ((maintab.isin in (select code from client.dbo.pfisin where accid=997 and actflag='I')
      and ToDate > getdate())
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=997 and actflag='I')
and maintab.changed>'2019/01/01'))
and convtype='BB'
and Actflag  <>''
order by caref2 desc, caref1, caref3
