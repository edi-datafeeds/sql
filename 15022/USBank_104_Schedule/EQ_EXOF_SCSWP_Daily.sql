--SEMETABLE=client.dbo.seme_usbank
--FileName=edi_YYYYMMDD
--EXOF_SCSWP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7800
--MESSTERMINATOR=-}
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
'select ScswpNotes As Notes FROM SCSWP WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01USBKUS4TAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Exdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Exdate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '198' END as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, announcedate,112) ELSE '' END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , Exdate, 112)
     ELSE ':98B::EFFD//UKWN'
     END as DateEx,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN maintab.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN maintab.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN maintab.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN maintab.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN maintab.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN maintab.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':69J::PWAL//UKWN',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>'UKWN' and resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN newratio <>'' AND oldratio <>''
     THEN ':92D::NEWO//'+rtrim(cast(newratio as char (15)))+
                    '/'+rtrim(cast(oldratio as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN Paydate<>'' and Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , Paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as WholeWordNotes, 
'-}'
From v_EQ_EXOF_SCSWP as maintab
left outer join client.dbo.seme_usbank as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'EXOF'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=104 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=104 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=104 and actflag='I'))
and exdate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=104 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=104 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=104 and actflag='U'))
and changed>(select max(acttime_new)-0.05 from wca.dbo.tbl_opslog)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and maintab.changed>(select max(acttime_new)-0.05 from wca.dbo.tbl_opslog)
order by caref2 desc, caref1, caref3
