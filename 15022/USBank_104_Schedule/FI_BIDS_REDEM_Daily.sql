--SEMETABLE=client.dbo.seme_usbank
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_REDM
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7800
--MESSTERMINATOR=-}
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then rtrim(char(32)) else RedemNotes end As Notes FROM REDEM WHERE RedemID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01USBKUS4TAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.Redemdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Redemdate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '201' END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//BIDS' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
 ':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
/*':70E::OFFO//'+substring(Issuername,1,35) as TIDYTEXT,*/
CASE WHEN RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//N',
CASE WHEN TenderCloseDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN TenderOpenDate<>'' AND TenderCloseDate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , TenderOpenDate,112)+'/'
          +CONVERT ( varchar , TenderCloseDate,112)
     WHEN TenderOpenDate<>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , TenderOpenDate,112)+'/UKWN'
     WHEN TenderCloseDate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , TenderCloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar , RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
case when actuvalue is not null then ':90B::OFFR//ACTU/'+curencd else '' end as NOCRLF1, 
case when actuvalue is null or actuvalue ='' then ':90E::OFFR//UKWN' else '' end,
case when actuvalue is not null then cast(actuvalue as varchar) else '' end as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//NOAC',
CASE WHEN CurenCD <>''
     THEN ':11A::OPTN//' + CurenCD ELSE ''
     END,
':17B::DFLT//Y',
CASE WHEN TenderCloseDate <>''
     THEN ':98A::MKDT//' + CONVERT(varchar , TenderCloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16S:CAOPTN',
'' as WholeWordNotes,  
'-}'
From v_FI_REDM as maintab
left outer join client.dbo.seme_usbank as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'BIDS'=SR.CAEVMAIN
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=104 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=104 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=104 and actflag='I'))
and TenderCloseDate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=104 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=104 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=104 and actflag='U'))
and changed>(select max(acttime_new)-0.05 from wca.dbo.tbl_opslog)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and (redemtype='BB')
and DutchAuction<>'T'
order by caref2 desc, caref1, caref3
