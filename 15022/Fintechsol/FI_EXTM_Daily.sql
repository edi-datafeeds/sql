--SEMETABLE=client.dbo.seme_fintechsol
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_EXTM
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select
changed,
'select case when len(cast(Notes as varchar(24)))=22 then rtrim(char(32)) else Notes end As Notes FROM MTCHG WHERE MtchgID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01BICSAM22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '415'+EXCHGID+cast(Seqnum as char(1))
     ELSE '415'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXTM' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB2 as TFB,
case when substring(mainTFB2,1,4)='/GB/' and isin<>'' then 'ISIN ' + isin else '' end,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else '' 
     end,
case when len(Issuername)<36
     then Issuername
     else substring(Issuername,24,35)
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end,
substring(bonddesc3,1,35),
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' or MCD='SECM' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD<>'' and PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD and ExchgCD<>'' THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when issuedate<>'' then ':98A::ISSU//'+CONVERT(varchar, issuedate,112) else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN NewMaturityDate is not null and NewMaturityDate <>''
     THEN ':98A::MATU//'+CONVERT ( varchar , NewMaturityDate,112)
     ELSE ':98A::MATU//UKWN'
     END,
':16S:CADETL',
'' as Notes, 
'-}$'
From v_FI_EXTM as maintab
WHERE
changed>(select max(acttime)-0.05 as maxdate from wca.dbo.tbl_Opslog where seq = 2)
and mainTFB2<>''
order by caref2 desc, caref1, caref3
