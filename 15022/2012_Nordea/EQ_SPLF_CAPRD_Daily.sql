--SEMETABLE=client.dbo.seme_nordea
--FileName=YYYYMMDD_MT564
--SPLF_CAPRD_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_nordea\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=

--# 1
use wca
select distinct
Changed,
'{1:F01NDEADKK2CXXX1111111111}{2:O5641111111111XEDIGB2LCXXX11111111111111111111N}{4:' as ISOHDR,
':16R:GENL',
'122'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLF',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP,
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Recdate <>'' AND Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN newTFB2<>'UKWN' and newTFB2<>'UKWN' and newTFB2<>''
     THEN ':35B:' + newTFB2
     ELSE ':35B:' + mainTFB2
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN NewRatio <>''
          AND OldRatio <>''
     THEN ':92D::NEWO//'+ltrim(cast(NewRatio as char (15)))+
                    '/'+ltrim(cast(OldRatio as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN PayDate <>'' and PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_EQ_DECR_CAPRD
WHERE 
((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
     and sedol in (select code from portfolio.dbo.fsedol where accid = 170 and actflag='U'))
     OR 
    (Effectivedate>getdate()-8
     and sedol in (select code from portfolio.dbo.fsedol where accid = 170 and actflag='I')))
and mainTFB2<>''
and (mcd<>'' and mcd is not null)
and Effectivedate is not null
and CalcListdate<=Effectivedate
and CalcDelistdate>=Effectivedate
and OldRatio<>''
and NewRatio<>''
and cast(rtrim(OldRatio) as float(15,7))<
      cast(rtrim(NewRatio) as float(15,7))
order by caref2 desc, caref1, caref3