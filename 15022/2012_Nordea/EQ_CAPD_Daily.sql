--SEMETABLE=client.dbo.seme_nordea
--FileName=YYYYMMDD_MT564
--DV_CA_SE_OP_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.TXT
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\2012_nordea\
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=



--# 1
use wca
select    
Changed,
'{1:F01NDEADKK2CXXX1111111111}{2:O5641111111111XEDIGB2LCXXX11111111111111111111N}{4:' as ISOHDR,
'select DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( EventID as char(16)) as ChkNotes,
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
cntrycd as MPAYLINK3,
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
':23G:NEWM' AS MessageStatus,
':22F::CAEP//DISN',
':22F::CAEV//CAPD',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
CASE WHEN cfi <> '' THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN exdate<>'' and Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN divopt1.CurenCD <> '' AND divopt1.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + divopt1.CurenCD
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN  Paydate <>'' AND  Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN  divopt1.Netdividend <> '' AND divopt1.Netdividend IS NOT NULL
     THEN ':92F::NETT//'+ divopt1.CurenCD+ltrim(cast(divopt1.Netdividend as char(15)))
     WHEN  divopt1.Grossdividend <> '' AND  divopt1.Grossdividend IS NOT NULL
     THEN ''
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN  divopt1.Grossdividend <> ''
             AND  divopt1.Grossdividend IS NOT NULL
     THEN ':92F::GRSS//'+ divopt1.CurenCD+ltrim(cast(divopt1.Grossdividend as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From wca.dbo.v_EQ_DV_CA_SE_OP
left outer join wca.dbo.divpy as divopt1 on wca.dbo.v_EQ_DV_CA_SE_OP.eventid = divopt1.divid and 1=divopt1.optionid and 'D'<>divopt1.actflag
left outer join wca.dbo.divpy as divopt2 on wca.dbo.v_EQ_DV_CA_SE_OP.eventid = divopt2.divid and 2=divopt2.optionid and 'D'<>divopt2.actflag
left outer join wca.dbo.divpy as divopt3 on wca.dbo.v_EQ_DV_CA_SE_OP.eventid = divopt3.divid and 3=divopt3.optionid and 'D'<>divopt3.actflag
WHERE 
((changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
     and sedol in (select code from portfolio.dbo.fsedol where accid = 170 and actflag='U'))
     OR 
    (Exdate>getdate()-8
     and sedol in (select code from portfolio.dbo.fsedol where accid = 170 and actflag='I')))
and mainTFB2<>''
and (mcd<>'' and mcd is not null)
and divopt1.divid is not null 
and divopt2.divid is null 
and divopt3.divid is null
and wca.dbo.v_EQ_DV_CA_SE_OP.divperiodcd='ISC'
and Exdate is not null
and CalcListdate<=Exdate
and CalcDelistdate>=Exdate
order by caref2 desc, caref1, caref3
