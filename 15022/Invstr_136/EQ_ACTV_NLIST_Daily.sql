--SEMETABLE=client.dbo.seme_invstr
--FileName=edi_YYYYMMDD
--ACTV_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\
--FileTidy=n
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=10000
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
Changed,
case when 1=1 then (select '{1:F01INVSTR3BAXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'168' as CAref1,
'150' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ACTV' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':98B::ANOU//UKWN',
CASE WHEN EffectiveDate <>'' and effectivedate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From v_EQ_ACTV_NLIST as maintab
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=134 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=134 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=134 and actflag='I'))
and EffectiveDate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=134 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=134 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=134 and actflag='U'))
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)))
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by caref2 desc, caref1, caref3
