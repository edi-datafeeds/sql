--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_BONU
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_BONU.Changed,
'select  BonNotes As Notes FROM BON WHERE RDID = '+ cast(t_EQ_BONU.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'114'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN t_EQ_BONU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_BONU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_BONU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_BONU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BONU',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_BONU.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_BONU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_BONU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_BONU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_BONU.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_BONU.Exdate,112)
     ELSE ':98A::XDTE//UKWN'
     END as DateEx,
CASE WHEN t_EQ_BONU.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_BONU.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END as DateRec,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN t_EQ_BONU.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_BONU.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_BONU.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_BONU.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_BONU.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_BONU.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB2<>''
     THEN ':35B:' + resTFB2
     ELSE ':35B:' + mainTFB2
     END,
CASE WHEN t_EQ_BONU.RatioNew <>''
          AND t_EQ_BONU.RatioOld <>''
     THEN ':92D::ADEX//'+ltrim(cast(t_EQ_BONU.RatioNew as char (15)))+
                    '/'+ltrim(cast(t_EQ_BONU.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN t_EQ_BONU.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_BONU.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_BONU
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3