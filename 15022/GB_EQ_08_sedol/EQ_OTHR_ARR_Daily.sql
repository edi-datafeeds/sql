--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_OTHR
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_OTHR_ARR.Changed,
'select  ArrNotes As Notes FROM Arr WHERE RDID = '+ cast(t_EQ_OTHR_ARR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'112'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_OTHR_ARR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_OTHR_ARR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_OTHR_ARR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_OTHR_ARR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_OTHR_ARR.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_OTHR_ARR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_OTHR_ARR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_OTHR_ARR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_OTHR_ARR.ExDate <>'' and t_EQ_OTHR_ARR.ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_OTHR_ARR.ExDate,112)
     ELSE ':98A::XDTE//UKWN'
     END,
CASE WHEN t_EQ_OTHR_ARR.PayDate <>'' and t_EQ_OTHR_ARR.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_OTHR_ARR.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN t_EQ_OTHR_ARR.RecDate <>'' and t_EQ_OTHR_ARR.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_OTHR_ARR.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
'' as Notes,
'-}$'
From t_EQ_OTHR_ARR
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3