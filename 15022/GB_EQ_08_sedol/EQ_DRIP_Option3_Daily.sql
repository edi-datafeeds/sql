--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DRIP
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_DVOP.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast( t_EQ_DVOP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'103'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN  t_EQ_DVOP.Actflag_3 = 'D'
     THEN ':23G:CANC'
     WHEN  t_EQ_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN  t_EQ_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN  t_EQ_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN  t_EQ_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DRIP',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring( t_EQ_DVOP.Issuername,1,35) as TIDYTEXT,
substring( t_EQ_DVOP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN  t_EQ_DVOP.MCD <>''
     THEN ':94B::PLIS//EXCH/' +  t_EQ_DVOP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN  t_EQ_DVOP.Exdate <>'' AND  t_EQ_DVOP.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar ,  t_EQ_DVOP.Exdate,112)
     ELSE ':98A::XDTE//UKWN'
     END,
CASE WHEN  t_EQ_DVOP.Recdate <>'' AND  t_EQ_DVOP.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar ,  t_EQ_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN t_EQ_DVOP.DivPeriodCD='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN t_EQ_DVOP.DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     WHEN t_EQ_DVOP.DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN t_EQ_DVOP.Frequency='ANL'
     THEN ':22F::DIVI//FINL'
     WHEN t_EQ_DVOP.Frequency='SMA'
     THEN ':22F::DIVI//REGR'
     WHEN t_EQ_DVOP.Frequency='TRM'
     THEN ':22F::DIVI//REGR'
     WHEN t_EQ_DVOP.Frequency='QTR'
     THEN ':22F::DIVI//REGR'
     WHEN t_EQ_DVOP.Frequency='MNT'
     THEN ':22F::DIVI//REGR'
     WHEN t_EQ_DVOP.Frequency='WKL'
     THEN ':22F::DIVI//REGR'
     WHEN t_EQ_DVOP.Frequency='DLY'
     THEN ':22F::DIVI//REGR'
     ELSE ':22F::DIVI//INTE'
     END,
/* START In case of Franking */
CASE WHEN  t_EQ_DVOP.UnFrankDiv <> ''
                 AND  t_EQ_DVOP.UnFrankDiv IS NOT NULL
                    AND  t_EQ_DVOP.CurenCD_3 IS NOT NULL
     THEN ':92J::GRSS//UNFR/'+ t_EQ_DVOP.CurenCD_3+rtrim(cast( t_EQ_DVOP.UnFrankDiv as char(15)))
     ELSE ''
     END as COMMASUB,
CASE WHEN  t_EQ_DVOP.FrankDiv <> ''
             AND  t_EQ_DVOP.FrankDiv IS NOT NULL
                    AND  t_EQ_DVOP.CurenCD_3 IS NOT NULL
     THEN ':92J::GRSS//FLFR/'+ t_EQ_DVOP.CurenCD_3+rtrim(cast( t_EQ_DVOP.FrankDiv as char(15)))
     ELSE ''
     END AS COMMASUB,
/* END In case of Franking */
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast( t_EQ_DVOP.RdID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN  t_EQ_DVOP.CurenCD_3 <> ''
     THEN ':11A::OPTN//' +  t_EQ_DVOP.CurenCD_3
     END,
':17B::DFLT//Y',
CASE WHEN  t_EQ_DVOP.DripLastDate <>'' AND  t_EQ_DVOP.DripLastDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar ,  t_EQ_DVOP.DripLastDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN  t_EQ_DVOP.Paydate <>'' 
             AND  t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  t_EQ_DVOP.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN  t_EQ_DVOP.Netdividend_3 <> ''
                 AND  t_EQ_DVOP.Netdividend_3 IS NOT NULL
     THEN ':92F::NETT//'+ t_EQ_DVOP.CurenCD_3+ltrim(cast( t_EQ_DVOP.Netdividend_3 as char(15)))
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN  t_EQ_DVOP.Grossdividend_3 <> ''
             AND  t_EQ_DVOP.Grossdividend_3 IS NOT NULL
     THEN ':92F::GRSS//'+ t_EQ_DVOP.CurenCD_3+ltrim(cast( t_EQ_DVOP.Grossdividend_3 as char(15)))
     ELSE ':92K::GRSS//UKWN' 
     END as COMMASUB,
CASE WHEN  t_EQ_DVOP.Taxrate_3 <> ''
     THEN ':92A::TAXR//'+cast( t_EQ_DVOP.Taxrate_3 as char(15))
     ELSE ':92K::TAXR//UKWN' 
     END as COMMASUB,
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SECU',
':17B::DFLT//N',
CASE WHEN  t_EQ_DVOP.Fractions_3 = 'C'
     THEN ':22F::DISF//CINL'
     WHEN  t_EQ_DVOP.Fractions_3 = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN  t_EQ_DVOP.Fractions_3 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN  t_EQ_DVOP.Fractions_3 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN  t_EQ_DVOP.Fractions_3 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN  t_EQ_DVOP.Fractions_3 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN  t_EQ_DVOP.DripReinvPrice <>'' 
             AND  t_EQ_DVOP.DripReinvPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+ t_EQ_DVOP.CurenCD_3+ltrim(cast( t_EQ_DVOP.DripReinvPrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB2,
CASE WHEN  t_EQ_DVOP.DripPaydate <>'' AND  t_EQ_DVOP.DripPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar ,  t_EQ_DVOP.DripPaydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From  t_EQ_DVOP
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and Actflag_3 <> ''
and Divtype_3 = 'C'
and DripCntryCD <> ''
and substring(Curencd_3,1,2)=Dripcntrycd
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3