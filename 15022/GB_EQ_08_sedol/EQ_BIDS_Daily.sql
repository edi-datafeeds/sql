--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_BIDS
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_BIDS.Changed,
'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(t_EQ_BIDS.EventID as char(16)) as ChkNotes,
'' as ISOHDR,
':16R:GENL',
'115'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_BIDS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_BIDS.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN t_EQ_BIDS.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_BIDS.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_BIDS.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_BIDS.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_BIDS.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_BIDS.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_BIDS.StartDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_BIDS.StartDate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
CASE WHEN t_EQ_BIDS.EndDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_BIDS.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_BIDS.EndDate <>''
       AND t_EQ_BIDS.StartDate <>'' 
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_BIDS.StartDate,112)+'/'
          +CONVERT ( varchar , t_EQ_BIDS.EndDate,112)
     WHEN t_EQ_BIDS.StartDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_BIDS.StartDate,112)+'/UKWN'
     WHEN t_EQ_BIDS.EndDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_BIDS.EndDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_BIDS.MaxQlyQty <> ''
     THEN ':36B::QTSO//'+substring(t_EQ_BIDS.MaxQlyQty,1,15) + ','
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN t_EQ_BIDS.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_BIDS.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_BIDS.paydate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_BIDS.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN t_EQ_BIDS.MaxPrice <> ''
     THEN ':90B::OFFR//ACTU/'+t_EQ_BIDS.CurenCD+
          +substring(t_EQ_BIDS.MaxPrice,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_BIDS
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and Actflag <> ''
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3
