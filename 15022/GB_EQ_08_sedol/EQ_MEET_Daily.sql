--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_MEET
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_MEET.Changed,
'' as ISOHDR,
':16R:GENL',
'161'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_MEET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_MEET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_MEET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_MEET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN t_EQ_MEET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN t_EQ_MEET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN t_EQ_MEET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN t_EQ_MEET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN t_EQ_MEET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_MEET.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_MEET.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_MEET.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_MEET.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_MEET.AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , t_EQ_MEET.AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//' + substring(rtrim(t_EQ_MEET.Add1),1,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Add1),36,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Add2),1,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Add2),36,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Add3),1,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Add4),1,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Add5),1,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Add6),1,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.City),1,35) as TIDYTEXT,
substring(rtrim(t_EQ_MEET.Country),1,35) as TIDYTEXT,
':16S:ADDINFO',
'-}$'
From t_EQ_MEET
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
and (t_EQ_MEET.AGMEGM = 'AG'
or t_EQ_MEET.AGMEGM = 'CG'
or t_EQ_MEET.AGMEGM = 'EG'
or t_EQ_MEET.AGMEGM = 'GM'
or t_EQ_MEET.AGMEGM = 'SG')
order by caref2 desc, caref1, caref3