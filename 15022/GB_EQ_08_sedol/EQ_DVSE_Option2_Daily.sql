--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_DVSE
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_DVOP.Changed,
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(t_EQ_DVOP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_DVOP.Actflag_2 = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVSE',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_DVOP.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_DVOP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_DVOP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_DVOP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_DVOP.Exdate <>'' AND t_EQ_DVOP.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_DVOP.Exdate,112)
     ELSE ':98A::XDTE//UKWN'
     END,
CASE WHEN t_EQ_DVOP.Recdate <>'' AND t_EQ_DVOP.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN t_EQ_DVOP.Taxrate_2 <> ''
     THEN ':92A::TAXR//'+cast(t_EQ_DVOP.Taxrate_2 as char(15))
     ELSE ':92K::TAXR//UKWN' 
     END as COMMASUB,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_DVOP.RdID as char(16)) as Notes, */*/ 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN t_EQ_DVOP.Fractions_2 = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN t_EQ_DVOP.Fractions_2 = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN t_EQ_DVOP.Fractions_2 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN t_EQ_DVOP.Fractions_2 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN t_EQ_DVOP.Fractions_2 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN t_EQ_DVOP.Fractions_2 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB2_2 <> ''
           THEN ':35B:' + resTFB2_2
           ELSE ':35B:' + mainTFB2
           END,
CASE WHEN t_EQ_DVOP.RatioNew_2 <>''
            AND t_EQ_DVOP.RatioNew_2 IS NOT NULL
            AND t_EQ_DVOP.RatioOld_2 <>''
            AND t_EQ_DVOP.RatioOld_2 IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(t_EQ_DVOP.RatioNew_2 as char (15)))+
                    '/'+ltrim(cast(t_EQ_DVOP.RatioOld_2 as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN t_EQ_DVOP.Paydate <>'' 
               AND t_EQ_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_DVOP.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_DVOP
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and DIVType_2 = 'S'
AND (OpID_1 = '' or Actflag_1='C' or Actflag_1='D')
AND (OpID_3 = '' or Actflag_3='C' or Actflag_3='D')
AND OpID_4 = ''
AND OpID_5 = ''
AND OpID_6 = ''
and not ((actflag_2='D' or actflag_2='C') and (actflag_1='D' or actflag_1='C'))
and not ((actflag_2='D' or actflag_2='C') and (actflag_3='D' or actflag_3='C'))
and (ListStatus<>'D' or SCEXHActtime>changed-2) 
order by caref2 desc, caref1, caref3