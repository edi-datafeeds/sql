--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_REDO
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_REDO.Changed,
'select  CurrdNotes As Notes FROM CURRD WHERE CurrdID = '+ cast(t_EQ_REDO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'162'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_REDO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN t_EQ_REDO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_REDO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_REDO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_REDO.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_REDO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + t_EQ_REDO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_REDO.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_REDO.EffectiveDate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:' + mainTFB2,
':16R:FIA',
CASE WHEN t_EQ_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + t_EQ_REDO.MCD
     END,
':11A::DENO//'+t_EQ_REDO.NewCurenCD,
':16S:FIA',
/*CASE WHEN t_EQ_REDO.oldparvalue  <>'' and t_EQ_REDO.newparvalue <>''
     and cast(t_EQ_REDO.oldparvalue as float(11,5))>0 and cast(t_EQ_REDO.newparvalue as float(11,5))>0
     THEN ':92D::NEWO//1,/'+cast(cast(t_EQ_REDO.oldparvalue as float(11,5))/cast(t_EQ_REDO.newparvalue as float(11,5)) as varchar(30))
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,*/
CASE WHEN t_EQ_REDO.newparvalue <>''
            AND t_EQ_REDO.oldparvalue <>''
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_REDO.newparvalue as char (15)))+
                    '/'+rtrim(cast(t_EQ_REDO.oldparvalue as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_REDO
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and NewCurenCD <>''
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3