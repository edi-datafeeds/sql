--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_RHTS
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_RHTS_ENT.Changed,
'select  ENTNotes As Notes FROM ENT WHERE RDID = '+ cast(t_EQ_RHTS_ENT.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'116'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_RHTS_ENT.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_RHTS_ENT.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_RHTS_ENT.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_RHTS_ENT.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHTS',
':22F::CAMV//CHOS',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_RHTS_ENT.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_RHTS_ENT.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_RHTS_ENT.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_RHTS_ENT.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , t_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN t_EQ_RHTS_ENT.EndSubscription <>''
AND t_EQ_RHTS_ENT.EndSubscription IS NOT NULL
       AND t_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_RHTS_ENT.StartSubscription,112)+'/'
          +CONVERT ( varchar , t_EQ_RHTS_ENT.EndSubscription,112)
     WHEN t_EQ_RHTS_ENT.StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_RHTS_ENT.StartSubscription,112)+'/UKWN'
     WHEN t_EQ_RHTS_ENT.EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_RHTS_ENT.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN t_EQ_RHTS_ENT.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_RHTS_ENT.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_RHTS_ENT.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_RHTS_ENT.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_RHTS_ENT.Paydate <> '' and t_EQ_RHTS_ENT.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_RHTS_ENT.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN t_EQ_RHTS_ENT.EntIssuePrice <> ''
     THEN ':90B::PRPP//ACTU/'+CurenCD+ltrim(cast(t_EQ_RHTS_ENT.EntIssuePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB2<>''
     THEN ':35B:' + resTFB2
     ELSE ':35B:' + mainTFB2
     END,
CASE WHEN t_EQ_RHTS_ENT.RatioNew <>''
          AND t_EQ_RHTS_ENT.RatioOld <>''
     THEN ':92D::NWRT//'+ltrim(cast(t_EQ_RHTS_ENT.RatioNew as char (15)))+
                    '/'+ltrim(cast(t_EQ_RHTS_ENT.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_RHTS_ENT
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3