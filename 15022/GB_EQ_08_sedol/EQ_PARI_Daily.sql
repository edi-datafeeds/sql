--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_PARI
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_PARI.Changed,
'' as ISOHDR,
':16R:GENL',
'124'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_PARI.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_PARI.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_PARI.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_PARI.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PARI',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_PARI.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_PARI.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_PARI.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_PARI.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_PARI.AssimilationDate <>''
     THEN ':98A::PPDT//'+CONVERT ( varchar , t_EQ_PARI.AssimilationDate,112)
     ELSE ':98B::PPDT//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB2<>''
     THEN ':35B:' + resTFB2
     ELSE ':35B:' + mainTFB2
     END,
/* CASE WHEN t_EQ_PARI.RatioNew <>''
            AND t_EQ_PARI.RatioNew IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(t_EQ_PARI.RatioNew as char (15)))+
                    '/'+rtrim(cast(t_EQ_PARI.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB, 
next line is a placebo, needs a ratioold,rationew added to parent */
':92K::NEWO//UKWN',
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From t_EQ_PARI
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3