--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXRI
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
t_EQ_EXRI_RTS.Changed,
'select  RTSNotes As Notes FROM RTS WHERE RDID = '+ cast(t_EQ_EXRI_RTS.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'118'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_EXRI_RTS.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_EXRI_RTS.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_EXRI_RTS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_EXRI_RTS.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXRI',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
/*
Removed 2008/11/12, but left in-case it's required later
'' as link568,
':16R:LINK',
':20C::RELA//117'+t_EQ_EXRI_RTS.EXCHGID+cast(t_EQ_EXRI_RTS.Seqnum as char(1))+ cast(t_EQ_EXRI_RTS.EventID as char(16)) as CAref,
':16S:LINK',
*/
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_EXRI_RTS.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_EXRI_RTS.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_EXRI_RTS.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_EXRI_RTS.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_EXRI_RTS.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_EXRI_RTS.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_EXRI_RTS.EndTrade <> '' 
       AND t_EQ_EXRI_RTS.StartTrade <> '' 
     THEN ':69A::TRDP//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartTrade,112) + '/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndTrade,112)
     WHEN t_EQ_EXRI_RTS.StartTrade <> '' 
     THEN ':69C::TRDP//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartTrade,112) + '//UKWN'
     WHEN t_EQ_EXRI_RTS.EndTrade <> '' 
     THEN ':69E::TRDP//UKWN/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndTrade,112)
     ELSE ':69J::TRDP//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_EXRI_RTS.RdID as char(16)) as Notes, */ 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN t_EQ_EXRI_RTS.CurenCD <> '' 
     THEN ':11A::OPTN//' + t_EQ_EXRI_RTS.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN t_EQ_EXRI_RTS.EndSubscription <>''
       AND t_EQ_EXRI_RTS.StartSubscription <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartSubscription,112)+'/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndSubscription,112)
     WHEN t_EQ_EXRI_RTS.StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.StartSubscription,112)+'/UKWN'
     WHEN t_EQ_EXRI_RTS.EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_EXRI_RTS.EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN t_EQ_EXRI_RTS.IssuePrice <> ''
     THEN ':90B::PRPP//ACTU/'+CurenCD+ltrim(cast(t_EQ_EXRI_RTS.IssuePrice as char(15)))
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB2<>''
     THEN ':35B:' + resTFB2
     ELSE ':35B:' + mainTFB2
     END,
CASE WHEN t_EQ_EXRI_RTS.RatioNew <>''
               AND t_EQ_EXRI_RTS.RatioOld <>''
     AND t_EQ_EXRI_RTS.RatioNew is not null
               AND t_EQ_EXRI_RTS.RatioOld is not null
     THEN ':92D::NWRT//'+ltrim(cast(t_EQ_EXRI_RTS.RatioNew as char (15)))+
                    '/'+ltrim(cast(t_EQ_EXRI_RTS.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* EXERCISE OPTION END */
/* SELL RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SLLE',
':17B::DFLT//N',
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* SELL RIGHTS OPTION END */
/* LAPSE RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//003',
':22F::CAOP//LAPS',
':17B::DFLT//N',
CASE WHEN t_EQ_EXRI_RTS.Paydate <>'' 
               AND t_EQ_EXRI_RTS.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , t_EQ_EXRI_RTS.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* LAPSE RIGHTS OPTION END */
'' as Notes,
'-}$'
From t_EQ_EXRI_RTS
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3