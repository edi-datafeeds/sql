--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_TEND
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=TKOVR
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
'select distinct * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' 
+ rtrim(cast(t_EQ_TEND_TKOVR.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) 
+ ' Order By OptionID, SerialID' as MPAYlink,
t_EQ_TEND_TKOVR.Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(t_EQ_TEND_TKOVR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'131'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_TEND_TKOVR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_TEND_TKOVR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_TEND_TKOVR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_TEND_TKOVR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
CASE WHEN t_EQ_TEND_TKOVR.CmAcqdate <> ''
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_TEND_TKOVR.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_TEND_TKOVR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_TEND_TKOVR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_TEND_TKOVR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_TEND_TKOVR.Opendate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , t_EQ_TEND_TKOVR.opendate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.CmAcqdate <> ''
     THEN ':98A::WUCO//'+CONVERT ( varchar , t_EQ_TEND_TKOVR.CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.CloseDate <> ''
       AND t_EQ_TEND_TKOVR.OpenDate <> ''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.OpenDate,112)+'/'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.CloseDate,112)
     WHEN t_EQ_TEND_TKOVR.OpenDate <> ''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.OpenDate,112)+'/UKWN'
     WHEN t_EQ_TEND_TKOVR.CloseDate <> ''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , t_EQ_TEND_TKOVR.CloseDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.CloseDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , t_EQ_TEND_TKOVR.CloseDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN t_EQ_TEND_TKOVR.UnconditionalDate <>'' and t_EQ_TEND_TKOVR.UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From t_EQ_TEND_TKOVR
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3