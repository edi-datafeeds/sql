--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_EXOF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
Changed,
'select  DvstNotes As Notes FROM DVST WHERE RDID = '+ cast(t_EQ_EXOF_DVST.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'128'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD <>''
     THEN ':94B::PLIS//EXCH/' + MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <> ''
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98A::XDTE//UKWN'
     END,
CASE WHEN EndSubscription <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndSubscription <>''
       AND StartSubscription <>''
       THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/'
          +CONVERT ( varchar , EndSubscription,112)
     WHEN StartSubscription <>''
          THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/UKWN'
     WHEN EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASE',
CASE WHEN CurenCD <> '' 
     THEN ':11A::OPTN//' + CurenCD
     END,
':17B::DFLT//N',
CASE WHEN MaxPrice <>''
     THEN ':90B::PRPP//ACTU/'+CurenCD+
          +substring(MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB2<>''
     THEN ':35B:' + resTFB2
     ELSE ':35B:' + mainTFB2
     END,
':92D::NEWO//1,/1,',
CASE WHEN Paydate <>'' 
             AND Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , Paydate,112)
     WHEN paydate <>'' 
             AND paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN resTFB2<>''
     THEN ':35B:' + resTFB2
     ELSE ':35B:' + mainTFB2
     END,
CASE WHEN Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
CASE WHEN RatioNew <>''
               AND RatioOld <>''
     THEN ':92D::ADEX//'+ltrim(cast(RatioNew as char (15)))+
                    '/'+ltrim(cast(RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
from t_EQ_EXOF_DVST
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3