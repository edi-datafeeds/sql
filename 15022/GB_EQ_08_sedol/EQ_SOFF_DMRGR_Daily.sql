--FilePath=o:\DataFeed\15022\GB_EQ_08_sedol\
--FilePrefix=
--FileName=YYYYMMDD_EQ564_SOFF
--FileNameNotes=YYYYMMDD_EQ568.TXT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\GB_EQ_08_sedol\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DMRGR
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
select
'select distinct * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + ltrim(cast(t_EQ_SOFF_DMRGR.EventID as char(10))) + ' and sEvent = '+ char(39) + 'DMRGR'+ char(39) + ' Order By OptionID, SerialID' as MPAYlink,
t_EQ_SOFF_DMRGR.Changed,
'select  DmrgrNotes As Notes FROM DMRGR WHERE RDID = '+ cast(t_EQ_SOFF_DMRGR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'138'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN t_EQ_SOFF_DMRGR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN t_EQ_SOFF_DMRGR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN t_EQ_SOFF_DMRGR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN t_EQ_SOFF_DMRGR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SOFF',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB2,
substring(t_EQ_SOFF_DMRGR.Issuername,1,35) as TIDYTEXT,
substring(t_EQ_SOFF_DMRGR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN t_EQ_SOFF_DMRGR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + t_EQ_SOFF_DMRGR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN t_EQ_SOFF_DMRGR.ExDate <> '' and  t_EQ_SOFF_DMRGR.ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , t_EQ_SOFF_DMRGR.ExDate,112)
     ELSE ':98A::XDTE//UKWN'
     END,
CASE WHEN t_EQ_SOFF_DMRGR.Recdate <> '' and t_EQ_SOFF_DMRGR.Recdate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , t_EQ_SOFF_DMRGR.Recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,/* Notes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(t_EQ_SOFF_DMRGR.RDID as char(16)) as Notes, */ 
':16S:CADETL',
/* START OF CASH BLOCK */
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From t_EQ_SOFF_DMRGR
WHERE 
mainTFB2<>''
and MCD = 'XLON'
and (ListStatus<>'D' or SCEXHActtime>changed-2)
order by caref2 desc, caref1, caref3