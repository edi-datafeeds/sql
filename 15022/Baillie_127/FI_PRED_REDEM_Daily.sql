--SEMETABLE=client.dbo.seme_baillie
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_PRED
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(RedemNotes as varchar(24)))=22 then rtrim(char(32)) else RedemNotes end as Notes FROM REDEM WHERE RedemID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01BAILLIEXXXXO0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Redemdate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Redemdate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '207' END as CAref1, */
'150' as CAref1,
EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRED' as CAEV,
case when maintab.mandoptflag = 'M' then ':22F::CAMV//MAND' else ':22F::CAMV//VOLU' end as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
CASE WHEN redem.poolfactor is not null
     THEN ':25D::PROC//COMP'
     ELSE ':25D::PROC//PREC'
     END as PROCCOMP,
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN maintab.RedemDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , maintab.RedemDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':92A::PRFC//' as NOCRLF1,
CASE WHEN redem.poolfactor is not null
     THEN redem.poolfactor
     ELSE '1'
     END as COMMASUB,
':92A::NWFC//' as NOCRLF2,
maintab.poolfactor as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN maintab.CurenCD  <>''
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.RedemDate <>''
     THEN ':98A::PAYD//' + CONVERT(varchar, maintab.RedemDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':92A::RATE//' as NOCRLF3,
CASE WHEN redem.poolfactor is not null
     THEN round(cast(redem.poolfactor as decimal(18,13)) - cast(maintab.poolfactor as decimal(18,13)),9)
     ELSE round(1-cast(maintab.parvalue as decimal(18,13)),9)
     END as COMMASUB,
case WHEN maintab.denomination1 ='0' or maintab.denomination1=''
     THEN ':90E::OFFR//UKWN'
     ELSE ''
     END,
case WHEN maintab.denomination1 <>'0' and maintab.denomination1<>''
     THEN ':90F::OFFR//PLOT/'+maintab.curencd
     ELSE ''
     END as NOCRLF4,
CASE WHEN redem.poolfactor is not null and maintab.denomination1 <>'' and maintab.denomination1 <>'0'
     THEN round(cast(maintab.denomination1 as float) * ((cast(redem.poolfactor as float) - cast(maintab.poolfactor as float))/100),9)
     WHEN maintab.denomination1 <>'' and maintab.denomination1 <>'0'
     THEN round(cast(maintab.denomination1 as float) * ((1-cast(maintab.poolfactor as float))/100),9)
     ELSE 0
     END as COMMASUBNOCRLF,
case WHEN maintab.denomination1 ='0' or maintab.denomination1=''
     THEN ''
     ELSE '/FAMT/'
     END as NOCRLF5,
case WHEN maintab.denomination1 ='0' or maintab.denomination1=''
     THEN ''
     ELSE maintab.denomination1 
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes70F,
'-}$'
From v_FI_REDM as maintab
left outer join redem on maintab.secid=redem.secid
                           and redem.partfinal = 'P'
                           and redem.poolfactor <> ''
                           and redem.poolfactor <> '0'
/* left outer join client.dbo.seme_baillie as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'PRED'=SR.CAEVMAIN */
WHERE
maintab.Sedol<>''
and maintab.RedemDate > getdate() and maintab.RedemDate < getdate()+2
and ((maintab.sedol in (select code from client.dbo.pfsedol where accid=127 and actflag='I')
and maintab.RedemDate > getdate())
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=127 and actflag='U')
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)))
and maintab.denomination1<>'1'
and maintab.redemtype<>'BB' and maintab.redemtype<>'BBED' and maintab.redemtype<>'BBRD'
and (maintab.indefpay='' or maintab.indefpay = 'P')
and maintab.partfinal = 'P'
and maintab.redemdate <>''
and maintab.redemdate <> '1800/01/01'
and not (maintab.redemtype='MAT' or maturitydate=maintab.redemdate)
and maintab.poolfactor <> ''
and maintab.redemtype <> 'PUT'
and maintab.mandoptflag<>''
and (redem.redemid is null or redem.redemid=
(select top 1 redemid from redem as subredem
where
subredem.partfinal = 'P'
and subredem.redemtype <> 'PUT'
and subredem.mandoptflag<>''
and not (subredem.redemtype='MAT' or maintab.maturitydate=subredem.redemdate)
and ''<>subredem.poolfactor
and '0'<>subredem.poolfactor
and maintab.secid=subredem.secid
and 'D'<>subredem.actflag
and maintab.redemdate>subredem.redemdate
order by subredem.redemdate desc))
order by caref2 desc, caref1, caref3
