--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_CONV
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--Archive=off
--ArchivePath=n:\15022\newedge_268\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(ConvNotes as varchar(24)))<30 then rtrim(char(32)) else ConvNotes end As Notes FROM CONV WHERE ConvID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01SAMPLEAAXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '325'+EXCHGID+cast(Seqnum as char(1))
     ELSE '325'
     END as CAref1,
EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//REOR',
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB2 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
/*':70E::OFFO//'+substring(Issuername,1,35) as TIDYTEXT,*/
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
CASE WHEN ToDate is not null and ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN ToDate is not null and ToDate <>''
       AND FromDate is not null and FromDate <>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/'
          +CONVERT ( varchar , ToDate,112)
     WHEN FromDate is not null and FromDate <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , FromDate,112)+'/UKWN'
     WHEN ToDate is not null and ToDate <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , ToDate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1 <>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:/US/999999999'
     END,
CASE WHEN resTFB1 =''
     THEN 'NOT AVAILABLE AT PRESENT'
     ELSE ''
     END,
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN Price <>''
     THEN ':90B::PRPP//ACTU/'+CurenCD+substring(Price,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
CASE WHEN RatioNew <>''
          AND RatioOld <>''
     THEN ':92D::NEWO//'+rtrim(cast(RatioNew as char (15)))+
                    '/'+rtrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN SettlementDate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , SettlementDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN ToDate is not null and ToDate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16S:CAOPTN',
'' as Notes,  
'-}$'
From v_FI_CONV as maintab
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=1000 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=994 and actflag='I'))
and Todate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=1000 and actflag='I')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=1000 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=994 and actflag='U'))
and changed>(select max(feeddate)-92 from wca.dbo.tbl_Opslog where seq = 3)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='' or (maintab.sedol in (select code from client.dbo.pfsedol where accid=994 and actflag<>'D')))
and convtype='BB'
and Actflag  <>''
order by caref2 desc, caref1, caref3
