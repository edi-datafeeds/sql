--SEMETABLE=client.dbo.seme_sample
--FileName=edi_YYYYMMDD
--OTHR_ARR_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\RAT\
--FileTidy=N
--sEvent=ARR
--TFBNUMBER=2
--INCREMENTAL=OFF
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select
'' as bbgcntry,
'' as BBGCUREN,
Changed,
'select  ArrNotes As Notes FROM Arr WHERE RDID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01SAMPLEAAXXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'112'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB2 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <>'' and ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN RecDate <>'' and RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
case when wca.dbo.MPAY.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_OTHR_ARR as maintab
left outer join wca.dbo.MPAY on maintab.eventid = wca.dbo.MPAY.eventid and 'ARR' = wca.dbo.MPAY.sevent
                                             and 'D'=wca.dbo.MPAY.paytype and 'D'<>wca.dbo.MPAY.actflag
WHERE 
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=1000 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=994 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=994 and actflag='I'))
      and Exdate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=1000 and actflag='I')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=994 and actflag='I')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=994 and actflag='I'))
and changed>(select max(feeddate)-92 as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by caref2 desc, caref1, caref3
