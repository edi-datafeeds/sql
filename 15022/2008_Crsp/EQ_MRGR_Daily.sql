--FilePath=o:\DataFeed\15022\2008_Crsp\
--FilePrefix=
--MRGR
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\2008_Crsp\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=MRGR
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
'select distinct * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + ltrim(cast(v_EQ_MRGR.EventID as char(10))) + ' and sEvent = '+ char(39) + 'MRGR'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
v_EQ_MRGR.Changed,
'select  MrgrTerms As Notes FROM MRGR WHERE RdID = '+ cast(v_EQ_MRGR.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'126'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_MRGR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_MRGR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_MRGR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_MRGR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//MRGR',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_MRGR.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_MRGR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_MRGR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_MRGR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_MRGR.EffectiveDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_MRGR.EffectiveDate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
CASE WHEN v_EQ_MRGR.RecDate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , v_EQ_MRGR.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v_EQ_MRGR.RDID as char(16)) as Notes, */ 
':16S:CADETL',
/* START OF CASH BLOCK */
':16R:CAOPTN' as StartseqE,
'' as Notes,
'-}$'
From v_EQ_MRGR
WHERE 
uscode<>''
and (CalcListdate<=Exdate or (Exdate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=AnnounceDate))
and changed >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3