--FilePath=o:\DataFeed\15022\2008_Crsp\
--FilePrefix=
--REDO
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\2008_Crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_REDO.Changed,
'select  CurrdNotes As Notes FROM CURRD WHERE CurrdID = '+ cast(v_EQ_REDO.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'162'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_REDO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_REDO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_REDO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_REDO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO',
':22F::CAMV//MAND',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_REDO.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_REDO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + v_EQ_REDO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_REDO.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , v_EQ_REDO.EffectiveDate,112)
     ELSE ':98A::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
':35B:/US/' + uscode,
':16R:FIA',
CASE WHEN v_EQ_REDO.MCD <> ''
     THEN ':94B::PLIS//EXCH/' + v_EQ_REDO.MCD
     END,
':11A::DENO//'+v_EQ_REDO.NewCurenCD,
':16S:FIA',
/*CASE WHEN v_EQ_REDO.oldparvalue  <>'' and v_EQ_REDO.newparvalue <>''
     and cast(v_EQ_REDO.oldparvalue as float(11,5))>0 and cast(v_EQ_REDO.newparvalue as float(11,5))>0
     THEN ':92D::NEWO//1,/'+cast(cast(v_EQ_REDO.oldparvalue as float(11,5))/cast(v_EQ_REDO.newparvalue as float(11,5)) as varchar(30))
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,*/
CASE WHEN v_EQ_REDO.newparvalue <>''
            AND v_EQ_REDO.oldparvalue <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_EQ_REDO.newparvalue as char (15)))+
                    '/'+rtrim(cast(v_EQ_REDO.oldparvalue as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_REDO
WHERE 
uscode<>''
and NewCurenCD <>''
and (CalcListdate<=EffectiveDate or (EffectiveDate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=EffectiveDate or (EffectiveDate is null and CalcDelistdate>=AnnounceDate))
and changed >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3