--FilePath=o:\DataFeed\15022\2008_Crsp\
--FilePrefix=
--MEET
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\2008_Crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_MEET.Changed,
'' as ISOHDR,
':16R:GENL',
'161'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_MEET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_EQ_MEET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_MEET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_MEET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN v_EQ_MEET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN v_EQ_MEET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN v_EQ_MEET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN v_EQ_MEET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN v_EQ_MEET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_MEET.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_MEET.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_MEET.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_MEET.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v_EQ_MEET.AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , v_EQ_MEET.AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
':70E::ADTX//' + substring(rtrim(v_EQ_MEET.Add1),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add1),36,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add2),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add2),36,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add3),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add4),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add5),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Add6),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.City),1,35) as TIDYTEXT,
substring(rtrim(v_EQ_MEET.Country),1,35) as TIDYTEXT,
':16S:ADDINFO',
'-}$'
From v_EQ_MEET
WHERE 
uscode<>''
and (CalcListdate<=AGMDate or (AGMDate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=AGMDate or (AGMDate is null and CalcDelistdate>=AnnounceDate))
and (v_EQ_MEET.AGMEGM = 'AG'
or v_EQ_MEET.AGMEGM = 'CG'
or v_EQ_MEET.AGMEGM = 'EG'
or v_EQ_MEET.AGMEGM = 'GM'
or v_EQ_MEET.AGMEGM = 'SG')
and changed >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3