--FilePath=o:\DataFeed\15022\2008_Crsp\
--FilePrefix=
--EXOF
--FileName=YYYYMMDD_EQ564
--FileNameNotes=YYYYMMDD_EQ568.txt
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\2008_Crsp\
--FIELDHEADERS=off
--FileTidy=N
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select
v_EQ_EXOF_SCSWP.Changed,
'select  ScswpNotes As Notes FROM SCSWP WHERE RdID = '+ cast(v_EQ_EXOF_SCSWP.EventID as char(16)) as ChkNotes, 
'' as ISOHDR,
':16R:GENL',
'164'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v_EQ_EXOF_SCSWP.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN v_EQ_EXOF_SCSWP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_EQ_EXOF_SCSWP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_EQ_EXOF_SCSWP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:/US/' + uscode,
substring(v_EQ_EXOF_SCSWP.Issuername,1,35) as TIDYTEXT,
substring(v_EQ_EXOF_SCSWP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v_EQ_EXOF_SCSWP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v_EQ_EXOF_SCSWP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':69J::PWAL//UKWN'
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB3<>''
     THEN ':35B:' + resTFB3
     ELSE ':35B:/US/' + uscode
     END,
CASE WHEN v_EQ_EXOF_SCSWP.newratio <>''
          AND v_EQ_EXOF_SCSWP.oldratio <>''
     THEN ':92D::NEWO//'+rtrim(cast(v_EQ_EXOF_SCSWP.newratio as char (15)))+
                    '/'+rtrim(cast(v_EQ_EXOF_SCSWP.oldratio as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN v_EQ_EXOF_SCSWP.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , v_EQ_EXOF_SCSWP.Paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
'' as Notes,
'-}$'
From v_EQ_EXOF_SCSWP
WHERE 
uscode<>''
and (CalcListdate<=Exdate or (Exdate is null and CalcListdate<=AnnounceDate))
and (CalcDelistdate>=Exdate or (Exdate is null and CalcDelistdate>=AnnounceDate))
and changed >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (primaryexchgcd = 'USAMEX'
or primaryexchgcd = 'USNASD'
or primaryexchgcd = 'USNYSE'
or primaryexchgcd = 'USPAC')
and primaryexchgcd = exchgcd

order by caref2 desc, caref1, caref3