select distinct
mrgr.rdid as EDI_EventID,
rd.secid,
mpay.optionid,
mpay.serialid,
exchg.mic,
sb.corp,
issur.issuername as issuer_name,
pit35.issuername as ASOF_issuer_name,
scmst.isin,
pit35.isin as ASOF_Parent_Isin,
mpay.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
pit.isin as ASOF_resultant_isin,
pit.issuername as ASOF_resultant_issuer_name,
mrgr.effectivedate,
case when exdt.rdid is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date
from mrgr
inner join rd on mrgr.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'MRGR' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'MRGR' = pexdt.eventtype
left outer join wca2.pitsrf as pit35 on scmst.secid=pit35.secid 
     and ((ifnull(mrgr.effectivedate,'2099-01-01')>=date_sub(pit35.startdt, interval 7 day) and ifnull(mrgr.effectivedate,'2099-01-01')<=pit35.enddt)
       or (mrgr.effectivedate is null and exdt.exdate>=date_sub(pit35.startdt, interval 7 day) and exdt.exdate<=pit35.enddt))
left outer join mpay on mrgr.rdid = mpay.eventid and 'MRGR' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join wca2.pitsrf as pit on mpay.ressecid=pit.secid 
     and ((ifnull(mrgr.effectivedate,'2099-01-01')>=date_sub(pit.startdt, interval 7 day) and ifnull(mrgr.effectivedate,'2099-01-01')<=pit.enddt)
       or (mrgr.effectivedate is null and exdt.exdate>=date_sub(pit.startdt, interval 7 day) and exdt.exdate<=pit.enddt))
inner join wca2.semebuy as sb on mrgr.rdid=sb.eventid and rd.secid=sb.secid
where
(mpay.paytype='B' or mpay.paytype='S')
and sectygrp.secgrpid is not null
and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (mrgr.announcedate < scexh.acttime or scexh.liststatus<>'D')
and scexh.actflag<>'D'
and mrgr.actflag<>'D'
and (EffectiveDate>='2010/01/01' or exdt.exdate>'2010/01/01' or pexdt.exdate>'2010/01/01' or mrgr.announcedate>'2010/01/01')
and (scexh.exchgcd='JPTSE' or scexh.exchgcd='USNASD' or scexh.exchgcd='USNYSE')
order by EDI_EventID, rd.secid, mpay.optionid, mpay.serialid, exchg.mic;
