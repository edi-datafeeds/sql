select distinct
tkovr.tkovrid as EDI_EventID,
tkovr.secid,
mpay.optionid,
mpay.serialid,
exchg.mic,
sb.corp,
issur.issuername as issuer_name,
pit35.issuername as ASOF_issuer_name,
scmst.isin,
pit35.isin as ASOF_Parent_Isin,
mpay.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
pit.isin as ASOF_resultant_isin,
pit.issuername as ASOF_resultant_issuer_name,
tkovr.CmAcqdate,
tkovr.Closedate
from tkovr
inner join scmst on tkovr.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
left outer join wca2.pitsrf as pit35 on scmst.secid=pit35.secid 
     and ((ifnull(tkovr.CmAcqdate,'2099-01-01')>=date_sub(pit35.startdt, interval 7 day) and ifnull(tkovr.CmAcqdate,'2099-01-01')<=pit35.enddt)
       or (tkovr.CmAcqdate is null and tkovr.closedate>=date_sub(pit35.startdt, interval 7 day) and tkovr.closedate<=pit35.enddt))
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join mpay on tkovr.tkovrid = mpay.eventid and 'TKOVR' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join wca2.pitsrf as pit on mpay.ressecid=pit.secid 
     and ((ifnull(tkovr.CmAcqdate,'2099-01-01')>=date_sub(pit.startdt, interval 7 day) and ifnull(tkovr.CmAcqdate,'2099-01-01')<=pit.enddt)
       or (tkovr.CmAcqdate is null and tkovr.closedate>=date_sub(pit.startdt, interval 7 day) and tkovr.closedate<=pit.enddt))
inner join wca2.semebuy as sb on tkovr.tkovrid=sb.eventid and tkovr.secid=sb.secid
where
(mpay.paytype='B' or mpay.paytype='S')
and sectygrp.secgrpid is not null
and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (tkovr.announcedate < scexh.acttime or scexh.liststatus<>'D')
and scexh.actflag<>'D'
and tkovr.actflag<>'D'
and tkovr.opendate is null
and (tkovr.closedate>='2010/01/01' or tkovr.cmAcqdate>='2010/01/01')
and tkovr.minitkovr<>'T'
and (scexh.exchgcd='JPTSE' or scexh.exchgcd='USNASD' or scexh.exchgcd='USNYSE')
order by EDI_EventID, tkovr.secid, mpay.optionid, mpay.serialid, exchg.mic;
