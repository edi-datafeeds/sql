--SEMETABLE=client.dbo.seme_buysignal
--FileName=edi_YYYYMMDD
--DVCA_CHOS_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where feeddate='2017/08/31'
--archive=off
--ArchivePath=n:\15022\Citibank\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DIV
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed as Changed,
'select case when len(cast(DivNotes as varchar(255)))>40 then DivNotes else rtrim(char(32)) end As Notes FROM DIV WHERE DIVID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
paydate as MPAYLINK,
paydate2 as MPAYLINK2,
maintab.cntrycd as MPAYLINK3,
maintab.frankdiv as FLFR,
maintab.unfrankdiv as UNFR,
case when maintab.StructCD<>'XREIT' then '' else maintab.StructCD end as structcd,
case when 1=1 then (select '{1:F01BUYSIG22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ':23G:NEWM'
     END AS MessageStatus,
':22F::CAEV//DRIP' as CAEV,
':22F::CAMV//CHOS' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , maintab.Changed,112) + replace(convert ( varchar, maintab.Changed,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:ISIN ' + maintab.isin as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN DeclarationDate IS NOT NULL
     THEN ':98A::ANOU//'+CONVERT ( varchar , DeclarationDate,112)
     WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     WHEN maintab.Actflag = 'D'
     THEN ':98A::XDTE//20990101'
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL and maintab.cntrycd<>'LK'
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE 
     WHEN Marker='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN Marker='SPL'
     THEN ':22F::DIVI//SPEC'
     WHEN Frequency='IRG'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency='UN'
     THEN ':22F::DIVI//INTE'
     WHEN Frequency=''
     THEN ':22F::DIVI//INTE'
     ELSE ':22F::DIVI//REGR'
     END,
':16S:CADETL',
'' as StartseqE,
case when maintab.dripcntrycd<>'' then ':16R:CAOPTN' else '' end,
case when maintab.dripcntrycd<>''
     then ':13A::CAON//00'
     +cast((select max(optionid)+1 from wca.dbo.divpy where wca.dbo.divpy.divid=maintab.EventID) as char(1)) else '' end,
case when maintab.dripcntrycd<>'' then ':22F::CAOP//SECU' else '' end,
case when maintab.dripcntrycd<>'' then ':17B::DFLT//N' else '' end,
case when maintab.dripcntrycd<>'' then ':16R:SECMOVE' else '' end,
case when maintab.dripcntrycd<>'' then ':22H::CRDB//CRED' else '' end,
case when maintab.dripcntrycd<>'' then ':35B:/GB/' + maintab.isin else '' end,
case when maintab.dripcntrycd<>'' then ':16R:FIA' else '' end,
case when maintab.dripcntrycd<>'' then ':94B::PLIS//SECM' else '' end,
case when maintab.dripcntrycd<>'' then ':16S:FIA' else '' end,
CASE WHEN maintab.DripReinvPrice<>''
     THEN ':90B::PRPP//ACTU/'+maintab.DripCurenCD+ltrim(cast(maintab.DripReinvPrice as char(15)))
     WHEN maintab.dripcntrycd<>'' THEN ':90E::PRPP//UKWN'
     else '' end as COMMASUB,
CASE WHEN maintab.DripPaydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT(varchar, maintab.DripPaydate,112)
     WHEN maintab.dripcntrycd<>'' THEN ':98B::PAYD//UKWN'
     else '' end,
case when maintab.dripcntrycd<>'' then ':16S:SECMOVE' else '' end,
case when maintab.dripcntrycd<>'' then ':16S:CAOPTN' else '' end,
'' as Notes,
'-}'
From v_EQ_DV_CA_SE_OP as maintab
left outer join client.dbo.seme_buysignal as SR on maintab.eventid=SR.caref2
                                         and maintab.SecID=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'DRIP'=SR.CAEVMAIN
left outer join v10s_divpy as divopt1 on maintab.eventid = divopt1.divid 
left outer join v10s_divpy as divopt2 on maintab.eventid = divopt2.divid 
                   and 2=divopt2.optionid and 'D'<>divopt2.actflag
                   and 2=divopt2.optionid and 'C'<>divopt2.actflag
left outer join v10s_divpy as divopt3 on maintab.eventid = divopt3.divid 
                   and 3=divopt3.optionid and 'D'<>divopt3.actflag
                   and 3=divopt3.optionid and 'C'<>divopt3.actflag
WHERE
maintab.isin<>'' and maintab.isin is not null
and maintab.actflag<>'D'
and divopt1.divid is not null 
and (exchgcd='JPTSE' or exchgcd='USNASD' or exchgcd='USNYSE')
and changed>'2019/02/12' and changed<'2019/03/06'
and divopt1.optionid is not null
and ((maintab.dripcntrycd='CA' or maintab.dripcntrycd='GB' or maintab.dripcntrycd='ZA')
   or (maintab.dripcntrycd<>''
       and not ((divopt1.actflag is not null and divopt2.actflag is not null)
       or (divopt1.actflag is not null and divopt3.actflag is not null)      
       or (divopt2.actflag is not null and divopt3.actflag is not null))))
order by caref2 desc, caref1, caref3
