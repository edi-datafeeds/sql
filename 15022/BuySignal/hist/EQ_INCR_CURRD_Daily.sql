--SEMETABLE=client.dbo.seme_buysignal
--FileName=edi_YYYYMMDD
--INCR_CURRD_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where feeddate='2017/08/31'
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--TEXTCLEAN=2

--# 1
use wca
select distinct
maintab.Changed as Changed,
'select CurrdNotes As Notes FROM CURRD WHERE CurrdID = '+ cast(EventID as char(16)) as ChkNotes, 
case when 1=1 then (select '{1:F01BUYSIG22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
'146'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
maintab.EventID as CAREF2,
maintab.SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INCR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , maintab.Changed,112) + replace(convert ( varchar, maintab.Changed,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:ISIN ' + maintab.isin as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN effectivedate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT/'+NewParValue as commasub,
':16S:CADETL',
'' as Notes,
'-}'
From v_EQ_REDO as maintab
left outer join client.dbo.seme_buysignal as SR on maintab.eventid=SR.caref2
                                         and maintab.SecID=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'INCR'=SR.CAEVMAIN
WHERE
maintab.isin<>'' and maintab.isin is not null
and maintab.actflag<>'D'
and (exchgcd='JPTSE' or exchgcd='USNASD' or exchgcd='USNYSE')
and changed>'2016/01/01' and changed<'2017/01/01'
and rtrim(OldParValue)<>'' and rtrim(NewParValue)<>''
and rtrim(OldParValue)<>'0' and rtrim(NewParValue)<>'0'
and cast(rtrim(OldParValue) as float(16,7))<cast(rtrim(NewParValue) as float(16,7))/* and effectivedate>getdate()-8 and effectivedate is not null */
order by caref2 desc, caref1, caref3
