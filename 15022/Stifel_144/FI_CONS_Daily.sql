--SEMETABLE=client.dbo.seme_stifel
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_CONS
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq=3
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=OFF
--NOTESLIMIT=7900
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(Notes as varchar(24)))=22 then rtrim(char(32)) else Notes end As Notes FROM COSNT WHERE RDID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01STIFELXXXXXO0300000054}{2:I564EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Expirydate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Expirydate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '215' END as CAref1, */
'150' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONS' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN cast(parvalue as float)>1 and parvalue like '%.00000%' 
     THEN ':36B::MINO//FAMT/'+rtrim(replace(ParValue,'.00000','.'))
     WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue
     ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
':98B::ANOU//UKWN',
CASE WHEN maintab.RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , maintab.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':22F::CONS//CTRM',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONY',
':17B::DFLT//N',
CASE WHEN MinimumDenomination<>'' THEN ':36B::MIEX//FAMT/'+rtrim(replace(MinimumDenomination,'.000000','.')) ELSE '' END as COMMASUB,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN 1=2 AND Expirydate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/'
          +CONVERT ( varchar , Expirydate,112)
     WHEN 1=2
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/UKWN'
     WHEN Expirydate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Expirydate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::PAYD//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:CASHMOVE',
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//CONN',
':17B::DFLT//N',
CASE WHEN MinimumDenomination<>'' THEN ':36B::MIEX//FAMT/'+rtrim(replace(MinimumDenomination,'.000000','.')) ELSE '' END as COMMASUB,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN 1=2 AND Expirydate<>''
     THEN ':69A::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/'
          +CONVERT ( varchar , Expirydate,112)
     WHEN 1=2
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , Expirydate,112)+'/UKWN'
     WHEN Expirydate<>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , Expirydate,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16S:CAOPTN',
':16R:CAOPTN',
':13A::CAON//999',
':22F::CAOP//NOAC',
':17B::DFLT//Y',
CASE WHEN maintab.Expirydate <>''
     THEN ':98A::MKDT//'+CONVERT ( varchar , maintab.Expirydate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
':16S:CAOPTN',
'' as Notes70F,
'-}$'
From v_FI_CONS as maintab
/* left outer join client.dbo.seme_stifel as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CONS'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and ((maintab.isin in (select code from client.dbo.pfisin where accid=144 and actflag='I')
      and Expirydate > getdate())
OR
(maintab.isin in (select code from client.dbo.pfisin where accid=144 and actflag='U')
and maintab.changed>(select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)))
order by caref2 desc, caref1, caref3
