--SEMETABLE=client.dbo.seme_markitmca
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_MEET
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog
--Archive=off
--ArchivePath=n:\15022\sstream\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=8000
--TEXTCLEAN=2

--# 1
use wca
select distinct
v_FI_MEET.changed,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'NFI}{4:' as ISOHDR,
':16R:GENL',
CASE WHEN EXCHGID is not null
     THEN '361'+EXCHGID+cast(Seqnum as char(1))
     ELSE '361'
     END as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN v_FI_MEET.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v_FI_MEET.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v_FI_MEET.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v_FI_MEET.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
CASE WHEN v_FI_MEET.AGMEGM = 'CG'
     THEN ':22F::CAEV//CMET'
     WHEN v_FI_MEET.AGMEGM = 'GM'
     THEN ':22F::CAEV//OMET'
     WHEN v_FI_MEET.AGMEGM = 'EG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_MEET.AGMEGM = 'SG'
     THEN ':22F::CAEV//XMET'
     WHEN v_FI_MEET.AGMEGM = 'AG'
     THEN ':22F::CAEV//MEET'
     END,
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', 
'' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1,
case when substring(mainTFB1,1,4)='ISIN' and sedol<>'' then '/GB/' + sedol else '' end,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(Localcode,'%','PCT'),'&','and')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' WHEN PrimaryExchgCD=ExchgCD THEN ':94B::PLIS//PRIM/'+MCD WHEN PrimaryExchgCD<>ExchgCD THEN ':94B::PLIS//SECM/'+MCD ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN parvalue<>'' THEN ':36B::MIEX//FAMT/'+parvalue ELSE '' END as commasub,
CASE WHEN denominationmultiple is not null and denominationmultiple<>0 THEN ':36B::MILT//FAMT/'+cast(denominationmultiple as varchar(20))+',' ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN recdate<>'' and Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN AGMDate <> ''
     THEN ':98A::MEET//'+CONVERT ( varchar , AGMDate,112)
     ELSE ':98B::MEET//UKWN'
     END,
':94E::MEET//'+substring(rtrim(Country),1,35) as TIDYTEXT,
substring(rtrim(Add1),1,35) as TIDYTEXT,
substring(rtrim(Add1),36,35) as TIDYTEXT,
substring(rtrim(Add2),1,35) as TIDYTEXT,
substring(rtrim(Add2),36,35) as TIDYTEXT,
substring(rtrim(Add3),1,35) as TIDYTEXT,
substring(rtrim(Add4),1,35) as TIDYTEXT,
substring(rtrim(Add5),1,35) as TIDYTEXT,
substring(rtrim(Add6),1,35) as TIDYTEXT,
substring(rtrim(City),1,35) as TIDYTEXT,
':16S:CADETL',
'-}$'
From v_FI_MEET
WHERE 
mainTFB1<>''
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)
/* and substring(primaryexchgcd,1,2)='US' */
and (AGMEGM = 'CG' or AGMEGM = 'GM' or AGMEGM = 'EG' or AGMEGM = 'SG' or AGMEGM = 'AG')
order by caref2 desc, caref1, caref3
