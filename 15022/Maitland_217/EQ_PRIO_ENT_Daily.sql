--SEMETABLE=client.dbo.seme_maitland
--FileName=edi_YYYYMMDD
--PRIO_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\maitland\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=

--# 1
use wca
select distinct
Changed,
'select  EntNotes As Notes FROM Ent WHERE EntID = '+ cast(EventID as char(16)) as ChkNotes, 
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'116' as CAref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO',
':22F::CAMV//VOLU',
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Exdate <>'' AND Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN Recdate <>'' AND Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':22F::SELL//NREN',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//N',
CASE WHEN EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN EndSubscription <>''
       AND StartSubscription <>''
      THEN ':69A::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/'
          +CONVERT ( varchar , EndSubscription,112)
     WHEN StartSubscription <>''
     THEN ':69C::PWAL//'
          +CONVERT ( varchar , StartSubscription,112)+'/UKWN'
     WHEN EndSubscription <>''
     THEN ':69E::PWAL//UKWN/'
          +CONVERT ( varchar , EndSubscription,112)
     ELSE ':69J::PWAL//UKWN'
     END,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN resTFB1<>'UKWN' and resTFB1<>''
     THEN ':35B:' + resTFB1
     ELSE ':35B:' + mainTFB1
     END,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
CASE WHEN RatioNew <>'' AND RatioOld <>''
     THEN ':92D::NEWO//'+ltrim(cast(RatioNew as char (15)))+
                    '/'+ltrim(cast(RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN paydate2 <>'' AND paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate2,112)
     WHEN paydate <>'' AND paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16R:CASHMOVE',
':22H::CRDB//DEBT',
CASE WHEN paydate <>'' AND paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN ENTIssuePrice <>''
     THEN ':90B::PRPP//ACTU/'+CurenCD+
          +substring(ENTIssuePrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
/* '' as Notes, */
'-}$'
From wca2.dbo.i_EQ_PRIO_ENT
WHERE 
mainTFB1<>''
and primaryexchgcd=exchgcd
and exchgid<>'' and exchgid is not null

     and (changed>=(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
          and (sedol in (select code from client.dbo.pfsedol where accid = 217 and actflag<>'D')
          or isin in (select code from client.dbo.pfisin where accid = 217 and actflag<>'D')
          or uscode in (select code from client.dbo.pfuscode where accid = 217 and actflag<>'D')))

order by caref2 desc, caref1, caref3