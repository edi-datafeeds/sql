--SEMETABLE=client.dbo.seme_maitland
--FileName=edi_YYYYMMDD_235959
--TEND_TKOVR_EQ564_YYYYMMDD
--FileNameNotes=
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\maitland\
--FileTidy=N
--sEvent=TKOVR
--TFBNUMBER=1
--INCREMENTAL=
--NOTESLIMIT=

--# 1
use wca
select distinct
'select distinct V10s_MPAY.* from V10s_MPAY'
+ ' where (v10s_MPAY.actflag='+char(39)+'I'+char(39) +' or v10S_MPAY.actflag='+char(39)+'U'+char(39)
+') and eventid = ' + rtrim(cast(EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'TKOVR'+ char(39)
+ ' Order By OptionID, SerialID' as MPAYlink,
Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(EventID as char(16)) as ChkNotes, 
null as PAYLINK,
Opendate as OPENLINK,
Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:' as ISOHDR,
':16R:GENL',
'131' as CAref1,
EventID as CAREF2,
case when Sedol is not null and sedol<>'' then SedolID else secid end as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
CASE WHEN CmAcqdate <> ''
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':35B:' + mainTFB1,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
':16R:FIA',
':94B::PLIS//SECM',
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Opendate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN CmAcqdate <> ''
     THEN ':98A::WUCO//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN UnconditionalDate <>'' and UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
/* '' as Notes, */
'-}$'
From wca.dbo.v_EQ_TEND_TKOVR
WHERE 
mainTFB1<>''
and primaryexchgcd=exchgcd
and exchgid<>'' and exchgid is not null
and (Closedate>getdate()-183 or Closedate is null and Announcedate>getdate()-31)
     and (sedol in (select code from client.dbo.pfsedol where accid = 217 and actflag='I')
          or isin in (select code from client.dbo.pfisin where accid = 217 and actflag='I')
          or uscode in (select code from client.dbo.pfuscode where accid = 217 and actflag='I'))
order by caref2 desc, caref1, caref3