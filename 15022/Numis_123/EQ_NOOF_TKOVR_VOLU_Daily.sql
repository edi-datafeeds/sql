--SEMETABLE=client.dbo.seme_numis
--FileName=edi_YYYYMMDD
--NOOF_TKOVR_VOLU_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--archive=off
--ArchivePath=n:\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=TKOVR
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
'' as bbgcntry,
'' as BBGCUREN,
Changed,
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(maintab.EventID as char(16)) as ChkNotes, 
null as PAYLINK,
Opendate as OPENLINK,
maintab.Closedate as CLOSELINK,
MaxAcpQty as MAQLINK,
'{1:F01NUSEGB21XXXO0300000054}{2:I564EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.Closedate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.Closedate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '131' END as CAref1, */
'131' as CAref1,
maintab.EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     WHEN tkovrstatus = 'L'
     THEN ':23G:WITH'
     WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//NOOF' as CAEV,
':22F::CAMV//VOLU' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , Changed,112) + replace(convert ( varchar, Changed, 08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID, 
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(Securitydesc,1,35) as TIDYTEXT,
CASE WHEN Localcode <>''
     THEN '/TS/' + replace(replace(replace(Localcode,'%','PCT'),'&','and'),'_',' ')
     ELSE ''
     END,
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN Opendate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN CmAcqdate IS NOT NULL
     THEN ':98A::WUCO//'+CONVERT ( varchar , CmAcqdate,112)
     ELSE ':98B::WUCO//UKWN'
     END,
CASE WHEN UnconditionalDate IS NOT NULL and UnconditionalDate > getdate()
     THEN ':22F::ESTA//SUAP'
     ELSE ':22F::ESTA//UNAC'
     END,
case when wca.dbo.MPAY.eventid is not null
     then ':22F::OFFE//DISS'
     else ''
     end,
case when maintab.Offerorname is not null
     then ':70E::OFFO//'+replace(substring(maintab.Offerorname,1,23),'�','e')
     else ''
     end as TIDYTEXT,
':16S:CADETL',
':16R:CAOPTN' as StartseqE,
'-}$'
From v_EQ_TEND_TKOVR as maintab
left outer join wca.dbo.MPAY on maintab.eventid = wca.dbo.MPAY.eventid and 'TKOVR' = wca.dbo.MPAY.sevent
                                             and 'D'=wca.dbo.MPAY.paytype and 'D'<>wca.dbo.MPAY.actflag
/* left outer join client.dbo.seme_numis as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'CANC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'NOOF'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (maintab.minitkovr='T'
     or (maintab.Actflag='D'))
and (((maintab.isin in (select code from client.dbo.pfisin where accid=123 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=123 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=123 and actflag='I'))
and (Closedate > getdate() or CmAcqdate > getdate()))
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=123 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=123 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=123 and actflag='U'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
order by caref2 desc, caref1, caref3
