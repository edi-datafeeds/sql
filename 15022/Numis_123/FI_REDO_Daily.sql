--SEMETABLE=client.dbo.seme_numis
--FileName=edi_YYYYMMDD
--YYYYMMDD_FI564_REDO
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(CurrdNotes as varchar(24)))=22 then rtrim(char(32)) else CurrdNotes end As Notes FROM CURRD WHERE CurrdID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01NUSEGB21XXXO0300000054}{2:I564EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.EffectiveDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.EffectiveDate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '220' END as CAref1, */
'150' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//REDO' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID,'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN oldparvalue<>'' THEN ':36B::MINO//FAMT/'+oldparvalue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.EffectiveDate <>''
     THEN ':98A::EFFD//'+CONVERT ( varchar , maintab.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':36B::NEWD//UNIT/'+newparvalue as commasub,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN maintab.mainTFB1 <>''
     THEN ':35B:' + mainTFB1
     ELSE ':35B:UKWN'
     END,
':16R:FIA',
':94B::PLIS//SECM',
':11A::DENO//'+NewCurenCD,
':16S:FIA',
':16S:SECMOVE',
':16S:CAOPTN',
'-}$'
From v_FI_REDO as maintab
/* left outer join client.dbo.seme_numis as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'REDO'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=123 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=123 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=123 and actflag='I'))
and Effectivedate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=123 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=123 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=123 and actflag='U'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and OldParvalue <>''
and NewParvalue <>''
and OldCurenCD <>''
and NewCurenCD <>''
and Actflag  <>''
order by caref2 desc, caref1, caref3
