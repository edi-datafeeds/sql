--SEMETABLE=client.dbo.seme_numis
--FileName=edi_YYYYMMDD
-- YYYYMMDD_FI564_INTR
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog 
--Archive=off
--ArchivePath=n:\
--FileTidy=N
--TFBNUMBER=1
--INCREMENTAL=ON
--NOTESLIMIT=
--MESSTERMINATOR=
--TEXTCLEAN=2

--# 1
use wca
select distinct
changed,
'select case when len(cast(IntNotes as varchar(24)))=22 then rtrim(char(32)) else IntNotes end As Notes FROM INT WHERE RdID = '+ cast(EventID as char(16)) as ChkNotes,
'{1:F01NUSEGB21XXXO0300000054}{2:I564EDISGB2LAXXXA2333}{4:' as ISOHDR,
':16R:GENL',
/* CASE WHEN maintab.InterestToDate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.InterestToDate is not null
              and SR.TFB<>':35B:'+mainTFB1
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '200' END as CAref1, */
'150' as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN Actflag = 'U'
     THEN ':23G:REPL'
     WHEN Actflag = 'D'
     THEN ':23G:CANC'
     WHEN Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INTR' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , changed,112) + replace(convert ( varchar, changed, 08),':','') as prep,
':25D::PROC//COMP', '' as PSRID, '' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:' + mainTFB1 as TFB,
case when len(Issuername)>35
     then substring(Issuername,1,35)
     else Issuername 
     end as TIDYTEXT,
case when len(Issuername)>35
     then replace(substring(Issuername,36,35),'&','and')
     else ''
     end,
case when bonddesc1<>''
     then substring(bonddesc1+' '+bonddesc2,1,35)
     else substring(bonddesc2,1,35)
     end as TIDYTEXT,
replace(substring(bonddesc3,1,35),'`',' '),
':16R:FIA',
CASE WHEN MCD='' THEN ':94B::PLIS//SECM' ELSE ':94B::PLIS//EXCH/' + MCD END,
/*
case when debtcurrency='USD' then ':22F::MICO//A006' else '' end,
*/
CASE WHEN len(cfi)=6 THEN ':12C::CLAS//'+cfi ELSE '' END,
case when debtcurrency<>'' then ':11A::DENO//'+debtcurrency else '' end,
case when MaturityDate is not null then ':98A::MATU//'+CONVERT(varchar, MaturityDate,112) else '' end,
CASE WHEN ParValue<>'' THEN ':36B::MINO//FAMT/'+ParValue ELSE '' END as commasub,
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN ExDate <>''
     THEN ':98A::XDTE//'+CONVERT ( varchar , Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN DuebillsRedemDate IS NOT NULL
     THEN ':98A::MCTD//'+CONVERT ( varchar , DuebillsRedemDate,112)
     ELSE ''
     END,
CASE WHEN RecDate <>''
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN InterestFromdate <>'' AND InterestTodate <>''
     THEN ':69A::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/'
          +CONVERT ( varchar , InterestToDate,112)
     WHEN InterestFromDate <>''
     THEN ':69C::INPE//'
          +CONVERT ( varchar , InterestFromDate,112)+'/UKWN'
     WHEN InterestToDate <>''
     THEN ':69E::INPE//UKWN/'
          +CONVERT ( varchar , InterestToDate,112)
     ELSE ':69J::INPE//UKWN'
     END,
case when debtcurrency<>'USD' and days is not null
     then ':99A:DAAC//'+cast(days as varchar(20))+','
     else ''
     end,
CASE WHEN AnlCoupRate <> ''
     THEN ':92A::INTR//' + AnlCoupRate
     ELSE ':92K::INTR//UKWN'
     END as COMMASUB,
case when denomination1<>''
     then ':36B::NEWD//FAMT/'+denomination1
     else ''
     end as COMMASUB,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN CurenCD <> ''
     THEN ':11A::OPTN//' + CurenCD 
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN Paydate <>''
     THEN ':98A::PAYD//' + CONVERT ( varchar , PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN Nilint='T'
     THEN ':92K::INTP//NILP'
     WHEN InterestPaymentFrequency = 'ANL'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + AnlCoupRate
     WHEN InterestPaymentFrequency = 'SMA'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/2 as char(18))
     WHEN InterestPaymentFrequency = 'QTR'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/4 as char(18))
     WHEN InterestPaymentFrequency = 'MNT'
           AND AnlCoupRate <> ''
     THEN ':92A::INTP//' + cast(cast(AnlCoupRate as decimal(18,9))/12 as char(18))
     ELSE ':92K::INTP//UKWN'
     END as COMMASUB,
CASE WHEN maintab.GrossInterest<>'' and maintab.GrossInterest<>'0'
     THEN ':92F::GRSS//'+ maintab.CurenCD
     ELSE ''
     END as NOCRLF1,
CASE WHEN maintab.GrossInterest<>'' and maintab.GrossInterest<>'0'
     THEN cast(cast(maintab.GrossInterest as float) as varchar)
     ELSE ''
     END as COMMASUB,
CASE WHEN maintab.GrossInterest='' or maintab.GrossInterest='0'
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
':16S:CASHMOVE',
':16S:CAOPTN',
'-}$'
From v_FI_INTR as maintab
/* left outer join client.dbo.seme_numis as SR on maintab.eventid=SR.caref2
                                         and maintab.secid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'INTR'=SR.CAEVMAIN */
WHERE
mainTFB1<>''
and (((maintab.isin in (select code from client.dbo.pfisin where accid=123 and actflag='I')
      or maintab.uscode in (select code from client.dbo.pfuscode where accid=123 and actflag='I')
      or maintab.sedol in (select code from client.dbo.pfsedol where accid=123 and actflag='I'))
and Recdate > getdate())
OR
((maintab.isin in (select code from client.dbo.pfisin where accid=123 and actflag='U')
or maintab.uscode in (select code from client.dbo.pfuscode where accid=123 and actflag='U')
or maintab.sedol in (select code from client.dbo.pfsedol where accid=123 and actflag='U'))
and changed>(select max(acttime)-0.05 from wca.dbo.tbl_opslog)))
and (maintab.primaryexchgcd=maintab.exchgcd or maintab.primaryexchgcd ='')
and (indefpay='' or indefpay = 'P')
order by caref2 desc, caref1, caref3
