Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022DECR
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_DECR.Changed,
'' as ISOHDR,
':16R:GENL',
'148'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_DECR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_DECR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_DECR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_DECR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DECR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
CASE WHEN tISO_DECR.OldSid <> '' and tISO_DECR.OldSidName = 'ISIN'
     THEN ':35B:' + tISO_DECR.OldSidname +' '+ tISO_DECR.OldSid
     WHEN tISO_DECR.OldSid <> '' and tISO_DECR.SidName = '/GB/'
     THEN ':35B:' + tISO_DECR.OldSidname +' '+ tISO_DECR.OldSid
     ELSE ':35B:' + tISO_DECR.Sidname +' '+ tISO_DECR.Sid
     END,
substring(tISO_DECR.Issuername,1,35) as TIDYTEXT,
substring(tISO_DECR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_DECR.Sedol <> '' and tISO_DECR.Sidname <> '/GB/'
     THEN '/GB/' + tISO_DECR.Sedol
     ELSE ''
     END,
CASE WHEN tISO_DECR.Localcode <> ''
     THEN '/TS/' + tISO_DECR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_DECR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_DECR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_DECR.EffectiveDate <>'' and tISO_DECR.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_DECR.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  CapRdNotes As Notes FROM CAPRD WHERE CAPRDID = '+ cast(tISO_DECR.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_DECR
WHERE 
(SID <> '' or OldSID <> '')
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
