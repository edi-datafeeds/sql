Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022PARI
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_PARI.Changed,
'' as ISOHDR,
':16R:GENL',
'124'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_PARI.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_PARI.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_PARI.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_PARI.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PARI',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_PARI.Sidname +' '+ tISO_PARI.Sid,
substring(tISO_PARI.Issuername,1,35) as TIDYTEXT,
substring(tISO_PARI.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_PARI.Sedol <> '' and tISO_PARI.Sidname <> '/GB/'
     THEN '/GB/' + tISO_PARI.Sedol
     ELSE ''
     END,
CASE WHEN tISO_PARI.Localcode <> ''
     THEN '/TS/' + tISO_PARI.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_PARI.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_PARI.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_PARI.AssimilationDate <>'' and tISO_PARI.AssimilationDate is not null
     THEN ':98A::PPDT//'+CONVERT ( varchar , tISO_PARI.AssimilationDate,112)
     ELSE ':98B::PPDT//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_PARI.Ressid <> ''
     THEN ':35B:' + tISO_PARI.Ressidname +' '+ tISO_PARI.Ressid
     ELSE ':35B:ISIN UKWN'
     END,
/* CASE WHEN tISO_PARI.RatioNew <>''
            AND tISO_PARI.RatioNew IS NOT NULL
     THEN ':92D::NEWO//'+rtrim(cast(tISO_PARI.RatioNew as char (15)))+
                    '/'+rtrim(cast(tISO_PARI.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB, 
next line is a placebo, needs a ratioold,rationew added to parent */
':92K::NEWO//UKWN',
CASE WHEN tISO_PARI.AssimilationDate <>'' and tISO_PARI.AssimilationDate is not null
     THEN ':98A::PPDT//'+CONVERT ( varchar , tISO_PARI.AssimilationDate,112)
     ELSE ':98B::PPDT//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Intend to put in notes in next revision
populated by rendering programme :70E::TXNR// 
'select  LawstNotes As Notes FROM LAWST WHERE LAWSTID = '+ cast(tISO_CLSA.EventID as char(16)) as Notes,  */
':16S:ADDINFO',
'-}$'
From tISO_PARI
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
