Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022EXWA
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_EXWA.Changed,
'' as ISOHDR,
':16R:GENL',
'300'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_EXWA.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_EXWA.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_EXWA.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_EXWA.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXWA',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_EXWA.Sidname +' '+ tISO_EXWA.Sid,
substring(tISO_EXWA.Issuername,1,35) as TIDYTEXT,
substring(tISO_EXWA.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_EXWA.Sedol <> '' and tISO_EXWA.Sidname <> '/GB/'
     THEN '/GB/' + tISO_EXWA.Sedol
     ELSE ''
     END,
CASE WHEN tISO_EXWA.Localcode <> ''
     THEN '/TS/' + tISO_EXWA.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_EXWA.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_EXWA.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_EXWA.ExpirationDate <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , tISO_EXWA.ExpirationDate,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN tISO_EXWA.ToDate <>'' AND tISO_EXWA.ToDate IS NOT NULL
       AND tISO_EXWA.FromDate <>'' AND tISO_EXWA.FromDate IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , tISO_EXWA.FromDate,112)+'/'
          +CONVERT ( varchar , tISO_EXWA.ToDate,112)
     WHEN tISO_EXWA.FromDate <>'' AND tISO_EXWA.FromDate IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , tISO_EXWA.FromDate,112)+'/UKWN'
     WHEN tISO_EXWA.ToDate <>'' AND tISO_EXWA.ToDate IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , tISO_EXWA.ToDate,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN tISO_EXWA.PricePerShare <> ''
     THEN ':90B::MRKT//ACTU/'+CurenCD+rtrim(cast(tISO_EXWA.PricePerShare as char(15)))
     ELSE ':90E::MRKT//UKWN'
     END as COMMASUB,
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN tISO_EXWA.CurenCD <> '' 
     THEN ':11A::OPTN//' + tISO_EXWA.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN tISO_EXWA.StrikePrice <> ''
     THEN ':90B::EXER//ACTU/'+CurenCD+rtrim(cast(tISO_EXWA.StrikePrice as char(15)))
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_EXWA.ExerSid <> ''
     THEN ':35B:' + tISO_EXWA.ExerSidname +' '+ tISO_EXWA.ExerSid
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_EXWA.RatioNew <>''
     THEN ':92D::NEWO//'+rtrim(cast(tISO_EXWA.RatioNew as char (15)))+
                    '/'+rtrim(cast(tISO_EXWA.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN tISO_EXWA.Paydate <>'' 
               AND tISO_EXWA.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_EXWA.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
/* EXERCISE OPTION END */
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  WartmNotes As Notes FROM WARTM WHERE SECID = '+ cast(tISO_EXWA.SecID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_EXWA
WHERE 
SID <> ''
and EventID is not null
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
