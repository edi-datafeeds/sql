Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022DVOP
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_DVOP.Changed,
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_DVOP.Actflag_1 = ''
     THEN ':23G:CANC'
     WHEN tISO_DVOP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_DVOP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_DVOP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_DVOP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVOP',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_DVOP.Sidname +' '+ tISO_DVOP.Sid,
substring(tISO_DVOP.Issuername,1,35) as TIDYTEXT,
substring(tISO_DVOP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_DVOP.Sedol <> '' and tISO_DVOP.Sidname <> '/GB/'
     THEN '/GB/' + tISO_DVOP.Sedol
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Localcode <> ''
     THEN '/TS/' + tISO_DVOP.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_DVOP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_DVOP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_DVOP.Exdate <> '' 
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_DVOP.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN tISO_DVOP.Recdate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_DVOP.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN tISO_DVOP.DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN tISO_DVOP.DivPeriodCD='UN' OR  tISO_DVOP.DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN tISO_DVOP.DivPeriodCD='SPL' OR  tISO_DVOP.DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     ELSE ':22F::DIVI//REGR'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_DVOP.RdID as char(16)) as Notes, 
':16S:CADETL',
/* CASH OPTION START_1 */
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
     THEN ':13A::CAON//001'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 <> 'C'
     THEN ''
     WHEN tISO_DVOP.CurenCD_1 <> '' AND tISO_DVOP.CurenCD_1 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_1
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
     THEN ':17B::DFLT//Y'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
           AND tISO_DVOP.Paydate <> '' 
              AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_1 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
            AND  tISO_DVOP.Netdividend_1 <> ''
     THEN ':92F::NETT//'+CurenCD_1+ltrim(cast(tISO_DVOP.Netdividend_1 as char(15)))
     WHEN tISO_DVOP.Divtype_1 = 'C'
            AND  tISO_DVOP.Netdividend_1 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
            AND tISO_DVOP.Grossdividend_1 <> ''
     THEN ':92F::GRSS//'+CurenCD_1+ltrim(cast(tISO_DVOP.Grossdividend_1 as char(15)))
     WHEN tISO_DVOP.Divtype_1 = 'C'
            AND tISO_DVOP.Grossdividend_1 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
           AND tISO_DVOP.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_1 as char(15))
     WHEN tISO_DVOP.Divtype_1 = 'C'
           AND tISO_DVOP.Taxrate_1 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_1 */
/* STOCK OPTION START_1 */
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
          AND tISO_DVOP.ResSecID_1 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_1
            AND tISO_DVOP.ResSid_1 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_1 +' '+ tISO_DVOP.ResSID_1
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd1,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
            AND  tISO_DVOP.RatioNew_1 <> ''
            AND  tISO_DVOP.RatioOld_1 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_1,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_1,1,15)
     WHEN tISO_DVOP.Divtype_1 = 'S'
            AND  tISO_DVOP.RatioNew_1 <> ''
            AND  tISO_DVOP.RatioOld_1 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_1,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_1 = 'S'
            AND  tISO_DVOP.RatioNew_1 =''
            AND  tISO_DVOP.RatioOld_1 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_1,1,15)
     WHEN tISO_DVOP.Divtype_1 = 'S'
            AND  tISO_DVOP.RatioNew_1 = ''
              AND  tISO_DVOP.RatioOld_1 =''
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_1 = 'S'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_1 */
/* BOTH OPTION START_1 */
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':13A::CAON//001'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 <> 'B'
     THEN ''
     WHEN tISO_DVOP.CurenCD_1 <> '' AND tISO_DVOP.CurenCD_1 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_1
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
           AND tISO_DVOP.Paydate <> '' 
             AND tISO_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.Paydate,112)
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND  tISO_DVOP.Netdividend_1 <> ''
     THEN ':92F::NETT//'+CurenCD_1+ltrim(cast(tISO_DVOP.Netdividend_1 as char(15)))
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND  tISO_DVOP.Netdividend_1 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND tISO_DVOP.Grossdividend_1 <> ''
     THEN ':92F::GRSS//'+CurenCD_1+ltrim(cast(tISO_DVOP.Grossdividend_1 as char(15)))
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND tISO_DVOP.Grossdividend_1 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
           AND tISO_DVOP.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_1 as char(15))
     WHEN tISO_DVOP.Divtype_1 = 'B'
           AND tISO_DVOP.Taxrate_1 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
          AND tISO_DVOP.ResSecID_1 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_1
            AND tISO_DVOP.ResSid_1 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_1 +' '+ tISO_DVOP.ResSID_1
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd1,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND  tISO_DVOP.RatioNew_1 <> ''
            AND  tISO_DVOP.RatioOld_1 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_1,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_1,1,15)
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND  tISO_DVOP.RatioNew_1 <> ''
            AND  tISO_DVOP.RatioOld_1 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_1,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND  tISO_DVOP.RatioNew_1 =''
            AND  tISO_DVOP.RatioOld_1 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_1,1,15)
     WHEN tISO_DVOP.Divtype_1 = 'B'
            AND  tISO_DVOP.RatioNew_1 = ''
              AND  tISO_DVOP.RatioOld_1 =''
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_1 = 'B'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_1 = 'D' OR tISO_DVOP.Divtype_1 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_1 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_1 */
/* CASH OPTION START_2 */
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
     THEN ':13A::CAON//002'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 <> 'C'
     THEN ''
     WHEN tISO_DVOP.CurenCD_2 <> '' AND tISO_DVOP.CurenCD_2 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_2
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
     THEN ':17B::DFLT//Y'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
           AND tISO_DVOP.Paydate <> '' 
              AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_2 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
            AND  tISO_DVOP.Netdividend_2 <> ''
     THEN ':92F::NETT//'+CurenCD_2+ltrim(cast(tISO_DVOP.Netdividend_2 as char(15)))
     WHEN tISO_DVOP.Divtype_2 = 'C'
            AND  tISO_DVOP.Netdividend_2 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
            AND tISO_DVOP.Grossdividend_2 <> ''
     THEN ':92F::GRSS//'+CurenCD_2+ltrim(cast(tISO_DVOP.Grossdividend_2 as char(15)))
     WHEN tISO_DVOP.Divtype_2 = 'C'
            AND tISO_DVOP.Grossdividend_2 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
           AND tISO_DVOP.Taxrate_2 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_2 as char(15))
     WHEN tISO_DVOP.Divtype_2 = 'C'
           AND tISO_DVOP.Taxrate_2 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_2 */
/* STOCK OPTION START_2 */
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':13A::CAON//002'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
          AND tISO_DVOP.ResSecID_2 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_2
            AND tISO_DVOP.ResSid_2 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_2 +' '+ tISO_DVOP.ResSID_2
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd2,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
            AND  tISO_DVOP.RatioNew_2 <> ''
            AND  tISO_DVOP.RatioOld_2 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_2,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_2,1,15)
     WHEN tISO_DVOP.Divtype_2 = 'S'
            AND  tISO_DVOP.RatioNew_2 <> ''
            AND  tISO_DVOP.RatioOld_2 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_2,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_2 = 'S'
            AND  tISO_DVOP.RatioNew_2 =''
            AND  tISO_DVOP.RatioOld_2 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_2,1,15)
     WHEN tISO_DVOP.Divtype_2 = 'S'
            AND  tISO_DVOP.RatioNew_2 = ''
              AND  tISO_DVOP.RatioOld_2 =''
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_2 = 'S'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_2 */
/* BOTH OPTION START_2 */
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':13A::CAON//002'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 <> 'B'
     THEN ''
     WHEN tISO_DVOP.CurenCD_2 <> '' AND tISO_DVOP.CurenCD_2 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_2
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
           AND tISO_DVOP.Paydate <> '' 
             AND tISO_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.Paydate,112)
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND  tISO_DVOP.Netdividend_2 <> ''
     THEN ':92F::NETT//'+CurenCD_2+ltrim(cast(tISO_DVOP.Netdividend_2 as char(15)))
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND  tISO_DVOP.Netdividend_2 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND tISO_DVOP.Grossdividend_2 <> ''
     THEN ':92F::GRSS//'+CurenCD_2+ltrim(cast(tISO_DVOP.Grossdividend_2 as char(15)))
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND tISO_DVOP.Grossdividend_2 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
           AND tISO_DVOP.Taxrate_2 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_2 as char(15))
     WHEN tISO_DVOP.Divtype_2 = 'B'
           AND tISO_DVOP.Taxrate_2 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
          AND tISO_DVOP.ResSecID_2 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_2
            AND tISO_DVOP.ResSid_2 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_2 +' '+ tISO_DVOP.ResSID_2
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd2,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND  tISO_DVOP.RatioNew_2 <> ''
            AND  tISO_DVOP.RatioOld_2 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_2,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_2,1,15)
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND  tISO_DVOP.RatioNew_2 <> ''
            AND  tISO_DVOP.RatioOld_2 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_2,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND  tISO_DVOP.RatioNew_2 =''
            AND  tISO_DVOP.RatioOld_2 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_2,1,15)
     WHEN tISO_DVOP.Divtype_2 = 'B'
            AND  tISO_DVOP.RatioNew_2 = ''
              AND  tISO_DVOP.RatioOld_2 =''
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_2 = 'B'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_2 = 'D' OR tISO_DVOP.Divtype_2 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_2 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_2 */
/* CASH OPTION START_3 */
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
     THEN ':13A::CAON//003'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 <> 'C'
     THEN ''
     WHEN tISO_DVOP.CurenCD_3 <> '' AND tISO_DVOP.CurenCD_3 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_3
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
     THEN ':17B::DFLT//Y'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
           AND tISO_DVOP.Paydate <> '' 
              AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_3 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
            AND  tISO_DVOP.Netdividend_3 <> ''
     THEN ':92F::NETT//'+CurenCD_3+ltrim(cast(tISO_DVOP.Netdividend_3 as char(15)))
     WHEN tISO_DVOP.Divtype_3 = 'C'
            AND  tISO_DVOP.Netdividend_3 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
            AND tISO_DVOP.Grossdividend_3 <> ''
     THEN ':92F::GRSS//'+CurenCD_3+ltrim(cast(tISO_DVOP.Grossdividend_3 as char(15)))
     WHEN tISO_DVOP.Divtype_3 = 'C'
            AND tISO_DVOP.Grossdividend_3 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
           AND tISO_DVOP.Taxrate_3 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_3 as char(15))
     WHEN tISO_DVOP.Divtype_3 = 'C'
           AND tISO_DVOP.Taxrate_3 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_3 */
/* STOCK OPTION START_3 */
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':13A::CAON//003'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
          AND tISO_DVOP.ResSecID_3 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_3
            AND tISO_DVOP.ResSid_3 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_3 +' '+ tISO_DVOP.ResSID_3
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd3,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
            AND  tISO_DVOP.RatioNew_3 <> ''
            AND  tISO_DVOP.RatioOld_3 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_3,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_3,1,15)
     WHEN tISO_DVOP.Divtype_3 = 'S'
            AND  tISO_DVOP.RatioNew_3 <> ''
            AND  tISO_DVOP.RatioOld_3 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_3,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_3 = 'S'
            AND  tISO_DVOP.RatioNew_3 =''
            AND  tISO_DVOP.RatioOld_3 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_3,1,15)
     WHEN tISO_DVOP.Divtype_3 = 'S'
            AND  tISO_DVOP.RatioNew_3 = ''
              AND  tISO_DVOP.RatioOld_3 =''
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_3 = 'S'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_3 */
/* BOTH OPTION START_3 */
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':13A::CAON//003'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 <> 'B'
     THEN ''
     WHEN tISO_DVOP.CurenCD_3 <> '' AND tISO_DVOP.CurenCD_3 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_3
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
           AND tISO_DVOP.Paydate <> '' 
             AND tISO_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.Paydate,112)
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND  tISO_DVOP.Netdividend_3 <> ''
     THEN ':92F::NETT//'+CurenCD_3+ltrim(cast(tISO_DVOP.Netdividend_3 as char(15)))
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND  tISO_DVOP.Netdividend_3 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND tISO_DVOP.Grossdividend_3 <> ''
     THEN ':92F::GRSS//'+CurenCD_3+ltrim(cast(tISO_DVOP.Grossdividend_3 as char(15)))
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND tISO_DVOP.Grossdividend_3 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
           AND tISO_DVOP.Taxrate_3 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_3 as char(15))
     WHEN tISO_DVOP.Divtype_3 = 'B'
           AND tISO_DVOP.Taxrate_3 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
          AND tISO_DVOP.ResSecID_3 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_3
            AND tISO_DVOP.ResSid_3 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_3 +' '+ tISO_DVOP.ResSID_3
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd3,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND  tISO_DVOP.RatioNew_3 <> ''
            AND  tISO_DVOP.RatioOld_3 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_3,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_3,1,15)
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND  tISO_DVOP.RatioNew_3 <> ''
            AND  tISO_DVOP.RatioOld_3 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_3,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND  tISO_DVOP.RatioNew_3 =''
            AND  tISO_DVOP.RatioOld_3 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_3,1,15)
     WHEN tISO_DVOP.Divtype_3 = 'B'
            AND  tISO_DVOP.RatioNew_3 = ''
              AND  tISO_DVOP.RatioOld_3 =''
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_3 = 'B'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_3 = 'D' OR tISO_DVOP.Divtype_3 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_3 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_3 */
/* CASH OPTION START_4 */
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
     THEN ':13A::CAON//004'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 <> 'C'
     THEN ''
     WHEN tISO_DVOP.CurenCD_4 <> '' AND tISO_DVOP.CurenCD_4 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_4
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
     THEN ':17B::DFLT//Y'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
           AND tISO_DVOP.Paydate <> '' 
              AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_4 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
            AND  tISO_DVOP.Netdividend_4 <> ''
     THEN ':92F::NETT//'+CurenCD_4+ltrim(cast(tISO_DVOP.Netdividend_4 as char(15)))
     WHEN tISO_DVOP.Divtype_4 = 'C'
            AND  tISO_DVOP.Netdividend_4 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
            AND tISO_DVOP.Grossdividend_4 <> ''
     THEN ':92F::GRSS//'+CurenCD_4+ltrim(cast(tISO_DVOP.Grossdividend_4 as char(15)))
     WHEN tISO_DVOP.Divtype_4 = 'C'
            AND tISO_DVOP.Grossdividend_4 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
           AND tISO_DVOP.Taxrate_4 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_4 as char(15))
     WHEN tISO_DVOP.Divtype_4 = 'C'
           AND tISO_DVOP.Taxrate_4 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_4 */
/* STOCK OPTION START_4 */
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':13A::CAON//004'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
          AND tISO_DVOP.ResSecID_4 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_4
            AND tISO_DVOP.ResSid_4 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_4 +' '+ tISO_DVOP.ResSID_4
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd3,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
            AND  tISO_DVOP.RatioNew_4 <> ''
            AND  tISO_DVOP.RatioOld_4 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_4,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_4,1,15)
     WHEN tISO_DVOP.Divtype_4 = 'S'
            AND  tISO_DVOP.RatioNew_4 <> ''
            AND  tISO_DVOP.RatioOld_4 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_4,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_4 = 'S'
            AND  tISO_DVOP.RatioNew_4 =''
            AND  tISO_DVOP.RatioOld_4 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_4,1,15)
     WHEN tISO_DVOP.Divtype_4 = 'S'
            AND  tISO_DVOP.RatioNew_4 = ''
              AND  tISO_DVOP.RatioOld_4 =''
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_4 = 'S'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_4 */
/* BOTH OPTION START_4 */
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':13A::CAON//004'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 <> 'B'
     THEN ''
     WHEN tISO_DVOP.CurenCD_4 <> '' AND tISO_DVOP.CurenCD_4 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_4
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
           AND tISO_DVOP.Paydate <> '' 
             AND tISO_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.Paydate,112)
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND  tISO_DVOP.Netdividend_4 <> ''
     THEN ':92F::NETT//'+CurenCD_4+ltrim(cast(tISO_DVOP.Netdividend_4 as char(15)))
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND  tISO_DVOP.Netdividend_4 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND tISO_DVOP.Grossdividend_4 <> ''
     THEN ':92F::GRSS//'+CurenCD_4+ltrim(cast(tISO_DVOP.Grossdividend_4 as char(15)))
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND tISO_DVOP.Grossdividend_4 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
           AND tISO_DVOP.Taxrate_4 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_4 as char(15))
     WHEN tISO_DVOP.Divtype_4 = 'B'
           AND tISO_DVOP.Taxrate_4 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
          AND tISO_DVOP.ResSecID_4 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_4
            AND tISO_DVOP.ResSid_4 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_4 +' '+ tISO_DVOP.ResSID_4
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd3,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND  tISO_DVOP.RatioNew_4 <> ''
            AND  tISO_DVOP.RatioOld_4 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_4,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_4,1,15)
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND  tISO_DVOP.RatioNew_4 <> ''
            AND  tISO_DVOP.RatioOld_4 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_4,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND  tISO_DVOP.RatioNew_4 =''
            AND  tISO_DVOP.RatioOld_4 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_4,1,15)
     WHEN tISO_DVOP.Divtype_4 = 'B'
            AND  tISO_DVOP.RatioNew_4 = ''
              AND  tISO_DVOP.RatioOld_4 =''
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_4 = 'B'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_4 = 'D' OR tISO_DVOP.Divtype_4 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_4 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_4 */
/* CASH OPTION START_5 */
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
     THEN ':13A::CAON//005'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 <> 'C'
     THEN ''
     WHEN tISO_DVOP.CurenCD_5 <> '' AND tISO_DVOP.CurenCD_5 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_5
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
     THEN ':17B::DFLT//Y'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
           AND tISO_DVOP.Paydate <> '' 
              AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_5 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
            AND  tISO_DVOP.Netdividend_5 <> ''
     THEN ':92F::NETT//'+CurenCD_5+ltrim(cast(tISO_DVOP.Netdividend_5 as char(15)))
     WHEN tISO_DVOP.Divtype_5 = 'C'
            AND  tISO_DVOP.Netdividend_5 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
            AND tISO_DVOP.Grossdividend_5 <> ''
     THEN ':92F::GRSS//'+CurenCD_5+ltrim(cast(tISO_DVOP.Grossdividend_5 as char(15)))
     WHEN tISO_DVOP.Divtype_5 = 'C'
            AND tISO_DVOP.Grossdividend_5 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
           AND tISO_DVOP.Taxrate_5 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_5 as char(15))
     WHEN tISO_DVOP.Divtype_5 = 'C'
           AND tISO_DVOP.Taxrate_5 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_5 */
/* STOCK OPTION START_5 */
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':13A::CAON//005'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
          AND tISO_DVOP.ResSecID_5 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_5
            AND tISO_DVOP.ResSid_5 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_5 +' '+ tISO_DVOP.ResSID_5
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
            AND  tISO_DVOP.RatioNew_5 <> ''
            AND  tISO_DVOP.RatioOld_5 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_5,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_5,1,15)
     WHEN tISO_DVOP.Divtype_5 = 'S'
            AND  tISO_DVOP.RatioNew_5 <> ''
            AND  tISO_DVOP.RatioOld_5 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_5,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_5 = 'S'
            AND  tISO_DVOP.RatioNew_5 =''
            AND  tISO_DVOP.RatioOld_5 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_5,1,15)
     WHEN tISO_DVOP.Divtype_5 = 'S'
            AND  tISO_DVOP.RatioNew_5 = ''
              AND  tISO_DVOP.RatioOld_5 =''
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_5 = 'S'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_5 */
/* BOTH OPTION START_5 */
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':13A::CAON//005'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 <> 'B'
     THEN ''
     WHEN tISO_DVOP.CurenCD_5 <> '' AND tISO_DVOP.CurenCD_5 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_5
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
           AND tISO_DVOP.Paydate <> '' 
             AND tISO_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.Paydate,112)
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND  tISO_DVOP.Netdividend_5 <> ''
     THEN ':92F::NETT//'+CurenCD_5+ltrim(cast(tISO_DVOP.Netdividend_5 as char(15)))
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND  tISO_DVOP.Netdividend_5 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND tISO_DVOP.Grossdividend_5 <> ''
     THEN ':92F::GRSS//'+CurenCD_5+ltrim(cast(tISO_DVOP.Grossdividend_5 as char(15)))
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND tISO_DVOP.Grossdividend_5 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
           AND tISO_DVOP.Taxrate_5 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_5 as char(15))
     WHEN tISO_DVOP.Divtype_5 = 'B'
           AND tISO_DVOP.Taxrate_5 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
          AND tISO_DVOP.ResSecID_5 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_5
            AND tISO_DVOP.ResSid_5 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_5 +' '+ tISO_DVOP.ResSID_5
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd5,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND  tISO_DVOP.RatioNew_5 <> ''
            AND  tISO_DVOP.RatioOld_5 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_5,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_5,1,15)
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND  tISO_DVOP.RatioNew_5 <> ''
            AND  tISO_DVOP.RatioOld_5 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_5,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND  tISO_DVOP.RatioNew_5 =''
            AND  tISO_DVOP.RatioOld_5 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_5,1,15)
     WHEN tISO_DVOP.Divtype_5 = 'B'
            AND  tISO_DVOP.RatioNew_5 = ''
              AND  tISO_DVOP.RatioOld_5 =''
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_5 = 'B'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_5 = 'D' OR tISO_DVOP.Divtype_5 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_5 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_5 */
/* CASH OPTION START_6 */
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
     THEN ':16R:CAOPTN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
     THEN ':13A::CAON//006'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
     THEN ':22F::CAOP//CASH'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 <> 'C'
     THEN ''
     WHEN tISO_DVOP.CurenCD_6 <> '' AND tISO_DVOP.CurenCD_6 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_6
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
     THEN ':17B::DFLT//Y'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
           AND tISO_DVOP.Paydate <> '' 
              AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_6 = 'C'
     THEN ':98B::PAYD//UKWN'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
            AND  tISO_DVOP.Netdividend_6 <> ''
     THEN ':92F::NETT//'+CurenCD_6+ltrim(cast(tISO_DVOP.Netdividend_6 as char(15)))
     WHEN tISO_DVOP.Divtype_6 = 'C'
            AND  tISO_DVOP.Netdividend_6 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
            AND tISO_DVOP.Grossdividend_6 <> ''
     THEN ':92F::GRSS//'+CurenCD_6+ltrim(cast(tISO_DVOP.Grossdividend_6 as char(15)))
     WHEN tISO_DVOP.Divtype_6 = 'C'
            AND tISO_DVOP.Grossdividend_6 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
           AND tISO_DVOP.Taxrate_6 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_6 as char(15))
     WHEN tISO_DVOP.Divtype_6 = 'C'
           AND tISO_DVOP.Taxrate_6 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'C'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* CASH OPTION END_6 */
/* STOCK OPTION START_6 */
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':13A::CAON//006'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':22F::CAOP//SECU'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':17B::DFLT//N'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':16R:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':22H::CRDB//CRED'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
          AND tISO_DVOP.ResSecID_6 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_6
            AND tISO_DVOP.ResSid_6 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_6 +' '+ tISO_DVOP.ResSID_6
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
            AND  tISO_DVOP.RatioNew_6 <> ''
            AND  tISO_DVOP.RatioOld_6 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_6,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_6,1,15)
     WHEN tISO_DVOP.Divtype_6 = 'S'
            AND  tISO_DVOP.RatioNew_6 <> ''
            AND  tISO_DVOP.RatioOld_6 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_6,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_6 = 'S'
            AND  tISO_DVOP.RatioNew_6 =''
            AND  tISO_DVOP.RatioOld_6 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_6,1,15)
     WHEN tISO_DVOP.Divtype_6 = 'S'
            AND  tISO_DVOP.RatioNew_6 = ''
              AND  tISO_DVOP.RatioOld_6 =''
     THEN ':92K::ADEX//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_6 = 'S'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'S'
     THEN ':16S:CAOPTN'
     ELSE ''
     END,
/* STOCK OPTION END_6 */
/* BOTH OPTION START_6 */
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':16R:CAOPTN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':13A::CAON//006'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':22F::CAOP//CASE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 <> 'B'
     THEN ''
     WHEN tISO_DVOP.CurenCD_6 <> '' AND tISO_DVOP.CurenCD_6 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_DVOP.CurenCD_6
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':17B::DFLT//N'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
           AND tISO_DVOP.Paydate <> '' 
             AND tISO_DVOP.Paydate IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.Paydate,112)
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':98B::PAYD//UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND  tISO_DVOP.Netdividend_6 <> ''
     THEN ':92F::NETT//'+CurenCD_6+ltrim(cast(tISO_DVOP.Netdividend_6 as char(15)))
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND  tISO_DVOP.Netdividend_6 = ''
     THEN ':92K::NETT//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND tISO_DVOP.Grossdividend_6 <> ''
     THEN ':92F::GRSS//'+CurenCD_6+ltrim(cast(tISO_DVOP.Grossdividend_6 as char(15)))
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND tISO_DVOP.Grossdividend_6 = ''
     THEN ':92K::GRSS//UKWN'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
           AND tISO_DVOP.Taxrate_6 <> ''
     THEN ':92A::TAXR//'+cast(tISO_DVOP.Taxrate_6 as char(15))
     WHEN tISO_DVOP.Divtype_6 = 'B'
           AND tISO_DVOP.Taxrate_6 = ''
     THEN ':92K::TAXR//UKWN'
     ELSE '' 
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':16R:SECMOVE'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':22H::CRDB//CRED'
     ELSE '' 
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
          AND tISO_DVOP.ResSecID_6 <> '' 
           AND tISO_DVOP.SecID <> tISO_DVOP.ResSecID_6
            AND tISO_DVOP.ResSid_6 <> ''
     THEN ':35B:' + tISO_DVOP.resSidname_6 +' '+ tISO_DVOP.ResSID_6
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':35B:ISIN UKWN'
     ELSE ''
     END as merd6,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND  tISO_DVOP.RatioNew_6 <> ''
            AND  tISO_DVOP.RatioOld_6 <> ''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_6,1,15)
                        +'/'+substring(tISO_DVOP.RatioOld_6,1,15)
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND  tISO_DVOP.RatioNew_6 <> ''
            AND  tISO_DVOP.RatioOld_6 =''
     THEN ':92D::ADEX//'+substring(tISO_DVOP.RatioNew_6,1,15)+'//UKWN'
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND  tISO_DVOP.RatioNew_6 =''
            AND  tISO_DVOP.RatioOld_6 <> ''
     THEN ':92K::ADEX//UKWN/'+substring(tISO_DVOP.RatioOld_6,1,15)
     WHEN tISO_DVOP.Divtype_6 = 'B'
            AND  tISO_DVOP.RatioNew_6 = ''
              AND  tISO_DVOP.RatioOld_6 =''
     THEN ':92K::ADEX//UKWN'
     ELSE ''
     END as COMMASUB,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
             AND tISO_DVOP.Paydate2 <> '' 
                AND tISO_DVOP.Paydate2 IS NOT NULL 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate2,112)
     WHEN tISO_DVOP.Divtype_6 = 'B'
             AND tISO_DVOP.Paydate <> '' 
                AND tISO_DVOP.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_DVOP.paydate,112)
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':98B::PAYD//UKWN' 
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':16S:SECMOVE'
     ELSE ''
     END,
CASE WHEN tISO_DVOP.Actflag_6 = 'D' OR tISO_DVOP.Divtype_6 = ''
     THEN ''
     WHEN tISO_DVOP.Divtype_6 = 'B'
     THEN ':16S:CAOPTN'
     ELSE '' 
     END,
/* BOTH OPTION END_6 */
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(tISO_DVOP.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_DVOP
WHERE 
SID <> ''
and Actflag <> ''
and Divtype_1 <> ''
/* START eliminate Cash only Stock only AND Both only */
AND (not (DIVTYPE_2 = ''
AND DIVTYPE_3 = ''
AND DIVTYPE_4 = ''
AND DIVTYPE_5 = ''
AND DIVTYPE_6 = '') or Divtype_1 = 'B')
/* END eliminate Cash option only */
AND not ((DIVTYPE_1 = 'C' OR DIVTYPE_1 = '')
     AND (DIVTYPE_2 = 'C' OR DIVTYPE_2 = '')
     AND (DIVTYPE_3 = 'C' OR DIVTYPE_3 = '')
     AND (DIVTYPE_4 = 'C' OR DIVTYPE_4 = '')
     AND (DIVTYPE_5 = 'C' OR DIVTYPE_5 = '')
     AND (DIVTYPE_6 = 'C' OR DIVTYPE_6 = ''))
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
