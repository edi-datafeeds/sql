Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022SOFF
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DIST

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca2
select   
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + ltrim(cast(tISO_SOFF_DIST.EventID as char(10))) + ' and sEvent = '+ char(39) + 'DIST'+ char(39)+ ' Order By OptionID, SerialID' as MPAYlink,
tISO_SOFF_DIST.Changed,
'' as ISOHDR,
':16R:GENL',
'136'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_SOFF_DIST.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_SOFF_DIST.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_SOFF_DIST.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_SOFF_DIST.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SOFF',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_SOFF_DIST.Sidname +' '+ tISO_SOFF_DIST.Sid,
substring(tISO_SOFF_DIST.Issuername,1,35) as TIDYTEXT,
substring(tISO_SOFF_DIST.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_SOFF_DIST.Sedol <> '' and tISO_SOFF_DIST.Sidname <> '/GB/'
     THEN '/GB/' + tISO_SOFF_DIST.Sedol
     ELSE ''
     END,
CASE WHEN tISO_SOFF_DIST.Localcode <> ''
     THEN '/TS/' + tISO_SOFF_DIST.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_SOFF_DIST.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_SOFF_DIST.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_SOFF_DIST.ExDate <> ''
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_SOFF_DIST.ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN tISO_SOFF_DIST.Recdate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_SOFF_DIST.Recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_SOFF_DIST.RDID as char(16)) as Notes, 
':16S:CADETL',
/* START OF STOCK BLOCK */
':16R:CAOPTN' as StartSeqE,
':13A::CAON//001' as SeqE,
':22F::CAOP//SECU' as SeqE,
':17B::DFLT//N' as SeqE,
CASE WHEN tISO_SOFF_DIST.Fractions_1 = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END as SeqE,
':16R:SECMOVE' as SeqE,
':22H::CRDB//CRED' as SeqE,
CASE WHEN tISO_SOFF_DIST.ResSID_1 <> ''
     THEN ':35B:' + tISO_SOFF_DIST.ResSIDname_1 +' '+ tISO_SOFF_DIST.ResSID_1
     ELSE ':35B:ISIN UKWN'
     END as SeqE,
CASE WHEN tISO_SOFF_DIST.RatioNew_1 <>''
     THEN ':92D::ADEX//'+ltrim(cast(tISO_SOFF_DIST.RatioNew_1 as char (15)))+
                    '/'+ltrim(cast(tISO_SOFF_DIST.RatioOld_1 as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as SeqE,
CASE WHEN tISO_SOFF_DIST.Paydate <> ''
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SOFF_DIST.Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END as SeqE,
':16S:SECMOVE' as SeqE,
':16S:CAOPTN' as SeqE,
/* END OF STOCK BLOCK */
/* START OF BOTH BLOCK */
':16R:CAOPTN' as SeqE,
':13A::CAON//001' as SeqE,
':22F::CAOP//CASE' as SeqE,
CASE WHEN tISO_SOFF_DIST.Fractions_1 = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_SOFF_DIST.Fractions_1 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END as SeqE,
CASE WHEN tISO_SOFF_DIST.CurenCD_1 <> '' AND tISO_SOFF_DIST.CurenCD_1 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_SOFF_DIST.CurenCD_1
     END as SeqE,
':17B::DFLT//N' as SeqE,
CASE WHEN tISO_SOFF_DIST.TndrStrkPrice_1 <>'' AND tISO_SOFF_DIST.TndrStrkPrice_1 IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_SOFF_DIST.CurenCD_1+
          +substring(tISO_SOFF_DIST.TndrStrkPrice_1,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as SeqE,
':16R:SECMOVE' as SeqE,
':22H::CRDB//CRED' as SeqE,
CASE WHEN tISO_SOFF_DIST.ResSID_1 <> ''
     THEN ':35B:' + tISO_SOFF_DIST.ResSIDname_1 +' '+ tISO_SOFF_DIST.ResSID_1
     ELSE ':35B:ISIN UKWN'
     END as SeqE,
CASE WHEN tISO_SOFF_DIST.RatioNew_1 <>''
     THEN ':92D::NEWO//'+ltrim(cast(tISO_SOFF_DIST.RatioNew_1 as char (15)))+
                    '/'+ltrim(cast(tISO_SOFF_DIST.RatioOld_1 as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as SeqE,
CASE WHEN tISO_SOFF_DIST.Paydate <> ''
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SOFF_DIST.Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END as SeqE,
':16S:SECMOVE' as SeqE,
':16R:CASHMOVE' as SeqE,
':22H::CRDB//CRED' as SeqE,
CASE WHEN tISO_SOFF_DIST.Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SOFF_DIST.Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END as SeqE,
':16S:CASHMOVE' as SeqE,
':16S:CAOPTN' as SeqE,
/* END OF BOTH BLOCK */
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  DistNotes As Notes FROM DIST WHERE RDID = '+ cast(tISO_SOFF_DIST.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_SOFF_DIST
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
