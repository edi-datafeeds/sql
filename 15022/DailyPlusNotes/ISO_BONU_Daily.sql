Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022BONU
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--FileTidy=N
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca2
select   
tISO_BONU.Changed,
'' as ISOHDR,
':16R:GENL',
'114'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
'' as SRID,
CASE WHEN tISO_BONU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_BONU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_BONU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_BONU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BONU',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_BONU.Sidname +' '+ tISO_BONU.Sid,
substring(tISO_BONU.Issuername,1,35) as TIDYTEXT,
substring(tISO_BONU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_BONU.Sedol <> '' and tISO_BONU.Sidname <> '/GB/'
     THEN '/GB/' + tISO_BONU.Sedol
     ELSE ''
     END,
CASE WHEN tISO_BONU.Localcode <> ''
     THEN '/TS/' + tISO_BONU.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_BONU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_BONU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_BONU.Exdate <>'' AND tISO_BONU.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_BONU.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN tISO_BONU.Recdate <>'' AND tISO_BONU.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_BONU.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END as DateRec,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN tISO_BONU.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN tISO_BONU.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN tISO_BONU.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_BONU.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_BONU.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_BONU.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_BONU.Ressid <> ''
     THEN ':35B:' + tISO_BONU.Ressidname +' '+ tISO_BONU.Ressid
     WHEN (tISO_BONU.Ressecid = '')
     THEN ':35B:' + tISO_BONU.Sidname +' '+ tISO_BONU.Sid
     WHEN tISO_BONU.SecID = tISO_BONU.Ressecid
     THEN ':35B:' + tISO_BONU.Sidname +' '+ tISO_BONU.Sid
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_BONU.RatioNew <>''
            AND tISO_BONU.RatioNew IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(tISO_BONU.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_BONU.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN tISO_BONU.Paydate <>'' 
               AND tISO_BONU.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_BONU.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  BonNotes As Notes FROM BON WHERE RDID = '+ cast(tISO_BONU.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_BONU
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
