Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022TEND2
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca2
select   
tISO_TEND_PO.Changed,
'' as ISOHDR,
':16R:GENL',
'132'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_TEND_PO.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_TEND_PO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_TEND_PO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_TEND_PO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_TEND_PO.Sidname +' '+ tISO_TEND_PO.Sid,
substring(tISO_TEND_PO.Issuername,1,35) as TIDYTEXT,
substring(tISO_TEND_PO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_TEND_PO.Sedol <> '' and tISO_TEND_PO.Sidname <> '/GB/'
     THEN '/GB/' + tISO_TEND_PO.Sedol
     ELSE ''
     END,
CASE WHEN tISO_TEND_PO.Localcode <> ''
     THEN '/TS/' + tISO_TEND_PO.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_TEND_PO.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_TEND_PO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_TEND_PO.OfferOpens <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_TEND_PO.OfferOpens,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN tISO_TEND_PO.OfferCloses <> ''
       AND tISO_TEND_PO.OfferOpens <> ''
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , tISO_TEND_PO.OfferOpens,112)+'/'
          +CONVERT ( varchar , tISO_TEND_PO.OfferCloses,112)
     WHEN tISO_TEND_PO.OfferOpens <> ''
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , tISO_TEND_PO.OfferOpens,112)+'/UKWN'
     WHEN tISO_TEND_PO.OfferCloses <> ''
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , tISO_TEND_PO.OfferCloses,112)
     ELSE ':69J::OFFR//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
/*'select  RDNotes As Notes FROM rd WHERE RDID = '+ cast(tISO_TEND_PO.RDID as char(16)) as Notes, */
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN tISO_TEND_PO.CurenCD <> '' 
     THEN ':11A::OPTN//' + tISO_TEND_PO.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN tISO_TEND_PO.Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_TEND_PO.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN tISO_TEND_PO.TndrStrkPrice <> '' 
     THEN ':90B::OFFR//ACTU/'+tISO_TEND_PO.CurenCD+
          +substring(tISO_TEND_PO.TndrStrkPrice,1,15)
     ELSE ':90E::OFFR//UKWN'
     END as COMMASUB,
CASE WHEN tISO_TEND_PO.MaxPrice <>'' AND tISO_TEND_PO.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_TEND_PO.CurenCD+
          +substring(tISO_TEND_PO.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  PONotes As Notes FROM PO WHERE RDID = '+ cast(tISO_TEND_PO.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_TEND_PO
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
