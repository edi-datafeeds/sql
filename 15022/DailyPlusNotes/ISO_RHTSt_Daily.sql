Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022RHTSt
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_RHTSt.Changed,
'' as ISOHDR,
':16R:GENL',
'118'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_RHTSt.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_RHTSt.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_RHTSt.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_RHTSt.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHTS',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
/*':20C::RELA//117'+tISO_RHTSt.EXCHGID+cast(tISO_RHTSt.Seqnum as char(1))+ cast(tISO_RHTSt.EventID as char(16)) as CAref,*/
':16S:GENL',
':16R:USECU',
CASE WHEN rtrim(tISO_RHTSt.TraSid) <> ''
     THEN ':35B:' + tISO_RHTSt.TraSidname +' '+ tISO_RHTSt.TraSid
     ELSE ':35B:ISIN UKWN'
     END,
substring(tISO_RHTSt.tIssuername,1,35),
substring(tISO_RHTSt.tSecuritydesc,1,35) as tsecdesc,
CASE WHEN tISO_RHTSt.Sedol <> '' and tISO_RHTSt.Sidname <> '/GB/'
     THEN '/GB/' + tISO_RHTSt.Sedol
     ELSE ''
     END,
CASE WHEN tISO_RHTSt.Localcode <> ''
     THEN '/TS/' + tISO_RHTSt.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_RHTSt.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_RHTSt.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_RHTSt.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , tISO_RHTSt.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN tISO_RHTSt.EndSubscription <>'' AND tISO_RHTSt.EndSubscription IS NOT NULL
       AND tISO_RHTSt.StartSubscription <>'' AND tISO_RHTSt.StartSubscription IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , tISO_RHTSt.StartSubscription,112)+'/'
          +CONVERT ( varchar , tISO_RHTSt.EndSubscription,112)
     WHEN tISO_RHTSt.StartSubscription <>'' AND tISO_RHTSt.StartSubscription IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , tISO_RHTSt.StartSubscription,112)+'/UKWN'
     WHEN tISO_RHTSt.EndSubscription <>'' AND tISO_RHTSt.EndSubscription IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , tISO_RHTSt.EndSubscription,112)
     ELSE ':69J::IACC//UKWN'
     END,
CASE WHEN tISO_RHTSt.EndTrade <> '' 
       AND tISO_RHTSt.StartTrade <> '' 
     THEN ':69A::CONV//'
          +CONVERT ( varchar , tISO_RHTSt.StartTrade,112) + '/'
          +CONVERT ( varchar , tISO_RHTSt.EndTrade,112)
     WHEN tISO_RHTSt.StartTrade <> '' 
     THEN ':69C::CONV//'
          +CONVERT ( varchar , tISO_RHTSt.StartTrade,112) + '//UKWN'
     WHEN tISO_RHTSt.EndTrade <> '' 
     THEN ':69E::CONV//UKWN/'
          +CONVERT ( varchar , tISO_RHTSt.EndTrade,112)
     ELSE ':69J::CONV//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_RHTSt.RdID as char(16)) as Notes, 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN tISO_RHTSt.CurenCD <> '' 
     THEN ':11A::OPTN//' + tISO_RHTSt.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN tISO_RHTSt.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , tISO_RHTSt.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN tISO_RHTSt.Paydate <>'' 
               AND tISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN tISO_RHTSt.IssuePrice <> ''
     THEN ':90B::SUPR//ACTU/'+CurenCD+ltrim(cast(tISO_RHTSt.IssuePrice as char(15)))
     ELSE ':90E::SUPR//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_RHTSt.ResSid <> ''
     THEN ':35B:' + tISO_RHTSt.ResSidname +' '+ tISO_RHTSt.ResSid
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_RHTSt.RatioNew <>''
     THEN ':92D::NWRT//'+ltrim(cast(tISO_RHTSt.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_RHTSt.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
/* EXERCISE OPTION END */
/* SELL RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//002',
':22F::CAOP//SLLE',
':17B::DFLT//N',
CASE WHEN tISO_RHTSt.Paydate <>'' 
               AND tISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* SELL RIGHTS OPTION END */
/* LAPSE RIGHTS OPTION START */
':16R:CAOPTN',
':13A::CAON//003',
':22F::CAOP//LAPS',
':17B::DFLT//N',
CASE WHEN tISO_RHTSt.Paydate <>'' 
               AND tISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:CAOPTN',
/* LAPSE RIGHTS OPTION END */
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  RTSNotes As Notes FROM RTS WHERE RDID = '+ cast(tISO_RHTSt.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_RHTSt
WHERE 
traSID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
