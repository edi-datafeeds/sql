Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022TEND
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=TKOVR

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca2
select   
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' 
+ rtrim(cast(tISO_TEND_TKOVR.EventID as char(10))) 
+ ' and sEvent = '+ char(39) + 'TKOVR'+ char(39)
+ ' and Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) 
+ ' Order By OptionID, SerialID' as MPAYlink,
tISO_TEND_TKOVR.Changed,
'' as ISOHDR,
':16R:GENL',
'131'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_TEND_TKOVR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_TEND_TKOVR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_TEND_TKOVR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_TEND_TKOVR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//TEND',
':22F::CAMV//VOLU', 
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_TEND_TKOVR.Sidname +' '+ tISO_TEND_TKOVR.Sid,
substring(tISO_TEND_TKOVR.Issuername,1,35) as TIDYTEXT,
substring(tISO_TEND_TKOVR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_TEND_TKOVR.Sedol <> '' and tISO_TEND_TKOVR.Sidname <> '/GB/'
     THEN '/GB/' + tISO_TEND_TKOVR.Sedol
     ELSE ''
     END,
CASE WHEN tISO_TEND_TKOVR.Localcode <> ''
     THEN '/TS/' + tISO_TEND_TKOVR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_TEND_TKOVR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_TEND_TKOVR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_TEND_TKOVR.Opendate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_TEND_TKOVR.opendate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
CASE WHEN tISO_TEND_TKOVR.CloseDate <> ''
       AND tISO_TEND_TKOVR.OpenDate <> ''
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , tISO_TEND_TKOVR.OpenDate,112)+'/'
          +CONVERT ( varchar , tISO_TEND_TKOVR.CloseDate,112)
     WHEN tISO_TEND_TKOVR.OpenDate <> ''
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , tISO_TEND_TKOVR.OpenDate,112)+'/UKWN'
     WHEN tISO_TEND_TKOVR.CloseDate <> ''
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , tISO_TEND_TKOVR.CloseDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN tISO_TEND_TKOVR.UnconditionalDate <>'' and tISO_TEND_TKOVR.UnconditionalDate > getdate()
     THEN ':22F::TAKO//COND'
     ELSE ':22F::TAKO//UNCO'
     END,
':16S:CADETL',
/* START OF CASH BLOCK */
':16R:CAOPTN' as StartseqE,
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  TkovrNotes As Notes FROM TKOVR WHERE TKOVRID = '+ cast(tISO_TEND_TKOVR.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_TEND_TKOVR
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
