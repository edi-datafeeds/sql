Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022BRUP
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_BRUP.Changed,
'' as ISOHDR,
':16R:GENL',
'119'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_BRUP.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_BRUP.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_BRUP.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_BRUP.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BRUP',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_BRUP.Sidname +' '+ tISO_BRUP.Sid,
substring(tISO_BRUP.Issuername,1,35) as TIDYTEXT,
substring(tISO_BRUP.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_BRUP.Sedol <> '' and tISO_BRUP.Sidname <> '/GB/'
     THEN '/GB/' + tISO_BRUP.Sedol
     ELSE ''
     END,
CASE WHEN tISO_BRUP.Localcode <> ''
     THEN '/TS/' + tISO_BRUP.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_BRUP.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_BRUP.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_BRUP.NotificationDate <>'' and tISO_BRUP.NotificationDate is not null
     THEN ':98A::ANOU//'+CONVERT ( varchar , tISO_BRUP.NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN tISO_BRUP.FilingDate <>'' and tISO_BRUP.FilingDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_BRUP.FilingDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  BkrpNotes As Notes FROM BKRP WHERE BKRPID = '+ cast(tISO_BRUP.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_BRUP
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
