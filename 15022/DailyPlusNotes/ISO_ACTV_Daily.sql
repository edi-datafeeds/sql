Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022ACTV
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_ACTV.Changed,
'' as ISOHDR,
':16R:GENL',
'150'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_ACTV.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_ACTV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_ACTV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_ACTV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//ACTV',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_ACTV.Sidname +' '+ tISO_ACTV.Sid,
substring(tISO_ACTV.Issuername,1,35) as TIDYTEXT,
substring(tISO_ACTV.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_ACTV.Sedol <> '' and tISO_ACTV.Sidname <> '/GB/'
     THEN '/GB/' + tISO_ACTV.Sedol
     ELSE ''
     END,
CASE WHEN tISO_ACTV.Localcode <> ''
     THEN '/TS/' + tISO_ACTV.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_ACTV.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_ACTV.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_ACTV.NotificationDate <>'' and tISO_ACTV.NotificationDate is not null
     THEN ':98A::ANOU//'+CONVERT ( varchar , tISO_ACTV.NotificationDate,112)
     ELSE ':98B::ANOU//UKWN'
     END,
CASE WHEN tISO_ACTV.EffectiveDate <>'' and tISO_ACTV.EffectiveDate is not null
     THEN ':98A::TRDT//'+CONVERT ( varchar , tISO_ACTV.EffectiveDate,112)
     ELSE ':98B::TRDT//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  Reason As Notes FROM LSTAT WHERE LSTATID = '+ cast(tISO_ACTV.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_ACTV
WHERE 
SID <> ''
and LStatStatus = 'R'
and upper(eventtype)<>'CLEAN'
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
