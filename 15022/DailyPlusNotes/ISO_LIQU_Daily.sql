Declare @StartDate datetime
Declare @EndDate datetime


set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022LIQU
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=LIQ

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca2
select   
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + rtrim(cast(tISO_LIQU.EventID as char(10))) + ' and sEvent = '+ char(39) + 'LIQ'+ char(39)+ 'And Paytype Is Not NULL And Paytype <> ' +char(39) +char(39) + ' Order By OptionID, SerialID' as MPAYlink,
tISO_LIQU.Changed,
'' as ISOHDR,
':16R:GENL',
'113'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_LIQU.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_LIQU.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_LIQU.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_LIQU.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//LIQU',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_LIQU.Sidname +' '+ tISO_LIQU.Sid,
substring(tISO_LIQU.Issuername,1,35) as TIDYTEXT,
substring(tISO_LIQU.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_LIQU.Sedol <> '' and tISO_LIQU.Sidname <> '/GB/'
     THEN '/GB/' + tISO_LIQU.Sedol
     ELSE ''
     END,
CASE WHEN tISO_LIQU.Localcode <> ''
     THEN '/TS/' + tISO_LIQU.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_LIQU.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_LIQU.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_LIQU.RdDate <>'' AND tISO_LIQU.RdDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_LIQU.RdDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
/* START OF CASH BLOCK */
':16R:CAOPTN' as StartseqE,
':13A::CAON//001' as seqE,
':22F::CAOP//CASH' as seqE,
CASE WHEN tISO_LIQU.CurenCD_1 <> '' AND tISO_LIQU.CurenCD_1 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_LIQU.CurenCD_1
     END as seqE,
':17B::DFLT//N' as seqE,
CASE WHEN tISO_LIQU.MaxPrice_1 <>'' AND tISO_LIQU.MaxPrice_1 IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_LIQU.CurenCD_1+
          +substring(tISO_LIQU.MaxPrice_1,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as seqE,
':16R:CASHMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_LIQU.Paydate_1 <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_LIQU.paydate_1,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:CASHMOVE' as seqE,
':16S:CAOPTN' as seqE,
/* END OF CASH BLOCK */
/* START OF STOCK BLOCK */
':16R:CAOPTN' as seqE,
':13A::CAON//001' as seqE,
':22F::CAOP//SECU' as seqE,
':17B::DFLT//N' as seqE,
':16R:SECMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_LIQU.ResSID_1 <> ''
     THEN ':35B:' + tISO_LIQU.ResSIDname_1 +' '+ tISO_LIQU.ResSID_1
     ELSE ':35B:ISIN UKWN'
     END as seqE,
CASE WHEN tISO_LIQU.RatioNew_1 <>''
     THEN ':92D::NEWO//'+rtrim(cast(tISO_LIQU.RatioNew_1 as char (15)))+
                    '/'+rtrim(cast(tISO_LIQU.RatioOld_1 as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as seqE,
CASE WHEN tISO_LIQU.paydate_1 <> ''
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_LIQU.paydate_1,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:SECMOVE' as seqE,
':16S:CAOPTN' as seqE,
/* END OF STOCK BLOCK */
/* START OF BOTH BLOCK */
':16R:CAOPTN' as seqE,
':13A::CAON//001' as seqE,
':22F::CAOP//CASE' as seqE,
CASE WHEN tISO_LIQU.CurenCD_1 <> '' AND tISO_LIQU.CurenCD_1 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_LIQU.CurenCD_1
     END as seqE,
':17B::DFLT//N' as seqE,
CASE WHEN tISO_LIQU.MaxPrice_1 <>'' AND tISO_LIQU.MaxPrice_1 IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_LIQU.CurenCD_1+
          +substring(tISO_LIQU.MaxPrice_1,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as seqE,
':16R:SECMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_LIQU.ResSID_1 <> ''
     THEN ':35B:' + tISO_LIQU.ResSIDname_1 +' '+ tISO_LIQU.ResSID_1
     ELSE ':35B:ISIN UKWN'
     END as seqE,
CASE WHEN tISO_LIQU.RatioNew_1 <>''
     THEN ':92D::NEWO//'+rtrim(cast(tISO_LIQU.RatioNew_1 as char (15)))+
                    '/'+rtrim(cast(tISO_LIQU.RatioOld_1 as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as seqE,
CASE WHEN tISO_LIQU.paydate_1 <> ''
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_LIQU.paydate_1,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:SECMOVE' as seqE,
':16R:CASHMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_LIQU.Paydate_1 <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_LIQU.paydate_1,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:CASHMOVE' as seqE,
':16S:CAOPTN' as seqE,
/* END OF BOTH BLOCK */
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  LiquidationTerms As Notes FROM LIQ WHERE LIQID = '+ cast(tISO_LIQU.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_LIQU
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
