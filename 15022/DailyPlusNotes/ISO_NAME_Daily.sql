Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022NAME
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_NAME.Changed,
'' as ISOHDR,
':16R:GENL',
'153'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_NAME.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_NAME.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_NAME.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_NAME.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//NAME',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_NAME.Sidname +' '+ tISO_NAME.Sid,
substring(tISO_NAME.IssOldname,1,35) as TIDYTEXT,
substring(tISO_NAME.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_NAME.Sedol <> '' and tISO_NAME.Sidname <> '/GB/'
     THEN '/GB/' + tISO_NAME.Sedol
     ELSE ''
     END,
CASE WHEN tISO_NAME.Localcode <> ''
     THEN '/TS/' + tISO_NAME.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_NAME.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_NAME.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_NAME.NameChangeDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_NAME.NameChangeDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':70E::NAME//' + substring(tISO_NAME.IssNewname,1,35) as TIDYTEXT,
':16S:CADETL',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  IschgNotes As Notes FROM ISCHG WHERE ISCHGID = '+ cast(tISO_NAME.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_NAME
WHERE 
SID <> ''
and upper(eventtype)<>'CLEAN'
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
