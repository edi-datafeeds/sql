Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022PRIO
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_PRIO.Changed,
'' as ISOHDR,
':16R:GENL',
'110'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_PRIO.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN tISO_PRIO.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_PRIO.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_PRIO.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PRIO',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_PRIO.Sidname +' '+ tISO_PRIO.Sid,
substring(tISO_PRIO.Issuername,1,35) as TIDYTEXT,
substring(tISO_PRIO.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_PRIO.Sedol <> '' and tISO_PRIO.Sidname <> '/GB/'
     THEN '/GB/' + tISO_PRIO.Sedol
     ELSE ''
     END,
CASE WHEN tISO_PRIO.Localcode <> ''
     THEN '/TS/' + tISO_PRIO.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_PRIO.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_PRIO.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_PRIO.Exdate <>'' AND tISO_PRIO.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_PRIO.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN tISO_PRIO.Recdate <>'' AND tISO_PRIO.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_PRIO.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN tISO_PRIO.EndSubscription <>'' AND tISO_PRIO.EndSubscription IS NOT NULL
       AND tISO_PRIO.StartSubscription <>'' AND tISO_PRIO.StartSubscription IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , tISO_PRIO.StartSubscription,112)+'/'
          +CONVERT ( varchar , tISO_PRIO.EndSubscription,112)
     WHEN tISO_PRIO.StartSubscription <>'' AND tISO_PRIO.StartSubscription IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , tISO_PRIO.StartSubscription,112)+'/UKWN'
     WHEN tISO_PRIO.EndSubscription <>'' AND tISO_PRIO.EndSubscription IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , tISO_PRIO.EndSubscription,112)
     ELSE ':69J::OFFR//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_PRIO.EventID as char(16)) as Notes, 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
CASE WHEN tISO_PRIO.CurenCD <> '' AND tISO_PRIO.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + tISO_PRIO.CurenCD
     END,
CASE WHEN tISO_PRIO.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , tISO_PRIO.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN tISO_PRIO.MaxPrice <>'' AND tISO_PRIO.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_PRIO.CurenCD+
          +substring(tISO_PRIO.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_PRIO.ResSID <> ''
     THEN ':35B:' + tISO_PRIO.resSIDname +' '+ tISO_PRIO.ResSID
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_PRIO.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN tISO_PRIO.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN tISO_PRIO.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_PRIO.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_PRIO.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_PRIO.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN tISO_PRIO.RatioNew <>''
     THEN ':92D::NEWO//'+ltrim(cast(tISO_PRIO.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_PRIO.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN tISO_PRIO.paydate2 <>'' 
             AND tISO_PRIO.paydate2 IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_PRIO.paydate2,112)
     WHEN tISO_PRIO.paydate <>'' 
             AND tISO_PRIO.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_PRIO.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme*/
'select  PrfNotes As Notes FROM PRF WHERE RDID = '+ cast(tISO_PRIO.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_PRIO
WHERE 
SID <> ''
and Actflag <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3

