Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022PPMT
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_PPMT.Changed,
'' as ISOHDR,
':16R:GENL',
'111'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_PPMT.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_PPMT.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_PPMT.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_PPMT.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//PPMT',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_PPMT.Sidname +' '+ tISO_PPMT.Sid,
substring(tISO_PPMT.Issuername,1,35) as TIDYTEXT,
substring(tISO_PPMT.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_PPMT.Sedol <> '' and tISO_PPMT.Sidname <> '/GB/'
     THEN '/GB/' + tISO_PPMT.Sedol
     ELSE ''
     END,
CASE WHEN tISO_PPMT.Localcode <> ''
     THEN '/TS/' + tISO_PPMT.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_PPMT.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_PPMT.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_PPMT.DueDate <>'' AND tISO_PPMT.DueDate IS NOT NULL
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_PPMT.DueDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN tISO_PPMT.CurenCD <> '' AND tISO_PPMT.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + tISO_PPMT.CurenCD
     END,
':17B::DFLT//Y',
CASE WHEN tISO_PPMT.DueDate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_PPMT.DueDate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN tISO_PPMT.ToFacevalue <> '' AND tISO_PPMT.ToFacevalue is not null
     THEN ':90B::PRPP//ACTU/'+curenCD+rtrim(cast(tISO_PPMT.ToFacevalue as char(15)))
     ELSE ':90E::PRPP//UKWN' END,
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  CallNotes As Notes FROM CALL WHERE CALLID = '+ cast(tISO_PPMT.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_PPMT
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3

