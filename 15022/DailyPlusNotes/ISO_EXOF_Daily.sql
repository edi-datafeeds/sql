Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022EXOF
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_EXOF.Changed,
'' as ISOHDR,
':16R:GENL',
'127'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_EXOF.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN tISO_EXOF.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_EXOF.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_EXOF.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//EXOF',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_EXOF.Sidname +' '+ tISO_EXOF.Sid,
substring(tISO_EXOF.Issuername,1,35) as TIDYTEXT,
substring(tISO_EXOF.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_EXOF.Sedol <> '' and tISO_EXOF.Sidname <> '/GB/'
     THEN '/GB/' + tISO_EXOF.Sedol
     ELSE ''
     END,
CASE WHEN tISO_EXOF.Localcode <> ''
     THEN '/TS/' + tISO_EXOF.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_EXOF.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_EXOF.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_EXOF.EndDate <>'' AND tISO_EXOF.EndDate IS NOT NULL
       AND tISO_EXOF.StartDate <>'' AND tISO_EXOF.StartDate IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , tISO_EXOF.StartDate,112)+'/'
          +CONVERT ( varchar , tISO_EXOF.EndDate,112)
     WHEN tISO_EXOF.StartDate <>'' AND tISO_EXOF.StartDate IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , tISO_EXOF.StartDate,112)+'/UKWN'
     WHEN tISO_EXOF.EndDate <>'' AND tISO_EXOF.EndDate IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , tISO_EXOF.EndDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
':17B::DFLT//N',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_EXOF.ResSID <> ''
     THEN ':35B:' + tISO_EXOF.resSIDname +' '+ tISO_EXOF.ResSID
     ELSE ':35B:ISIN UKWN'
     END,
':92D::NEWO//1,0000/1,0000',
CASE WHEN tISO_EXOF.Paydate <>'' 
             AND tISO_EXOF.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_EXOF.Paydate,112)
     WHEN tISO_EXOF.paydate <>'' 
             AND tISO_EXOF.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_EXOF.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme*/
'select  CtxNotes As Notes FROM CtX WHERE CtxID = '+ cast(tISO_EXOF.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_EXOF
WHERE 
SID <> ''
and Actflag <> ''
and upper(eventtype)<>'CLEAN'
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
