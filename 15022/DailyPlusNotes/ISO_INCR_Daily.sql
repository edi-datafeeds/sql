Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022INCR
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_INCR.Changed,
'' as ISOHDR,
':16R:GENL',
'154'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_INCR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_INCR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_INCR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_INCR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//INCR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_INCR.Sidname +' '+ tISO_INCR.Sid,
substring(tISO_INCR.Issuername,1,35) as TIDYTEXT,
substring(tISO_INCR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_INCR.Sedol <> '' and tISO_INCR.Sidname <> '/GB/'
     THEN '/GB/' + tISO_INCR.Sedol
     ELSE ''
     END,
CASE WHEN tISO_INCR.Localcode <> ''
     THEN '/TS/' + tISO_INCR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_INCR.MCD <>''
     THEN ':94B::PLIS//EXCH/' +tISO_INCR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_INCR.EffectiveDate <>'' and tISO_INCR.EffectiveDate is not null
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_INCR.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN tISO_INCR.CurenCD <> '' 
     THEN ':11A::OPTN//' + tISO_INCR.CurenCD
     END,
':17B::DFLT//Y',
CASE WHEN tISO_INCR.CSPYDate <>'' and tISO_INCR.CSPYDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_INCR.CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN tISO_INCR.CashBak <>'' AND tISO_INCR.CashBak IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_INCR.CurenCD+
          +substring(tISO_INCR.CashBak,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as Commasub,
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  RcapNotes As Notes FROM RCAP WHERE RCAPID = '+ cast(tISO_INCR.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_INCR
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
