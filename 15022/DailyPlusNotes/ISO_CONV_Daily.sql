Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022CONV
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_CONV.Changed,
'' as ISOHDR,
':16R:GENL',
'125'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_CONV.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN tISO_CONV.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_CONV.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_CONV.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//CONV',
CASE WHEN tISO_CONV.MandOptFlag = 'M'
     THEN ':22F::CAMV//MAND'
     ELSE ':22F::CAMV//VOLU'
     END,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_CONV.Sidname +' '+ tISO_CONV.Sid,
substring(tISO_CONV.Issuername,1,35) as TIDYTEXT,
substring(tISO_CONV.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_CONV.Sedol <> '' and tISO_CONV.Sidname <> '/GB/'
     THEN '/GB/' + tISO_CONV.Sedol
     ELSE ''
     END,
CASE WHEN tISO_CONV.Localcode <> ''
     THEN '/TS/' + tISO_CONV.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_CONV.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_CONV.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_CONV.ToDate <>'' AND tISO_CONV.ToDate IS NOT NULL
     THEN ':98A::MKDT//'+CONVERT ( varchar , tISO_CONV.ToDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_CONV.EventID as char(16)) as Notes, 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CONV',
CASE WHEN tISO_CONV.MandOptFlag = 'M'
     THEN ':17B::DFLT//Y'
     ELSE ':17B::DFLT//N'
     END,
CASE WHEN tISO_CONV.PfrCnPrice <>'' AND tISO_CONV.PfrCnPrice IS NOT NULL
     THEN ':90B::EXER//ACTU/'+tISO_CONV.CurenCD+
          +substring(tISO_CONV.PfrCnPrice,1,15)
     ELSE ':90E::EXER//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_CONV.ResSID <> ''
     THEN ':35B:' + tISO_CONV.resSIDname +' '+ tISO_CONV.ResSID
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_CONV.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN tISO_CONV.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN tISO_CONV.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_CONV.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_CONV.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_CONV.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
CASE WHEN tISO_CONV.RatioNew <>''
     THEN ':92D::NEWO//'+rtrim(cast(tISO_CONV.RatioNew as char (15)))+
                    '/'+rtrim(cast(tISO_CONV.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' END as COMMASUB,
CASE WHEN tISO_CONV.Paydate <>'' 
             AND tISO_CONV.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_CONV.Paydate,112)
     WHEN tISO_CONV.paydate <>'' 
             AND tISO_CONV.paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_CONV.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme*/
'select ConvNotes As Notes FROM CONV WHERE ConvID = '+ cast(tISO_CONV.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_CONV
WHERE 
SID <> ''
and Actflag <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
