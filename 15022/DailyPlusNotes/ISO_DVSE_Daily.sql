Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022DVSE
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_D.Changed,
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_D.Actflag_1 = 'D'
     THEN ':23G:CANC'
     WHEN tISO_D.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_D.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_D.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_D.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVSE',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_D.Sidname +' '+ tISO_D.Sid,
substring(tISO_D.Issuername,1,35) as TIDYTEXT,
substring(tISO_D.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_D.Sedol <> '' and tISO_D.Sidname <> '/GB/'
     THEN '/GB/' + tISO_D.Sedol
     ELSE ''
     END,
CASE WHEN tISO_D.Localcode <> ''
     THEN '/TS/' + tISO_D.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_D.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_D.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_D.Exdate <>'' AND tISO_D.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_D.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN tISO_D.Recdate <>'' AND tISO_D.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_D.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN tISO_D.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(tISO_D.Taxrate_1 as char(15))
     ELSE ':92K::TAXR//UKWN' 
     END as COMMASUB,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_D.RdID as char(16)) as Notes, 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN tISO_D.Fractions_1 = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN tISO_D.Fractions_1 = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN tISO_D.Fractions_1 = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_D.Fractions_1 = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_D.Fractions_1 = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_D.Fractions_1 = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_D.Ressid_1 <> ''
     THEN ':35B:' + tISO_D.Ressidname_1 +' '+ tISO_D.Ressid_1
     WHEN (tISO_D.Ressecid_1 = '')
     THEN ':35B:' + tISO_D.Sidname +' '+ tISO_D.Sid
     WHEN tISO_D.SecID = tISO_D.Ressecid_1
     THEN ':35B:' + tISO_D.Sidname +' '+ tISO_D.Sid
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_D.RatioNew_1 <>''
     THEN ':92D::ADEX//'+ltrim(cast(tISO_D.RatioNew_1 as char (15)))+
                    '/'+ltrim(cast(tISO_D.RatioOld_1 as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN tISO_D.Paydate <>'' 
               AND tISO_D.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_D.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(tISO_D.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_D
WHERE 
SID <> ''
and Actflag <> ''
and (DIVType_1 = 'S'
AND OpID_2 = ''
AND OpID_3 = ''
AND OpID_4 = ''
AND OpID_5 = ''
AND OpID_6 = '')
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
