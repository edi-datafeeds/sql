Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022OTHR
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca2
select   
tISO_OTHR_DVST.Changed,
'' as ISOHDR,
':16R:GENL',
'137'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_OTHR_DVST.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_OTHR_DVST.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_OTHR_DVST.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_OTHR_DVST.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_OTHR_DVST.Sidname +' '+ tISO_OTHR_DVST.Sid,
substring(tISO_OTHR_DVST.Issuername,1,35) as TIDYTEXT,
substring(tISO_OTHR_DVST.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_OTHR_DVST.Sedol <> '' and tISO_OTHR_DVST.Sidname <> '/GB/'
     THEN '/GB/' + tISO_OTHR_DVST.Sedol
     ELSE ''
     END,
CASE WHEN tISO_OTHR_DVST.Localcode <> ''
     THEN '/TS/' + tISO_OTHR_DVST.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_OTHR_DVST.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_OTHR_DVST.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_OTHR_DVST.Exdate <> ''
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_OTHR_DVST.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN tISO_OTHR_DVST.Recdate <> ''
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_OTHR_DVST.Recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN tISO_OTHR_DVST.EndSubscription <> ''
       AND tISO_OTHR_DVST.StartSubscription <> ''
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , tISO_OTHR_DVST.StartSubscription,112)+'/'
          +CONVERT ( varchar , tISO_OTHR_DVST.EndSubscription,112)
     WHEN tISO_OTHR_DVST.StartSubscription <> ''
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , tISO_OTHR_DVST.StartSubscription,112)+'/UKWN'
     WHEN tISO_OTHR_DVST.EndSubscription <> ''
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , tISO_OTHR_DVST.EndSubscription,112)
     ELSE ':69J::OFFR//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_OTHR_DVST.RDID as char(16)) as Notes, 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASE',
CASE WHEN tISO_OTHR_DVST.CurenCD <> '' AND tISO_OTHR_DVST.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + tISO_OTHR_DVST.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN tISO_OTHR_DVST.MaxPrice <>'' AND tISO_OTHR_DVST.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_OTHR_DVST.CurenCD+
          +substring(tISO_OTHR_DVST.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_OTHR_DVST.ResSID <> ''
     THEN ':35B:' + tISO_OTHR_DVST.ResSIDname +' '+ tISO_OTHR_DVST.ResSID
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_OTHR_DVST.Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN tISO_OTHR_DVST.Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN tISO_OTHR_DVST.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_OTHR_DVST.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_OTHR_DVST.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_OTHR_DVST.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
CASE WHEN tISO_OTHR_DVST.RatioNew <>''
     THEN ':92D::ADEX//'+ltrim(cast(tISO_OTHR_DVST.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_OTHR_DVST.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' 
     END as COMMASUB,
CASE WHEN tISO_OTHR_DVST.paydate <> ''
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_OTHR_DVST.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  DvstNotes As Notes FROM DVST WHERE RDID = '+ cast(tISO_OTHR_DVST.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_OTHR_DVST
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
