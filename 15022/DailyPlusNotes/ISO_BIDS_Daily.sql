Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022BIDS
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_BIDS.Changed,
'' as ISOHDR,
':16R:GENL',
'115'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_BIDS.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_BIDS.Actflag = 'C'
     THEN ':23G:CANC'
     WHEN tISO_BIDS.Actflag = 'I' 
     THEN ':23G:NEWM'
     WHEN tISO_BIDS.Actflag = 'U'
     THEN ':23G:REPL'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//BIDS',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_BIDS.Sidname +' '+ tISO_BIDS.Sid,
substring(tISO_BIDS.Issuername,1,35) as TIDYTEXT,
substring(tISO_BIDS.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_BIDS.Sedol <> '' and tISO_BIDS.Sidname <> '/GB/'
     THEN '/GB/' + tISO_BIDS.Sedol
     ELSE ''
     END,
CASE WHEN tISO_BIDS.Localcode <> ''
     THEN '/TS/' + tISO_BIDS.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_BIDS.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_BIDS.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_BIDS.EndDate <>'' AND tISO_BIDS.EndDate IS NOT NULL
       AND tISO_BIDS.StartDate <>'' AND tISO_BIDS.StartDate IS NOT NULL
     THEN ':69A::OFFR//'
          +CONVERT ( varchar , tISO_BIDS.StartDate,112)+'/'
          +CONVERT ( varchar , tISO_BIDS.EndDate,112)
     WHEN tISO_BIDS.StartDate <>'' AND tISO_BIDS.StartDate IS NOT NULL
     THEN ':69C::OFFR//'
          +CONVERT ( varchar , tISO_BIDS.StartDate,112)+'/UKWN'
     WHEN tISO_BIDS.EndDate <>'' AND tISO_BIDS.EndDate IS NOT NULL
     THEN ':69E::OFFR//UKWN/'
          +CONVERT ( varchar , tISO_BIDS.EndDate,112)
     ELSE ':69J::OFFR//UKWN'
     END,
CASE WHEN tISO_BIDS.MaxQlyQty <> ''
     THEN ':36B::QTSO//'+substring(tISO_BIDS.MaxQlyQty,1,15) + ','
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//PUTT',
CASE WHEN tISO_BIDS.CurenCD <> '' AND tISO_BIDS.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + tISO_BIDS.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN tISO_BIDS.EndDate <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , tISO_BIDS.EndDate,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN tISO_BIDS.paydate <>'' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_BIDS.paydate,112)
     ELSE ':98B::PAYD//UKWN' END,
CASE WHEN tISO_BIDS.MaxPrice <> '' AND tISO_BIDS.MaxPrice IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_BIDS.CurenCD+
          +substring(tISO_BIDS.MaxPrice,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as COMMASUB,
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  BBNotes As Notes FROM BB WHERE BBID = '+ cast(tISO_BIDS.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_BIDS
WHERE 
SID <> ''
and Actflag <> ''
/*and (sedol = 'ZZZZZZZ')*/
/*and (cntrycd <> 'ZZ')*/

