Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022SOFF2
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N
--sEvent=DMRGR

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets
':35B:' + tISO_SOFF_DMRGR.Sidname +' '+ tISO_SOFF_DMRGR.Sid as DEBTSECU,
*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca2
select   
'select  * from V10s_MPAY where actflag<>'+char(39)+'D'+char(39)+' and eventid = ' + ltrim(cast(tISO_SOFF_DMRGR.EventID as char(10))) + ' and sEvent = '+ char(39) + 'DMRGR'+ char(39) + ' Order By OptionID, SerialID' as MPAYlink,
tISO_SOFF_DMRGR.Changed,
'' as ISOHDR,
':16R:GENL',
'138'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_SOFF_DMRGR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_SOFF_DMRGR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_SOFF_DMRGR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_SOFF_DMRGR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SOFF',
/* populated by rendering programme ':22F::CAMV//' 
CHOS if > option and~or serial
else MAND
*/
'' as CHOICE,
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_SOFF_DMRGR.Sidname +' '+ tISO_SOFF_DMRGR.Sid,
substring(tISO_SOFF_DMRGR.Issuername,1,35) as TIDYTEXT,
substring(tISO_SOFF_DMRGR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_SOFF_DMRGR.Sedol <> '' and tISO_SOFF_DMRGR.Sidname <> '/GB/'
     THEN '/GB/' + tISO_SOFF_DMRGR.Sedol
     ELSE ''
     END,
CASE WHEN tISO_SOFF_DMRGR.Localcode <> ''
     THEN '/TS/' + tISO_SOFF_DMRGR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_SOFF_DMRGR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_SOFF_DMRGR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_SOFF_DMRGR.EffectiveDate <> ''
     THEN ':98A::EFFD//'+CONVERT ( varchar , tISO_SOFF_DMRGR.EffectiveDate,112)
     ELSE ':98B::EFFD//UKWN'
     END,
/* Notes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_SOFF_DMRGR.RDID as char(16)) as Notes, 
':16S:CADETL',
/* START OF CASH BLOCK */
':16R:CAOPTN' as StartseqE,
':13A::CAON//001' as seqE,
':22F::CAOP//CASH' as seqE,
CASE WHEN tISO_SOFF_DMRGR.CurenCD_1 <> '' 
     THEN ':11A::OPTN//' + tISO_SOFF_DMRGR.CurenCD_1
     END as seqE,
':17B::DFLT//N' as seqE,
CASE WHEN tISO_SOFF_DMRGR.TndrStrkPrice_1 <> '' 
     THEN ':90B::OFFR//ACTU/'+tISO_SOFF_DMRGR.CurenCD_1+
          +substring(tISO_SOFF_DMRGR.TndrStrkPrice_1,1,15)
     ELSE ':90E::OFFR//UKWN '
     END as seqE,
':16R:CASHMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_SOFF_DMRGR.Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SOFF_DMRGR.Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:CASHMOVE' as seqE,
':16S:CAOPTN' as seqE,
/* END OF CASH BLOCK */
/* START OF STOCK BLOCK */
':16R:CAOPTN' as seqE,
':13A::CAON//001' as seqE,
':22F::CAOP//SECU' as seqE,
':17B::DFLT//N' as seqE,
':16R:SECMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_SOFF_DMRGR.ResSID_1 <> ''
     THEN ':35B:' + tISO_SOFF_DMRGR.ResSIDname_1 +' '+ tISO_SOFF_DMRGR.ResSID_1
     ELSE ':35B:ISIN UKWN'
     END as seqE,
CASE WHEN tISO_SOFF_DMRGR.RatioNew_1 <>''
     THEN ':92D::NEWO//'+ltrim(cast(tISO_SOFF_DMRGR.RatioNew_1 as char (15)))+
                    '/'+ltrim(cast(tISO_SOFF_DMRGR.RatioOld_1 as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as seqE,
CASE WHEN tISO_SOFF_DMRGR.Paydate <> ''
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SOFF_DMRGR.Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:SECMOVE' as seqE,
':16S:CAOPTN' as seqE,
/* END OF STOCK BLOCK */
/* START OF BOTH BLOCK */
':16R:CAOPTN' as seqE,
':13A::CAON//001' as seqE,
':22F::CAOP//CASE' as seqE,
CASE WHEN tISO_SOFF_DMRGR.CurenCD_1 <> '' AND tISO_SOFF_DMRGR.CurenCD_1 IS NOT NULL
     THEN ':11A::OPTN//' + tISO_SOFF_DMRGR.CurenCD_1
     END as seqE,
':17B::DFLT//N' as seqE,
CASE WHEN tISO_SOFF_DMRGR.TndrStrkPrice_1 <>'' AND tISO_SOFF_DMRGR.TndrStrkPrice_1 IS NOT NULL
     THEN ':90B::PRPP//ACTU/'+tISO_SOFF_DMRGR.CurenCD_1+
          +substring(tISO_SOFF_DMRGR.TndrStrkPrice_1,1,15)
     ELSE ':90E::PRPP//UKWN'
     END as seqE,
':16R:SECMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_SOFF_DMRGR.ResSID_1 <> ''
     THEN ':35B:' + tISO_SOFF_DMRGR.ResSIDname_1 +' '+ tISO_SOFF_DMRGR.ResSID_1
     ELSE ':35B:ISIN UKWN'
     END as seqE,
CASE WHEN tISO_SOFF_DMRGR.RatioNew_1 <>''
     THEN ':92D::NEWO//'+ltrim(cast(tISO_SOFF_DMRGR.RatioNew_1 as char (15)))+
                    '/'+ltrim(cast(tISO_SOFF_DMRGR.RatioOld_1 as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as seqE,
CASE WHEN tISO_SOFF_DMRGR.Paydate <> ''
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SOFF_DMRGR.Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:SECMOVE' as seqE,
':16R:CASHMOVE' as seqE,
':22H::CRDB//CRED' as seqE,
CASE WHEN tISO_SOFF_DMRGR.Paydate <> '' 
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SOFF_DMRGR.Paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END as seqE,
':16S:CASHMOVE' as seqE,
':16S:CAOPTN' as seqE,
/* END OF BOTH BLOCK */
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  DmrgrNotes As Notes FROM DMRGR WHERE RDID = '+ cast(tISO_SOFF_DMRGR.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_SOFF_DMRGR
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
