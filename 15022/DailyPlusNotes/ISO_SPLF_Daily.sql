Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022SPLF
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_SPLF.Changed,
'' as ISOHDR,
':16R:GENL',
'120'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_SPLF.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_SPLF.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_SPLF.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_SPLF.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLF',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
CASE WHEN tISO_SPLF.OldSid <> '' and tISO_SPLF.OldSidName = 'ISIN'
     THEN ':35B:' + tISO_SPLF.OldSidname +' '+ tISO_SPLF.OldSid
     WHEN tISO_SPLF.OldSid <> '' and tISO_SPLF.SidName = '/GB/'
     THEN ':35B:' + tISO_SPLF.OldSidname +' '+ tISO_SPLF.OldSid
     ELSE ':35B:' + tISO_SPLF.Sidname +' '+ tISO_SPLF.Sid
     END,
substring(tISO_SPLF.Issuername,1,35) as TIDYTEXT,
substring(tISO_SPLF.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_SPLF.Sedol <> '' and tISO_SPLF.Sidname <> '/GB/'
     THEN '/GB/' + tISO_SPLF.Sedol
     ELSE ''
     END,
CASE WHEN tISO_SPLF.Localcode <> ''
     THEN '/TS/' + tISO_SPLF.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_SPLF.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_SPLF.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_SPLF.Exdate <>'' AND tISO_SPLF.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_SPLF.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END as DateEx,
CASE WHEN tISO_SPLF.Recdate <>'' AND tISO_SPLF.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_SPLF.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END as DateRec,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN tISO_SPLF.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN tISO_SPLF.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN tISO_SPLF.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_SPLF.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_SPLF.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_SPLF.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_SPLF.NewSid <>''
     THEN ':35B:' + tISO_SPLF.NewSidname +' '+ tISO_SPLF.NewSid
     ELSE ':35B:' + tISO_SPLF.Sidname +' '+ tISO_SPLF.Sid
     END,
CASE WHEN tISO_SPLF.RatioNew <>''
            AND tISO_SPLF.RatioNew IS NOT NULL
     THEN ':92D::ADEX//'+ltrim(cast(tISO_SPLF.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_SPLF.RatioOld as char (15))) 
     ELSE ':92K::ADEX//UKWN' END as COMMASUB,
CASE WHEN tISO_SPLF.Paydate <>'' 
               AND tISO_SPLF.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SPLF.paydate,112)
     ELSE ':98B::PAYD//UKWN' END as DatePay,
':16S:SECMOVE',
':16S:CAOPTN',
CASE WHEN tISO_SPLF.OldParValue <> '' or tISO_SPLF.NewParValue <> ''
     THEN ':16R:ADDINFO'
     ELSE '' END,
CASE WHEN tISO_SPLF.OldParValue <> ''
     THEN ':70E::ADTX//' + 'OldParValue '+tISO_SPLF.OldParValue
     ELSE ''
     END, 
CASE WHEN tISO_SPLF.NewParValue <> ''
     THEN ':70E::ADTX//' + 'NewParValue '+tISO_SPLF.NewParValue
     ELSE ''
     END, 
CASE WHEN tISO_SPLF.OldParValue <> '' or tISO_SPLF.NewParValue <> ''
     THEN ':16S:ADDINFO'
     ELSE '' END,
'-}$'
From tISO_SPLF
WHERE 
(SID <> '' or OldSID <> '')
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
