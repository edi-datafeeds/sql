Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022RHTSn
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_RHTSn.Changed,
'' as ISOHDR,
':16R:GENL',
'116'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_RHTSn.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_RHTSn.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_RHTSn.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_RHTSn.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHTS',
':22F::CAMV//CHOS',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_RHTSn.Sidname +' '+ tISO_RHTSn.Sid,
substring(tISO_RHTSn.Issuername,1,35) as TIDYTEXT,
substring(tISO_RHTSn.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_RHTSn.Sedol <> '' and tISO_RHTSn.Sidname <> '/GB/'
     THEN '/GB/' + tISO_RHTSn.Sedol
     ELSE ''
     END,
CASE WHEN tISO_RHTSn.Localcode <> ''
     THEN '/TS/' + tISO_RHTSn.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_RHTSn.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_RHTSn.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_RHTSn.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , tISO_RHTSn.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN tISO_RHTSn.EndSubscription <> ''
     THEN ':98A::EXPI//'+CONVERT ( varchar , tISO_RHTSn.EndSubscription,112)
     ELSE ':98B::EXPI//UKWN'
     END,
CASE WHEN tISO_RHTSn.EndSubscription <>'' AND tISO_RHTSn.EndSubscription IS NOT NULL
       AND tISO_RHTSn.StartSubscription <>'' AND tISO_RHTSn.StartSubscription IS NOT NULL
     THEN ':69A::IACC//'
          +CONVERT ( varchar , tISO_RHTSn.StartSubscription,112)+'/'
          +CONVERT ( varchar , tISO_RHTSn.EndSubscription,112)
     WHEN tISO_RHTSn.StartSubscription <>'' AND tISO_RHTSn.StartSubscription IS NOT NULL
     THEN ':69C::IACC//'
          +CONVERT ( varchar , tISO_RHTSn.StartSubscription,112)+'/UKWN'
     WHEN tISO_RHTSn.EndSubscription <>'' AND tISO_RHTSn.EndSubscription IS NOT NULL
     THEN ':69E::IACC//UKWN/'
          +CONVERT ( varchar , tISO_RHTSn.EndSubscription,112)
     ELSE ':69J::IACC//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_RHTSn.RdID as char(16)) as Notes, 
':16S:CADETL',
/* EXERCISE OPTION START */
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//EXER',
CASE WHEN tISO_RHTSn.CurenCD <> '' 
     THEN ':11A::OPTN//' + tISO_RHTSn.CurenCD
     END,
':17B::DFLT//N',
CASE WHEN tISO_RHTSn.EndSubscription <> ''
     THEN ':98A::MKDT//'+CONVERT ( varchar , tISO_RHTSn.EndSubscription,112)
     ELSE ':98B::MKDT//UKWN'
     END,
CASE WHEN tISO_RHTSn.Paydate <> '' and tISO_RHTSn.Paydate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_RHTSn.paydate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN tISO_RHTSn.EntIssuePrice <> ''
     THEN ':90B::SUPR//ACTU/'+CurenCD+ltrim(cast(tISO_RHTSn.EntIssuePrice as char(15)))
     ELSE ':90E::SUPR//UKWN'
     END as COMMASUB,
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_RHTSn.ResSid <> ''
     THEN ':35B:' + tISO_RHTSn.ResSidname +' '+ tISO_RHTSn.ResSid
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_RHTSn.RatioNew <>''
     THEN ':92D::NWRT//'+ltrim(cast(tISO_RHTSn.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_RHTSn.RatioOld as char (15))) 
     ELSE ':92K::NWRT//UKWN' 
     END as COMMASUB,
':16S:SECMOVE',
':16S:CAOPTN',
/* EXERCISE OPTION END */
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  ENTNotes As Notes FROM ENT WHERE RDID = '+ cast(tISO_RHTSn.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_RHTSn
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
