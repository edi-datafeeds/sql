Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022RHDI
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_RHTSt.Changed,
'' as ISOHDR,
':16R:GENL',
'117'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_RHTSt.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_RHTSt.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_RHTSt.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_RHTSt.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//RHDI',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
/*':20C::RELA//118'+tISO_RHTSt.EXCHGID+cast(tISO_RHTSt.Seqnum as char(1))+ cast(tISO_RHTSt.EventID as char(16)) as CAref,*/
':16S:GENL',
':16R:USECU',
':35B:' + tISO_RHTSt.Sidname +' '+ tISO_RHTSt.Sid,
substring(tISO_RHTSt.Issuername,1,35) as TIDYTEXT,
substring(tISO_RHTSt.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_RHTSt.Sedol <> '' and tISO_RHTSt.Sidname <> '/GB/'
     THEN '/GB/' + tISO_RHTSt.Sedol
     ELSE ''
     END,
CASE WHEN tISO_RHTSt.Localcode <> ''
     THEN '/TS/' + tISO_RHTSt.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_RHTSt.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_RHTSt.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_RHTSt.Exdate <>'' AND tISO_RHTSt.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_RHTSt.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN tISO_RHTSt.Recdate <>'' AND tISO_RHTSt.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_RHTSt.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN tISO_RHTSt.EndTrade <> '' 
       AND tISO_RHTSt.StartTrade <> '' 
     THEN ':69A::REVO//'
          +CONVERT ( varchar , tISO_RHTSt.StartTrade,112) + '/'
          +CONVERT ( varchar , tISO_RHTSt.EndTrade,112)
     WHEN tISO_RHTSt.StartTrade <> '' 
     THEN ':69C::REVO//'
          +CONVERT ( varchar , tISO_RHTSt.StartTrade,112) + '/UKWN'
     WHEN tISO_RHTSt.EndTrade <> '' 
     THEN ':69E::REVO//UKWN/'
          +CONVERT ( varchar , tISO_RHTSt.EndTrade,112)
     ELSE ':69J::REVO//UKWN'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(tISO_RHTSt.RdID as char(16)) as Notes, 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN tISO_RHTSt.Fractions = 'C' 
     THEN ':22F::DISF//CINL'
     WHEN tISO_RHTSt.Fractions = 'U' 
     THEN ':22F::DISF//RDUP'
     WHEN tISO_RHTSt.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_RHTSt.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_RHTSt.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_RHTSt.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE '' 
     END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_RHTSt.TraSid <> ''
     THEN ':35B:' + tISO_RHTSt.TraSidname +' '+ tISO_RHTSt.TraSid
     ELSE ':35B:ISIN UKWN'
     END,
CASE WHEN tISO_RHTSt.RatioNew <>''
     THEN ':92D::NEWO//'+ltrim(cast(tISO_RHTSt.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_RHTSt.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN' 
     END as COMMASUB,
CASE WHEN tISO_RHTSt.Paydate <>'' 
               AND tISO_RHTSt.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_RHTSt.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  RtsNotes As Notes FROM RTS WHERE RDID = '+ cast(tISO_RHTSt.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_RHTSt
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
