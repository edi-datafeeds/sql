@StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022SPLR
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_SPLR.Changed,
'' as ISOHDR,
':16R:GENL',
'121'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_SPLR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_SPLR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_SPLR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_SPLR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//SPLR',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
CASE WHEN tISO_SPLR.OldSid <> '' and tISO_SPLR.OldSidName = 'ISIN'
     THEN ':35B:' + tISO_SPLR.OldSidname +' '+ tISO_SPLR.OldSid
     WHEN tISO_SPLR.OldSid <> '' and tISO_SPLR.SidName = '/GB/'
     THEN ':35B:' + tISO_SPLR.OldSidname +' '+ tISO_SPLR.OldSid
     ELSE ':35B:' + tISO_SPLR.Sidname +' '+ tISO_SPLR.Sid
     END,
substring(tISO_SPLR.Issuername,1,35) as TIDYTEXT,
substring(tISO_SPLR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_SPLR.Sedol <> '' and tISO_SPLR.Sidname <> '/GB/'
     THEN '/GB/' + tISO_SPLR.Sedol
     ELSE ''
     END,
CASE WHEN tISO_SPLR.Localcode <> ''
     THEN '/TS/' + tISO_SPLR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_SPLR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_SPLR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_SPLR.Exdate <>'' AND tISO_SPLR.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_SPLR.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN tISO_SPLR.Recdate <>'' AND tISO_SPLR.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_SPLR.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//SECU',
CASE WHEN tISO_SPLR.Fractions = 'C'
     THEN ':22F::DISF//CINL'
     WHEN tISO_SPLR.Fractions = 'U'
     THEN ':22F::DISF//RDUP'
     WHEN tISO_SPLR.Fractions = 'D'
     THEN ':22F::DISF//RDDN'
     WHEN tISO_SPLR.Fractions = 'B'
     THEN ':22F::DISF//BUYU'
     WHEN tISO_SPLR.Fractions = 'T'
     THEN ':22F::DISF//DIST'
     WHEN tISO_SPLR.Fractions = 'S'
     THEN ':22F::DISF//STAN'
     ELSE ''
     END,
':17B::DFLT//Y',
':16R:SECMOVE',
':22H::CRDB//CRED',
CASE WHEN tISO_SPLR.NewSid <>''
     THEN ':35B:' + tISO_SPLR.NewSidname +' '+ tISO_SPLR.NewSid
     ELSE ':35B:' + tISO_SPLR.Sidname +' '+ tISO_SPLR.Sid
     END,
CASE WHEN tISO_SPLR.RatioNew <>''
            AND tISO_SPLR.RatioNew IS NOT NULL
     THEN ':92D::NEWO//'+ltrim(cast(tISO_SPLR.RatioNew as char (15)))+
                    '/'+ltrim(cast(tISO_SPLR.RatioOld as char (15))) 
     ELSE ':92K::NEWO//UKWN'
     END as COMMASUB,
CASE WHEN tISO_SPLR.Paydate <>'' 
               AND tISO_SPLR.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_SPLR.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
':16S:SECMOVE',
':16S:CAOPTN',
CASE WHEN tISO_SPLR.OldParValue <> '' or tISO_SPLR.NewParValue <> ''
     THEN ':16R:ADDINFO'
     ELSE '' END,
CASE WHEN tISO_SPLR.OldParValue <> ''
     THEN ':70E::ADTX//' + 'OldParValue '+tISO_SPLR.OldParValue
     ELSE ''
     END, 
CASE WHEN tISO_SPLR.NewParValue <> ''
     THEN ':70E::ADTX//' + 'NewParValue '+tISO_SPLR.NewParValue
     ELSE ''
     END, 
CASE WHEN tISO_SPLR.OldParValue <> '' or tISO_SPLR.NewParValue <> ''
     THEN ':16S:ADDINFO'
     ELSE '' END,
'-}$'
From tISO_SPLR
WHERE 
(SID <> '' or OldSID <> '')
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
