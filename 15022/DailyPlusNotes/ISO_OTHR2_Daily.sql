Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=O:\Datafeed\15022\DAILYPLUSNOTES\
--FilePrefix=
--FileName=YYYYMMDD_15022OTHR2
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=on
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca2
select   
tISO_OTHR_ARR.Changed,
'' as ISOHDR,
':16R:GENL',
'112'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN tISO_OTHR_ARR.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN tISO_OTHR_ARR.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN tISO_OTHR_ARR.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN tISO_OTHR_ARR.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//OTHR',
':22F::CAMV//VOLU',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + tISO_OTHR_ARR.Sidname +' '+ tISO_OTHR_ARR.Sid,
substring(tISO_OTHR_ARR.Issuername,1,35) as TIDYTEXT,
substring(tISO_OTHR_ARR.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN tISO_OTHR_ARR.Sedol <> '' and tISO_OTHR_ARR.Sidname <> '/GB/'
     THEN '/GB/' + tISO_OTHR_ARR.Sedol
     ELSE ''
     END,
CASE WHEN tISO_OTHR_ARR.Localcode <> ''
     THEN '/TS/' + tISO_OTHR_ARR.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN tISO_OTHR_ARR.MCD <>''
     THEN ':94B::PLIS//EXCH/' + tISO_OTHR_ARR.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN tISO_OTHR_ARR.ExDate <>'' and tISO_OTHR_ARR.ExDate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , tISO_OTHR_ARR.ExDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN tISO_OTHR_ARR.PayDate <>'' and tISO_OTHR_ARR.PayDate is not null
     THEN ':98A::PAYD//'+CONVERT ( varchar , tISO_OTHR_ARR.PayDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN tISO_OTHR_ARR.RecDate <>'' and tISO_OTHR_ARR.RecDate is not null
     THEN ':98A::RDTE//'+CONVERT ( varchar , tISO_OTHR_ARR.RecDate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
':16S:CADETL',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  ArrNotes As Notes FROM Arr WHERE RDID = '+ cast(tISO_OTHR_ARR.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From tISO_OTHR_ARR
WHERE 
SID <> ''
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
