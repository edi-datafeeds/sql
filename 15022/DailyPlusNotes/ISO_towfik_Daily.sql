Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
set @EndDate = getdate()+1

--FilePath=c:\
--FilePrefix=
--FileName=YYYYMMDD_15022DVCA
--FileExtension=.TXT
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--OutputStyle=15022
--FieldSeparator=	
--archive=y
--ArchivePath=n:\15022\DAILYPLUSNOTES\
--FIELDHEADERS=off
--FileTidy=N

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

/* Use script Var AND Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1

use wca
select 
v54f_ISO_D.Changed,
'' as ISOHDR,
':16R:GENL',
'100'+EXCHGID+cast(Seqnum as char(1)) as CAref1,
EventID as CAREF2,
SecID as CAREF3,
/* populated by rendering programme*/
'' as SRID,
CASE WHEN v54f_ISO_D.Actflag_1 = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_D.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN v54f_ISO_D.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN v54f_ISO_D.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN v54f_ISO_D.Actflag = 'C'
     THEN ':23G:CANC'
     ELSE ''
     END AS MessageStatus,
':22F::CAEV//DVCA',
':22F::CAMV//MAND',
':25D::PROC//COMP', 
/* populated by rendering programme*/
'' as PSRID,
':16S:GENL',
':16R:USECU',
':35B:' + v54f_ISO_D.Sidname +' '+ v54f_ISO_D.Sid,
substring(v54f_ISO_D.Issuername,1,35) as TIDYTEXT,
substring(v54f_ISO_D.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN v54f_ISO_D.Sedol <> '' and v54f_ISO_D.Sidname <> '/GB/'
     THEN '/GB/' + v54f_ISO_D.Sedol
     ELSE ''
     END,
CASE WHEN v54f_ISO_D.Localcode <> ''
     THEN '/TS/' + v54f_ISO_D.Localcode
     ELSE ''
     END,
':16R:FIA',
CASE WHEN v54f_ISO_D.MCD <>''
     THEN ':94B::PLIS//EXCH/' + v54f_ISO_D.MCD
     END,
':16S:FIA',
':16R:ACCTINFO',
':97A::SAFE//NONREF',
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN v54f_ISO_D.Exdate <>'' AND v54f_ISO_D.Exdate IS NOT NULL
     THEN ':98A::XDTE//'+CONVERT ( varchar , v54f_ISO_D.Exdate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
CASE WHEN v54f_ISO_D.Recdate <>'' AND v54f_ISO_D.Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , v54f_ISO_D.recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN v54f_ISO_D.DivPeriodCD='FNL'
     THEN ':22F::DIVI//FINL'
     WHEN v54f_ISO_D.DivPeriodCD='UN' OR  v54f_ISO_D.DivPeriodCD='INT'
     THEN ':22F::DIVI//INTE'
     WHEN v54f_ISO_D.DivPeriodCD='SPL' OR  v54f_ISO_D.DivPeriodCD='NL'
     THEN ':22F::DIVI//SPEC'
     ELSE ':22F::DIVI//REGR'
     END,
/* RDNotes populated by rendering programme :70E::TXNR// */
'select  RDNotes As Notes FROM RD WHERE RDID = '+ cast(v54f_ISO_D.RdID as char(16)) as Notes, 
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN v54f_ISO_D.CurenCD_1 <> '' AND v54f_ISO_D.CurenCD_1 IS NOT NULL
     THEN ':11A::OPTN//' + v54f_ISO_D.CurenCD_1
     END,
':17B::DFLT//Y',
CASE WHEN v54f_ISO_D.Paydate <>'' 
             AND v54f_ISO_D.Paydate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , v54f_ISO_D.paydate,112)
     ELSE ':98B::PAYD//UKWN' 
     END,
CASE WHEN v54f_ISO_D.Netdividend_1 <> ''
                 AND v54f_ISO_D.Netdividend_1 IS NOT NULL
     THEN ':92F::NETT//'+CurenCD_1+ltrim(cast(v54f_ISO_D.Netdividend_1 as char(15)))
     ELSE ':92K::NETT//UKWN'
     END as COMMASUB,
CASE WHEN v54f_ISO_D.Grossdividend_1 <> ''
             AND v54f_ISO_D.Grossdividend_1 IS NOT NULL
     THEN ':92F::GRSS//'+CurenCD_1+ltrim(cast(v54f_ISO_D.Grossdividend_1 as char(15)))
     ELSE ':92K::GRSS//UKWN'
     END as COMMASUB,
CASE WHEN v54f_ISO_D.Taxrate_1 <> ''
     THEN ':92A::TAXR//'+cast(v54f_ISO_D.Taxrate_1 as char(15))
     ELSE ':92K::TAXR//UKWN' 
     END as COMMASUB,
':16S:CAOPTN',
':16R:ADDINFO',
/* Notes populated by rendering programme :70E::TXNR// */
'select  DivNotes As Notes FROM DIV WHERE DIVID = '+ cast(v54f_ISO_D.EventID as char(16)) as Notes, 
':16S:ADDINFO',
'-}$'
From v54f_ISO_D
WHERE 
exdate > '2006/04/03' and
SID <> ''
and (DIVType_1 = 'C'
AND OpID_2 = ''
AND OpID_3 = ''
AND OpID_4 = ''
AND OpID_5 = ''
AND OpID_6 = '')
/*and (sedol = 'ZZZZZZZ')*/
order by caref1, caref2, caref3
