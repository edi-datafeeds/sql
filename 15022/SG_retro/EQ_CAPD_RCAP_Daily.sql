--SEMETABLE=client.dbo.seme_SGCIB
--FileName=aptp_tlmca_ca_camkt_YYYYMMDD
--CAPD_RCAP_EQ564_YYYYMMDD
--FileNameNotes=15022_EQ568.TXT
--FileExtension=.txt
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--archive=off
--ArchivePath=n:\15022\RAT\
--FIELDHEADERS=off
--FileTidy=N
--TFBNUMBER=2
--INCREMENTAL=ON
--NOTESLIMIT=350
--TEXTCLEAN=2

--# 1
use wca
select distinct
case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end as Changed,
'select case when len(cast(rcapnotes as varchar(255)))>40 then RcapNotes else rtrim(char(32)) end As Notes FROM RCAP WHERE RCAPID = '+ cast(EventID as char(16)) as ChkNotes,
case when 1=1 then (select '{1:F01SOGEGB22AXXX0300000054}{2:O564'+(select replace(convert(varchar(5),max(wca.dbo.tbl_opslog.acttime), 8),':','') from wca.dbo.tbl_opslog where seq=3)+(select convert(varchar(30),max(wca.dbo.tbl_opslog.feeddate), 12) from wca.dbo.tbl_opslog where seq=3)+'EDISGB2LAXXX1234512345'+(select convert(varchar(30),getdate(), 12))+(select replace(convert(varchar(5),getdate(), 8),':',''))+'N}{4:') end as ISOHDR,
':16R:GENL',
CASE WHEN maintab.effectivedate>=(select max(feeddate) from wca.dbo.tbl_Opslog) and maintab.effectivedate is not null
              and SR.TFB<>':35B:/GB/'+maintab.Sedol
     THEN cast(cast(SR.CAREF1 as integer)+200 as varchar(3))
     WHEN SR.CAREF1 is not null
     THEN SR.CAREF1
     ELSE '103' END as CAref1,
maintab.EventID as CAREF2,
maintab.SedolID as CAREF3,
'' as SRID,
CASE WHEN maintab.Actflag = 'I'
     THEN ':23G:NEWM'
     WHEN maintab.Actflag = 'U'
     THEN ':23G:REPL'
     WHEN maintab.Actflag = 'D'
     THEN ':23G:CANC'
     WHEN maintab.Actflag = 'C'
     THEN ':23G:WITH'
     ELSE ''
     END AS MessageStatus,
':22F::CAEP//DISN',
':22F::CAEV//CAPD' as CAEV,
':22F::CAMV//MAND' as CHOICE,
':98C::PREP//'+CONVERT ( varchar , case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,112) + replace(convert ( varchar, case when maintab.sedolacttime>maintab.Changed then maintab.sedolacttime else maintab.Changed end,08),':','') as prep,
':25D::PROC//COMP' as PROCCOMP, 
'' as PSRID,
'' as link568,
':16S:GENL',
':16R:USECU',
':97C::SAFE//GENR' as MT568only,
':35B:/GB/' + maintab.Sedol as TFB,
substring(Issuername,1,35) as TIDYTEXT,
substring(maintab.Securitydesc,1,35) as TIDYTEXT,
CASE WHEN maintab.isin <>'' THEN 'ISIN '+maintab.isin
     ELSE '' END,
':16R:FIA',
CASE WHEN maintab.opol is null or maintab.opol ='' or maintab.opol='XXXX' or maintab.opol='XFMQ'
     THEN ':94B::PLIS//SECM'
     ELSE ':94B::PLIS//EXCH/'+upper(maintab.opol)
     END,
/* CASE WHEN pvcurrency<>'' THEN ':11A::DENO//' + pvcurrency END, */
':16S:FIA',
':16R:ACCTINFO',
':97C::SAFE//GENR' as MT564only,
':16S:ACCTINFO',
':16S:USECU',
':16R:CADETL',
CASE WHEN maintab.cntrycd='TW' THEN ':98A::ANOU//'+CONVERT(varchar, maintab.AnnounceDate,112) ELSE '' END,
CASE WHEN (maintab.cntrycd='HK' or maintab.cntrycd='SG' or maintab.cntrycd='TH' or maintab.cntrycd='MY') and RegistrationDate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT (varchar , RegistrationDate,112)
     WHEN Recdate IS NOT NULL
     THEN ':98A::RDTE//'+CONVERT ( varchar , recdate,112)
     ELSE ':98B::RDTE//UKWN'
     END,
CASE WHEN effectivedate is not null
     THEN ':98A::XDTE//'+CONVERT ( varchar , EffectiveDate,112)
     ELSE ':98B::XDTE//UKWN'
     END,
':22F::DIVI//SPEC',
':16S:CADETL',
':16R:CAOPTN',
':13A::CAON//001',
':22F::CAOP//CASH',
CASE WHEN maintab.CurenCD<>'' AND maintab.CurenCD IS NOT NULL
     THEN ':11A::OPTN//' + maintab.CurenCD ELSE ''
     END,
':17B::DFLT//Y',
':16R:CASHMOVE',
':22H::CRDB//CRED',
CASE WHEN CSPYDate IS NOT NULL
     THEN ':98A::PAYD//'+CONVERT ( varchar , CSPYDate,112)
     ELSE ':98B::PAYD//UKWN'
     END,
CASE WHEN CashBak<>'' AND CashBak IS NOT NULL
     THEN ':92F::GRSS//'+maintab.CurenCD+substring(CashBak,1,15)
     ELSE ':92K::GRSS//UKWN'
     END as Commasub,
':16S:CASHMOVE',
':16S:CAOPTN',
'' as Notes,
'-}'
From v_EQ_CAPG_RCAP as maintab
left outer join client.dbo.seme_SGCIB as SR on maintab.eventid=SR.caref2
                                         and maintab.sedolid=SR.caref3
                                         and 'SYSC'<>SR.T3G
                                         and '568'<>SR.MTflag
                                         and 'CAPD'=SR.CAEVMAIN
WHERE
maintab.sedol<>'' and maintab.sedol is not null
and ((maintab.sedol in (select code from client.dbo.pfsedol where accid=355 and actflag='I')
and effectivedate > getdate())
OR
(maintab.sedol in (select code from client.dbo.pfsedol where accid=355 and actflag='U')
and changed>'2017/03/07 11:00:00' and changed<'2017/03/07 16:30:00'))
and (maintab.rocbasis='RES' or maintab.rocbasis is null or maintab.rocbasis='')
and caprdid is null
order by caref2 desc, caref1, caref3
