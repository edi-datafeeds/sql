--filepath=o:\
--fileheadertext=
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=_FR
--fileheadertext=EDI_STATIC_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select * 
FROM x50f_620_Company_Meeting 
WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
use wca
select *
FROM v53f_620_Call
WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
use wca
select * 
FROM v50f_620_Liquidation
WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
use wca
select *
FROM v51f_620_Certificate_Exchange
WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
use wca
select * 
FROM x51f_620_International_Code_Change

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
use wca
select * 
FROM v51f_620_Conversion_Terms

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
select * 
FROM v51f_620_Redemption_Terms

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
select * 
FROM v51f_620_Security_Reclassification

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
select * 
FROM v52f_620_Lot_Change

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
select * 
FROM x52f_620_Sedol_Change

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca
select * 
FROM v53f_620_Buy_Back

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
select * 
FROM v53f_620_Capital_Reduction

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
use wca
select * 
FROM v53f_620_Takeover

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
use wca
select * 
FROM v54f_620_Arrangement

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
select * 
FROM v54f_620_Bonus

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
select * 
FROM v54f_620_Bonus_Rights

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
select * 
FROM v54f_620_Consolidation

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca
select * 
FROM v54f_620_Demerger

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
select * 
FROM v54f_620_Distribution

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
use wca
select * 
FROM v54f_620_Divestment

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
select * 
FROM v54f_620_Entitlement

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
use wca
select * 
FROM v54f_620_Merger

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
use wca
select * 
FROM v54f_620_Preferential_Offer

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
select * 
FROM v54f_620_Purchase_Offer

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
select * 
FROM v54f_620_Rights 

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
select * 
FROM v54f_620_Security_Swap 

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
select *
FROM v54f_620_Subdivision

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
select *
FROM v50f_620_Bankruptcy 

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
select *
FROM v50f_620_Financial_Year_Change

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
select *
FROM v50f_620_Incorporation_Change

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
select *
FROM x50f_620_Issuer_Name_change

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
select *
FROM v50f_620_Class_Action

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
select *
FROM x51f_620_Security_Description_Change

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
select *
FROM v52f_620_Assimilation

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
select *
FROM x52f_620_Listing_Status_Change

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
select *
FROM x52f_620_Local_Code_Change

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
select * 
FROM x52f_620_New_Listing

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
select * 
FROM x50f_620_Announcement

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
use wca
select * 
FROM x51f_620_Parvalue_Redenomination 

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
use wca
select * 
FROM v51f_620_Currency_Redenomination 

WHERE created >= '2007/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
use wca
select * 
FROM v53f_620_Return_of_Capital 

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
use wca
select * 
FROM x54f_620_Dividend
WHERE created >= '2002/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
use wca
select * 
FROM v54f_620_Dividend_Reinvestment_Plan

WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
use wca
select * 
FROM v54f_620_Franking
WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 46
use wca
select *
FROM v51f_620_Conversion_Terms_Change
WHERE created >= '2007/01/01'
and (ExCountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol
