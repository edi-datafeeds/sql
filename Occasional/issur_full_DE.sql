--filepath=o:\
--fileheadertext=
--filenameprefix=
--filename=
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISSUER_LIST_DE
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select distinct
issur.issid,
issuername,
indusid,
website
from issur
left outer join isscn on issur.issid = isscn.issid
inner join scmst on issur.issid = scmst.issid
inner join scexh on scmst.secid = scexh.secid
where substring(exchgcd,1,2) = 'DE'
and liststatus<>'D'
and statusflag<>'I'
and scexh.actflag<>'D'
and scmst.actflag<>'D'
and issur.actflag<>'D'
