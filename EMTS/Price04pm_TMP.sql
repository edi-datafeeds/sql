--FilePath=O:\Datafeed\EMTS\prices\
--FileHeaderText=
--FileHeaderDate=
--FileNamePrefix=YB
--suffix=-PRICES04PM
--FileName=YYYYMMDD
--FileExtension=.CSV
--filenamealt=SELECT MAX(snap_shot_date) FROM mts_emts.prices WHERE snap_shot_time = '16:00:00' and snap_shot_date =  '2010-03-02'
--FieldHeaders=y
--FieldSeparator=,
--Archive=y
--ArchivePath=N:\EMTS\prices\
--FileTidy=Y
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime
--FileFooterText=

--# 1
SELECT DISTINCT
prices.description,
prices.isin,
prices.snap_shot_date,
prices.snap_shot_time,
prices.ref_price,
prices.ref_yield FROM mts_emts.prices
WHERE snap_shot_date =  '2010-03-02'
AND snap_shot_time = '16:00:00'