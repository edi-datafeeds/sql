--FilePath=O:\Datafeed\Debt\EuroMTS\
--FilePrefix=
--suffix=_NewIssue
--FileName=YYYYMMDD
--FileExtension=.TXT
--filenamealt=SELECT MAX(ref_date) FROM mts_emts.refdata
--FieldHeaders=y
--FieldSeparator=	
--archive=y
--ArchivePath=N:\debt\euroMTS\
--FileTidy=Y
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
SELECT * FROM mts_emts.refdata
WHERE creation_date = (SELECT MAX(ref_date) FROM mts_emts.refdata)
AND actflag = 'I'