--FilePath=O:\Datafeed\EMTS\prices_IT_5DP\
--FileHeaderText=
--FileHeaderDate=
--FileNamePrefix=YB
--suffix=-PRICES5DP
--FileName=YYYYMMDD
--FileExtension=.CSV
--filenamealt=SELECT MAX(replace(ref_date,'-','')) FROM mts_emts.prices_it_5dp WHERE ref_date = '2010-06-04'
--FieldHeaders=y
--FieldSeparator=,
--Archive=y
--ArchivePath=N:\EMTS\prices_IT_5DP\
--FileTidy=y
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime
--FileFooterText=

--# 1
SELECT DISTINCT * FROM mts_emts.prices_it_5dp
WHERE ref_date =  (SELECT MAX(replace(ref_date,'-','')) FROM mts_emts.prices_it_5dp WHERE ref_date = '2010-06-04')