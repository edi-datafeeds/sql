--listed wca cts
use wca
SELECT cntry.country, Count(distinct scmst.secid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on issur.cntryofincorp = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus<>'D'
AND scexh.exchgcd=scmst.primaryexchgcd
GROUP BY cntry.country order by cntry.country

SELECT 'Total', Count(distinct scmst.secid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on issur.cntryofincorp = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus<>'D'
AND scexh.exchgcd=scmst.primaryexchgcd

--delisted wca cts
use wca
SELECT cntry.country, Count(distinct scmst.secid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on issur.cntryofincorp = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and (scmst.statusflag='I' OR scexh.liststatus='D')
and scexh.exchgcd=scmst.primaryexchgcd
GROUP BY cntry.country order by cntry.country

USE WCA
SELECT 'Total', Count(distinct scmst.secid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on issur.cntryofincorp = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and (scmst.statusflag='I' OR scexh.liststatus='D')
and scexh.exchgcd=scmst.primaryexchgcd

-- SMF listed cts
use smf4
select wca.dbo.cntry.country, count(distinct code) as Ct
from portfolio.dbo.cibcisin2
left outer join security on portfolio.dbo.cibcisin2.code = security.isin
left outer join issuer on security.issuerid = issuer.issuerid
left outer join wca.dbo.cntry on issuer.cinccode = wca.dbo.cntry.cntrycd
where statusflag<>'D'
and opol<>'XXXX'
and opol<>'XFMQ'
GROUP BY wca.dbo.cntry.country order by wca.dbo.cntry.country

use smf4
select 'SMF_UNLISTED', count(distinct code) as Ct
from portfolio.dbo.cibcisin2
left outer join security on portfolio.dbo.cibcisin2.code = security.isin
where statusflag<>'D'
and (opol='XXXX' or opol='XFMQ')

use smf4
select 'Total', count(distinct code) as Ct
from portfolio.dbo.cibcisin2
left outer join security on portfolio.dbo.cibcisin2.code = security.isin
where statusflag<>'D'
--and mic.country is not null

-- smf delisted cts
use smf4
select wca.dbo.cntry.country, count(distinct code) as Ct
from portfolio.dbo.cibcisin2
left outer join security on portfolio.dbo.cibcisin2.code = security.isin
left outer join issuer on security.issuerid = issuer.issuerid
left outer join wca.dbo.cntry on issuer.cinccode = wca.dbo.cntry.cntrycd
where statusflag='D'
and opol<>'XXXX'
and opol<>'XFMQ'
GROUP BY wca.dbo.cntry.country order by wca.dbo.cntry.country

use smf4
select 'SMF_UNLISTED', count(distinct code) as Ct
from portfolio.dbo.cibcisin2
left outer join security on portfolio.dbo.cibcisin2.code = security.isin
where statusflag='D'
and (opol='XXXX' or opol='XFMQ')

use smf4
select 'Total', count(distinct code) as Ct
from portfolio.dbo.cibcisin2
left outer join security on portfolio.dbo.cibcisin2.code = security.isin
where statusflag='D'
--and mic.country is not null

-- cant match to anything
use smf4
select 'Unmatched', count(distinct code) as Ct
from portfolio.dbo.cibcisin2
left join security on portfolio.dbo.cibcisin2.code = security.isin
where securityid is null
--and mic.country is not null










-- dump out isins unmatched in wca
USE WCA
SELECT distinct isin
--SELECT 'Total' , Count(scexh.scexhid) AS Ct
from client.dbo.pfisin
left outer join scmst on client.dbo.pfisin.code = scmst.isin
left outer join scexh on scmst.secid = scexh.secid
where
accid=999
and (scmst.isin is null or scexh.exchgcd<>scmst.primaryexchgcd)





SELECT distinct scmst.isin
into cibcwca
--SELECT 'Total' , Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on issur.cntryofincorp = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
AND scexh.exchgcd=scmst.primaryexchgcd

select distinct code 
into portfolio.dbo.cibcisin2
from client.dbo.pfisin
where
code not in (select isin from wca.dbo.cibcwca)
and accid=999

