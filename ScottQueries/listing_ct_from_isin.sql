--listed global counts
use wca
SELECT cntry.country, Count(distinct scmst.secid) AS Ct
--SELECT cntry.country, Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on exchg.cntrycd = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus<>'D'
GROUP BY cntry.country order by cntry.country


SELECT 'Total', Count(distinct scmst.secid) AS Ct
--SELECT 'Total' , Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on exchg.cntrycd = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus<>'D'


--delisted global counts
use wca
SELECT cntry.country, Count(distinct scmst.secid) AS Ct
--SELECT cntry.country, Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on exchg.cntrycd = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus='D'
GROUP BY cntry.country order by cntry.country


SELECT 'Total', Count(distinct scmst.secid) AS Ct
--SELECT 'Total' , Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on exchg.cntrycd = cntry.cntrycd
where
scmst.isin in (select code from client.dbo.pfisin where accid = 999)
and scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus='D'

-- get unmatched code into temp table
SELECT distinct code
into portfolio.dbo.cibcisin
--SELECT 'Total' , Count(scexh.scexhid) AS Ct
FROM client.dbo.pfisin
left outer join scmst on code = isin
where
scmst.secid is null

select * from portfolio.dbo.cibcisin

-- check unmatched code against SMF
use smf4
select mic.country, count(distinct code) as Ct
from portfolio.dbo.cibcisin
left join security on portfolio.dbo.cibcisin.code = security.isin
left outer join mic on opol = mic
where statusflag<>'D'
--and mic.country is not null
GROUP BY mic.country order by mic.country

-- check unmatched code against SMF
use smf4
select 'Total', count(distinct code) as Ct
from portfolio.dbo.cibcisin
left join security on portfolio.dbo.cibcisin.code = security.isin
left outer join mic on opol = mic
where statusflag<>'D'
--and mic.country is not null

-- check unmatched code against SMF
use smf4
select mic.country, count(distinct code) as Ct
from portfolio.dbo.cibcisin
left join security on portfolio.dbo.cibcisin.code = security.isin
left outer join mic on opol = mic
where statusflag='D'
--and mic.country is not null
GROUP BY mic.country order by mic.country

-- check unmatched code against SMF
use smf4
select 'Total', count(distinct code) as Ct
from portfolio.dbo.cibcisin
left join security on portfolio.dbo.cibcisin.code = security.isin
left outer join mic on opol = mic
where statusflag='D'
--and mic.country is not null

-- check unmatched code against SMF
use smf4
select 'Unmatched', count(distinct code) as Ct
from portfolio.dbo.cibcisin
left join security on portfolio.dbo.cibcisin.code = security.isin
left outer join mic on opol = mic
where securityid is null
--and mic.country is not null