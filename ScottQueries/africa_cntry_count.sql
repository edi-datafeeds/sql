use wca
SELECT cntry.country, Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on exchg.cntrycd = cntry.cntrycd
where
scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus<>'D'
and sectygrp.secgrpid<4
and exchg.cntrycd in (select cntrycd from continent where continent='Africa')
GROUP BY cntry.country order by cntry.country


SELECT 'Total', Count(scexh.scexhid) AS Ct
FROM scexh
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join cntry on exchg.cntrycd = cntry.cntrycd
where
scexh.actflag<>'D'
and scmst.statusflag<>'I'
and scexh.liststatus<>'D'
and sectygrp.secgrpid<4
and exchg.cntrycd in (select cntrycd from continent where continent='Africa')
