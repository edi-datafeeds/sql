use wol
select PSecID, CabEpoch, CabEdition, SitCD, SitText, 
TerText, RDDText, OffText, FurText, CabStatus, ChangedBy, 
Sentby, DisplayDate, PIDocLink, DisplayID, DisplaySeq, 
Issuername, SecDesc, Acttime
FROM wol.dbo.Cab 
where cabstatus = 'sent'
and displaydate>=(select max(FromDate) from FEEDLOG where FeedFreq = 'BaharRTF')
and displaydate<(select max(ToDate) from FEEDLOG where FeedFreq = 'BaharRTF')
