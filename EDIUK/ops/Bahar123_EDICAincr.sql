print " Generating Bahar123EDICABaharFlat, please wait..."
go
use smf4
declare @fromdate smalldatetime
declare @todate smalldatetime

select @fromdate = max(smf4.dbo.feedlog.fromdate) from smf4.dbo.feedlog where smf4.dbo.feedlog.feedfreq = '123incr'
select @todate = max(smf4.dbo.feedlog.todate) from smf4.dbo.feedlog where smf4.dbo.feedlog.feedfreq = '123incr'
if exists (select * from sysobjects where name = 'EDICABaharFlat')
	drop table EDICABaharFlat
SELECT 
EDICADetail.SecurityId, 
EDICADetail.CadID, 
EDICAOption.CadoID, 
case when EDICADetail.Headerdate>EDICAoption.Headerdate then EDICADetail.Headerdate else EDICAoption.Headerdate end as Headerdate,
tempActflag =
case
  WHEN EDICADetail.actdate < EDICAOption.actdate THEN EDICAOption.actflag
else
  EDICADetail.actflag
END
Into EDICABaharFlat
FROM EDICADetail
INNER JOIN EDICAOption ON (EDICADetail.Securityid = EDICAOption.SecurityId) AND (EDICADetail.CadID = EDICAOption.CadID)
WHERE EDICADetail.Actdate >= @fromdate and EDICADetail.Actdate < @Todate
and EDICADetail.Status = 'S'
UNION
SELECT 
EDICAOption.SecurityId,
EDICAOption.CadID,
EDICAOption.CadoID,
case when EDICADetail.Headerdate>EDICAoption.Headerdate then EDICADetail.Headerdate else EDICAoption.Headerdate end as Headerdate,
tempActflag =
case
  WHEN EDICADetail.actdate < EDICAOption.actdate THEN EDICAOption.actflag
else
  EDICADetail.actflag
END
FROM EDICAOption
INNER JOIN EDICADetail ON (EDICAOption.Securityid = EDICADetail.SecurityId) AND (EDICAOption.CadID = EDICADetail.CadID)
WHERE EDICAOption.Actdate >= @fromdate and EDICAOption.Actdate < @Todate
go