use smf4
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = '123incr')
set @tdate = getdate()

if @fdate<@tdate-0.02
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( '123incr', @fdate, @tdate)
  end
  go
