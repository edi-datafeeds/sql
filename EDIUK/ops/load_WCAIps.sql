
print ""
go
print "STEP-3 delete all unconfirmed EDICAOPTION records"
go
print ""
go
use smf4
delete from edicaoption
where exists 
(select
wca.dbo.v54f_920_Interest_Payment.eventid
from wca.dbo.v54f_920_Interest_Payment
where 
wca.dbo.v54f_920_Interest_Payment.ExCountry='GB'
and edicaoption.CADID = wca.dbo.v54f_920_Interest_Payment.eventid
and edicaoption.distsedol='WCAI'
and changed = (select max(acttime) from wca.dbo.tbl_Opslog))

go

print ""
go
print "STEP-4 delete all unconfirmed EDICADETAIL records"
go
print ""
go
use smf4
delete from edicadetail
where exists 
(select
wca.dbo.v54f_920_Interest_Payment.eventid
from wca.dbo.v54f_920_Interest_Payment
where 
wca.dbo.v54f_920_Interest_Payment.ExCountry='GB'
and edicadetail.CADID = wca.dbo.v54f_920_Interest_Payment.eventid
and edicadetail.SsnYear='WCAI'
and changed = (select max(acttime) from wca.dbo.tbl_Opslog))

go



print ""
go
print "STEP-5 insert EDICADETAIL records where key not found"
go
print ""
go

use smf4
insert into edicadetail
(Actflag,
Actdate,
CADID,
SecurityID, 
Closedate,
Exdate,
PayDate,
PeriodStartDate,
PeriodEndDate,
Confirmation,
credate,
ssnyear,
CAtype,
Status)
select distinct
wca.dbo.v54f_920_Interest_Payment.Actflag,
changed,
wca.dbo.v54f_920_Interest_Payment.eventid,
security.securityID,
wca.dbo.v54f_920_Interest_Payment.RecDate as Closedate,
wca.dbo.v54f_920_Interest_Payment.Exdate,
wca.dbo.v54f_920_Interest_Payment.Paydate,
wca.dbo.v54f_920_Interest_Payment.IntFromDate as PeriodStartDate,
wca.dbo.v54f_920_Interest_Payment.IntToDate as PeriodEndDate,
'C' as Confirmation,
wca.dbo.v54f_920_Interest_Payment.Created,
'WCAI' as ssnyear,
'IP' as CAType,
'R' as Status
from wca.dbo.v54f_920_Interest_Payment
inner join security on wca.dbo.v54f_920_Interest_Payment.sedol = security.sedol
where
changed = (select max(acttime) from wca.dbo.tbl_Opslog)
and ExCountry='GB'
go

print ""
go
print "STEP-6 insert EDICAOPTON records where key not found"
go
print ""
go

use smf4
insert into edicaoption
(Actflag,
Actdate,
CADID,
SecurityID,
Ratetype,
CashDistCallRate,
OptionCurr,
NetoftaxInd,
TaxRateInd,
UkTaxRate, 
credate,
DistSedol,
Comment)
select distinct
wca.dbo.v54f_920_Interest_Payment.Actflag,
wca.dbo.v54f_920_Interest_Payment.changed,
wca.dbo.v54f_920_Interest_Payment.eventid,
security.securityID,
'C' as Ratetype,
case when wca.dbo.v54f_920_Interest_Payment.Grossinterest<>'' then
                    wca.dbo.v54f_920_Interest_Payment.Grossinterest
     else wca.dbo.v54f_920_Interest_Payment.Netinterest end as CashDistCallRate,
wca.dbo.v54f_920_Interest_Payment.IntCurrency,
case when wca.dbo.v54f_920_Interest_Payment.Grossinterest<>'' then 'N'
     when wca.dbo.v54f_920_Interest_Payment.Netinterest<>'' then 'Y'
     else '' end as NetoftaxInd,
case when wca.dbo.v54f_920_Interest_Payment.Grossinterest<>'' then 'G'
     when wca.dbo.v54f_920_Interest_Payment.Netinterest<>'' then 'N'
     else '' end as TaxRateInd,
wca.dbo.v54f_920_Interest_Payment.DomesticTaxrate,
wca.dbo.v54f_920_Interest_Payment.Created,
'WCAI' as distsedol,
case when wca.dbo.v54f_920_Interest_Payment.Intrate<>'' then
            'Interest Rate: '+wca.dbo.v54f_920_Interest_Payment.Intrate
end as Comment
from wca.dbo.v54f_920_Interest_Payment
inner join smf4.dbo.security on wca.dbo.v54f_920_Interest_Payment.sedol = security.sedol
where
changed = (select max(acttime) from wca.dbo.tbl_Opslog)
and ExCountry='GB'

go