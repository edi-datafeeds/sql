use smf4
update edicadetail set edicadetail.taxrateind = 'N'
from edicaoption
left outer join edicadetail on edicaoption.cadid = edicadetail.cadid
where substring(1,1,edicadetail.catype) = 'D'
and (substring(1,1,edicadetail.taxrateind) = 'N'
or (edicadetail.taxrateind is null and substring(1,1,edicaoption.taxrateind)='N'))

use smf4
select edicadetail.taxrateind, edicaoption.taxrateind, edicaoption.netoftaxind
from edicaoption
left outer join edicadetail on edicaoption.cadid = edicadetail.cadid
where substring(edicadetail.catype,1,1) = 'D'
and (substring(edicadetail.taxrateind,1,1) = 'N'
or (edicadetail.taxrateind ='' and substring(edicaoption.taxrateind,1,1)='N'))

use smf4
select edicadetail.taxrateind, edicaoption.taxrateind, edicaoption.netoftaxind
from edicaoption
left outer join edicadetail on edicaoption.cadid = edicadetail.cadid
where substring(edicadetail.catype,1,1) = 'D'
and edicadetail.taxrateind is null
and edicadetail.confirmaTION='C'

use smf4
update edicadetail set edicadetail.taxrateind = 'N'
from edicadetail
where substring(edicadetail.catype,1,1) = 'D'
and substring(edicadetail.taxrateind,1,1) = 'N'