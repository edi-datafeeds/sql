use smf4
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'AFTERNOON')
set @tdate = getdate()

if @fdate<@tdate-0.75
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'AFTERNOON', @fdate, @tdate)
    print "SMF4 AFTERNOON Log record creation successful"
  end
else
  print "Already run today !!!"
  go

use wol
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'AFTERNOON')
set @tdate = getdate()

if @fdate<@tdate-0.75
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'AFTERNOON', @fdate, @tdate)
    print "Wincab AFTERNOON Log record creation successful"
  end
else
  print "Already run today !!!"
  go
  
  