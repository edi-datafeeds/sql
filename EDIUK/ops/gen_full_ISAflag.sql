
print " "
GO 
print " Deleting previous ISA table, please wait..."
go
print " "
GO 


use SMF4
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'ISA')
set @tdate = getdate()

if @fdate<@tdate-0.75
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'ISA', @fdate, @tdate)
    print "Log record creation successful"
    GO 
    use smf4
    if exists (select * from sysobjects where name = 'PREVISA')
    delete PREVISA
    GO
    use smf4
    insert into previsa
    (SecurityID, Acttime, Actflag, Isaflag)
    select 
    securityID,
    Acttime,
    Actflag,
    IsaFlag
    from isa
    GO
  end
else
  print "Already run today !!!"
  go
  
print " "
GO 
print " Deleting existing ISA table, please wait..."
go
print " "
GO 

use smf4
if exists (select * from sysobjects where name = 'ISA')
 delete ISA
GO

print " "
GO 
print " Generating ISA table from SECURITY not defunct, please wait..."
go
print " "
GO 

use smf4
insert into isa
(SecurityID, Acttime, Actflag, Isaflag)
select 
securityID,
getdate(),
'I',
''
from security
where security.statusflag<>'D'
GO

print " "
GO 
print " Set to no based on exchange only ,please  wait ....."
GO 
print " "
GO 

use smf4
update isa set isaflag='N', acttime = getdate(), actflag = 'U'
where
securityid in 
(select securityid from security 
where (opol = 'ALXB'
OR opol = 'ALXP'
OR opol = 'ENXL'
OR opol = 'ALXA'
OR opol = 'XTSX'
OR opol = 'XTNX'
OR opol = 'XGEM'
OR opol = 'XIEX'
or opol = 'XXXX'
--or opol = 'AIMX'
or opol = 'BTEC'
or opol = 'BTEE'
or opol = 'CANX'
or opol = 'DSMD'
or opol = 'EMTS'
or opol = 'EUWX'
or opol = 'ICEL'
or opol = 'IEPA'
or opol = 'JASR'
or opol = 'KOCN'
or opol = 'MDIP'
or opol = 'NNCS'
or opol = 'ONEX'
or opol = 'PFTS'
or opol = 'PINX'
or opol = 'ROCO'
or opol = 'RTSX'
or opol = 'THRD'
or opol = 'TREU'
or opol = 'TRWB'
or opol = 'URCE'
or opol = 'XABJ'
or opol = 'XADE'
or opol = 'XADS'
or opol = 'XALG'
or opol = 'XAMM'
or opol = 'XAOM'
or opol = 'XAPI'
or opol = 'XARM'
or opol = 'XAZX'
or opol = 'XBAA'
or opol = 'XBAB'
or opol = 'XBAH'
or opol = 'XBAN'
or opol = 'XBAV'
or opol = 'XBBF'
or opol = 'XBBJ'
or opol = 'XBCE'
or opol = 'XBCL'
or opol = 'XBDA'
or opol = 'XBEL'
or opol = 'XBEY'
or opol = 'XBMF'
or opol = 'XBNV'
or opol = 'XBOG'
or opol = 'XBOL'
or opol = 'XBOM'
or opol = 'XBOT'
or opol = 'XBOX'
or opol = 'XBRM'
or opol = 'XBRN'
or opol = 'XBSP'
or opol = 'XBTF'
or opol = 'XBUE'
or opol = 'XBVP'
or opol = 'XCAI'
or opol = 'XCAL'
or opol = 'XCAR'
or opol = 'XCAS'
or opol = 'XCCE'
or opol = 'XCEC'
or opol = 'XCET'
or opol = 'XCFE'
or opol = 'XCFF'
or opol = 'XCHG'
or opol = 'XCOR'
or opol = 'XCRC'
or opol = 'XCSC'
or opol = 'XCUE'
or opol = 'XDAR'
or opol = 'XDCE'
or opol = 'XDES'
or opol = 'XDFM'
or opol = 'XDHA'
or opol = 'XDMI'
or opol = 'XDPA'
or opol = 'XDRF'
or opol = 'XEAS'
or opol = 'XEEE'
or opol = 'XEMD'
or opol = 'XETR'
or opol = 'XEUC'
or opol = 'XEUE'
or opol = 'XEUI'
or opol = 'XEUR'
or opol = 'XFCM'
or opol = 'XFFE'
or opol = 'XFKA'
or opol = 'XFMN'
or opol = 'XFOM'
or opol = 'XGHA'
or opol = 'XGSE'
or opol = 'XGTG'
or opol = 'XGUA'
or opol = 'XHCE'
or opol = 'XHER'
or opol = 'XHKF'
or opol = 'XHON'
or opol = 'XIBE'
or opol = 'XIME'
or opol = 'XIMM'
or opol = 'XIPE'
or opol = 'XISL'
or opol = 'XIST'
or opol = 'XISX'
or opol = 'XJAM'
or opol = 'XJKT'
or opol = 'XJNB'
or opol = 'XJSE'
or opol = 'XJWY'
or opol = 'XKAC'
or opol = 'XKAR'
or opol = 'XKAZ'
or opol = 'XKBT'
or opol = 'XKCE'
or opol = 'XKFE'
or opol = 'XKGT'
or opol = 'XKHA'
or opol = 'XKHR'
or opol = 'XKIE'
or opol = 'XKKT'
or opol = 'XKOR'
or opol = 'XKOS'
or opol = 'XKST'
or opol = 'XKUW'
or opol = 'XLAH'
or opol = 'XLIF'
or opol = 'XLIM'
or opol = 'XLME'
or opol = 'XLOF'
or opol = 'XLUS'
or opol = 'XMAC'
or opol = 'XMAE'
or opol = 'XMAN'
or opol = 'XMAP'
or opol = 'XMAT'
or opol = 'XMAU'
or opol = 'XMCE'
or opol = 'XMDG'
or opol = 'XMDS'
or opol = 'XMEF'
or opol = 'XMER'
or opol = 'XMEV'
or opol = 'XMGE'
or opol = 'XMIC'
or opol = 'XMIF'
or opol = 'XMLI'
or opol = 'XMLX'
or opol = 'XMNT'
or opol = 'XMOD'
or opol = 'XMOL'
or opol = 'XMON'
or opol = 'XMOO'
or opol = 'XMOS'
or opol = 'XMRV'
or opol = 'XMSW'
or opol = 'XMUS'
or opol = 'XNAI'
or opol = 'XNAM'
or opol = 'XNEC'
or opol = 'XNEE'
or opol = 'XNEP'
or opol = 'XNEW'
or opol = 'XNGM'
or opol = 'XNKS'
or opol = 'XNSA'
or opol = 'XNSE'
or opol = 'XNST'
or opol = 'XNYC'
or opol = 'XNYF'
or opol = 'XNYM'
or opol = 'XODE'
or opol = 'XOSM'
or opol = 'XOST'
or opol = 'XOTB'
or opol = 'XOTC'
or opol = 'XPAE'
or opol = 'XPBT'
or opol = 'XPET'
or opol = 'XPHO'
or opol = 'XPHS'
or opol = 'XPIC'
or opol = 'XPOM'
or opol = 'XPOR'
or opol = 'XPRI'
or opol = 'XPTY'
or opol = 'XQTX'
or opol = 'XQUI'
or opol = 'XRAS'
or opol = 'XRMS'
or opol = 'XROS'
or opol = 'XROV'
or opol = 'XRUS'
or opol = 'XSAF'
or opol = 'XSAM'
or opol = 'XSAP'
or opol = 'XSAU'
or opol = 'XSCE'
or opol = 'XSES'
or opol = 'XSFA'
or opol = 'XSFE'
or opol = 'XSGE'
or opol = 'XSGO'
or opol = 'XSHE'
or opol = 'XSHG'
or opol = 'XSIB'
or opol = 'XSIC'
or opol = 'XSIM'
or opol = 'XSME'
or opol = 'XSTE'
or opol = 'XSUR'
or opol = 'XSVA'
or opol = 'XSWA'
or opol = 'XTAD'
or opol = 'XTAE'
or opol = 'XTAF'
or opol = 'XTAI'
or opol = 'XTEH'
or opol = 'XTFE'
or opol = 'XTFF'
or opol = 'XTFN'
or opol = 'XTIR'
or opol = 'XTKA'
or opol = 'XTKO'
or opol = 'XTKT'
or opol = 'XTNX'
or opol = 'XTOE'
or opol = 'XTRN'
or opol = 'XTUN'
or opol = 'XUGA'
or opol = 'XUKR'
or opol = 'XULA'
or opol = 'XUNI'
or opol = 'XUNL'
or opol = 'XVAL'
or opol = 'XVLA'
or opol = 'XVPA'
or opol = 'XWCE'
or opol = 'XYKT'
or opol = 'XZAG'
or opol = 'XZCE'
or opol = 'XZIM'
or opol = 'ZOBZ'
or opol = 'NOTC'
or opol = 'XBER'))
or securityid in (select securityid from market 
where market.mic = 'AIMX')
GO

print " "
GO 
print " Options and MISC excluded ,please  wait ....."
GO 
print " "
GO 

use smf4
update isa set isaflag= 'N', acttime = getdate(), actflag = 'U'
where
securityid in 
(select securityid from security 
where smf4.dbo.security.statusflag <> 'D'
AND
(sectype = 'CB'
OR sectype = 'CC'
OR sectype = 'CG'
OR sectype = 'DE'
OR sectype = 'DD'
OR sectype = 'DF'
OR sectype = 'DH'
OR sectype = 'DI'
OR sectype = 'DJ'
OR sectype = 'DK'
OR sectype = 'CI'))
GO

print " "
GO 
print " Depository receipts ,please  wait ....."
GO 
print " "
GO 

use smf4
update i2
set i2.isaflag= 'Y', i2.acttime = getdate(), i2.actflag = 'U'
from isa
inner join security on isa.securityid = security.securityid
inner join udr.dbo.udrnew on unsedol = sedol
INNER join security as s2 on udr.dbo.udrnew.drsedol = s2.sedol
INNER join isa as i2 on s2.securityid = i2.securityid

where

((UNEXCHANGE = 'AUASX'
OR UNEXCHANGE = 'ATVSE'
OR UNEXCHANGE = 'BEENB'
OR UNEXCHANGE = 'BRBVRJ'
OR UNEXCHANGE = 'BRSOMA'
OR UNEXCHANGE = 'BRBVRJ'
OR UNEXCHANGE = 'CATSE'
OR UNEXCHANGE = 'KYSX'
OR UNEXCHANGE = 'DKCSE'
OR UNEXCHANGE = 'FIHSE'
OR UNEXCHANGE = 'FRPEN'
OR UNEXCHANGE = 'DEFSX'
OR UNEXCHANGE = 'GRASE'
OR UNEXCHANGE = 'GGCISX'
OR UNEXCHANGE = 'ISISE'
OR UNEXCHANGE = 'IEISE'
OR UNEXCHANGE = 'ITMSE'
OR UNEXCHANGE = 'JPTSE'
OR UNEXCHANGE = 'KRKSE'
OR UNEXCHANGE = 'LULSE'
OR UNEXCHANGE = 'MYKLSE'
OR UNEXCHANGE = 'MTMSE'
OR UNEXCHANGE = 'MXMSE'
OR UNEXCHANGE = 'NZSE'
OR UNEXCHANGE = 'NOOB'
OR UNEXCHANGE = 'PTBVL'
OR UNEXCHANGE = 'SGSSE'
OR UNEXCHANGE = 'ZAJSE'
OR UNEXCHANGE = 'ESMSE'
OR UNEXCHANGE = 'LKCSE'
OR UNEXCHANGE = 'SESSE'
OR UNEXCHANGE = 'CHSSX'
OR UNEXCHANGE = 'THSET'
OR UNEXCHANGE = 'GBLSE'
OR UNEXCHANGE = 'GBOFX'
OR UNEXCHANGE = 'USBOS'
OR UNEXCHANGE = 'USCHI'
OR UNEXCHANGE = 'USCIN'
OR UNEXCHANGE = 'USPAC'
OR UNEXCHANGE = 'USAMEX'
OR UNEXCHANGE = 'USNASD'
OR UNEXCHANGE = 'USNYSE'
OR UNEXCHANGE = 'USPHIL'
OR UNEXCHANGE = 'DEDSE'
OR UNEXCHANGE = 'DEMSE'
OR UNEXCHANGE = 'DESSE'
OR UNEXCHANGE = 'CACNQ'
OR UNEXCHANGE = 'HKSEHK'
OR UNEXCHANGE = 'ESBSE'
OR UNEXCHANGE = 'ESBBSE'
OR UNEXCHANGE = 'JPJASD'
OR UNEXCHANGE = 'JPNSE'
OR UNEXCHANGE = 'JPOSE')

AND

(EXCHANGE = 'AUASX'
OR EXCHANGE = 'ATVSE'
OR EXCHANGE = 'BEENB'
OR EXCHANGE = 'BRBVRJ'
OR EXCHANGE = 'BRSOMA'
OR EXCHANGE = 'BRBVRJ'
OR EXCHANGE = 'CATSE'
OR EXCHANGE = 'KYSX'
OR EXCHANGE = 'DKCSE'
OR EXCHANGE = 'FIHSE'
OR EXCHANGE = 'FRPEN'
OR EXCHANGE = 'DEFSX'
OR EXCHANGE = 'GRASE'
OR EXCHANGE = 'GGCISX'
OR EXCHANGE = 'ISISE'
OR EXCHANGE = 'IEISE'
OR EXCHANGE = 'ITMSE'
OR EXCHANGE = 'JPTSE'
OR EXCHANGE = 'KRKSE'
OR EXCHANGE = 'LULSE'
OR EXCHANGE = 'MYKLSE'
OR EXCHANGE = 'MTMSE'
OR EXCHANGE = 'MXMSE'
OR EXCHANGE = 'NZSE'
OR EXCHANGE = 'NOOB'
OR EXCHANGE = 'PTBVL'
OR EXCHANGE = 'SGSSE'
OR EXCHANGE = 'ZAJSE'
OR EXCHANGE = 'ESMSE'
OR EXCHANGE = 'LKCSE'
OR EXCHANGE = 'SESSE'
OR EXCHANGE = 'CHSSX'
OR EXCHANGE = 'THSET'
OR EXCHANGE = 'GBLSE'
OR EXCHANGE = 'GBOFX'
OR EXCHANGE = 'USBOS'
OR EXCHANGE = 'USCHI'
OR EXCHANGE = 'USCIN'
OR EXCHANGE = 'USPAC'
OR EXCHANGE = 'USAMEX'
OR EXCHANGE = 'USNASD'
OR EXCHANGE = 'USNYSE'
OR EXCHANGE = 'USPHIL'
OR EXCHANGE = 'DEDSE'
OR EXCHANGE = 'DEMSE'
OR EXCHANGE = 'DESSE'
OR EXCHANGE = 'CACNQ'
OR EXCHANGE = 'HKSEHK'
OR EXCHANGE = 'ESBSE'
OR EXCHANGE = 'ESBBSE'
OR EXCHANGE = 'JPJASD'
OR EXCHANGE = 'JPNSE'
OR EXCHANGE = 'JPOSE'))
GO

print " "
GO 
print " Set ISA flag to yes on exchange, please  wait ....."
GO 
print " "
GO 

use smf4
update isa set isaflag='Y', acttime = getdate(), actflag = 'U'
where
securityid in 
(select securityid from security 
where smf4.dbo.security.statusflag <> 'D'
AND
(sectype = 'AG'
OR sectype = 'AB'
OR sectype = 'AF'
OR sectype = 'AJ'
OR sectype = 'AO'
OR sectype = 'AP'
OR sectype = 'AA'
or sectype = 'AD'
or sectype = 'DC')
and (opol = 'XASX'
OR OPOL = 'XWBO'
OR OPOL = 'XBRU'
OR OPOL = 'XRIO'
OR OPOL = 'XSOM'
OR OPOL = 'XBSP'
OR OPOL = 'XTSE'
OR OPOL = 'XCAY'
OR OPOL = 'XCSE'
OR OPOL = 'XHEL'
OR OPOL = 'XPAR'
OR OPOL = 'XFRA'
OR OPOL = 'XATH'
OR OPOL = 'XCIE'
OR OPOL = 'XICE'
OR OPOL = 'XDUB'
OR OPOL = 'XMIL'
OR OPOL = 'XTKO'
OR OPOL = 'XKRX'
OR OPOL = 'XLUX'
OR OPOL = 'XKLS'
OR OPOL = 'XMAL'
OR OPOL = 'XMEX'
OR OPOL = 'XNZE'
OR OPOL = 'XOSL'
OR OPOL = 'XLIS'
OR OPOL = 'XSES'
OR OPOL = 'XJSE'
OR OPOL = 'XMAD'
OR OPOL = 'XCOL'
OR OPOL = 'XOME'
OR OPOL = 'XSWX'
OR OPOL = 'XBKK'
OR OPOL = 'XLON'
OR OPOL = 'XPLU'
OR OPOL = 'XCBT'
OR OPOL = 'XBOS'
OR OPOL = 'XCHI'
OR OPOL = 'XCIS'
OR OPOL = 'ARCX'
OR OPOL = 'XASE'
OR OPOL = 'XISX'
OR OPOL = 'XNAS'
OR OPOL = 'XNGS'
OR OPOL = 'XNMS'
OR OPOL = 'XNYS'
OR OPOL = 'XPHL'
OR OPOL = 'XBRE'
OR OPOL = 'XDUS'
OR OPOL = 'XMUN'
OR OPOL = 'XSTU'
OR OPOL = 'XCNQ'
OR OPOL = 'XHKG'
or opol = 'GEMX'
or opol = 'XBAR'
or opol = 'XBIL'
or opol = 'XDWZ'
or opol = 'XJAS'
or opol = 'XNGO'
or opol = 'XOMF'
or opol = 'XOSE'
or opol = 'XTKS'
or opol = 'XBUL'
or opol = 'XCYS'
or opol = 'XPRA'
or opol = 'XTAL'
or opol = 'XBUD'
or opol = 'XLIT'
or opol = 'XRIS'
or opol = 'XWAR'
or opol = 'XBSE'
or opol = 'XLJU'
or opol = 'XBRA')
OR (OPOL = 'XBER'
AND ISIN LIKE 'DE%'))
GO

print " "
GO 
print " Allowing certain bonds, please  wait ....."
GO 
print " "
GO 

use smf4
update isa set isaflag='Y', acttime = getdate(), actflag = 'U'
where
securityid in 
(select securityid from security 
where smf4.dbo.security.statusflag <> 'D'
AND
(sectype = 'BA'
OR sectype = 'BB'
OR sectype = 'BC'
OR sectype = 'BD'
OR sectype = 'BE'
OR sectype = 'BF'
OR sectype = 'BG'
OR sectype = 'BH'
OR sectype = 'BI'
OR sectype = 'BJ'
OR sectype = 'BK'
OR sectype = 'BL'
OR sectype = 'BM'
OR sectype = 'BN'
OR sectype = 'BO'
OR sectype = 'BP'
OR sectype = 'BR'
OR sectype = 'BS'
OR SECTYPE = 'UB'
OR SECTYPE = 'UN'
OR SECTYPE = 'DA'
OR SECTYPE = 'DB'
OR SECTYPE = 'DG')
and (opol = 'XASX'
OR OPOL = 'XWBO'
OR OPOL = 'XBRU'
OR OPOL = 'XRIO'
OR OPOL = 'XSOM'
OR OPOL = 'XBSP'
OR OPOL = 'XTSE'
OR OPOL = 'XCAY'
OR OPOL = 'XCSE'
OR OPOL = 'XHEL'
OR OPOL = 'XPAR'
OR OPOL = 'XFRA'
OR OPOL = 'XATH'
OR OPOL = 'XCIE'
OR OPOL = 'XICE'
OR OPOL = 'XDUB'
OR OPOL = 'XMIL'
OR OPOL = 'XTKO'
OR OPOL = 'XKRX'
OR OPOL = 'XLUX'
OR OPOL = 'XKLS'
OR OPOL = 'XMAL'
OR OPOL = 'XMEX'
OR OPOL = 'XNZE'
OR OPOL = 'XOSL'
OR OPOL = 'XLIS'
OR OPOL = 'XSES'
OR OPOL = 'XJSE'
OR OPOL = 'XMAD'
OR OPOL = 'XCOL'
OR OPOL = 'XOME'
OR OPOL = 'XSWX'
OR OPOL = 'XBKK'
OR OPOL = 'XLON'
OR OPOL = 'XPLU'
OR OPOL = 'XCBT'
OR OPOL = 'XBOS'
OR OPOL = 'XCHI'
OR OPOL = 'XCIS'
OR OPOL = 'ARCX'
OR OPOL = 'XASE'
OR OPOL = 'XISX'
OR OPOL = 'XNAS'
OR OPOL = 'XNGS'
OR OPOL = 'XNMS'
OR OPOL = 'XNYS'
OR OPOL = 'XPHL'
OR OPOL = 'XBRE'
OR OPOL = 'XDUS'
OR OPOL = 'XMUN'
OR OPOL = 'XSTU'
OR OPOL = 'XCNQ'
OR OPOL = 'XHKG'
or opol = 'GEMX'
or opol = 'XBAR'
or opol = 'XBIL'
or opol = 'XDWZ'
or opol = 'XJAS'
or opol = 'XNGO'
or opol = 'XOMF'
or opol = 'XOSE'
or opol = 'XTKS'
or opol = 'XBUL'
or opol = 'XCYS'
or opol = 'XPRA'
or opol = 'XTAL'
or opol = 'XBUD'
or opol = 'XLIT'
or opol = 'XRIS'
or opol = 'XWAR'
or opol = 'XBSE'
or opol = 'XLJU'
or opol = 'XBRA'))
GO

print " "
GO 
print " Set conditional unit trusts, INCLUDES XFMQ OPOL, please  wait ....."
GO 
print " "
GO 

use smf4
update isa set isaflag='C', acttime = getdate(), actflag = 'U'
where
securityid in 
(select securityid from security 
where smf4.dbo.security.statusflag <> 'D'
AND
(sectype = 'CK'
OR sectype = 'CL'
OR sectype = 'CJ')
AND
smf4.dbo.security.ISIN LIKE 'GB%'
and 
(opol = 'XASX'
OR OPOL = 'XWBO'
OR OPOL = 'XBRU'
OR OPOL = 'XRIO'
OR OPOL = 'XSOM'
OR OPOL = 'XBSP'
OR OPOL = 'XTSE'
OR OPOL = 'XCAY'
OR OPOL = 'XCSE'
OR OPOL = 'XHEL'
OR OPOL = 'XPAR'
OR OPOL = 'XFRA'
OR OPOL = 'XATH'
OR OPOL = 'XCIE'
OR OPOL = 'XICE'
OR OPOL = 'XDUB'
OR OPOL = 'XMIL'
OR OPOL = 'XTKO'
OR OPOL = 'XKRX'
OR OPOL = 'XLUX'
OR OPOL = 'XKLS'
OR OPOL = 'XMAL'
OR OPOL = 'XMEX'
OR OPOL = 'XNZE'
OR OPOL = 'XOSL'
OR OPOL = 'XLIS'
OR OPOL = 'XSES'
OR OPOL = 'XJSE'
OR OPOL = 'XMAD'
OR OPOL = 'XCOL'
OR OPOL = 'XOME'
OR OPOL = 'XSWX'
OR OPOL = 'XBKK'
OR OPOL = 'XLON'
OR OPOL = 'XPLU'
OR OPOL = 'XCBT'
OR OPOL = 'XBOS'
OR OPOL = 'XCHI'
OR OPOL = 'XCIS'
OR OPOL = 'ARCX'
OR OPOL = 'XASE'
OR OPOL = 'XISX'
OR OPOL = 'XNAS'
OR OPOL = 'XNGS'
OR OPOL = 'XNMS'
OR OPOL = 'XNYS'
OR OPOL = 'XPHL'
OR OPOL = 'XBRE'
OR OPOL = 'XDUS'
OR OPOL = 'XMUN'
OR OPOL = 'XSTU'
OR OPOL = 'XCNQ'
OR OPOL = 'XHKG'
or opol = 'GEMX'
or opol = 'XBAR'
or opol = 'XBIL'
or opol = 'XDWZ'
or opol = 'XJAS'
or opol = 'XNGO'
or opol = 'XOMF'
or opol = 'XOSE'
or opol = 'XTKS'
or opol = 'XFMQ'
or opol = 'XBUL'
or opol = 'XCYS'
or opol = 'XPRA'
or opol = 'XTAL'
or opol = 'XBUD'
or opol = 'XLIT'
or opol = 'XRIS'
or opol = 'XWAR'
or opol = 'XBSE'
or opol = 'XLJU'
or opol = 'XBRA'
or opol = 'XFMQ'
OR OPOL = 'XXXX'))

GO

print " "
GO 
print " Applying close date restrictions, please wait..."
go
print " "
GO 

use smf4
update isa set isaflag= 'N', acttime = getdate(), actflag = 'U'
where
securityid in 
(select securityid from security 
where smf4.dbo.security.statusflag <> 'D'
AND
smf4.dbo.security.closedate is not null
and
smf4.dbo.security.closedate < getdate()+365*5+2)
go

print " "
GO 
print " Generating LISTLINK table to harmonise multiple listings, please wait..."
go
print " "
GO 

use smf4
if exists (select * from sysobjects where name = 'listlink')
 drop table listlink
GO

print " "
GO 
print " Flag ISA from flagged multiple listings, please  wait ....."
GO 
print " "
GO 

select distinct security.issuerid, security.longdesc
into listlink
from isa
inner join security on isa.securityid = security.securityid
where isa.isaflag = 'Y'
GO

print " "
GO 
print " Generating LISTLINK indices ,please  wait ....."
GO 
print " "
GO 

use smf4
ALTER TABLE LISTLINK ALTER COLUMN  issuerid int NOT NULL
ALTER TABLE LISTLINK ALTER COLUMN  longdesc varchar(40) NOT NULL
GO 

use smf4
ALTER TABLE [DBO].[listlink] WITH NOCHECK ADD 
 CONSTRAINT [pk_listlink] PRIMARY KEY ([issuerid],[longdesc])  ON [PRIMARY]
GO 

update isa
set isaflag = 'Y', acttime = getdate(), actflag = 'U'
from isa
inner join security on isa.securityid = security.securityid
inner join listlink on security.issuerid = listlink.issuerid
                and security.longdesc = listlink.longdesc
where isa.isaflag = 'N'
GO

print " "
GO 
print " Check ISAflag = 'Y' count is OK ....."
GO 
print " "
GO 

use smf4
select count(securityid) from ISA
where isaflag = 'Y'
GO
