use WCA
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'WCAD')
set @tdate = getdate()

if @fdate<@tdate-0.75
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'WCAD', @fdate, @tdate)
    print "SMF4 WCAD Log record creation successful"
  end
else
  print "Already run today !!!"
  go

use wol
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'WCAD')
set @tdate = getdate()

if @fdate<@tdate-0.70
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'WCAD', @fdate, @tdate)
    print "Wincab WCAD Log record creation successful"
  end
else
  print "Already run today !!!"
  go
  
  