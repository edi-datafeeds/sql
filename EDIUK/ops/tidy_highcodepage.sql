use wol
declare @mycount as integer
set @mycount=128

WHILE @mycount<256
BEGIN
   update cab set sittext = replace(cast(sittext as varchar(8000)), char(@mycount), '') where datalength(sittext) < 8000
   update cab set tertext = replace(cast(tertext as varchar(8000)), char(@mycount), '') where datalength(tertext) < 8000
   update cab set rddtext = replace(cast(rddtext as varchar(8000)), char(@mycount), '') where datalength(rddtext) < 8000
   update cab set offtext = replace(cast(offtext as varchar(8000)), char(@mycount), '') where datalength(offtext) < 8000
   update cab set furtext = replace(cast(furtext as varchar(8000)), char(@mycount), '') where datalength(furtext) < 8000
   set @mycount = @mycount+1
END
