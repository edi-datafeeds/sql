use wol
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'ISA')
set @tdate = getdate()

if @fdate<@tdate-0.3
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'ISA', @fdate, @tdate)
    print "Log record creation successful"
  end
else
  print "Already run in the last 8 hours !!!"
  go