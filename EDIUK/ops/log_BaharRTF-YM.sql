use wol
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'BaharRTF')
set @tdate = getdate()

if @fdate<@tdate-0.05
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'BaharRTF', @fdate, @tdate)
    print "Log record creation successful"
  end
else
  print "Already run in the last 60 minutes !!!"
  go