use smf4
delete edicaoption from edicaoption
left outer join edicadetail on edicaoption.cadid = edicadetail.cadid
where edicadetail.catype = 'SH'
or edicadetail.confirmation <> 'C'
go

use smf4
delete edicadetail from edicadetail
where edicadetail.catype = 'SH'
or edicadetail.confirmation <> 'C'
go
