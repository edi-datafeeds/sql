use ipo
declare @fdate datetime
declare @tdate datetime
set @fdate = (select max(ToDate) from FEEDLOG where FeedFreq = 'EVENING')
set @tdate = getdate()

if @fdate<@tdate-0.75
  begin
    insert into FEEDLOG (FeedFreq , FromDate, ToDate)
    values( 'EVENING', @fdate, @tdate)
    print "Log record creation successful"
  end
else
  print "Already run today !!!"
  go
  
  