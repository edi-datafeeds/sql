use smf4
declare @startdate smalldatetime
select @startdate = max(smf4.dbo.feedlog.fromdate) from smf4.dbo.feedlog where smf4.dbo.feedlog.feedfreq = 'AFTERNOON'
declare @enddate smalldatetime
select @enddate = max(smf4.dbo.feedlog.todate) from smf4.dbo.feedlog where smf4.dbo.feedlog.feedfreq = 'AFTERNOON'

if exists (select * from sysobjects where name = 'EDICAFlat')
	drop table EDICAFlat
SELECT EDICADetail.SecurityId, EDICADetail.CadID, EDICAOption.CadoID, 
case when EDICADetail.actdate>EDICAoption.actdate then EDICADetail.actdate else EDICAoption.actdate end as Headerdate,
tempActflag =
case
  WHEN EDICADetail.actdate < EDICAOption.actdate THEN EDICAOption.actflag
else
  EDICADetail.actflag
END
Into EDICAFlat
FROM EDICADetail
INNER JOIN EDICAOption ON (EDICADetail.Securityid = EDICAOption.SecurityId) AND (EDICADetail.CadID = EDICAOption.CadID)
WHERE EDICADetail.actdate >= @startdate and EDICADetail.actdate < @enddate
UNION
SELECT EDICAOption.SecurityId, EDICAOption.CadID, EDICAOption.CadoID,
case when EDICADetail.actdate>EDICAoption.actdate then EDICADetail.actdate else EDICAoption.actdate end as Headerdate,
tempActflag =
case
  WHEN EDICADetail.actdate < EDICAOption.actdate THEN EDICAOption.actflag
else
  EDICADetail.actflag
END
FROM EDICAOption
INNER JOIN EDICADetail ON (EDICAOption.Securityid = EDICADetail.SecurityId) AND (EDICAOption.CadID = EDICADetail.CadID)
WHERE EDICAOption.actdate >= @startdate and EDICAOption.actdate < @enddate
