print " Generating Bahar123EDICAFLAT, please wait..."
go
use smf4
declare @fromdate smalldatetime
declare @todate smalldatetime
select @fromdate = max(Fromdate) from FEEDLOG
select @todate = max(todate) from FEEDLOG
if exists (select * from sysobjects where name = 'EDICAFlat')
	drop table EDICAFlat
SELECT 
EDICADetail.SecurityId, 
EDICADetail.CadID, 
EDICAOption.CadoID, 
case when EDICADetail.actdate>EDICAoption.actdate then EDICADetail.actdate else EDICAoption.actdate end as Headerdate,
tempActflag =
case
  WHEN EDICADetail.actdate < EDICAOption.actdate THEN EDICAOption.actflag
else
  EDICADetail.actflag
END
Into EDICAFlat
FROM EDICADetail
INNER JOIN EDICAOption ON (EDICADetail.Securityid = EDICAOption.SecurityId) AND (EDICADetail.CadID = EDICAOption.CadID)
WHERE EDICADetail.Actdate >= @fromdate and EDICADetail.Actdate < @Todate
UNION
SELECT 
EDICAOption.SecurityId,
EDICAOption.CadID,
EDICAOption.CadoID,
case when EDICADetail.actdate>EDICAoption.actdate then EDICADetail.actdate else EDICAoption.actdate end as Headerdate,
tempActflag =
case
  WHEN EDICADetail.actdate < EDICAOption.actdate THEN EDICAOption.actflag
else
  EDICADetail.actflag
END
FROM EDICAOption
INNER JOIN EDICADetail ON (EDICAOption.Securityid = EDICADetail.SecurityId) AND (EDICAOption.CadID = EDICADetail.CadID)
WHERE EDICAOption.Actdate >= @fromdate and EDICAOption.Actdate < @Todate
