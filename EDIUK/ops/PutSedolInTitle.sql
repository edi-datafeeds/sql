use wol
update sec set isin = sedol
where isin is null or isin = '' or isin = 'NO ISIN'
go

use wol
update sec set title = title + ' ('+sec.sedol+')'
where sec.title not like '%'+sedol+'%'
and substring(isin,5,7)<>sedol
go
