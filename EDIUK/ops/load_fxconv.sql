print ''
go
print 'STEP-1 Drop table if exists'
go
print ''
go
use smf4
if exists (select * from sysobjects where name = 'EDIfxconv')
 drop table EDIfxconv
go
print ''
go
print ''
go
print 'STEP-2 Creating Table'
go
print ''
go

create table smf4.dbo.EDIfxconv
(
 
 cPayDate varchar(10),
 DistCurr char(3),
 ConvCurr char(3),
 Xrate    varchar(20),
 id       int IDENTITY PRIMARY KEY
 
)
go 
print ''
go
print ''
go
print 'STEP-3 Importing O:\Datafeed\EDIUK\inputs\EDIfxconv.txt'
go
print ''
go
print 'Rows Inserted'
go

BULK INSERT smf4.dbo.EDIfxconv FROM '\\192.168.2.163\ops\Datafeed\EDIUK\inputs\EDIfxconv.txt'
WITH (    
    FIELDTERMINATOR = ' ',
    TABLOCK     		
)
go

ALTER TABLE smf4.dbo.EDIfxconv 
ADD PayDate datetime NULL

go
print ''
go
print ''
go
print 'STEP-4 Checking Dates'
go
print ''
go

use smf4
update EDIfxconv
set paydate = substring(cPaydate,7,4)
            +'/'+substring(cPaydate,4,2)
            +'/'+substring(cPaydate,1,2)

go
print ''
go
print 'If error message re-check import file for bad dates.'
go
print ''
go