--filepath=o:\smfproc\di\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.030
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\030\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4

SELECT  
Security.Sedol,
EDICADetail.CADID AS DistribId,
EDICAOption.CADOID * 10 AS ElementID,
Issuer.Issuername+' '+Security.Longdesc AS SecurityDescription,
Security.Isin,
EDICAOption.OptionCurr as Currcode,
EDICADetail.HeaderDate As FeedDate,
EDICADetail.Exdate,
EDICADetail.closedate AS Recdate,
EDICADetail.Paydate,
EDICAOption.CashdistCallrate AS Payrate,
Case when EDICADetail.TaxRateInd = 'N' then 'Net'
else 'Gross' End AS Taxcode,
EDICADetail.uktaxrate,
rtrim(EDICADetail.divtype)+' '+rtrim(EDICAOption.comment) + ' ' + rtrim(EDICADetail.divfreq) as Comment,
Case when EDICADetail.CAType = 'DD' then 'CASH'
     when EDICADetail.CAType = 'DO' then 'DIVIDEND OPTION'
     when EDICADetail.CAType = 'SV' then 'SCRIP DIVIDEND'
     when EDICADetail.CAType = 'CP' then 'STOCK DIVIDEND'
     when EDICADetail.CAType = 'D2' then 'CURRENCY ELECTION'
Else '' End As Divtype,
substring(security.sectype,1,1) As Stocktype,
EDICAOption.Numerat,
EDICAOption.Denomin,
EDICADetail.credate AS CreationDate,
EDICAOPTION.Actflag
FROM EDICAOPTION
INNER JOIN Security ON Security.SecurityID = EDICAOPTION.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
INNER JOIN EDICADetail ON (EDICAOPTION.Securityid = EDICADetail.SecurityId) AND (EDICAOPTION.CADID = EDICADetail.CADID)
Where  
EDICADetail.status = 'S'
and (EDICADetail.CAType ='DD'
Or EDICADetail.CAType = 'DO'
Or EDICADetail.CAType = 'SV'
Or EDICADetail.CAType = 'CP'
Or EDICADetail.CAType = 'D2'
Or EDICADetail.CAType = 'IP')
ORDER BY Security.Sedol