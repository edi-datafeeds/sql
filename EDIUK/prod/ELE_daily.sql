--filepath=o:\Datafeed\EDIUK\ELE\
--filenameprefix=
--filenamesql=select convert(varchar(30),max(smf4.dbo.feedlog.fromdate), 112)+'000000'+'_'+ convert(varchar(30),max(smf4.dbo.feedlog.Todate), 112)+'000000'+'_'+'ELE_FDC_4' as Filenametext from smf4.dbo.feedlog  where smf4.dbo.feedlog.feedfreq = 'Bahar123'
--fileextension=.TXT
--suffix=
--fileheadersql=select 'H01'+convert(varchar(30),max(smf4.dbo.feedlog.fromdate), 112)+'000000'+ convert(varchar(30),max(smf4.dbo.feedlog.Todate), 112)+'000000ELE' as Fileheadertext from smf4.dbo.feedlog where smf4.dbo.feedlog.feedfreq = 'Bahar123'
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\ELE\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefootertext=T01
--reccount=nnnnnnnnnn


--# 1
Use smf4
Select
EDICAOption.Actflag,
case when(len(convert(varchar(30),EDICAOption.Actdate, 126))=19) then (convert(varchar(30),EDICAOption.Actdate, 126)+'.000Z') 
     when(len(convert(varchar(30),EDICAOption.Actdate, 126))=23) then (convert(varchar(30),EDICAOption.Actdate, 126)+'Z')  
     else '' end as Actdate,
EDICAOption.CADOID,
EDICAOption.CADID,
EDICAOption.SecurityID,
EDICAOption.CashDistCallRate,
EDICAOption.Denomin,
EDICAOption.Minfract,
EDICAOption.Numerat,
EDICAOption.Firstdate,
EDICAOption.Resflag,
EDICAOption.SecCodeOfDist,
EDICADetail.DivType,
EDICAOption.FracEnt,
EDICAOption.OptionCurr,
'false' as OverSubscrflag,
rtrim(EDICADetail.divtype)+' '+rtrim(EDICAOption.comment) + ' ' + rtrim(EDICADetail.divfreq) as Comment,
EDICAOption.TaxRateInd as netOfTaxInd,
EDICADetail.OsTaxCurr as OverseasTaxCurr,
EDICADetail.OsTaxRate as OverseasTaxRate,
EDICADetail.UkTaxCurr,
EDICADetail.UktaxRate,
EDICAOption.RateType,
'' as TaxRateInd,
EDICAOption.RateStatus,
EDICAOption.OvrSubscipRate
FROM EDICAFlat
INNER JOIN Security ON Security.SecurityID = EDICAFlat.SecurityID
INNER JOIN EDICADetail ON (EDICAFlat.Securityid = EDICADetail.SecurityId) AND (EDICAFlat.CadID = EDICADetail.CadID)
INNER JOIN EDICAOption ON (EDICAFlat.Securityid = EDICAOption.SecurityId) AND (EDICAFlat.CadID = EDICAOption.CadID) AND (EDICAFlat.CadoID = EDICAOption.CadoID)
where
EDICADetail.Confirmation = 'C'
and 
(  EDICADetail.CAType = 'DD'
or EDICADetail.CAType = 'IP'
or EDICADetail.CAType = 'D2'
or EDICADetail.CAType = 'DV'
or EDICADetail.CAType = 'DO'
or EDICADetail.CAType = 'SV'
or EDICADetail.CAType = 'CP')
