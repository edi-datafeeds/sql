--filepath=o:\Datafeed\Bahar\
--filenameprefix=missing_
--filename=yyyymmdd
--filenamealt=
--fileextension=.123
--suffix=
--fileheadertext=EDI_123_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\bahar\123\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select cast(count(smf4.dbo.feedlog.feedfreq) as char(1)) as seq FROM smf4.dbo.feedlog where todate>getdate()-0.4 and feedfreq = '123incr'       
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT 
security.sedol,
EDICAFlat.SecurityId,
EDICAFlat.CadID,
EDICAFlat.CadoID,
EDICAFlat.Headerdate,
EDICAFlat.tempActflag,
EDICADetail.Actflag,
EDICADetail.Actdate,
EDICADetail.CADID,
EDICADetail.SecurityID,
case when EDICAOption.Firstdate is not null then EDICAOption.Firstdate else EDICADetail.Acceptdate end as Acceptdate,
EDICADetail.Closedate,
EDICADetail.Exdate,
EDICADetail.Paydate,
EDICADetail.Snpdate,
EDICADetail.Sfpdate,
EDICADetail.ResultPubDate,
EDICADetail.Redemdate,
EDICADetail.CalculDate,
EDICADetail.PeriodStartDate,
EDICADetail.PeriodEndDate,
EDICADetail.EffecDate,
EDICADetail.CondSaleDate,
EDICADetail.WhollyUncondDate,
EDICADetail.AnnounceDate,
EDICADetail.ConversionDate,
EDICADetail.ConversionStatus,
EDICADetail.Ssn,
EDICADetail.Ssnyear,
EDICADetail.BusYrFrm,
EDICADetail.BusYrTo,
EDICADetail.MaxPrice,
EDICADetail.MinPrice,
EDICADetail.Quantity,
EDICADetail.MeetingDate,
EDICADetail.Confirmation,
EDICAOption.OptionType,
EDICADetail.CreDate,
EDICADetail.HeaderDate,
'' as Currcode,
'' as Typecode,
'' as Taxflag,
'' as Taxcurrcode,
EDICADetail.Ostaxrate,
EDICADetail.Uktaxrate,
EDICAOption.Actflag,
EDICAOption.Actdate,
EDICAOption.CADOID,
EDICAOption.CADID,
EDICAOption.SecurityID,
EDICAOption.CashDistCallRate,
EDICAOption.Denomin,
EDICAOption.Minfract,
EDICAOption.Numerat,
EDICAOption.Firstdate,
EDICAOption.Resflag,
EDICAOption.SecCodeOfDist,
EDICADetail.DivType,
EDICAOption.FracEnt,
EDICAOption.OptionCurr,
EDICAOption.OverSubscrflag,
EDICAOption.Comment,
'' as netOfTaxInd,
EDICADetail.OsTaxCurr as OverseasTaxCurr,
EDICADetail.OsTaxRate as OverseasTaxRate,
EDICADetail.UkTaxCurr,
EDICADetail.UktaxRate,
EDICAOption.RateType,
EDICADetail.TaxRateInd,
EDICAOption.RateStatus,
EDICAOption.OvrSubscipRate,
EDICAOption.CreDate,
EDICAOption.HeaderDate,
EDICAOption.Payrate,
EDICAOption.Distsedol
FROM EDICAFlat
INNER JOIN Security ON Security.SecurityID = EDICAFlat.SecurityID
INNER JOIN EDICADetail ON (EDICAFlat.Securityid = EDICADetail.SecurityId) AND (EDICAFlat.CadID = EDICADetail.CadID)
INNER JOIN EDICAOption ON (EDICAFlat.Securityid = EDICAOption.SecurityId) AND (EDICAFlat.CadID = EDICAOption.CadID) AND (EDICAFlat.CadoID = EDICAOption.CadoID)
left outer join tmp123 on (EDICAFlat.Securityid = tmp123.SecurityId) AND (EDICAFlat.CadID = tmp123.CadID)
where
tmp123.securityid is null
and 
(
EDICADetail.CAType='DD'
or EDICADetail.CAType = 'D2'
or EDICADetail.CAType = 'DV'
or EDICADetail.CAType = 'DO'
or EDICADetail.CAType = 'SV'
or EDICADetail.CAType = 'CP'
)
and EDICADetail.status = 'S'

ORDER BY Security.Sedol
