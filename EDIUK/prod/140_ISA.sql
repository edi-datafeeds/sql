--filepath=o:\Datafeed\EDIUK\140\
--filenameprefix= 
--filename=yyyymmdd
--filenamealt=select max(acttime) from smf4.dbo.isa
--fileextension=.140
--suffix=
--fileheadertext=EDI_ISA_140_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\140\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--filefootertext=EDI_ENDOFFILE

--# 1
use smf4
select 
'ISA CHANGES' as Reason,
issuername,
isin,
sedol,
isa.isaflag as NewISAflag,
previsa.isaflag as OldISAFlag,
longdesc,
statusflag
from isa
inner join previsa on isa.securityid = previsa.securityid
left outer join security on isa.securityid = security.securityid
left outer join issuer on security.issuerid = issuer.issuerid
where
previsa.isaflag<>isa.isaflag

union

select 
'ISA ADDITIONS' as Reason,
issuername,
isin,
sedol,
isa.isaflag as NewISAflag,
'' as OldISAFlag,
longdesc,
statusflag
from isa
left outer join previsa on isa.securityid = previsa.securityid
left outer join security on isa.securityid = security.securityid
left outer join issuer on security.issuerid = issuer.issuerid
where
previsa.securityID is null

union

select 
'ISA DELETIONS' as Reason,
issuername,
isin,
sedol,
'' as NewISAflag,
previsa.isaflag as OldISAFlag,
longdesc,
statusflag
from previsa
left outer join isa on previsa.securityid = isa.securityid
left outer join security on previsa.securityid = security.securityid
left outer join issuer on security.issuerid = issuer.issuerid
where
isa.securityID is null
order by reason, issuername

