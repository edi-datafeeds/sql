--filepath=o:\Datafeed\EDIUK\123\
--filenameprefix=AL
--filename=YYMMDD
--filenamealt=
--fileextension=.123
--suffix=
--fileheadertext=EDI_123_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\123\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4
SELECT 
security.sedol,
EDICADetail.SecurityId,
EDICADetail.CadID,
EDICAOption.CadoID,
EDICAOption.Headerdate,
EDICAOption.Actflag as tempActflag,
EDICADetail.Actflag,
EDICADetail.Actdate,
EDICADetail.CADID,
EDICADetail.SecurityID,
case when EDICAOption.Firstdate is not null then EDICAOption.Firstdate else EDICADetail.Acceptdate end as Acceptdate,
EDICADetail.Closedate,
EDICADetail.Exdate,
EDICADetail.Paydate,
EDICADetail.Snpdate,
EDICADetail.Sfpdate,
EDICADetail.ResultPubDate,
EDICADetail.Redemdate,
EDICADetail.CalculDate,
EDICADetail.PeriodStartDate,
EDICADetail.PeriodEndDate,
EDICADetail.EffecDate,
EDICADetail.CondSaleDate,
EDICADetail.WhollyUncondDate,
EDICADetail.AnnounceDate,
EDICADetail.ConversionDate,
EDICADetail.ConversionStatus,
EDICADetail.Ssn,
EDICADetail.Ssnyear,
EDICADetail.BusYrFrm,
EDICADetail.BusYrTo,
EDICADetail.MaxPrice,
EDICADetail.MinPrice,
EDICADetail.Quantity,
EDICADetail.MeetingDate,
EDICADetail.Confirmation,
EDICAOption.OptionType,
EDICADetail.CreDate,
EDICADetail.HeaderDate,
'' as Currcode,
'' as Typecode,
'' as Taxflag,
'' as Taxcurrcode,
EDICADetail.Ostaxrate,
EDICADetail.Uktaxrate,
EDICAOption.Actflag,
EDICAOption.Actdate,
EDICAOption.CADOID,
EDICAOption.CADID,
EDICAOption.SecurityID,
EDICAOption.CashDistCallRate,
EDICAOption.Denomin,
EDICAOption.Minfract,
EDICAOption.Numerat,
NULL as Firstdate,
EDICAOption.Resflag,
EDICAOption.SecCodeOfDist,
EDICADetail.DivType,
EDICAOption.FracEnt,
EDICAOption.OptionCurr,
EDICAOption.OverSubscrflag,
EDICAOption.Comment,
'' as netOfTaxInd,
EDICADetail.OsTaxCurr as OverseasTaxCurr,
EDICADetail.OsTaxRate as OverseasTaxRate,
EDICADetail.UkTaxCurr,
EDICADetail.UktaxRate,
EDICAOption.RateType,
EDICADetail.TaxRateInd,
EDICAOption.RateStatus,
EDICAOption.OvrSubscipRate,
EDICAOption.CreDate,
EDICAOption.HeaderDate,
EDICAOption.Payrate,
EDICAOption.Distsedol
FROM EDICAOption
INNER JOIN Security ON Security.SecurityID = EDICAOption.SecurityID
INNER JOIN EDICADetail ON (EDICAOption.Securityid = EDICADetail.SecurityId) AND (EDICAOption.CadID = EDICADetail.CadID)
where
EDICADetail.Confirmation<> 'U'
and EDICADetail.Status = 'S'
AND (EDICADetail.CAType='DD'
or EDICADetail.CAType = 'D2'
or EDICADetail.CAType = 'DV'
or EDICADetail.CAType = 'DO'
or EDICADetail.CAType = 'SV'
or EDICADetail.CAType = 'CP'
)
and ((EDICADetail.Actdate > '2006/10/01'  and EDICADetail.Actdate < '2007/03/27')
     or (EDICAOption.Actdate > '2006/10/01' and EDICAOption.Actdate < '2007/03/27'))
ORDER BY Security.Sedol
