--filepath=o:\Datafeed\EDIUK\DIS\
--filenameprefix=
--filenamealt=
--filenamesql=select convert(varchar(30),max(smf4.dbo.feedlog.fromdate), 112)+'000000'+'_'+ convert(varchar(30),max(smf4.dbo.feedlog.Todate), 112)+'000000'+'_'+'DIS_FDC_4' as Filenametext from smf4.dbo.feedlog  where smf4.dbo.feedlog.feedfreq = 'Bahar123'
--fileextension=.TXT
--suffix=
--fileheadersql=select 'H01'+convert(varchar(30),max(smf4.dbo.feedlog.fromdate), 112)+'000000'+ convert(varchar(30),max(smf4.dbo.feedlog.Todate), 112)+'000000DIS' as Fileheadertext from smf4.dbo.feedlog where smf4.dbo.feedlog.feedfreq = 'Bahar123'
--fileheaderdate=
--datadateformat=yyyy-mm-dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\DIS\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefootertext=T01
--reccount=nnnnnnnnnn


--# 1
Use smf4
Select
EDICADetail.Actflag,
case when(len(convert(varchar(30),EDICADetail.Actdate, 126))=19) then (convert(varchar(30),EDICADetail.Actdate, 126)+'.000Z') 
     when(len(convert(varchar(30),EDICADetail.Actdate, 126))=23) then (convert(varchar(30),EDICADetail.Actdate, 126)+'Z')  
     else '' end as Actdate,
EDICADetail.CADID,
EDICADetail.SecurityID,
EDICADetail.Acceptdate,
EDICADetail.Closedate,
EDICADetail.Exdate,
EDICADetail.Paydate,
EDICADetail.Snpdate,
EDICADetail.Sfpdate,
EDICADetail.ResultPubDate,
EDICADetail.Redemdate,
EDICADetail.CalculDate,
EDICADetail.PeriodStartDate,
EDICADetail.PeriodEndDate,
EDICADetail.EffecDate,
EDICADetail.CondSaleDate,
EDICADetail.WhollyUncondDate,
EDICADetail.AnnounceDate,
EDICADetail.ConversionDate,
EDICADetail.ConversionStatus,
EDICADetail.Ssn,
EDICADetail.Ssnyear,
EDICADetail.BusYrFrm,
EDICADetail.BusYrTo,
EDICADetail.MaxPrice,
EDICADetail.MinPrice,
EDICADetail.Quantity,
EDICADetail.MeetingDate,
EDICADetail.Confirmation,
EDICADetail.CAType
FROM EDICAFlat
INNER JOIN Security ON Security.SecurityID = EDICAFlat.SecurityID
INNER JOIN EDICADetail ON (EDICAFlat.Securityid = EDICADetail.SecurityId) AND (EDICAFlat.CadID = EDICADetail.CadID)
INNER JOIN EDICAOption ON (EDICAFlat.Securityid = EDICAOption.SecurityId) AND (EDICAFlat.CadID = EDICAOption.CadID) AND (EDICAFlat.CadoID = EDICAOption.CadoID)
where
EDICADetail.Confirmation = 'C'
and 
(  EDICADetail.CAType = 'DD'
or EDICADetail.CAType = 'IP'
or EDICADetail.CAType = 'D2'
or EDICADetail.CAType = 'DV'
or EDICADetail.CAType = 'DO'
or EDICADetail.CAType = 'SV'
or EDICADetail.CAType = 'CP')
