--filepath=o:\dayedit\wol\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.w05
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\wol\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--removestrike=y
--startcomment=//
--endcomment=
--removehtml=y
--hardlinebreaks=n
--enableordinal=y
--enablepunctuation=y
--allowhighcodepage=
--replacechar=(39,96;34,96;59,44;95,45)


--# 1
use wol
select 
cab.displayid,
cab.cabedition,
cab.issuername,
cab.displaydate 
from cab
left outer join blocked on cab.sitcd = blocked.sitcd
where
blocked.SitCD is null
and cab.cabstatus='SENT'
and cab.displayid is not null
and cab.displaydate is not null
and cab.displaydate>=(select max(feedlog.FromDate) from FEEDLOG where feedlog.FeedFreq = 'AFTERNOON')
and cab.displaydate<(select max(feedlog.ToDate) from FEEDLOG where feedlog.FeedFreq = 'AFTERNOON')
order by cab.displayid, cab.cabedition
