--filepath=o:\Datafeed\EDIUK\201\
--filenameprefix= 
--filename=YYYYMMDD
--filenamealt=
--fileextension=.201
--suffix=
--fileheadertext=EDI_201_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\201\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4
select 
smf4.dbo.security.isin as ISIN,
smf4.dbo.EDIcadetail.cadid as EventID,
smf4.dbo.issuer.issuername as CompanyName,
smf4.dbo.security.longdesc as SecurityTitle,
smf4.dbo.security.parvalue as NominalValue,
smf4.dbo.security.pvcurrencd as NominalValueCurrency,
smf4.dbo.EDIcadetail.exdate as ExDivDate,
smf4.dbo.EDIcadetail.paydate as PaymentDate,
smf4.dbo.EDIcaoption.cashdistcallrate as DividendAmount,
smf4.dbo.EDIcaoption.optioncurr as DivCurrency,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  smf4.dbo.EDIcaoption.optioncurr <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then smf4.dbo.EDIfxexcept.xrate
     when EDIfxexcept2.[id] is not null
	   and  smf4.dbo.EDIcaoption.optioncurr <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then EDIfxexcept2.xrate  
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  smf4.dbo.EDIcaoption.optioncurr <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then smf4.dbo.EDIfxconv.xrate
     else '' end as ExchangeRate,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  smf4.dbo.EDIcaoption.optioncurr <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxexcept.xrate as decimal(18,8)) * cast(smf4.dbo.EDIcaoption.cashdistcallrate as decimal(18,8)))
     when EDIfxexcept2.[id] is not null
	   and  smf4.dbo.EDIcaoption.optioncurr <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then (cast(EDIfxexcept2.xrate as decimal(18,8)) * cast(smf4.dbo.EDIcaoption.cashdistcallrate as decimal(18,8)))
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  smf4.dbo.EDIcaoption.optioncurr <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxconv.xrate as decimal(18,8)) * cast(smf4.dbo.EDIcaoption.cashdistcallrate as decimal(18,8)))
     else null end as UKEquivDiv,
case when  (smf4.dbo.EDIcaoption.denomin is not null and smf4.dbo.EDIcadetail.catype='DD') then 'SCRIP'
     when  (smf4.dbo.EDIcaoption.denomin is not null and smf4.dbo.EDIcadetail.catype='D2') then 'SCRIP'
     when  (smf4.dbo.EDIcaoption.denomin is not null and smf4.dbo.EDIcadetail.catype='DO') then 'SCRIP'
     when  (smf4.dbo.EDIcaoption.denomin is not null and smf4.dbo.EDIcadetail.catype='SV') then 'SCRIP'
     else '' end as SCRIPMarker,
case when smf4.dbo.EDIcadetail.catype='DO' then smf4.dbo.EDIcaoption.denomin
     else '' end as SCRIPOld,
case when smf4.dbo.EDIcadetail.catype='DO' then smf4.dbo.EDIcaoption.numerat
     else '' end as SCRIPNew,
case when smf4.dbo.EDIcadetail.catype='DV' then smf4.dbo.EDIcadetail.catype
     else '' end as DRIPMarker,
case when smf4.dbo.EDIcadetail.catype='DV' then smf4.dbo.EDIcaoption.payrate
     else '' end as DRIPrice,
case when smf4.dbo.EDIcadetail.catype='DV' then smf4.dbo.EDIcaoption.optioncurr
     else '' end as DRIPriceCurr


from smf4.dbo.EDIcadetail
left outer join smf4.dbo.EDIcaoption on smf4.dbo.EDIcadetail.CADID = smf4.dbo.EDIcaoption.CADID
                        and smf4.dbo.EDIcadetail.securityid = smf4.dbo.EDIcaoption.securityid
left outer join smf4.dbo.security on smf4.dbo.EDIcadetail.securityid = smf4.dbo.security.securityid
left outer join smf4.dbo.issuer on smf4.dbo.security.issuerid = smf4.dbo.issuer.issuerid
left outer join smf4.dbo.EDIfxexcept on smf4.dbo.security.isin = smf4.dbo.EDIfxexcept.link1
			and smf4.dbo.EDIcadetail.paydate = smf4.dbo.EDIfxexcept.paydate
			and smf4.dbo.EDIcaoption.optioncurr = smf4.dbo.EDIfxexcept.distcurr
left outer join smf4.dbo.EDIfxexcept as EDIfxexcept2 on smf4.dbo.security.isin = EDIfxexcept2.link2
			and smf4.dbo.EDIcadetail.paydate = EDIfxexcept2.paydate
			and smf4.dbo.EDIcaoption.optioncurr = EDIfxexcept2.distcurr
left outer join smf4.dbo.EDIfxconv on smf4.dbo.EDIcadetail.paydate = smf4.dbo.EDIfxconv.paydate
			and smf4.dbo.EDIcaoption.optioncurr = smf4.dbo.EDIfxconv.distcurr

where smf4.dbo.EDIcadetail.exdate > '2006-09-30'
and (smf4.dbo.EDIcadetail.catype='DV'
     or smf4.dbo.EDIcadetail.catype='DD'
     or smf4.dbo.EDIcadetail.catype='D2'
     or smf4.dbo.EDIcadetail.catype='DO'
     or smf4.dbo.EDIcadetail.catype='SV')





