--filepath=o:\Datafeed\EDIUK\126\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(HeaderDate),112) from smf4.dbo.EDICAFlat
--fileextension=.126
--suffix=
--fileheadersql=select 'EDI_SMF_126_'+convert(varchar(30), max(HeaderDate), 112) as Fileheadertext from smf4.dbo.EDICAFlat
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\126\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefootertext=EDI_ENDOFFILE

--# 1
use smf4
SELECT 
Issuer.Issuername as ISSUERNAME,
Security.Longdesc AS SECURITYDESCRIPTION,
EDICAOption.OptionCurr AS OPTIONCURR,
Case when EDICADetail.Taxrateind = 'N' then rtrim(cast(EDICAOption.CashDistCallRate as varchar(16))) else '' end as NETRATE,
Case when EDICADetail.Taxrateind <> 'N' OR EDICADetail.Taxrateind is null then rtrim(cast(EDICAOption.CashDistCallRate as varchar(16))) else '' end as GROSSRATE,
EDICADetail.Closedate as RECDATE,
EDICADetail.Exdate as EXDATE,
EDICADetail.Paydate as PAYDATE,
Security.Sedol as SEDOL,
Security.Isin as ISIN,
EDICAFlat.TempActflag as ACTFLAG
FROM EDICAFlat
INNER JOIN Security ON Security.SecurityID = EDICAFlat.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID
INNER JOIN EDICADetail ON (EDICAFlat.Securityid = EDICADetail.SecurityId)
      AND (EDICAFlat.CadID = EDICADetail.CadID)
left outer JOIN EDICAOption ON (EDICAFlat.Securityid = EDICAOption.SecurityId) 
      AND (EDICAFlat.CadID = EDICAOption.CadID) AND (EDICAFlat.CadoID = EDICAOption.CadoID)
LEFT OUTER JOIN SECTYPE ON Security.Sectype = SECTYPE.Code
Where
upper(sectype.Typegroup) = 'EQUITY'
and EDICADetail.Confirmation = 'C'
and EDICADetail.CaType = 'DD'
ORDER BY issuername