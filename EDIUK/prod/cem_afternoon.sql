select
cab.PSecID,
cab.CabEpoch,
cab.CabEdition,
cab.SitCD,
cab.SitText, 
cab.TerText,
cab.RDDText,
cab.OffText,
cab.FurText,
cab.CabStatus,
cab.ChangedBy, 
cab.Sentby,
GETDATE() as DisplayDate,
cab.PIDocLink,
cab.displayid,
cab.DisplaySeq, 
cab.Issuername,
cab.SecDesc,
cab.Acttime
FROM wol.dbo.Cab
left outer join blocked on cab.sitcd = blocked.sitcd
where
blocked.SitCD is null
and cabstatus = 'sent'
and cab.displayid is not null
and cab.displaydate is not null
and displaydate>=(select max(FromDate) from FEEDLOG where FeedFreq = 'AFTERNOON')
and displaydate<(select max(ToDate) from FEEDLOG where FeedFreq = 'AFTERNOON')
