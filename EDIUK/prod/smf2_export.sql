--filepath=o:\Datafeed\Bahar\
--filenameprefix=smf2_export_ 
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=smf2_historical_export
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\bahar\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use smf2
select
Sedol,
Isin,
lookup,
Issuername,
Longdesc,
MIC,
Infosource,
redemdate,
security.actdate,
security.actflag
from security
left outer join issuer on security.securityid = issuer.issuerid
left outer join sectype on typeflag = code