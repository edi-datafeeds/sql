--filepath=o:\Datafeed\EDIUK\202\
--filenameprefix= 
--filename=YYYYMMDD
--filenamealt=
--fileextension=.202
--suffix=
--fileheadertext=EDI_202_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\202\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select
wca.dbo.v54f_701_Dividend.isin as ISIN,
wca.dbo.v54f_701_Dividend.issuername as FundName,
wca.dbo.v54f_701_Dividend.securitydesc as SecurityTitle,
wca.dbo.MF.distributiontype as SecSubType,
wca.dbo.v54f_701_Dividend.exdate as ExDivDate,
wca.dbo.v54f_701_Dividend.paydate as PayDate,
wca.dbo.v54f_701_Dividend.grossdividend as Group1Amt,
case when (wca.dbo.v54f_701_Dividend.grossdividend is not null and wca.dbo.v54f_701_Dividend.grossdividend <> '') 
     then wca.dbo.v54f_701_Dividend.curencd
     else '' end as Group1Curr,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then smf4.dbo.EDIfxexcept.xrate
     when EDIfxexcept2.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then EDIfxexcept2.xrate  
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then smf4.dbo.EDIfxconv.xrate
     else null end as Group1XRate,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxexcept.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.grossdividend as decimal(18,8)))
     when EDIfxexcept2.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then (cast(EDIfxexcept2.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.grossdividend as decimal(18,8)))
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxconv.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.grossdividend as decimal(18,8)))
     else null end as Group1UKEqui,
wca.dbo.v54f_701_Dividend.group2grossdiv as Group2Amt,
case when (wca.dbo.v54f_701_Dividend.group2grossdiv is not null and wca.dbo.v54f_701_Dividend.group2grossdiv <> '') 
     then wca.dbo.v54f_701_Dividend.curencd
     else '' end as Group2Curr,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then smf4.dbo.EDIfxexcept.xrate
     when EDIfxexcept2.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then EDIfxexcept2.xrate  
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then smf4.dbo.EDIfxconv.xrate
     else null end as Group2XRate,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxexcept.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.group2grossdiv as decimal(18,8)))
     when EDIfxexcept2.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then (cast(EDIfxexcept2.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.group2grossdiv as decimal(18,8)))
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxconv.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.group2grossdiv as decimal(18,8)))
     else null end as Group2UKEqui,
wca.dbo.v54f_701_Dividend.equalisation as EqualisAmt,
case when (wca.dbo.v54f_701_Dividend.equalisation is not null and wca.dbo.v54f_701_Dividend.equalisation <> '') 
     then wca.dbo.v54f_701_Dividend.curencd
     else '' end as EqualisCurr,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then smf4.dbo.EDIfxexcept.xrate
     when EDIfxexcept2.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then EDIfxexcept2.xrate  
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then smf4.dbo.EDIfxconv.xrate
     else null end as EqualisXRate,
case when smf4.dbo.EDIfxexcept.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxexcept.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxexcept.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.equalisation as decimal(18,8)))
     when EDIfxexcept2.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  EDIfxexcept2.convcurr = 'GBP'  
     then (cast(EDIfxexcept2.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.equalisation as decimal(18,8)))
     when smf4.dbo.EDIfxconv.[id] is not null
	   and  wca.dbo.v54f_701_Dividend.curencd <> 'GBP'
	   and  smf4.dbo.EDIfxconv.convcurr = 'GBP'  
     then (cast(smf4.dbo.EDIfxconv.xrate as decimal(18,8)) * cast(wca.dbo.v54f_701_Dividend.equalisation as decimal(18,8)))
     else null end as EqualisUKEqui,
case when wca.dbo.v54f_701_Dividend.equalisation is null then 'YES'
     else null end as EqualisUnavailable,
case when wca.dbo.v54f_701_Dividend.taxrate = '20.00000' then 'INTEREST'
     when wca.dbo.v54f_701_Dividend.taxrate = '10.00000' then 'DIVIDEND'
     else null end as INTERESTMark
from wca.dbo.v54f_701_Dividend
left outer join smf4.dbo.EDIfxexcept on wca.dbo.v54f_701_Dividend.isin = smf4.dbo.EDIfxexcept.link1
			and wca.dbo.v54f_701_Dividend.paydate = smf4.dbo.EDIfxexcept.paydate
			and wca.dbo.v54f_701_Dividend.curencd = smf4.dbo.EDIfxexcept.distcurr
left outer join smf4.dbo.EDIfxexcept as EDIfxexcept2 on wca.dbo.v54f_701_Dividend.isin = EDIfxexcept2.link2
			and wca.dbo.v54f_701_Dividend.paydate = EDIfxexcept2.paydate
			and wca.dbo.v54f_701_Dividend.curencd = EDIfxexcept2.distcurr
left outer join smf4.dbo.EDIfxconv on wca.dbo.v54f_701_Dividend.paydate = smf4.dbo.EDIfxconv.paydate
			and wca.dbo.v54f_701_Dividend.curencd = smf4.dbo.EDIfxconv.distcurr
left outer join wca.dbo.MF on wca.dbo.v54f_701_Dividend.secid = wca.dbo.MF.secid
where wca.dbo.v54f_701_Dividend.created > '2006-09-30'
