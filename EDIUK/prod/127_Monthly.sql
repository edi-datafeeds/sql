--filepath=o:\Datafeed\EDIUK\126\
--filenameprefix=
--filenamesql=select convert(varchar(30),max(smf4.dbo.feedlog.todate), 112) as Filenametext from smf4.dbo.feedlog  where smf4.dbo.feedlog.feedfreq = 'AFTERNOON'
--fileextension=.126
--suffix=
--fileheadersql=select 'EDI_SMF_126_'+convert(varchar(30),max(smf4.dbo.feedlog.todate), 112) as Fileheadertext from smf4.dbo.feedlog where smf4.dbo.feedlog.feedfreq = 'AFTERNOON'
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\126\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefootertext=EDI_ENDOFFILE

--# 1
use wca
select
isin,
issuername,
securitydesc,
sedol,
sectycd,
divtype,
exdate,
recdate,
paydate,
grossdividend,
netdividend,
rationew,
ratioold,
created,
changed
from v54f_620_dividend
where
paydate>getdate()-28 and paydate<getdate()
and exchgcd = 'GBLSE'
and actflag<>'D'
and actflag<>'C'
order by isin

