--filepath=o:\datafeed\EDIUK\050\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.050
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyymmdd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\050\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4

SELECT
Case when EDICAOption.OptionType is not null then EDICAOption.OptionType
else EDICADetail.CaType End AS CaType,
EDICAFlat.tempActflag AS Actflag, 
Security.Sedol,
EDICADetail.CADID AS DistribId,
EDICAOption.CADOID * 10 AS ElementID,
EDICAFlat.HeaderDate As FeedDate,
Security.Isin,
Issuer.Issuername,
Security.Longdesc,
EDICAOption.OptionCurr as Currcode,
Case when EDICADetail.TaxRateInd = 'N' then 'Y'
else 'N' End AS Taxcode,
EDICADetail.OsTaxCurr as OverseasTaxCurr,
EDICADetail.uktaxrate,
EDICADetail.OsTaxRate as OverseasTaxRate,
EDICAOption.CashdistCallrate AS Payrate,
rtrim(EDICADetail.divtype)+' '+rtrim(EDICAOption.comment) + ' ' + rtrim(EDICADetail.divfreq) as Comment,
EDICADetail.closedate AS Recdate,
EDICADetail.Exdate,
EDICADetail.Paydate,
'' as dummy1,
'' as dummy2

FROM EDICAFlat
INNER JOIN Security ON Security.SecurityID = EDICAFlat.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
INNER JOIN EDICADetail ON (EDICAFlat.Securityid = EDICADetail.SecurityId) AND (EDICAFlat.CADID = EDICADetail.CADID)
INNER JOIN EDICAOption ON (EDICAFlat.Securityid = EDICAOption.SecurityId) AND (EDICAFlat.CADID = EDICAOption.CADID) AND (EDICAFlat.CADoID = EDICAOption.CADoID)
Where  
EDICADetail.Confirmation = 'C'
and (EDICADetail.CAType ='DD'
Or EDICADetail.CAType = 'D2')

ORDER BY Security.Sedol

