--filepath=o:\datafeed\EDIUK\030\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(HeaderDate),112) from smf4.dbo.EDICAFlat
--fileextension=.030
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\030\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use smf4

SELECT  
Security.Sedol,
EDICADetail.CADID AS DistribId,
EDICAOption.CADOID * 10 AS ElementID,
Issuer.Issuername+' '+Security.Longdesc AS SecurityDescription,
Security.Isin,
EDICAOption.OptionCurr as Currcode,
EDICAFlat.HeaderDate As FeedDate,
EDICADetail.Exdate,
EDICADetail.closedate AS Recdate,
EDICADetail.Paydate,
EDICAOption.CashdistCallrate AS Payrate,
Case when EDICADetail.TaxRateInd = 'N' then 'Net'
else 'Gross' End AS Taxcode,
EDICADetail.uktaxrate,
rtrim(EDICADetail.divtype)+' '+rtrim(EDICAOption.comment) + ' ' + rtrim(EDICADetail.divfreq) as Comment,
Case when EDICAOption.OptionType = 'DD' then 'CASH'
     when EDICAOption.OptionType = 'DO' then 'DIVIDEND OPTION'
     when EDICAOption.OptionType = 'SV' then 'SCRIP DIVIDEND'
     when EDICAOption.OptionType = 'CP' then 'STOCK DIVIDEND'
     when EDICAOption.OptionType = 'D2' then 'CURRENCY ELECTION'
     when EDICAOption.OptionType = 'IP' then 'INTEREST PAYMENT'
Else '' End As Divtype,
substring(security.sectype,1,1) As Stocktype,
EDICAOption.Numerat,
EDICAOption.Denomin,
Case when EDICAFlat.tempActflag = 'I' then EDICAFlat.HeaderDate
else EDICADetail.credate End AS CreationDate,
EDICAFlat.tempActflag AS Actflag
FROM EDICAFlat
INNER JOIN Security ON Security.SecurityID = EDICAFlat.SecurityID
LEFT OUTER JOIN Issuer ON Security.IssuerID = Issuer.IssuerID 
INNER JOIN EDICADetail ON (EDICAFlat.Securityid = EDICADetail.SecurityId) AND (EDICAFlat.CADID = EDICADetail.CADID)
INNER JOIN EDICAOption ON (EDICAFlat.Securityid = EDICAOption.SecurityId) AND (EDICAFlat.CADID = EDICAOption.CADID) AND (EDICAFlat.CADoID = EDICAOption.CADoID)
Where  
EDICADetail.status = 'S'
and (EDICAOption.OptionType ='DD'
Or EDICAOption.OptionType = 'DO'
Or EDICAOption.OptionType = 'SV'
Or EDICAOption.OptionType = 'CP'
Or EDICAOption.OptionType = 'D2'
Or EDICAOption.OptionType = 'IP')
ORDER BY Security.Sedol
