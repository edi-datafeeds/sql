--filepath=o:\Datafeed\Bahar\123\
--filenameprefix= 
--filename=yyyymmdd
--filenamealt=
--fileextension=.123
--suffix=
--fileheadertext=EDI_123_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\bahar\123\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select cast(count(smf4.dbo.feedlog.feedfreq) as char(1)) as seq FROM smf4.dbo.feedlog where todate>getdate()-0.4 and feedfreq = '123incr'       
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use smf4
SELECT 
security.sedol,
EDICABaharFlat.SecurityId,
EDICABaharFlat.CadID,
EDICABaharFlat.CadoID,
EDICABaharFlat.Headerdate,
EDICABaharFlat.tempActflag,
EDICADetail.Actflag,
EDICADetail.Actdate,
EDICADetail.CADID,
EDICADetail.SecurityID,
case when EDICAOption.Firstdate is not null then EDICAOption.Firstdate else EDICADetail.Acceptdate end as Acceptdate,
EDICADetail.Closedate,
EDICADetail.Exdate,
EDICADetail.Paydate,
EDICADetail.Snpdate as CrestCreditDate,
EDICADetail.Sfpdate as CetDispatchDate,
EDICADetail.ResultPubDate,
EDICADetail.Redemdate,
EDICADetail.CalculDate,
EDICADetail.PeriodStartDate,
EDICADetail.PeriodEndDate,
EDICADetail.EffecDate,
EDICADetail.CondSaleDate,
EDICADetail.WhollyUncondDate,
EDICADetail.AnnounceDate,
EDICADetail.ConversionDate,
EDICADetail.ConversionStatus,
EDICADetail.Ssn,
EDICADetail.Ssnyear,
EDICADetail.BusYrFrm,
EDICADetail.BusYrTo,
EDICADetail.MaxPrice as StampDuty,
EDICADetail.MinPrice,
EDICADetail.Quantity as CommissionCost,
EDICADetail.MeetingDate,
EDICADetail.Confirmation,
EDICAOption.OptionType,
EDICADetail.CreDate,
EDICADetail.HeaderDate,
'' as Currcode,
'' as Typecode,
'' as Taxflag,
'' as Taxcurrcode,
'' as Ostaxrate,
EDICADetail.Uktaxrate,
EDICAOption.Actflag,
EDICAOption.Actdate,
EDICAOption.CADOID,
EDICAOption.CADID,
EDICAOption.SecurityID,
EDICAOption.CashDistCallRate,
EDICAOption.Denomin,
EDICAOption.Minfract,
EDICAOption.Numerat,
EDICAOption.Firstdate,
EDICAOption.Resflag,
EDICAOption.SecCodeOfDist,
EDICADetail.DivType,
EDICAOption.FracEnt,
EDICAOption.OptionCurr,
EDICAOption.OverSubscrflag,
EDICAOption.Comment,
'' as netOfTaxInd,
EDICADetail.OsTaxCurr as OverseasTaxCurr,
EDICADetail.OsTaxRate,
EDICADetail.UkTaxCurr,
EDICADetail.UktaxRate,
EDICAOption.RateType,
EDICADetail.TaxRateInd,
EDICAOption.RateStatus,
EDICAOption.OvrSubscipRate,
EDICAOption.CreDate,
EDICAOption.HeaderDate,
EDICAOption.Payrate as DripRate,
EDICAOption.Distsedol,
issuer.cinccode
FROM EDICABaharFlat
INNER JOIN Security ON Security.SecurityID = EDICABaharFlat.SecurityID
left outer JOIN issuer ON Security.issuerID = issuer.issuerID
INNER JOIN EDICADetail ON (EDICABaharFlat.Securityid = EDICADetail.SecurityId) AND (EDICABaharFlat.CadID = EDICADetail.CadID)
INNER JOIN EDICAOption ON (EDICABaharFlat.Securityid = EDICAOption.SecurityId) AND (EDICABaharFlat.CadID = EDICAOption.CadID) AND (EDICABaharFlat.CadoID = EDICAOption.CadoID)
where
(
EDICADetail.CAType='DD'
or EDICADetail.CAType = 'D2'
or EDICADetail.CAType = 'DV'
or EDICADetail.CAType = 'DO'
or EDICADetail.CAType = 'SV'
or EDICADetail.CAType = 'CP'
)
and EDICADetail.status = 'S'

ORDER BY Security.Sedol
