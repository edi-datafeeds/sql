-- {summary query}
use wol
select
sitlist.sitname as Situation,
rtrim(cast(cab.displaydate as varchar(20))) as [Document Date],
cab.displayid+'/'+rtrim(cast(cab.cabedition as 
varchar(20))) as [Bulletin ID],
'http://www.exchange-data.net/php/cab/full_view.php?cab_id='+rtrim(cast(cab.cabepoch 
as varchar(20)))+'&cab_count='+rtrim(cast(cab.cabedition as  
varchar(20)))+'&PSecID='++rtrim(cast(cab.psecid as varchar(20)))+'&n=6011&acc=2' as URL,
convert(varchar(20),cab.displaydate,112) as DisplayDate
from cab
left outer join sitlist on cab.SitCD = sitlist.SitCD
where
cab.cabstatus='SENT'
and displaydate>=(select max(FromDate) from FEEDLOG where FeedFreq = 'ISA')
and displaydate<(select max(ToDate) from FEEDLOG where FeedFreq = 'ISA')
and cab.SitCD<>'CEL'
and cab.SitCD<>'DRP'
and cab.SitCD<>'SCD'
order by DisplayDate


-- {cab query}
use wol
select
convert(varchar(20),cab.displaydate,112) + '_' + cast(cab.cabepoch as varchar(12)) +'_'+ 
cast(cab.cabedition as varchar(2))+'_'+sitlist.sitCD+'.txt' as isafilename,
'http://www.exchange-data.net/php/cab/full_view.php?cab_id='+rtrim(cast(cab.cabepoch 
as varchar(20)))+'&cab_count='+rtrim(cast(cab.cabedition as 
varchar(20)))+'&PSecID='++rtrim(cast(cab.psecid as varchar(20)))+'&n=6011&acc=2' as URL,
'' as space1,
'Bulletin ID - '+cab.displayid+'/'+rtrim(cast(cab.cabedition as 
varchar(20))) as BulletinID,
'' as space2,
'Situation - '+sitlist.sitname as situation,
'' as space3,
'Document Date - '+rtrim(convert(varchar(20),cab.displaydate,107)) as DocumentDate,
'' as space4,
'Valid ISA Status names -' as ISAKey1,
'Q =Qualifying, N =Not qualifying, R =not Relevant,' as ISAKey2,
'L =No Longer qualifying, U =Unspecified' as ISAKey3,
'' as space5,
'' as RunSecQuery,
cab.cabepoch,
cab.cabedition,
cab.psecid,
'' as space6,
'TISA Notes below the line' as footer1,
'' as space7,
'____________________________________________________________________' as footer2
from cab
left outer join sitlist on cab.SitCD = sitlist.SitCD
left outer join isa on cab.cabepoch = isa.cabepoch
                   and cab.cabedition = isa.cabedition
                   and cab.psecid = isa.psecid

where
cab.cabstatus='SENT'
and displaydate>=(select max(FromDate) from FEEDLOG where FeedFreq = 'ISA')
and displaydate<(select max(ToDate) from FEEDLOG where FeedFreq = 'ISA')
and cab.SitCD<>'CEL'
and cab.SitCD<>'DRP'
and cab.SitCD<>'SCD'
order by cab.cabepoch desc


-- {sec query}
use wol
select
case when ISA.secid is null 
     then '(U) '+sec.isin+' - '+sec.title 
     else '('+ISA.isaflag+') ' +sec.isin+' - '+sec.title end as writeline,
'' as RunMarketQuery,
sec.secid
from cab
left outer join sec on cab.cabepoch = sec.cabepoch
                   and cab.cabedition = sec.cabedition
                   and cab.psecid = sec.psecid
left outer join isa on sec.cabepoch = isa.cabepoch
                   and sec.cabedition = isa.cabedition
                   and sec.psecid = isa.psecid
                   and sec.secid = isa.secid


-- {market query}
use wol
select distinct
smf4.dbo.market.mic
from sec
inner join smf4.dbo.market on sec.secid = smf4.dbo.market.securityid
where
smf4.dbo.market.actflag<>'D'
and smf4.dbo.market.confirmation='C'
and smf4.dbo.market.Statusflag<>'D'

