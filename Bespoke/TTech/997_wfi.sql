--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ttech
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#1
select distinct
client.dbo.pfisin.code as Isin,
wca.dbo.issur.Issuername,
wca.dbo.issur.IssID,
wca.dbo.scmst.SecID,
wca.dbo.issur.CntryofIncorp,
wca.dbo.scmst.Uscode,
wca.dbo.scexh.ExchgCD,
wca.dbo.scexh.LocalCode,
wca.dbo.scmst.SecurityDesc,
wca.dbo.scmst.SectyCD,
wca.dbo.bond.InterestRate,
wca.dbo.bond.InterestPaymentFrequency,
wca.dbo.bond.InterestAccrualConvention,
wca.dbo.bond.IntCommencementDate,
wca.dbo.bond.MaturityDate,
wca.dbo.bond.Perpetual,
wca.dbo.bond.IssueAmount,
wca.dbo.bond.OutstandingAmount,
wca.dbo.bond.Callable,
wca.dbo.bond.Puttable,
wca.dbo.cpopt.CallPut,
wca.dbo.cpopt.CPType,
wca.dbo.cpopt.PriceAsPercent,
wca.dbo.cpopt.FromDate,
wca.dbo.cpopt.ToDate,
wca.dbo.bond.CurenCD as DebtCurrency,
case when wca.dbo.bond.largeparvalue<>'' then wca.dbo.bond.largeparvalue when wca.dbo.bond.parvalue<>'' then wca.dbo.bond.parvalue else wca.dbo.scmst.parvalue end as NominalValue,
wca.dbo.bond.Series
from client.dbo.pfisin
left outer join wca.dbo.scmst on client.dbo.pfisin.code=wca.dbo.scmst.isin
left outer join wca.dbo.bond on wca.dbo.scmst.secid=wca.dbo.bond.secid and wca.dbo.bond.actflag<>'D'
left outer join wca.dbo.issur on wca.dbo.scmst.issid=wca.dbo.issur.issid and wca.dbo.issur.actflag<>'D'
left outer join wca.dbo.cpopt on wca.dbo.scmst.secid=wca.dbo.cpopt.secid and wca.dbo.cpopt.actflag<>'D'
left outer join wca.dbo.scexh on wca.dbo.scmst.secid=wca.dbo.scexh.secid and wca.dbo.scexh.actflag<>'D'
where
client.dbo.pfisin.accid = 997
and client.dbo.pfisin.actflag<>'D'
order by wca.dbo.scmst.secid
