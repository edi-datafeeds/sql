--filepath=
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.645
--suffix=
--fileheadertext=EDI_SRF_645_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
use WCA
SELECT
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
left outer join scmst on issur.issid = scmst.issid
left outer join scexh on scmst.secid = scexh.secid
where issur.actflag<>'D'
and (exchgcd = 'EENB'
or exchgcd = 'NLENA'
or exchgcd = 'PTBVL'
or exchgcd = 'FRPEN'
or exchgcd = 'DEFSX'
or exchgcd = 'CHSSX'
or exchgcd = 'ITMSE'
or exchgcd = 'DKCSE'
or exchgcd = 'EETSE'
or exchgcd = 'FIHSE'
or exchgcd = 'ISISE'
or exchgcd = 'LTNSE'
or exchgcd = 'SESSE'
or exchgcd = 'PLOTC'
or exchgcd = 'TWOTC'
or exchgcd = 'USOTC')
AND issur.acttime > (select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 3)

--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM scmst
left outer join scexh on scmst.secid = scexh.secid
WHERE 
scmst.actflag<>'D'
and scexh.actflag<>'D'
and (exchgcd = 'EENB'
or exchgcd = 'NLENA'
or exchgcd = 'PTBVL'
or exchgcd = 'FRPEN'
or exchgcd = 'DEFSX'
or exchgcd = 'CHSSX'
or exchgcd = 'ITMSE'
or exchgcd = 'DKCSE'
or exchgcd = 'EETSE'
or exchgcd = 'FIHSE'
or exchgcd = 'ISISE'
or exchgcd = 'LTNSE'
or exchgcd = 'SESSE'
or exchgcd = 'PLOTC'
or exchgcd = 'TWOTC'
or exchgcd = 'USOTC')
AND scmst.acttime > (select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 3)

--# 3
use WCA
SELECT
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
left outer join scexh on sedol.secid = scexh.secid
WHERE 
sedol.actflag<>'D'
and (exchgcd = 'EENB'
or exchgcd = 'NLENA'
or exchgcd = 'PTBVL'
or exchgcd = 'FRPEN'
or exchgcd = 'DEFSX'
or exchgcd = 'CHSSX'
or exchgcd = 'ITMSE'
or exchgcd = 'DKCSE'
or exchgcd = 'EETSE'
or exchgcd = 'FIHSE'
or exchgcd = 'ISISE'
or exchgcd = 'LTNSE'
or exchgcd = 'SESSE'
or exchgcd = 'PLOTC'
or exchgcd = 'TWOTC'
or exchgcd = 'USOTC')
AND sedol.acttime > (select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 3)


--# 4
use WCA
SELECT
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
WHERE 
(exchgcd = 'EENB'
or exchgcd = 'NLENA'
or exchgcd = 'PTBVL'
or exchgcd = 'FRPEN'
or exchgcd = 'DEFSX'
or exchgcd = 'CHSSX'
or exchgcd = 'ITMSE'
or exchgcd = 'DKCSE'
or exchgcd = 'EETSE'
or exchgcd = 'FIHSE'
or exchgcd = 'ISISE'
or exchgcd = 'LTNSE'
or exchgcd = 'SESSE'
or exchgcd = 'PLOTC'
or exchgcd = 'TWOTC'
or exchgcd = 'USOTC')
AND scexh.acttime > (select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 3)


--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
left outer join scexh on exchg.exchgcd = scexh.exchgcd
where exchg.actflag<>'D'
and (scexh.exchgcd = 'EENB'
or scexh.exchgcd = 'NLENA'
or scexh.exchgcd = 'PTBVL'
or scexh.exchgcd = 'FRPEN'
or scexh.exchgcd = 'DEFSX'
or scexh.exchgcd = 'CHSSX'
or scexh.exchgcd = 'ITMSE'
or scexh.exchgcd = 'DKCSE'
or scexh.exchgcd = 'EETSE'
or scexh.exchgcd = 'FIHSE'
or scexh.exchgcd = 'ISISE'
or scexh.exchgcd = 'LTNSE'
or scexh.exchgcd = 'SESSE'
or scexh.exchgcd = 'PLOTC'
or scexh.exchgcd = 'TWOTC'
or scexh.exchgcd = 'USOTC')
AND exchg.acttime > (select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 3)
