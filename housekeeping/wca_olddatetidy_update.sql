update  wca.dbo.bond set IssueDate = null
 where IssueDate < '1901-01-01'
go
update wca.dbo.bond set IssueAmountDate = null
where IssueAmountDate < '1901-01-01'
go
update wca.dbo.bond set OutstandingAmountDate = null
where OutstandingAmountDate < '1901-01-01'
go
update wca.dbo.bond set IntCommencementDate = null
where IntCommencementDate < '1901-01-01'
go
update wca.dbo.bond set FirstCouponDate = null
where FirstCouponDate < '1901-01-01'
go
update wca.dbo.bond set TapExpiryDate = null
 where TapExpiryDate < '1901-01-01'
go
update wca.dbo.bond set MaturityDate = null
where MaturityDate < '1901-01-01' 
go

update  wca.dbo.bochg set EffectiveDate = null
where EffectiveDate < '1901-01-01'
go
update  wca.dbo.bochg set OldOutDate = null
where OldOutDate < '1901-01-01'
go
update wca.dbo.bochg set NewOutDate = null
 where NewOutDate < '1901-01-01' 
go
update  wca.dbo.redem set RedemDate = null
 where RedemDate < '1901-01-01' 
go
