--filepath=c:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.641
--suffix=_portfolio
--fileheadertext=EDI_BBG_641_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
select distinct
upper('BBC') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbcid,
main.secid,
main.cntrycd,
main.curencd,
main.bbgcompid,
main.bbgcomptk
from bbc as main
inner join scmst on main.secid = scmst.secid
WHERE 
scmst.actflag<>'D'
and main.cntrycd<>''
and main.curencd<>''
and scmst.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')


--# 2
use WCA
SELECT distinct
upper('BBE') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbeid,
main.secid,
main.exchgcd,
main.curencd,
main.bbgexhid,
main.bbgexhtk
from bbe as main
inner join bbc on main.secid = bbc.secid and substring(main.exchgcd,1,2) = bbc.cntrycd and main.curencd=bbc.curencd
inner join scmst on main.secid = scmst.secid
WHERE 
scmst.actflag<>'D'
and main.exchgcd<>''
and main.curencd<>''
and scmst.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')



--# 3
use WCA
SELECT
upper('BBCC') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbccid,
main.secid,
'' as bbcid,
main.oldcntrycd,
main.oldcurencd,
main.effectivedate,
main.newcntrycd,
main.newcurencd,
main.oldbbgcompid,
main.newbbgcompid,
main.oldbbgcomptk,
main.newbbgcomptk,
main.releventid,
main.eventtype,
main.notes
FROM bbcc as main
inner join scmst on main.secid = scmst.secid
--  inner join bbc on main.bbecid = bbe.bbecid
WHERE
-- scmst.actflag<>'D'
-- and scmst.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
1=2

--# 4
use WCA
SELECT
upper('BBEC') as tablename,
main.actflag,
main.announcedate,
main.acttime,
main.bbecid,
main.secid,
'' as bbeid,
main.oldexchgcd,
main.oldcurencd,
main.effectivedate,
main.newexchgcd,
main.newcurencd,
main.oldbbgexhid,
main.newbbgexhid,
main.oldbbgexhtk,
main.newbbgexhtk,
main.releventid,
main.eventtype,
main.notes
FROM bbec as main
inner join scmst on main.secid = scmst.secid
-- inner join bbe on main.bbecd = bbe.bbeid
--  inner join bbc on bbe.secid = bbc.secid and substring(bbe.exchgcd,1,2) = bbc.cntrycd and bbe.curencd=bbc.curencd
WHERE
-- scmst.actflag<>'D'
-- and scmst.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
1=2
