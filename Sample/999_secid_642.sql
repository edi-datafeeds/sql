--filepath=c:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.642
--suffix=_portfolio
--fileheadertext=EDI_STATIC_CHANGE_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select top 50
SDCHG.SdchgID as EventID,
'Sedol Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
sdchg.Actflag,
sdchg.eventtype as Reason,
case when sdchg.effectivedate is null
     then sdchg.announcedate
     else sdchg.effectivedate
     end as EffectiveDate,
sdchg.OldSedol as OldStatic,
sdchg.NewSedol as NewStatic
FROM full_SDCHG as SDCHG
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.SecID = SDCHG.SecID
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.SecID = v20c_620_dSCEXH.SecID
WHERE SDCHG.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')


--# 3
use wca
select top 50
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic
FROM full_ICC as ICC
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.SecID = v20c_620_dSCEXH.SecID
WHERE ICC.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')

--# 4
use wca
select top 50
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
inchg.Actflag,
inchg.eventtype as Reason,
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic
FROM inchg
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.issid = inchg.issid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
WHERE INCHG.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')

--# 5
use wca
select top 50
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
ischg.Actflag,
ischg.eventtype as Reason,
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic
FROM full_ischg as ISCHG
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.issid = ischg.issid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
WHERE ISCHG.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')


--# 6
use wca
select top 50
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM full_lcc as LCC
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.secid = lcc.secid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
WHERE LCC.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')

--# 7
use wca
select top 50
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
scchg.Actflag,
scchg.eventtype as Reason,
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic
FROM full_scchg as SCCHG
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.secid = scchg.secid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
WHERE SCCHG.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')

--# 8
use wca
select top 50
LTCHG.LTCHGID as EventID,
'Lot Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic
FROM ltchg
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.secid = ltchg.secid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
WHERE LTCHG.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
and (ltchg.OldLot <> ''
or ltchg.NewLot <> '')

--# 9
use wca
select top 50
LTCHG.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldMinTrdQty as OldStatic,
ltchg.NewMinTrdgQty as NewStatic
FROM ltchg
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.secid = ltchg.secid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
WHERE LTCHG.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
and (ltchg.OldMinTrdQty <> ''
or ltchg.NewMinTrdgQty <> '')

--# 10
use wca
select top 50
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic
FROM full_lstat as LSTAT
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.secid = lstat.secid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
WHERE LSTAT.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')

--# 11
use wca
select top 50
nlist.scexhID as EventID,
'New Listing' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
nlist.Actflag,
case when v20c_620_dSCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when v20c_620_dSCEXH.Listdate is not null 
     then v20c_620_dSCEXH.Listdate 
     else nlist.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic
FROM full_nlist as NLIST
INNER JOIN v20c_620_dSCEXH ON nlist.scexhid = v20c_620_dSCEXH.scexhid
INNER JOIN v20c_620_SCMST ON v20c_620_dSCEXH.secid = v20c_620_SCMST.secid
WHERE NLIST.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')

--# 12
use wca
select top 50
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.EffectiveDate is null
     then icc.announcedate
     else icc.effectivedate
     end as EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic
FROM full_ICC as ICC
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.SecID = v20c_620_dSCEXH.SecID
WHERE ICC.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
and (icc.OldUSCode <> ''
or icc.NewUSCode <> '')

--# 13
use wca
select top 50
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic
FROM Currd
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.SecID = currd.SecID
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.SecID = v20c_620_dSCEXH.SecID
WHERE CURRD.actflag<>'D'
and v20c_620_SCMST.secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
