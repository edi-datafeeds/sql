--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.627
--suffix=_SampleData
--fileheadertext=EDI_DIVIDEND_627_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
select top 20 *  
FROM v54f_620_Dividend
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd = primaryexchgcd
ORDER BY EventID desc, ExchgCD, Sedol
