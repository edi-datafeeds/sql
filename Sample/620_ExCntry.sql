--filepath=o:\upload\acc\999\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\999\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA
select * 
FROM v50f_620_Company_Meeting
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
USE WCA
select *
FROM v53f_620_Call
inner join sectygrp on v53f_620_Call.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
USE WCA
select *
FROM v50f_620_Liquidation
inner join sectygrp on v50f_620_Liquidation.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 4
USE WCA
select *
FROM v51f_620_Certificate_Exchange
inner join sectygrp on v51f_620_Certificate_Exchange.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 5
USE WCA
select *  
FROM v51f_620_International_Code_Change
inner join sectygrp on v51f_620_International_Code_Change.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 6
USE WCA
select *  
FROM v51f_620_Conversion_Terms
inner join sectygrp on v51f_620_Conversion_Terms.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca
select *  
FROM v51f_620_Redemption_Terms
inner join sectygrp on v51f_620_Redemption_Terms.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 8
use wca
select *  
FROM v51f_620_Security_Reclassification
inner join sectygrp on v51f_620_Security_Reclassification.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 9
use wca
select *
FROM v52f_620_Lot_Change
inner join sectygrp on v52f_620_Lot_Change.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca
select *
FROM v52f_620_Sedol_Change
inner join sectygrp on v52f_620_Sedol_Change.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 11
use wca
select *  
FROM v53f_620_Buy_Back
inner join sectygrp on v53f_620_Buy_Back.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 12
use wca
select *  
FROM v53f_620_Capital_Reduction
inner join sectygrp on v53f_620_Capital_Reduction.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 13
use wca
select *  
FROM v53f_620_Takeover
inner join sectygrp on v53f_620_Takeover.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 14
use wca
select *  
FROM v54f_620_Arrangement
inner join sectygrp on v54f_620_Arrangement.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 15
use wca
select *  
FROM v54f_620_Bonus
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 16
use wca
select *  
FROM v54f_620_Bonus_Rights
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 17
use wca
select *  
FROM v54f_620_Consolidation
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 18
use wca
select *  
FROM v54f_620_Demerger
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 19
use wca
select *  
FROM v54f_620_Distribution
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 20
use wca
select *  
FROM v54f_620_Divestment
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 21
use wca
select *  
FROM v54f_620_Entitlement
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 22
use wca
select *  
FROM v54f_620_Merger
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 23
use wca
select *  
FROM v54f_620_Preferential_Offer
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 24
use wca
select *  
FROM v54f_620_Purchase_Offer
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 25
use wca
select *  
FROM v54f_620_Rights
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 26
use wca
select *  
FROM v54f_620_Security_Swap
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 27
use wca
select * 
FROM v54f_620_Subdivision
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 28
use wca
select * 
FROM v50f_620_Bankruptcy
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 29
use wca
select * 
FROM v50f_620_Financial_Year_Change
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca
select * 
FROM v50f_620_Incorporation_Change
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca
select * 
FROM v50f_620_Issuer_Name_change
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca
select * 
FROM v50f_620_Class_Action
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca
select * 
FROM v51f_620_Security_Description_Change
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 34
use wca
select * 
FROM v52f_620_Assimilation
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 35
use wca
select * 
FROM v52f_620_Listing_Status_Change
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 36
use wca
select * 
FROM v52f_620_Local_Code_Change
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 37
use wca
select *  
FROM v52f_620_New_Listing
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca
select *  
FROM v50f_620_Announcement
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca
select *  
FROM v51f_620_Parvalue_Redenomination
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 40
USE WCA
select *  
FROM v51f_620_Currency_Redenomination
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 41
USE WCA
select *  
FROM v53f_620_Return_of_Capital
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 42
USE WCA
select *  
FROM v54f_620_Dividend_Reinvestment_Plan
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 43
USE WCA
select *  
FROM v54f_620_Franking
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 44
use wca
select * 
FROM v51f_620_Conversion_Terms_Change
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 45
USE WCA
select *  
FROM v54f_620_Dividend
inner join sectygrp on v50f_620_Company_Meeting.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
WHERE 
(ExCountry = 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef




(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')
