--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=
--filename=20130812
--filenamealt=
--fileextension=.txt
--suffix=_Rates
--fileheadertext=EDI_Daily_Rates_20130812
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate = '2013-08-12'
