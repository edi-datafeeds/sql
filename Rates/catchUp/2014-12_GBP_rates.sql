--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(xrates.rates.Feeddate) from xrates.rates
--fileextension=.csv
--suffix=_GBP_1800
--fileheadertext=EDI_Daily_Rates_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select distinct Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate like '2014-11%'
AND Base = 'GBP';