--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=XDR_
--filename=yyyymmdd
--filenamealt=
--fileextension=.csv
--suffix=_Hist
--fileheadertext=EDI_XDR_Hist_Rates
--fileheaderdate="1999-2014"
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Acttime like '2014-11-30%' 
AND xrates.rates.Curr = 'XDR' 
order by xrates.rates.Feeddate ASC