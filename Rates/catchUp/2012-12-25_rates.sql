--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=
--filename=20121225
--filenamealt=
--fileextension=.txt
--suffix=_Rates
--fileheadertext=EDI_Daily_Rates_20121225
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate = '2012-12-25'
