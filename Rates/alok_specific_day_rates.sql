--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=
--filename=2015-01-10
--filenamealt=
--fileextension=.txt
--suffix=_Rates
--fileheadertext=EDI_Daily_Rates_
--fileheaderdate=2015-01-10
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate = '2015-01-10' 
and base = "USD";


