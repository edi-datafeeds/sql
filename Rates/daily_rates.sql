--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(xrates.rates.Feeddate) from xrates.rates
--fileextension=.txt
--suffix=_Rates
--fileheadertext=EDI_Daily_Rates_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select distinct Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate = (select max(xrates.rates.Feeddate) from xrates.rates) 
and xrates.rates.Base = 'USD';
