--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(xrates.rates.Feeddate) from xrates.rates
--fileextension=.txt
--suffix=_Rates
--fileheadertext=EDI_Daily_Rates_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
SELECT 
	Curr as ISO,
	base, 
	rate, feeddate as `Date`
FROM xrates.hourly_rates
	WHERE xrates.hourly_rates.feeddate = ( SELECT MAX(xrates.hourly_rates.feeddate) FROM xrates.hourly_rates ) 
	AND xrates.hourly_rates.base = 'USD';
