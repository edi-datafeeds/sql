--filepath=O:\Datafeed\Xrates\Feed\Daily\Dups\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(xrates.rates.Feeddate) from xrates.rates
--fileextension=.txt
--suffix=_Dups
--fileheadertext=EDI_Weekly_Dups_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\Dups\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
SELECT * 
FROM xrates.rates, xrates.rates2 
WHERE ((xrates.rates.feeddate = date_sub(xrates.rates2.feeddate, INTERVAL 1 day) AND rates.rate = rates2.rate)
AND rates.Curr = rates2.Curr)
