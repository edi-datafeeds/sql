--filepath=O:\Datafeed\Xrates\Feed\Daily\GBP\Hist\
--filenameprefix=ISK_
--filename=yyyy-mm
--filenamealt=
--fileextension=.csv
--suffix=_GBP_Hist
--fileheadertext=EDI_ISK_Nov_2014
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select DISTINCT Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate LIKE '2014-11%'
AND Base = 'GBP'
AND Curr = 'ISK'
ORDER BY Feeddate ASC;
