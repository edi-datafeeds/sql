Delete from exchgrates.liverates
where exchgrates.liverates.Feeddate <> (select max(xrates.rates.Feeddate) from xrates.rates);

insert into exchgrates.liverates
select * from xrates.rates
where xrates.rates.Base = 'USD' and xrates.rates.Feeddate = (select max(xrates.rates.Feeddate) from xrates.rates);

insert into exchgrates.histrate
select * from xrates.rates
where xrates.rates.Feeddate = (select max(xrates.rates.Feeddate) from xrates.rates) and xrates.rates.Base = 'USD';
