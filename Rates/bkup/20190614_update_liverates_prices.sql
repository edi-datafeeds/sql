DELETE FROM exchgrates.liverates
WHERE exchgrates.liverates.Feeddate < ( SELECT * FROM ( SELECT MAX(Feeddate) FROM exchgrates.liverates ) AS tmpTab );

INSERT INTO exchgrates.liverates (Curr, Rate, Base, Src, time, actflag, feeddate, Acttime)
SELECT 
	'GBX' AS curr,  ( 100 * rate ) AS rate, base, Src, time, actflag, feeddate, Acttime
FROM  exchgrates.liverates
	WHERE curr = 'GBP' 
ORDER BY acttime DESC;
