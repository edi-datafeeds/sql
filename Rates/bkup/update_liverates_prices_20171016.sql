Delete from exchgrates.liverates
where exchgrates.liverates.Feeddate < CURDATE();

INSERT INTO exchgrates.liverates (Curr, Rate, Base, Src, time, actflag, feeddate, Acttime)
SELECT 
	'GBX' AS curr,  ( 100 * rate ) AS rate, base, Src, time, actflag, feeddate, Acttime
FROM  exchgrates.liverates
	WHERE curr = 'GBP' 
ORDER BY acttime DESC;
