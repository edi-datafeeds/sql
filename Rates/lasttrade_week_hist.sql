INSERT INTO prices.lasttrade_week_hist 
SELECT * FROM prices.lasttrade 
WHERE  MktcloseDate = CURDATE()
HAVING PriceDate  >= DATE_SUB( NOW(), INTERVAL 2 DAY ) AND PriceDate  <= NOW()
ORDER BY MktcloseDate DESC;