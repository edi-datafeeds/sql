--filepath=O:\Datafeed\Xrates\Feed\Daily\
--filenameprefix=Missing
--filename=2012
--filenamealt=
--fileextension=.txt
--suffix=_08-09
--fileheadertext=EDI_2009_Rates_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\xrates\Daily\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
select Curr as ISO,Base, Rate, Feeddate as Date 
from xrates.rates
where xrates.rates.Feeddate > "2012-08-14" and xrates.rates.Feeddate < "2012-09-26" order by xrates.rates.Feeddate asc;
