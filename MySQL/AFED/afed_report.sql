
## 08/10/2014 - DJ - 2014100610000014 
#---------------------------------------
use afed_data
SELECT * from list_series_value as a
JOIN list_series as b on a.lbl_id = b.lbl_id
WHERE a.active = 1 and b.active <> 1;

SELECT * from list_series_value as a
JOIN units as b on a.unt_id = b.unt_id
WHERE a.active = 1 and b.active <> 1;

SELECT * from list_series_value as a
JOIN scales as b on a.sc_id = b.sc_id
WHERE a.active = 1 and b.active <> 1;

SELECT * from list_series_value as a
JOIN freq as b on a.frq_id = b.frq_id
WHERE a.active = 1 and b.active <> 1;

SELECT * from list_series_value as a
JOIN value_type as b on a.vt_id = b.vt_id
WHERE a.active = 1 and b.active <> 1;

SELECT * from list_series_value as a
JOIN locality as b on a.cntrycd = b.CD
WHERE a.active = 1 and b.active <> 1;

SELECT v.* from list_series_value as v
JOIN list_series as s on v.lbl_id = s.lbl_id
JOIN topics as t on s.topic_cd = t.topic_cd
where v.active = 1 and t.active <> 1;

SELECT * from list_series_value as v
INNER JOIN locality as l on v.cntrycd = l.CD
WHERE l.active = 1 and v.active = 1 and l.type_cd = 'C' and v.type_cd <> 'C';  
SELECT * from list_series_value as v
INNER JOIN locality as l on v.cntrycd = l.CD
WHERE l.active = 1 and v.active = 1 and l.type_cd = 'R' and v.type_cd <> 'R';  
SELECT * from list_series_value as v
INNER JOIN locality as l on v.cntrycd = l.CD
WHERE l.active = 1 and v.active = 1 and l.type_cd = 'A' and v.type_cd <> 'A';

SELECT * from list_series_value WHERE active = 1 and `value` is NULL;

SELECT * from list_series_value as v
JOIN locality as l on v.cntrycd = l.CD
WHERE v.active = 1 and l.active <> 1;