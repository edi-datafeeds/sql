--filepath=O:\Datafeed\MySQL\Internal\Qrt\Marketing\
--filenameprefix=
--filename=yyyymmdd
--fileextension=.csv
--suffix=_New_Client_Update
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=,PLEASE REPORT ANY, ISSUES WITH THIS, FILE TO, ServiceRequests@exchange-data.com
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\Marketing\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--filenamealt=
--fileheadertext=New Client Report 

--# 1
 SELECT distinct
 company.company_id,
 Company,
 ClientManager,
 client_date,
 concat_ws('-',mtx_products.prod_cd,mtx_products.prod_type) as Product,
 mtx_products.start_service
 from edi_cms.company
 inner join edi_cms.mtx_contract on company.company_id = mtx_contract.company_id
 inner join edi_cms.mtx_products on mtx_contract.contract_id = mtx_products.contract_id
 where client_date > date_sub(curdate(), interval 3 month)
 and mtx_contract.freq !='One Off'
 group by mtx_products.prod_cd, mtx_contract.contract_id, company_id
 order by Company
