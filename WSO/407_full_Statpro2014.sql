--FileNamePrefix=
--FileName=ALyymmdd
--FileExtension=.407
--FileHeaderTEXT=EDI_WSO_407_
--fileheaderdate=yyyymmdd
--archive=y
--ArchivePath=n:\WSO\407\
--FileTidy=y
--fieldheaders=u
--filenameprefix=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT DISTINCT
EXCHG.CntryCD as CTRYCODE,
CASE WHEN (SCEXH.ExchgCD is null) THEN Exchg.Exchgname WHEN (Exchg.Exchgname is null) and (LEN(SCEXH.ExchgCD) > 0) THEN '[' + SCEXH.ExchgCD +'] not found' ELSE Exchg.Exchgname END as EXCHNAME,
SCMST.Acttime as SOURCEDATE,
SCEXH.Acttime as MODDATE,
ISSUR.Issuername as ISSNAME,
case when sedol.actflag<>'D' and sedol.defunct='F' then SEDOL.sedol else '' end as Sedol,
SCMST.Isin as ISIN,
Scexh.LocalCode as LOCALCODE,
SCMST.SecurityDesc +' '+ SCMST.Parvalue +' '+ SCMST.CurenCD as SECDESC,
CASE WHEN SCMST.SectyCD = 'EQS' THEN 'Equity' WHEN SCMST.SectyCD = 'PRF' THEN 'Preference' WHEN SCMST.SectyCD = 'DR' THEN 'Depositary Receipt' ELSE SCMST.SectyCD END as SECTYPE,
INDUS.Indusname as INDSECTOR,
SCMST.SharesOutstanding as SOS,
CASE WHEN (EXCHG.CntryCD = 'US' or EXCHG.CntryCD= 'CA') THEN SCMST.Uscode ELSE '' END as USCODE
FROM SCMST
LEFT OUTER JOIN Issur ON Scmst.IssID = Issur.IssID
LEFT OUTER JOIN Scexh ON Scmst.SecID = Scexh.SecID
LEFT OUTER JOIN EXCHG ON SCEXH.ExchgCD = EXCHG.ExchgCD
left outer join sedol ON scmst.secid = sedol.secid
                        and Sedol.CntryCD = exchg.CntryCD
LEFT OUTER JOIN INDUS ON ISSUR.IndusID = INDUS.IndusID
Where 
(scmst.primaryexchgcd = scexh.exchgcd 
     or scmst.primaryexchgcd is null
     or scmst.primaryexchgcd = '')
AND ISSUR.Issuername is not null
AND SCMST.Actflag <> 'D'
AND SCEXH.Actflag <> 'D'
AND SCEXH.liststatus <> 'D'
and SCMST.SharesOutstanding<>''
and SCMST.SharesOutstanding<>'0'
and SCMST.sectycd<>'BND'
ORDER BY ISSUR.Issuername


/*  Data format 407 feed
field			Datatype(maxwidth)	Description
Ctrycode,		char(2)			Country code of Exchange
Exchname,		varchar(50)		Exchange Name
Sourcedate,		date(yyyy/mm/dd)	Date of last refresh from EDI's datasource
Acttime,		date(yyyy/mm/dd)	Date record last modified
Issname,		char(75)		Issuer Name
Sedol,			char(7)
Isin,			char(12) 
Localcode,		varchar(20)
Secdesc,		varchar(50)		Security Description
Sectype,		varchar(40)		Security Type
Indsector,		varchar(250)		Industry Sector
Sos,			varchar(25)		Shares outstanding
Uscode,			char(9)	
*/
