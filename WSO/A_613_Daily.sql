


--filepath=o:\datafeed\wso\613\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.613
--suffix=
--fileheadertext=EDI_WCA_xShares_Outstanding_Change_yyyymmdd
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WSO\316\
--fieldheaders=
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT * 
FROM v51f_612_xShares_Outstanding_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
order by eventid