--filepath=o:\upload\acc\173\feed\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.615
--suffix=
--fileheadertext=EDI_WCA_SOS_Bespoke_Portfolio_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\173\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
use wca
select distinct
case when scmst2.isin = client.dbo.pfisin.code or scmst.secid is null then 'PORT' ELSE 'EDI' end as IsinOrigin,
case when scmst2.isin = client.dbo.pfisin.code or scmst.secid is null THEN client.dbo.pfisin.code else scmst2.isin end as isin,
scmst2.IssID,
scmst2.SecID,
case when SHOCH.shochid is not null then SHOCH.AnnounceDate else null end as Created,
case when SHOCH.shochid is not null then SHOCH.Acttime else null end as Changed,
'' as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst2.SecurityDesc,
scmst2.Parvalue,
scmst2.CurenCD as PVCurrency,
scmst2.USCode,
case when scmst2.secid is null then '' when scmst2.Statusflag = 'A'  or scmst2.Statusflag is null then 'A' ELSE 'I' END as Statusflag,
CASE WHEN (scmst2.PrimaryExchgCD IS NULL OR scmst2.PrimaryExchgCD='') THEN '' WHEN (scmst2.PrimaryExchgCD=v20c_610_SCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
case when bond.bondtype is not null then bond.bondtype else scmst2.sectycd end as sectycd,
v20c_610_SCEXH.Exchange,
v20c_610_SCEXH.ExchgCD,
v20c_610_SCEXH.MIC,
v20c_610_SCEXH.ExCountry,
v20c_610_SCEXH.RegCountry,
v20c_610_SCEXH.Sedol,
v20c_610_SCEXH.LocalCode,
v20c_610_SCEXH.ListingStatus,
v20c_610_SCEXH.ListDate,
SHOCH.shochid as EventID,
SHOCH.EffectiveDate,
SHOCH.OldSos,
case when SHOCH.shochid is not null then SHOCH.NewSos else scmst2.sharesoutstanding end as newsos,
dprcp.levdesc
from client.dbo.pfisin
left outer join scmst on client.dbo.pfisin.code = scmst.isin
left outer JOIN issur on scmst.issid = issur.issid
left outer JOIN scmst as scmst2 on issur.issid = scmst2.issid
left outer JOIN bond on scmst2.secid = bond.secid
LEFT OUTER JOIN SHOCHSEQ ON scmst2.SecID = SHOCHSEQ.secid and 1 = shochseq.seqnum
LEFT OUTER JOIN SHOCH ON shochseq.shochid = SHOCH.shochID
left outer JOIN v20c_610_SCEXH ON scmst2.SecID = v20c_610_SCEXH.SecID
LEFT OUTER JOIN DPRCP ON scmst2.secid = dprcp.secid
where
client.dbo.pfisin.accid = 173 and client.dbo.pfisin.actflag<>'D'
and (scmst2.PrimaryExchgCD=v20c_610_SCEXH.ExchgCD
     or scmst2.PrimaryExchgCD IS NULL OR scmst2.PrimaryExchgCD='')
and (scmst2.statusflag <> 'I' or scmst.secid = scmst2.secid or scmst2.secid is null)
and (scmst2.SectyCD<>'CDI' or scmst2.secid is null)
and (scmst2.SectyCD<>'DEB' or scmst2.secid is null)
and (scmst2.SectyCD<>'MF' or scmst2.secid is null)
and (scmst2.SectyCD<>'NCD' or scmst2.secid is null)
and (scmst2.SectyCD<>'NTS' or scmst2.secid is null)
and (scmst2.SectyCD<>'CW' or scmst2.secid is null)
and (scmst2.SectyCD<>'DEB' or scmst2.secid is null)
and (scmst2.SectyCD<>'RUN' or scmst2.secid is null)
and (bond.Bondtype<>'DB' or bond.secid is null)
and (bond.Bondtype<>'NT' or bond.secid is null)
and not (scmst2.SectyCD='BND' and bond.Bondtype is null)
AND (not (scmst2.isin <> client.dbo.pfisin.code and scmst2.Isin IN 
  (select code from client.dbo.pfisin 
        where client.dbo.pfisin.accid = 173 and client.dbo.pfisin.actflag<>'D')) or scmst.secid is null) 
and ((bond.bondtype = 'BD' and scmst2.securitydesc like '%conv%') or bond.secid is null or scmst2.sectycd='PRF')
and (v20c_610_SCEXH.ListingStatus<>'Delisted' or v20c_610_SCEXH.ListingStatus is null or scmst.secid = scmst2.secid or scmst2.secid is null)
