--FilePath=o:\Datafeed\wso\503MFULL\
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--FileNamePrefix=AL
--FileName=YYMMDD
--FileExtension=.503
--FileHeaderTEXT=RECORDS = nnnnnn SECURITY DATA 
--FileHeaderDate=dd/mm/yyyy
--FieldHeaders=n
--OutputStyle=FW
--FWOffsets=LS1,Rz10,LS1,LS11,LS9,LS2,LS3,LS8,Rz10,LS1,LS21,LS13,LS41,LS2,LS8,LS11,Rz25,LS1,LS12,Rz25,LS1,LS11,LS11,LS12,Rz10
--archive=on
--ArchivePath=n:\WSO\503mel\
--DataDateFormat=DD/MM/YYYY
--DataTimeFormat=
--ForceTime

--# 1
use wca
SELECT 
' ' as dummy1,
SCMST.SecID,
' ' as dummy2,
SCMST.Acttime as SCMSTChanged,
CONVERT ( varchar , SCMST.Acttime,8) as SCMSTtime,
SCMST.Actflag,  
substring(PrimaryExchgCD,1,2) as ExCountry, 
SCMST.PrimaryExchgCD,
SCMST.IssID,  
' ' as dummy3,
'                    ' as Localcode,
SCMST.ISIN, 
substring(SCMST.SecurityDesc,1,41) as SecurityDesc,
' ' as Typecode,
SEDOL.Sedol,  
SCMST.USCode,
case when SHOCH.shochid is not null then SHOCH.NewSos else scmst.sharesoutstanding end as newsos,
' ' as dummy4,
case when SHOCH.shochid is not null then CONVERT ( varchar , SHOCH.EffectiveDate,103) else '01/01/1900' end as EffectiveDate,
case when SHOCH.shochid is not null then SHOCH.OldSos else '                         ' end as oldsos,
' ' as dummy5,
'          ' as Prevdate, 
SCMST.AnnounceDate as SCMSTAnnounce,
case when SHOCH.shochid is not null then CONVERT ( varchar , SHOCH.AnnounceDate,103) else '01/01/1900' end as SHOCHAnnounce,
SCMST.IssID
from scmst
INNER JOIN issur on scmst.issid = issur.issid
INNER JOIN SectyMap on scmst.SectyCD = SectyMap.SectyCD and SectyMap.Typegroup = 'Equity'
LEFT OUTER JOIN shochseq1 ON scmst.SecID = shochseq1.secid and 1 = shochseq1.seqnum
LEFT OUTER JOIN SHOCH ON shochseq1.shochid = SHOCH.shochID
LEFT OUTER JOIN SEDOL ON substring(SCMST.PrimaryExchgCD,1,2) = SEDOL.CntryCD
and SCMST.SecID = SEDOL.SecID
where
(shoch.shochid is not null or scmst.sharesoutstanding <> '')
and (Statusflag<>'I' or Statusflag is null)
and scmst.actflag<>'D'
and issur.actflag<>'D'
order by scmst.secid
