--filepath=o:\datafeed\wso\619_GB_full\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.619
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\204\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
use wca
select
scmst.SecID,
case when SHOCH.shochid is not null then SHOCH.AnnounceDate else null end as Created,
case when SHOCH.shochid is not null then SHOCH.Acttime else null end as Changed,
case when SHOCH.Actflag='D' or scmst.actflag='D' then 'D' else '' end as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.ISIN,
scmst.USCode,
case when scmst.Statusflag = 'A'  or scmst.Statusflag is null or scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
scmst.PrimaryExchgCD,
scmst.SectyCD,
v20c_610_SCEXH.Exchange,
v20c_610_SCEXH.ExchgCD,
v20c_610_SCEXH.MIC,
v20c_610_SCEXH.ExCountry,
v20c_610_SCEXH.RegCountry,
v20c_610_SCEXH.Sedol,
v20c_610_SCEXH.LocalCode,
v20c_610_SCEXH.ListingStatus,
v20c_610_SCEXH.ListDate,
SHOCH.shochid as EventID,
SHOCH.EffectiveDate,
SHOCH.OldSos,
case when SHOCH.shochid is not null then SHOCH.NewSos else scmst.sharesoutstanding end as newsos,
SHOCH.ShochNotes
from scmst
INNER JOIN v20c_610_SCEXH ON scmst.SecID = v20c_610_SCEXH.SecID
INNER JOIN issur on scmst.issid = issur.issid
INNER JOIN SectyMap on scmst.SectyCD = SectyMap.SectyCD and SectyMap.Typegroup = 'Equity'
LEFT OUTER JOIN shochseq1 ON scmst.SecID = shochseq1.secid and 1 = shochseq1.seqnum
LEFT OUTER JOIN SHOCH ON shochseq1.shochid = SHOCH.shochID
where
v20c_610_SCEXH.ExCountry='GB'
and (shoch.shochid is not null or scmst.sharesoutstanding <> '')
and (Statusflag<>'I' or Statusflag is null)
and scmst.actflag<>'D'
and issur.actflag<>'D'
