--FilePath=o:\Datafeed\wso\503MELdup\
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--FileNamePrefix=AL
--FileName=YYMMDD
--FileExtension=.503
--FileHeaderTEXT=RECORDS = nnnnnn SECURITY DATA 
--FileHeaderDate=dd/mm/yyyy
--FieldHeaders=n
--OutputStyle=FW
--FWOffsets=LS1,Rz10,LS1,LS11,LS9,LS2,LS3,LS8,Rz10,LS1,LS21,LS13,LS41,LS2,LS8,LS11,Rz25,LS1,LS12,Rz25,LS1,LS11,LS11,LS12,Rz10
--Archive=y
--ArchivePath=n:\WSO\503mel\
--DataDateFormat=DD/MM/YYYY
--DataTimeFormat=
--ForceTime

--# 1
use wca
SELECT 
' ' as dummy1,
SCMST.SecID,
' ' as dummy2,
SCMST.Acttime,
CONVERT ( varchar , SCMST.Acttime,8) as shochtime,
SCMST.Actflag,  
substring(PrimaryExchgCD,1,2) as ExCountry, 
SCMST.PrimaryExchgCD,
SCMST.IssID,  
' ' as dummy3,
'                    ' as Localcode,
SCMST.ISIN, 
substring(SCMST.SecurityDesc,1,41),
' ' as Typecode,
SEDOL.Sedol,  
SCMST.USCode,
case when SHOCH.shochid is not null then SHOCH.NewSos else scmst.sharesoutstanding end as newsos,
' ' as dummy4,
case when SHOCH.shochid is not null then CONVERT ( varchar , SHOCH.EffectiveDate,103) else '01/01/1900' end as EffectiveDate,
case when SHOCH.shochid is not null then SHOCH.OldSos else '                         ' end as newsos,
' ' as dummy5,
'          ' as Prevdate, 
SCMST.AnnounceDate,
case when SHOCH.shochid is not null then CONVERT ( varchar , SHOCH.AnnounceDate,103) else '01/01/1900' end as AnnounceDate1,
SCMST.IssID
FROM SCMST
INNER JOIN SHOCH ON SHOCH.SecID = SCMST.SecID
LEFT OUTER JOIN SEDOL ON substring(SCMST.PrimaryExchgCD,1,2) = SEDOL.CntryCD
and SCMST.SecID = SEDOL.SecID
LEFT OUTER JOIN irACTION ON SHOCH.Actflag = irACTION.Code
where
SHOCH.acttime >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (SCMST.Statusflag <> 'I' OR SCMST.Statusflag IS NULL)
order by SCMST.secid
