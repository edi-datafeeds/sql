--filenameprefix=619
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_myCAC40_Daily_SampleData
--fileheadertext=EDI_REORG_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--#
select
wca.scmst.SecID,
case when wca.shh.shochid is not null then wca.shh.AnnounceDate else null end as Created,
case when wca.shh.shochid is not null then wca.shh.Acttime else null end as Changed,
'' as Actflag,
wca.issur.CntryofIncorp,
wca.issur.IssuerName,
wca.scmst.SecurityDesc,
wca.scmst.Parvalue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.ISIN,
wca.scmst.USCode,
case when wca.scmst.Statusflag = 'A'  or wca.scmst.Statusflag is null or wca.scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
wca.scmst.PrimaryExchgCD,
wca.scmst.SectyCD,
wca.exchg.exchgname as Exchange,
wca.scexh.ExchgCD,
wca.exchg.MIC,
wca.exchg.cntrycd as ExCountry,
case when wca.sedol.defunct='F' then wca.sedol.rcntrycd else '' end as RegCountry,
case when wca.sedol.defunct='F' then wca.sedol.Sedol else '' end as Sedol,
wca.scexh.LocalCode,
case when wca.scexh.ListStatus='S' then 'S' else 'L' end as ListingStatus,
wca.scexh.ListDate,
wca.shh.shochid as EventID,
wca.shh.EffectiveDate,
wca.shh.OldSos,
case when wca.shh.shochid is not null then wca.shh.NewSos else wca.scmst.sharesoutstanding end as newsos,
'' as shochNotes
FROM wca.scmst
inner JOIN wca.sectygrp on wca.scmst.SectyCD = wca.sectygrp.SectyCD and 3>secgrpid
inner join wca.issur ON wca.scmst.IssID = wca.issur.IssID
inner JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID and 'D'<>scexh.liststatus
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.sedol on wca.scexh.secid = wca.sedol.secid and wca.exchg.cntrycd = wca.sedol.cntrycd
left outer join wca.shoch as shh on wca.scmst.secid = wca.shh.secid and 'D'<>shh.actflag
where
wca.scmst.secid in (select secid from icon.ixcon where ixnamid=11 and onindex='T')
and (Statusflag<>'I' or Statusflag is null)
and wca.scmst.actflag<>'D'
and wca.issur.actflag<>'D'
and wca.exchg.exchgcd in (select exchgcd from icon.ixnam where ixnamid=11)
and ((wca.shh.shochid is null and wca.scmst.sharesoutstanding<>'') or
wca.shh.shochid = (select shochid from wca.shoch where wca.scmst.secid=wca.shoch.secid order by wca.shoch.secid, effectivedate desc limit 1))
