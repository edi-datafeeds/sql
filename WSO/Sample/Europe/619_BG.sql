--filenameprefix=619
--filename=
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select top 5
scmst.SecID,
case when shh.shochid is not null then shh.AnnounceDate else null end as Created,
case when shh.shochid is not null then shh.Acttime else null end as Changed,
'' as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.ISIN,
scmst.USCode,
case when scmst.Statusflag = 'A'  or scmst.Statusflag is null or scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
scmst.PrimaryExchgCD,
scmst.SectyCD,
exchg.exchgname as Exchange,
SCEXH.ExchgCD,
exchg.MIC,
exchg.cntrycd as ExCountry,
case when sedol.defunct='F' then sedol.rcntrycd else '' end as RegCountry,
case when sedol.defunct='F' then sedol.Sedol else '' end as Sedol,
SCEXH.LocalCode,
case when SCEXH.ListStatus='S' then 'S' else 'L' end as ListingStatus,
SCEXH.ListDate,
shh.shochid as EventID,
shh.EffectiveDate,
shh.OldSos,
case when shh.shochid is not null then shh.NewSos else scmst.sharesoutstanding end as newsos,
'' as shochNotes
FROM scmst
inner JOIN Sectygrp on scmst.SectyCD = Sectygrp.SectyCD and 3>secgrpid
inner join ISSUR ON SCMST.IssID = ISSUR.IssID
inner JOIN scexh ON scmst.SecID = scexh.SecID and 'D'<>scexh.liststatus
inner join exchg on scexh.exchgcd = exchg.exchgcd and 'BG'=exchg.cntrycd
left outer join sedol on scexh.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join shoch as shh on scmst.secid = shh.secid and 'D'<>shh.actflag
where
(Statusflag<>'I' or Statusflag is null)
and scmst.actflag<>'D'
and issur.actflag<>'D'
order by shh.acttime desc, scmst.acttime desc
