--filepath=o:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.670
--suffix=_670
--fileheadertext=EDI_WCA_Shares_Outstanding_670_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=hh:mm:ss
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select
case when shh.shochid is not null then shh.Acttime else null end as acttime,
shh.actflag,
scmst.secid,
issur.issuername,
scmst.isin,
scmst.securitydesc,
scmst.sectycd,
bbc.cntrycd as listing_country,
bbc.curencd as trading_currency,
bbc.bbgcompid as composite_global_id,
bbc.bbgcomptk as bloomberg_composite_ticker,
bbe.exchgcd as listing_exchange,
bbe.bbgexhid as bloomberg_global_id,
bbe.bbgexhtk as bloomberg_exchange_ticker,
shh.shochid as eventid,
'SHOCH' as eventtype,
shh.effectivedate,
'F' as lateflag,
'' as ratioold,
'' as rationew,
shh.oldsos,
case when shh.shochid is not null then shh.NewSos else scmst.sharesoutstanding end as newsos
from scmst
inner JOIN Sectygrp on scmst.SectyCD = Sectygrp.SectyCD and 3>secgrpid
inner join ISSUR ON SCMST.IssID = ISSUR.IssID
inner JOIN scexh ON scmst.SecID = scexh.SecID and 'D'<>scexh.liststatus
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join shoch as shh on scmst.secid = shh.secid and 'D'<>shh.actflag
left outer join bbc on scmst.secid = bbc.secid and substring(scmst.primaryexchgcd,1,2)=bbc.cntrycd and 'D'<>bbc.actflag
left outer join bbe on scmst.secid = bbe.secid and bbc.cntrycd = substring(bbe.exchgcd,1,2) and bbc.curencd=bbe.curencd and 'D'<>bbe.actflag
where
(Statusflag<>'I' or Statusflag is null)
and scmst.actflag<>'D'
and issur.actflag<>'D'
and ((shh.shochid is null and scmst.sharesoutstanding<>'') or
shh.shochid in (select top 1 shochid from wca.dbo.shoch where scmst.secid=wca.dbo.shoch.secid order by wca.dbo.shoch.secid, effectivedate desc))
