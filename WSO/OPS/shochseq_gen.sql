use wca
if exists (select * from sysobjects where name = 'SHOCHSEQ1')
	drop table SHOCHSEQ1
go

select 
SHOCH.secid,
SHOCH.effectivedate,
1 as seqnum,
SHOCH.shochid
into SHOCHSEQ1
from SHOCH
inner join scmst on shoch.secid = scmst.secid
where 
SHOCH.actflag<>'D'
and scmst.actflag<>'D'
and scmst.statusflag<>'I'
go


use WCA
ALTER TABLE SHOCHSEQ1 ALTER COLUMN  shochid int NOT NULL
go


ALTER TABLE [DBO].[SHOCHSEQ1] WITH NOCHECK ADD
	
	CONSTRAINT [PK_SHOCHSEQ1] PRIMARY KEY 
	
	(
		[shochid]
	)  ON [PRIMARY] 

go 

use WCA
CREATE  INDEX [ix_secid_SHOCHSEQ1] ON [dbo].[SHOCHSEQ1]([secid]) ON [PRIMARY] 
GO 

USE WCA 
CREATE  INDEX [ix_effd_Shochseq1] ON [dbo].[Shochseq1]([secid],[effectivedate] desc) ON [PRIMARY] 
GO

EXEC sp_shochseq1

go
