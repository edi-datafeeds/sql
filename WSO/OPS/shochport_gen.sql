use wca
if exists (select * from sysobjects where name = 'shochseq')
	drop table shochseq
go

use wca
select distinct
scmst2.SecID,
SHOCH.effectivedate,
1 as seqnum,
SHOCH.shochid
into shochseq
from client.dbo.pfisin
left outer join scmst on client.dbo.pfisin.code = scmst.isin
left outer JOIN issur on scmst.issid = issur.issid
left outer JOIN scmst as scmst2 on issur.issid = scmst2.issid
left outer JOIN bond on scmst2.secid = bond.secid
LEFT OUTER JOIN SHOCH ON scmst2.SecID = SHOCH.SecID
where
client.dbo.pfisin.accid = 173 and client.dbo.pfisin.actflag<>'D'
and ((scmst2.SectyCD<>'CDI'
    and (scmst2.statusflag <> 'I' or scmst.secid = scmst2.secid)
     and scmst2.SectyCD<>'DEB'
     and scmst2.SectyCD<>'MF'
     and scmst2.SectyCD<>'NCD'
     and scmst2.SectyCD<>'NTS'
     and scmst2.SectyCD<>'CW'
     and scmst2.SectyCD<>'DEB'
     and scmst2.SectyCD<>'RUN'
     and (bond.Bondtype<>'BD' or bond.secid is null)
     and (bond.Bondtype<>'DB' or bond.secid is null)
     and (bond.Bondtype<>'NT' or bond.secid is null)))
AND SHOCH.actflag<>'D'
AND SHOCH.shochid is not null

go



use WCA
ALTER TABLE shochseq ALTER COLUMN  shochid int NOT NULL
go

ALTER TABLE [DBO].[shochseq] WITH NOCHECK ADD
	
	CONSTRAINT [PK_shochseq] PRIMARY KEY 
	
	(
		[shochid]
	)  ON [PRIMARY] 

go 

use WCA
CREATE  INDEX [ix_secid_shochseq] ON [dbo].[shochseq]([secid]) ON [PRIMARY] 
GO 

EXEC sp_shochseq
go
