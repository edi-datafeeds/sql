--filepath=o:\datafeed\wso\614\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.614
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_614_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wso\614\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
use wca
select
scmst.SecID,
case when SHOCH.shochid is not null then SHOCH.AnnounceDate else null end as Created,
case when SHOCH.shochid is not null then SHOCH.Acttime else null end as Changed,
'' as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.ISIN,
scmst.USCode,
case when scmst.Statusflag = 'A'  or scmst.Statusflag is null then 'A' ELSE 'I' END as Statusflag,
CASE WHEN (scmst.PrimaryExchgCD IS NULL OR scmst.PrimaryExchgCD='') THEN '' WHEN (scmst.PrimaryExchgCD=v20c_610_SCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
scmst.SectyCD,
v20c_610_SCEXH.Exchange,
v20c_610_SCEXH.ExchgCD,
v20c_610_SCEXH.MIC,
v20c_610_SCEXH.ExCountry,
v20c_610_SCEXH.RegCountry,
v20c_610_SCEXH.Sedol,
v20c_610_SCEXH.LocalCode,
v20c_610_SCEXH.ListingStatus,
v20c_610_SCEXH.ListDate,
SHOCH.shochid as EventID,
SHOCH.EffectiveDate,
SHOCH.OldSos,
case when SHOCH.shochid is not null then SHOCH.NewSos else scmst.sharesoutstanding end as newsos,
SHOCH.ShochNotes
from scmst
INNER JOIN issur on scmst.issid = issur.issid
INNER JOIN SectyMap on scmst.SectyCD = SectyMap.SectyCD and SectyMap.Typegroup = 'Equity'
LEFT OUTER JOIN shoch ON scmst.SecID = shoch.secid
INNER JOIN v20c_610_SCEXH ON scmst.SecID = v20c_610_SCEXH.SecID
where
(shoch.shochid is not null or scmst.sharesoutstanding <> '')
and (Statusflag<>'I' or Statusflag is null)
and scmst.actflag<>'D'
and issur.actflag<>'D'
and (scmst.PrimaryExchgCD=v20c_610_SCEXH.ExchgCD or
scmst.PrimaryExchgCD IS NULL OR scmst.PrimaryExchgCD='')
and (shoch.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
    or scmst.announcedate >= (select max(acttime) from tbl_Opslog where seq = 1))
