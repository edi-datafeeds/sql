--filepath=o:\Datafeed\wso\503dup\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.503
--suffix=
--fileheadertext=RECORDS = nnnnnn SECURITY DATA dd/mm/yyyy
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WSO\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n





Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = '2002/01/01'
set @EndDate = getDate()+1





/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca
SELECT 
'' as dummy1,
v20c_610_SCMST.SecID,
'' as dummy2,
SHOCH.Acttime,
CONVERT ( varchar , SHOCH.GcDatetime,8) as shochtime,
SHOCH.Actflag,
substring(SCEXH.ExchgCD,1,2) as ExCountry, 
v20c_610_SCMST.PrimaryExchgCD,
v20c_610_SCMST.IssID,  
'' as dummy3,
'' as Localcode,
v20c_610_SCMST.ISIN, 
v20c_610_SCMST.SecurityDesc,
'' as Typecode,
SEDOL.Sedol,  
v20c_610_SCMST.USCode,
SHOCH.NewSos, 
'' as dummy4,
SHOCH.EffectiveDate,
SHOCH.OldSos, 
'' as dummy5,
'' as Prevdate, 
SHOCH.AnnounceDate,
SHOCH.Acttime,
v20c_610_SCMST.IssID
FROM v20c_610_SCMST
INNER JOIN SHOCH ON SHOCH.SecID = v20c_610_SCMST.SecID
INNER JOIN SCEXH ON v20c_610_SCMST.SecID = SCEXH.SecID
                  AND substring(SCEXH.ExchgCD,1,2)='GB'
LEFT OUTER JOIN SEDOL ON substring(SCEXH.ExchgCD,1,2) = SEDOL.CntryCD
and v20c_610_SCMST.SecID = SEDOL.SecID
LEFT OUTER JOIN irACTION ON SHOCH.Actflag = irACTION.Code
WHERE (SHOCH.acttime BETWEEN '2002/01/01' AND getDate()+1) 
AND (v20c_610_SCMST.SecStatus <> 'Inactive' OR v20c_610_SCMST.Secstatus IS NULL)
AND SCEXH.ListStatus<>'D'
order by v20c_610_scmst.secid, scexh.exchgcd, SHOCH.GcDateTime desc

