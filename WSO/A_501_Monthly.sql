--filepath=o:\Datafeed\wso\GBSOS\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.501
--suffix=
--fileheadertext=RECORDS = nnnnnn SECURITY DATA 
--fileheaderdate=dd/mm/yyyy
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WSO\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = '2002/01/01'
set @EndDate = getDate()+1


/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
use wca
SELECT 
ISSUR.IssID,
ISSUR.Acttime,
ISSUR.Actflag,
SCEXH.ExchgCD,
ISSUR.Issuername,
ISSUR.Cntryofincorp,
ISSUR.IndusID,
ISSUR.Announcedate
FROM ISSUR
inner join scmst on issur.issid = scmst.issid
INNER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
                  AND substring(SCEXH.ExchgCD,1,2)='GB'
where 
SCMST.sharesoutstanding <> '0'
AND SCMST.sharesoutstanding <> ''
AND SCMST.sharesoutstanding is not null
order by ISSUR.issuername
