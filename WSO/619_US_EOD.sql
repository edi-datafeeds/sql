--filepath=o:\datafeed\wso\619_US\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.619
--suffix=_US
--fileheadertext=EDI_WCA_Shares_Outstanding_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\wso\619_AU\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--#
use wca
select
scmst.SecID,
case when SHOCH.shochid is not null then SHOCH.AnnounceDate else null end as Created,
case when SHOCH.shochid is not null then SHOCH.Acttime else null end as Changed,
case when SHOCH.Actflag='D' or scmst.actflag='D' then 'D' else '' end as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.ISIN,
scmst.USCode,
case when scmst.Statusflag = 'A'  or scmst.Statusflag is null or scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
scmst.PrimaryExchgCD,
scmst.SectyCD,
v20c_610_SCEXH.Exchange,
v20c_610_SCEXH.ExchgCD,
v20c_610_SCEXH.MIC,
v20c_610_SCEXH.ExCountry,
v20c_610_SCEXH.RegCountry,
v20c_610_SCEXH.Sedol,
v20c_610_SCEXH.LocalCode,
v20c_610_SCEXH.ListingStatus,
v20c_610_SCEXH.ListDate,
SHOCH.shochid as EventID,
SHOCH.EffectiveDate,
SHOCH.OldSos,
case when SHOCH.shochid is not null then SHOCH.NewSos else scmst.sharesoutstanding end as newsos,
SHOCH.ShochNotes
from scmst
INNER JOIN issur on scmst.issid = issur.issid
INNER JOIN SectyMap on scmst.SectyCD = SectyMap.SectyCD and SectyMap.Typegroup = 'Equity'
LEFT OUTER JOIN shoch ON scmst.SecID = shoch.secid
INNER JOIN v20c_610_SCEXH ON scmst.SecID = v20c_610_SCEXH.SecID
where
(shoch.shochid is not null or scmst.sharesoutstanding <> '')
and ((shoch.acttime >='2017/02/24' and shoch.acttime<'2017/02/25')
    or (scmst.announcedate >= '2017/02/24' and scmst.announcedate < '2017/02/25'))
and v20c_610_SCEXH.ExCountry = 'US'
