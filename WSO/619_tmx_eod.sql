--filepath=o:\datafeed\bespoke\tmx\619\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.619
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_619_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\bespoke\tmx\619\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
use wca
select
scmst.SecID,
case when SHOCH.shochid is not null then SHOCH.AnnounceDate else null end as Created,
case when SHOCH.shochid is not null then SHOCH.Acttime else null end as Changed,
case when SHOCH.Actflag='D' or scmst.actflag='D' then 'D' else '' end as Actflag,
issur.CntryofIncorp,
issur.IssuerName,
scmst.SecurityDesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.ISIN,
scmst.USCode,
case when scmst.Statusflag = 'A'  or scmst.Statusflag is null or scmst.Statusflag = '' then 'A' ELSE 'I' END as Statusflag,
scmst.PrimaryExchgCD,
scmst.SectyCD,
v20c_610_SCEXH.Exchange,
v20c_610_SCEXH.ExchgCD,
v20c_610_SCEXH.MIC,
v20c_610_SCEXH.ExCountry,
v20c_610_SCEXH.RegCountry,
v20c_610_SCEXH.Sedol,
CASE when v20c_610_SCEXH.exchgcd<>'USNYSE'
     then v20c_610_SCEXH.LocalCode
     when v20c_610_SCEXH.LocalCode='BPRAP' or v20c_610_SCEXH.LocalCode='SJIU'
     then v20c_610_SCEXH.LocalCode
     when scmst.sectycd='PRF' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-2),
                        replace(substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-1,2),'PR','/PR'))
     when scmst.sectycd='PRF' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-4),'/PR/CL')
     when scmst.sectycd='PRF' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-3),
                        replace(substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-2,3),'PR','/PR/'))
     when scmst.sectycd='PRF' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-4),
                        substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-3,4),'PR','/PR/')
     when scmst.sectycd='PRF' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-5),'/PR/',
                        substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-2,1),'/',
                        substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-1,2))
     when scmst.sectycd='WAR' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-3),'/WS/',
                        substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ','')),1))
     when scmst.sectycd='WAR' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-2),'/WS')
     when (scmst.sectycd='CVR' or scmst.sectycd='TRT') and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-2,3) = '/RT'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-3),'/RT')
     when (scmst.sectycd='CVR' or scmst.sectycd='TRT') and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-2),'/RT')
     when (scmst.sectycd='EQS' or scmst.sectycd='DR') and securitydesc like '%Class A%' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ','')),1) = 'A'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-1),'/A')
     when (scmst.sectycd='EQS' or scmst.sectycd='DR') and securitydesc like '%Class B%' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ','')),1) = 'B'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-1),'/B')
     when substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-3),'/WI')
     when substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-3),'/WD')
     when securitydesc like 'Unit%' and substring(replace(v20c_610_SCEXH.localcode,' ',''),len(replace(v20c_610_SCEXH.localcode,' ','')),1) = 'U'
          and scmst.sectycd='STP'
     then concat(substring(replace(v20c_610_SCEXH.localcode,' ',''),1,len(replace(v20c_610_SCEXH.localcode,' ',''))-1),'/U')
     else v20c_610_SCEXH.LocalCode
     end as LocalCode,
v20c_610_SCEXH.ListingStatus,
v20c_610_SCEXH.ListDate,
case when SHOCH.shochid is not null then SHOCH.shochid else 0 end as EventID,
case when shoch.shochid is not null then shoch.EffectiveDate else scmst.sharesoutstandingdate end as effectivedate,
SHOCH.OldSos,
case when SHOCH.shochid is not null then SHOCH.NewSos else scmst.sharesoutstanding end as newsos,
SHOCH.ShochNotes
from scmst
INNER JOIN issur on scmst.issid = issur.issid
INNER JOIN SectyMap on scmst.SectyCD = SectyMap.SectyCD and SectyMap.Typegroup = 'Equity'
left outer JOIN shoch ON scmst.SecID = shoch.secid
INNER JOIN v20c_610_SCEXH ON scmst.SecID = v20c_610_SCEXH.SecID
where
(v20c_610_SCEXH.ExCountry='AD'
or v20c_610_SCEXH.ExCountry='AL'
or v20c_610_SCEXH.ExCountry='AM'
or v20c_610_SCEXH.ExCountry='AT'
or v20c_610_SCEXH.ExCountry='AZ'
or v20c_610_SCEXH.ExCountry='BA'
or v20c_610_SCEXH.ExCountry='BE'
or v20c_610_SCEXH.ExCountry='BG'
or v20c_610_SCEXH.ExCountry='BY'
or v20c_610_SCEXH.ExCountry='CA'
or v20c_610_SCEXH.ExCountry='CH'
or v20c_610_SCEXH.ExCountry='CY'
or v20c_610_SCEXH.ExCountry='CZ'
or v20c_610_SCEXH.ExCountry='DE'
or v20c_610_SCEXH.ExCountry='DK'
or v20c_610_SCEXH.ExCountry='EE'
or v20c_610_SCEXH.ExCountry='ES'
or v20c_610_SCEXH.ExCountry='FI'
or v20c_610_SCEXH.ExCountry='FR'
or v20c_610_SCEXH.ExCountry='GB'
or v20c_610_SCEXH.ExCountry='GE'
or v20c_610_SCEXH.ExCountry='GR'
or v20c_610_SCEXH.ExCountry='HR'
or v20c_610_SCEXH.ExCountry='HU'
or v20c_610_SCEXH.ExCountry='IE'
or v20c_610_SCEXH.ExCountry='IS'
or v20c_610_SCEXH.ExCountry='IT'
or v20c_610_SCEXH.ExCountry='LI'
or v20c_610_SCEXH.ExCountry='LT'
or v20c_610_SCEXH.ExCountry='LU'
or v20c_610_SCEXH.ExCountry='LV'
or v20c_610_SCEXH.ExCountry='MD'
or v20c_610_SCEXH.ExCountry='ME'
or v20c_610_SCEXH.ExCountry='MK'
or v20c_610_SCEXH.ExCountry='MT'
or v20c_610_SCEXH.ExCountry='NL'
or v20c_610_SCEXH.ExCountry='NO'
or v20c_610_SCEXH.ExCountry='PL'
or v20c_610_SCEXH.ExCountry='PT'
or v20c_610_SCEXH.ExCountry='RO'
or v20c_610_SCEXH.ExCountry='RS'
or v20c_610_SCEXH.ExCountry='SE'
or v20c_610_SCEXH.ExCountry='SI'
or v20c_610_SCEXH.ExCountry='SK'
or v20c_610_SCEXH.ExCountry='SM'
or v20c_610_SCEXH.ExCountry='UA'
or v20c_610_SCEXH.ExCountry='US'
or v20c_610_SCEXH.ExCountry='VA')
and (shoch.shochid is not null
and shoch.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
and SHOCH.NewSos<>SHOCH.OldSos)
OR
(shoch.shochid is not null
and (shoch.eventtype = 'CONSD' or shoch.eventtype = 'SD' or shoch.eventtype = 'CAPRD' 
         or shoch.eventtype = 'ISCHG' or shoch.eventtype = 'ARR')
and shoch.acttime > (select max(feeddate)-31 from tbl_Opslog where seq = 3)
and scmst.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
and SHOCH.NewSos<>SHOCH.OldSos)
OR
(shoch.shochid is null and 
scmst.sharesoutstanding <> ''
and scmst.acttime > (select max(feeddate) from tbl_Opslog where seq = 3))
