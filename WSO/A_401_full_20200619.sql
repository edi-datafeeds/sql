--filepath=o:\DataFeed\WSO\401\
--filenameprefix=
--filename=YYYYMMDD
--filenamealt=
--fileextension=.401
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=DD/MM/YYYY
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wso\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=
--shownulls=





--# 1
use WCA
SELECT 
EXCHG.CntryCD as Ctrycode,
CASE WHEN (SCEXH.ExchgCD is null) THEN Exchg.Exchgname WHEN (Exchg.Exchgname is null) and (LEN(SCEXH.ExchgCD) > 0) THEN '[' + SCEXH.ExchgCD +'] not found' ELSE Exchg.Exchgname END as EXCHNAME,
SCMST.Acttime as SourceDate,
SCEXH.Acttime,
ISSUR.Issuername as Issname,
CASE WHEN (sedol.defunct is null) or (sedol.defunct <> 'T') THEN Sedol.Sedol ELSE '' END as Sedol,
SCMST.Isin,
Scexh.LocalCode,  
SCMST.SecurityDesc +' '+ SCMST.Parvalue +' '+ SCMST.CurenCD as SecDesc,
CASE WHEN SCMST.SectyCD = 'EQS' THEN 'Equity' WHEN SCMST.SectyCD = 'PRF' THEN 'Preference' WHEN SCMST.SectyCD = 'DR' THEN 'Depositary Receipt' ELSE '' END as Sectype,
INDUS.Indusname as Indsector,
SCMST.SharesOutstanding as SOS,
CASE WHEN (EXCHG.CntryCD = 'US' or EXCHG.CntryCD= 'CA') THEN SCMST.Uscode ELSE '' END as Uscode
FROM Scmst
LEFT OUTER JOIN Issur ON Scmst.IssID = Issur.IssID
LEFT OUTER JOIN Scexh ON Scmst.SecID = Scexh.SecID
LEFT OUTER JOIN EXCHG ON SCEXH.ExchgCD = EXCHG.ExchgCD
LEFT OUTER JOIN SEDOL ON SCEXH.SecID = SEDOL.SecID AND EXCHG.CntryCD = SEDOL.CntryCD 
LEFT OUTER JOIN INDUS ON ISSUR.IndusID = INDUS.IndusID
Where (len(ltrim(SCMST.SharesOutstanding)) > 0) 
AND (ISSUR.Issuername is not null)
AND (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null) 
AND (SCMST.SectyCD = 'EQS' or SCMST.SectyCD = 'PRF' or SCMST.SectyCD = 'DR')
AND (SCEXH.ListStatus<>'D' or SCEXH.ListStatus is null)
ORDER BY ISSUR.Issuername

/* Format
field			Datatype	Description
Ctrycode,		char(2)		Country of Exchange
Exchname,		varchar(50)	Exchange name
Sourcedate,		Date		Legacy - now contain security table last modified timestamp
Acttime,		Date		Legacy - Security Excchange (i.e. listing) last modified timestamp
Issname,		varchar(75)	Issuer name
Sedol,			char(7)
Isin,			char(12)
Localcode,		varchar(20)
Secdesc,		varchar(50)	Security Description
lookup AS Sectype,	varchar(50)	Security type
lookup AS Indsector,	varchar(250)	Industry Sector
Sos,			varchar(25)	Shares Outstanding figure
Uscode			char(9)
*/