--filepath=o:\datafeed\wso\403_us_dup\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.403
--suffix=
--fileheadertext=EDI_WCA_403_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wso\403_us_dup\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use WCA
SELECT 
EXCHG.CntryCD as Ctrycode,
CASE WHEN (SCEXH.ExchgCD is null) THEN Exchg.Exchgname WHEN (Exchg.Exchgname is null) and (LEN(SCEXH.ExchgCD) > 0) THEN '[' + SCEXH.ExchgCD +'] not found' ELSE Exchg.Exchgname END as [Exchange],
SHOCH.EffectiveDate,
SHOCH.Acttime as Moddate,
ISSUR.Issuername as Issname,
CASE WHEN (sedol.defunct is null) or (sedol.defunct <> 'T') THEN Sedol.Sedol ELSE '' END as Sedol,
SCMST.Isin,
Scexh.LocalCode,  
SCMST.SecurityDesc +' '+ SCMST.Parvalue +' '+ SCMST.CurenCD as SecDesc,
CASE WHEN SCMST.SectyCD = 'EQS' THEN 'Equity' WHEN SCMST.SectyCD = 'PRF' THEN 'Preference' WHEN SCMST.SectyCD = 'DR' THEN 'Depositary Receipt' ELSE '' END as Sectype,
INDUS.Indusname as Indsector,
SCMST.SharesOutstanding as SOS,
CASE WHEN (EXCHG.CntryCD = 'US' or EXCHG.CntryCD= 'CA') THEN SCMST.Uscode ELSE '' END as Uscode
FROM Shoch
left outer join scmst on shoch.secid = scmst.secid
LEFT OUTER JOIN Scexh ON Scmst.SecID = Scexh.SecID
LEFT OUTER JOIN Issur ON Scmst.IssID = Issur.IssID
LEFT OUTER JOIN EXCHG ON SCEXH.ExchgCD = EXCHG.ExchgCD
LEFT OUTER JOIN SEDOL ON SCEXH.SecID = SEDOL.SecID AND EXCHG.CntryCD = SEDOL.CntryCD
LEFT OUTER JOIN INDUS ON ISSUR.IndusID = INDUS.IndusID
Where (len(ltrim(SCMST.SharesOutstanding)) > 0) 
and (shoch.acttime between getdate()-0.5 and getdate()+1)
and substring(scexh.exchgCD,1,2)='US'
AND (ISSUR.Issuername is not null)
AND (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null) 
AND (SCMST.SectyCD = 'EQS' or SCMST.SectyCD = 'PRF' or SCMST.SectyCD = 'DR')
AND (SCEXH.ListStatus<>'D' or SCEXH.ListStatus is null)
AND (scmst.isin <> '')
AND (scmst.isin is not null)
ORDER BY ISSUR.Issuername, isin, shoch.Acttime desc

/* Format
field				Datatype
Ctrycode,		char(2)
Exchname,		varchar(50)
Sourcedate,		Date
Acttime,		Date
Issname,		varchar(75)
Sedol,			char(7)
Isin,			char(12)
Localcode,		varchar(20)
Secdesc,		varchar(50)
lookup AS Sectype,	varchar(50)
lookup AS Indsector,	varchar(250)
Sos,			varchar(25)
Uscode			char(9)
*/