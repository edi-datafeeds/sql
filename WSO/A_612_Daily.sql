

--filepath=o:\routine\myinfo\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.612
--suffix=
--fileheadertext=EDI_WCA_Shares_Outstanding_Change_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WSO\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT * 
FROM v51f_612_Shares_Outstanding_Change
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 3)
order by eventid