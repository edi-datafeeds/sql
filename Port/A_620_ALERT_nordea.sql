--filepath=o:\upload\acc\170\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=_ALERT
--fileheadertext=EDI_620_ALERT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\170\feed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 



--# 103
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Liquidationdate>=getdate()-0 and Liquidationdate<getdate()+5)
order by changed


--# 105
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed




--# 108
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed



--# 110
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed



--# 112
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed



--# 113
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and ((Closedate>=getdate()-0 and Closedate<getdate()+5)
or (CmAcqdate>=getdate()-0 and CmAcqdate<getdate()+5)
or (TkovrPaydate>=getdate()-0 and TkovrPaydate<getdate()+5))
order by changed



--# 115
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed




--# 117
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed



--# 118
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed


--# 119
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed



--# 121
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed



--# 122
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed


--# 123
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed



--# 124
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed



--# 125
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed

--# 126
use wca
SELECT * 
FROM v54f_620_Bonus_Rights 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed



--# 127
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed



--# 128
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (NotificationDate>=getdate()-0 and NotificationDate<getdate()+5)
order by changed



--# 131
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (NameChangeDate>=getdate()-0 and NameChangeDate<getdate()+5)
order by changed



--# 133
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (DateOfChange>=getdate()-0 and DateOfChange<getdate()+5)
order by changed


--# 134
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed



--# 135
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed




--# 136
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Exdate>=getdate()-0 and Exdate<getdate()+5)
order by changed




--# 137
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed



--# 138
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed

--# 139
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Redemptiondate>=getdate()-0 and Redemptiondate<getdate()+5)
order by changed


--# 140
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed



--# 141
use wca
SELECT * 
FROM v51f_612_xShares_Outstanding_Change
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=170 and actflag<>'D'))
and (Effectivedate>=getdate()-0 and Effectivedate<getdate()+5)
order by changed
