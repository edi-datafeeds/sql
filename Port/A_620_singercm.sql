--filepath=o:\upload\acc\183\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\183\feed\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 2
use wca2
SELECT *
FROM t620_Call
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 3
use wca2
SELECT * 
FROM t620_Liquidation
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 4
use wca2
SELECT *
FROM t620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 5
use wca2
SELECT * 
FROM t620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 6
use wca2
SELECT * 
FROM t620_Conversion_Terms
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef


--# 7
use wca2
SELECT * 
FROM t620_Redemption_Terms
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 8
use wca2
SELECT * 
FROM t620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 9
use wca2
SELECT * 
FROM t620_Lot_Change
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM t620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 11
use wca2
SELECT * 
FROM t620_Buy_Back
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 12
use wca2
SELECT * 
FROM t620_Capital_Reduction
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 13
use wca2
SELECT * 
FROM t620_Takeover
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 14
use wca2
SELECT * 
FROM t620_Arrangement
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 15
use wca2
SELECT * 
FROM t620_Bonus
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 17
use wca2
SELECT * 
FROM t620_Consolidation
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 18
use wca2
SELECT * 
FROM t620_Demerger
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 19
use wca2
SELECT * 
FROM t620_Distribution
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 20
use wca2
SELECT * 
FROM t620_Divestment
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 21
use wca2
SELECT * 
FROM t620_Entitlement
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 22
use wca2
SELECT * 
FROM t620_Merger
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 23
use wca2
SELECT * 
FROM t620_Preferential_Offer
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 24
use wca2
SELECT * 
FROM t620_Purchase_Offer
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 25
use wca2
SELECT * 
FROM t620_Rights 
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 26
use wca2
SELECT * 
FROM t620_Security_Swap 
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM t620_Subdivision
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 28
use wca2
SELECT *
FROM t620_Bankruptcy 
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 29
use wca2
SELECT *
FROM t620_Financial_Year_Change
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 30
use wca2
SELECT *
FROM t620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM t620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 32
use wca2
SELECT *
FROM t620_Class_Action
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
 ORDER BY CaRef

--# 34
use wca2
SELECT *
FROM t620_Assimilation
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM t620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM t620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_New_Listing
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 38
use wca2
SELECT * 
FROM t620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 39
use wca2
SELECT * 
FROM t620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 40
use wca2
SELECT * 
FROM t620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 41
use wca2
SELECT * 
FROM t620_Return_of_Capital 
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef


--# 45
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620_Bonus_Rights
WHERE
(sedol in (select code from portfolio.dbo.fsedol where accid=183 and portfolio.dbo.fsedol.actflag='U'))
and primaryex<>'No'
ORDER BY CaRef


--# 52
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v53f_620_Call
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v53f_620_Call 


--# 53
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v50f_620_Liquidation
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v50f_620_Liquidation 

--# 54
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v51f_620_Certificate_Exchange
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Certificate_Exchange 

--# 55
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v51f_620_International_Code_Change
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_International_Code_Change 

--# 56
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v51f_620_Conversion_Terms
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Conversion_Terms

--# 57
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Redemption_Terms

--# 58
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Security_Reclassification 

--# 59
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Lot_Change 

--# 60
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Sedol_Change 

--# 61
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v53f_620_Buy_Back
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v53f_620_Buy_Back 

--# 62
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v53f_620_Capital_Reduction 

--# 63
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v53f_620_Takeover 

--# 64
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Arrangement
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Arrangement 

--# 65
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Bonus 


--# 67
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Consolidation 

--# 68
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_620_Demerger 

--# 69
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_620_Distribution 

--# 70
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Divestment
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Divestment 

--# 71
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Entitlement 

--# 72
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Merger
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' ORDER BY CaRef, OptionID, SerialID
end
else
SELECT top 0 * FROM v54f_620_Merger 

--# 73
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Preferential_Offer 

--# 74
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Purchase_Offer 

--# 75
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Rights 

--# 76
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Security_Swap 
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Security_Swap 

--# 77
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Subdivision 

--# 78
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Bankruptcy 

--# 79
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v50f_620_Financial_Year_Change
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Financial_Year_Change 

--# 80
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v50f_620_Incorporation_Change
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Incorporation_Change 

--# 81
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Issuer_Name_change 

--# 82
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Class_Action

--# 83
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Security_Description_Change 

--# 84
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v52f_620_Assimilation
WHERE (CHANGED > getdate()-32)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Assimilation 

--# 85
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v52f_620_Listing_Status_Change
WHERE (CHANGED > getdate()-32)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' 
ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Listing_Status_Change 

--# 86
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT *
FROM v52f_620_Local_Code_Change
WHERE (CHANGED > getdate()-32)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' 
ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_Local_Code_Change 

--# 87
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v52f_620_New_Listing
WHERE (CHANGED > getdate()-32)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No' 
ORDER BY CaRef
end
else
SELECT top 0 * FROM v52f_620_New_Listing 

--# 88
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v50f_620_Announcement
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v50f_620_Announcement 

--# 89
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v51f_620_Parvalue_Redenomination 
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Parvalue_Redenomination 

--# 90
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v51f_620_Currency_Redenomination 
WHERE (CHANGED > getdate()-32)
AND 
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Currency_Redenomination 

--# 91
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v53f_620_Return_of_Capital 
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v53f_620_Return_of_Capital 


--# 95
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v51f_620_Conversion_Terms_Change

--# 96
if (select count(portfolio.dbo.fsedol.actflag)  from portfolio.dbo.fsedol where portfolio.dbo.fsedol.actflag = 'I' and accid=183) > 0
begin
use wca
SELECT * 
FROM v54f_620_Bonus_Rights
WHERE (CHANGED > getdate()-32)
AND 
(secid in (select wca.dbo.sedol.secid from wca.dbo.sedol inner join portfolio.dbo.fsedol on wca.dbo.sedol.sedol = portfolio.dbo.fsedol.code where accid=183 and portfolio.dbo.fsedol.actflag='I'))
and primaryex<>'No'
 ORDER BY CaRef
end
else
SELECT top 0 * FROM v54f_620_Bonus_Rights

