--filepath=o:\Datafeed\Equity\620i_prev\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620i_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620i\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog where seq = 3
--sevent=n
--shownulls=n


--# 1
use wca2
SELECT * 
FROM tt620_Company_Meeting 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
use wca2
SELECT *
FROM tt620_Call
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
use wca2
SELECT * 
FROM tt620_Liquidation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 4
use wca2
SELECT *
FROM tt620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 5
use wca2
SELECT * 
FROM tt620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 6
use wca2
SELECT * 
FROM tt620_Conversion_Terms
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca2
SELECT * 
FROM tt620_Redemption_Terms
ORDER BY CaRef

--# 8
use wca2
SELECT * 
FROM tt620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 9
use wca2
SELECT * 
FROM tt620_Lot_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM tt620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 11
use wca2
SELECT * 
FROM tt620_Buy_Back
ORDER BY CaRef

--# 12
use wca2
SELECT * 
FROM tt620_Capital_Reduction
ORDER BY CaRef

--# 13
use wca2
SELECT * 
FROM tt620_Takeover
ORDER BY CaRef

--# 14
use wca2
SELECT * 
FROM tt620_Arrangement
ORDER BY CaRef

--# 15
use wca2
SELECT * 
FROM tt620_Bonus
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 17
use wca2
SELECT * 
FROM tt620_Consolidation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 18
use wca2
SELECT * 
FROM tt620_Demerger
ORDER BY CaRef

--# 19
use wca2
SELECT * 
FROM tt620_Distribution
ORDER BY CaRef

--# 20
use wca2
SELECT * 
FROM tt620_Divestment
ORDER BY CaRef

--# 21
use wca2
SELECT * 
FROM tt620_Entitlement
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 22
use wca2
SELECT * 
FROM tt620_Merger
ORDER BY CaRef

--# 23
use wca2
SELECT * 
FROM tt620_Preferential_Offer
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 24
use wca2
SELECT * 
FROM tt620_Purchase_Offer
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 25
use wca2
SELECT * 
FROM tt620_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 26
use wca2
SELECT * 
FROM tt620_Security_Swap 
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM tt620_Subdivision
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 28
use wca2
SELECT *
FROM tt620_Bankruptcy 
ORDER BY CaRef

--# 29
use wca2
SELECT *
FROM tt620_Financial_Year_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca2
SELECT *
FROM tt620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM tt620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca2
SELECT *
FROM tt620_Class_Action
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM tt620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 34
use wca2
SELECT *
FROM tt620_Assimilation
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM tt620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM tt620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM tt620_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca2
SELECT * 
FROM tt620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca2
SELECT * 
FROM tt620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 40
use wca2
SELECT * 
FROM tt620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 41
use wca2
SELECT * 
FROM tt620_Return_of_Capital 
ORDER BY CaRef


--# 42
use wca2
SELECT * 
FROM tt620_Dividend
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 43
use wca2
SELECT * 
FROM tt620_Dividend_Reinvestment_Plan
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 44
use wca2
SELECT * 
FROM tt620_Franking
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 45
use wca2
SELECT * 
FROM tt620_Conversion_Terms_Change
WHERE
(SecStatus = 'X') 
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM tt620_Bonus_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
