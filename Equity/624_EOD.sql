--filepath=o:\Datafeed\Equity\624\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.624
--suffix=
--fileheadertext=EDI_STATIC_624_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\624\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT 
scexh.ScexhID,
CASE WHEN scexh.Acttime>=(select max(acttime) from tbl_Opslog where seq = 1) THEN scexh.Acttime
WHEN scmst.Acttime>=(select max(acttime) from tbl_Opslog where seq = 1) THEN scmst.Acttime
WHEN issur.Acttime>=(select max(acttime) from tbl_Opslog where seq = 1) THEN issur.Acttime
ELSE exchg.acttime END as Changed,
scexh.AnnounceDate as Created,
scexh.Actflag,
scmst.IssID,
scmst.SecID,
issur.CntryofIncorp,
issur.Issuername,
scmst.Securitydesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.Isin,
scmst.Uscode,
case when scmst.Statusflag is not null then scmst.Statusflag else 'A' end as SecStatus,
scmst.PrimaryExchgCD,
scmst.SectyCD,
substring(scexh.Exchgcd,1,2) as ExCountry,
scexh.Exchgcd,
exchg.Mic,
scexh.Localcode,
CASE WHEN (Scexh.ListStatus IS NULL) or (Scexh.ListStatus='') THEN 'L' ELSE Scexh.ListStatus END as ListStatus,
scexh.ListDate,
scexh.Lot
FROM scmst
INNER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN cntry ON issur.CntryofIncorp = cntry.cntryCD
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
where
(SCMST.PrimaryExchgCD = scexh.exchgcd
or SCMST.PrimaryExchgCD = ''
or SCMST.PrimaryExchgCD is null
or scexh.exchgcd = 'DEFSX')
and (scmst.sectycd = 'EQS'
or scmst.sectycd = 'PRF'
or scmst.sectycd = 'DR')
and (scmst.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or exchg.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or scexh.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or issur.acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and exchg.actflag<>'D'
