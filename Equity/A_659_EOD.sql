--filepath=o:\Datafeed\Equity\659\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.659
--suffix=
--fileheadertext=EDI_REORG_659_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\659\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 42
use wca2
SELECT * 
FROM t620_Dividend
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
