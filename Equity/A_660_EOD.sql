--filepath=o:\Datafeed\Equity\660\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.660
--suffix=
--fileheadertext=EDI_REORG_660_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\660\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 0
use wca
select
'Levent' as f1,
'EventID' as f2,
'SecID' as f3,
'IssID' as f4,
'Created' as f5,
'Changed' as f6,
'Actflag' as f7,
'PrimaryDate' as f12,
'EventDate1' as f11,
'SecondaryDate' as f12,
'EventDate2' as f13,
'TertiaryDate' as f14,
'EventDate3' as f15,
'RatioNew' as f16,
'RatioOld' as f17,
'Fractions' as f18,
'ResSecID' as f19,
'ResIsin' as f20,
'Addfld1' as f21,
'AddInfo1' as f22,
'Addfld2' as f23,
'AddInfo2' as f24,
'Addfld3' as f25,
'AddInfo3' as f26


--# 1
use wca
select distinct
'Bankruptcy' as Levent,
BKRP.BkrpID as EventID,
'' as SecID,
BKRP.IssID,
BKRP.AnnounceDate as Created,
BKRP.Acttime as Changed,
BKRP.ActFlag,
'FilingDate' as PrimaryDate,
BKRP.FilingDate as EventDate1,
'NotificationDate' as SecondaryDate,
BKRP.NotificationDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BKRP
Where
BKRP.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 


--# 2
use wca
select distinct
'Lawsuit' as Levent,
LAWST.LAWSTID as EventID,
'' as SecID,
LAWST.IssID,
LAWST.AnnounceDate as Created,
LAWST.Acttime as Changed,
LAWST.ActFlag,
'EffectiveDate' as PrimaryDate,
LAWST.EffectiveDate as EventDate1,
'' as SecondaryDate,
'' as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM LAWST
Where
LAWST.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 



--# 3
use wca
select distinct
'Company Meeting' as Levent,
AGM.AGMID as EventID,
'' as SecID,
AGM.IssID,
AGM.AnnounceDate as Created,
AGM.Acttime as Changed,
AGM.ActFlag,
'AGMDate' as PrimaryDate,
AGM.AGMDate as EventDate1,
'' as SecondaryDate,
'' as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM AGM
Where
AGM.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 



--# 4
use wca
select distinct
'Call' as Levent,
CALL.CallID as EventID,
SCMST.SecID,
SCMST.IssID,
CALL.AnnounceDate as Created,
CASE WHEN RD.Acttime > CALL.Acttime THEN RD.Acttime ELSE CALL.Acttime END as Changed,
CALL.ActFlag,
'DueDate' as PrimaryDate,
CALL.DueDate as EventDate1,
'RecDate' as SecondaryDate,
RD.RecDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CALL
INNER JOIN SCMST ON SCMST.SecID = CALL.SecID
LEFT OUTER JOIN RD ON CALL.RdID = RD.RdID
Where
(CALL.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 5
use wca
select distinct
'Capital Reduction' as Levent,
CAPRD.CaprdID as EventID,
SCMST.SecID,
SCMST.IssID,
CAPRD.AnnounceDate as Created,
CASE WHEN RD.Acttime > CAPRD.Acttime THEN RD.Acttime ELSE CAPRD.Acttime END as Changed,
CAPRD.ActFlag,
'PayDate' as PrimaryDate,
CAPRD.PayDate as EventDate1,
'EffectiveDate' as SecondaryDate,
CAPRD.EffectiveDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
CAPRD.NewRatio as RatioNew,
CAPRD.OldRatio as RatioOld,
CAPRD.Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CAPRD
INNER JOIN SCMST ON SCMST.SecID = CAPRD.SecID
LEFT OUTER JOIN RD ON CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON CaprdID = ICC.RelEventID AND 'CAPRD' = ICC.EventType
Where
(CAPRD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3))


--# 6
use wca
select distinct
'Arrangement' as Levent,
ARR.RdID as EventID,
SCMST.SecID,
SCMST.IssID,
ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > ARR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > ARR.Acttime) THEN PEXDT.Acttime ELSE ARR.Acttime END as Changed,
ARR.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM ARR
INNER JOIN RD ON RD.RdID = ARR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON ARR.RdID = ICC.RelEventID AND 'ARR' = ICC.EventType
Where
(ARR.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 7
use wca
select distinct
'Bonus' as Levent,
BON.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > BON.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > BON.Acttime) THEN PEXDT.Acttime ELSE BON.Acttime END as Changed,
BON.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BON
INNER JOIN RD ON RD.RdID = BON.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
Where
(BON.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 8
use wca
select distinct
'Bonus Rights' as Levent,
BR.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
BR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > BR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > BR.Acttime) THEN PEXDT.Acttime ELSE BR.Acttime END as Changed,
BR.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BR
INNER JOIN RD ON RD.RdID = BR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
Where
(BR.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 9
use wca
select distinct
'Consolidation' as Levent,
CONSD.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONSD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > CONSD.Acttime) THEN PEXDT.Acttime ELSE CONSD.Acttime END as Changed,
CONSD.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CONSD
INNER JOIN RD ON RD.RdID = CONSD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON CONSD.RdId = ICC.RelEventID AND 'CONSD' = ICC.EventType
Where
(CONSD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )



--# 12
use wca
select distinct
'Divestment' as Levent,
DVST.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DVST.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DVST.Acttime) THEN PEXDT.Acttime ELSE DVST.Acttime END as Changed,
DVST.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DVST
INNER JOIN RD ON RD.RdID = DVST.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
Where
(DVST.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 16
use wca
select distinct
'Rights' as Levent,
RTS.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
RTS.AnnounceDate as Created,
RD.Acttime Changed,
RTS.ActFlag,
'EndTrade' as PrimaryDate,
RTS.EndTrade as EventDate1,
'StartTrade' as SecondaryDate,
RTS.StartTrade as EventDate2,
'SplitDate' as TertiaryDate,
RTS.SplitDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM RTS
INNER JOIN RD ON RD.RdID = RTS.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
Where
RTS.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 


--# 13
use wca
select distinct
'Entitlement' as Levent,
ENT.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > ENT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > ENT.Acttime) THEN PEXDT.Acttime ELSE ENT.Acttime END as Changed,
ENT.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM ENT
INNER JOIN RD ON RD.RdID = ENT.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
Where
(ENT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 14
use wca
select distinct
'Sub-Division' as Levent,
SD.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > SD.Acttime) THEN PEXDT.Acttime ELSE SD.Acttime END as Changed,
SD.ActFlag,
'NewCodeDate' as PrimaryDate,
ICC.Acttime as EventDate1,
'PayDate' as SecondaryDate,
PEXDT.PayDate as EventDate2,
'RecDate' as TertiaryDate,
PEXDT.ExDate as EventDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM SD
INNER JOIN RD ON RD.RdID = SD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON SD.RdId = ICC.RelEventID AND 'SD' = ICC.EventType
Where
(SD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or ICC.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )



--# 17
use wca
select distinct
'Preferential Offer' as Levent,
PRF.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > PRF.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > PRF.Acttime) THEN PEXDT.Acttime ELSE PRF.Acttime END as Changed,
PRF.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM PRF
INNER JOIN RD ON RD.RdID = PRF.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
Where
(PRF.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )



--# 18
use wca
select distinct
'Assimilation' as Levent,
ASSM.AssmId as EventID,
SCMST.SecID,
SCMST.IssID,
ASSM.AnnounceDate as Created,
ASSM.Acttime as Changed,
ASSM.ActFlag,
'AssimilationDate' as PrimaryDate,
AssimilationDate as EventDate1,
'' as SecondaryDate,
'' as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM ASSM
INNER JOIN SCMST ON ASSM.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
Where
ASSM.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
and ASSM.Actflag <> 'D' 


--# 21
use wca
select distinct
'Preference Redemption' as Levent,
REDEM.RedemID as EventID,
SCMST.SecID,
SCMST.IssID,
REDEM.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > REDEM.Acttime) THEN RD.Acttime ELSE REDEM.Acttime END as Changed,
REDEM.ActFlag,
'RedemDate' as PrimaryDate,
REDEM.RedemDate as EventDate1,
'RecDate' as SecondaryDate,
RD.RecDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM REDEM
LEFT OUTER JOIN RD ON REDEM.RdID = RD.RdID
INNER JOIN SCMST ON REDEM.SecID = SCMST.SecID
Where
(REDEM.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3))
and scmst.sectycd<>'BND'


--# 22
use wca
select distinct
'Preference Conversion' as Levent,
CONV.CONVID as EventID,
SCMST.SecID,
SCMST.IssID,
CONV.AnnounceDate as Created,
CONV.Acttime as Changed,
CONV.ActFlag,
'FromDate' as PrimaryDate,
CONV.FromDate as EventDate1,
'ToDate' as SecondaryDate,
CONV.ToDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
CONV.RatioNew as RatioNew,
CONV.RatioOld as RatioOld,
CONV.Fractions as Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CONV
INNER JOIN SCMST ON CONV.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
Where
CONV.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3)
and scmst.sectycd<>'BND'


--# 23
use wca
select distinct
'Security Swap' as Levent,
SCSWP.RdID as EventID,
SCMST.SecID,
SCMST.IssID,
SCSWP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SCSWP.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > SCSWP.Acttime) THEN PEXDT.Acttime ELSE SCSWP.Acttime END as Changed,
SCSWP.ActFlag,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'RecDate' as TertiaryDate,
RD.RecDate as EventDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM SCSWP
INNER JOIN RD ON RD.RdID = SCSWP.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SCSWP' = PEXDT.EventType
Where
(SCSWP.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 24
use wca
select distinct
'Security Reclassification' as Levent,
SECRC.SecrcID as EventID,
SCMST.SecID,
SCMST.IssID,
SECRC.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SECRC.Acttime) THEN RD.Acttime ELSE SECRC.Acttime END as Changed,
SECRC.ActFlag,
'EffectiveDate' as PrimaryDate,
EffectiveDate as EventDate1,
'RecDate' as SecondaryDate,
RecDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
RatioNew,
RatioOld,
'' as Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM SECRC
INNER JOIN RD ON RD.RdID = SECRC.SecRcID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
Where
(SECRC.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3))


--# 25
use wca
select distinct
'DRIP' as Levent,
DRIP.DivID as EventID,
SCMST.SecID,
SCMST.IssID,
DRIP.AnnounceDate as Created,
DRIP.Acttime as Changed,
DRIP.ActFlag,
'DripPayDate' as PrimaryDate,
DRIP.DripPayDate as EventDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EventDate2,
'LastDate' as TertiaryDate,
DRIP.DripLastDate as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DRIP
LEFT OUTER JOIN DIV ON DRIP.DivID = DIV.DivID
INNER JOIN RD ON DIV.RdID = RD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
Where
(DRIP.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3) )


--# 26
use wca
select distinct
'Return of Capital' as Levent,
RCAP.RCAPID as EventID,
SCMST.SecID,
SCMST.IssID,
RCAP.AnnounceDate as Created,
RCAP.Acttime as Changed,
RCAP.ActFlag,
'PayDate' as PrimaryDate,
RCAP.CSPYDate as EventDate1,
'EffectiveDate' as SecondaryDate,
RCAP.EffectiveDate as EventDate2,
'' as TertiaryDate,
'' as EventDate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM RCAP
INNER JOIN SCMST ON SCMST.SecID = RCAP.SecID
Where
RCAP.Acttime between (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3) and (select max(acttime)+0.05 from wca.dbo.tbl_Opslog where seq = 3)
