--filepath=o:\Datafeed\Equity\620_br\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_MRG_BR\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefooterchk=y


--# 1
use wca
SELECT *  
FROM v54f_620_Dividend
WHERE
changed >= (select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 2)
and ExCountry = 'BR'
ORDER BY CaRef
