--filepath=o:\Datafeed\Equity\620_CarryQuote\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_CarryQuote\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 5
use wca2
SELECT *
FROM t620_International_Code_Change
ORDER BY eventid desc

--# 31
use wca2
SELECT *
FROM t620_Issuer_Name_change
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620_Security_Description_Change
ORDER BY CaRef

--# 35
use wca2
SELECT * FROM t620_Listing_Status_Change
ORDER BY eventid desc


--# 36
use wca2
SELECT *
FROM t620_Local_Code_Change
ORDER BY CaRef


--# 37
use wca2
SELECT *
FROM t620_New_Listing
ORDER BY eventid desc
