--filepath=o:\Datafeed\Equity\647\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--fileextension=.645
--suffix=
--fileheadertext=EDI_WARRANT_647_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\647\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
WHERE issur.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)

--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM scmst
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE scmst.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
and sectygrp.secgrpid<4
and scmst.sectycd<>'BND'


--# 3
use WCA
SELECT
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE sedol.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
and scmst.sectycd <> 'BND'
and sectygrp.secgrpid<4

--# 4
use WCA
SELECT
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
inner join scmst on scexh.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE scexh.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
and scmst.sectycd <> 'BND'
and sectygrp.secgrpid<4


--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
WHERE acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
