--filepath=o:\Datafeed\Equity\621\
--filenameprefix=
--filename=2008_paydate
--filenamealt=
--fileextension=.621
--suffix=
--fileheadertext=EDI_DIV_paydate_621_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT *  
FROM v54f_621_Dividend
WHERE
paydate >='2007/04/06' and paydate<'2008/04/06'
ORDER BY EventID desc
