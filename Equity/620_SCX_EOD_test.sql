--filepath=o:\Datafeed\Equity\620\SCX\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620\SCX\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 
use wca
select * into wca2.dbo.tSCX_Dividend FROM v54f_SCX_Dividend 
where changed>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 1
usw wca
select * FROM wca2.dbo.tSCX_Dividend
ORDER BY CaRef
