--filepath=o:\Datafeed\Equity\indsect\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_SEC
--fileheadertext=EDI_INDSECT_SEC_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
scmst.Actflag,
scmst.IssID,
scmst.isin
FROM scmst
inner join issur on scmst.issid = issur.issid
where
issur.actflag<>'D'
and scmst.actflag<>'D'
and scmst.isin<>''
and issur.indusid<>0
and issur.indusid is not null
