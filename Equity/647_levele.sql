--filepath=o:\Upload\Acc\200\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.647
--suffix=
--fileheadertext=EDI_REORG_647_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Upload\Acc\200\feed\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 0
use wca
select
'Levent' as f1,
'EventID' as f2,
'SecID' as f3,
'OptionID' as f1,
'SerialID' as f1,
'IssID' as f4,
'Created' as f5,
'Changed' as f6,
'Actflag' as f7,
'PrimaryExchgCD' as f8,
'Isin' as f9,
'Localcode' as f9,
'DateType1' as f10,
'Date1' as f11,
'DateType2' as f12,
'Date2' as f13,
'DateType3' as f14,
'Date3' as f15,
'RatioNew' as f16,
'RatioOld' as f17,
'Fractions' as f18,
'ResSecID' as f19,
'ResIsin' as f20,
'Addfld1' as f21,
'AddInfo1' as f22,
'Addfld2' as f23,
'AddInfo2' as f24,
'Addfld3' as f25,
'AddInfo3' as f26,
'Addfld4' as f23,
'AddInfo4' as f24,
'Addfld5' as f25,
'AddInfo5' as f26,
'Addfld6' as f25,
'AddInfo6' as f26,
'Addfld7' as f23,
'AddInfo7' as f24,
'Addfld8' as f25,
'AddInfo8' as f26




--# 1
use wca
select distinct
'Local Code Change' as ChangeType,
LCC.LCCID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
LCC.AnnounceDate as Created,
LCC.Acttime as Changed,
LCC.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'EffectiveDate' as DateType1,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as Date1,
'' as DateType2,
'' as Date2,
'' as DateType3,
'' as Date3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'OldLocalcode' as Addfld1,
lcc.OldLocalcode as AddInfo1,
'NewLocalcode' as Addfld2,
lcc. NewLocalcode as AddInfo2,
'RelatedEvent' as Addfld3,
LCC. EventType as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM LCC
INNER JOIN SCMST ON LCC.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
lcc.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and eventtype<>'CLEAN'
and LCC.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)



--# 1
use wca
select distinct
'Bankruptcy' as Levent,
BKRP.BkrpID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
BKRP.AnnounceDate as Created,
BKRP.Acttime as Changed,
BKRP.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'FilingDate' as DateType1,
BKRP.FilingDate as Date1,
'NotificationDate' as DateType2,
BKRP.NotificationDate as Date2,
'' as DateType3,
'' as Date3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM BKRP
INNER JOIN SCMST ON SCMST.IssID = BKRP.IssID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and BKRP.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)



--# 1
use wca
select distinct
'Legal Action' as Levent,
LAWST.LAWSTID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
LAWST.AnnounceDate as Created,
LAWST.Acttime as Changed,
LAWST.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'EffectiveDate' as DateType1,
LAWST.EffectiveDate as Date1,
'RegistrationDate' as DateType2,
LAWST.Regdate as Date2,
'' as DateType3,
'' as Date3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'LaType' as Addfld1,
LAWST.LAtype as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM LAWST
INNER JOIN SCMST ON SCMST.IssID = LAWST.IssID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and LAWST.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 2
use wca
select distinct
'Dividend' as Levent,
DIV.DivID as EventID,
SCMST.SecID,
DIVPY.OptionID,
'1' as SerialID,
SCMST.IssID,
DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DIV.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DIV.Acttime) THEN PEXDT.Acttime ELSE DIV.Acttime END as Changed,
case when div.actflag= 'C' or div.actflag='D' then div.actflag else DIVPY.ActFlag end as Actflag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'PayDate' as DateType1,
case when PEXDT.PayDate2 is not null then PEXDT.PayDate2 else PEXDT.PayDate end as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
DIVPY.RatioNew,
DIVPY.RatioOld,
DIVPY.Fractions,
DIVPY.ResSecID,
SCMSTRES.Isin as ResIsin,
'Divtype' as Addfld1,
DIVPY.Divtype as AddInfo1,
'CurenCD' as Addfld2,
DIVPY.CurenCD as AddInfo2,
'GrossDividend' as Addfld3,
DIVPY.GrossDividend as AddInfo3,
'NetDividend' as Addfld4,
DIVPY.NetDividend as AddInfo4,
'Driplastdate' as Addfld5,
DRIP.Driplastdate as AddInfo5,
'DripReinvPrice' as Addfld6,
DRIP.DripReinvPrice as AddInfo6,
'Depfees' as Addfld7,
DIVPY.Depfees as AddInfo7,
'USDratetoCurrency' as Addfld8,
DIVPY.USDratetoCurrency as AddInfo8,
'NilDividend' as Addfld9,
DIV.NilDividend as AddInfo9,
'Tbaflag' as Addfld10,
DIV.Tbaflag as AddInfo10,
'RescindCashDiv' as Addfld11,
DIVPY.RecindCashDiv as AddInfo11,
'RescindStockDiv' as Addfld12,
DIVPY.RecindStockDiv as AddInfo12,
'DefaultOpt' as Addfld13,
DIVPY.DefaultOpt as AddInfo13
FROM DIV
INNER JOIN RD ON RD.RdID = DIV.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON DIV.DivID = DIVPY.DivID
LEFT OUTER JOIN SCMST as SCMSTRES ON DIVPY.ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID
     AND 'GBLSE' = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
left outer join drip on div.divid = drip.divid
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (DIVPY.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or DIV.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
order by eventid, optionid



--# 5
use wca
select distinct
'Capital Reduction' as Levent,
CAPRD.CaprdID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
CAPRD.AnnounceDate as Created,
CASE WHEN RD.Acttime > CAPRD.Acttime THEN RD.Acttime ELSE CAPRD.Acttime END as Changed,
CAPRD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
SCEXH.Localcode,
'PayDate' as DateType1,
CAPRD.PayDate as Date1,
'EffectiveDate' as DateType2,
CAPRD.EffectiveDate as Endate2,
'RecDate' as DateType3,
RD.RecDate as Endate3,
CAPRD.NewRatio as RatioNew,
CAPRD.OldRatio as RatioOld,
CAPRD.Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM CAPRD
INNER JOIN SCMST ON SCMST.SecID = CAPRD.SecID
LEFT OUTER JOIN RD ON CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON CaprdID = ICC.RelEventID AND 'CAPRD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (CAPRD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or ICC.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))



--# 7
use wca
select distinct
'Bonus' as Levent,
BON.RdId as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > BON.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > BON.Acttime) THEN PEXDT.Acttime ELSE BON.Acttime END as Changed,
BON.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'PayDate' as DateType1,
PEXDT.PayDate as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM BON
INNER JOIN RD ON RD.RdID = BON.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (BON.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))




--# 9
use wca
select distinct
'Consolidation' as Levent,
CONSD.RdId as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONSD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > CONSD.Acttime) THEN PEXDT.Acttime ELSE CONSD.Acttime END as Changed,
CONSD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
SCEXH.Localcode,
'PayDate' as DateType1,
PEXDT.PayDate as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM CONSD
INNER JOIN RD ON RD.RdID = CONSD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON CONSD.RdId = ICC.RelEventID AND 'CONSD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (CONSD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or ICC.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))


--# 10
use wca
select distinct
'Demerger' as Levent,
DMRGR.RdID as EventID,
SCMST.SecID,
MPAY.OptionID,
MPAY.SerialID,
SCMST.IssID,
DMRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DMRGR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DMRGR.Acttime) THEN PEXDT.Acttime ELSE DMRGR.Acttime END as Changed,
DMRGR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'PayDate' as DateType1,
PEXDT.PayDate as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
MPAY.RatioNew,
MPAY.RatioOld,
MPAY.Fractions,
MPAY.ResSecID as ResSecID,
resscmst.isin as ResIsin,
'DefaultOpt' as Addfld1,
MPAY.DefaultOpt as AddInfo1,
'Paytype' as Addfld2,
MPAY.Paytype as AddInfo2,
'CurenCD' as Addfld3,
MPAY.CurenCD as AddInfo3,
'MaxPrice' as Addfld4,
MPAY.MaxPrice as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM DMRGR
INNER JOIN RD ON RD.RdID = DMRGR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN MPAY ON RD.RdID = MPAY.EventID AND 'DMRGR' = MPAY.SEvent
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
LEFT OUTER JOIN SCMST as resscmst ON MPAY.ResSecID = resscmst.SecID
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (DMRGR.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or MPAY.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))



--# 12
use wca
select distinct
'Divestment' as Levent,
DVST.RdId as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DVST.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DVST.Acttime) THEN PEXDT.Acttime ELSE DVST.Acttime END as Changed,
DVST.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'PayDate' as DateType1,
PEXDT.PayDate as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM DVST
INNER JOIN RD ON RD.RdID = DVST.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (DVST.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))



--# 15
use wca
select distinct
'Merger' as Levent,
MRGR.RdId as EventID,
SCMST.SecID,
MPAY.OptionID,
MPAY.SerialID,
SCMST.IssID,
MRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > MRGR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > MRGR.Acttime) THEN PEXDT.Acttime ELSE MRGR.Acttime END as Changed,
MRGR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'AppointedDate' as PrimaryDate,
MRGR.AppointedDate as Enddate1,
'EffectiveDate' as SecondaryDate,
MRGR.EffectiveDate as EndDate2,
'PayDate' as TertiaryDate,
PEXDT.PayDate as Enddate3,
MPAY.RatioNew,
MPAY.RatioOld,
MPAY.Fractions,
MPAY.ResSecID as ResSecID,
resscmst.isin as ResIsin,
'DefaultOpt' as Addfld1,
MPAY.DefaultOpt as AddInfo1,
'Paytype' as Addfld2,
MPAY.Paytype as AddInfo2,
'CurenCD' as Addfld3,
MPAY.CurenCD as AddInfo3,
'MaxPrice' as Addfld4,
MPAY.MaxPrice as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM MRGR
INNER JOIN RD ON RD.RdID = MRGR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN MPAY ON RD.RdID = MPAY.EventID AND 'MRGR' = MPAY.SEvent
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN SCMST as resscmst ON MPAY.ResSecID = resscmst.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (MRGR.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))


--# 16
use wca
select distinct
'Rights' as Levent,
RTS.RdId as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
RTS.AnnounceDate as Created,
RD.Acttime Changed,
RTS.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'EndTrade' as DateType1,
RTS.EndTrade as Date1,
'StartTrade' as DateType2,
RTS.StartTrade as Date2,
'SplitDate' as DateType3,
RTS.SplitDate as Date3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'IssuePrice' as Addfld1,
RTS.IssuePrice as AddInfo1,
'SplitDate' as Addfld2,
RTS.SplitDate as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM RTS
INNER JOIN RD ON RD.RdID = RTS.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (RTS.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))


--# 13
use wca
select distinct
'Entitlement' as Levent,
ENT.RdId as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > ENT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > ENT.Acttime) THEN PEXDT.Acttime ELSE ENT.Acttime END as Changed,
ENT.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'PayDate' as DateType1,
PEXDT.PayDate as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'IssuePrice' as Addfld1,
ENT.EntIssuePrice as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM ENT
INNER JOIN RD ON RD.RdID = ENT.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (ENT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))


--# 14
use wca
select distinct
'Sub-Division' as Levent,
SD.RdId as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > SD.Acttime) THEN PEXDT.Acttime ELSE SD.Acttime END as Changed,
SD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
SCEXH.Localcode,
'NewCodeDate' as DateType1,
ICC.Acttime as Date1,
'PayDate' as DateType2,
PEXDT.PayDate as Date2,
'RecDate' as DateType3,
PEXDT.ExDate as Date3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM SD
INNER JOIN RD ON RD.RdID = SD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON SD.RdId = ICC.RelEventID AND 'SD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (SD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or ICC.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))



--# 17
use wca
select distinct
'Preferential Offer' as Levent,
PRF.RdId as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > PRF.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > PRF.Acttime) THEN PEXDT.Acttime ELSE PRF.Acttime END as Changed,
PRF.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'PayDate' as DateType1,
PEXDT.PayDate as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM PRF
INNER JOIN RD ON RD.RdID = PRF.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (PRF.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))



--# 22
use wca
select distinct
'Takeover' as Levent,
TKOVR.TKOVRID as EventID,
SCMST.SecID,
MPAY.OptionID,
MPAY.SerialID,
SCMST.IssID,
TKOVR.AnnounceDate as Created,
TKOVR.Acttime Changed,
case when MPAY.actflag= 'C' or MPAY.actflag='D' then MPAY.actflag else TKOVR.ActFlag end as Actflag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'CloseDate' as DateType1,
CloseDate as Date1,
'CmAcqDate' as DateType2,
CmAcqDate as Date2,
'PayDate' as DateType3,
MPAY.Paydate as Date3,
MPAY.RatioNew,
MPAY.RatioOld,
MPAY.Fractions,
MPAY.ResSecID as ResSecID,
resscmst.isin as ResIsin,
'DefaultOpt' as Addfld1,
MPAY.DefaultOpt as AddInfo1,
'Paytype' as Addfld2,
MPAY.Paytype as AddInfo2,
'CurenCD' as Addfld3,
MPAY.CurenCD as AddInfo3,
'MaxPrice' as Addfld4,
MPAY.MaxPrice as AddInfo4,
'MinPrice' as Addfld5,
MPAY.MinPrice as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM TKOVR
INNER JOIN SCMST ON TKOVR.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
LEFT OUTER JOIN MPAY ON TKOVR.TkovrID = MPAY.EventID AND 'TKOVR' = MPAY.SEvent
LEFT OUTER JOIN SCMST as resscmst ON MPAY.ResSecID = resscmst.SecID
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (TKOVR.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))



--# 23
use wca
select distinct
'Security Swap' as Levent,
SCSWP.RdID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
SCSWP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SCSWP.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > SCSWP.Acttime) THEN PEXDT.Acttime ELSE SCSWP.Acttime END as Changed,
SCSWP.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'PayDate' as DateType1,
PEXDT.PayDate as Date1,
'ExDate' as DateType2,
PEXDT.ExDate as Date2,
'RecDate' as DateType3,
RD.RecDate as Date3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM SCSWP
INNER JOIN RD ON RD.RdID = SCSWP.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SCSWP' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (SCSWP.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or PEXDT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))


--# 24
use wca
select distinct
'Security Reclassification' as Levent,
SECRC.SecrcID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
SECRC.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SECRC.Acttime) THEN RD.Acttime ELSE SECRC.Acttime END as Changed,
SECRC.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'EffectiveDate' as DateType1,
EffectiveDate as Date1,
'RecDate' as DateType2,
RecDate as Date2,
'' as DateType3,
'' as Date3,
RatioNew,
RatioOld,
'' as Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM SECRC
INNER JOIN RD ON RD.RdID = SECRC.SecRcID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and (SECRC.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
     or RD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))


--# 25
use wca
select distinct
'Currency Redenomination' as ChangeType,
CURRD.CURRDID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
CURRD.AnnounceDate as Created,
CURRD.Acttime as Changed,
CURRD.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'EffectiveDate' as DateType1,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as Date1,
'' as DateType2,
'' as Date2,
'' as DateType3,
'' as Date3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'OldCurenCD' as Addfld1,
CURRD.OldCurenCD as AddInfo1,
'NewCurenCD' as Addfld2,
CURRD. NewCurenCD as AddInfo2,
'OldParvalue' as Addfld3,
CURRD.OldParvalue as AddInfo3,
'NewParvalue' as Addfld4,
CURRD. NewParvalue as AddInfo4,
'RelatedEvent' as Addfld5,
CURRD. EventType as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM CURRD
INNER JOIN SCMST ON CURRD.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and CURRD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 26
use wca
select distinct
'Parvalue Redenomination' as ChangeType,
PVRD.PVRDID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
PVRD.AnnounceDate as Created,
PVRD.Acttime as Changed,
PVRD.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'EffectiveDate' as DateType1,
case when PVRD.EffectiveDate is null
     then PVRD.announcedate
     else PVRD.effectivedate
     end as Date1,
'' as DateType2,
'' as Date2,
'' as DateType3,
'' as Date3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'CurenCD' as Addfld1,
PVRD.CurenCD as AddInfo1,
'OldParvalue' as Addfld2,
PVRD.OldParvalue as AddInfo2,
'NewParvalue' as Addfld3,
PVRD. NewParvalue as AddInfo3,
'RelatedEvent' as Addfld4,
PVRD. EventType as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM PVRD
INNER JOIN SCMST ON PVRD.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and PVRD.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 27
use wca
select distinct
'Listing Status Change' as ChangeType,
LSTAT.LSTATID as EventID,
SCMST.SecID,
'1' as OptionID,
'1' as SerialID,
SCMST.IssID,
LSTAT.AnnounceDate as Created,
LSTAT.Acttime as Changed,
LSTAT.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
SCEXH.Localcode,
'EffectiveDate' as DateType1,
case when LSTAT.EffectiveDate is null
     then LSTAT.announcedate
     else LSTAT.effectivedate
     end as Date1,
'NotificationDate' as DateType1,
LSTAT.NotificationDate as Date2,
'' as DateType3,
'' as Date3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'NewListingStatus' as Addfld1,
LSTAT.LstatStatus as AddInfo1,
'RelatedEvent' as Addfld2,
LSTAT.Eventtype as AddInfo2,
'' as Addfld3,
'' as AddInfo3,
'' as Addfld4,
'' as AddInfo4,
'' as Addfld5,
'' as AddInfo5,
'' as Addfld6,
'' as AddInfo6,
'' as Addfld7,
'' as AddInfo7,
'' as Addfld8,
'' as AddInfo8
FROM LSTAT
INNER JOIN SCMST ON LSTAT.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
          and lstat.exchgcd = wca.dbo.scexh.exchgcd
Where
scexh.exchgcd = 'GBLSE'
and scmst.isin in (select code from portfolio.dbo.fisin where accid = 200 and actflag<>'D')
and LSTAT.acttime>= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

