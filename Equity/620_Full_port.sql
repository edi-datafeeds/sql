--filepath=o:\Datafeed\Equity\620_port\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_STATIC_620_Port_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT *  
FROM v50f_620_Company_Meeting 
where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
SELECT * 
FROM v53f_620_Call
where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
SELECT *  
FROM v50f_620_Liquidation
where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA
SELECT * 
FROM v51f_620_Certificate_Exchange
where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
SELECT *  
FROM v51f_620_International_Code_Change

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
SELECT *  
FROM v51f_620_Conversion_Terms

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
SELECT *  
FROM v51f_620_Redemption_Terms

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
SELECT *  
FROM v51f_620_Security_Reclassification

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
SELECT *  
FROM v52f_620_Lot_Change

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
SELECT *  
FROM v52f_620_Sedol_Change

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca
SELECT *  
FROM v53f_620_Buy_Back

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
SELECT *  
FROM v53f_620_Capital_Reduction

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
use wca
SELECT *  
FROM v53f_620_Takeover

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
use wca
SELECT *  
FROM v54f_620_Arrangement

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
SELECT *  
FROM v54f_620_Bonus

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
SELECT *  
FROM v54f_620_Bonus_Rights

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
SELECT *  
FROM v54f_620_Consolidation

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca
SELECT *  
FROM v54f_620_Demerger

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
SELECT *  
FROM v54f_620_Distribution

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
use wca
SELECT *  
FROM v54f_620_Divestment

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
SELECT *  
FROM v54f_620_Entitlement

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
use wca
SELECT *  
FROM v54f_620_Merger

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
use wca
SELECT *  
FROM v54f_620_Preferential_Offer

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
SELECT *  
FROM v54f_620_Purchase_Offer

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
SELECT *  
FROM v54f_620_Rights 

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
SELECT *  
FROM v54f_620_Security_Swap 

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
SELECT * 
FROM v54f_620_Subdivision

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
SELECT * 
FROM v50f_620_Bankruptcy 

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
SELECT * 
FROM v50f_620_Financial_Year_Change

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
SELECT * 
FROM v50f_620_Incorporation_Change

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
SELECT * 
FROM v50f_620_Issuer_Name_change

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
SELECT * 
FROM v50f_620_Class_Action

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
SELECT * 
FROM v51f_620_Security_Description_Change

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
SELECT * 
FROM v52f_620_Assimilation

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
SELECT * 
FROM v52f_620_Local_Code_Change

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
SELECT *  
FROM v52f_620_New_Listing

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
SELECT *  
FROM v50f_620_Announcement

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
use wca
SELECT *  
FROM v51f_620_Parvalue_Redenomination 

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
SELECT *  
FROM v51f_620_Currency_Redenomination 

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
SELECT *  
FROM v53f_620_Return_of_Capital 

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
SELECT *  
FROM v54f_620_Dividend

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
USE WCA
SELECT *  
FROM v54f_620_Dividend_Reinvestment_Plan

where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
USE WCA
SELECT *  
FROM v54f_620_Franking

where secid in (select secid from portfolio.dbo.MKREP)

and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol

--# 46
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
where secid in (select secid from portfolio.dbo.MKREP)
and created>'2002/01/01'
ORDER BY EventID desc, ExchgCD, Sedol
