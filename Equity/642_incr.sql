--filepath=o:\Datafeed\Equity\642\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(acttime) from wca.dbo.tbl_Opslog)
--fileextension=.642
--suffix=
--fileheadertext=EDI_REORG_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\642\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select
SDCHG.SdchgID as EventID,
'Sedol Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
sdchg.Actflag,
sdchg.eventtype as Reason,
sdchg.effectivedate as EffectiveDate,
sdchg.OldSedol as OldStatic,
sdchg.NewSedol as NewStatic
FROM SDCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = SDCHG.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
SDCHG.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)


--# 3
use wca
select
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
icc.effectivedate as EffectiveDate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
ICC.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (icc.OldIsin <> '' or icc.NewIsin <> '')

--# 4
use wca
select
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
inchg.Actflag,
inchg.eventtype as Reason,
inchg.inchgdate as EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic
FROM inchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = inchg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where
inchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)

--# 5
use wca
select
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
ischg.Actflag,
ischg.eventtype as Reason,
ischg.namechangedate as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic
FROM ischg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = ischg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
ischg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)

--# 6
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
lcc.Actflag,
lcc.eventtype as Reason,
lcc.effectivedate as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = lcc.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
lcc.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)

--# 7
use wca
select
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
scchg.Actflag,
scchg.eventtype as Reason,
scchg.dateofchange as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic
FROM scchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = scchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
scchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)

--# 8
use wca
select
LTCHG.LTCHGID as EventID,
'Lot Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
ltchg.Actflag,
'' as Reason,
ltchg.effectivedate as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = ltchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
ltchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (ltchg.OldLot <> ''
or ltchg.NewLot <> '')

--# 9
use wca
select
LTCHG.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
ltchg.Actflag,
'' as Reason,
ltchg.effectivedate as EffectiveDate,
ltchg.OldMinTrdQty as OldStatic,
ltchg.NewMinTrdgQty as NewStatic
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = ltchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where
ltchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (ltchg.OldMinTrdQty <> ''
or ltchg.NewMinTrdgQty <> '')

--# 10
use wca
select
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
lstat.Actflag,
lstat.eventtype as Reason,
lstat.effectivedate as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic
FROM lstat
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = lstat.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where
lstat.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)

--# 11
use wca
select
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
icc.EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
ICC.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (icc.OldUSCode <> ''
or icc.NewUSCode <> '')

--# 12
use wca
select
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
currd.Actflag,
currd.eventtype as Reason,
currd.EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic
FROM Currd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = currd.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
currd.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)

/*
use wca
select
SDCHG.SdchgID as EventID,
'Par Value Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
v20c_EV_SCMST.StructCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
pvrd.Actflag,
pvrd.eventtype as Reason,
pvrd.EffectiveDate,
pvrd.OldParValue as OldStatic,
pvrd.NewParValue as NewStatic
FROM pvrd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = pvrd.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
pvrd.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
*/