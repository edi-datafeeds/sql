--filepath=o:\Datafeed\Equity\636\
--filenameprefix=
--filename=yyyymmdd
--filenamesql=
--fileextension=.636
--suffix=
--fileheadersql=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\636\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefootertext=

--# 1
use wca
select
EventID,
Created,
Changed,
IssuerName,
SecurityDesc,
ISIN,
SectyCD,
Sedol,
Recdate,
ExDate,
PayDate,
OptionID,
Divtype,
CurenCD,
GrossDividend
NetDividend,
RatioNew,
RatioOld
from v54f_620_dividend
where
paydate>getdate()-40 and paydate<getdate()
and exchgcd = 'GBLSE'
and actflag<>'D'
and actflag<>'C'
order by isin

