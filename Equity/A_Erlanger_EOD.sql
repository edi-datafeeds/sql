--filepath=o:\Datafeed\Equity\Erlanger\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\Erlanger\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 17
use wca2
SELECT * 
FROM t620_Consolidation
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM t620_Subdivision
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM t620_Issuer_Name_change
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM t620_Listing_Status_Change
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM t620_Local_Code_Change
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_New_Listing
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
