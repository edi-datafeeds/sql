--filepath=o:\Datafeed\Equity\634_investt\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.634
--suffix=
--fileheadertext=EDI_STATIC_634_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\634_investt\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT 
scexh.ScexhID,
scexh.Acttime as Changed,
scexh.AnnounceDate as Created,
scexh.Actflag,
case when scmst.Actflag='D' then 'D' WHEN scexh.AnnounceDate>GETDATE()-5 THEN 'I' ELSE 'U' END as Actflag,
scmst.IssID,
scmst.SecID,
issur.CntryofIncorp,
issur.Issuername,
scmst.Securitydesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.Isin,
scmst.Uscode,
case when scmst.Statusflag is not null and scmst.Statusflag <> '' then scmst.Statusflag else 'A' end as SecStatus,
scmst.PrimaryExchgCD,
scmst.SectyCD,
substring(scexh.Exchgcd,1,2) as ExCountry,
case when sedol.actflag <> 'D' then Sedol.Sedol
else '' end as Sedol,
scexh.Exchgcd,
exchg.Mic,
scexh.Localcode,
CASE WHEN (Scexh.ListStatus IS NULL) or (Scexh.ListStatus='') THEN 'L' ELSE Scexh.ListStatus END as ListStatus,
scexh.ListDate,
scexh.Lot
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN cntry ON issur.CntryofIncorp = cntry.cntryCD
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                 AND exchg.cntryCD = Sedol.CntryCD
LEFT OUTER JOIN SECTYMAP ON Scmst.SectyCD = SECTYMAP.SectyCD
where issur.actflag <> 'D'
and scmst.actflag <> 'D'
and scexh.actflag <> 'D'
and sectymap.typegroup='Equity'
and (Exchg.CntryCD = 'DE' or Exchg.CntryCD = 'DK' or Exchg.CntryCD = 'FI' or Exchg.CntryCD = 'NL' or Exchg.CntryCD = 'SE' or Exchg.CntryCD = 'NO')
