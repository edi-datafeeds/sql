--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
--fileextension=.642
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\642\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic
FROM ICC
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.SecID = v20c_620_dSCEXH.SecID
where
--ICC.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
--and 
(icc.OldIsin <> '' or icc.NewIsin <> '')
and exchgcd='IRTSE'

union

select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_620_SCMST.IssID,
v20c_620_SCMST.SecID,
v20c_620_SCMST.Issuername,
v20c_620_SCMST.CntryofIncorp,
v20c_620_SCMST.SecurityDesc,
rtrim(v20c_620_SCMST.SectyCD) as SectyCD,
/*v20c_620_SCMST.StructCD,*/
v20c_620_SCMST.Isin,
v20c_620_SCMST.Uscode,
v20c_620_dSCEXH.Sedol,
v20c_620_dSCEXH.RegCountry,
case when v20c_620_dSCEXH.scexhid is null 
     then rtrim(v20c_620_SCMST.PrimaryExchgCD)
     else rtrim(v20c_620_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_620_dSCEXH.Localcode,
rtrim(v20c_620_dSCEXH.ListStatus) as ListStatus,
v20c_620_SCMST.secstatus as StatusFlag,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN v20c_620_SCMST ON v20c_620_SCMST.secid = lcc.secid
LEFT OUTER JOIN v20c_620_dSCEXH ON v20c_620_SCMST.secid = v20c_620_dSCEXH.secID
where 
v20c_620_dSCEXH.exchgcd='IRTSE'
order by changetype, issuername