--filepath=o:\Datafeed\Equity\DeShaw_631_632\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.632
--suffix=
--fileheadertext=EDI_REORG_632_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\DeShaw_631_632\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select 
v10s_BKRP.Levent,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'FilingDate' as PrimaryDate,
v10s_BKRP.FilingDate as Enddate1,
'NotificationDate' as SecondaryDate,
v10s_BKRP.NotificationDate as Enddate2,
'' as TertiaryDate,
'' as Enddate3
FROM v10s_BKRP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_BKRP.IssID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
v10s_BKRP.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
and v20c_EV_dSCEXH.secid is not null and v20c_EV_dSCEXH.exchgcd <> '' 
and v20c_EV_dSCEXH.exchgcd is not null and v20c_EV_dSCEXH.exchgcd <> '' 
and v20c_EV_dSCEXH.exchgcd is not null

union

select 
v10s_INCHG.Levent,
v10s_INCHG.AnnounceDate as Created,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'EffectiveDate' as PrimaryDate,
v10s_INCHG.InChgDate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_INCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_INCHG.IssID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
where
v10s_INCHG.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1) and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_INCHG.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

union

select 
v10s_CLACT.Levent,
v10s_CLACT.AnnounceDate as Created,
v10s_CLACT.Acttime as Changed,
v10s_CLACT.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'EffectiveDate' as PrimaryDate,
v10s_CLACT.EffectiveDate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_CLACT
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_CLACT.IssID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
where
v10s_CLACT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' 
and v20c_EV_dscexh.exchgcd is not null
and v10s_CLACT.Actflag <> 'D'
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_LIQ.Levent,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as Changed,
v10s_LIQ.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'LiquidationDate' as PrimaryDate,
MPAY.Paydate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_LIQ
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
where
v10s_LIQ.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1) and v20c_EV_dSCEXH.secid is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null
and v10s_LIQ.Actflag <> 'D'
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_LSTAT.Levent,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dSCEXH.Sedol,
v20c_EV_dSCEXH.ExchgCD,
CASE WHEN v10s_LSTAT.LstatStatus = 'D' THEN 'DelistingDate'
     WHEN v10s_LSTAT.LstatStatus = 'R' THEN 'ResumptionDate'
     WHEN v10s_LSTAT.LstatStatus = 'S' THEN 'SuspensionDate'
     ELSE 'UnknownDate' END as PrimaryDate,
v10s_LSTAT.EffectiveDate as Enddate1,
'NotificationDate' as SecondaryDate,
v10s_LSTAT.NotificationDate as Enddate2,
'' as TertiaryDate,
'' as Enddate3
FROM v10s_LSTAT
INNER JOIN v20c_EV_dSCEXH ON v10s_LSTAT.SecID = v20c_EV_dSCEXH.SecID AND v10s_LSTAT.ExchgCD = v20c_EV_dSCEXH.ExchgCD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LSTAT.SecID
where
v10s_LSTAT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1) and v20c_EV_dSCEXH.secid is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null
and v10s_LSTAT.Actflag <> 'D'
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select
v10s_NLIST1.Levent,
v10s_NLIST1.AnnounceDate as Created,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'Listdate' as PrimaryDate,
v20c_EV_dscexh.Listdate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_NLIST1
INNER JOIN v20c_EV_dscexh ON v10s_NLIST1.ScexhID = v20c_EV_dscexh.ScexhID
INNER JOIN v20c_EV_SCMST ON v20c_EV_dscexh.SecID = v20c_EV_SCMST.SecID
where
v10s_NLIST1.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1) and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_NLIST1.Actflag <> 'D' 
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_DIV.Levent,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
v10s_DIV.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
where 
(v10s_DIV.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and DIVPY.OptionID is not null
and v10s_DIV.Actflag <> 'D'
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select
v10s_CTX.Levent,
v10s_CTX.AnnounceDate as Created,
v10s_CTX.Acttime as Changed,
v10s_CTX.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'EndDate' as PrimaryDate,
v10s_CTX.EndDate as Enddate1,
'StartDate' as SecondaryDate,
v10s_CTX.StartDate as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_CTX
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CTX.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN SCMST ON v10s_CTX.ResSecID = SCMST.SecID
where
v10s_CTX.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_CTX.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_REDEM.Levent,
v10s_REDEM.AnnounceDate as Created,
v10s_REDEM.Acttime as Changed,
v10s_REDEM.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'RedemptionDate' as PrimaryDate,
v10s_REDEM.RedemptionDate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_REDEM
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_REDEM.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
where
v10s_REDEM.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_REDEM.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
 
union 
 
select 
v10s_BB.Levent,
v10s_BB.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_BB.Acttime) THEN MPAY.Acttime ELSE v10s_BB.Acttime END as [Changed],
v10s_BB.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'EndDate' as PrimaryDate,
v10s_BB.EndDate as Enddate1,
'StartDate' as SecondaryDate,
v10s_BB.StartDate as Endate2,
'PayDate' as TertiaryDate,
MPAY.PayDate as Endate3
FROM v10s_BB
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_BB.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN MPAY ON v10s_BB.EventID = MPAY.EventID AND v10s_BB.SEvent = MPAY.SEvent
where
(v10s_BB.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or MPAY.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_BB.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_CALL.Levent,
v10s_CALL.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CALL.Acttime THEN RD.Acttime ELSE v10s_CALL.Acttime END as [Changed],
v10s_CALL.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'DueDate' as PrimaryDate,
v10s_CALL.DueDate as Enddate1,
'RecDate' as SecondaryDate,
RD.RecDate as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_CALL
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CALL.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN RD ON v10s_CALL.RdID = RD.RdID
where
(v10s_CALL.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_CALL.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_CAPRD.Levent,
v10s_CAPRD.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CAPRD.Acttime THEN RD.Acttime ELSE v10s_CAPRD.Acttime END as [Changed],
v10s_CAPRD.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
v10s_CAPRD.PayDate as Enddate1,
'EffectiveDate' as SecondaryDate,
v10s_CAPRD.EffectiveDate as Endate2,
'RecDate' as TertiaryDate,
RD.RecDate as Endate3
FROM v10s_CAPRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CAPRD.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN RD ON v10s_CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(v10s_CAPRD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_CAPRD.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_RCAP.Levent,
v10s_RCAP.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_RCAP.Acttime THEN RD.Acttime ELSE v10s_RCAP.Acttime END as [Changed],
v10s_RCAP.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'CSPYDate' as PrimaryDate,
v10s_RCAP.CSPYDate as Enddate1,
'EffectiveDate' as SecondaryDate,
v10s_RCAP.EffectiveDate as Endate2,
'RecDate' as TertiaryDate,
RD.RecDate as Endate3
FROM v10s_RCAP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_RCAP.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
where
(v10s_RCAP.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_RCAP.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_TKOVR.Levent,
v10s_TKOVR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_TKOVR.Acttime) THEN MPAY.Acttime ELSE v10s_TKOVR.Acttime END as [Changed],
v10s_TKOVR.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dSCEXH.Sedol,
v20c_EV_dSCEXH.ExchgCD,
'CompulsoryAcqDate' as PrimaryDate,
v10s_TKOVR.CmAcqDate as Enddate1,
'UnconditionalDate' as SecondaryDate,
v10s_TKOVR.UnconditionalDate as Endate2,
'CloseDate' as TertiaryDate,
v10s_TKOVR.CloseDate as Endate3
FROM v10s_TKOVR
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_TKOVR.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID AND v10s_TKOVR.SEvent = MPAY.SEvent
where
(v10s_TKOVR.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or MPAY.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dSCEXH.secid is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null
and v10s_TKOVR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dSCEXH.Sedol is not null)

union

select 
v10s_ARR.Levent,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v10s_ARR.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(v10s_ARR.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_ARR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_BON.Levent,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
v10s_BON.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
where
(v10s_BON.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_BON.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_BR.Levent,
v10s_BR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BR.Acttime) THEN PEXDT.Acttime ELSE v10s_BR.Acttime END as [Changed],
v10s_BR.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_BR
INNER JOIN RD ON RD.RdID = v10s_BR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'BR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
where
(v10s_BR.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_BR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_CONSD.Levent,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
v10s_CONSD.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(v10s_CONSD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_CONSD.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_DMRGR.Levent,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
v10s_DMRGR.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
where
(v10s_DMRGR.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_DMRGR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_DIST.Levent,
v10s_DIST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIST.Acttime) THEN PEXDT.Acttime ELSE v10s_DIST.Acttime END as [Changed],
v10s_DIST.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
where
(v10s_DIST.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_DIST.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select
v10s_DVST.Levent,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v10s_DVST.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
where
(v10s_DVST.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_DVST.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_ENT.Levent,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
v10s_ENT.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
where
(v10s_ENT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_ENT.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_SD.Levent,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
v10s_SD.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_dscexh.ExchgCD,
'NewCodeDate' as PrimaryDate,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as EndDate1,
'PayDate' as SecondaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate2,
'RecDate' as TertiaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate3
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(v10s_SD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or SDCHG.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or ICC.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_SD.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_MRGR.Levent,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v10s_MRGR.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'AppointedDate' as PrimaryDate,
v10s_MRGR.AppointedDate as Enddate1,
'EffectiveDate' as SecondaryDate,
v10s_MRGR.EffectiveDate as EndDate2,
'PayDate' as TertiaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate3
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID AND v10s_MRGR.SEvent = MPAY.SEvent
where
(v10s_MRGR.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_MRGR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_RTS.Levent,
v10s_RTS.AnnounceDate as Created,
RD.Acttime Changed,
v10s_RTS.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'EndTrade' as PrimaryDate,
v10s_RTS.EndTrade as Enddate1,
'StartTrade' as SecondaryDate,
v10s_RTS.StartTrade as EndDate2,
'SplitDate' as TertiaryDate,
v10s_RTS.SplitDate as EndDate3
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
where
v10s_RTS.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_RTS.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_PRF.Levent,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
v10s_PRF.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
where
(v10s_PRF.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_PRF.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_PO.Levent,
v10s_PO.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PO.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PO.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PO.Acttime) THEN PEXDT.Acttime ELSE v10s_PO.Acttime END as [Changed],
v10s_PO.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_PO
INNER JOIN RD ON RD.RdID = v10s_PO.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'PO' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PO' = PEXDT.EventType
where
(v10s_PO.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_PO.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)

union

select 
v10s_DRIP.Levent,
v10s_DRIP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DRIP.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DRIP.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DRIP.Acttime) THEN PEXDT.Acttime ELSE v10s_DRIP.Acttime END as [Changed],
v10s_DRIP.ActFlag,
v20c_EV_SCMST.PrimaryExchgCD, v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.ExchgCD,
'PayDate' as PrimaryDate,
CASE WHEN DRIP.DripPayDate  is not null THEN DRIP.DripPayDate WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DRIP
INNER JOIN DRIP ON v10s_DRIP.EventID = DRIP.DivID
INNER JOIN RD ON RD.RdID = v10s_DRIP.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID and v20c_EV_dscexh.ExCountry = DRIP.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
where
(v10s_DRIP.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or DRIP.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime between .acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and DRIP.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
