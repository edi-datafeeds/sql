--filepath=o:\Datafeed\Equity\625\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.625
--suffix=
--fileheadertext=EDI_LOOKUP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\Equity\625\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
upper(Actflag) as Actflag,
lookup.Acttime,
upper(TypeGroup) as TypeGroup,
upper(Code) as Code,
lookup.Lookup
FROM LOOKUP
where 
code <> 'BLANK'
and code <> ''
and code <> 'INTONMAT'
and code <> 'INTONTRIG'

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('ACTION') as TypeGroup,
irAction.Code,
irAction.Lookup
from irAction

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('DIVPERIOD') as TypeGroup,
DVPRD.DvprdCD,
DVPRD.Divperiod
from DVPRD

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('EXSTYLE') as TypeGroup,
irExstyle.Code,
irExstyle.Lookup
from irExstyle

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('FRACTIONS') as TypeGroup,
irFractions.Code,
irFractions.Lookup
from irFractions

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('LSTATSTAT') as TypeGroup,
irLstatStat.Code,
irLstatStat.Lookup
from irLstatStat

union

SELECT
upper('I') as Actflag,
'2006/01/26' as AnnounceDate,
upper('MANDOPT') as TypeGroup,
irMandopt.Code,
irMandopt.Lookup
from irMandopt

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('MRGRSTAT') as TypeGroup,
irMrgrStat.Code,
irMrgrStat.Lookup
from irMrgrStat

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('ONOFFMARKET') as TypeGroup,
irOnOffMarket.Code,
irOnOffMarket.Lookup
from irOnOffMarket

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('PARTFINAL') as TypeGroup,
irPartfinal.Code,
irPartfinal.Lookup
from irPartfinal

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('PAYTYPE') as TypeGroup,
irPaytype.Code,
irPaytype.Lookup
from irPaytype

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('SCMSTSTAT') as TypeGroup,
irScmstStat.Code,
irScmstStat.Lookup
from irScmstStat

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('TKOVRSTAT') as TypeGroup,
irTkovrStat.Code,
irTkovrStat.Lookup
from irTkovrStat

union

SELECT
upper('I') as Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.Exchgname as Lookup
from EXCHG

union

SELECT
upper('I') as Actflag,
'2006/01/26' as Acttime,
upper('MEETING') as TypeGroup,
irMeeting.Code,
irMeeting.Lookup
from irMeeting

union

SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('MIC') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.MIC as Lookup
from EXCHG

order by TypeGroup, Code