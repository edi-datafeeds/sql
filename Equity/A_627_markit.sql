--filepath=o:\Datafeed\Equity\627_markit\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.627
--suffix=
--fileheadertext=EDI_DIVIDEND_627_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\627_markit\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
SELECT *  
FROM t620_Dividend
left outer join wca.dbo.dprcp on t620_Dividend.secid = wca.dbo.dprcp.secid
WHERE
PrimaryExchgCD=ExchgCD
and (SecStatus <> 'I'  or SecStatus is null)
and (t620_Dividend.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and sectycd <> 'UNT'
and sectycd <> 'CDI'
and sectycd <> 'TRT'
and sectycd <> 'DRT'
and sectycd <> 'SP'
and sectycd <> 'STP'
and sectycd <> 'SS'
and (
substring(primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))
