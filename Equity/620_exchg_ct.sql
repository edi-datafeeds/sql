--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=SG_eventcounts_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=Event_Counts_for_SG_from_2007001
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 0
use wca
select '' as Event, '' as EventsSince2007


--# 1
USE WCA
select 'Company_Meeting' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Company_Meeting 
WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 2
USE WCA
select 'Call' as event, count(distinct eventid) as EventsSince2007
FROM v53f_620_Call 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 3
USE WCA
select 'Liquidation' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Liquidation 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 4
USE WCA
select 'Certificate_Exchange' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Certificate_Exchange 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 5
USE WCA
select 'International_Code_Change' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_International_Code_Change
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 6
USE WCA
select 'Conversion_Terms' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Conversion_Terms
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 7
USE WCA
select 'Redemption_Terms' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Redemption_Terms
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 8
USE WCA
select 'Security_Reclassification' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Security_Reclassification
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 9
USE WCA
select 'Lot_Change' as event, count(distinct eventid) as EventsSince2007
FROM v52f_620_Lot_Change
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 10
USE WCA
select 'Sedol_Change' as event, count(distinct eventid) as EventsSince2007
FROM v52f_620_Sedol_Change
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 11
USE WCA
select 'Buy_Back' as event, count(distinct eventid) as EventsSince2007
FROM v53f_620_Buy_Back
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 12
USE WCA
select 'Capital_Reduction' as event, count(distinct eventid) as EventsSince2007
FROM v53f_620_Capital_Reduction
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 14
USE WCA
select 'Takeover' as event, count(distinct eventid) as EventsSince2007
FROM v53f_620_Takeover
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 15
USE WCA
select 'Arrangement' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Arrangement
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 16
USE WCA
select 'Bonus' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Bonus
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 17
USE WCA
select 'Bonus_Rights' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Bonus_Rights
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 18
USE WCA
select 'Consolidation' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Consolidation
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 19
USE WCA
select 'Demerger' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Demerger
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 20
USE WCA
select 'Distribution' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Distribution
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 21
USE WCA
select 'Divestment' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Divestment
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 22
USE WCA
select 'Entitlement' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Entitlement
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 23
USE WCA
select 'Merger' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Merger
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 24
USE WCA
select 'Preferential_Offer' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Preferential_Offer
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 25
USE WCA
select 'Purchase_Offer' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Purchase_Offer
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 26
USE WCA
select 'Rights' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Rights 
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 27
USE WCA
select 'Security_Swap' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Security_Swap 
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 28
USE WCA
select 'Subdivision' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Subdivision
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 29
USE WCA
select 'Bankruptcy' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Bankruptcy 
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 30
USE WCA
select 'Financial_Year_Change' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Financial_Year_Change
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 31
USE WCA
select 'Incorporation_Change' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Incorporation_Change
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 32
USE WCA
select 'Issuer_Name_change' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Issuer_Name_change
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 33
USE WCA
select 'Class_Action' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Class_Action
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 34
USE WCA
select 'Security_Description_Change' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Security_Description_Change
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 35
USE WCA
select 'Assimilation' as event, count(distinct eventid) as EventsSince2007
FROM v52f_620_Assimilation
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 36
USE WCA
select 'Listing_Status_Change' as event, count(distinct eventid) as EventsSince2007
FROM v52f_620_Listing_Status_Change
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 37
USE WCA
select 'Local_Code_Change' as event, count(distinct eventid) as EventsSince2007
FROM v52f_620_Local_Code_Change
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 38
USE WCA
select 'New_Listing' as event, count(distinct eventid) as EventsSince2007
FROM v52f_620_New_Listing
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 39
USE WCA
select 'Announcement' as event, count(distinct eventid) as EventsSince2007
FROM v50f_620_Announcement
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 40
USE WCA
select 'Parvalue_Redenomination' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Parvalue_Redenomination 
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 41
USE WCA
select 'Currency_Redenomination' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Currency_Redenomination 
 

WHERE CHANGED >= '2007/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='SG'


--# 42
USE WCA
select 'Return_of_Capital' as event, count(distinct eventid) as EventsSince2007
FROM v53f_620_Return_of_Capital 
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 43
USE WCA
select 'Dividend' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Dividend
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 44
USE WCA
select 'Dividend_Reinvestment_Plan' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Dividend_Reinvestment_Plan
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 45
USE WCA
select 'Franking' as event, count(distinct eventid) as EventsSince2007
FROM v54f_620_Franking
 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'



--# 46
USE WCA
select 'Conversion_Terms_Change' as event, count(distinct eventid) as EventsSince2007
FROM v51f_620_Conversion_Terms_Change 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'


--# 47
USE WCA
select 'Shares_Outstanding_Change' as event, count(distinct eventid) as EventsSince2007
FROM v51f_612_xShares_Outstanding_Change 

WHERE CHANGED >= '2007/01/01'
and excountry='SG'
