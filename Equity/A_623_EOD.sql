--filepath=o:\Datafeed\Equity\623\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.623
--suffix=
--fileheadertext=EDI_STATIC_623_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\623\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
scexh.ScexhID,
scexh.Actflag,
scexh.Acttime as Changed,
scmst.IssID,
scmst.SecID,
issur.Issuername,
issur.CntryofIncorp,
issur.IndusID,
issur.Financialyearend,
case when scmst.Statusflag <> '' then scmst.Statusflag else 'A' end as SecStatus,
scmst.PrimaryExchgCD,
scmst.Securitydesc,
scmst.CurenCD as ParValueCurrency,
scmst.Parvalue,
scmst.SectyCD,
scmst.Uscode,
scmst.Isin,
case when sedol.defunct <> 'T' then sedol.sedol else '' end as Sedol,
case when sedol.defunct <> 'T' then sedol.rcntrycd else '' end as CountryOfRegister,
scexh.exchgcd,
scexh.localcode,
case when scexh.liststatus is not null then scexh.liststatus else 'L' end as ListStatus
FROM scmst
INNER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN cntry ON issur.CntryofIncorp = cntry.cntryCD
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                      AND exchg.cntryCD = Sedol.CntryCD
where 
(scmst.sectycd = 'EQS'
or scmst.sectycd = 'PRF'
or scmst.sectycd = 'DR')
and (scmst.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or sedol.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or scexh.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or issur.acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
