--filepath=o:\Datafeed\Equity\610_Statpro\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_610_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\610_Statpro\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca2
SELECT * 
FROM t610_Company_Meeting 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 2
use wca2
SELECT *
FROM t610_Call
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 3
use wca2
SELECT * 
FROM t610_Liquidation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 4
use wca2
SELECT *
FROM t610_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 5
use wca2
SELECT * 
FROM t610_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')




--# 7
use wca2
SELECT * 
FROM t610_Preference_Redemption
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 8
use wca2
SELECT * 
FROM t610_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 9
use wca2
SELECT * 
FROM t610_Lot_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 10
use wca2
SELECT * 
FROM t610_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 11
use wca2
SELECT * 
FROM t610_Buy_Back
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 12
use wca2
SELECT * 
FROM t610_Capital_Reduction
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 13
use wca2
SELECT * 
FROM t610_Takeover
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 14
use wca2
SELECT * 
FROM t610_Arrangement
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 15
use wca2
SELECT * 
FROM t610_Bonus
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL)
and OfferedSecType<>'Warrants'
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 16
use wca2
SELECT * 
FROM t610_Bonus_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 17
use wca2
SELECT * 
FROM t610_Consolidation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 18
use wca2
SELECT * 
FROM t610_Demerger
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 19
use wca2
SELECT * 
FROM t610_Distribution
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 20
use wca2
SELECT * 
FROM t610_Divestment
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 21
use wca2
SELECT * 
FROM t610_Entitlement_From_BON
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 



--# 22
use wca2
SELECT * 
FROM t610_Merger
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 23
use wca2
SELECT * 
FROM t610_Preferential_Offer
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 24
use wca2
SELECT * 
FROM t610_Purchase_Offer
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 25
use wca2
SELECT * 
FROM t610_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 26
use wca2
SELECT * 
FROM t610_Security_Swap 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 27
use wca2
SELECT *
FROM t610_Subdivision
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 28
use wca2
SELECT *
FROM t610_Bankruptcy 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 29
use wca2
SELECT *
FROM t610_Financial_Year_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 30
use wca2
SELECT *
FROM t610_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 31
use wca2
SELECT *
FROM t610_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 32
use wca2
SELECT *
FROM t610_lawsuit
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 33
use wca2
SELECT *
FROM t610_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 34
use wca2
SELECT *
FROM t610_Assimilation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 35
use wca2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 36
use wca2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 37
use wca2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 38
use wca2
SELECT * 
FROM t610_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 39
use wca2
SELECT * 
FROM t610_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 40
use wca2
SELECT * 
FROM t610_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 41
use wca2
SELECT * 
FROM t610_Return_of_Capital 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')


--# 42
use wca
SELECT * 
FROM v54f_610_Dividend_Statpro
WHERE
changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (listingstatus<>'De-Listed' or listingstatus is null)
and not (divperiodcd='CG' and nildividend='T')
and not (nildividend='T'
and 1<(select count(div.divid) from rd as subrd
inner join div on subrd.rdid = div.rdid
where 
(subrd.secid=v54f_610_Dividend_Statpro.secid and subrd.recdate=v54f_610_Dividend_Statpro.recdate)))
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 43
use wca2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (listingstatus<>'De-Listed' or listingstatus is null)
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')



--# 44
use wca2
SELECT * 
FROM t610_Franking
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (listingstatus<>'De-Listed' or listingstatus is null)
-- and excountry<>'US' and excountry<>'CA'
--and (primaryex='Yes' or primaryex = '')

