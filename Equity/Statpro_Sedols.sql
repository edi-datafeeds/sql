--filepath=o:\Datafeed\Equity\statpro_sedols\
--filenameprefix=
--filename=sedol
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select distinct sedol
from v20c_620_scmst
inner join v20c_620_scexh on  v20c_620_scmst.secid = v20c_620_scexh.secid
where
sedol<>'' and sedol is not null
and (exchgcd = primaryexchgcd or primaryexchgcd ='')
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
order by sedol
