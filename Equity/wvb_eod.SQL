--filepath=o:\Datafeed\Equity\wvb\
--filenameprefix=WCA_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\wvb\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT 
issur.Issuername,
issur.CntryofIncorp,
scmst.Acttime,
scmst.Actflag,
scmst.IssID,
scmst.SecID,
scmst.Statusflag,
scmst.PrimaryExchgCD,
scmst.Securitydesc,
scmst.CurenCD as ParValueCurrency,
scmst.Parvalue,
scmst.SectyCD,
scmst.Uscode,
scmst.Isin,
case when sedol.actflag <> 'D' then Sedol.Sedol else '' end as Sedol,
case when sedol.actflag <> 'D' then Sedol.Defunct else '' end as Defunct,
scexh.exchgcd,
scexh.localcode,
scexh.liststatus,
cntry.curenCD as DomicileCurrency,
issur.IndusID
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN cntry ON issur.CntryofIncorp = cntry.cntryCD
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                      AND exchg.cntryCD = Sedol.CntryCD
where scmst.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or sedol.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or scexh.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or issur.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
order by scexh.exchgcd
