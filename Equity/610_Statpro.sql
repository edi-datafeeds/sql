--filepath=o:\Datafeed\Equity\610_Statpro\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_610_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\610_Statpro\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca2
SELECT * 
FROM t610_Company_Meeting 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 2
use wca2
SELECT *
FROM t610_Call
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 3
use wca2
SELECT * 
FROM t610_Liquidation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 4
use wca2
SELECT *
FROM t610_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 5
use wca2
SELECT * 
FROM t610_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null



--# 7
use wca2
SELECT * 
FROM t610_Preference_Redemption
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 8
use wca2
SELECT * 
FROM t610_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 9
use wca2
SELECT * 
FROM t610_Lot_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 10
use wca2
SELECT * 
FROM t610_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 11
use wca2
SELECT * 
FROM t610_Buy_Back
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 12
use wca2
SELECT * 
FROM t610_Capital_Reduction
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 13
use wca2
SELECT * 
FROM t610_Takeover
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 14
use wca2
SELECT * 
FROM t610_Arrangement
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 15
use wca2
SELECT * 
FROM t610_Bonus
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 16
use wca2
SELECT * 
FROM t610_Bonus_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 17
use wca2
SELECT * 
FROM t610_Consolidation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 18
use wca2
SELECT * 
FROM t610_Demerger
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 19
use wca2
SELECT * 
FROM t610_Distribution
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 20
use wca2
SELECT * 
FROM t610_Divestment
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 21
use wca2
SELECT * 
FROM t610_Entitlement
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 22
use wca2
SELECT * 
FROM t610_Merger
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 23
use wca2
SELECT * 
FROM t610_Preferential_Offer
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 24
use wca2
SELECT * 
FROM t610_Purchase_Offer
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 25
use wca2
SELECT * 
FROM t610_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 26
use wca2
SELECT * 
FROM t610_Security_Swap 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 27
use wca2
SELECT *
FROM t610_Subdivision
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 28
use wca2
SELECT *
FROM t610_Bankruptcy 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 29
use wca2
SELECT *
FROM t610_Financial_Year_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 30
use wca2
SELECT *
FROM t610_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 31
use wca2
SELECT *
FROM t610_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 32
use wca2
SELECT *
FROM t610_lawsuit
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 33
use wca2
SELECT *
FROM t610_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 34
use wca2
SELECT *
FROM t610_Assimilation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 35
use wca2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 36
use wca2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 37
use wca2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 38
use wca2
SELECT * 
FROM t610_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 39
use wca2
SELECT * 
FROM t610_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 40
use wca2
SELECT * 
FROM t610_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 41
use wca2
SELECT * 
FROM t610_Return_of_Capital 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null



--# 42
use wca2
SELECT * 
FROM t610_Dividend
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 43
use wca2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null


--# 44
use wca2
SELECT * 
FROM t610_Franking
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and excountry<>'US' and excountry<>'CA'
and (primaryex='Yes' or primaryex = '')
and sedol<>'' and sedol is not null

