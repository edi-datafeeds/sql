--filepath=o:\prodman\salesfeeds\wca\620\sampledata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=_SampleEffectiveDate
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

USE wCA
SELECT *  
FROM x54f_620_Dividend
WHERE ((ExDate >= '2009/01/26' and ExDate < '2010/01/22')
or (Paydate >= '2009/01/26' and payDate < '2010/01/22'))
and secid in (select code from client.dbo.pfsecid where accid=999)
ORDER BY EventID desc, ExchgCD, Sedol