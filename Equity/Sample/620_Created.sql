--filepath=o:\prodman\salesfeeds\wca\620\sampledata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=_SampleCreated
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
USE WCA
SELECT *  
FROM v50f_620_Company_Meeting 
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
SELECT * 
FROM v53f_620_Call
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
SELECT *  
FROM v50f_620_Liquidation
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 4
USE WCA
SELECT * 
FROM v51f_620_Certificate_Exchange
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
SELECT *  
FROM v51f_620_International_Code_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
SELECT *  
FROM v51f_620_Conversion_Terms
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
SELECT *  
FROM v51f_620_Redemption_Terms
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
SELECT *  
FROM v51f_620_Security_Reclassification
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
SELECT *  
FROM v52f_620_Lot_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
SELECT *  
FROM v52f_620_Sedol_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca
SELECT *  
FROM v53f_620_Buy_Back
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
SELECT *  
FROM v53f_620_Capital_Reduction
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
use wca
SELECT *  
FROM v53f_620_Takeover
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
use wca
SELECT *  
FROM v54f_620_Arrangement
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
SELECT *  
FROM v54f_620_Bonus
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
SELECT *  
FROM v54f_620_Bonus_Rights
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
SELECT *  
FROM v54f_620_Consolidation
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca
SELECT *  
FROM v54f_620_Demerger
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
SELECT *  
FROM v54f_620_Distribution
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
use wca
SELECT *  
FROM v54f_620_Divestment
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
SELECT *  
FROM v54f_620_Entitlement
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
use wca
SELECT *  
FROM v54f_620_Merger
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
use wca
SELECT *  
FROM v54f_620_Preferential_Offer
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
SELECT *  
FROM v54f_620_Purchase_Offer
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
SELECT *  
FROM v54f_620_Rights 
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
SELECT *  
FROM v54f_620_Security_Swap 
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
SELECT * 
FROM v54f_620_Subdivision
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
SELECT * 
FROM v50f_620_Bankruptcy 
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
SELECT * 
FROM v50f_620_Financial_Year_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
SELECT * 
FROM v50f_620_Incorporation_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
SELECT * 
FROM v50f_620_Issuer_Name_change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
SELECT * 
FROM v50f_620_Class_Action
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
SELECT * 
FROM v51f_620_Security_Description_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
SELECT * 
FROM v52f_620_Assimilation
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
SELECT *  
FROM v52f_620_New_Listing
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
SELECT *  
FROM v50f_620_Announcement
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
use wca
SELECT *  
FROM v51f_620_Parvalue_Redenomination 
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
SELECT *  
FROM v51f_620_Currency_Redenomination 
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
AND RelatedEvent <> 'Clean' or RelatedEvent is null
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
SELECT *  
FROM v53f_620_Return_of_Capital 
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
SELECT *  
FROM v54f_620_Dividend
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
USE WCA
SELECT *  
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
USE WCA
SELECT *  
FROM v54f_620_Franking
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol

--# 46
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE Created >= '2012/01/01' and Created < '2013/01/01'
and secid in (select code from client.dbo.pfsecid where accid = 999 and actflag<>'D')
ORDER BY EventID desc, ExchgCD, Sedol
