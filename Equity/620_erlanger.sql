--filepath=o:\Datafeed\Equity\620_erlanger\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 18
use wca
SELECT *  
FROM v54f_620_Consolidation
WHERE CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (exchgcd = 'USNASD'
or exchgcd='USAMEX'
or exchgcd='USNYSE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
SELECT * 
FROM v54f_620_Subdivision
WHERE CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (exchgcd = 'USNASD'
or exchgcd='USAMEX'
or exchgcd='USNYSE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (exchgcd = 'USNASD'
or exchgcd='USAMEX'
or exchgcd='USNYSE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (exchgcd = 'USNASD'
or exchgcd='USAMEX'
or exchgcd='USNYSE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
SELECT *  
FROM v52f_620_New_Listing
WHERE CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (exchgcd = 'USNASD'
or exchgcd='USAMEX'
or exchgcd='USNYSE')
ORDER BY EventID desc, ExchgCD, Sedol
