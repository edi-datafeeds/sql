--filepath=o:\Datafeed\Equity\DeShaw_631_632\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.632
--suffix=
--fileheadertext=EDI_REORG_632_
--fileheaderdate=yymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\DeShaw_631_632\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
select 
v20c_620_SCMST.Issuername,
v10s_DIV.Levent,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
case when v10s_DIV.Actflag='D' OR v10s_DIV.Actflag='C' then v10s_DIV.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
where 
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' 
and v20c_620_scexh.exchgcd is not null
and v10s_DIV.Actflag <> 'D'
and DIVPY.divtype = 'S'
and DIVPY.optionid = 1

union

select 
v20c_620_SCMST.Issuername,
v10s_BON.Levent,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
case when v10s_BON.Actflag='D' OR v10s_BON.Actflag='C' then v10s_BON.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
       and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' 
and v20c_620_scexh.exchgcd is not null
and v10s_BON.Actflag <> 'D'

union

select 
v20c_620_SCMST.Issuername,
v10s_CONSD.Levent,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
case when v10s_CONSD.Actflag='D' OR v10s_CONSD.Actflag='C' then v10s_CONSD.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' and v20c_620_scexh.exchgcd is not null
and v10s_CONSD.Actflag <> 'D' 

union

select 
v20c_620_SCMST.Issuername,
v10s_DMRGR.Levent,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
case when v10s_DMRGR.Actflag='D' OR v10s_DMRGR.Actflag='C' then v10s_DMRGR.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' and v20c_620_scexh.exchgcd is not null
and v10s_DMRGR.Actflag <> 'D'

union


select 
v20c_620_SCMST.Issuername,
v10s_ENT.Levent,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
case when v10s_ENT.Actflag='D' OR v10s_ENT.Actflag='C' then v10s_ENT.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' and v20c_620_scexh.exchgcd is not null
and v10s_ENT.Actflag <> 'D'

union

select 
v20c_620_SCMST.Issuername,
v10s_SD.Levent,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
case when v10s_SD.Actflag='D' OR v10s_SD.Actflag='C' then v10s_SD.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' and v20c_620_scexh.exchgcd is not null
and v10s_SD.Actflag <> 'D'

union

select 
v20c_620_SCMST.Issuername,
v10s_RTS.Levent,
v10s_RTS.AnnounceDate as Created,
RD.Acttime Changed,
case when v10s_RTS.Actflag='D' OR v10s_RTS.Actflag='C' then v10s_RTS.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'RTS' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'RTS' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' and v20c_620_scexh.exchgcd is not null
and v10s_RTS.Actflag <> 'D' 

union

select 
v20c_620_SCMST.Issuername,
v10s_DVST.Levent,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
case when v10s_DVST.Actflag='D' OR v10s_DVST.Actflag='C' then v10s_DVST.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.Exdate is not null THEN EXDT.Exdate WHEN PEXDT.Exdate is not null THEN PEXDT.Exdate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.Exdate is not null THEN 'Exdate' WHEN PEXDT.Exdate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' and v20c_620_scexh.exchgcd is not null
and v10s_DVST.Actflag <> 'D'

union

select 
v20c_620_SCMST.Issuername,
v10s_CAPRD.Levent,
v10s_CAPRD.AnnounceDate as Created,
v10s_CAPRD.Acttime as [Changed],
v10s_CAPRD.Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
v10s_CAPRD.Effectivedate as TriggerDate,
'Effectivedate' as TriggerType
FROM v10s_CAPRD
INNER JOIN v20c_620_SCMST ON v10s_CAPRD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and (v10s_CAPRD.Effectivedate is not null and v10s_CAPRD.Effectivedate between '2007/03/01' and '2007/03/31')
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' and v20c_620_scexh.exchgcd is not null
and v10s_CAPRD.Actflag <> 'D' 

union

select 
v20c_620_SCMST.Issuername,
v10s_PRF.Levent,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
case when v10s_PRF.Actflag='D' OR v10s_PRF.Actflag='C' then v10s_PRF.Actflag ELSE '' END as Actflag,
v20c_620_SCMST.ISIN,
v20c_620_SCMST.Uscode,
v20c_620_scexh.Sedol,
v20c_620_SCMST.PrimaryExchgCD,
v20c_620_scexh.ExchgCD,
v20c_620_scexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate WHEN PEXDT.ExDate is not null THEN PEXDT.ExDate ELSE RD.RecDate END as TriggerDate,
CASE WHEN EXDT.ExDate is not null THEN 'Exdate' WHEN PEXDT.ExDate is not null THEN 'Exdate' ELSE 'Recdate' END as TriggerType
FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_620_SCMST ON RD.SecID = v20c_620_SCMST.SecID
INNER JOIN v20c_620_scexh ON v20c_620_SCMST.SecID = v20c_620_scexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_620_scexh.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_620_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
      and (substring(PEXDT.ExchgCD,1,2) = 'US' or substring(PEXDT.ExchgCD,1,2) = 'CA')
where
(v20c_620_scexh.ExCountry='CA' or v20c_620_scexh.ExCountry='US') AND (v20c_620_scmst.SECTYCD='EQS' OR v20c_620_scmst.SECTYCD='PRF')
and ((EXDT.Exdate IS NOT NULL AND EXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NOT NULL AND PEXDT.Exdate between '2007/03/01' and '2007/03/31')
      or (EXDT.Exdate IS NULL AND PEXDT.Exdate IS NULL AND RD.Recdate between '2007/03/01' and '2007/03/31'))
and v20c_620_scexh.secid is not null and v20c_620_scexh.exchgcd <> '' 
and v20c_620_scexh.exchgcd is not null
and v10s_PRF.Actflag <> 'D' 

order by levent, triggerdate desc
