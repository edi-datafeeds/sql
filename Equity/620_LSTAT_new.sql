--filepath=o:\Datafeed\Equity\620_LSTAT_new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Equity\620_LSTAT_new\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
--filepath=o:\Datafeed\Equity\620_LSTAT_new\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Equity\620_LSTAT_new\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca2
SELECT distinct vtab.eventid, vtab.secid, vtab.isin, vtab.liststatchangedto,  
case when vtab.liststatchangedto<>'D' then 'Yes'
when wca.dbo.scexh.exchgcd<>vtab.exchgcd then 'Yes' 
else 'No' end as StillLive,
vtab.effectivedate
FROM t620_Listing_Status_Change as vtab
left outer join wca.dbo.scexh on vtab.secid=wca.dbo.scexh.secid
                              and 'D'<>wca.dbo.scexh.liststatus
                              and vtab.exchgcd<>wca.dbo.scexh.exchgcd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY vtab.eventid

