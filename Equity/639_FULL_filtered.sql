--filepath=o:\Datafeed\Equity\639\
--filenameprefix=FILTERED_
--filename=yyyymmdd
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--fileextension=.639
--suffix=
--fileheadertext=EDI_STATIC_FILTERED_639_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\639\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
where issur.actflag<>'D'

--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.X as CommonCode
FROM scmst
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
WHERE 
scmst.actflag<>'D'
and scexh.actflag<>'D'
and (cntrycd = 'AT'
or cntrycd = 'BE'
or cntrycd = 'BG'
or cntrycd = 'CH'
or cntrycd = 'CY'
or cntrycd = 'CZ'
or cntrycd = 'DE'
or cntrycd = 'DK'
or cntrycd = 'EE'
or cntrycd = 'ES'
or cntrycd = 'FI'
or cntrycd = 'FR'
or cntrycd = 'GB'
or cntrycd = 'GR'
or cntrycd = 'HU'
or cntrycd = 'IE'
or cntrycd = 'IS'
or cntrycd = 'IT'
or cntrycd = 'LT'
or cntrycd = 'LU'
or cntrycd = 'LV'
or cntrycd = 'MT'
or cntrycd = 'NL'
or cntrycd = 'NO'
or cntrycd = 'PL'
or cntrycd = 'PT'
or cntrycd = 'RO'
or cntrycd = 'SE'
or cntrycd = 'SI'
or cntrycd = 'SK'
or substring(primaryexchgcd,1,2) = 'AT'
or substring(primaryexchgcd,1,2) = 'BE'
or substring(primaryexchgcd,1,2) = 'BG'
or substring(primaryexchgcd,1,2) = 'CH'
or substring(primaryexchgcd,1,2) = 'CY'
or substring(primaryexchgcd,1,2) = 'CZ'
or substring(primaryexchgcd,1,2) = 'DE'
or substring(primaryexchgcd,1,2) = 'DK'
or substring(primaryexchgcd,1,2) = 'EE'
or substring(primaryexchgcd,1,2) = 'ES'
or substring(primaryexchgcd,1,2) = 'FI'
or substring(primaryexchgcd,1,2) = 'FR'
or substring(primaryexchgcd,1,2) = 'GB'
or substring(primaryexchgcd,1,2) = 'GR'
or substring(primaryexchgcd,1,2) = 'HU'
or substring(primaryexchgcd,1,2) = 'IE'
or substring(primaryexchgcd,1,2) = 'IS'
or substring(primaryexchgcd,1,2) = 'IT'
or substring(primaryexchgcd,1,2) = 'LT'
or substring(primaryexchgcd,1,2) = 'LU'
or substring(primaryexchgcd,1,2) = 'LV'
or substring(primaryexchgcd,1,2) = 'MT'
or substring(primaryexchgcd,1,2) = 'NL'
or substring(primaryexchgcd,1,2) = 'NO'
or substring(primaryexchgcd,1,2) = 'PL'
or substring(primaryexchgcd,1,2) = 'PT'
or substring(primaryexchgcd,1,2) = 'RO'
or substring(primaryexchgcd,1,2) = 'SE'
or substring(primaryexchgcd,1,2) = 'SI'
or substring(primaryexchgcd,1,2) = 'SK')

--# 3
use WCA
SELECT
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
WHERE 
sedol.actflag<>'D'
and (cntrycd = 'AT'
or cntrycd = 'BE'
or cntrycd = 'BG'
or cntrycd = 'CH'
or cntrycd = 'CY'
or cntrycd = 'CZ'
or cntrycd = 'DE'
or cntrycd = 'DK'
or cntrycd = 'EE'
or cntrycd = 'ES'
or cntrycd = 'FI'
or cntrycd = 'FR'
or cntrycd = 'GB'
or cntrycd = 'GR'
or cntrycd = 'HU'
or cntrycd = 'IE'
or cntrycd = 'IS'
or cntrycd = 'IT'
or cntrycd = 'LT'
or cntrycd = 'LU'
or cntrycd = 'LV'
or cntrycd = 'MT'
or cntrycd = 'NL'
or cntrycd = 'NO'
or cntrycd = 'PL'
or cntrycd = 'PT'
or cntrycd = 'RO'
or cntrycd = 'SE'
or cntrycd = 'SI'
or cntrycd = 'SK')


--# 4
use WCA
SELECT
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
left outer join exchg on scexh.exchgcd = exchg.exchgcd
WHERE 
scexh.actflag<>'D'
and (cntrycd = 'AT'
or cntrycd = 'BE'
or cntrycd = 'BG'
or cntrycd = 'CH'
or cntrycd = 'CY'
or cntrycd = 'CZ'
or cntrycd = 'DE'
or cntrycd = 'DK'
or cntrycd = 'EE'
or cntrycd = 'ES'
or cntrycd = 'FI'
or cntrycd = 'FR'
or cntrycd = 'GB'
or cntrycd = 'GR'
or cntrycd = 'HU'
or cntrycd = 'IE'
or cntrycd = 'IS'
or cntrycd = 'IT'
or cntrycd = 'LT'
or cntrycd = 'LU'
or cntrycd = 'LV'
or cntrycd = 'MT'
or cntrycd = 'NL'
or cntrycd = 'NO'
or cntrycd = 'PL'
or cntrycd = 'PT'
or cntrycd = 'RO'
or cntrycd = 'SE'
or cntrycd = 'SI'
or cntrycd = 'SK')


--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
where exchg.actflag<>'D'
