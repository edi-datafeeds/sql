--filepath=o:\Datafeed\Equity\620_QuadCap\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_US_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\equity\620_QuadCap\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefooterchk=y


--# 1
use wca2
SELECT * 
FROM t620_Company_Meeting 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')


ORDER BY CaRef

--# 2
use wca2
SELECT *
FROM t620_Call
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 3
use wca
SELECT * 
FROM v50f_620_Liquidation_Extended
WHERE
changed>(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 3)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 4
use wca2
SELECT *
FROM t620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 5
use wca2
SELECT * 
FROM t620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 6
use wca2
SELECT * 
FROM t620_Conversion_Terms
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 7
use wca2
SELECT * 
FROM t620_Redemption_Terms
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 8
use wca2
SELECT * 
FROM t620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 9
use wca2
SELECT * 
FROM t620_Lot_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM t620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 11
use wca2
SELECT * 
FROM t620_Buy_Back
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 12
use wca2
SELECT * 
FROM t620_Capital_Reduction
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 13
use wca2
SELECT * 
FROM t620_Takeover
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 14
use wca2
SELECT * 
FROM t620_Arrangement
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 15
use wca2
SELECT * 
FROM t620_Bonus
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 17
use wca2
SELECT * 
FROM t620_Consolidation
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 18
use wca2
SELECT * 
FROM t620_Demerger
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 19
use wca2
SELECT * 
FROM t620_Distribution
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 20
use wca2
SELECT * 
FROM t620_Divestment
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 21
use wca2
SELECT * 
FROM t620_Entitlement
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 22
use wca
SELECT * 
FROM v54f_620_Merger_extended
WHERE
changed>(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 3)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 23
use wca2
SELECT * 
FROM t620_Preferential_Offer
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 24
use wca2
SELECT * 
FROM t620_Purchase_Offer
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 25
use wca2
SELECT * 
FROM t620_Rights 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 26
use wca2
SELECT * 
FROM t620_Security_Swap 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM t620_Subdivision
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 28
use wca2
SELECT *
FROM t620_Bankruptcy 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 29
use wca2
SELECT *
FROM t620_Financial_Year_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 30
use wca2
SELECT *
FROM t620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM t620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 32
use wca2
SELECT *
FROM t620_Class_Action
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 34
use wca2
SELECT *
FROM t620_Assimilation
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM t620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM t620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 38
use wca2
SELECT * 
FROM t620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 39
use wca2
SELECT * 
FROM t620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 40
use wca2
SELECT * 
FROM t620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 41
use wca2
SELECT * 
FROM t620_Return_of_Capital 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef


--# 42
use wca2
SELECT * 
FROM t620_Dividend
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 43
use wca2
SELECT * 
FROM t620_Dividend_Reinvestment_Plan
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 44
use wca2
SELECT * 
FROM t620_Franking
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 45
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
WHERE
(SecStatus = 'X') 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620_Bonus_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='CL'
  or excountry='CN'
  or excountry='CO'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='ID'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='MY'
  or excountry='MX'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SA'
  or excountry='SE'
  or excountry='SG'
  or excountry='TH'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef
