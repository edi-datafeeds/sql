--filepath=o:\Datafeed\Equity\635\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.635
--suffix=
--fileheadertext=EDI_STATIC_635_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\635\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT 
scexh.ScexhID,
scexh.Acttime as Changed,
scexh.AnnounceDate as Created,
case when scmst.Actflag='D' then 'D' WHEN scexh.AnnounceDate>GETDATE()-5 THEN 'I' ELSE 'U' END as Actflag,
scmst.IssID,
scmst.SecID,
issur.CntryofIncorp,
issur.Issuername,
scmst.Securitydesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.Isin,
scmst.Uscode,
scmst.Voting,
case when scmst.Statusflag is not null then scmst.Statusflag else 'A' end as SecStatus,
scmst.PrimaryExchgCD,
scmst.SectyCD,
substring(scexh.Exchgcd,1,2) as ExCountry,
case when sedol.actflag <> 'D' then Sedol.Sedol
else '' end as Sedol,
scexh.Exchgcd,
exchg.Mic,
scexh.Localcode,
CASE WHEN (Scexh.ListStatus IS NULL) or (Scexh.ListStatus='') THEN 'L' ELSE Scexh.ListStatus END as ListStatus,
scexh.ListDate,
scexh.Lot
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN cntry ON issur.CntryofIncorp = cntry.cntryCD
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                 AND exchg.cntryCD = Sedol.CntryCD
where
(scmst.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or exchg.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or scexh.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or sedol.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or issur.acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and (scmst.sectycd='DR'
or scmst.sectycd='EQS'
or scmst.sectycd='WAR'
or scmst.sectycd='PRF'
or scmst.sectycd='RDS'
or scmst.sectycd='UNT')
and (scmst.PrimaryExchgCD=SCEXH.ExchgCD or
scmst.PrimaryExchgCD IS NULL OR scmst.PrimaryExchgCD='')
