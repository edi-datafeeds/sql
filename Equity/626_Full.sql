--filepath=o:\Datafeed\Equity\626\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.626
--suffix=
--fileheadertext=EDI_STATIC_626_
--fileheaderdate=yymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\626\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
issur.Actflag,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
where actflag<>'D'

--# 2
use WCA
SELECT
scmst.Actflag,
scmst.Acttime,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.FYENPPDate,
scmst.USCode,
scmst.ISIN
from scmst
where scmst.actflag<>'D'
and (scmst.sectycd = 'EQS'
or scmst.sectycd = 'PRF'
or scmst.sectycd = 'DR')

--# 3
use WCA
SELECT
sedol.Actflag,
sedol.Acttime,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM SEDOL
LEFT OUTER JOIN SCMST ON SEDOL.SECID = SCMST.SECID
where sedol.actflag<>'D'
and (scmst.sectycd = 'EQS'
or scmst.sectycd = 'PRF'
or scmst.sectycd = 'DR')


--# 4
use WCA
SELECT
scexh.Actflag,
scexh.Acttime,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
'' as TradeStatus,
scexh.LocalCode,
scexh.ScExhID
FROM scexh
LEFT OUTER JOIN SCMST ON SCEXH.SECID = SCMST.SECID
where scexh.actflag<>'D'
and (scmst.sectycd = 'EQS'
or scmst.sectycd = 'PRF'
or scmst.sectycd = 'DR')

--# 5
SELECT
SecTy.Actflag,
SecTy.Acttime,
upper('SECTYPE') as TypeGroup,
SecTy.SectyCD,
SecTy.SecurityDescriptor
from SECTY

--# 6
SELECT
Exchg.Actflag,
Exchg.Acttime,
upper('EXCHANGE') as TypeGroup,
Exchg.ExchgCD as Code,
Exchg.Exchgname as Lookup
from EXCHG

--# 7
SELECT
upper('I') as Actflag,
'2002/01/01' as Acttime,
upper('LSTATSTAT') as TypeGroup,
irLstatStat.Code,
irLstatStat.Lookup
from irLstatStat

--# 8
SELECT
Indus.Actflag,
Indus.Acttime,
upper('INDUS') as TypeGroup,
Indus.IndusID,
Indus.Indusname
from Indus

