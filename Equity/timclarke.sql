--filepath=o:\Datafeed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use ipo
SELECT 
Created,
Modified,
ID,
Status,
Actflag,
Issuer,
CntryInc,
ExchgCd,
ISIN,
Sedol,
USCode,
Symbol,
SecDescription,
PVCurrency,
ParValue,
SecType,
LotSize,
SubPeriodFrom,
SubperiodTo,
MinSharesOffered,
MaxSharesOffered,
SharesOutstanding,
Currency,
SharePriceLowest,
SharePriceHighest,
ProposedPrice,
InitialPrice,
InitialTradedVolume,
Underwriter,
DealType,
LawFirm,
TransferAgent,
IndusCode,
FirstTradingDate,
SecLevel,
TOFCurrency,
TotalOfferSize,
Notes
from ipo
where
issuer like 'Minoan%'
or issuer like 'Blinkx%'
or issuer like 'Modern Water%'
or issuer like 'CBay%'
or issuer like 'Silanis%'
or issuer like 'EAG Limited%'
or issuer like 'China Medical System%'

--# 2
use wol
select issuername, cab.displaydate, dir.eventdesc, dir.eventdate from cab
left outer join dir on cab.cabepoch = dir.cabepoch
where
(issuername like 'Minoan%'
or issuername like 'Blinkx%'
or issuername like 'Modern Water%'
or issuername like 'CBay%'
or issuername like 'Silanis%'
or issuername like 'EAG Limited%'
or issuername like 'China Medical System%')
and dir.eventdate is not null


--# 3
use wca
select * from v52f_620_new_listing
where
issuername like 'Minoan%'
or issuername like 'Blinkx%'
or issuername like 'Modern Water%'
or issuername like 'CBay%'
or issuername like 'Silanis%'
or issuername like 'EAG Limited%'
or issuername like 'China Medical System%'
