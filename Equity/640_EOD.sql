--filepath=o:\Datafeed\Equity\640\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--fileextension=.640
--suffix=
--fileheadertext=EDI_STATIC_640_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\640\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select distinct 
wca.dbo.scmst.isin,
wca.dbo.scmst.CurenCD,
case when sedol.actflag<>'D' and sedol.secid is not null then sedol.sedol else '' end as Sedol,
wca.dbo.scmst.securitydesc,
wca.dbo.issur.issuername,
wca.dbo.scexh.exchgcd,
wca.dbo.scexh.localcode,
wca.dbo.scmst.primaryexchgcd,
wca.dbo.scmst.sharesoutstanding,
wca.dbo.issur.indusID

from wca.dbo.scmst
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid

left outer join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd

left outer join wca.dbo.sedol on wca.dbo.scexh.secid = wca.dbo.sedol.secid
                     and wca.dbo.exchg.cntrycd = wca.dbo.sedol.CntryCD
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid
 
where (scmst.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or sedol.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or scexh.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or issur.acttime>= (select max(acttime) from tbl_Opslog where seq = 1))
and (scmst.sectycd<>'TRT')
and (scmst.sectycd<>'DRT')
and (sectygrp.secgrpid=1 or (sectygrp.secgrpid=2 and dprcp.secid is null))
and (exchg.cntrycd = 'AT'
or exchg.cntrycd = 'BE'
or exchg.cntrycd = 'BG'
or exchg.cntrycd = 'CH'
or exchg.cntrycd = 'CY'
or exchg.cntrycd = 'CZ'
or exchg.cntrycd = 'DE'
or exchg.cntrycd = 'DK'
or exchg.cntrycd = 'EE'
or exchg.cntrycd = 'ES'
or exchg.cntrycd = 'FI'
or exchg.cntrycd = 'FR'
or exchg.cntrycd = 'GB'
or exchg.cntrycd = 'GR'
or exchg.cntrycd = 'HU'
or exchg.cntrycd = 'IE'
or exchg.cntrycd = 'IS'
or exchg.cntrycd = 'IT'
or exchg.cntrycd = 'LT'
or exchg.cntrycd = 'LU'
or exchg.cntrycd = 'LV'
or exchg.cntrycd = 'MT'
or exchg.cntrycd = 'NL'
or exchg.cntrycd = 'NO'
or exchg.cntrycd = 'PL'
or exchg.cntrycd = 'PT'
or exchg.cntrycd = 'RO'
or exchg.cntrycd = 'SE'
or exchg.cntrycd = 'SI'
or exchg.cntrycd = 'SK')
