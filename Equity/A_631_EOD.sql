--filepath=o:\Datafeed\Equity\DeShaw_631_632\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.631
--suffix=
--fileheadertext=EDI_REORG_631_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\DeShaw_631_632\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select 
v10s_DIV.Levent,
v10s_DIV.EventID,
v20c_EV_SCMST.SecID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
case when DIVPY.Actflag='D' OR DIVPY.Actflag='C' then DIVPY.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN DIVPY.Approxflag = 'Y' THEN 'Estimated'
     WHEN v10s_DIV.DivPeriodCD = 'FNL' OR v10s_DIV.DivPeriodCD = 'ANL' THEN 'Final'
     WHEN v10s_DIV.DivPeriodCD = 'INT' THEN 'Interim'
     WHEN v10s_DIV.DivPeriodCD = 'SPL' THEN 'Special'
     WHEN v10s_DIV.DivPeriodCD = 'INS' THEN 'Installment'
     WHEN v10s_DIV.DivPeriodCD = 'MEM' THEN 'Memorial'
     WHEN v10s_DIV.DivPeriodCD = 'MEM' THEN 'Memorial'
     WHEN v10s_DIV.DivPeriodCD = 'SUP' THEN 'Supplementary'
     WHEN v10s_DIV.DivPeriodCD = 'CG' THEN 'Capital Gains'
     WHEN v10s_DIV.DivPeriodCD = 'CGL' THEN 'Capital Gains Long Term'
     WHEN v10s_DIV.DivPeriodCD = 'CGS' THEN 'Capital Gains Short Term'
     WHEN v10s_DIV.DivPeriodCD = 'ISC' THEN 'Interest on Capital'
     ELSE 'Regular Cash' END as DivType,
DIVPY.GrossDividend,
DIVPY.NetDividend,
DIVPY.CurenCD,
CASE WHEN (v10s_DIV.Frequency is not null AND v10s_DIV.Frequency <> '')
                    THEN v10s_DIV.Frequency
     WHEN (v10s_DIV.Frequency is null OR v10s_DIV.Frequency = '')
          and (v10s_DIV.DivPeriodCD = 'ANL' or
              v10s_DIV.DivPeriodCD = 'SMA' or
              v10s_DIV.DivPeriodCD = 'QTR' or
              v10s_DIV.DivPeriodCD = 'MNT' or
              v10s_DIV.DivPeriodCD = 'TRM' or
              v10s_DIV.DivPeriodCD = 'IRG' or
              v10s_DIV.DivPeriodCD = 'BIM' or
              v10s_DIV.DivPeriodCD = 'REG') THEN v10s_DIV.DivPeriodCD
     ELSE 'UN' END as Frequency,
RD.Recdate,
CASE WHEN EXDT.Paydate is not null THEN EXDT.Paydate ELSE PEXDT.Paydate END as Paydate
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
where 
(v10s_DIV.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' 
and v20c_EV_dscexh.exchgcd is not null
and DIVPY.OptionID is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and DIVPY.divtype <> 'S'

union

select 
v10s_LIQ.Levent,
v10s_LIQ.EventID,
v20c_EV_SCMST.SecID,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as Changed,
case when v10s_LIQ.Actflag='D' OR v10s_LIQ.Actflag='C' then v10s_LIQ.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
'' as Exdate,
'Liquidation' as DivType,
MPAY.MinPrice as GrossDividend,
'' as NetDividend,
MPAY.CurenCD,
'' as Frequency,
'' as Recdate,
MPAY.Paydate
FROM v10s_LIQ
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
where
(v10s_LIQ.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or MPAY.Paydate >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dSCEXH.secid is not null and v20c_EV_dSCEXH.exchgcd <> ''
and v20c_EV_dSCEXH.exchgcd is not null and v20c_EV_dSCEXH.exchgcd <> '' 
and v20c_EV_dSCEXH.exchgcd is not null
and MPAY.OptionID is not null

union

select 
v10s_RCAP.Levent,
v10s_RCAP.EventID,
v20c_EV_SCMST.SecID,
v10s_RCAP.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_RCAP.Acttime THEN RD.Acttime ELSE v10s_RCAP.Acttime END as [Changed],
case when v10s_RCAP.Actflag='D' OR v10s_RCAP.Actflag='C' then v10s_RCAP.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
v10s_RCAP.EffectiveDate as Exdate,
'Return of Capital' as DivType,
v10s_RCAP.CashBak as GrossDividend,
'' as NetDividend,
v10s_RCAP.CurenCD,
'' as Frequency,
RD.Recdate,
v10s_RCAP.CSPYdate as Paydate
FROM v10s_RCAP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_RCAP.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
where
(v10s_RCAP.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' 
and v20c_EV_dscexh.exchgcd is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

order by divtype, exdate