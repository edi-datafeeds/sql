--filepath=o:\Datafeed\Equity\620i_investt\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620i_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620i_investt\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n

--# 1
use wca2
SELECT *  
FROM t620i_Company_Meeting 
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
use wca2
SELECT * 
FROM t620i_Call
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
use wca2
SELECT *  
FROM t620i_Liquidation
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 4
use wca2
SELECT * 
FROM t620i_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 5
use wca2
SELECT *  
FROM t620i_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 6
use wca2
SELECT *  
FROM t620i_Conversion_Terms
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca2
SELECT *  
FROM t620i_Redemption_Terms
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 8
use wca2
SELECT *  
FROM t620i_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 9
use wca2
SELECT *  
FROM t620i_Lot_Change
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca2
SELECT *  
FROM t620i_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 11
use wca2
SELECT *  
FROM t620i_Buy_Back
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 12
use wca2
SELECT *  
FROM t620i_Capital_Reduction
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 14
use wca2
SELECT *  
FROM t620i_Takeover
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 15
use wca2
SELECT *  
FROM t620i_Arrangement
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 16
use wca2
SELECT *  
FROM t620i_Bonus
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 17
use wca2
SELECT *  
FROM t620i_Bonus_Rights
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 18
use wca2
SELECT *  
FROM t620i_Consolidation
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 19
use wca2
SELECT *  
FROM t620i_Demerger
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 20
use wca2
SELECT *  
FROM t620i_Distribution
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 21
use wca2
SELECT *  
FROM t620i_Divestment
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 22
use wca2
SELECT *  
FROM t620i_Entitlement
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 23
use wca2
SELECT *  
FROM t620i_Merger
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 24
use wca2
SELECT *  
FROM t620i_Preferential_Offer
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 25
use wca2
SELECT *  
FROM t620i_Purchase_Offer
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 26
use wca2
SELECT *  
FROM t620i_Rights 
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 27
use wca2
SELECT *  
FROM t620i_Security_Swap 
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 28
use wca2
SELECT * 
FROM t620i_Subdivision
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 29
use wca2
SELECT * 
FROM t620i_Bankruptcy 
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca2
SELECT * 
FROM t620i_Financial_Year_Change
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca2
SELECT * 
FROM t620i_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca2
SELECT * 
FROM t620i_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT * 
FROM t620i_Class_Action
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 34
use wca2
SELECT * 
FROM t620i_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 35
use wca2
SELECT * 
FROM t620i_Assimilation
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 36
use wca2
SELECT * 
FROM t620i_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620i_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca2
SELECT *  
FROM t620i_New_Listing
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca2
SELECT *  
FROM t620i_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 40
use wca2
SELECT *  
FROM t620i_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 41
use wca2
SELECT *  
FROM t620i_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 42
use wca2
SELECT *  
FROM t620i_Return_of_Capital 
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 43
use wca2
SELECT *  
FROM t620i_Dividend
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 44
use wca2
SELECT *  
FROM t620i_Dividend_Reinvestment_Plan
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 45
use wca2
SELECT *  
FROM t620i_Franking
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620i_Conversion_Terms_Change
WHERE
(ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
