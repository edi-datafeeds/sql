--filepath=o:\Datafeed\Equity\627_GB\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.627
--suffix=
--fileheadertext=EDI_DIVIDEND_627_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\627_GB\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
SELECT *  
FROM t620_Dividend
WHERE
(ExCountry = 'GB')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
