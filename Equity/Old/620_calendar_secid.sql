--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=_deltix
--fileheadertext=EDI_620_ALERT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N 



--# 103
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
(Liquidationdate>='2010/04/01' and Liquidationdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed


--# 105
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed




--# 108
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 110
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 112
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 113
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
((Closedate>='2010/04/01' and Closedate<'2010/06/01')
or (CmAcqdate>='2010/04/01' and CmAcqdate<'2010/06/01')
or (TkovrPaydate>='2010/04/01' and TkovrPaydate<'2010/06/01'))
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 115
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed




--# 117
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 118
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed


--# 119
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 121
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 122
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed


--# 123
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 124
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 125
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed

--# 126
use wca
SELECT * 
FROM v54f_620_Bonus_Rights 
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 127
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 128
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
(NotificationDate>='2010/04/01' and NotificationDate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 131
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(NameChangeDate>='2010/04/01' and NameChangeDate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 133
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
(DateOfChange>='2010/04/01' and DateOfChange<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed


--# 134
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 135
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed




--# 136
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
(Exdate>='2010/04/01' and Exdate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed




--# 137
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed



--# 138
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed

--# 139
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
(Redemptiondate>='2010/04/01' and Redemptiondate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed


--# 140
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
(Effectivedate>='2010/04/01' and Effectivedate<'2010/06/01')
and secid in (select secid from portfolio.dbo.deltix)
and (primaryexchgcd=exchgcd or primaryexchgcd ='')
order by changed
