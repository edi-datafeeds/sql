--filepath=o:\Datafeed\Equity\620_msci_full\
--filenameprefix=new_AL
--filename=yymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT *  
FROM v54f_620_Franking
left outer join lstat on v54f_620_Franking.secid = lstat.secid and v54f_620_Franking.exchgcd = lstat.exchgcd and 'D'= lstat.lstatstatus and v54f_620_Franking.liststatus = 'D'
WHERE 
(CREATED >= '2009/01/01')
and (lstat.effectivedate is null or lstat.effectivedate>=Exdate)
and v54f_620_Franking.actflag<>'D'
and v54f_620_Franking.frankflag<>'N'

