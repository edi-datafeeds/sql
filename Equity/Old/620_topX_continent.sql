--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT top 20 *  
FROM v50f_620_Company_Meeting 
inner join continent on excountry = continent.cntrycd 
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
SELECT top 20 * 
FROM v53f_620_Call 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
SELECT top 20 *  
FROM v50f_620_Liquidation 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA
SELECT top 20 * 
FROM v51f_620_Certificate_Exchange 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
SELECT top 20 *  
FROM v51f_620_International_Code_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
SELECT top 20 *  
FROM v51f_620_Conversion_Terms
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
SELECT top 20 *  
FROM v51f_620_Redemption_Terms
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
SELECT top 20 *  
FROM v51f_620_Security_Reclassification
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
SELECT top 20 *  
FROM v52f_620_Lot_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
SELECT top 20 *  
FROM v52f_620_Sedol_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca
SELECT top 20 *  
FROM v53f_620_Buy_Back
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
SELECT top 20 *  
FROM v53f_620_Capital_Reduction
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
use wca
SELECT top 20 *  
FROM v53f_620_Takeover
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
use wca
SELECT top 20 *  
FROM v54f_620_Arrangement
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
SELECT top 20 *  
FROM v54f_620_Bonus
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
SELECT top 20 *  
FROM v54f_620_Bonus_Rights
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
SELECT top 20 *  
FROM v54f_620_Consolidation
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca
SELECT top 20 *  
FROM v54f_620_Demerger
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
SELECT top 20 *  
FROM v54f_620_Distribution
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
use wca
SELECT top 20 *  
FROM v54f_620_Divestment
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
SELECT top 20 *  
FROM v54f_620_Entitlement
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
use wca
SELECT top 20 *  
FROM v54f_620_Merger
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
use wca
SELECT top 20 *  
FROM v54f_620_Preferential_Offer
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
SELECT top 20 *  
FROM v54f_620_Purchase_Offer
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
SELECT top 20 *  
FROM v54f_620_Rights 
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
SELECT top 20 *  
FROM v54f_620_Security_Swap 
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
SELECT top 20 * 
FROM v54f_620_Subdivision
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
SELECT top 20 * 
FROM v50f_620_Bankruptcy 
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
SELECT top 20 * 
FROM v50f_620_Financial_Year_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
SELECT top 20 * 
FROM v50f_620_Incorporation_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
SELECT top 20 * 
FROM v50f_620_Issuer_Name_change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
SELECT top 20 * 
FROM v50f_620_Class_Action
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
SELECT top 20 * 
FROM v51f_620_Security_Description_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
SELECT top 20 * 
FROM v52f_620_Assimilation
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
SELECT top 20 * 
FROM v52f_620_Listing_Status_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
SELECT top 20 * 
FROM v52f_620_Local_Code_Change
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
SELECT top 20 *  
FROM v52f_620_New_Listing
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
SELECT top 20 *  
FROM v50f_620_Announcement
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
use wca
SELECT top 20 *  
FROM v51f_620_Parvalue_Redenomination 
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
SELECT top 20 *  
FROM v51f_620_Currency_Redenomination 
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
SELECT top 20 *  
FROM v53f_620_Return_of_Capital 
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
SELECT top 20 *  
FROM v54f_620_Dividend
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
USE WCA
SELECT top 20 *  
FROM v54f_620_Dividend_Reinvestment_Plan
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
USE WCA
SELECT top 20 *  
FROM v54f_620_Franking
 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'

ORDER BY EventID desc, ExchgCD, Sedol

--# 46
use wca
SELECT top 20 * 
FROM v51f_620_Conversion_Terms_Change 
inner join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and continent='Africa'
ORDER BY EventID desc, ExchgCD, Sedol
