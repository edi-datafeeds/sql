--filepath=o:\Datafeed\Equity\663\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.663
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_663_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select
SDCHG.SdchgID as EventID,
'Sedol Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
sdchg.Actflag,
sdchg.eventtype as Reason,
case when sdchg.effectivedate is null
     then sdchg.announcedate
     else sdchg.effectivedate
     end as EffectiveDate,
sdchg.OldSedol as OldStatic,
sdchg.NewSedol as NewStatic
FROM SDCHG
INNER JOIN SCMST ON SCMST.SecID = SDCHG.SecID
where
SDCHG.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 3
use wca
select
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic
FROM ICC
INNER JOIN SCMST ON SCMST.SecID = icc.SecID
where
ICC.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (icc.OldIsin <> '' or icc.NewIsin <> '')


--# 4
use wca
select
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
inchg.Actflag,
inchg.eventtype as Reason,
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic
FROM inchg
INNER JOIN SCMST ON SCMST.issid = inchg.issid
where
inchg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 5
use wca
select
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
ischg.Actflag,
ischg.eventtype as Reason,
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic
FROM ischg
INNER JOIN SCMST ON SCMST.issid = ischg.issid
where 
ischg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and scmst.statusflag<>'I'


--# 6
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN SCMST ON SCMST.secid = lcc.secid
where 
lcc.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 7
use wca
select
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
scchg.Actflag,
scchg.eventtype as Reason,
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic
FROM scchg
INNER JOIN SCMST ON SCMST.secid = scchg.secid
where 
scchg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 8
use wca
select
LTCHG.LTCHGID as EventID,
'Lot Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic
FROM ltchg
INNER JOIN SCMST ON SCMST.secid = ltchg.secid
where 
ltchg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (ltchg.OldLot <> ''
or ltchg.NewLot <> '')


--# 9
use wca
select
LTCHG.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldMinTrdQty as OldStatic,
ltchg.NewMinTrdgQty as NewStatic
FROM ltchg
INNER JOIN SCMST ON SCMST.secid = ltchg.secid
where
ltchg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (ltchg.OldMinTrdQty <> ''
or ltchg.NewMinTrdgQty <> '')


--# 10
use wca
select
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic
FROM lstat
INNER JOIN SCMST ON SCMST.secid = lstat.secid
where
lstat.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)



--# 12
use wca
select
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
icc.Actflag,
icc.eventtype as Reason,
case when icc.EffectiveDate is null
     then icc.announcedate
     else icc.effectivedate
     end as EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic
FROM ICC
INNER JOIN SCMST ON SCMST.SecID = icc.SecID
where
ICC.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (icc.OldUSCode <> ''
or icc.NewUSCode <> '')


--# 13
use wca
select
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
SCMST.IssID,
SCMST.SecID,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic
FROM Currd
INNER JOIN SCMST ON SCMST.SecID = currd.SecID
where
currd.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

