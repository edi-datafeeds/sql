--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.627
--suffix=
--fileheadertext=EDI_DIVIDEND_627_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
SELECT *
FROM v54f_620_Dividend
WHERE
exdate<getdate()-1
and paydate>getdate()-1
and v54f_620_Dividend.Actflag<>'D'
and (isin in (select col001 from portfolio.dbo.leggall)
     or sedol in (select col001 from portfolio.dbo.leggall)
     or uscode in (select col001 from portfolio.dbo.leggall))
ORDER BY CaRef desc
