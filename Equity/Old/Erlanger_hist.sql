--filepath=o:\Datafeed\Equity\Erlanger\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 17
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and changed>'2009/10/01'
ORDER BY CaRef

--# 27
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and changed>'2009/10/01'
ORDER BY CaRef

--# 31
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2009/10/01'
ORDER BY CaRef

--# 35
use wca
SELECT *
FROM v52f_620_Listing_Status_Change
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and changed>'2009/10/01'
ORDER BY CaRef

--# 36
use wca
SELECT *
FROM v52f_620_Local_Code_Change
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and changed>'2009/10/01'
ORDER BY CaRef

--# 37
use wca
SELECT * 
FROM v52f_620_New_Listing
WHERE
(exchgcd = 'USNYSE' or exchgcd = 'USNASD' or exchgcd = 'USAMEX')
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2009/10/01'
ORDER BY CaRef
