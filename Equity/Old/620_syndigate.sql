--filepath=o:\Datafeed\Equity\620_syndigate\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
SELECT *  
FROM t620_Company_Meeting 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
use wca2
SELECT * 
FROM t620_Call
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
use wca2
SELECT *  
FROM t620_Liquidation
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
use wca2
SELECT * 
FROM t620_Certificate_Exchange
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
use wca2
SELECT *  
FROM t620_International_Code_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
use wca2
SELECT *  
FROM t620_Conversion_Terms
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca2
SELECT *  
FROM t620_Redemption_Terms
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca2
SELECT *  
FROM t620_Security_Reclassification
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca2
SELECT *  
FROM t620_Lot_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca2
SELECT *  
FROM t620_Sedol_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca2
SELECT *  
FROM t620_Buy_Back
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca2
SELECT *  
FROM t620_Capital_Reduction
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
use wca2
SELECT *  
FROM t620_Takeover
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
use wca2
SELECT *  
FROM t620_Arrangement
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca2
SELECT *  
FROM t620_Bonus
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca2
SELECT *  
FROM t620_Bonus_Rights
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca2
SELECT *  
FROM t620_Consolidation
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca2
SELECT *  
FROM t620_Demerger
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca2
SELECT *  
FROM t620_Distribution
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
use wca2
SELECT *  
FROM t620_Divestment
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca2
SELECT *  
FROM t620_Entitlement
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
use wca2
SELECT *  
FROM t620_Merger
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
use wca2
SELECT *  
FROM t620_Preferential_Offer
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca2
SELECT *  
FROM t620_Purchase_Offer
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca2
SELECT *  
FROM t620_Rights 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca2
SELECT *  
FROM t620_Security_Swap 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca2
SELECT * 
FROM t620_Subdivision
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca2
SELECT * 
FROM t620_Bankruptcy 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca2
SELECT * 
FROM t620_Financial_Year_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca2
SELECT * 
FROM t620_Incorporation_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca2
SELECT * 
FROM t620_Issuer_Name_change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca2
SELECT * 
FROM t620_Class_Action
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca2
SELECT * 
FROM t620_Security_Description_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca2
SELECT * 
FROM t620_Assimilation
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca2
SELECT * 
FROM t620_Listing_Status_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca2
SELECT * 
FROM t620_Local_Code_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca2
SELECT *  
FROM t620_New_Listing
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca2
SELECT *  
FROM t620_Announcement
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
use wca2
SELECT *  
FROM t620_Parvalue_Redenomination 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
use wca2
SELECT *  
FROM t620_Currency_Redenomination 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
use wca2
SELECT *  
FROM t620_Return_of_Capital 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
use wca2
SELECT *  
FROM t620_Dividend 
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
use wca2
SELECT *  
FROM t620_Dividend_Reinvestment_Plan
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
use wca2
SELECT *  
FROM t620_Franking
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol

--# 46
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
WHERE CHANGED >= (select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3)
and (excountry='AE'
or excountry='BH'
or excountry='IL'
or excountry='IQ'
or excountry='IR'
or excountry='JO'
or excountry='KW'
or excountry='LB'
or excountry='PS'
or excountry='QA'
or excountry='SA'
or excountry='YE')
ORDER BY EventID desc, ExchgCD, Sedol
