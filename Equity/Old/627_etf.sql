--filepath=o:\Datafeed\Equity\627_full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
--fileextension=.627
--suffix=
--fileheadertext=EDI_DIVIDEND_627_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select *
from v54f_620_dividend
where 
v54f_620_dividend.actflag <> 'd'
and v54f_620_dividend.secid in (select secid from portfolio.dbo.etf)