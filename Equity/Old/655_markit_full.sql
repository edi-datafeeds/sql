--filepath=o:\Datafeed\Equity\655_Markit_full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.655
--suffix=
--fileheadertext=EDI_DR_655_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\655\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT distinct
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
where actflag = 'Q'


--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.statusflag<>'I' OR scmst.statusflag is null then 'A' else 'I' end as statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.X as CommonCode,
scmst.SharesOutstanding
FROM scmst
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join dprcp as dr on scmst.secid = dr.secid
where
scmst.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and scmst.actflag<>'D'
and scexh.actflag<>'D'
and (scmst.sectycd = 'DR' or dr.secid is not null)


--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.cntrycd,
sedol.Sedol,
sedol.Defunct,
sedol.Rcntrycd,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join dprcp as dr on scmst.secid = dr.secid
where 
scmst.actflag<>'D'
and sedol.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and (scmst.sectycd = 'DR' or dr.secid is not null)


--# 4
use WCA
SELECT distinct
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.LocalCode,
scmst.ISIN,
scmst.CurenCD as ScmstCurenCD,
exchg.MIC
FROM scexh
inner join scmst on scexh.secid = scmst.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join dprcp as dr on scmst.secid = dr.secid
where
scmst.actflag<>'D'
and scexh.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and (scmst.sectycd = 'DR' or dr.secid is not null)

--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.cntrycd,
Exchg.MIC
from EXCHG
where
exchg.actflag<>'D'


--# 6
use WCA
SELECT
upper('DPRCP') as Tablename,
DPRCP.Actflag,
DPRCP.Acttime,
DPRCP.Announcedate,
DPRCP.SecID,
DPRCP.UnSecID,
DPRCP.DRRatio,
DPRCP.USRatio,
DPRCP.SpnFlag,
DPRCP.DRtype,
DPRCP.DepBank,
DPRCP.LevDesc,
DPRCP.OtherDepBank
from DPRCP
inner join scmst on dprcp.secid = scmst.secid
where
DPRCP.actflag<>'D'
and SCMST.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and scmst.secid is not null
