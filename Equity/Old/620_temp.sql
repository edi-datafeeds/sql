--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 5
use wca2
SELECT * 
FROM tt620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM tt620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM tt620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM tt620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM tt620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM tt620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM tt620_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

