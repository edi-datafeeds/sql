--filepath=o:\Datafeed\Equity\645_full_aim\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.645
--suffix=
--fileheadertext=EDI_STATIC_645_AIM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\645_aim\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT DISTINCT
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
inner join scmst on issur.issid = scmst.issid
left outer join scexh on scmst.secid = scexh.secid
where (exchgcd='GBLSE' or exchgcd='GBOFX')
and scexh.actflag<>'D'
and ListStatus<>'D'
and issur.actflag<>'D'


--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM SCMST
left outer join scexh on scmst.secid = scexh.secid
where (exchgcd='GBLSE' or exchgcd='GBOFX')
and scmst.actflag<>'D'
and statusflag<>'I'
and scexh.actflag<>'D'
and ListStatus<>'D'


--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolID
FROM sedol
left outer join scexh on sedol.secid = scexh.secid
where (exchgcd='GBLSE' or exchgcd='GBOFX')
and scexh.actflag<>'D'
and ListStatus<>'D'


--# 4
use WCA
SELECT
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
case when scexh.ListStatus='R' or scexh.ListStatus='S' or scexh.ListStatus='D' then scexh.ListStatus else 'L' end as ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
left outer join portfolio.dbo.aimticker on scexh.localcode = portfolio.dbo.aimticker.ticker
where exchgcd='GBLSE' or exchgcd='GBOFX'
and scexh.actflag<>'D'
and ListStatus<>'D'
and (ticker is null or (portfolio.dbo.aimticker.actflag='D'
      and portfolio.dbo.aimticker.acttime = (select max(portfolio.dbo.aimticker.acttime) from portfolio.dbo.aimticker)))

union

SELECT
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
'GBAIM' as ExchgCD,
case when scexh.ListStatus='R' or scexh.ListStatus='S' or scexh.ListStatus='D' then scexh.ListStatus else 'L' end as ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM portfolio.dbo.aimticker
inner join scexh on portfolio.dbo.aimticker.ticker = scexh.localcode
WHERE
(exchgcd='GBLSE' or exchgcd='GBOFX')
and portfolio.dbo.aimticker.actflag<>'D'

order by Exchgcd, scexh.SecID
