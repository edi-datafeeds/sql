--filepath=o:\Datafeed\Equity\659\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.659
--suffix=
--fileheadertext=EDI_REORG_659_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\659\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 42
use wca2
SELECT * 
FROM t620_Dividend
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
