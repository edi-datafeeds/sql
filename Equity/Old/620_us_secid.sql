--filepath=o:\Datafeed\Equity\
--filenameprefix=
--filename=US_SecID_Extract
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCAHIST
SELECT *  
FROM v50f_620_Company_Meeting 
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCAHIST
SELECT * 
FROM v53f_620_Call
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCAHIST
SELECT *  
FROM v50f_620_Liquidation
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCAHIST
SELECT * 
FROM v51f_620_Certificate_Exchange
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCAHIST
SELECT *  
FROM v51f_620_International_Code_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCAHIST
SELECT *  
FROM v51f_620_Conversion_Terms
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
USE WCAHIST
SELECT *  
FROM v51f_620_Redemption_Terms
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
USE WCAHIST
SELECT *  
FROM v51f_620_Security_Reclassification
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
USE WCAHIST
SELECT *  
FROM v52f_620_Lot_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
USE WCAHIST
SELECT *  
FROM v52f_620_Sedol_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
USE WCAHIST
SELECT *  
FROM v53f_620_Buy_Back
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
USE WCAHIST
SELECT *  
FROM v53f_620_Capital_Reduction
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
USE WCAHIST
SELECT *  
FROM v53f_620_Takeover
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
USE WCAHIST
SELECT *  
FROM v54f_620_Arrangement
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
USE WCAHIST
SELECT *  
FROM v54f_620_Bonus
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
USE WCAHIST
SELECT *  
FROM v54f_620_Bonus_Rights
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
USE WCAHIST
SELECT *  
FROM v54f_620_Consolidation
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
USE WCAHIST
SELECT *  
FROM v54f_620_Demerger
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
USE WCAHIST
SELECT *  
FROM v54f_620_Distribution
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
USE WCAHIST
SELECT *  
FROM v54f_620_Divestment
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
USE WCAHIST
SELECT *  
FROM v54f_620_Entitlement
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
USE WCAHIST
SELECT *  
FROM v54f_620_Merger
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
USE WCAHIST
SELECT *  
FROM v54f_620_Preferential_Offer
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
USE WCAHIST
SELECT *  
FROM v54f_620_Purchase_Offer
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
USE WCAHIST
SELECT *  
FROM v54f_620_Rights 
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
USE WCAHIST
SELECT *  
FROM v54f_620_Security_Swap 
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
USE WCAHIST
SELECT * 
FROM v54f_620_Subdivision
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
USE WCAHIST
SELECT * 
FROM v50f_620_Bankruptcy 
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
USE WCAHIST
SELECT * 
FROM v50f_620_Financial_Year_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
USE WCAHIST
SELECT * 
FROM v50f_620_Incorporation_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
USE WCAHIST
SELECT * 
FROM v50f_620_Issuer_Name_change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
USE WCAHIST
SELECT * 
FROM v50f_620_Class_Action
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
USE WCAHIST
SELECT * 
FROM v51f_620_Security_Description_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
USE WCAHIST
SELECT * 
FROM v52f_620_Assimilation
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
USE WCAHIST
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
USE WCAHIST
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
USE WCAHIST
SELECT *  
FROM v52f_620_New_Listing
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
USE WCAHIST
SELECT *  
FROM v50f_620_Announcement
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
USE WCAHIST
SELECT *  
FROM v51f_620_Parvalue_Redenomination 
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCAHIST
SELECT *  
FROM v51f_620_Currency_Redenomination 
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCAHIST
SELECT *  
FROM v53f_620_Return_of_Capital 
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCAHIST
SELECT *  
FROM v54f_620_Dividend
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
USE WCAHIST
SELECT *  
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
USE WCAHIST
SELECT *  
FROM v54f_620_Franking
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)

ORDER BY EventID desc, ExchgCD, Sedol

--# 46
USE WCAHIST
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE
CREATED >='2007/01/01'
and excountry='US'
and (secid=34390 or secid=2221416 or secid=3002597 or secid=273222 or secid=41430)
ORDER BY EventID desc, ExchgCD, Sedol
