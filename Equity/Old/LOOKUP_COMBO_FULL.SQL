--filepath=o:\Datafeed\Equity\625\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.625
--suffix=_LOOKUP
--fileheadertext=EDI_LOOKUP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\WCA\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
lookup.Actflag,
lookup.Acttime,
lookup.TypeGroup,
lookup.Code,
lookup.Lookup
FROM LOOKUP

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'ACTION' as TypeGroup,
irAction.Code,
irAction.Lookup
from irAction

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'DIVPERIOD' as TypeGroup,
irDivperiod.Code,
irDivperiod.Lookup
from irDivperiod

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'EXSTYLE' as TypeGroup,
irExstyle.Code,
irExstyle.Lookup
from irExstyle

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'FRACTIONS' as TypeGroup,
irFractions.Code,
irFractions.Lookup
from irFractions

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'HOSTILE' as TypeGroup,
irHostile.Code,
irHostile.Lookup
from irHostile

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'LSTATSTAT' as TypeGroup,
irLstatStat.Code,
irLstatStat.Lookup
from irLstatStat

union

SELECT
'I' as Actflag,
'2006/01/26' as AnnounceDate,
'MANDOPT' as TypeGroup,
irMandopt.Code,
irMandopt.Lookup
from irMandopt

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'MRGRSTAT' as TypeGroup,
irMrgrStat.Code,
irMrgrStat.Lookup
from irMrgrStat

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'ONOFFMARKET' as TypeGroup,
irOnOffMarket.Code,
irOnOffMarket.Lookup
from irOnOffMarket

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'PAYTYPE' as TypeGroup,
irPaytype.Code,
irPaytype.Lookup
from irPaytype

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'SCMSTSTAT' as TypeGroup,
irScmstStat.Code,
irScmstStat.Lookup
from irScmstStat

union

SELECT
'I' as Actflag,
'2006/01/26' as Acttime,
'TKOVRSTAT' as TypeGroup,
irTkovrStat.Code,
irTkovrStat.Lookup
from irTkovrStat

order by TypeGroup, Code