--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.660
--suffix=_NO
--fileheadertext=EDI_660_SRF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select distinct
SCMST.SecID,
case when SCEXH.ScexhID is null then 0 else SCEXH.ScexhID end as ScexhID,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.Sedol else '' end as Sedol,
case when scmst.statusflag='' then 'A' else scmst.statusflag end as IssueStatus,
SCEXH.ListStatus,
case when scexh.actflag is not null then scexh.actflag else scmst.actflag end as RecordStatus,
SCMST.IssID,
scmst.Isin as ISIN,
scmst.Uscode as UScode,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.Sedol else '' end as Sedol,
SCEXH.Localcode,
ISSUR.IssuerName,
ISSUR.CntryofIncorp,
ISSUR.IndusID,
SCMST.SecurityDesc,
SCMST.SectyCD,
scmst.Parvalue,
scmst.CurenCD as ParvalueCurrency,
SCMST.StructCD,
SCMST.PrimaryExchgCD,
SCEXH.ExchgCD,
SCEXH.ListDate,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.CurenCD else '' end as SedolCurrency,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.RcntryCD else '' end as RegisterCountry
FROM scmst
inner JOIN scexh ON scmst.SecID = scexh.SecID
left outer join ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join sedol on scexh.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
where
grpname='equity'
and exchg.cntryCD='NO'
and scmst.actflag<>'D' 
and scexh.actflag<>'D'
and issur.actflag<>'D'
