--filepath=o:\Datafeed\Equity\645_US\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where feeddate = '2010/03/19' and seq = 3
--fileextension=.645
--suffix=
--fileheadertext=EDI_STATIC_645_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT distinct
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
left outer join scmst on issur.issid = scmst.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE issur.acttime between '2010/03/19' and '2010/03/20'
and scmst.sectycd<>'BND'
and sectygrp.secgrpid<3
and substring(scexh.exchgcd,1,2)='US'


--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM scmst
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join scexh on scmst.secid = scexh.secid
WHERE scmst.acttime between '2010/03/19' and '2010/03/20'
and sectygrp.secgrpid<3
and scmst.sectycd<>'BND'
and substring(scexh.exchgcd,1,2)='US'

--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join scexh on sedol.secid = scexh.secid
WHERE sedol.acttime between '2010/03/19' and '2010/03/20'
and scmst.sectycd <> 'BND'
and sectygrp.secgrpid<3
and substring(scexh.exchgcd,1,2)='US'


--# 4
use WCA
SELECT distinct
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
inner join scmst on scexh.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE scexh.acttime between '2010/03/19' and '2010/03/20'
and scmst.sectycd <> 'BND'
and sectygrp.secgrpid<3
and substring(scexh.exchgcd,1,2)='US'


--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
WHERE acttime between '2010/03/19' and '2010/03/20'
and substring(exchgcd,1,2)='US'

