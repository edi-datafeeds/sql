--filepath=o:\Datafeed\Equity\638\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.638
--suffix=
--fileheadertext=EDI_REORG_638_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
use wca2
SELECT * 
FROM t620_Takeover
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
