--filepath=o:\upload\acc\170\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.629
--suffix=
--fileheadertext=EDI_ALERT_629_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 103
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Liquidationdate>=getdate()+1 and Liquidationdate<getdate()+2)


--# 105
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)




--# 108
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)



--# 110
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)



--# 112
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)



--# 113
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and ((Closedate>=getdate()+1 and Closedate<getdate()+2)
or (CmAcqdate>=getdate()+1 and CmAcqdate<getdate()+2)
or (TkovrPaydate>=getdate()+1 and TkovrPaydate<getdate()+2))



--# 115
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)




--# 117
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)



--# 118
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)


--# 119
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)



--# 121
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)



--# 122
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)


--# 123
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)



--# 124
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)



--# 125
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)

--# 126
use wca
SELECT * 
FROM v54f_620_Bonus_Rights 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)



--# 127
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)



--# 128
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (NotificationDate>=getdate()+1 and NotificationDate<getdate()+2)



--# 131
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (NameChangeDate>=getdate()+1 and NameChangeDate<getdate()+2)



--# 133
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (DateOfChange>=getdate()+1 and DateOfChange<getdate()+2)


--# 134
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)



--# 135
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)




--# 136
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (isin in (select isincode from portfolio.dbo.nordea))
and (Exdate>=getdate()+1 and Exdate<getdate()+2)




--# 137
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)



--# 138
use wca
SELECT * 
FROM v52f_620_Lot_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)

--# 139
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (isin in (select isincode from portfolio.dbo.nordea))
and (Redemptiondate>=getdate()+1 and Redemptiondate<getdate()+2)


--# 140
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)



--# 141
use wca
SELECT * 
FROM v51f_612_xShares_Outstanding_Change
WHERE
(isin in (select isincode from portfolio.dbo.nordea))
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+2)
