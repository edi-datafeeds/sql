--filepath=o:\Datafeed\Equity\662\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.662
--suffix=
--fileheadertext=EDI_662_NOTES_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select
'sEvent' as dummy1,
'EventID' as dummy2,
'Notes' as dummy3

--# 2
use wca
SELECT
'CALL' as sEvent,
CallID,
CallNotes
FROM Call
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 3
use wca
SELECT
'LIQ' as sEvent,
LiqID,
LiquidationTerms
FROM LIQ
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 4
use wca
SELECT
'CTX' as sEvent,
CtxID,
CtxNotes
FROM CTX
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)



--# 6
use wca
SELECT
'CONV' as sEvent,
ConvID,
ConvNotes
FROM CONV
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 7
use wca
SELECT
'REDEM' as sEvent,
RedemID,
RedemNotes
FROM REDEM
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 8
use wca
SELECT
'SECRC' as sEvent,
SecrcID,
SecrcNotes
FROM SECRC
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)



--# 10
use wca
SELECT
'SDCHG' as sEvent,
SdchgID,
SdchgNotes
FROM SDCHG
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 11
use wca
SELECT
'BB' as sEvent,
BBID,
BBNotes
FROM BB
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 12
use wca
SELECT
'CAPRD' as sEvent,
CaprdID,
CapRDNotes
FROM CAPRD
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 13
use wca
SELECT
'TKOVR' as sEvent,
TkovrID,
TkovrNotes
FROM TKOVR
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 14
use wca
SELECT
'ARR' as sEvent,
RdID,
ArrNotes
FROM ARR
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 15
use wca
SELECT
'BON' as sEvent,
RdID,
BonNotes
FROM BON
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 17
use wca
SELECT
'CONSD' as sEvent,
RdID,
ConsdNotes
FROM CONSD
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 18
use wca
SELECT
'DMRGR' as sEvent,
RdID,
DmrgrNotes
FROM DMRGR
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 19
use wca
SELECT
'DIST' as sEvent,
RdID,
DistNotes
FROM DIST
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 20
use wca
SELECT
'DVST' as sEvent,
RdID,
DvstNotes
FROM DVST
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 21
use wca
SELECT
'ENT' as sEvent,
RdID,
EntNotes
FROM ENT
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 22
use wca
SELECT
'MRGR' as sEvent,
RdID,
MrgrTerms
FROM MRGR
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 23
use wca
SELECT
'PRF' as sEvent,
RdID,
PrfNotes
FROM PRF
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 24
use wca
SELECT
'PO' as sEvent,
RdID,
PoNotes
FROM PO
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 25
use wca
SELECT
'RTS' as sEvent,
RdID,
RtsNotes
FROM RTS
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 26
use wca
SELECT
'SCSWP' as sEvent,
RdID,
ScswpNotes
FROM SCSWP
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 27
use wca
SELECT
'SD' as sEvent,
RdID,
SDNotes
FROM SD
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 28
use wca
SELECT
'BKRP' as sEvent,
BkrpID,
BkrpNotes
FROM BKRP 
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 31
use wca
SELECT
'ISCHG' as sEvent,
IschgID,
IschgNotes
FROM ISCHG
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 32
use wca
SELECT
'LAWST' as sEvent,
LawstID,
LawstNotes
FROM LAWST
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 33
use wca
SELECT
'SCCHG' as sEvent,
ScchgID,
ScchgNotes
FROM SCCHG
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 35
use wca
SELECT
'LSTAT' as sEvent,
LstatID,
Reason
FROM LSTAT
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 38
use wca
SELECT
'ANN' as sEvent,
AnnID,
AnnNotes
FROM ANN
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 39
use wca
SELECT
'PVRD' as sEvent,
PvrdID,
PvrdNotes
FROM PVRD
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 40
use wca
SELECT
'CURRD' as sEvent,
CurrdID,
CurrdNotes
FROM CURRD
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 41
use wca
SELECT
'RCAP' as sEvent,
RcapID,
RcapNotes
FROM RCAP
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 42
use wca
SELECT
'DIVPY' as sEvent,
RdID,
DivNotes
FROM DIV
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 46
use wca
SELECT
'BR' as sEvent,
RdID,
BRNotes
FROM BR
WHERE
acttime>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
