--filepath=o:\Datafeed\Equity\Quotemedia_listdate\
--filenameprefix=Listdata_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_LISTDATA_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select distinct
SCMST.Isin,
SCMST.SecID,
scmst.Isin,
ISSUR.Issuername,
ISSUR.CntryofIncorp,
SCMST.SecurityDesc,
SCMST.PrimaryExchgCD,
SCEXH.ExchgCD,
SCEXH.Localcode,
scexh.listdate,
SCEXH.Liststatus,
lstat.effectivedate as ListStatusChangeDate
FROM scexh
INNER JOIN SCMST ON SCEXH.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join lstat on scexh.secid = lstat.secid and lstat.exchgcd = scexh.exchgcd
where
scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and (scmst.issid = 21875
or scmst.issid = 30980
or scmst.issid = 75564
or scmst.issid = 62176
or scmst.issid = 24533
or scmst.issid = 47218
or scmst.issid = 65231
or scmst.issid = 45861)
