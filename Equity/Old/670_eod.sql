--filepath=o:\Datafeed\Equity\670\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.670
--suffix=
--fileheadertext=EDI_FLATSRF_670_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
issur.actflag as issur_actflag,
issur.issid, 
issur.issuername,
issur.cntryofincorp,
scmst.actflag as scmst_actflag,
scmst.secid,
scmst.sectycd,
scmst.securitydesc,
case when scmst.statusflag<>'i' then 'a' else 'i' end as statusflag,
scmst.primaryexchgcd,
scmst.curencd as parvalue_curencd,
case when sedol.secid is not null and sedol.actflag<>'d' and sedol.defunct<>'t' then sedol.cntrycd else '' end as sedol_cntrycd,
case when sedol.secid is not null and sedol.actflag<>'d' and sedol.defunct<>'t' then sedol.rcntrycd else '' end as sedol_rcntrycd,
scexh.actflag as scexch_actflag,
scexh.exchgcd,
case when scexh.liststatus is null then 'u' when scexh.liststatus='n' or scexh.liststatus='' then 'l' else scexh.liststatus end as wcaliststatus,
scexh.localcode,
exchg.cntrycd as exchg_cntrycd,
exchg.mic
from scmst
inner join issur on scmst.issid = issur.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scexh.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
where 
(scmst.acttime >= (select max(acttime) from wca.dbo.tbl_opslog where seq = 1)
or issur.acttime >= (select max(acttime) from wca.dbo.tbl_opslog where seq = 1)
or scexh.acttime >= (select max(acttime) from wca.dbo.tbl_opslog where seq = 1))
and scmst.sectycd<>'BND'
and scmst.sectycd<>'CW'

