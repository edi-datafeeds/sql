--filepath=o:\Datafeed\Equity\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.658
--suffix=
--fileheadertext=EDI_658_LISTDATA_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select distinct
SCMST.SecID,
SCMST.IssID,
scmst.Isin,
scmst.Uscode,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.Sedol else '' end as Sedol,
ISSUR.IssuerName,
ISSUR.CntryofIncorp,
SCMST.SecurityDesc,
SCMST.SectyCD,
SCMST.PrimaryExchgCD,
SCEXH.ExchgCD,
SCEXH.LocalCode,
scexh.listDate,
SCEXH.ListStatus,
case when scexh.actflag is not null then scexh.actflag else scmst.actflag end as RecordStatus
FROM scexh
INNER JOIN SCMST ON SCEXH.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join sedol on scexh.secid = sedol.secid and substring(scexh.exchgcd,1,2) = sedol.cntrycd
where
scmst.actflag<>'D' 
and scexh.actflag<>'D'
and issur.actflag<>'D'
and SCEXH.ExchgCD='UAPFTS'

