--filepath=o:\Datafeed\Equity\652\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.652
--suffix=
--fileheadertext=EDI_REORG_652_
--fileheaderdate=yymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\652\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 0
use wca
select
'Levent' as f1,
'EventID' as f2,
'Created' as f3,
'Changed' as f4,
'Actflag' as f5,
'IssID' as f6,
'SecID' as f7,
'SectyCD' as f8,
'Issuername' as f9,
'PrimaryExchgCD' as f10,
'ExchgCD' as f11,
'Localcode' as f12,
'SecurityStatus' as f13,
'PrimaryDate' as f14,
'Date1' as f15,
'SecondaryDate' as f16,
'Date2' as f17,
'TertiaryDate' as f18,
'Date3' as f19,
'Addfield1' as f20,
'AddInfo1' as f21,
'Addfield2' as f22,
'AddInfo2' as f23,
'Addfield3' as f24,
'AddInfo3' as f25,
'Addfield4' as f26,
'AddInfo4' as f27,
'Addfield5' as f28,
'AddInfo5' as f29,
'Notes' as f20


--# 1
use wca
select 
Levent,
EventID,
v10s_ANN.AnnounceDate as Created,
v10s_ANN.Acttime as Changed,
v10s_ANN.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'NotificationDate' as PrimaryDate,
v10s_ANN.NotificationDate as Date1,
'' as SecondaryDate,
'' as Date2,
'' as TertiaryDate,
'' as Date3,
'Reason',
v10s_ANN.Eventtype,
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
v10s_ANN.AnnNotes as Notes
FROM v10s_ANN
INNER JOIN SCMST ON SCMST.IssID = v10s_ANN.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_ANN.Acttime>getdate()-31
and v10s_ANN.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1
and v10s_ANN.Eventtype<>'CLEAN'
and v10s_ANN.Eventtype<>'CORR'


--# 2
use wca
select 
Levent,
EventID,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'FilingDate' as PrimaryDate,
v10s_BKRP.FilingDate as Date1,
'NotificationDate' as SecondaryDate,
v10s_BKRP.NotificationDate as Date2,
'' as TertiaryDate,
'' as Date3,
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
v10s_BKRP.BkrpNotes as Notes
FROM v10s_BKRP
INNER JOIN SCMST ON SCMST.IssID = v10s_BKRP.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_BKRP.Acttime>getdate()-31
and v10s_BKRP.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1


--# 3
use wca
select 
Levent,
EventID,
v10s_FYCHG.AnnounceDate as Created,
v10s_FYCHG.Acttime as Changed,
v10s_FYCHG.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'NewFYStartdate' as PrimaryDate,
v10s_FYCHG.NewFYStartdate as Date1,
'NewFYEnddate' as SecondaryDate,
v10s_FYCHG.NewFYEnddate as Date2,
'' as TertiaryDate,
'' as Date3,
'Reason',
v10s_FYCHG.Eventtype,
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ' as Notes
FROM v10s_FYCHG
INNER JOIN SCMST ON SCMST.IssID = v10s_FYCHG.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_FYCHG.Acttime>getdate()-31
and v10s_FYCHG.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1
and v10s_FYCHG.Eventtype<>'CLEAN'
--and v10s_FYCHG.Eventtype<>'CORR'


--# 4
use wca
select 
Levent,
EventID,
v10s_INCHG.AnnounceDate as Created,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'EffectiveDate' as PrimaryDate,
v10s_INCHG.InChgDate as Date1,
'' as SecondaryDate,
'' as Date2,
'' as TertiaryDate,
'' as Date3,
'Reason',
v10s_INCHG.Eventtype,
'Old_CntryCD',
v10s_INCHG.OldCntryCD,
'New_CntryCD',
v10s_INCHG.NewCntryCD,
' ',
' ',
' ',
' ',
' ' as Notes
FROM v10s_INCHG
INNER JOIN SCMST ON SCMST.IssID = v10s_INCHG.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_INCHG.Acttime>getdate()-300
and v10s_INCHG.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1
and v10s_INCHG.Eventtype<>'CLEAN'
--and v10s_INCHG.Eventtype<>'CORR'



--# 5
use wca
select 
Levent,
EventID,
v10s_ISCHG.AnnounceDate as Created,
v10s_ISCHG.Acttime as Changed,
v10s_ISCHG.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'NameChangeDate' as PrimaryDate,
v10s_ISCHG.NameChangeDate as Date1,
'' as SecondaryDate,
'' as Date2,
'' as TertiaryDate,
'' as Date3,
'Reason',
v10s_ISCHG.Eventtype,
'LegalName',
v10s_ISCHG.LegalName,
'MeetingDateFlag',
v10s_ISCHG.MeetingDateFlag,
' ',
' ',
' ',
' ',
v10s_ISCHG.ISCHGNotes as Notes
FROM v10s_ISCHG
INNER JOIN SCMST ON SCMST.IssID = v10s_ISCHG.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_ISCHG.Acttime>getdate()-31
and v10s_ISCHG.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1
and v10s_ISCHG.Eventtype<>'CLEAN'
and v10s_ISCHG.Eventtype<>'CORR'


--# 6
use wca
select 
Levent,
v10s_LIQ.EventID,
v10s_LIQ.AnnounceDate as Created,
v10s_LIQ.Acttime as Changed,
v10s_LIQ.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'LiquidationDate' as PrimaryDate,
MPAY.Paydate as Date1,
'Recdate' as SecondaryDate,
v10s_LIQ.RDDate as Date2,
'' as TertiaryDate,
'' as Date3,
'Liquidator',
V10s_LIQ.Liquidator,
'LiquidationPrice',
MPAY.MinPrice,
' ',
' ',
' ',
' ',
' ',
' ',
v10s_LIQ.LiquidationTerms as Notes
FROM v10s_LIQ
INNER JOIN SCMST ON SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN MPAY on v10s_LIQ.LiqID = MPAY.EventID and 'LIQ' = MPAY.sEvent
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_LIQ.Acttime>getdate()-150
and v10s_LIQ.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1


--# 7
use wca
select 
Levent,
EventID,
v10s_TKOVR.AnnounceDate as Created,
v10s_TKOVR.Acttime as Changed,
v10s_TKOVR.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'CompulsoryAcqDate' as PrimaryDate,
v10s_TKOVR.CmAcqDate as Date1,
'UnconditionalDate' as SecondaryDate,
v10s_TKOVR.UnconditionalDate as Date2,
'CloseDate' as TertiaryDate,
v10s_TKOVR.CloseDate as Date3,
'Hostile',
v10s_TKOVR.Hostile,
'OfferorName',
v10s_TKOVR.OfferorName,
' ',
' ',
' ',
' ',
' ',
' ',
v10s_TKOVR.TKOVRNotes as Notes
FROM v10s_TKOVR
INNER JOIN SCMST ON SCMST.SecID = v10s_TKOVR.SecID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_TKOVR.Acttime>getdate()-31
and v10s_TKOVR.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1


--# 8
use wca
select 
Levent,
EventID,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v10s_ARR.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Date1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as Date2,
'RecDate' as TertiaryDate,
RD.RecDate as Date3,
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
v10s_ARR.ARRNotes as Notes
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN SCMST ON RD.Secid = SCMST.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
(v10s_ARR.Acttime >= getdate()-300
or RD.Acttime >= getdate()-300
or EXDT.Acttime >= getdate()-300
or PEXDT.Acttime >= getdate()-300)
and v10s_ARR.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1


--# 9
use wca
select 
Levent,
EventID,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v10s_DVST.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Date1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as Date2,
'RecDate' as TertiaryDate,
RD.RecDate as Date3,
'ResultantIssid',
rissur.issid,
'ResultantIssuername',
rissur.issuername,
' ',
' ',
' ',
' ',
' ',
' ',
v10s_DVST.DVSTNotes as Notes
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN SCMST ON RD.Secid = SCMST.IssID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join scmst as rscmst on ressecid = rscmst.secid
left outer join issur as rissur on rscmst.issid = rissur.issid
where
(v10s_DVST.Acttime >= getdate()-1000
or RD.Acttime >= getdate()-1000
or EXDT.Acttime >= getdate()-1000
or PEXDT.Acttime >= getdate()-1000)
and v10s_DVST.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1


--# 10
use wca
select 
Levent,
EventID,
v10s_MRGR.AnnounceDate as Created,
v10s_MRGR.Acttime as Changed,
v10s_MRGR.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'AppointedDate' as PrimaryDate,
v10s_MRGR.AppointedDate as Date1,
'EffectiveDate' as SecondaryDate,
v10s_MRGR.EffectiveDate as Date2,
'PayDate' as TertiaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Date3,
'MergerStatus',
v10s_MRGR.MrgrStatus,
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
v10s_MRGR.MRGRTerms as Notes
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_MRGR.Acttime>getdate()-300
and v10s_MRGR.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1


--# 11
use wca
select 
Levent,
EventID,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
CASE WHEN v10s_LSTAT.LstatStatus = 'D' THEN 'DelistingDate'
     WHEN v10s_LSTAT.LstatStatus = 'R' THEN 'ResumptionDate'
     WHEN v10s_LSTAT.LstatStatus = 'S' THEN 'SuspensionDate'
     ELSE 'UnknownDate' END as PrimaryDate,
v10s_LSTAT.EffectiveDate as Date1,
'NotificationDate' as SecondaryDate,
v10s_LSTAT.NotificationDate as Date2,
'' as TertiaryDate,
'' as Date3,
'Reason',
v10s_LSTAT.Eventtype,
'ListingStatusChangedTo',
v10s_LSTAT.LstatStatus,
' ',
' ',
' ',
' ',
' ',
' ',
' ' as Notes
FROM v10s_LSTAT
INNER JOIN SCEXH ON v10s_LSTAT.SecID = SCEXH.SecID AND v10s_LSTAT.ExchgCD = SCEXH.ExchgCD
INNER JOIN SCMST ON SCMST.SecID = v10s_LSTAT.SecID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_LSTAT.Acttime>getdate()-15
and v10s_LSTAT.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1
and v10s_LSTAT.Eventtype<>'CLEAN'
and v10s_LSTAT.Eventtype<>'CORR'


--# 12
use wca
select 
Levent,
EventID,
v10s_NLIST1.AnnounceDate as Created,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'Listdate' as PrimaryDate,
SCEXH.Listdate as Date1,
'' as SecondaryDate,
'' as Date2,
'' as TertiaryDate,
'' as Date3,
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ',
' ' as Notes
FROM v10s_NLIST1
INNER JOIN SCEXH ON v10s_NLIST1.ScexhID = SCEXH.ScexhID
INNER JOIN SCMST ON SCEXH.SecID = SCMST.SecID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_NLIST1.Acttime>getdate()-31
and v10s_NLIST1.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1


--# 13
use wca
select 
Levent,
EventID,
v10s_LCC.AnnounceDate as Created,
v10s_LCC.Acttime as Changed,
v10s_LCC.ActFlag,
SCMST.IssID,
SCMST.SecID,
SCMST.SectyCD,
ISSUR.Issuername,
SCMST.PrimaryExchgCD,
case when SCMST.PrimaryExchgCD='' then SCEXH.ExchgCD else '' end as ExchgCD,
SCEXH.Localcode,
case when scmst.statusflag='I' then 'Inactive' when SCEXH.Liststatus='D' then 'Delisted' when SCEXH.secid is null then 'No listing' else 'Active' end as SecurityStatus,
'EffectiveDate' as PrimaryDate,
v10s_LCC.EffectiveDate as Date1,
'' as SecondaryDate,
'',
'' as TertiaryDate,
'' as Date3,
'Reason',
v10s_LCC.Eventtype,
'OldLocalcode',
v10s_LCC.OldLocalcode,
'NewLocalcode',
v10s_LCC.NewLocalcode,
' ',
' ',
' ',
' ',
' ' as Notes
FROM v10s_LCC
INNER JOIN SCEXH ON v10s_LCC.SecID = SCEXH.SecID AND v10s_LCC.ExchgCD = SCEXH.ExchgCD
INNER JOIN SCMST ON SCMST.SecID = v10s_LCC.SecID
LEFT OUTER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join wca.dbo.sectygrp on SCMST.sectycd = wca.dbo.sectygrp.sectycd
where
v10s_LCC.Acttime>getdate()-15
and v10s_LCC.Actflag <> 'D'
and scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and sectygrp.secgrpid=1
and v10s_LCC.Eventtype<>'CLEAN'
and v10s_LCC.Eventtype<>'CORR'
