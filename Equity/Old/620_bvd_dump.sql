--filepath=o:\Datafeed\Equity\Sample\
--fileheadertext=
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=_BVD
--fileheadertext=EDI_STATIC_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 12
use wcahist
select *
FROM v53f_620_Capital_Reduction
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wcahist
select *
FROM v54f_620_Bonus
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wcahist
select *
FROM v54f_620_Consolidation
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wcahist
select *
FROM v54f_620_Subdivision
ORDER BY EventID desc, ExchgCD, Sedol

