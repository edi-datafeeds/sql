--filepath=o:\Datafeed\Equity\642_BBH\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(acttime) from wca.dbo.tbl_Opslog)
--fileextension=.642
--suffix=
--fileheadertext=EDI_STATIC_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\642_BBH\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 1
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
lcc.Actflag,
lcc.eventtype as Reason,
lcc.effectivedate as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = lcc.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
lcc.Acttime > getdate()-31
and lcc.NewLocalcode <> ''
and substring(v20c_EV_dSCEXH.ExchgCD,1,2)='US'


--# 2
use wca
select
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null then v20c_EV_SCMST.PrimaryExchgCD else v20c_EV_dSCEXH.ExchgCD end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_SCMST.StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
icc.EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
ICC.Acttime > getdate()-31
and icc.NewUSCode <> ''
and substring(v20c_EV_dSCEXH.ExchgCD,1,2)='US'
