--filepath=o:\Datafeed\Equity\
--filenameprefix=
--filename=2009_JP_FUTURE
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


USE WCA
SELECT *  
FROM v54f_620_Dividend
WHERE
excountry='JP'
and
(recdate > '2009/10/14'
or exdate > '2009/10/14')
ORDER BY EventID desc, ExchgCD, Sedol
