--filepath=o:\Datafeed\Equity\620_NOT_US\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_NOT_US_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_NOT_US\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
SELECT *  
FROM v50f_620_Company_Meeting 
WHERE
(ExCountry <> 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>getdate()-183
ORDER BY CaRef

--# 3
use wca
SELECT *  
FROM v50f_620_Liquidation
WHERE
(ExCountry <> 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>getdate()-183
ORDER BY CaRef, OptionID, SerialID

--# 5
use wca
SELECT *  
FROM v51f_620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 8
use wca
SELECT *  
FROM v51f_620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 10
use wca
SELECT *  
FROM v52f_620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 14
use wca
SELECT *  
FROM v53f_620_Takeover
WHERE
(ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef, OptionID, SerialID

--# 15
use wca
SELECT *  
FROM v54f_620_Arrangement
WHERE
(ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 19
use wca
SELECT *  
FROM v54f_620_Demerger
WHERE
(ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef, OptionID, SerialID

--# 23
use wca
SELECT *  
FROM v54f_620_Merger
WHERE
(ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef, OptionID, SerialID

--# 31
use wca
SELECT * 
FROM v50f_620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>getdate()-183
ORDER BY CaRef

--# 32
use wca
SELECT * 
FROM v50f_620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>getdate()-183
ORDER BY CaRef

--# 34
use wca
SELECT * 
FROM v51f_620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 36
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 37
use wca
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 38
use wca
SELECT *  
FROM v52f_620_New_Listing
WHERE
(ExCountry <> 'US')
and changed>getdate()-183
ORDER BY CaRef

--# 39
use wca
SELECT *  
FROM v50f_620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry <> 'US')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>getdate()-183
ORDER BY CaRef
