--filepath=o:\Datafeed\Equity\646\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=(select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--fileextension=.646
--suffix=
--fileheadertext=EDI_REORG_646_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\646_Markit\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 0
use wca
select
'sevent' as f1,
'EventID' as f2,
'SecID' as f3,
'IssID' as f4,
'ExchgCD' as f10,
'Exdate' as f11,
'RatioNew' as f16,
'RatioOld' as f17,
'Created' as f5,
'Actflag' as f7,


We wish to receive an extraction of your WCA database, containing all
records related to the following events: consolidation (event type 6) ,
subdivision (event type 5), capital reduction (event type 16) and bonus
(event type 13)
For each event, we wish to receive the following fields:  "EventID",
"IssID", "SecID", "ExDate", "ExchgCD", "RatioOld", "RatioNew".


--# 5
use wca
select distinct
'CAPRD' as Levent,
CAPRD.CaprdID as EventID,
SCMST.SecID,
SCMST.IssID,
SCMST.PrimaryExchgCD,
CAPRD.EffectiveDate as Exdate,
CAPRD.NewRatio as RatioNew,
CAPRD.OldRatio as RatioOld,
CAPRD.AnnounceDate as Created,
CAPRD.ActFlag
FROM CAPRD
INNER JOIN SCMST ON SCMST.SecID = CAPRD.SecID
LEFT OUTER JOIN RD ON CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON CaprdID = ICC.RelEventID AND 'CAPRD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(CAPRD.Announcedate between '2007/07/01' and '2007/10/01' or RD.Announcedate between '2007/07/01' and '2007/10/01')
and wca.dbo.scexh.ListStatus<>'D'
and exchg.cntrycd = 'US'


--# 7
use wca
select distinct
'Bonus' as Levent,
BON.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > BON.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > BON.Acttime) THEN PEXDT.Acttime ELSE BON.Acttime END as Changed,
BON.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BON
INNER JOIN RD ON RD.RdID = BON.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(BON.Announcedate between '2007/07/01' and '2007/10/01' or RD.Announcedate between '2007/07/01' and '2007/10/01' or PEXDT.Announcedate between '2007/07/01' and '2007/10/01')
and wca.dbo.scexh.ListStatus<>'D'
and exchg.cntrycd = 'US'


--# 9
use wca
select distinct
'Consolidation' as Levent,
CONSD.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONSD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > CONSD.Acttime) THEN PEXDT.Acttime ELSE CONSD.Acttime END as Changed,
CONSD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CONSD
INNER JOIN RD ON RD.RdID = CONSD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON CONSD.RdId = ICC.RelEventID AND 'CONSD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(CONSD.Announcedate between '2007/07/01' and '2007/10/01' or RD.Announcedate between '2007/07/01' and '2007/10/01' or PEXDT.Announcedate between '2007/07/01' and '2007/10/01')
and wca.dbo.scexh.ListStatus<>'D'
and exchg.cntrycd = 'US'



--# 14
use wca
select distinct
'Sub-Division' as Levent,
SD.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > SD.Acttime) THEN PEXDT.Acttime ELSE SD.Acttime END as Changed,
SD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
'NewCodeDate' as PrimaryDate,
ICC.Acttime as EndDate1,
'PayDate' as SecondaryDate,
PEXDT.PayDate as EndDate2,
'RecDate' as TertiaryDate,
PEXDT.ExDate as EndDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM SD
INNER JOIN RD ON RD.RdID = SD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON SD.RdId = ICC.RelEventID AND 'SD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(SD.Announcedate between '2007/07/01' and '2007/10/01' or RD.Announcedate between '2007/07/01' and '2007/10/01' or ICC.Announcedate between '2007/07/01' and '2007/10/01' or PEXDT.Announcedate between '2007/07/01' and '2007/10/01')
and wca.dbo.scexh.ListStatus<>'D'
and exchg.cntrycd = 'US'

