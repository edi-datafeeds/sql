--filepath=o:\Datafeed\Equity\cplink_refdata\
--filenameprefix=cplink_refdata_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_CPLINK_REFDATA_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select  distinct
SCMST.IssID,
SCMST.SecID,
scmst.Isin,
ISSUR.Issuername,
ISSUR.CntryofIncorp,
SCMST.SecurityDesc,
SCMST.PrimaryExchgCD,
SCEXH.ExchgCD,
SCEXH.Localcode
FROM scexh
INNER JOIN SCMST ON SCEXH.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
where
scmst.actflag<>'D' and scexh.actflag<>'D' and issur.actflag<>'D'
and scmst.issid not in (select issid from portfolio.dbo.cplissid)
and (scmst.primaryexchgcd='' or scmst.primaryexchgcd=SCEXH.ExchgCD)
and (scmst.statusflag<>'I' or scmst.statusflag is null)
and (SCEXH.Liststatus<>'D')
