--filepath=o:\Datafeed\Equity\639_test\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1
--fileextension=.639
--suffix=_test
--fileheadertext=EDI_STATIC_639_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\639\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
WHERE acttime >= (select max(acttime) from tbl_Opslog where seq = 1)

--# 2
use WCA
SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.X as CommonCode,
scmst.SharesOutstanding
FROM scmst
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid
WHERE scmst.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
and wca.dbo.scmst.structcd <> 'CEF'
and (scmst.sectycd<>'TRT')
and (scmst.sectycd<>'DRT')
and (sectygrp.secgrpid=1 or (sectygrp.secgrpid=2 and dprcp.secid is null))
and (exchg.cntrycd = 'AT'
or exchg.cntrycd = 'BE'
or exchg.cntrycd = 'BG'
or exchg.cntrycd = 'CH'
or exchg.cntrycd = 'CY'
or exchg.cntrycd = 'CZ'
or exchg.cntrycd = 'DE'
or exchg.cntrycd = 'DK'
or exchg.cntrycd = 'EE'
or exchg.cntrycd = 'ES'
or exchg.cntrycd = 'FI'
or exchg.cntrycd = 'FR'
or exchg.cntrycd = 'GB'
or exchg.cntrycd = 'GR'
or exchg.cntrycd = 'HU'
or exchg.cntrycd = 'IE'
or exchg.cntrycd = 'IS'
or exchg.cntrycd = 'IT'
or exchg.cntrycd = 'LT'
or exchg.cntrycd = 'LU'
or exchg.cntrycd = 'LV'
or exchg.cntrycd = 'MT'
or exchg.cntrycd = 'NL'
or exchg.cntrycd = 'NO'
or exchg.cntrycd = 'PL'
or exchg.cntrycd = 'PT'
or exchg.cntrycd = 'RO'
or exchg.cntrycd = 'SE'
or exchg.cntrycd = 'SI'
or exchg.cntrycd = 'SK')

--# 3
use WCA
SELECT
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid
WHERE sedol.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
and wca.dbo.scmst.structcd <> 'CEF'
and (scmst.sectycd<>'TRT')
and (scmst.sectycd<>'DRT')
and (sectygrp.secgrpid=1 or (sectygrp.secgrpid=2 and dprcp.secid is null))
and (sedol.cntrycd = 'AT'
or sedol.cntrycd = 'BE'
or sedol.cntrycd = 'BG'
or sedol.cntrycd = 'CH'
or sedol.cntrycd = 'CY'
or sedol.cntrycd = 'CZ'
or sedol.cntrycd = 'DE'
or sedol.cntrycd = 'DK'
or sedol.cntrycd = 'EE'
or sedol.cntrycd = 'ES'
or sedol.cntrycd = 'FI'
or sedol.cntrycd = 'FR'
or sedol.cntrycd = 'GB'
or sedol.cntrycd = 'GR'
or sedol.cntrycd = 'HU'
or sedol.cntrycd = 'IE'
or sedol.cntrycd = 'IS'
or sedol.cntrycd = 'IT'
or sedol.cntrycd = 'LT'
or sedol.cntrycd = 'LU'
or sedol.cntrycd = 'LV'
or sedol.cntrycd = 'MT'
or sedol.cntrycd = 'NL'
or sedol.cntrycd = 'NO'
or sedol.cntrycd = 'PL'
or sedol.cntrycd = 'PT'
or sedol.cntrycd = 'RO'
or sedol.cntrycd = 'SE'
or sedol.cntrycd = 'SI'
or sedol.cntrycd = 'SK')

--# 4
use WCA
SELECT
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
left outer join wca.dbo.scmst on wca.dbo.scexh.secid = wca.dbo.scmst.secid
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid
WHERE scexh.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
and wca.dbo.scmst.structcd <> 'CEF'
and (scmst.sectycd<>'TRT')
and (scmst.sectycd<>'DRT')
and (sectygrp.secgrpid=1 or (sectygrp.secgrpid=2 and dprcp.secid is null))
and (exchg.cntrycd = 'AT'
or exchg.cntrycd = 'BE'
or exchg.cntrycd = 'BG'
or exchg.cntrycd = 'CH'
or exchg.cntrycd = 'CY'
or exchg.cntrycd = 'CZ'
or exchg.cntrycd = 'DE'
or exchg.cntrycd = 'DK'
or exchg.cntrycd = 'EE'
or exchg.cntrycd = 'ES'
or exchg.cntrycd = 'FI'
or exchg.cntrycd = 'FR'
or exchg.cntrycd = 'GB'
or exchg.cntrycd = 'GR'
or exchg.cntrycd = 'HU'
or exchg.cntrycd = 'IE'
or exchg.cntrycd = 'IS'
or exchg.cntrycd = 'IT'
or exchg.cntrycd = 'LT'
or exchg.cntrycd = 'LU'
or exchg.cntrycd = 'LV'
or exchg.cntrycd = 'MT'
or exchg.cntrycd = 'NL'
or exchg.cntrycd = 'NO'
or exchg.cntrycd = 'PL'
or exchg.cntrycd = 'PT'
or exchg.cntrycd = 'RO'
or exchg.cntrycd = 'SE'
or exchg.cntrycd = 'SI'
or exchg.cntrycd = 'SK')

--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
WHERE acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
