--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
--fileextension=.642
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\642\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select
SDCHG.SdchgID as EventID,
'Sedol Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,

v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
sdchg.Actflag,
sdchg.eventtype as Reason,
case when sdchg.effectivedate is null
     then sdchg.announcedate
     else sdchg.effectivedate
     end as EffectiveDate,
sdchg.OldSedol as OldStatic,
sdchg.NewSedol as NewStatic
FROM SDCHG
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = SDCHG.SecID
LEFT OUTER JOIN v20c_WCA2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_WCA2_dSCEXH.SecID
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and eventtype<>'CORR'
and eventtype<>'CLEAN'
and (sdchg.effectivedate between calclistdate and calcdelistdate
or sdchg.announcedate between calclistdate and calcdelistdate)


--# 3
use wca
select
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,

v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic
FROM ICC
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_WCA2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_WCA2_dSCEXH.SecID
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and (icc.OldIsin <> '' or icc.NewIsin <> '')
and eventtype<>'CORR'
and eventtype<>'CLEAN'
and (icc.effectivedate between calclistdate and calcdelistdate
or icc.announcedate between calclistdate and calcdelistdate)


--# 4
use wca
select
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,

v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
inchg.Actflag,
inchg.eventtype as Reason,
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic
FROM inchg
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.issid = inchg.issid
LEFT OUTER JOIN v20c_WCA2_dSCEXH ON v20c_WCA2_SCMST.secid = v20c_WCA2_dSCEXH.secID
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and eventtype<>'CORR'
and eventtype<>'CLEAN'
and (inchg.inchgdate between calclistdate and calcdelistdate
or inchg.announcedate between calclistdate and calcdelistdate)


--# 5
use wca
select
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,

v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
ischg.Actflag,
ischg.eventtype as Reason,
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic
FROM ischg
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.issid = ischg.issid
LEFT OUTER JOIN v20c_WCA2_dSCEXH ON v20c_WCA2_SCMST.secid = v20c_WCA2_dSCEXH.secID
where 
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and eventtype<>'CORR'
and eventtype<>'CLEAN'
and (ischg.namechangedate between calclistdate and calcdelistdate
or ischg.announcedate between calclistdate and calcdelistdate)



--# 6
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,

v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN v20c_WCA2_dSCEXH ON lcc.secid = v20c_WCA2_dSCEXH.secID
                    and lcc.exchgcd = v20c_WCA2_dSCEXH.exchgcd
INNER JOIN v20c_WCA2_SCMST ON lcc.secid = v20c_WCA2_SCMST.secid
where 
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and eventtype<>'CORR'
and eventtype<>'CLEAN'


--# 7
use wca
select
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,

v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
scchg.Actflag,
scchg.eventtype as Reason,
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic
FROM scchg
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.secid = scchg.secid
LEFT OUTER JOIN v20c_WCA2_dSCEXH ON v20c_WCA2_SCMST.secid = v20c_WCA2_dSCEXH.secID
where 
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and eventtype<>'CORR'
and eventtype<>'CLEAN'
and (scchg.dateofchange between calclistdate and calcdelistdate
or scchg.announcedate between calclistdate and calcdelistdate)

--# 8
use wca
select
LTCHG.LTCHGID as EventID,
'Lot Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,
v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic
FROM ltchg
INNER JOIN v20c_WCA2_dSCEXH ON ltchg.secid = v20c_WCA2_dSCEXH.secID
                    and ltchg.exchgcd = v20c_WCA2_dSCEXH.exchgcd
INNER JOIN v20c_WCA2_SCMST ON ltchg.secid = v20c_WCA2_SCMST.secid
where 
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and (ltchg.OldLot <> ''
or ltchg.NewLot <> '')


--# 9
use wca
select
LTCHG.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,
v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldMinTrdQty as OldStatic,
ltchg.NewMinTrdgQty as NewStatic
FROM ltchg
INNER JOIN v20c_WCA2_dSCEXH ON ltchg.secid = v20c_WCA2_dSCEXH.secID
                    and ltchg.exchgcd = v20c_WCA2_dSCEXH.exchgcd
INNER JOIN v20c_WCA2_SCMST ON ltchg.secid = v20c_WCA2_SCMST.secid
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and (ltchg.OldMinTrdQty <> ''
or ltchg.NewMinTrdgQty <> '')


--# 10
use wca
select
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,
v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic
FROM lstat
INNER JOIN v20c_WCA2_dSCEXH ON lstat.secid = v20c_WCA2_dSCEXH.secID
                    and lstat.exchgcd = v20c_WCA2_dSCEXH.exchgcd
INNER JOIN v20c_WCA2_SCMST ON lstat.secid = v20c_WCA2_SCMST.secid
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and eventtype<>'CORR'
and eventtype<>'CLEAN'


--# 11
use wca
select
nlist.scexhID as EventID,
'New Listing' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,
v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
nlist.Actflag,
case when v20c_WCA2_dSCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when v20c_WCA2_dSCEXH.Listdate is not null 
     then v20c_WCA2_dSCEXH.Listdate 
     else nlist.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic
FROM nlist
INNER JOIN v20c_WCA2_dSCEXH ON nlist.scexhid = v20c_WCA2_dSCEXH.scexhid
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_dSCEXH.secid = v20c_WCA2_SCMST.secid
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)


--# 12
use wca
select
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,
v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.EffectiveDate is null
     then icc.announcedate
     else icc.effectivedate
     end as EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic
FROM ICC
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_WCA2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_WCA2_dSCEXH.SecID
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and (icc.OldUSCode <> ''
or icc.NewUSCode <> '')
and eventtype<>'CORR'
and eventtype<>'CLEAN'
and (icc.effectivedate between calclistdate and calcdelistdate
or icc.announcedate between calclistdate and calcdelistdate)


--# 13
use wca
select
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
v20c_WCA2_SCMST.IssID,
v20c_WCA2_SCMST.SecID,
v20c_WCA2_SCMST.Issuername,
v20c_WCA2_SCMST.CntryofIncorp,
v20c_WCA2_SCMST.SecurityDesc,
rtrim(v20c_WCA2_SCMST.SectyCD) as SectyCD,
v20c_WCA2_SCMST.Isin,
v20c_WCA2_SCMST.Uscode,
v20c_WCA2_dSCEXH.Sedol,
v20c_WCA2_dSCEXH.RegCountry,
case when v20c_WCA2_dSCEXH.scexhid is null 
     then rtrim(v20c_WCA2_SCMST.PrimaryExchgCD)
     else rtrim(v20c_WCA2_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_WCA2_dSCEXH.Localcode,
rtrim(v20c_WCA2_dSCEXH.ListStatus) as ListStatus,
v20c_WCA2_SCMST.StatusFlag,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic
FROM Currd
INNER JOIN v20c_WCA2_SCMST ON v20c_WCA2_SCMST.SecID = currd.SecID
LEFT OUTER JOIN v20c_WCA2_dSCEXH ON v20c_WCA2_SCMST.SecID = v20c_WCA2_dSCEXH.SecID
where
v20c_WCA2_SCMST.secid in (select secid from portfolio.dbo.etf)
and eventtype<>'CORR'
and eventtype<>'CLEAN'
and (Currd.effectivedate between calclistdate and calcdelistdate
or Currd.announcedate between calclistdate and calcdelistdate)

