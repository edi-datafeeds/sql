--filepath=o:\Datafeed\Equity\620_NOT_US\
--filenameprefix=Active_
--filename=yyyymmdd
--filenamealt=
--fileextension=.643
--suffix=
--fileheadertext=EDI_REORG_643_NOT_US_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_NOT_US\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
SELECT *  
FROM v50f_620_SRF
WHERE
ExCountry <> 'US'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and liststatus<>'D'
ORDER BY secid