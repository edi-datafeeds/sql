--filepath=o:\Datafeed\Equity\Markit_Exdiv\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=_EXDIV
--fileheadertext=EDI_EXDIV_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select distinct
SecID,
Isin,
Exchgcd,
primaryEx,
Exdate
from v54f_620_dividend
left outer join wca.dbo.sectygrp on v54f_620_dividend.sectycd = wca.dbo.sectygrp.sectycd
where
exdate>getdate() and exdate<getdate()+62
and primaryex<>'No'
and (substring(exchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
