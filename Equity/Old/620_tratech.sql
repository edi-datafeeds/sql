--filepath=o:\Datafeed\Equity\620_tratech\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT *
FROM v50f_620_Company_Meeting 
WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
SELECT *
FROM v53f_620_Call 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
SELECT *
FROM v50f_620_Liquidation 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA
SELECT *
FROM v51f_620_Certificate_Exchange 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
SELECT *
FROM v51f_620_International_Code_Change
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
SELECT *
FROM v51f_620_Conversion_Terms
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
USE WCA
SELECT *
FROM v51f_620_Redemption_Terms
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
USE WCA
SELECT *
FROM v51f_620_Security_Reclassification
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
USE WCA
SELECT *
FROM v52f_620_Lot_Change
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
USE WCA
SELECT *
FROM v52f_620_Sedol_Change
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
USE WCA
SELECT *
FROM v53f_620_Buy_Back
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
USE WCA
SELECT *
FROM v53f_620_Capital_Reduction
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
USE WCA
SELECT *
FROM v53f_620_Takeover
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
USE WCA
SELECT *
FROM v54f_620_Arrangement
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
USE WCA
SELECT *
FROM v54f_620_Bonus
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
USE WCA
SELECT *
FROM v54f_620_Bonus_Rights
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
USE WCA
SELECT *
FROM v54f_620_Consolidation
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
USE WCA
SELECT *
FROM v54f_620_Demerger
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
USE WCA
SELECT *
FROM v54f_620_Distribution
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
USE WCA
SELECT *
FROM v54f_620_Divestment
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
USE WCA
SELECT *
FROM v54f_620_Entitlement
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
USE WCA
SELECT *
FROM v54f_620_Merger
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
USE WCA
SELECT *
FROM v54f_620_Preferential_Offer
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
USE WCA
SELECT *
FROM v54f_620_Purchase_Offer
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
USE WCA
SELECT *
FROM v54f_620_Rights 
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
USE WCA
SELECT *
FROM v54f_620_Security_Swap 
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
USE WCA
SELECT *
FROM v54f_620_Subdivision
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
USE WCA
SELECT *
FROM v50f_620_Bankruptcy 
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
USE WCA
SELECT *
FROM v50f_620_Financial_Year_Change
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
USE WCA
SELECT *
FROM v50f_620_Incorporation_Change
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
USE WCA
SELECT *
FROM v50f_620_Issuer_Name_change
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
USE WCA
SELECT *
FROM v50f_620_Class_Action
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
USE WCA
SELECT *
FROM v51f_620_Security_Description_Change
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
USE WCA
SELECT *
FROM v52f_620_Assimilation
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
USE WCA
SELECT *
FROM v52f_620_Listing_Status_Change
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
USE WCA
SELECT *
FROM v52f_620_Local_Code_Change
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
USE WCA
SELECT *
FROM v52f_620_New_Listing
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
USE WCA
SELECT *
FROM v50f_620_Announcement
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
USE WCA
SELECT *
FROM v51f_620_Parvalue_Redenomination 
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
SELECT *
FROM v51f_620_Currency_Redenomination 
 

WHERE CHANGED >= '2009/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
SELECT *
FROM v53f_620_Return_of_Capital 
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
SELECT *
FROM v54f_620_Dividend
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
USE WCA
SELECT *
FROM v54f_620_Dividend_Reinvestment_Plan
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
USE WCA
SELECT *
FROM v54f_620_Franking
 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')

ORDER BY EventID desc, ExchgCD, Sedol

--# 46
USE WCA
SELECT *
FROM v51f_620_Conversion_Terms_Change 

WHERE CHANGED >= '2009/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry = 'EG'
or excountry = 'LY'
or excountry = 'MA'
or excountry = 'SD'
or excountry = 'TN'
or excountry = 'EH'
or excountry = 'AE'
or excountry = 'SA'
or excountry = 'KW'
or excountry = 'BH'
or excountry = 'OM'
or excountry = 'QA'
or excountry = 'LB'
or excountry = 'JO'
or excountry = 'TN'
or excountry = 'PS'
or excountry = 'DZ')
ORDER BY EventID desc, ExchgCD, Sedol
