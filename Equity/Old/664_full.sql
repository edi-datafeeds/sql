--filepath=o:\Datafeed\Equity\664\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.664
--suffix=
--fileheadertext=EDI_DR_664_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\664\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('DPRCP') as Tablename,
DPRCP.Actflag,
DPRCP.Acttime,
DPRCP.Announcedate,
DPRCP.SecID,
DPRCP.UnSecID,
DPRCP.DRRatio,
DPRCP.USRatio,
DPRCP.SpnFlag,
DPRCP.DRtype,
DPRCP.DepBank,
DPRCP.LevDesc,
DPRCP.OtherDepBank
from DPRCP
WHERE
actflag<>'D'

--# 2
use WCA
SELECT
upper('DRCHG') as Tablename,
DRCHG.Actflag,
DRCHG.Acttime,
DRCHG.Announcedate,
DRCHG.DrchgID,
DRCHG.SecID,
DRCHG.EffectiveDate,
DRCHG.OldDRRatio,
DRCHG.NewDRRatio,
DRCHG.OldUNRatio,
DRCHG.NewUNRatio,
DRCHG.OldUNSecID,
DRCHG.NewUNSecID,
DRCHG.Eventtype as RelatedEvent,
DRCHG.OldDepbank,
DRCHG.NewDepbank,
DRCHG.OldDRtype,
DRCHG.NewDRtype,
DRCHG.OldLevel,
DRCHG.Newlevel,
DrchgNotes
from DRCHG
where
actflag<>'D'
