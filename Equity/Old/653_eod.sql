--filepath=o:\Datafeed\Equity\653\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(acttime) from wca.dbo.tbl_Opslog)
--fileextension=.653
--suffix=
--fileheadertext=EDI_FLATSRF_653_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
exchg.cntrycd,
exchg.mic,
scmst.secid,
scmst.isin,
scexh.Localcode,
issur.issuername, 
scmst.issid,
scmst.securitydesc,
scexh.exchgcd,
scmst.sectycd,
scmst.primaryexchgcd,
case when scmst.statusflag<>'I' then 'A' else 'I' end as globalscmststatus,
case when scexh.liststatus is null then 'U' when scexh.liststatus='N' or scexh.liststatus='' then 'L' else scexh.liststatus end as wcaliststatus,
scmst.acttime as changed,
scmst.actflag as ScmstRecordStatus,
scexh.actflag as ScexhRecordStatus
from scmst
inner join issur on scmst.issid = issur.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
(scmst.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
or issur.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
or scexh.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1))
