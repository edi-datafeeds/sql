--filepath=o:\Datafeed\Equity\620_wringley\
--filenameprefix=Q4_2011_
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT v54f_620_Dividenddelist.*
FROM v54f_620_Dividenddelist
left outer join lstat on v54f_620_Dividenddelist.secid = lstat.secid and v54f_620_Dividenddelist.exchgcd = lstat.exchgcd and 'D'= lstat.lstatstatus and v54f_620_Dividenddelist.liststatus = 'D'
WHERE
paydate >= '2012/04/01' and paydate < '2012/07/01'
and excountry='GB'
and (lstat.effectivedate is null or lstat.effectivedate>=Exdate)
and v54f_620_Dividenddelist.actflag<>'D'
ORDER BY EventID desc, v54f_620_Dividenddelist.ExchgCD, Sedol
