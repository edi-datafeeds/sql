--filepath=o:\Datafeed\Equity\620_CarryQuote\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 5
use wca
SELECT * FROM v51f_620_International_Code_Change
WHERE
created>'2010/08/31'
and RelatedEvent <> 'Clean'
ORDER BY eventid desc

--# 31
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
created>'2010/08/31'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
created>'2010/08/31'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 35
use wca
SELECT * FROM v52f_620_Listing_Status_Change
WHERE
created>'2010/08/31'
and RelatedEvent <> 'Clean'
ORDER BY eventid desc


--# 36
use wca
SELECT *
FROM v52f_620_Local_Code_Change
WHERE
created>'2010/08/31'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 37
use wca
SELECT * FROM v52f_620_New_Listing
where
created>'2010/08/31'
ORDER BY eventid desc
