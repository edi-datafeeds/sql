--filepath=o:\Datafeed\Equity\652\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.652
--suffix=
--fileheadertext=EDI_REORG_652_
--fileheaderdate=yymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\652\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
use wca
select 
v10s_BKRP.Levent,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,
'FilingDate' as PrimaryDate,
v10s_BKRP.FilingDate as Enddate1,
'NotificationDate' as SecondaryDate,
v10s_BKRP.NotificationDate as Enddate2,
'' as TertiaryDate,
'' as Enddate3,
v10s_BKRP.BkrpNotes as Notes
FROM v10s_BKRP
INNER JOIN SCMST ON SCMST.IssID = v10s_BKRP.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
where
v10s_BKRP.Acttime >= getdate()-400 and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_BKRP.Actflag <> 'D'

--# 
use wca
select 
v10s_INCHG.Levent,
v10s_INCHG.AnnounceDate as Created,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,

'EffectiveDate' as PrimaryDate,
v10s_INCHG.InChgDate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3,
'' as Notes
FROM v10s_INCHG
INNER JOIN SCMST ON SCMST.IssID = v10s_INCHG.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
where
scmst.issid in (select issid from portfolio.dbo.cplissid)
and v10s_INCHG.Actflag <> 'D' 
and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)


--# 
use wca
select 
v10s_LIQ.Levent,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as Changed,
v10s_LIQ.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,
'LiquidationDate' as PrimaryDate,
MPAY.Paydate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_LIQ
INNER JOIN SCMST ON SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
where
v10s_LIQ.Acttime >= getdate()-400 and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_LIQ.Actflag <> 'D'
and (SCMST.ISIN<>'' OR SCEXH.Sedol is not null)

--# 
use wca
select 
v10s_TKOVR.Levent,
v10s_TKOVR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_TKOVR.Acttime) THEN MPAY.Acttime ELSE v10s_TKOVR.Acttime END as [Changed],
v10s_TKOVR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,
'CompulsoryAcqDate' as PrimaryDate,
v10s_TKOVR.CmAcqDate as Enddate1,
'UnconditionalDate' as SecondaryDate,
v10s_TKOVR.UnconditionalDate as Endate2,
'CloseDate' as TertiaryDate,
v10s_TKOVR.CloseDate as Endate3
FROM v10s_TKOVR
INNER JOIN SCMST ON SCMST.SecID = v10s_TKOVR.SecID
LEFT OUTER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID AND v10s_TKOVR.SEvent = MPAY.SEvent
where
(v10s_TKOVR.Acttime >= getdate()-400
or MPAY.Acttime >= getdate()-400)
and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_TKOVR.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR SCEXH.Sedol is not null)

--# 
use wca
select 
v10s_ARR.Levent,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v10s_ARR.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
SCEXH.Localcode,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND SCEXH.ExCountry = SDCHG.CntryCD AND SCEXH.RegCountry = SDCHG.RcntryCD
where
(v10s_ARR.Acttime >= getdate()-400
or RD.Acttime >= getdate()-400
or EXDT.Acttime >= getdate()-400
or PEXDT.Acttime >= getdate()-400)
and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_ARR.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR SCEXH.Sedol is not null)

--# 
use wca
select
v10s_DVST.Levent,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v10s_DVST.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,
'PayDate' as PrimaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
'ExDate' as SecondaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
where
(v10s_DVST.Acttime >= getdate()-400
or RD.Acttime >= getdate()-400
or EXDT.Acttime >= getdate()-400
or PEXDT.Acttime >= getdate()-400)
and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_DVST.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR SCEXH.Sedol is not null)

--# 
use wca
select 
v10s_MRGR.Levent,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v10s_MRGR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,
'AppointedDate' as PrimaryDate,
v10s_MRGR.AppointedDate as Enddate1,
'EffectiveDate' as SecondaryDate,
v10s_MRGR.EffectiveDate as EndDate2,
'PayDate' as TertiaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate3
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN SCEXH ON SCMST.SecID = SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND SCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID AND v10s_MRGR.SEvent = MPAY.SEvent
where
(v10s_MRGR.Acttime >= getdate()-400
or EXDT.Acttime >= getdate()-400
or PEXDT.Acttime >= getdate()-400)
and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_MRGR.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR SCEXH.Sedol is not null)

--# 
use wca
select 
v10s_LSTAT.Levent,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,
CASE WHEN v10s_LSTAT.LstatStatus = 'D' THEN 'DelistingDate'
     WHEN v10s_LSTAT.LstatStatus = 'R' THEN 'ResumptionDate'
     WHEN v10s_LSTAT.LstatStatus = 'S' THEN 'SuspensionDate'
     ELSE 'UnknownDate' END as PrimaryDate,
v10s_LSTAT.EffectiveDate as Enddate1,
'NotificationDate' as SecondaryDate,
v10s_LSTAT.NotificationDate as Enddate2,
'' as TertiaryDate,
'' as Enddate3
FROM v10s_LSTAT
INNER JOIN SCEXH ON v10s_LSTAT.SecID = SCEXH.SecID AND v10s_LSTAT.ExchgCD = SCEXH.ExchgCD
INNER JOIN SCMST ON SCMST.SecID = v10s_LSTAT.SecID
where
v10s_LSTAT.Acttime >= getdate()-400 and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_LSTAT.Actflag <> 'D'
and (SCMST.ISIN<>'' OR SCEXH.Sedol is not null)

--# 
use wca
select
v10s_NLIST1.Levent,
v10s_NLIST1.AnnounceDate as Created,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.IssID,
SCMST.SecID,
SCEXH.Localcode,
'Listdate' as PrimaryDate,
SCEXH.Listdate as Enddate1,
'' as SecondaryDate,
'' as Endate2,
'' as TertiaryDate,
'' as Endate3
FROM v10s_NLIST1
INNER JOIN SCEXH ON v10s_NLIST1.ScexhID = SCEXH.ScexhID
INNER JOIN SCMST ON SCEXH.SecID = SCMST.SecID
where
v10s_NLIST1.Acttime >= getdate()-400 and SCEXH.secid is not null and SCEXH.exchgcd <> '' and SCEXH.exchgcd is not null
and v10s_NLIST1.Actflag <> 'D' 
and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR SCEXH.Sedol is not null)
