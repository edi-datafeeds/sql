--filepath=o:\Datafeed\Equity\621\
--filenameprefix=
--filename=2009_CA
--filenamealt=
--fileextension=.621
--suffix=
--fileheadertext=EDI_CA_created_621_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 2
USE WCA
SELECT * 
FROM v53f_621_Call
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 3
USE WCA
SELECT *  
FROM v50f_621_Liquidation
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 6
USE WCA
SELECT *  
FROM v51f_621_Preference_Conversion
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 7
use wca
SELECT *  
FROM v51f_621_Preference_Redemption
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 8
use wca
SELECT *  
FROM v51f_621_Security_Reclassification
WHERE
created >='2009/01/01' and created<'2010/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)


--# 12
use wca
SELECT *  
FROM v53f_621_Capital_Reduction
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 14
use wca
SELECT *  
FROM v53f_621_Takeover
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 15
use wca
SELECT *  
FROM v54f_621_Arrangement
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 16
use wca
SELECT *  
FROM v54f_621_Bonus
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 18
use wca
SELECT *  
FROM v54f_621_Consolidation
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 19
use wca
SELECT *  
FROM v54f_621_Demerger
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 20
use wca
SELECT *  
FROM v54f_621_Distribution
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 22
use wca
SELECT *  
FROM v54f_621_Entitlement
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 23
use wca
SELECT *  
FROM v54f_621_Merger
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 24
use wca
SELECT *  
FROM v54f_621_Preferential_Offer
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 25
use wca
SELECT *  
FROM v54f_621_Purchase_Offer
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 26
use wca
SELECT *  
FROM v54f_621_Rights 
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 27
use wca
SELECT *  
FROM v54f_621_Security_Swap 
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 28
use wca
SELECT * 
FROM v54f_621_Subdivision
WHERE
created >='2009/01/01' and created<'2010/01/01'


--# 40
use wca
SELECT *  
FROM v51f_621_Parvalue_Redenomination 
WHERE
created >='2009/01/01' and created<'2010/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)


--# 41
USE WCA
SELECT *  
FROM v51f_621_Currency_Redenomination 
WHERE
created >='2009/01/01' and created<'2010/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)


--# 42
USE WCA
SELECT *  
FROM v53f_621_Return_of_Capital 
WHERE
created >='2009/01/01' and created<'2010/01/01'
