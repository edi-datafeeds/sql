--filepath=o:\Datafeed\Equity\620_QuadCap\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620i_US_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\equity\620i_QuadCap\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--filefooterchk=y


--# 1
use wca2
SELECT * 
FROM t620i_Company_Meeting 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 2
use wca2
SELECT *
FROM t620i_Call
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 3
use wca
SELECT * 
FROM v50f_620_Liquidation_Extended
WHERE
changed>(select max(wca.dbo.tbl_opslog.acttime)-0.05 from wca.dbo.tbl_opslog)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 4
use wca2
SELECT *
FROM t620i_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 5
use wca2
SELECT * 
FROM t620i_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 6
use wca2
SELECT * 
FROM t620i_Conversion_Terms
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 7
use wca2
SELECT * 
FROM t620i_Redemption_Terms
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 8
use wca2
SELECT * 
FROM t620i_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 9
use wca2
SELECT * 
FROM t620i_Lot_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM t620i_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 11
use wca2
SELECT * 
FROM t620i_Buy_Back
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 12
use wca2
SELECT * 
FROM t620i_Capital_Reduction
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 13
use wca2
SELECT * 
FROM t620i_Takeover
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 14
use wca2
SELECT * 
FROM t620i_Arrangement
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 15
use wca2
SELECT * 
FROM t620i_Bonus
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 17
use wca2
SELECT * 
FROM t620i_Consolidation
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 18
use wca2
SELECT * 
FROM t620i_Demerger
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 19
use wca2
SELECT * 
FROM t620i_Distribution
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 20
use wca2
SELECT * 
FROM t620i_Divestment
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 21
use wca2
SELECT * 
FROM t620i_Entitlement
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 22
use wca
SELECT * 
FROM v54f_620_Merger_extended
WHERE
changed>(select max(wca.dbo.tbl_opslog.acttime)-0.05 from wca.dbo.tbl_opslog)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 23
use wca2
SELECT * 
FROM t620i_Preferential_Offer
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 24
use wca2
SELECT * 
FROM t620i_Purchase_Offer
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 25
use wca2
SELECT * 
FROM t620i_Rights 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 26
use wca2
SELECT * 
FROM t620i_Security_Swap 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM t620i_Subdivision
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 28
use wca2
SELECT *
FROM t620i_Bankruptcy 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 29
use wca2
SELECT *
FROM t620i_Financial_Year_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 30
use wca2
SELECT *
FROM t620i_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM t620i_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 32
use wca2
SELECT *
FROM t620i_Class_Action
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620i_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 34
use wca2
SELECT *
FROM t620i_Assimilation
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM t620i_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM t620i_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620i_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 38
use wca2
SELECT * 
FROM t620i_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 39
use wca2
SELECT * 
FROM t620i_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 40
use wca2
SELECT * 
FROM t620i_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 41
use wca2
SELECT * 
FROM t620i_Return_of_Capital 
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef


--# 42
use wca2
SELECT * 
FROM t620i_Dividend
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 43
use wca2
SELECT * 
FROM t620i_Dividend_Reinvestment_Plan
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 44
use wca2
SELECT * 
FROM t620i_Franking
WHERE
(excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 45
use wca2
SELECT * 
FROM t620i_Conversion_Terms_Change
WHERE
(SecStatus = 'X') 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620i_Bonus_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and (excountry='AU'
  or excountry='BE'
  or excountry='BR'
  or excountry='CA'
  or excountry='CH'
  or excountry='DE'
  or excountry='DK'
  or excountry='ES'
  or excountry='FI'
  or excountry='FR'
  or excountry='GB'
  or excountry='HK'
  or excountry='IN'
  or excountry='IT'
  or excountry='JP'
  or excountry='KR'
  or excountry='NL'
  or excountry='NO'
  or excountry='PL'
  or excountry='PT'
  or excountry='RU'
  or excountry='SE'
  or excountry='SG'
  or excountry='TR'
  or excountry='TW'
  or excountry='US'
or excountry='ZA')
ORDER BY CaRef
