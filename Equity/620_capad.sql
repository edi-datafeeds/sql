--filepath=o:\Datafeed\Equity\Extracts\
--filenameprefix=OLSO_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select 
scexh.secid,isin,
uscode,
statusflag,
localcode,
liststatus,
issuername 
from scexh
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join SEDOL on scmst.secid = sedol.secid
where (issur.issid = 20125 or issur.issid = 47145 or issur.issid = 47064 or issur.issid = 4645 or issur.issid = 48579 or issur.issid = 48505 or issur.issid = 34934)
ORDER BY scexh.secid


--# 2
use wca
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
issid = 20125 or issid = 47145 or issid = 47064 or issid = 4645 or issid = 48579 or issid = 48505 or issid = 34934
ORDER BY CaRef


--# 3
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
issid = 20125 or issid = 47145 or issid = 47064 or issid = 4645 or issid = 48579 or issid = 48505 or issid = 34934
ORDER BY CaRef

--# 4
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
issid = 20125 or issid = 47145 or issid = 47064 or issid = 4645 or issid = 48579 or issid = 48505 or issid = 34934
ORDER BY CaRef

--# 5
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
issid = 20125 or issid = 47145 or issid = 47064 or issid = 4645 or issid = 48579 or issid = 48505 or issid = 34934
ORDER BY CaRef

--# 6
use wca
SELECT *
FROM v52f_620_Listing_Status_Change
WHERE
issid = 20125 or issid = 47145 or issid = 47064 or issid = 4645 or issid = 48579 or issid = 48505 or issid = 34934
ORDER BY CaRef

--# 7
use wca
SELECT *
FROM v52f_620_Local_Code_Change
WHERE
issid = 20125 or issid = 47145 or issid = 47064 or issid = 4645 or issid = 48579 or issid = 48505 or issid = 34934
ORDER BY CaRef

--# 8
use wca
SELECT * 
FROM v52f_620_New_Listing
WHERE
issid = 20125 or issid = 47145 or issid = 47064 or issid = 4645 or issid = 48579 or issid = 48505 or issid = 34934
ORDER BY CaRef
