--filepath=o:\datafeed\equity\620_JP_ALERT\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=_ALERT
--fileheadertext=EDI_620_JP_ALERT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 101

USE WCA
SELECT * 
FROM v50f_620_Company_Meeting 
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (AGMdate>=getdate()+1 and AGMdate<getdate()+5)
order by changed


--# 103
USE WCA
SELECT * 
FROM v50f_620_Liquidation
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Liquidationdate>=getdate()+1 and Liquidationdate<getdate()+5)
order by changed


--# 105
USE WCA
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed



--# 106
USE WCA
SELECT * 
FROM v51f_620_Conversion_Terms
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Fromdate>=getdate()+1 and Fromdate<getdate()+5)
order by changed



--# 107
use wca
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Redemptiondate>=getdate()+1 and Redemptiondate<getdate()+5)
order by changed



--# 108
use wca
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed



--# 110
use wca
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed



--# 112
use wca
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed



--# 113
use wca
SELECT * 
FROM v53f_620_Takeover
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and ((Closedate>=getdate()+1 and Closedate<getdate()+5)
or (CmAcqdate>=getdate()+1 and CmAcqdate<getdate()+5))
order by changed



--# 114
use wca
SELECT * 
FROM v54f_620_Arrangement
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed


--# 115
use wca
SELECT * 
FROM v54f_620_Bonus
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 116
use wca
SELECT * 
FROM v54f_620_Bonus_Rights
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 117
use wca
SELECT * 
FROM v54f_620_Consolidation
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 118
use wca
SELECT * 
FROM v54f_620_Demerger
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed


--# 119
use wca
SELECT * 
FROM v54f_620_Distribution
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 121
use wca
SELECT * 
FROM v54f_620_Entitlement
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 122
use wca
SELECT * 
FROM v54f_620_Merger
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed


--# 123
use wca
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 124
use wca
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 125
use wca
SELECT * 
FROM v54f_620_Rights 
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 127
use wca
SELECT *
FROM v54f_620_Subdivision
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed



--# 128
use wca
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (NotificationDate>=getdate()+1 and NotificationDate<getdate()+5)
order by changed



--# 131
use wca
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (NameChangeDate>=getdate()+1 and NameChangeDate<getdate()+5)
order by changed



--# 133
use wca
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (DateOfChange>=getdate()+1 and DateOfChange<getdate()+5)
order by changed



--# 134
use wca
SELECT *
FROM v52f_620_Assimilation
WHERE
(ExCountry = 'JP')
and (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (Assimilationdate>=getdate()+1 and Assimilationdate<getdate()+5)
order by changed


--# 136
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed


--# 137
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed


--# 139
use wca
SELECT * 
FROM v52f_620_Assimilation
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (AssimilationDate>=getdate()+1 and AssimilationDate<getdate()+5)
order by changed


--# 139
use wca
SELECT * 
FROM v51f_620_Parvalue_Redenomination 
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed


--# 140
USE WCA
SELECT * 
FROM v51f_620_Currency_Redenomination 
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed


--# 141
USE WCA
SELECT * 
FROM v54f_620_Dividend
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed


--# 142
use wca
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Exdate>=getdate()+1 and Exdate<getdate()+5)
order by changed


--# 143
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed



--# 144
use wca
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed


--# 145
use wca
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Effectivedate>=getdate()+1 and Effectivedate<getdate()+5)
order by changed


--# 146
use wca
SELECT * 
FROM v53f_620_Buy_Back
WHERE
(ExCountry = 'JP') and 
(ListStatus <> 'D' or ListStatus is null)
and (Enddate>=getdate()+1 and Enddate<getdate()+5)
order by changed
