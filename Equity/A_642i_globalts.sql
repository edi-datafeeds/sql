--filepath=o:\Datafeed\Equity\642i_Globalts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.642
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\642i_Globalts\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--ZEROROWCHK=N 

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select
SDCHG.SdchgID as EventID,
'Sedol Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
sdchg.Actflag,
sdchg.eventtype as Reason,
case when sdchg.effectivedate is null
     then sdchg.announcedate
     else sdchg.effectivedate
     end as EffectiveDate,
sdchg.OldSedol as OldStatic,
sdchg.NewSedol as NewStatic
FROM SDCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = SDCHG.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
SDCHG.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')


--# 3
use wca
select
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
ICC.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')
and (icc.OldIsin <> '' or icc.NewIsin <> '')

--# 4
use wca
select
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
inchg.Actflag,
inchg.eventtype as Reason,
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic
FROM inchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = inchg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where
inchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')

--# 5
use wca
select
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
ischg.Actflag,
ischg.eventtype as Reason,
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic
FROM ischg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = ischg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
ischg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')

--# 6
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = lcc.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
lcc.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')

--# 7
use wca
select
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
scchg.Actflag,
scchg.eventtype as Reason,
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic
FROM scchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = scchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
scchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')

--# 8
use wca
select
LTCHG.LTCHGID as EventID,
'Lot Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = ltchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
ltchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')
and (ltchg.OldLot <> ''
or ltchg.NewLot <> '')

--# 9
use wca
select
LTCHG.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldMinTrdQty as OldStatic,
ltchg.NewMinTrdgQty as NewStatic
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = ltchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where
ltchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')
and (ltchg.OldMinTrdQty <> ''
or ltchg.NewMinTrdgQty <> '')

--# 10
use wca
select
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic
FROM lstat
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = lstat.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where
lstat.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')

--# 11
use wca
select
nlist.scexhID as EventID,
'New Listing' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
nlist.Actflag,
case when v20c_EV_dSCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when v20c_EV_dSCEXH.Listdate is not null 
     then v20c_EV_dSCEXH.Listdate 
     else nlist.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic
FROM nlist
INNER JOIN v20c_EV_dSCEXH ON nlist.scexhid = v20c_EV_dSCEXH.scexhid
INNER JOIN v20c_EV_SCMST ON v20c_EV_dSCEXH.secid = v20c_EV_SCMST.secid
where
nlist.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')

--# 12
use wca
select
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.EffectiveDate is null
     then icc.announcedate
     else icc.effectivedate
     end as EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
ICC.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')
and (icc.OldUSCode <> ''
or icc.NewUSCode <> '')

--# 13
use wca
select
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic
FROM Currd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = currd.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
currd.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')

/*
use wca
select
case when v20c_EV_dSCEXH.scexhid is null then 'S'+cast(v20c_EV_SCMST.Secid as varchar(10)) else 'X'+cast(v20c_EV_dSCEXH.scexhid as varchar(10)) end as RowKey,
'Par Value Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.StructCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
pvrd.Actflag,
pvrd.eventtype as Reason,
pvrd.EffectiveDate,
pvrd.OldParValue as OldStatic,
pvrd.NewParValue as NewStatic
FROM pvrd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = pvrd.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
where
pvrd.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog)
and (excountry = 'AU' or excountry = 'GB' or excountry = 'NZ')
*/