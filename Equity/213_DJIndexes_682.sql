--filepath=o:\Datafeed\Equity\682\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.682
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_682_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\682\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventCD' as f1,
'EventID' as f2,
'Sedol' as f3,
'ExchgCD as f4,
'IssID' as f2,
'SecID' as f2,
'ScexhID' as f2,
'Created' as f7,
'Changed' as f8,
'Actflag' as f9,
'Issuername' as f10,
'SectyCD' as f11,
'PVCurrency' as f11,
'Isin' as f12,
'Localcode' as f13,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select
stab.SdchgID as EventID,
'SDCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
stab.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldSedol as OldStatic,
stab.NewSedol as NewStatic
FROM SDCHG as stab
INNER JOIN SCMST ON stab.SecID = scmst.secid
INNER JOIN ISSUR ON SCMST.issID = issur.issID
inner join SCEXH ON stab.SecID = SCEXH.SecID
inner join exchg ON scexh.exchgcd = exchg.exchgcd
INNER JOIN sedol ON scmst.secID = sedol.secid and exchg.cntrycd=sedol.cntrycd
where
(stab.OldSedol <> '' or stab.NewSedol <> '')
and ((stab.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3))
and sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='u'))
or 
(sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='i'))


--# 3
use wca
select
stab.IccID as EventID,
'ICC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldIsin as OldStatic,
stab.NewIsin as NewStatic
FROM ICC as stab
INNER JOIN SCMST ON SCMST.SecID = stab.SecID
LEFT OUTER JOIN SCEXH ON stab.SecID = SCEXH.SecID
where
stab.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (icc.OldIsin <> '' or icc.NewIsin <> '')


--# 3
use wca
select
stab.IccID as EventID,
'USCC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
FROM ICC as stab
INNER JOIN SCMST ON SCMST.SecID = stab.SecID
LEFT OUTER JOIN SCEXH ON stab.SecID = SCEXH.SecID
where
stab.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (icc.OldUscode <> '' or icc.NewUscode <> '')



--# 4
use wca
select
INCHG.INCHGID as EventID,
'INCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
FROM ICC as stab
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic
FROM inchg
INNER JOIN SCMST ON SCMST.issid = inchg.issid
LEFT OUTER JOIN dSCEXH ON SCMST.secid = dSCEXH.secID
where
inchg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 5
use wca
select
ISCHG.ISCHGID as EventID,
'ISCHG' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
FROM ICC as stab
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic
FROM ischg
INNER JOIN SCMST ON SCMST.issid = ischg.issid
LEFT OUTER JOIN dSCEXH ON SCMST.secid = dSCEXH.secID
where 
ischg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 6
use wca
select
LCC.LCCID as EventID,
'LCC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
FROM ICC as stab
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN SCMST ON SCMST.secid = lcc.secid
LEFT OUTER JOIN dSCEXH ON SCMST.secid = dSCEXH.secID
where 
lcc.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 7
use wca
select
SCCHG.SCCHGID as EventID,
'SCCHG' as EventCD,
stab.IccID as EventID,
'ICC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
FROM ICC as stab
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic
FROM scchg
INNER JOIN SCMST ON SCMST.secid = scchg.secid
LEFT OUTER JOIN dSCEXH ON SCMST.secid = dSCEXH.secID
where 
scchg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 8
use wca
select
LTCHG.LTCHGID as EventID,
'LTCHG' as EventCD,
stab.IccID as EventID,
'ICC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
case when scmst.statusflag='I' then 'D' else SCEXH.ListStatus end as ListStatus,

ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic
FROM ltchg
INNER JOIN SCMST ON SCMST.secid = ltchg.secid
LEFT OUTER JOIN dSCEXH ON SCMST.secid = dSCEXH.secID
where 
ltchg.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and (ltchg.OldLot <> ''
or ltchg.NewLot <> '')

--# 10
use wca
select
LSTAT.LSTATID as EventID,
'LSTAT' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
case when scmst.statusflag='I' then 'D' else SCEXH.ListStatus end as ListStatus,

SCMST.secstatus as StatusFlag,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic
FROM lstat
INNER JOIN SCMST ON SCMST.secid = lstat.secid
LEFT OUTER JOIN dSCEXH ON SCMST.secid = dSCEXH.secID
where
lstat.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

--# 11
use wca
select
nlist.scexhID as EventID,
'NLIST' as EventCD,
stab.IccID as EventID,
'ICC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
case when scmst.statusflag='I' then 'D' else SCEXH.ListStatus end as ListStatus,
nlist.Actflag,
case when dSCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when dSCEXH.Listdate is not null 
     then dSCEXH.Listdate 
     else nlist.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic
FROM nlist
INNER JOIN dSCEXH ON nlist.scexhid = dSCEXH.scexhid
INNER JOIN SCMST ON dSCEXH.secid = SCMST.secid
where
nlist.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)


--# 13
use wca
select
CURRD.CURRDID as EventID,
'CURRD' as EventCD,
stab.IccID as EventID,
'ICC' as EventCD,
sedol.sedol,
scexh.exchgcd as ExchgCD,
scmst.IssID,
scmst.SecID,
scexh.ScexhID,
stab.AnnounceDate as Created,
stab.Acttime as Changed,
stab.ActFlag,
issur.issuername,
scmst.SectyCD,
scmst.CurenCD as PVCurrency,
scmst.isin as Isin,
scexh.localcode as Localcode,
case when scmst.statusflag='I' then 'D' when SCEXH.ListStatus = 'N' or SCEXH.ListStatus = '' then 'L' else SCEXH.ListStatus end as ListStatus,
sdchg.eventtype as Reason,
case when stab.effectivedate is null
     then stab.announcedate
     else stab.effectivedate
     end as EffectiveDate,
stab.OldUscode as OldStatic,
stab.NewUscode as NewStatic
case when scmst.statusflag='I' then 'D' else SCEXH.ListStatus end as ListStatus,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic
FROM Currd
INNER JOIN SCMST ON SCMST.SecID = currd.SecID
LEFT OUTER JOIN dSCEXH ON SCMST.SecID = dSCEXH.SecID
where
currd.Acttime >= (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
