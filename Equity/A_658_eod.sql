--filepath=o:\Datafeed\Equity\658\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.658
--suffix=
--fileheadertext=EDI_658_LISTDATA_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\658\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select
SCMST.SecID,
SCMST.IssID,
scmst.Isin,
scmst.Uscode,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.Sedol else '' end as Sedol,
ISSUR.IssuerName,
ISSUR.CntryofIncorp,
SCMST.SecurityDesc,
SCMST.SectyCD,
SCMST.PrimaryExchgCD,
SCMST.SharesOutstanding,
SCEXH.ExchgCD,
SCEXH.LocalCode,
scexh.listDate,
SCEXH.ListStatus,
case when scexh.actflag is not null then scexh.actflag else scmst.actflag end as RecordStatus
FROM scexh
INNER JOIN SCMST ON SCEXH.SecID = SCMST.SecID
INNER JOIN ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join sedol on scexh.secid = sedol.secid and substring(scexh.exchgcd,1,2) = sedol.cntrycd
where
scmst.sectycd<>'CW'
and (scexh.acttime>=(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 1)
or scmst.acttime>=(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 1)
or issur.acttime>=(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 1))
