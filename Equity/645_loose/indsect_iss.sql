--filepath=o:\Datafeed\Equity\indsect\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_ISS
--fileheadertext=EDI_INDSECT_ISS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT distinct
issur.Actflag,
issur.IssID,
issur.IssuerName,
issur.IndusID
FROM issur
where
issur.actflag<>'D'
and issur.indusid<>0
and issur.indusid is not null
