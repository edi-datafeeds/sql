--filepath=o:\Datafeed\Equity\645_loose\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_EQUITY_645_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\645_loose\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE
scmst.sectycd <> 'BND'
and sectygrp.secgrpid<3
and sedol.acttime>=(select max(feeddate) from tbl_Opslog where seq = 3)


