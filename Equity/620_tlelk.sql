--filepath=o:\Datafeed\Equity\620_tlelk\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(feeddate),112) from wca.dbo.tbl_opslog where seq=3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\620\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 17
use wca
SELECT * 
FROM v54f_620_Consolidation_TLELK
WHERE
excountry='HK'
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 27
use wca
SELECT *
FROM v54f_620_Subdivision_TLELK
WHERE
excountry='HK'
and changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
