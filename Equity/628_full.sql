--filepath=o:\Datafeed\Equity\628\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.628
--suffix=
--fileheadertext=EDI_NONDEBT_628_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\628\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
case when scmst.Actflag<>'D' then '' ELSE scmst.Actflag END as Actflag,
scmst.SectyCD,
case 
when scexh.Liststatus = 'R' then upper('A')
when scexh.Liststatus = 'S' then upper('S')
when scmst.statusflag = '' then upper('A')
when scmst.statusflag = 'I' then upper('I')
when scmst.statusflag = 'A' then upper('A')
ELSE upper('A')
end as statusflag,
scmst.CurenCD as PVCurrency,
scmst.ParValue,
issur.Issuername,
scmst.Securitydesc,
scmst.IssID,
issur.CntryofIncorp,
case
when sedol.actflag <> 'D' then Sedol.Sedol
else '' end as Sedol,
scmst.Isin,
scmst.Uscode,
scmst.SecID,
'1' as DENOM1,
case when dprcp.unsecid is not null then dprcp.unsecid
     when wartm.wtexersecid is not null then wartm.wtexersecid
     ELSE '' END as UnSecID,
case when unscmst.secid is not null then unscmst.curencd
     when exscmst.secid is not null then exscmst.curencd
     ELSE '' END as UnCurrency,
exchg.mic as Mic,
substring(scexh.Localcode,1,20) as Localcode,
Sedol.RcntryCD
FROM scmst
inner JOIN scexh ON scmst.SecID = scexh.SecID
                 and scexh.exchgcd = scmst.primaryexchgcd
left outer join dprcp on scmst.secid = dprcp.secid
                 and dprcp.actflag<>'D'
left outer join wartm on scmst.secid = wartm.secid
                 and wartm.actflag<>'D'
left outer join scmst as unscmst on dprcp.secid = unscmst.secid
left outer join scmst as exscmst on wartm.wtexersecid = exscmst.secid
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                 AND exchg.cntryCD = Sedol.CntryCD
where
scmst.actflag <> 'D'
and scmst.sectycd <> 'BND'
and scexh.actflag <> 'D'
and (scmst.statusflag <> 'I' or scmst.statusflag is null)
