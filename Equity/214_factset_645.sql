--filepath=c:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.645
--suffix=
--fileheadertext=EDI_STATIC_645_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\upload\acc\214\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT distinct
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
left outer join scmst on issur.issid = scmst.issid
WHERE
(issur.actflag<>'D' and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='I'))
or
(issur.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3) and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='U'))


--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM scmst
WHERE 
(scmst.actflag<>'D' and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='I'))
or
(scmst.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3) and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='U'))


--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE
(sedol.actflag<>'D' and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='I'))
or
(sedol.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3) and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='U'))




--# 4
use WCA
SELECT distinct
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM scexh
inner join scmst on scexh.secid = scmst.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
WHERE
(scexh.actflag<>'D' and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='I'))
or
(scexh.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3) and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='U'))



--# 5
use WCA
SELECT distinct
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
Exchg.CntryCD,
Exchg.MIC
from EXCHG
inner join scexh on exchg.exchgcd = scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
WHERE
(exchg.actflag<>'D' and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='I'))
or
(exchg.acttime> (select max(feeddate) from wca.dbo.tbl_opslog where seq=3) and
scmst.isin in (select code from client.dbo.pfisin where accid=214 and actflag='U'))
