--filepath=o:\Datafeed\Equity\642i_Markit\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog 
--fileextension=.642
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\642i_Markit\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n

--# 1
use wca
select distinct
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21,
'Created' as f22,
'MIC' as f23,
'Industry' as f24



--# 3
use wca
select distinct
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic,
icc.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
OldIsin<>NewIsin
and icc.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))


--# 4
use wca
select distinct
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
inchg.Actflag,
inchg.eventtype as Reason,
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic,
inchg.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM inchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = inchg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
OldCntryCD<>NewCntryCD
and inchg.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))

--# 5
use wca
select distinct
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
ischg.Actflag,
ischg.eventtype as Reason,
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic,
ischg.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM ischg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = ischg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
IssOldName<>IssNewName
and ischg.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))


--# 7
use wca
select distinct
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
scchg.Actflag,
scchg.eventtype as Reason,
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic,
scchg.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM scchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = scchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
SecOldName<>SecNewName
and scchg.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))


--# 10
use wca
select distinct
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
case when v20c_EV_dscexh.liststatus <> lstat.LStatStatus then v20c_EV_dscexh.liststatus else ' ' end as OldStatic,
lstat.LStatStatus as NewStatic,
lstat.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM lstat
INNER JOIN v20c_EV_dSCEXH ON lstat.secid = v20c_EV_dSCEXH.secID
                      and lstat.exchgcd = v20c_EV_dSCEXH.exchgcd
INNER JOIN v20c_EV_SCMST ON v20c_EV_dSCEXH.secID = v20c_EV_SCMST.secid
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
lstat.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))


--# 11
use wca
select distinct
nlist.scexhID as EventID,
'New Listing' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
nlist.Actflag,
case when v20c_EV_dSCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when v20c_EV_dSCEXH.Listdate is not null 
     then v20c_EV_dSCEXH.Listdate 
     else nlist.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic,
nlist.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM nlist
INNER JOIN v20c_EV_dSCEXH ON nlist.scexhid = v20c_EV_dSCEXH.scexhid
INNER JOIN v20c_EV_SCMST ON v20c_EV_dSCEXH.secid = v20c_EV_SCMST.secid
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
nlist.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))


--# 12
use wca
select distinct
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.EffectiveDate is null
     then icc.announcedate
     else icc.effectivedate
     end as EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic,
icc.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
OldUSCode<>NewUSCode
and icc.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))


--# 13
use wca
select distinct
CURRD.CURRDID as EventID,
'Redenomination Currency Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.primaryexchgcd)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic,
currd.AnnounceDate as Created,
v20c_EV_dscexh.MIC,
indus.IndusName
FROM Currd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = currd.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.indus on v20c_EV_SCMST.IndusID = wca.dbo.indus.IndusID
left outer join wca.dbo.dprcp on v20c_EV_SCMST.secid = wca.dbo.dprcp.secid
where
OldCurenCD<>NewCurenCD
and currd.Acttime >= (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
and v20c_EV_SCMST.primaryexchgcd=v20c_EV_dSCEXH.ExchgCD
and (v20c_EV_SCMST.secid in (select secid from portfolio.dbo.MKLIST)
or ((wca.dbo.dprcp.drtype <> 'CDI' or wca.dbo.dprcp.drtype is null)
and (wca.dbo.dprcp.drtype <> 'NDR' or wca.dbo.dprcp.drtype is null)
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and v20c_EV_SCMST.sectycd <> 'SP'
and v20c_EV_SCMST.sectycd <> 'STP'
and v20c_EV_SCMST.sectycd <> 'SS'
and wca.dbo.sectygrp.secgrpid<3
and (
substring(v20c_EV_SCMST.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or v20c_EV_SCMST.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East'))
))
