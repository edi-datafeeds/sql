--filepath=o:\Datafeed\wca\align\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\WCA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
scmst.Actflag,
scmst.AnnounceDate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.FYENPPDate,
scmst.USCode,
scmst.ISIN,
scmst.VALOREN,
scmst.X,
scmst.CFI,
scmst.ScmstNotes, 
scmst.SharesOutstanding,
scmst.UmprgID
from scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
where 
uscode = '3136F7KF3'
or uscode = '906667BB5'
or uscode = '987049AB9'
or isin = 'ARARGE030056'
or isin = 'BE0000296054'
or isin = 'BE0008102510'
or isin = 'BE0008120694'
or isin = 'CRBCUSTB0772'
or isin = 'CZ0003501058'
or isin = 'TW0002444106'
or isin = 'XS0099436171'
or isin = 'XS0131670449'
or isin = 'XS0136200911'
or isin = 'XS0146567580'
or isin = 'XS0153275960'
or isin = 'XS0160473418'
or isin = 'XS0174143130'
or isin = 'XS0180943713'
or isin = 'XS0192354594'
or isin = 'XS0218115649'
or isin = 'XS0221690844'
or isin = 'XS0226747904'
or isin = 'XS0236640032'
or isin = 'ZAG000023797'
