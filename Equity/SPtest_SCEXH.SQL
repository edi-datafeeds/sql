--filepath=o:\Datafeed\wca\align\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=O:\WCA\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
scexh.Actflag,
scexh.AnnounceDate,
scexh.SecID,
scexh.ExchgCD,
scexh.ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.TradeStatus,
scexh.LocalCode,
'' as RTSCD,
'' AS BLTICK,
scexh.ScExhID,
scexh.ScexhNotes
FROM scexh
LEFT OUTER JOIN SCMST ON SCEXH.SECID = SCMST.SECID
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
where 
(issur.CntryofIncorp = 'BE'
or issur.CntryofIncorp = 'BG'
or issur.CntryofIncorp = 'BH'
or issur.CntryofIncorp = 'BO'
or issur.CntryofIncorp = 'BW'
or issur.CntryofIncorp = 'CR'
or issur.CntryofIncorp = 'CY'
or issur.CntryofIncorp = 'CZ'
or issur.CntryofIncorp = 'EG'
or issur.CntryofIncorp = 'FO'
or issur.CntryofIncorp = 'FR'
or issur.CntryofIncorp = 'HK'
or issur.CntryofIncorp = 'HU'
or issur.CntryofIncorp = 'IS'
or issur.CntryofIncorp = 'JO'
or issur.CntryofIncorp = 'KZ'
or issur.CntryofIncorp = 'NZ'
or issur.CntryofIncorp = 'PE'
or issur.CntryofIncorp = 'SK'
or issur.CntryofIncorp = 'SZ'
or issur.CntryofIncorp = 'TW' or issur.CntryofIncorp = 'AT' or issur.CntryofIncorp = 'NL' or issur.CntryofIncorp = 'FR' or issur.CntryofIncorp = 'AN' or issur.CntryofIncorp = 'GB' or issur.CntryofIncorp = 'LU' or issur.CntryofIncorp = 'CH' or issur.CntryofIncorp = 'SE' or issur.CntryofIncorp = 'DE'
or issur.CntryofIncorp = 'VN')
and scmst.statusflag <> 'I'
and scmst.actflag <> 'D'
and scexh.actflag <> 'D'
and scexh.Liststatus <> 'D'
