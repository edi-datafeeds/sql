--filepath=o:\Datafeed\Equity\653_full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),12) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.653
--suffix=
--fileheadertext=EDI_FLATSRF_653_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
exchg.cntrycd,
exchg.mic,
scmst.secid,
scmst.isin,
scexh.Localcode,
issur.issuername, 
scmst.issid,
scmst.securitydesc,
scexh.exchgcd,
scmst.sectycd,
scmst.primaryexchgcd,
case when scmst.statusflag<>'I' then 'A' else 'I' end as globalscmststatus,
case when scexh.liststatus is null then 'U' when scexh.liststatus='N' or scexh.liststatus='' then 'L' else scexh.liststatus end as wcaliststatus,
scmst.acttime as changed,
scmst.actflag as ScmstRecordStatus,
scexh.actflag as ScexhRecordStatus
from scmst
inner join issur on scmst.issid = issur.issid
left outer join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
where
scmst.actflag<>'D'
and scexh.actflag<>'D'
and sectygrp.secgrpid<3
and not (cntrycd='BE'
or cntrycd='CA'
or cntrycd='CH'
or cntrycd='DE'
or cntrycd='DK'
or cntrycd='ES'
or cntrycd='FI'
or cntrycd='FR'
or cntrycd='IT'
or cntrycd='NL'
or cntrycd='NO'
or cntrycd='PT'
or cntrycd='SE'
or cntrycd='US'
or cntrycd='ZA')
