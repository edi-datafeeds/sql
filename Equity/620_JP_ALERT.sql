--filepath=o:\datafeed\equity\620_JP_ALERT\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=_ALERT
--fileheadertext=EDI_620_JP_ALERT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 101

USE WCA
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v50f_620_Company_Meeting 
WHERE
(ListStatus <> 'D' or ListStatus is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((AGMdate>getdate()+1+@off1 and AGMdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (AGMdate>getdate()+@off2 and AGMdate<getdate()+1+@off2)))
and ExCountry = 'JP' 


--# 103
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v50f_620_Liquidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Liquidationdate>getdate()+1+@off1 and Liquidationdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Liquidationdate>getdate()+@off2 and Liquidationdate<getdate()+1+@off2)))
and ExCountry = 'JP'

--# 105
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v51f_620_International_Code_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'



--# 106
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v51f_620_Conversion_Terms
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Fromdate>getdate()+1+@off1 and Fromdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Fromdate>getdate()+@off2 and Fromdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 107
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v51f_620_Redemption_Terms
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Redemptiondate>getdate()+1+@off1 and Redemptiondate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Redemptiondate>getdate()+@off2 and Redemptiondate<getdate()+1+@off2)))
and ExCountry = 'JP'




--# 108
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v51f_620_Security_Reclassification
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'



--# 110
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v52f_620_Sedol_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'



--# 112
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v53f_620_Capital_Reduction
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'



--# 113
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v53f_620_Takeover
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 

and (((Closedate>getdate()+1+@off1 and Closedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Closedate>getdate()+@off2 and Closedate<getdate()+1+@off2)))

or ((CmAcqdate>getdate()+1+@off1 and CmAcqdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (CmAcqdate>getdate()+@off2 and CmAcqdate<getdate()+1+@off2))))

and ExCountry = 'JP'


--# 114
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Arrangement
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 115
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Bonus
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 116
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Bonus_Rights
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 117
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Consolidation
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 118
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Demerger
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'

--# 119
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Distribution
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'



--# 121
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Entitlement
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 122
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Merger
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 123
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Preferential_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'



--# 124
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Purchase_Offer
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 125
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Rights 
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'



--# 127
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT *
FROM v54f_620_Subdivision
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 128
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT *
FROM v50f_620_Bankruptcy 
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((NotificationDate>getdate()+1+@off1 and NotificationDate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (NotificationDate>getdate()+@off2 and NotificationDate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 131
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT *
FROM v50f_620_Issuer_Name_change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((NameChangeDate>getdate()+1+@off1 and NameChangeDate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (NameChangeDate>getdate()+@off2 and NameChangeDate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 133
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT *
FROM v51f_620_Security_Description_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((DateOfChange>getdate()+1+@off1 and DateOfChange<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (DateOfChange>getdate()+@off2 and DateOfChange<getdate()+1+@off2)))
and ExCountry = 'JP'





--# 136
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 137
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'




--# 139
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v52f_620_Assimilation
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Assimilationdate>getdate()+1+@off1 and Assimilationdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Assimilationdate>getdate()+@off2 and Assimilationdate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 139
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v51f_620_Parvalue_Redenomination 
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 140
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v51f_620_Currency_Redenomination 
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 141
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v54f_620_Dividend
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Exdate>getdate()+1+@off1 and Exdate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Exdate>getdate()+@off2 and Exdate<getdate()+1+@off2)))
and ExCountry = 'JP'




--# 144
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v53f_620_Return_Of_Capital
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 145
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Effectivedate>getdate()+1+@off1 and Effectivedate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Effectivedate>getdate()+@off2 and Effectivedate<getdate()+1+@off2)))
and ExCountry = 'JP'


--# 146
use wca
Declare @off1 integer
Declare @off2 integer
set @off1 = 0
set @off2 = 0
SET DATEFIRST 1
if DATEPART(dw, GETDATE())=4
  begin
  set @off1 = 2
  set @off2 = 0
  end
if DATEPART(dw, GETDATE())=5
  begin
  set @off1 = 2
  set @off2 = 2
  end
SELECT * 
FROM v53f_620_Buy_Back
WHERE
(ListStatus <> 'D' or ListStatus is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and ((Enddate>getdate()+1+@off1 and Enddate<getdate()+2+@off1)
or (changed>=(select max(acttime) from tbl_Opslog where seq = 1)
    and (Enddate>getdate()+@off2 and Enddate<getdate()+1+@off2)))
and ExCountry = 'JP'
