--filepath=o:\Datafeed\Equity\642_IRESS\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.642
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N 

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 6
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc.NewLocalCode as NewStatic
FROM lcc
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = lcc.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
where 
lcc.acttime > (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and lcc.OldLocalCode<>lcc.NewLocalCode
and lcc.eventtype<>'CLEAN'
and (excountry='BE'
or excountry='CA'
or excountry='CH'
or excountry='CN'
or excountry='DE'
or excountry='DK'
or excountry='ES'
or excountry='FI'
or excountry='FR'
or excountry='GB'
or excountry='HK'
or excountry='ID'
or excountry='IE'
or excountry='IN'
or excountry='IT'
or excountry='JP'
or excountry='KR'
or excountry='MX'
or excountry='MY'
or excountry='NL'
or excountry='NO'
or excountry='PH'
or excountry='PT'
or excountry='SE'
or excountry='SG'
or excountry='TH'
or excountry='TW'
or excountry='US')
