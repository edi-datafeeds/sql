--filepath=o:\Datafeed\Equity\620_hht\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_hht\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
SELECT *  
FROM t620_Company_Meeting 
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
use wca2
SELECT * 
FROM t620_Call
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
use wca2
SELECT *  
FROM t620_Liquidation
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 4
use wca2
SELECT * 
FROM t620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 5
use wca2
SELECT *  
FROM t620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 6
use wca2
SELECT *  
FROM t620_Conversion_Terms
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca2
SELECT *  
FROM t620_Redemption_Terms
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 8
use wca2
SELECT *  
FROM t620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 9
use wca2
SELECT *  
FROM t620_Lot_Change
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca2
SELECT *  
FROM t620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 11
use wca2
SELECT *  
FROM t620_Buy_Back
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 12
use wca2
SELECT *  
FROM t620_Capital_Reduction
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 14
use wca2
SELECT *  
FROM t620_Takeover
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 15
use wca2
SELECT *  
FROM t620_Arrangement
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 16
use wca2
SELECT *  
FROM t620_Bonus
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 17
use wca2
SELECT *  
FROM t620_Bonus_Rights
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 18
use wca2
SELECT *  
FROM t620_Consolidation
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 19
use wca2
SELECT *  
FROM t620_Demerger
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 20
use wca2
SELECT *  
FROM t620_Distribution
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 21
use wca2
SELECT *  
FROM t620_Divestment
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 22
use wca2
SELECT *  
FROM t620_Entitlement
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 23
use wca2
SELECT *  
FROM t620_Merger
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 24
use wca2
SELECT *  
FROM t620_Preferential_Offer
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 25
use wca2
SELECT *  
FROM t620_Purchase_Offer
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 26
use wca2
SELECT *  
FROM t620_Rights 
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 27
use wca2
SELECT *  
FROM t620_Security_Swap 
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 28
use wca2
SELECT * 
FROM t620_Subdivision
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 29
use wca2
SELECT * 
FROM t620_Bankruptcy 
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca2
SELECT * 
FROM t620_Financial_Year_Change
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca2
SELECT * 
FROM t620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca2
SELECT * 
FROM t620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT * 
FROM t620_Class_Action
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 34
use wca2
SELECT * 
FROM t620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 35
use wca2
SELECT * 
FROM t620_Assimilation
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 36
use wca2
SELECT * 
FROM t620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca2
SELECT *  
FROM t620_New_Listing
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca2
SELECT *  
FROM t620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 40
use wca2
SELECT *  
FROM t620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 41
use wca2
SELECT *  
FROM t620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 42
use wca2
SELECT *  
FROM t620_Return_of_Capital 
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 43
use wca2
SELECT *  
FROM t620_Dividend
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 44
use wca2
SELECT *  
FROM t620_Dividend_Reinvestment_Plan
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 45
use wca2
SELECT *  
FROM t620_Franking
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
WHERE
(ExCountry = 'US' or ExCountry = 'HK')
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
