--filepath=o:\Datafeed\Equity\620_msci_new\
--filenameprefix=new_
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_msci_new\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca2
SELECT *  
FROM t620_Franking
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and frankflag<>'N'
ORDER BY CaRef

