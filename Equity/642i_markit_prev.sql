--filepath=o:\Datafeed\Equity\642i_Markit\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
--fileextension=.642
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_642_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\642i_Markit\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45'
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'Issuername' as f5,
'CntryofIncorp' as f6,
'SecurityDesc' as f7,
'SectyCD' as f8,
'Isin' as f9,
'Uscode' as f10,
'Sedol' as f11,
'RegCountry' as f12,
'ExchgCD' as f13,
'Localcode' as f14,
'ListStatus' as f15,
'StatusFlag' as f16,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21,
'Created' as f22


--# 2
use wca
select
SDCHG.SdchgID as EventID,
'Sedol Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
sdchg.Actflag,
sdchg.eventtype as Reason,
case when sdchg.effectivedate is null
     then sdchg.announcedate
     else sdchg.effectivedate
     end as EffectiveDate,
sdchg.OldSedol as OldStatic,
sdchg.NewSedol as NewStatic,
sdchg.AnnounceDate as Created
FROM SDCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = SDCHG.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
SDCHG.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 3
use wca
select
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic,
icc.AnnounceDate as Created
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
ICC.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and (icc.OldIsin <> '' or icc.NewIsin <> '')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')

--# 4
use wca
select
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
inchg.Actflag,
inchg.eventtype as Reason,
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic,
inchg.AnnounceDate as Created
FROM inchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = inchg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
inchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')

--# 5
use wca
select
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
ischg.Actflag,
ischg.eventtype as Reason,
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic,
ischg.AnnounceDate as Created
FROM ischg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = ischg.issid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where 
ischg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 6
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic,
lcc.AnnounceDate as Created
FROM lcc
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = lcc.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where 
lcc.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 7
use wca
select
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
scchg.Actflag,
scchg.eventtype as Reason,
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic,
scchg.AnnounceDate as Created
FROM scchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = scchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where 
scchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 8
use wca
select
LTCHG.LTCHGID as EventID,
'Lot Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic,
ltchg.AnnounceDate as Created
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = ltchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where 
ltchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and (ltchg.OldLot <> '' or ltchg.NewLot <> '')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 9
use wca
select
LTCHG.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldMinTrdQty as OldStatic,
ltchg.NewMinTrdgQty as NewStatic,
ltchg.AnnounceDate as Created
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = ltchg.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
ltchg.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and (ltchg.OldMinTrdQty <> '' or ltchg.NewMinTrdgQty <> '')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 10
use wca
select
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic,
lstat.AnnounceDate as Created
FROM lstat
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = lstat.secid
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.secid = v20c_EV_dSCEXH.secID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
lstat.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 11
use wca
select
nlist.scexhID as EventID,
'New Listing' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
nlist.Actflag,
case when v20c_EV_dSCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when v20c_EV_dSCEXH.Listdate is not null 
     then v20c_EV_dSCEXH.Listdate 
     else nlist.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic,
nlist.AnnounceDate as Created
FROM nlist
INNER JOIN v20c_EV_dSCEXH ON nlist.scexhid = v20c_EV_dSCEXH.scexhid
INNER JOIN v20c_EV_SCMST ON v20c_EV_dSCEXH.secid = v20c_EV_SCMST.secid
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
nlist.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 12
use wca
select
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
/*v20c_EV_SCMST.StructCD,*/
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
icc.Actflag,
icc.eventtype as Reason,
case when icc.EffectiveDate is null
     then icc.announcedate
     else icc.effectivedate
     end as EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic,
icc.AnnounceDate as Created
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
ICC.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and (icc.OldUSCode <> '' or icc.NewUSCode <> '')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 13
use wca
select
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic,
currd.AnnounceDate as Created
FROM Currd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = currd.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
currd.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and (currd.OldCurenCD <> '' or currd.NewCurenCD <> '')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')


--# 14
use wca
select
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.SecurityDesc,
rtrim(v20c_EV_SCMST.SectyCD) as SectyCD,
v20c_EV_SCMST.Isin,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_dscexh.RegCountry,
case when v20c_EV_dSCEXH.scexhid is null 
     then rtrim(v20c_EV_SCMST.PrimaryExchgCD)
     else rtrim(v20c_EV_dSCEXH.ExchgCD)
     end as ExchgCD,
v20c_EV_dSCEXH.Localcode,
rtrim(v20c_EV_dSCEXH.ListStatus) as ListStatus,
rtrim(v20c_EV_SCMST.StatusFlag) as Statusflag,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldParvalue as OldStatic,
currd.NewParvalue as NewStatic,
currd.AnnounceDate as Created
FROM Currd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = currd.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join wca.dbo.sectygrp on v20c_EV_SCMST.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on v20c_EV_SCMST.SecID = wca.dbo.dprcp.secid
where
currd.Acttime = (select max(acttime) from wca.dbo.tbl_Opslog where acttime='2009/06/30 13:32:45')
and (currd.OldParvalue <> '' or currd.NewParvalue <> '')
and v20c_EV_SCMST.sectycd <> 'DR'
and v20c_EV_SCMST.sectycd <> 'UNT'
and v20c_EV_SCMST.sectycd <> 'CDI'
and v20c_EV_SCMST.sectycd <> 'TRT'
and v20c_EV_SCMST.sectycd <> 'DRT'
and sectygrp.secgrpid=1
and (excountry = 'AT'
or excountry = 'BE'
or excountry = 'BG'
or excountry = 'CH'
or excountry = 'CY'
or excountry = 'CZ'
or excountry = 'DE'
or excountry = 'DK'
or excountry = 'EE'
or excountry = 'ES'
or excountry = 'FI'
or excountry = 'FR'
or excountry = 'GB'
or excountry = 'GR'
or excountry = 'HU'
or excountry = 'IE'
or excountry = 'IS'
or excountry = 'IT'
or excountry = 'LT'
or excountry = 'LU'
or excountry = 'LV'
or excountry = 'MT'
or excountry = 'NL'
or excountry = 'NO'
or excountry = 'PL'
or excountry = 'PT'
or excountry = 'RO'
or excountry = 'SE'
or excountry = 'SI'
or excountry = 'SK')
