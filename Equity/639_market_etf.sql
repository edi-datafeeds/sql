--filepath=o:\Datafeed\Equity\639_full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=select max(feeddate) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.639
--suffix=
--fileheadertext=EDI_STATIC_639_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\639\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM issur
inner join scmst on issur.issid = scmst.issid
where
issur.actflag<>'D'
and scmst.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and scmst.sectycd = 'ETF'
and (
substring(scmst.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or issur.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
)


--# 2
use WCA
SELECT distinct
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.statusflag<>'I' OR scmst.statusflag is null then 'A' else 'I' end as statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.X as CommonCode,
scmst.SharesOutstanding
FROM scmst
inner join issur on scmst.issid = issur.issid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid

WHERE 
scmst.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and scmst.sectycd = 'ETF'
and (
substring(scmst.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or issur.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
)

--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.cntrycd,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM sedol
inner join scmst on sedol.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid
WHERE
sedol.actflag<>'D'
and scmst.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and scmst.sectycd = 'ETF'
and (
substring(scmst.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or issur.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
)

--# 4
use WCA
SELECT distinct
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
scexh.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.LocalCode,
scmst.ISIN,
scmst.CurenCD as ScmstCurenCD,
exchg.MIC
FROM scexh
left outer join wca.dbo.scmst on wca.dbo.scexh.secid = wca.dbo.scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.dprcp on wca.dbo.scmst.secid = wca.dbo.dprcp.secid
WHERE 
scexh.actflag<>'D'
and scmst.actflag<>'D'
and (scmst.statusflag <> 'I'  or scmst.statusflag is null)
and scmst.sectycd = 'ETF'
and (
substring(scmst.primaryexchgcd,1,2) in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
or issur.cntryofincorp  in (select cntrycd from continent where continent='Europe' or continent = 'Africa' or continent = 'Middle East')
)

--# 5
use WCA
SELECT
upper('EXCHG') as Tablename,
Exchg.Actflag,
Exchg.Acttime,
Exchg.Announcedate,
Exchg.ExchgCD,
Exchg.Exchgname,
exchg.cntrycd,
Exchg.MIC
from EXCHG
WHERE
EXCHG.actflag='X'

--# 6
use WCA
SELECT
upper('DPRCP') as Tablename,
DPRCP.Actflag,
DPRCP.Acttime,
DPRCP.Announcedate,
DPRCP.SecID,
DPRCP.UnSecID,
DPRCP.DRRatio,
DPRCP.USRatio,
DPRCP.SpnFlag,
DPRCP.DRtype,
DPRCP.DepBank,
DPRCP.LevDesc,
DPRCP.OtherDepBank
from DPRCP
where
dprcp.actflag='X'
