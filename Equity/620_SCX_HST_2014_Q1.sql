--filepath=o:\Datafeed\Equity\620\SCX\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=_2014_Q1
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\620\SCX\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select * FROM v54f_SCX_Dividend
where
created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 2
use wca
select * FROM v50f_SCX_Company_Meeting
where  v50f_SCX_Company_Meeting.agmegm<>'BHM'
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 3
use wca
select * FROM v53f_SCX_Call
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 4
use wca
select * FROM v50f_SCX_Liquidation
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 5
use wca
select * FROM v51f_SCX_Certificate_Exchange
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 6
use wca
select * FROM v51f_SCX_International_Code_Change
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 7
use wca
select * FROM v51f_SCX_Conversion_Terms
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 8
use wca
select * FROM v51f_SCX_Conversion_Terms_Change
where  (SecStatus = 'X') 
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 9
use wca
select * FROM v51f_SCX_Redemption_Terms
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 10
use wca
select * FROM v51f_SCX_Security_Reclassification
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 11
use wca
select * FROM v52f_SCX_Lot_Change
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 12
use wca
select * FROM v52f_SCX_Sedol_Change
where  (oldsedol<>newsedol
or oldexcountry<>newexcountry
or oldregcountry<>newregcountry)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 13
use wca
select * FROM v53f_SCX_Buy_Back
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 14
use wca
select * FROM v53f_SCX_Capital_Reduction
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 15
use wca
select * FROM v53f_SCX_Takeover
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 16
use wca
select * FROM v54f_SCX_Arrangement
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 17
use wca
select * FROM v54f_SCX_Bonus
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 18
use wca
select * FROM v54f_SCX_Bonus_Rights
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 19
use wca
select * FROM v54f_SCX_Consolidation
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 20
use wca
select * FROM v54f_SCX_Demerger
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 21
use wca
select * FROM v54f_SCX_Distribution
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 22
use wca
select * FROM v54f_SCX_Divestment
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 23
use wca
select * FROM v54f_SCX_Entitlement
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 24
use wca
select * FROM v54f_SCX_Merger
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 25
use wca
select * FROM v54f_SCX_Preferential_Offer
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 26
use wca
select * FROM v54f_SCX_Purchase_Offer
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 27
use wca
select * FROM v54f_SCX_Rights
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 28
use wca
select * FROM v54f_SCX_Security_Swap
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 29
use wca
select * FROM v54f_SCX_Subdivision
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 30
use wca
select * FROM v50f_SCX_Bankruptcy 
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 31
use wca
select * FROM v50f_SCX_Financial_Year_Change
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 32
use wca
select * FROM v50f_SCX_Incorporation_Change
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 33
use wca
select * FROM v50f_SCX_Issuer_Name_change
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 34
use wca
select * FROM v50f_SCX_Class_Action
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 35
use wca
select * FROM v51f_SCX_Security_Description_Change
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 36
use wca
select * FROM v52f_SCX_Assimilation
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 37
use wca
select * FROM v52f_SCX_Listing_Status_Change
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 38
use wca
select * FROM v52f_SCX_Local_Code_Change
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 39
use wca
select * FROM v52f_SCX_New_Listing
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 40
use wca
select * FROM v50f_SCX_Announcement
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef
--# 41
use wca
select * FROM v51f_SCX_Parvalue_Redenomination
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 42
use wca
select * FROM v51f_SCX_Currency_Redenomination
where  (RelatedEvent <> 'Clean' or RelatedEvent is null)
and created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 43
use wca
select * FROM v53f_SCX_Return_of_Capital
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 44
use wca
select * FROM v54f_SCX_Dividend_Reinvestment_Plan
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef

--# 45
use wca
select * FROM v54f_SCX_Franking
where created>'2013/12/31' and created<'2014/04/01'
ORDER BY CaRef
