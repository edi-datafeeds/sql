--filepath=o:\Datafeed\Equity\620_Cantors\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_Cantors\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
SELECT * 
FROM V54F_620_Dividend
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
AND (excountry = 'GB' or excountry = 'US')
AND (EXDATE between getdate()+1 and getdate()+31
or RECDATE between getdate()+1 and getdate()+31
or PAYDATE between getdate()+1 and getdate()+31)
ORDER BY CaRef
