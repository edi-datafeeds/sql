--filepath=o:\Datafeed\Equity\646\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--fileextension=.646
--suffix=
--fileheadertext=EDI_REORG_646_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\646\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 2
use wca
select 
v10s_BKRP.Levent,
v10s_BKRP.EventID,
SCMST.SecID,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.Isin,
'FilingDate' as PrimaryDate,
v10s_BKRP.FilingDate as Enddate1,
'NotificationDate' as SecondaryDate,
v10s_BKRP.NotificationDate as Enddate2,
'' as TertiaryDate,
null as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2
FROM v10s_BKRP
INNER JOIN SCMST ON SCMST.IssID = v10s_BKRP.IssID
where
v10s_BKRP.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 

--# 3
select 
v10s_DIV.Levent,
v10s_DIV.EventID,
SCMST.SecID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
v10s_DIV.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
where 
(v10s_DIV.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and DIVPY.OptionID is not null
and v10s_DIV.Actflag <> 'D'
and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_CALL.Levent,
v10s_CALL.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CALL.Acttime THEN RD.Acttime ELSE v10s_CALL.Acttime END as [Changed],
v10s_CALL.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'DueDate' as PrimaryDate,
v10s_CALL.DueDate as Enddate1,
'RecDate' as SecondaryDate,
RD.RecDate as Endate2,
'' as TertiaryDate,
null as Enddate3
FROM v10s_CALL
INNER JOIN SCMST ON SCMST.SecID = v10s_CALL.SecID
LEFT OUTER JOIN RD ON v10s_CALL.RdID = RD.RdID
where
(v10s_CALL.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_CALL.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_CAPRD.Levent,
v10s_CAPRD.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CAPRD.Acttime THEN RD.Acttime ELSE v10s_CAPRD.Acttime END as [Changed],
v10s_CAPRD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_scexh.ExchgCD,
'PayDate' as PrimaryDate,
v10s_CAPRD.PayDate as Enddate1,
'EffectiveDate' as SecondaryDate,
v10s_CAPRD.EffectiveDate as Endate2,
'RecDate' as TertiaryDate,
RD.RecDate as Endate3
FROM v10s_CAPRD
INNER JOIN SCMST ON SCMST.SecID = v10s_CAPRD.SecID

LEFT OUTER JOIN RD ON v10s_CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
where
(v10s_CAPRD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_CAPRD.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_ARR.Levent,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v10s_ARR.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_SCEXH.ExchgCD,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
where
(v10s_ARR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_ARR.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_SCEXH.Sedol is not null)

union

select 
v10s_BON.Levent,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
v10s_BON.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
where
(v10s_BON.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_BON.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_BR.Levent,
v10s_BR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BR.Acttime) THEN PEXDT.Acttime ELSE v10s_BR.Acttime END as [Changed],
v10s_BR.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_BR
INNER JOIN RD ON RD.RdID = v10s_BR.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
where
(v10s_BR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_BR.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_CONSD.Levent,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
v10s_CONSD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_scexh.ExchgCD,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
where
(v10s_CONSD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_CONSD.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_DMRGR.Levent,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
v10s_DMRGR.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
where
(v10s_DMRGR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_DMRGR.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_DIST.Levent,
v10s_DIST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIST.Acttime) THEN PEXDT.Acttime ELSE v10s_DIST.Acttime END as [Changed],
v10s_DIST.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
where
(v10s_DIST.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_DIST.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select
v10s_DVST.Levent,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v10s_DVST.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
where
(v10s_DVST.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_DVST.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_ENT.Levent,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
v10s_ENT.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
where
(v10s_ENT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_ENT.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_SD.Levent,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
v10s_SD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_scexh.ExchgCD,
'NewCodeDate' as PrimaryDate,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as EndDate1,
'PayDate' as SecondaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate2,
'RecDate' as TertiaryDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate3
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
where
(v10s_SD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or SDCHG.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or ICC.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_SD.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_MRGR.Levent,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v10s_MRGR.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'AppointedDate' as PrimaryDate,
v10s_MRGR.AppointedDate as Enddate1,
'EffectiveDate' as SecondaryDate,
v10s_MRGR.EffectiveDate as EndDate2,
'PayDate' as TertiaryDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate3
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID AND v10s_MRGR.SEvent = MPAY.SEvent
where
(v10s_MRGR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_MRGR.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_RTS.Levent,
v10s_RTS.AnnounceDate as Created,
RD.Acttime Changed,
v10s_RTS.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'EndTrade' as PrimaryDate,
v10s_RTS.EndTrade as Enddate1,
'StartTrade' as SecondaryDate,
v10s_RTS.StartTrade as EndDate2,
'SplitDate' as TertiaryDate,
v10s_RTS.SplitDate as EndDate3
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
where
v10s_RTS.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
and v10s_RTS.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)

union

select 
v10s_PRF.Levent,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
v10s_PRF.ActFlag,
SCMST.PrimaryExchgCD, SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3
FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
where
(v10s_PRF.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and v10s_PRF.Actflag <> 'D' and (SCMST.Statusflag <> 'I' or SCMST.Statusflag is null)
and (SCMST.ISIN<>'' OR v20c_EV_scexh.Sedol is not null)
