--filepath=o:\Datafeed\Debt\WFIfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_DEBT_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when SCEXH.ListStatus='N' or SCEXH.ListStatus='' then 'L' ELSE SCEXH.ListStatus end as ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
SCEXH.TradeStatus,
SCEXH.LocalCode
FROM SCEXH
INNER JOIN BOND ON SCEXH.SecID = BOND.SecID
where 
SCEXH.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
