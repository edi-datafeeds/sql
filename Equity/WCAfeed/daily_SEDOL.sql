--filepath=o:\Datafeed\Debt\WFIfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_DEBT_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use wca
select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM SEDOL
INNER JOIN BOND ON SEDOL.SecID = BOND.SecID
where 
SEDOL.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
