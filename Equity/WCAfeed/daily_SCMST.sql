--filepath=o:\Datafeed\Debt\WFIfeed\Refdata1\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_DEBT_Security_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT 
upper('SCMST') as TableName,
SCMST.Actflag,
SCMST.AnnounceDate,
SCMST.Acttime,
SCMST.SecID,
SCMST.IssID,
SCMST.SecurityDesc,
case when SCMST.Statusflag<>'I' or SCMST.Statusflag is null then 'A' ELSE 'I' end as StatusFlag,
SCMST.StatusReason,
SCMST.PrimaryExchgCD,
SCMST.CurenCD,
SCMST.ParValue,
SCMST.USCode,
SCMST.ISIN,
SCMST.X as CommonCode
FROM SCMST
INNER JOIN BOND ON SCMST.SecID = BOND.SecID
where 
SCMST.acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
