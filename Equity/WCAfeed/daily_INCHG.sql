--filepath=o:\Datafeed\Debt\WFIfeed\Refdata1_Event\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) from wca.dbo.tbl_Opslog where seq = 2
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_DEBT_Incorporation_Change_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Debt\WFIfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT *
FROM v50f_920_Incorporation_Change
WHERE Changed >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
ORDER BY EventID desc
