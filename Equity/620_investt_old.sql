--filepath=o:\Datafeed\Equity\620_investt\
--filenameprefix=
--filename=yyyymmdd
--filenamealt='2007/03/30'
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_investt
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT *  
FROM v50f_620_Company_Meeting 
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 2
USE WCA
SELECT * 
FROM v53f_620_Call
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 3
USE WCA
SELECT *  
FROM v50f_620_Liquidation
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef, OptionID, SerialID

--# 4
USE WCA
SELECT * 
FROM v51f_620_Certificate_Exchange
WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 5
USE WCA
SELECT *  
FROM v51f_620_International_Code_Change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 6
USE WCA
SELECT *  
FROM v51f_620_Conversion_Terms

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 7
use wca
SELECT *  
FROM v51f_620_Redemption_Terms

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 8
use wca
SELECT *  
FROM v51f_620_Security_Reclassification

WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 9
use wca
SELECT *  
FROM v52f_620_Lot_Change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 10
use wca
SELECT *  
FROM v52f_620_Sedol_Change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 11
use wca
SELECT *  
FROM v53f_620_Buy_Back

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef, OptionID, SerialID

--# 12
use wca
SELECT *  
FROM v53f_620_Capital_Reduction

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 14
use wca
SELECT *  
FROM v53f_620_Takeover

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef, OptionID, SerialID

--# 15
use wca
SELECT *  
FROM v54f_620_Arrangement

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 16
use wca
SELECT *  
FROM v54f_620_Bonus

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 17
use wca
SELECT *  
FROM v54f_620_Bonus_Rights

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 18
use wca
SELECT *  
FROM v54f_620_Consolidation

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 19
use wca
SELECT *  
FROM v54f_620_Demerger

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef, OptionID, SerialID

--# 20
use wca
SELECT *  
FROM v54f_620_Distribution

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef, OptionID, SerialID

--# 21
use wca
SELECT *  
FROM v54f_620_Divestment

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 22
use wca
SELECT *  
FROM v54f_620_Entitlement

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 23
use wca
SELECT *  
FROM v54f_620_Merger

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef, OptionID, SerialID

--# 24
use wca
SELECT *  
FROM v54f_620_Preferential_Offer

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 25
use wca
SELECT *  
FROM v54f_620_Purchase_Offer

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 26
use wca
SELECT *  
FROM v54f_620_Rights 

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 27
use wca
SELECT *  
FROM v54f_620_Security_Swap 

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 28
use wca
SELECT * 
FROM v54f_620_Subdivision

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 29
use wca
SELECT * 
FROM v50f_620_Bankruptcy 

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 30
use wca
SELECT * 
FROM v50f_620_Financial_Year_Change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 31
use wca
SELECT * 
FROM v50f_620_Incorporation_Change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 32
use wca
SELECT * 
FROM v50f_620_Issuer_Name_change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 33
use wca
SELECT * 
FROM v50f_620_Class_Action

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 34
use wca
SELECT * 
FROM v51f_620_Security_Description_Change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 35
use wca
SELECT * 
FROM v52f_620_Assimilation

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 36
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change

WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 37
use wca
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 38
use wca
SELECT *  
FROM v52f_620_New_Listing
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 39
use wca
SELECT *  
FROM v50f_620_Announcement
WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 40
use wca
SELECT *  
FROM v51f_620_Parvalue_Redenomination 
WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 41
USE WCA
SELECT *  
FROM v51f_620_Currency_Redenomination 
WHERE CHANGED = '2007/03/30 08:18:42'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 42
USE WCA
SELECT *  
FROM v53f_620_Return_of_Capital 
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 43
USE WCA
SELECT *  
FROM v54f_620_Dividend
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 44
USE WCA
SELECT *  
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 45
USE WCA
SELECT *  
FROM v54f_620_Franking

WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef

--# 46
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE CHANGED = '2007/03/30 08:18:42'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
AND (ExCountry = 'DE' or ExCountry = 'DK' or ExCountry = 'FI' or ExCountry = 'NL' or ExCountry = 'NO' or ExCountry = 'SE')
ORDER BY CaRef
