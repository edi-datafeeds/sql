--filepath=o:\
--filenameprefix=DIV_JP_2018_
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_DIVIDEND_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1

use wca
SELECT *
FROM v54f_620_Dividend
WHERE
excountry = 'JP'
and actflag<>'D'
and ((paydate>='2018/01/01' and paydate<'2019/01/01' and paydate is not null)
     or (exdate>='2018/01/01' and exdate<'2019/01/01' and exdate is not null))
ORDER BY CaRef desc