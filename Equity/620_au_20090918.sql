--filepath=o:\Datafeed\Equity\620_AU\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) from wca.dbo.tbl_opslog where seq=3
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_AU\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
SELECT *  
FROM t620_Company_Meeting 
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
use wca2
SELECT * 
FROM t620_Call
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
use wca2
SELECT *  
FROM t620_Liquidation
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 4
use wca2
SELECT * 
FROM t620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 5
use wca2
SELECT *  
FROM t620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 6
use wca2
SELECT *  
FROM t620_Conversion_Terms
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca2
SELECT *  
FROM t620_Redemption_Terms
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 8
use wca2
SELECT *  
FROM t620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 9
use wca2
SELECT *  
FROM t620_Lot_Change
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca2
SELECT *  
FROM t620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 11
use wca2
SELECT *  
FROM t620_Buy_Back
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 12
use wca2
SELECT *  
FROM t620_Capital_Reduction
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 14
use wca2
SELECT *  
FROM t620_Takeover
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 15
use wca2
SELECT *  
FROM t620_Arrangement
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 16
use wca2
SELECT *  
FROM t620_Bonus
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 17
use wca2
SELECT *  
FROM t620_Bonus_Rights
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 18
use wca2
SELECT *  
FROM t620_Consolidation
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 19
use wca2
SELECT *  
FROM t620_Demerger
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 20
use wca2
SELECT *  
FROM t620_Distribution
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 21
use wca2
SELECT *  
FROM t620_Divestment
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 22
use wca2
SELECT *  
FROM t620_Entitlement
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 23
use wca2
SELECT *  
FROM t620_Merger
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 24
use wca2
SELECT *  
FROM t620_Preferential_Offer
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 25
use wca2
SELECT *  
FROM t620_Purchase_Offer
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 26
use wca2
SELECT *  
FROM t620_Rights 
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 27
use wca2
SELECT *  
FROM t620_Security_Swap 
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 28
use wca2
SELECT * 
FROM t620_Subdivision
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 29
use wca2
SELECT * 
FROM t620_Bankruptcy 
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca2
SELECT * 
FROM t620_Financial_Year_Change
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca2
SELECT * 
FROM t620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca2
SELECT * 
FROM t620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT * 
FROM t620_Class_Action
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 34
use wca2
SELECT * 
FROM t620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 35
use wca2
SELECT * 
FROM t620_Assimilation
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 36
use wca2
SELECT * 
FROM t620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca2
SELECT *  
FROM t620_New_Listing
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca2
SELECT *  
FROM t620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 40
use wca2
SELECT *  
FROM t620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 41
use wca2
SELECT *  
FROM t620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 42
use wca
SELECT v53f_620_Return_of_Capital.*  
FROM v53f_620_Return_of_Capital
left outer join wca.dbo.lstat on v53f_620_Return_of_Capital.secid = wca.dbo.lstat.secid 
  and v53f_620_Return_of_Capital.exchgcd = wca.dbo.lstat.exchgcd 
  and 'D'= wca.dbo.lstat.lstatstatus 
  and v53f_620_Return_of_Capital.liststatus = 'D'
WHERE
ExCountry = 'AU'
and (wca.dbo.lstat.effectivedate>=v53f_620_Return_of_Capital.effectivedate
  	    or wca.dbo.lstat.effectivedate is null)
and changed>'2009/09/03'
ORDER BY CaRef


--# 43
use wca
SELECT *  
FROM v54f_620_Dividend
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and changed>'2009/09/03'
ORDER BY CaRef

--# 44
use wca
SELECT *  
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2009/09/03'
ORDER BY CaRef

--# 45
use wca
SELECT *  
FROM v54f_620_Franking
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2009/09/03'
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
WHERE
ExCountry = 'AU'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
