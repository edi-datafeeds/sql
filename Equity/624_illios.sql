--filepath=o:\Datafeed\Equity\extracts\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.624
--suffix=
--fileheadertext=EDI_STATIC_624_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\624\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT 
scexh.ScexhID,
scexh.Acttime as Changed,
scexh.AnnounceDate as Created,
scexh.Actflag,
scmst.IssID,
scmst.SecID,
issur.CntryofIncorp,
issur.Issuername,
scmst.Securitydesc,
scmst.Parvalue,
scmst.CurenCD as PVCurrency,
scmst.Isin,
scmst.Uscode,
case when scmst.Statusflag is not null then scmst.Statusflag else 'A' end as SecStatus,
scmst.PrimaryExchgCD,
scmst.SectyCD,
Sedol.Sedol,
Sedol.Defunct,
substring(scexh.Exchgcd,1,2) as ExCountry,
scexh.Exchgcd,
exchg.Mic,
scexh.Localcode,
CASE WHEN (Scexh.ListStatus IS NULL) or (Scexh.ListStatus='') THEN 'L' ELSE Scexh.ListStatus END as ListStatus,
scexh.ListDate,
scexh.Lot
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN cntry ON issur.CntryofIncorp = cntry.cntryCD
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                      AND exchg.cntryCD = Sedol.CntryCD
where issur.actflag <> 'D'
and scmst.actflag <> 'D'
and scexh.actflag <> 'D'
and (scmst.sectycd = 'EQS'
or scmst.sectycd = 'PRF'
or scmst.sectycd = 'DR')
