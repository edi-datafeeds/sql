--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.661
--suffix=
--fileheadertext=EDI_661_SRF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\661\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
select distinct
SCMST.SecID,
case when SCEXH.ScexhID is null then 0 else SCEXH.ScexhID end as ScexhID,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.Sedol else '' end as Sedol,
case when scmst.statusflag='' then 'A' else scmst.statusflag end as IssueStatus,
case when SCEXH.ListStatus='N' or SCEXH.ListStatus='' then 'L' else SCEXH.ListStatus end as ListStatus,
case when scexh.actflag is not null then scexh.actflag else scmst.actflag end as RecordStatus,
SCMST.IssID,
scmst.Isin as ISIN,
scmst.Uscode as UScode,
SCEXH.Localcode,
ISSUR.IssuerName,
ISSUR.CntryofIncorp,
ISSUR.IndusID,
SCMST.SecurityDesc,
SCMST.SectyCD,
scmst.Parvalue,
scmst.CurenCD as ParvalueCurrency,
SCMST.StructCD,
SCMST.PrimaryExchgCD,
exchg.cntryCD as ListingCountry,
SCEXH.ExchgCD,
SCEXH.ListDate,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.CurenCD else '' end as SedolTradingCurrency,
case when sedol.secid is not null and sedol.actflag<>'D' and sedol.defunct<>'T' then sedol.RcntryCD else '' end as SedolRegisterCountry
FROM scmst
inner JOIN scexh ON scmst.SecID = scexh.SecID
left outer join ISSUR ON SCMST.IssID = ISSUR.IssID
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
left outer join sedol on scexh.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
where
(scmst.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3) 
or scexh.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3) 
or issur.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3)
or sedol.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq =3))

