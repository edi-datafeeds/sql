--filepath=o:\Datafeed\Equity\645_aim\
--filenameprefix=al
--filename=yymmdd
--filenamealt=select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.645
--suffix=
--fileheadertext=EDI_STATIC_645_AIM_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=Y
--archivepath=n:\Equity\645_aim\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use WCA
SELECT DISTINCT
upper('ISSUER') as Tablename,
issur.Actflag,
issur.Announcedate,
issur.Acttime,
issur.IssID,
issur.IssuerName,
issur.IndusID,
issur.CntryofIncorp
FROM portfolio.dbo.aimticker
inner join scexh on portfolio.dbo.aimticker.ticker = scexh.localcode
        and 'GBLSE' = scexh.exchgcd
left outer join scmst on scexh.secid = scmst.secid
left outer join issur on scmst.issid = issur.issid
WHERE issur.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)


--# 2
use WCA
SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.ParentSecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
case when scmst.Statusflag = 'I' then 'I' ELSE 'A' END as Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.SharesOutstanding
FROM portfolio.dbo.aimticker
inner join scexh on portfolio.dbo.aimticker.ticker = scexh.localcode
        and 'GBLSE' = scexh.exchgcd
left outer join scmst on scexh.secid = scmst.secid
WHERE
issur.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)


--# 3
use WCA
SELECT distinct
upper('SEDOL') as Tablename,
sedol.Actflag,
sedol.Acttime,
sedol.Announcedate,
sedol.SecID,
sedol.CntryCD,
sedol.Sedol,
sedol.Defunct,
sedol.RcntryCD,
sedol.SedolId
FROM portfolio.dbo.aimticker
inner join scexh on portfolio.dbo.aimticker.ticker = scexh.localcode
        and 'GBLSE' = scexh.exchgcd
left outer join sedol on scexh.secid = scexh.secid
             and substring(exchgcd,1,2) = sedol.cntrycd



--# 4
use WCA
SELECT distinct
upper('SCEXH') as Tablename,
scexh.Actflag,
scexh.Acttime,
scexh.Announcedate,
scexh.ScExhID,
scexh.SecID,
'GBAIM' as ExchgCD,
case when portfolio.dbo.aimticker.actflag = 'D' then 'D' when scexh.ListStatus='R' or scexh.ListStatus='S' then scexh.ListStatus else 'L' end as ListStatus,
scexh.Lot,
scexh.MinTrdgQty,
scexh.ListDate,
scexh.ListStatus,
scexh.LocalCode
FROM portfolio.dbo.aimticker
inner join scexh on portfolio.dbo.aimticker.ticker = scexh.localcode
        and 'GBLSE' = scexh.exchgcd
WHERE 
(scexh.acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or 

(portfolio.dbo.aimticker  >= (select max(acttime) from tbl_Opslog where seq = 1)
and portfolio.dbo.aimticker.actflag='D')

or portfolio.dbo.aimticker.actflag='I'