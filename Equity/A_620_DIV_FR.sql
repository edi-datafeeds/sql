--filepath=o:\Datafeed\Equity\620_Tijd\
--filenameprefix=DIV_
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_DIVIDEND_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_Tijd\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
SELECT *
FROM v54f_620_Dividend
WHERE
(excountry = 'FR')
and changed>=(select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
ORDER BY CaRef desc
