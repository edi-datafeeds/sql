--filepath=o:\Datafeed\Equity\643\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.643
--suffix=
--fileheadertext=EDI_STATIC_CHANGE_643_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\643\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n

--# 1
use wca
select
'EventID' as f1,
'Changetype' as f2,
'IssID' as f3,
'SecID' as f4,
'ExchgCD' as f5,
'Actflag' as f17,
'Reason' as f18,
'EffectiveDate' as f19,
'OldStatic' as f20,
'NewStatic' as f21


--# 2
use wca
select
SDCHG.SdchgID as EventID,
'Sedol Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
     case when sdchg.newcntrycd <> ''
     then sdchg.Newcntrycd
     else sdchg.cntrycd
     end as CntryCD,
sdchg.Actflag,
sdchg.eventtype as Reason,
case when sdchg.effectivedate is null
     then sdchg.announcedate
     else sdchg.effectivedate
     end as EffectiveDate,
sdchg.OldSedol as OldStatic,
sdchg.NewSedol as NewStatic
FROM SDCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = SDCHG.SecID
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (sdchg.OldSedol <> '' or sdchg.NewSedol <> '')


--# 3
use wca
select
ICC.ICCID as EventID,
'ISIN Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.PrimaryExchgCD,
icc.Actflag,
icc.eventtype as Reason,
case when icc.effectivedate is null
     then icc.announcedate
     else icc.effectivedate
     end as Effectivedate,
icc.OldIsin as OldStatic,
icc.NewIsin as NewStatic
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (icc.OldIsin <> '' or icc.NewIsin <> '')


--# 4
use wca
select
INCHG.INCHGID as EventID,
'Country of Incorporation Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.PrimaryExchgCD,
inchg.Actflag,
inchg.eventtype as Reason,
CASE WHEN inchg.inchgdate IS NULL
     THEN INCHG.ANNOUNCEDATE
     ELSE inchg.inchgdate
     END AS EffectiveDate,
inchg.OldCntryCD as OldStatic,
inchg.NewCntryCD as NewStatic
FROM inchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = inchg.issid
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (inchg.OldCntryCD <> '' or inchg.NewCntryCD <> '')


--# 5
use wca
select
ISCHG.ISCHGID as EventID,
'Issuer Name Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.PrimaryExchgCD,
ischg.Actflag,
ischg.eventtype as Reason,
case when ischg.namechangedate is null
     then ischg.announcedate
     else ischg.namechangedate
     end as EffectiveDate,
ischg.IssOldName as OldStatic,
ischg.IssNewName as NewStatic
FROM ischg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.issid = ischg.issid
where 
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (ischg.IssOldName <> '' or ischg.IssNewName <> '')


--# 6
use wca
select
LCC.LCCID as EventID,
'Local Code Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
lcc.ExchgCD,
lcc.Actflag,
lcc.eventtype as Reason,
case when lcc.effectivedate is null
     then lcc.announcedate
     else lcc.effectivedate
     end as EffectiveDate,
lcc.OldLocalCode as OldStatic,
lcc. NewLocalCode as NewStatic
FROM lcc
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = lcc.secid
where 
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (lcc.OldLocalCode <> '' or lcc.NewLocalCode <> '')


--# 7
use wca
select
SCCHG.SCCHGID as EventID,
'Security Description Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.PrimaryExchgCD,
scchg.Actflag,
scchg.eventtype as Reason,
case when scchg.dateofchange is null
     then scchg.announcedate
     else  scchg.dateofchange
     end as EffectiveDate,
scchg.SecOldName as OldStatic,
scchg.SecNewName as NewStatic
FROM scchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = scchg.secid
where 
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (scchg.SecOldName <> '' or scchg.SecNewName <> '')


--# 8
use wca
select
LTCHG.LTCHGID as EventID,
'Lot Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
LTCHG.ExchgCD,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldLot as OldStatic,
ltchg.NewLot as NewStatic
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = ltchg.secid
where 
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (ltchg.OldLot <> '' or ltchg.NewLot <> '')


--# 9
use wca
select
LTCHG.LTCHGID as EventID,
'Minimum Trading Quantity Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
ltchg.ExchgCD,
ltchg.Actflag,
'' as Reason,
case when ltchg.effectivedate is null
     then ltchg.announcedate
     else ltchg.effectivedate
     end as EffectiveDate,
ltchg.OldMinTrdQty as OldStatic,
ltchg.NewMinTrdgQty as NewStatic
FROM ltchg
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = ltchg.secid
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (ltchg.OldMinTrdQty <> '' or ltchg.NewMinTrdgQty <> '')


--# 10
use wca
select
LSTAT.LSTATID as EventID,
'Listing Status Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
lstat.ExchgCD,
lstat.Actflag,
lstat.eventtype as Reason,
case when lstat.effectivedate is null
     then lstat.announcedate
     else lstat.effectivedate
     end as EffectiveDate,
'' as OldStatic,
lstat.LStatStatus as NewStatic
FROM lstat
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.secid = lstat.secid
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and lstat.LStatStatus <> ''



--# 11
use wca
select
nlist.scexhID as EventID,
'New Listing' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_dSCEXH.ExchgCD,
nlist.Actflag,
case when v20c_EV_dSCEXH.Listdate is not null 
     then 'Datetype = Effective'
     else 'Datetype = Announcement'
     end as Reason,
case when v20c_EV_dSCEXH.Listdate is not null 
     then v20c_EV_dSCEXH.Listdate 
     else nlist.AnnounceDate 
     end as EffectiveDate,
'' as OldStatic,
'L' as NewStatic
FROM nlist
INNER JOIN v20c_EV_dSCEXH ON nlist.scexhid = v20c_EV_dSCEXH.scexhid
INNER JOIN v20c_EV_SCMST ON v20c_EV_dSCEXH.secid = v20c_EV_SCMST.secid
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'


--# 12
use wca
select
ICC.ICCID as EventID,
'USCode Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.PrimaryExchgCD,
icc.Actflag,
icc.eventtype as Reason,
case when icc.EffectiveDate is null
     then icc.announcedate
     else icc.effectivedate
     end as EffectiveDate,
icc.OldUSCode as OldStatic,
icc.NewUSCode as NewStatic
FROM ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = icc.SecID
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (icc.OldUSCode <> '' or icc.NewUSCode <> '')


--# 13
use wca
select
CURRD.CURRDID as EventID,
'Par Value Currency Change' as ChangeType,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
v20c_EV_SCMST.PrimaryExchgCD,
currd.Actflag,
currd.eventtype as Reason,
case when currd.EffectiveDate is null
     then currd.announcedate
     else currd.effectivedate
     end as EffectiveDate,
currd.OldCurenCD as OldStatic,
currd.NewCurenCD as NewStatic
FROM Currd
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = currd.SecID
where
  	Acttime >= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--  Acttime > '2002/01/01'
and (currd.OldCurenCD <> '' or currd.NewCurenCD <> '')
