--filepath=o:\Datafeed\Equity\620_Tijd\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 1
--fileextension=.662
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_Tijd\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT *  
FROM v50f_620_Company_Meeting 
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
SELECT * 
FROM v53f_620_Call
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
SELECT *  
FROM v50f_620_Liquidation
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA
SELECT * 
FROM v51f_620_Certificate_Exchange
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
SELECT *  
FROM v51f_620_International_Code_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
SELECT *  
FROM v51f_620_Conversion_Terms
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
SELECT *  
FROM v51f_620_Redemption_Terms
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
SELECT *  
FROM v51f_620_Security_Reclassification
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
SELECT *  
FROM v52f_620_Lot_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
SELECT *  
FROM v52f_620_Sedol_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca
SELECT *  
FROM v53f_620_Buy_Back
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
SELECT *  
FROM v53f_620_Capital_Reduction
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
use wca
SELECT *  
FROM v53f_620_Takeover
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 15
use wca
SELECT *  
FROM v54f_620_Arrangement
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
SELECT *  
FROM v54f_620_Bonus
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
SELECT *  
FROM v54f_620_Bonus_Rights
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
SELECT *  
FROM v54f_620_Consolidation
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca
SELECT *  
FROM v54f_620_Demerger
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
SELECT *  
FROM v54f_620_Distribution
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 21
use wca
SELECT *  
FROM v54f_620_Divestment
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
SELECT *  
FROM v54f_620_Entitlement
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
use wca
SELECT *  
FROM v54f_620_Merger
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 24
use wca
SELECT *  
FROM v54f_620_Preferential_Offer
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
SELECT *  
FROM v54f_620_Purchase_Offer
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
SELECT *  
FROM v54f_620_Rights 
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
SELECT *  
FROM v54f_620_Security_Swap 
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
SELECT * 
FROM v54f_620_Subdivision
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
SELECT * 
FROM v50f_620_Bankruptcy 
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
SELECT * 
FROM v50f_620_Financial_Year_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
SELECT * 
FROM v50f_620_Incorporation_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
SELECT * 
FROM v50f_620_Issuer_Name_change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
SELECT * 
FROM v50f_620_Class_Action
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
SELECT * 
FROM v51f_620_Security_Description_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
SELECT * 
FROM v52f_620_Assimilation
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
SELECT *  
FROM v52f_620_New_Listing
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
SELECT *  
FROM v50f_620_Announcement
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
use wca
SELECT *  
FROM v51f_620_Parvalue_Redenomination 
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
SELECT *  
FROM v51f_620_Currency_Redenomination 
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
SELECT *  
FROM v53f_620_Return_of_Capital 
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
USE WCA
SELECT *  
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 45
USE WCA
SELECT *  
FROM v54f_620_Franking
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 46
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE CHANGED >= (select max(acttime) as maxdate from wca.dbo.tbl_Opslog where seq = 1)
and (excountry = 'FR')
ORDER BY EventID desc, ExchgCD, Sedol
