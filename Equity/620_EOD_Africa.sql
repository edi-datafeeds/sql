--filepath=o:\Datafeed\Equity\620_Africa\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_Africa\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca2
SELECT *  
FROM t620_Company_Meeting 
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
use wca2
SELECT * 
FROM t620_Call
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
use wca2
SELECT *  
FROM t620_Liquidation
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 4
use wca2
SELECT * 
FROM t620_Certificate_Exchange
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 5
use wca2
SELECT *  
FROM t620_International_Code_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 6
use wca2
SELECT *  
FROM t620_Conversion_Terms
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca2
SELECT *  
FROM t620_Redemption_Terms
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 8
use wca2
SELECT *  
FROM t620_Security_Reclassification
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 9
use wca2
SELECT *  
FROM t620_Lot_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca2
SELECT *  
FROM t620_Sedol_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 11
use wca2
SELECT *  
FROM t620_Buy_Back
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 12
use wca2
SELECT *  
FROM t620_Capital_Reduction
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 14
use wca2
SELECT *  
FROM t620_Takeover
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 15
use wca2
SELECT *  
FROM t620_Arrangement
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 16
use wca2
SELECT *  
FROM t620_Bonus
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 17
use wca2
SELECT *  
FROM t620_Bonus_Rights
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 18
use wca2
SELECT *  
FROM t620_Consolidation
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 19
use wca2
SELECT *  
FROM t620_Demerger
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 20
use wca2
SELECT *  
FROM t620_Distribution
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 21
use wca2
SELECT *  
FROM t620_Divestment
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 22
use wca2
SELECT *  
FROM t620_Entitlement
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 23
use wca2
SELECT *  
FROM t620_Merger
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 24
use wca2
SELECT *  
FROM t620_Preferential_Offer
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 25
use wca2
SELECT *  
FROM t620_Purchase_Offer
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 26
use wca2
SELECT *  
FROM t620_Rights 
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 27
use wca2
SELECT *  
FROM t620_Security_Swap 
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 28
use wca2
SELECT * 
FROM t620_Subdivision
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 29
use wca2
SELECT * 
FROM t620_Bankruptcy 
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca2
SELECT * 
FROM t620_Financial_Year_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca2
SELECT * 
FROM t620_Incorporation_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca2
SELECT * 
FROM t620_Issuer_Name_change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT * 
FROM t620_Class_Action
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 34
use wca2
SELECT * 
FROM t620_Security_Description_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 35
use wca2
SELECT * 
FROM t620_Assimilation
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 36
use wca2
SELECT * 
FROM t620_Listing_Status_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_Local_Code_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca2
SELECT *  
FROM t620_New_Listing
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca2
SELECT *  
FROM t620_Announcement
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 40
use wca2
SELECT *  
FROM t620_Parvalue_Redenomination 
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 41
use wca2
SELECT *  
FROM t620_Currency_Redenomination 
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 42
use wca2
SELECT *  
FROM t620_Return_of_Capital 
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 43
use wca2
SELECT *  
FROM t620_Dividend
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 44
use wca2
SELECT *  
FROM t620_Dividend_Reinvestment_Plan
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 45
use wca2
SELECT *  
FROM t620_Franking
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
inner join continent on excountry = continent.cntrycd
WHERE CONTINENT = 'Africa'
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
