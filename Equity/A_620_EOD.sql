--filepath=o:\Datafeed\Equity\620\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\620\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca2
SELECT * 
FROM t620_Company_Meeting 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
use wca2
SELECT *
FROM t620_Call
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
use wca2
SELECT * 
FROM t620_Liquidation
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 4
use wca2
SELECT *
FROM t620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 5
use wca2
SELECT * 
FROM t620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 6
use wca2
SELECT * 
FROM t620_Conversion_Terms
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca2
SELECT * 
FROM t620_Redemption_Terms
ORDER BY CaRef

--# 8
use wca2
SELECT * 
FROM t620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 9
use wca2
SELECT * 
FROM t620_Lot_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM t620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 11
use wca2
SELECT * 
FROM t620_Buy_Back
ORDER BY CaRef

--# 12
use wca2
SELECT * 
FROM t620_Capital_Reduction
ORDER BY CaRef

--# 13
use wca2
SELECT * 
FROM t620_Takeover
ORDER BY CaRef

--# 14
use wca2
SELECT * 
FROM t620_Arrangement
ORDER BY CaRef

--# 15
use wca2
SELECT * 
FROM t620_Bonus
ORDER BY CaRef

--# 17
use wca2
SELECT * 
FROM t620_Consolidation
ORDER BY CaRef

--# 18
use wca2
SELECT * 
FROM t620_Demerger
ORDER BY CaRef

--# 19
use wca2
SELECT * 
FROM t620_Distribution
ORDER BY CaRef

--# 20
use wca2
SELECT * 
FROM t620_Divestment
ORDER BY CaRef

--# 21
use wca2
SELECT * 
FROM t620_Entitlement
ORDER BY CaRef

--# 22
use wca2
SELECT * 
FROM t620_Merger
ORDER BY CaRef

--# 23
use wca2
SELECT * 
FROM t620_Preferential_Offer
ORDER BY CaRef

--# 24
use wca2
SELECT * 
FROM t620_Purchase_Offer
ORDER BY CaRef

--# 25
use wca2
SELECT * 
FROM t620_Rights 
ORDER BY CaRef

--# 26
use wca2
SELECT * 
FROM t620_Security_Swap 
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM t620_Subdivision
ORDER BY CaRef

--# 28
use wca2
SELECT *
FROM t620_Bankruptcy 
ORDER BY CaRef

--# 29
use wca2
SELECT *
FROM t620_Financial_Year_Change
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca2
SELECT *
FROM t620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM t620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca2
SELECT *
FROM t620_Class_Action
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 34
use wca2
SELECT *
FROM t620_Assimilation
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM t620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM t620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca2
SELECT * 
FROM t620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca2
SELECT * 
FROM t620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 40
use wca2
SELECT * 
FROM t620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 41
use wca2
SELECT * 
FROM t620_Return_of_Capital 
ORDER BY CaRef


--# 42
use wca2
SELECT * 
FROM t620_Dividend
ORDER BY CaRef

--# 43
use wca2
SELECT * 
FROM t620_Dividend_Reinvestment_Plan
ORDER BY CaRef

--# 44
use wca2
SELECT * 
FROM t620_Franking
ORDER BY CaRef

--# 45
use wca2
SELECT * 
FROM t620_Conversion_Terms_Change
WHERE
(SecStatus = 'X') 
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620_Bonus_Rights 
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

