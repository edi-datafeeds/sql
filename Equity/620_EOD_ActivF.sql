--filepath=o:\Datafeed\Equity\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select max(acttime) from wca.dbo.tbl_opslog where feeddate='2010/04/21'
--fileextension=.620
--suffix=
--fileheadertext=EDI_STATIC_620_ActivF_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca2
SELECT *  
FROM tt620_Company_Meeting 
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 2
use wca2
SELECT * 
FROM tt620_Call
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 3
use wca2
SELECT *  
FROM tt620_Liquidation
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 4
use wca2
SELECT * 
FROM tt620_Certificate_Exchange
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 5
use wca2
SELECT *  
FROM tt620_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 6
use wca2
SELECT *  
FROM tt620_Conversion_Terms
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 7
use wca2
SELECT *  
FROM tt620_Redemption_Terms
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 8
use wca2
SELECT *  
FROM tt620_Security_Reclassification
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 9
use wca2
SELECT *  
FROM tt620_Lot_Change
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 10
use wca2
SELECT *  
FROM tt620_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 11
use wca2
SELECT *  
FROM tt620_Buy_Back
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 12
use wca2
SELECT *  
FROM tt620_Capital_Reduction
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 14
use wca2
SELECT *  
FROM tt620_Takeover
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 15
use wca2
SELECT *  
FROM tt620_Arrangement
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 16
use wca2
SELECT *  
FROM tt620_Bonus
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 17
use wca2
SELECT *  
FROM tt620_Bonus_Rights
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 18
use wca2
SELECT *  
FROM tt620_Consolidation
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 19
use wca2
SELECT *  
FROM tt620_Demerger
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 20
use wca2
SELECT *  
FROM tt620_Distribution
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 21
use wca2
SELECT *  
FROM tt620_Divestment
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 22
use wca2
SELECT *  
FROM tt620_Entitlement
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 23
use wca2
SELECT *  
FROM tt620_Merger
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef, OptionID, SerialID

--# 24
use wca2
SELECT *  
FROM tt620_Preferential_Offer
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 25
use wca2
SELECT *  
FROM tt620_Purchase_Offer
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 26
use wca2
SELECT *  
FROM tt620_Rights 
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 27
use wca2
SELECT *  
FROM tt620_Security_Swap 
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 28
use wca2
SELECT * 
FROM tt620_Subdivision
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 29
use wca2
SELECT * 
FROM tt620_Bankruptcy 
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 30
use wca2
SELECT * 
FROM tt620_Financial_Year_Change
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 31
use wca2
SELECT * 
FROM tt620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 32
use wca2
SELECT * 
FROM tt620_Issuer_Name_change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 33
use wca2
SELECT * 
FROM tt620_Class_Action
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 34
use wca2
SELECT * 
FROM tt620_Security_Description_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 35
use wca2
SELECT * 
FROM tt620_Assimilation
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 36
use wca2
SELECT * 
FROM tt620_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM tt620_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 38
use wca2
SELECT *  
FROM tt620_New_Listing
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 39
use wca2
SELECT *  
FROM tt620_Announcement
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 40
use wca2
SELECT *  
FROM tt620_Parvalue_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 41
use wca2
SELECT *  
FROM tt620_Currency_Redenomination 
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 42
use wca2
SELECT *  
FROM tt620_Return_of_Capital 
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 43
use wca2
SELECT *  
FROM tt620_Dividend
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 44
use wca2
SELECT *  
FROM tt620_Dividend_Reinvestment_Plan
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 45
use wca2
SELECT *  
FROM tt620_Franking
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM tt620_Conversion_Terms_Change
WHERE
(excountry= 'BE'
or excountry= 'BR'
or excountry= 'CH'
or excountry= 'DE'
or excountry= 'DK'
or excountry= 'ES'
or excountry= 'FI'
or excountry= 'FR'
or excountry= 'GB'
or excountry= 'HK'
or excountry= 'IT'
or excountry= 'JP'
or excountry= 'KR'
or excountry= 'LU'
or excountry= 'NL'
or excountry= 'NO'
or excountry= 'PT'
or excountry= 'SE'
or excountry= 'SG'
or excountry= 'TW'
or excountry= 'EE'
or excountry= 'IS'
or excountry= 'LT'
or excountry= 'LV')

AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
