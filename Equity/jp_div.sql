--filepath=o:\Datafeed\Equity\630\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.630
--suffix=
--fileheadertext=EDI_630_JP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\630\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
SELECT * 
FROM v54f_620_Dividend
WHERE
Recdate='2007/03/30'
and excountry = 'JP'
ORDER BY CaRef
