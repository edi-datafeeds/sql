--filepath=o:\Datafeed\Equity\646\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=(select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
--fileextension=.646
--suffix=
--fileheadertext=EDI_REORG_646_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\646\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 0
use wca
select
'Levent' as f1,
'EventID' as f2,
'SecID' as f3,
'IssID' as f4,
'Created' as f5,
'Changed' as f6,
'Actflag' as f7,
'PrimaryExchgCD' as f8,
'Isin' as f9,
'PrimaryDate' as f10,
'Enddate1' as f11,
'SecondaryDate' as f12,
'Enddate2' as f13,
'TertiaryDate' as f14,
'Enddate3' as f15,
'RatioNew' as f16,
'RatioOld' as f17,
'Fractions' as f18,
'ResSecID' as f19,
'ResIsin' as f20,
'Addfld1' as f21,
'AddInfo1' as f22,
'Addfld2' as f23,
'AddInfo2' as f24,
'Addfld3' as f25,
'AddInfo3' as f26


--# 1
use wca
select distinct
'Bankruptcy' as Levent,
BKRP.BkrpID as EventID,
SCMST.SecID,
SCMST.IssID,
BKRP.AnnounceDate as Created,
BKRP.Acttime as Changed,
BKRP.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.Isin,
'FilingDate' as PrimaryDate,
BKRP.FilingDate as Enddate1,
'NotificationDate' as SecondaryDate,
BKRP.NotificationDate as Enddate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BKRP
INNER JOIN SCMST ON SCMST.IssID = BKRP.IssID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
BKRP.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 


--# 2
use wca
select distinct
'Stock Dividend' as Levent,
DIV.DivID as EventID,
SCMST.SecID,
SCMST.IssID,
DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DIV.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DIV.Acttime) THEN PEXDT.Acttime ELSE DIV.Acttime END as Changed,
DIV.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
case when PEXDT.PayDate2 is not null then PEXDT.PayDate2 else PEXDT.PayDate end as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
DIVPY.RatioNew,
DIVPY.RatioOld,
DIVPY.Fractions,
DIVPY.ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DIV
INNER JOIN RD ON RD.RdID = DIV.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON DIV.DivID = DIVPY.DivID
                     and ('B' = DIVPY.DivType or 'S' = DIVPY.DivType)
                      and DIVPY.Actflag<>'D'
LEFT OUTER JOIN SCMST as SCMSTRES ON DIVPY.ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID
     AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER join v10s_divpy as DVOP1 ON DIV.DivID = DVOP1.DivID
                     and 1 = DVOP1.OptionID
LEFT OUTER join v10s_divpy as DVOP2 ON DIV.DivID = DVOP2.DivID
                     and 2 = DVOP2.OptionID
LEFT OUTER join v10s_divpy as DVOP3 ON DIV.DivID = DVOP3.DivID
                     and 3 = DVOP3.OptionID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(DIVPY.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and DIVPY.OptionID is not null
and (((DVOP1.actflag = 'D' or DVOP1.DivID is null) and (DVOP2.actflag = 'D' or DVOP2.DivID is null))
        OR
    ((DVOP1.actflag = 'D' or DVOP1.DivID is null) and (DVOP3.actflag = 'D' or DVOP3.DivID is null))
        OR
    ((DVOP2.actflag = 'D' or DVOP2.DivID is null) and (DVOP3.actflag = 'D' or DVOP3.DivID is null)))
and wca.dbo.scexh.ListStatus<>'D'


--# 3
use wca
select distinct
'Stock Option Alert' as Levent,
DIV.DivID as EventID,
SCMST.SecID,
SCMST.IssID,
DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DIV.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DIV.Acttime) THEN PEXDT.Acttime ELSE DIV.Acttime END as Changed,
case when DIV.ActFlag<>'U' then DIV.ActFlag else 'I' end as Actflag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
case when PEXDT.PayDate2 is not null then PEXDT.PayDate2 else PEXDT.PayDate end as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
DIVPY.ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DIV
INNER JOIN RD ON RD.RdID = DIV.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON DIV.DivID = DIVPY.DivID
                     and ('B' = DIVPY.DivType or 'S' = DIVPY.DivType)
                      and DIVPY.Actflag<>'D'
LEFT OUTER JOIN SCMST as SCMSTRES ON DIVPY.ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID
     AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER join v10s_divpy as DVOP1 ON DIV.DivID = DVOP1.DivID
                     and 1 = DVOP1.OptionID
LEFT OUTER join v10s_divpy as DVOP2 ON DIV.DivID = DVOP2.DivID
                     and 2 = DVOP2.OptionID
LEFT OUTER join v10s_divpy as DVOP3 ON DIV.DivID = DVOP3.DivID
                     and 3 = DVOP3.OptionID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(DIVPY.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and DIVPY.OptionID is not null
and NOT (((DVOP1.actflag = 'D' or DVOP1.DivID is null) and (DVOP2.actflag = 'D' or DVOP2.DivID is null))
        OR
    ((DVOP1.actflag = 'D' or DVOP1.DivID is null) and (DVOP3.actflag = 'D' or DVOP3.DivID is null))
        OR
    ((DVOP2.actflag = 'D' or DVOP2.DivID is null) and (DVOP3.actflag = 'D' or DVOP3.DivID is null)))
and wca.dbo.scexh.ListStatus<>'D'


--# 4
use wca
select distinct
'Call' as Levent,
CALL.CallID as EventID,
SCMST.SecID,
SCMST.IssID,
CALL.AnnounceDate as Created,
CASE WHEN RD.Acttime > CALL.Acttime THEN RD.Acttime ELSE CALL.Acttime END as Changed,
CALL.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'DueDate' as PrimaryDate,
CALL.DueDate as Enddate1,
'RecDate' as SecondaryDate,
RD.RecDate as Endate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CALL
INNER JOIN SCMST ON SCMST.SecID = CALL.SecID
LEFT OUTER JOIN RD ON CALL.RdID = RD.RdID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(CALL.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and CALL.Actflag <> 'D'


--# 5
use wca
select distinct
'Capital Reduction' as Levent,
CAPRD.CaprdID as EventID,
SCMST.SecID,
SCMST.IssID,
CAPRD.AnnounceDate as Created,
CASE WHEN RD.Acttime > CAPRD.Acttime THEN RD.Acttime ELSE CAPRD.Acttime END as Changed,
CAPRD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
'PayDate' as PrimaryDate,
CAPRD.PayDate as Enddate1,
'EffectiveDate' as SecondaryDate,
CAPRD.EffectiveDate as Endate2,
'RecDate' as TertiaryDate,
RD.RecDate as Endate3,
CAPRD.NewRatio as RatioNew,
CAPRD.OldRatio as RatioOld,
CAPRD.Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CAPRD
INNER JOIN SCMST ON SCMST.SecID = CAPRD.SecID
LEFT OUTER JOIN RD ON CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON CaprdID = ICC.RelEventID AND 'CAPRD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(CAPRD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and CAPRD.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 6
use wca
select distinct
'Arrangement' as Levent,
ARR.RdID as EventID,
SCMST.SecID,
SCMST.IssID,
ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > ARR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > ARR.Acttime) THEN PEXDT.Acttime ELSE ARR.Acttime END as Changed,
ARR.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM ARR
INNER JOIN RD ON RD.RdID = ARR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON ARR.RdID = ICC.RelEventID AND 'ARR' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(ARR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and ARR.Actflag <> 'D'


--# 7
use wca
select distinct
'Bonus' as Levent,
BON.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > BON.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > BON.Acttime) THEN PEXDT.Acttime ELSE BON.Acttime END as Changed,
BON.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BON
INNER JOIN RD ON RD.RdID = BON.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(BON.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and BON.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 8
use wca
select distinct
'Bonus Rights' as Levent,
BR.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
BR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > BR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > BR.Acttime) THEN PEXDT.Acttime ELSE BR.Acttime END as Changed,
BR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BR
INNER JOIN RD ON RD.RdID = BR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(BR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and BR.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 9
use wca
select distinct
'Consolidation' as Levent,
CONSD.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > CONSD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > CONSD.Acttime) THEN PEXDT.Acttime ELSE CONSD.Acttime END as Changed,
CONSD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM CONSD
INNER JOIN RD ON RD.RdID = CONSD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON CONSD.RdId = ICC.RelEventID AND 'CONSD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(CONSD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and CONSD.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 10
use wca
select distinct
'Demerger Alert' as Levent,
DMRGR.RdID as EventID,
SCMST.SecID,
SCMST.IssID,
DMRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DMRGR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DMRGR.Acttime) THEN PEXDT.Acttime ELSE DMRGR.Acttime END as Changed,
DMRGR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DMRGR
INNER JOIN RD ON RD.RdID = DMRGR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(DMRGR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and DMRGR.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 11
use wca
select distinct
'Distribution Alert' as Levent,
DIST.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
DIST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DIST.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DIST.Acttime) THEN PEXDT.Acttime ELSE DIST.Acttime END as Changed,
DIST.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DIST
INNER JOIN RD ON RD.RdID = DIST.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(DIST.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and DIST.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 12
use wca
select distinct
'Divestment' as Levent,
DVST.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > DVST.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > DVST.Acttime) THEN PEXDT.Acttime ELSE DVST.Acttime END as Changed,
DVST.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DVST
INNER JOIN RD ON RD.RdID = DVST.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(DVST.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and DVST.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 16
use wca
select distinct
'Rights' as Levent,
RTS.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
RTS.AnnounceDate as Created,
RD.Acttime Changed,
RTS.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'EndTrade' as PrimaryDate,
RTS.EndTrade as Enddate1,
'StartTrade' as SecondaryDate,
RTS.StartTrade as EndDate2,
'SplitDate' as TertiaryDate,
RTS.SplitDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM RTS
INNER JOIN RD ON RD.RdID = RTS.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
RTS.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
and RTS.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 13
use wca
select distinct
'Entitlement' as Levent,
ENT.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > ENT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > ENT.Acttime) THEN PEXDT.Acttime ELSE ENT.Acttime END as Changed,
ENT.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM ENT
INNER JOIN RD ON RD.RdID = ENT.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(ENT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and ENT.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 14
use wca
select distinct
'Sub-Division' as Levent,
SD.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SD.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > SD.Acttime) THEN PEXDT.Acttime ELSE SD.Acttime END as Changed,
SD.ActFlag,
SCMST.PrimaryExchgCD,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN SCMST.ISIN ELSE ICC.OldISIN END as Isin,
'NewCodeDate' as PrimaryDate,
ICC.Acttime as EndDate1,
'PayDate' as SecondaryDate,
PEXDT.PayDate as EndDate2,
'RecDate' as TertiaryDate,
PEXDT.ExDate as EndDate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
SCMST.SecID as ResSecid,
CASE WHEN (ICC.RelEventID is null OR ICC.NewISIN = '') THEN '' ELSE ICC.NewISIN END as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM SD
INNER JOIN RD ON RD.RdID = SD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON SD.RdId = ICC.RelEventID AND 'SD' = ICC.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(SD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or ICC.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and SD.Actflag <> 'D'
and wca.dbo.scexh.ListStatus<>'D'


--# 15
use wca
select distinct
'Merger Alert' as Levent,
MRGR.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
MRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > MRGR.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > MRGR.Acttime) THEN PEXDT.Acttime ELSE MRGR.Acttime END as Changed,
MRGR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'AppointedDate' as PrimaryDate,
MRGR.AppointedDate as Enddate1,
'EffectiveDate' as SecondaryDate,
MRGR.EffectiveDate as EndDate2,
'PayDate' as TertiaryDate,
PEXDT.PayDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM MRGR
INNER JOIN RD ON RD.RdID = MRGR.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(MRGR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and MRGR.Actflag <> 'D'




--# 17
use wca
select distinct
'Preferential Offer' as Levent,
PRF.RdId as EventID,
SCMST.SecID,
SCMST.IssID,
PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > PRF.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > PRF.Acttime) THEN PEXDT.Acttime ELSE PRF.Acttime END as Changed,
PRF.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
RatioNew,
RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM PRF
INNER JOIN RD ON RD.RdID = PRF.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(PRF.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and PRF.Actflag <> 'D' 
and wca.dbo.scexh.ListStatus<>'D'



--# 18
use wca
select distinct
'Assimilation' as Levent,
ASSM.AssmId as EventID,
SCMST.SecID,
SCMST.IssID,
ASSM.AnnounceDate as Created,
ASSM.Acttime as Changed,
ASSM.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'AssimilationDate' as PrimaryDate,
AssimilationDate as EndDate1,
'' as SecondaryDate,
'' as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM ASSM
INNER JOIN SCMST ON ASSM.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
ASSM.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
and ASSM.Actflag <> 'D' 





--# 19
use wca
select distinct
'Buy Back Alert' as Levent,
BB.BBId as EventID,
SCMST.SecID,
SCMST.IssID,
BB.AnnounceDate as Created,
BB.Acttime as Changed,
BB.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'StartDate' as PrimaryDate,
StartDate as EndDate1,
'StartDate' as SecondaryDate,
EndDate as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM BB
INNER JOIN SCMST ON BB.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
BB.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
and BB.Actflag <> 'D' 
and wca.dbo.scexh.ListStatus<>'D'




--# 20
use wca
select distinct
'Liquidation Alert' as Levent,
LIQ.LiqID as EventID,
SCMST.SecID,
SCMST.IssID,
LIQ.AnnounceDate as Created,
LIQ.Acttime as Changed,
LIQ.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'RdDate' as PrimaryDate,
RdDate as EndDate1,
'' as SecondaryDate,
'' as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM LIQ
INNER JOIN SCMST ON LIQ.IssID = SCMST.IssID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
LIQ.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
and LIQ.Actflag <> 'D' 





--# 21
use wca
select distinct
'Redemption' as Levent,
REDEM.RedemID as EventID,
SCMST.SecID,
SCMST.IssID,
REDEM.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > REDEM.Acttime) THEN RD.Acttime ELSE REDEM.Acttime END as Changed,
REDEM.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'RedemDate' as PrimaryDate,
REDEM.RedemDate as EndDate1,
'RecDate' as SecondaryDate,
RD.RecDate as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM REDEM
LEFT OUTER JOIN RD ON REDEM.RdID = RD.RdID
INNER JOIN SCMST ON REDEM.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(REDEM.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3))
and REDEM.Actflag <> 'D' 



--# 22
use wca
select distinct
'Takeover Alert' as Levent,
TKOVR.TKOVRID as EventID,
SCMST.SecID,
SCMST.IssID,
TKOVR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > TKOVR.Acttime) THEN RD.Acttime ELSE TKOVR.Acttime END as Changed,
TKOVR.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'CloseDate' as PrimaryDate,
CloseDate as EndDate1,
'CmAcqDate' as SecondaryDate,
CmAcqDate as EndDate2,
'RecDate' as TertiaryDate,
RecDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM TKOVR
LEFT OUTER JOIN RD ON TKOVR.RdID = RD.RdID
INNER JOIN SCMST ON TKOVR.SecID = SCMST.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(TKOVR.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3))
and TKOVR.Actflag <> 'D' 





--# 23
use wca
select distinct
'Security Swap' as Levent,
SCSWP.RdID as EventID,
SCMST.SecID,
SCMST.IssID,
SCSWP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SCSWP.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > SCSWP.Acttime) THEN PEXDT.Acttime ELSE SCSWP.Acttime END as Changed,
SCSWP.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'PayDate' as PrimaryDate,
PEXDT.PayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'RecDate' as TertiaryDate,
RD.RecDate as Enddate3,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM SCSWP
INNER JOIN RD ON RD.RdID = SCSWP.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SCSWP' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(SCSWP.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and SCSWP.Actflag <> 'D' 




--# 24
use wca
select distinct
'Security Reclassification' as Levent,
SECRC.SecrcID as EventID,
SCMST.SecID,
SCMST.IssID,
SECRC.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > SECRC.Acttime) THEN RD.Acttime ELSE SECRC.Acttime END as Changed,
SECRC.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'EffectiveDate' as PrimaryDate,
EffectiveDate as EndDate1,
'RecDate' as SecondaryDate,
RecDate as EndDate2,
'' as TertiaryDate,
'' as Enddate3,
RatioNew,
RatioOld,
'' as Fractions,
ResSecID,
SCMSTRES.Isin as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM SECRC
INNER JOIN RD ON RD.RdID = SECRC.SecRcID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN SCMST as SCMSTRES ON ResSecID = SCMSTRES.SecID
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(SECRC.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3))
and SECRC.Actflag <> 'D' 
and wca.dbo.scexh.ListStatus<>'D'


--# 25
use wca
select distinct
'DRIP' as Levent,
DRIP.DivID as EventID,
SCMST.SecID,
SCMST.IssID,
DRIP.AnnounceDate as Created,
DRIP.Acttime as Changed,
DRIP.ActFlag,
SCMST.PrimaryExchgCD,
SCMST.ISIN,
'DripPayDate' as PrimaryDate,
DRIP.DripPayDate as EndDate1,
'ExDate' as SecondaryDate,
PEXDT.ExDate as EndDate2,
'LastDate' as TertiaryDate,
DRIP.DripLastDate as Enddate3,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as ResSecID,
'' as ResIsin,
'' as Addfld1,
'' as AddInfo1,
'' as Addfld2,
'' as AddInfo2,
'' as Addfld3,
'' as AddInfo3
FROM DRIP
LEFT OUTER JOIN DIV ON DRIP.DivID = DIV.DivID
INNER JOIN RD ON DIV.RdID = RD.RdID
INNER JOIN SCMST ON RD.SecID = SCMST.SecID
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
inner join wca.dbo.scexh on wca.dbo.scmst.secid = wca.dbo.scexh.secid
left outer join wca.dbo.sectygrp on wca.dbo.scmst.sectycd = wca.dbo.sectygrp.sectycd
left outer join wca.dbo.exchg on wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
Where
(DRIP.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or RD.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) 
or PEXDT.Acttime between (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1) and (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3) )
and DRIP.Actflag <> 'D' 
