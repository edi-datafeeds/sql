--filepath=o:\Datafeed\Equity\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime


--# 3
USE WCA
SELECT *  
FROM v50f_620_Liquidation

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'

ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID


--# 12
use wca
SELECT *  
FROM v53f_620_Capital_Reduction

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol


--# 16
use wca
SELECT *  
FROM v54f_620_Bonus

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
SELECT *  
FROM v54f_620_Bonus_Rights

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
SELECT *  
FROM v54f_620_Consolidation

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca
SELECT *  
FROM v54f_620_Demerger

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
SELECT *  
FROM v54f_620_Distribution

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID


--# 28
use wca
SELECT * 
FROM v54f_620_Subdivision

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
SELECT *  
FROM v50f_620_Announcement

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
use wca
SELECT *  
FROM v51f_620_Parvalue_Redenomination 

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
SELECT *  
FROM v51f_620_Currency_Redenomination 

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
SELECT *  
FROM v53f_620_Return_of_Capital 

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
SELECT *  
FROM v54f_620_Dividend

WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (SecStatus <> 'I' OR Secstatus IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 46
USE WCA
SELECT * 
FROM v57f_901_Interest_Payment
WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (Statusflag <> 'I' OR statusflag IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

--# 47
use wca
SELECT * 
FROM v57f_901_Interest_Rate_Change
WHERE ((CHANGED >= '2006/03/01' and changed < '2006/04/01') or (created >= '2006/03/01' and created < '2006/04/01'))
AND (Statusflag <> 'I' OR statusflag IS NULL) and isin in (select col001 from portfolio.dbo.isin_mar) and primaryex = 'Yes'
ORDER BY EventID desc, ExchgCD, Sedol

