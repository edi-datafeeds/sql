--filepath=o:\Datafeed\Equity\DeShaw_631_632\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.632
--suffix=
--fileheadertext=EDI_REORG_632_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\DeShaw_631_632\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select 
v10s_DIV.Levent,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
case when DIVPY.Actflag='D' OR DIVPY.Actflag='C' then DIVPY.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
where 
(v10s_DIV.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and DIVPY.OptionID is not null
and DIVPY.divtype = 'S'
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

union

select 
v10s_BON.Levent,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
case when v10s_BON.Actflag='D' OR v10s_BON.Actflag='C' then v10s_BON.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
where
(v10s_BON.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' 
and v20c_EV_dscexh.exchgcd is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

union

select 
v10s_CONSD.Levent,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
case when v10s_CONSD.Actflag='D' OR v10s_CONSD.Actflag='C' then v10s_CONSD.Actflag ELSE '' END as Actflag,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUscode = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldUscode END as Uscode,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(v10s_CONSD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or SDCHG.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or ICC.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

union

select 
v10s_DMRGR.Levent,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
case when v10s_DMRGR.Actflag='D' OR v10s_DMRGR.Actflag='C' then v10s_DMRGR.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
where
(v10s_DMRGR.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

union


select 
v10s_ENT.Levent,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
case when v10s_ENT.Actflag='D' OR v10s_ENT.Actflag='C' then v10s_ENT.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
where
(v10s_ENT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' 
and v20c_EV_dscexh.exchgcd is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

union

select 
v10s_SD.Levent,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
case when v10s_SD.Actflag='D' OR v10s_SD.Actflag='C' then v10s_SD.Actflag ELSE '' END as Actflag,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUscode = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldUscode END as Uscode,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(v10s_SD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or SDCHG.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or ICC.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

union

select 
v10s_RTS.Levent,
v10s_RTS.AnnounceDate as Created,
RD.Acttime Changed,
case when v10s_RTS.Actflag='D' OR v10s_RTS.Actflag='C' then v10s_RTS.Actflag ELSE '' END as Actflag,
v20c_EV_SCMST.ISIN,
v20c_EV_SCMST.Uscode,
v20c_EV_dscexh.Sedol,
v20c_EV_SCMST.PrimaryExchgCD,
v20c_EV_dscexh.ExchgCD,
v20c_EV_dscexh.Localcode,
v10s_RTS.EndTrade as Exdate
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
where
(v10s_RTS.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or RD.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or EXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1)
or PEXDT.Acttime >= (select max(acttime) from tbl_Opslog where seq = 1))
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)

order by levent, exdate desc

