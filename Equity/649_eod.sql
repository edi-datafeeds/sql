--filepath=o:\Datafeed\Equity\649\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.649
--suffix=
--fileheadertext=EDI_REORG_649_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\649\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
SELECT *
FROM v50f_620_Class_Action
WHERE
(excountry = 'US' 
or excountry = 'CA' 
or excountry = 'AU' )
and substring(primaryexchgcd,1,2)='GB'
ORDER BY CaRef
