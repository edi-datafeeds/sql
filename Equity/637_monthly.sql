--filepath=o:\Datafeed\Equity\637\
--filenameprefix=
--filename=yyyymmdd
--filenamesql=
--fileextension=.637
--suffix=
--fileheadersql=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\637\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--filefootertext=

--# 1
use wca
select 
v10s_LIQ.Levent,
v10s_LIQ.EventID,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as Changed,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
MPAY.Paydate
FROM v10s_LIQ
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
where
MPAY.Paydate between getdate()-40 and getdate() 
and v20c_EV_dSCEXH.secid is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null
and v10s_LIQ.Actflag <> 'D'
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_DIV.Levent,
v10s_DIV.EventID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
where 
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and DIVPY.OptionID is not null
and v10s_DIV.Actflag <> 'D'
and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_BB.Levent,
v10s_BB.EventID,
v10s_BB.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_BB.Acttime) THEN MPAY.Acttime ELSE v10s_BB.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
MPAY.PayDate
FROM v10s_BB
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_BB.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN MPAY ON v10s_BB.EventID = MPAY.EventID AND v10s_BB.SEvent = MPAY.SEvent
where
MPAY.Paydate between getdate()-40 and getdate()
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_BB.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_CAPRD.Levent,
v10s_CAPRD.EventID,
v10s_CAPRD.AnnounceDate as Created,
v10s_CAPRD.Acttime as Changed,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
v10s_CAPRD.PayDate
FROM v10s_CAPRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CAPRD.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
v10s_CAPRD.Paydate between getdate()-40 and getdate()
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_CAPRD.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_RCAP.Levent,
v10s_RCAP.EventID,
v10s_RCAP.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_RCAP.Acttime THEN RD.Acttime ELSE v10s_RCAP.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
v10s_RCAP.CSPYDate as Paydate
FROM v10s_RCAP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_RCAP.SecID
LEFT OUTER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
where
v10s_RCAP.CSPYDate between getdate()-40 and getdate()
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_RCAP.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_TKOVR.Levent,
v10s_TKOVR.EventID,
v10s_TKOVR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_TKOVR.Acttime) THEN MPAY.Acttime ELSE v10s_TKOVR.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dSCEXH.Sedol,
MPAY.Paydate
FROM v10s_TKOVR
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_TKOVR.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID AND v10s_TKOVR.SEvent = MPAY.SEvent
where
MPAY.Paydate between getdate()-40 and getdate()
and v20c_EV_dSCEXH.secid is not null and v20c_EV_dSCEXH.exchgcd <> '' and v20c_EV_dSCEXH.exchgcd is not null
and v10s_TKOVR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'
and v10s_TKOVR.CmAcqDate is not null

union

select 
v10s_ARR.Levent,
v10s_ARR.EventID,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_ARR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_BON.Levent,
v10s_BON.EventID,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_BON.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_BR.Levent,
v10s_BR.EventID,
v10s_BR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BR.Acttime) THEN PEXDT.Acttime ELSE v10s_BR.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_BR
INNER JOIN RD ON RD.RdID = v10s_BR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'BR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_BR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_CONSD.Levent,
v10s_CONSD.EventID,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_CONSD.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_DMRGR.Levent,
v10s_DMRGR.EventID,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_DMRGR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_DIST.Levent,
v10s_DIST.EventID,
v10s_DIST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIST.Acttime) THEN PEXDT.Acttime ELSE v10s_DIST.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_DIST.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select
v10s_DVST.Levent,
v10s_DVST.EventID,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_DVST.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_ENT.Levent,
v10s_ENT.EventID,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_ENT.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_SD.Levent,
v10s_SD.EventID,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_dscexh.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_dscexh.ExCountry = SDCHG.CntryCD AND v20c_EV_dscexh.RegCountry = SDCHG.RcntryCD
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_SD.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_MRGR.Levent,
v10s_MRGR.EventID,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_MRGR.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_RTS.Levent,
v10s_RTS.EventID,
v10s_RTS.AnnounceDate as Created,
RD.Acttime Changed,
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'RTS' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'RTS' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_RTS.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_PRF.Levent,
v10s_PRF.EventID,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
where
(EXDT.Paydate between getdate()-40 and getdate()
or PEXDT.Paydate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_PRF.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'

union

select 
v10s_PO.Levent,
v10s_PO.EventID,
v10s_PO.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PO.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PO.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PO.Acttime) THEN PEXDT.Acttime ELSE v10s_PO.Acttime END as [Changed],
v20c_EV_SCMST.Issuername,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ISIN,
v20c_EV_dscexh.Sedol,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as Paydate
FROM v10s_PO
INNER JOIN RD ON RD.RdID = v10s_PO.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_dscexh ON v20c_EV_SCMST.SecID = v20c_EV_dscexh.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_dscexh.ExchgCD = EXDT.ExchgCD AND 'PO' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PO' = PEXDT.EventType
where
(EXDT.PayDate between getdate()-40 and getdate()
or PEXDT.PayDate between getdate()-40 and getdate())
and v20c_EV_dscexh.secid is not null and v20c_EV_dscexh.exchgcd <> '' and v20c_EV_dscexh.exchgcd is not null
and v10s_PO.Actflag <> 'D' and (v20c_EV_SCMST.Statusflag <> 'I' or v20c_EV_SCMST.Statusflag is null)
and (v20c_EV_SCMST.ISIN<>'' OR v20c_EV_dscexh.Sedol is not null)
and v20c_EV_dscexh.exchgcd = 'GBLSE'
