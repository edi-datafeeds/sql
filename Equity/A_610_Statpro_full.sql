--filepath=o:\Datafeed\Equity\610_Statpro\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.610
--suffix=PFS
--fileheadertext=EDI_REORG_610_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\610_Statpro\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
use wca
SELECT * 
FROM v50f_610_Company_Meeting as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL)
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 2
use wca
SELECT *
FROM v53f_610_Call as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 3
use wca
SELECT * 
FROM v50f_610_Liquidation as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 4
use wca
SELECT *
FROM v51f_610_Certificate_Exchange as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 5
use wca
SELECT * 
FROM v51f_610_International_Code_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')




--# 7
use wca
SELECT * 
FROM v51f_610_Preference_Redemption as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 8
use wca
SELECT * 
FROM v51f_610_Security_Reclassification as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 9
use wca
SELECT * 
FROM v52f_610_Lot_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 10
use wca
SELECT * 
FROM v52f_610_Sedol_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 11
use wca
SELECT * 
FROM v53f_610_Buy_Back as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 12
use wca
SELECT * 
FROM v53f_610_Capital_Reduction as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 13
use wca
SELECT * 
FROM v53f_610_Takeover as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 14
use wca
SELECT * 
FROM v54f_610_Arrangement as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 15
use wca
SELECT * 
FROM v54f_610_Bonus as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 16
use wca
SELECT * 
FROM v54f_610_Bonus_Rights  as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 17
use wca
SELECT * 
FROM v54f_610_Consolidation as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 18
use wca
SELECT * 
FROM v54f_610_Demerger as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 19
use wca
SELECT * 
FROM v54f_610_Distribution as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 20
use wca
SELECT * 
FROM v54f_610_Divestment as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 21
use wca
SELECT * 
FROM v54f_610_Entitlement as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 22
use wca
SELECT * 
FROM v54f_610_Merger as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 23
use wca
SELECT * 
FROM v54f_610_Preferential_Offer as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 24
use wca
SELECT * 
FROM v54f_610_Purchase_Offer as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 25
use wca
SELECT * 
FROM v54f_610_Rights  as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 26
use wca
SELECT * 
FROM v54f_610_Security_Swap  as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 27
use wca
SELECT *
FROM v54f_610_Subdivision as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 28
use wca
SELECT *
FROM v50f_610_Bankruptcy  as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 29
use wca
SELECT *
FROM v50f_610_Financial_Year_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 30
use wca
SELECT *
FROM v50f_610_Incorporation_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 31
use wca
SELECT *
FROM v50f_610_Issuer_Name_change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 32
use wca
SELECT *
FROM v50f_610_lawsuit as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 33
use wca
SELECT *
FROM v51f_610_Security_Description_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 34
use wca
SELECT *
FROM v52f_610_Assimilation as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 35
use wca
SELECT *
FROM v52f_610_Listing_Status_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 36
use wca
SELECT *
FROM v52f_610_Local_Code_Change as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 37
use wca
SELECT * 
FROM v52f_610_New_Listing as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 38
use wca
SELECT * 
FROM v50f_610_Announcement as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 39
use wca
SELECT * 
FROM v51f_610_Parvalue_Redenomination  as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 40
use wca
SELECT * 
FROM v51f_610_Currency_Redenomination  as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 41
use wca
SELECT * 
FROM v53f_610_Return_of_Capital  as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')




--# 42
use wca
SELECT * 
FROM v54f_610_Dividend_Statpro as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
changed>'2013/01/01'
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')


--# 43
use wca
SELECT * 
FROM v54f_610_Dividend_Reinvestment_Plan as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')



--# 44
use wca
SELECT * 
FROM v54f_610_Franking as vtab
inner join scmst on vtab.secid = scmst.secid and 'PFS'= scmst.sectycd
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
and changed>'2013/01/01'
--and (primaryex='Yes' or primaryex = '')


