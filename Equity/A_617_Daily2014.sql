--filepath=O:\DATAFEED\EQUITY\617DUP\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.617
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\EQUITY\617\
--fieldheaders=u
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
USE WCA
SELECT
SUBSTRING(V20C_ISO_SCEXH.EXCHGID,2,3)+CAST(V20C_ISO_SCEXH.SEQNUM AS CHAR(1))+ CAST(V54F_610_DIVIDEND.EVENTID AS CHAR(16)) AS CAREFID,
'DIV',
EVENTID,
ISSID,
V54F_610_DIVIDEND.SECID,
CREATED,
CHANGED,
[ACTION],
CNTRYOFINCORP,
ISSUERNAME,
SECURITYDESC,	
PVCURRENCY,
ISIN,
USCODE,
SECSTATUS,
PRIMARYEX,
EXCHANGE,
V54F_610_DIVIDEND.EXCHGCD,
V54F_610_DIVIDEND.MIC,
V54F_610_DIVIDEND.EXCOUNTRY,
V54F_610_DIVIDEND.REGCOUNTRY,
V54F_610_DIVIDEND.SEDOL,
V54F_610_DIVIDEND.LOCALCODE,
V54F_610_DIVIDEND.LISTINGSTATUS,
V54F_610_DIVIDEND.LISTDATE,
V54F_610_DIVIDEND.LOT,
V54F_610_DIVIDEND.MINTRDGQTY,
RECDATE,
EXDATE,
PAYDATE,
PAYDATE2,
FYEDATE,
DIVPERIODCD,
TBAFLAG,
CASHOPT,
STOCKOPT,
OPTIONID,
ACTIONDIVPY,
DIVTYPE,
CURENCD,
NILDIVIDEND,
APPROXFLAG,
DIVINPERCENT,
GROSSDIVIDEND,
NETDIVIDEND,
TAXRATE,
RECINDCASHDIV,
USDRATETOCURRENCY,
RESSECTYPE,
FRACTION,
RECINDSTOCKDIV,
DEPFEES,
COUPON,
COUPONID,
RESSECID,
RESISIN,
RATIONEW,
RATIOOLD,
CASE WHEN OPTIONID IS NULL THEN
   V54F_610_DIVIDEND.SEDOL+RTRIM(CAST(EVENTID AS CHAR(10)))+'1'
ELSE
   V54F_610_DIVIDEND.SEDOL+RTRIM(CAST(EVENTID AS CHAR(10)))+RTRIM(CAST(OPTIONID AS CHAR(10)))
END AS SEDOLEVENTOPT,
GROUP2GROSSDIV
FROM V54F_610_DIVIDEND
LEFT OUTER JOIN V20C_ISO_SCEXH ON V54F_610_DIVIDEND.SECID = V20C_ISO_SCEXH.SECID
                  AND V54F_610_DIVIDEND.EXCHGCD = V20C_ISO_SCEXH.EXCHGCD
WHERE CHANGED >= (SELECT MAX(ACTTIME) FROM TBL_OPSLOG WHERE SEQ = 1) AND (LISTINGSTATUS <> 'DE-LISTED' OR LISTINGSTATUS IS NULL)
AND (SECSTATUS <> 'INACTIVE' OR SECSTATUS IS NULL)
AND V54F_610_DIVIDEND.SEDOL <> ''
AND V54F_610_DIVIDEND.SEDOL IS NOT NULL
AND (V54F_610_DIVIDEND.PAYDATE IS NULL OR V54F_610_DIVIDEND.PAYDATE>GETDATE()-365)
ORDER BY SEDOLEVENTOPT
