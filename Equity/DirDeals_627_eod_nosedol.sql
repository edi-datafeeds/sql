--filepath=o:\Datafeed\Equity\DirDeals_627\nosedol\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_DIVIDEND_DIRDEALS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\DirDeals_627\nosedol\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select *
FROM v54f_620NS_Dividend
WHERE
changed>(select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and primaryexchgcd = exchgcd
and primaryexchgcd is not null
