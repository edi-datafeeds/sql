--filepath=o:\Datafeed\Equity\620_au_full\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT v54f_620_Dividenddelist.*
FROM v54f_620_Dividenddelist
left outer join lstat on v54f_620_Dividenddelist.secid = lstat.secid and 
v54f_620_Dividenddelist.exchgcd = lstat.exchgcd and 'D'= 
lstat.lstatstatus and v54f_620_Dividenddelist.liststatus = 'D'
WHERE
eventid = 359860
and CREATED >= '2008/12/01'
and excountry='AU'
and (lstat.effectivedate is null or lstat.effectivedate>=Exdate)
ORDER BY EventID desc, v54f_620_Dividenddelist.ExchgCD, Sedol



--# 1
USE WCA
SELECT v54f_620_Franking.* 
FROM v54f_620_Franking
left outer join lstat on v54f_620_Franking.secid = lstat.secid and 
v54f_620_Franking.exchgcd = lstat.exchgcd and 'D'= lstat.lstatstatus and 
v54f_620_Franking.liststatus = 'D'
WHERE
eventid = 359860
and CREATED >= '2008/12/01'
and excountry='AU'
and (lstat.effectivedate is null or lstat.effectivedate>=Exdate)
and (v54f_620_Franking.frankflag<>'N')
ORDER BY EventID desc, v54f_620_Franking.ExchgCD, Sedol


--# 1
USE WCA
SELECT *
FROM v52f_620_Listing_Status_Change

WHERE
eventid = 756029
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and excountry='AU'
ORDER BY EventID desc, ExchgCD, Sedol

