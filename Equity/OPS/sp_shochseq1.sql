USE wca
GO
CREATE PROCEDURE sp_shochseq1
AS

DECLARE MyCursor CURSOR LOCAL FORWARD_ONLY FOR
  SELECT secid,seqnum,effectivedate FROM shochseq1 ORDER BY secid, effectivedate desc
 
OPEN MyCursor
DECLARE @Ssecid int
DECLARE @Tsecid int
DECLARE @Tseqnum int
DECLARE @Effectivedate datetime
DECLARE @cnt int
SET @cnt = 1
FETCH NEXT FROM MyCursor INTO @Tsecid, @Tseqnum, @Effectivedate
WHILE @@FETCH_STATUS = 0
BEGIN  
   IF @Ssecid = @Tsecid
   BEGIN      
      SET @cnt = @cnt + 1
      UPDATE shochseq1 SET shochseq1.seqnum= @cnt
      WHERE shochseq1.secid = @Tsecid and shochseq1.effectivedate = @Effectivedate
   END
   ELSE
   BEGIN  
       SET @cnt = 1
   END
   SET @Ssecid =  @Tsecid
   FETCH NEXT FROM MyCursor INTO @Tsecid, @Tseqnum, @Effectivedate
END
CLOSE MyCursor
DEALLOCATE MyCursor