CREATE PROCEDURE sp_sedolseq2()

BEGIN

DECLARE Ssecid int;
DECLARE Scntrycd char(2);
DECLARE Tsecid int;
DECLARE Tseqnum int;
DECLARE Tcntrycd char(2);
DECLARE Trcntrycd char(2);
DECLARE cnt int;

-- Declare the cursor
DECLARE mycursor CURSOR
FOR
  SELECT secid,cntrycd,rcntrycd,seqnum FROM sedolseq2 ORDER BY secid,cntrycd,rcntrycd;

-- Declare continue handler
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;

OPEN MyCursor;

SET cnt = 1;

FETCH MyCursor INTO Tsecid, Tcntrycd, Trcntrycd, Tseqnum;

REPEAT
   IF Ssecid = Tsecid and Scntrycd =Tcntrycd
   BEGIN 
      SET cnt = cnt + 1;
      UPDATE sedolseq2 SET sedolseq2.seqnum= cnt
      WHERE sedolseq2.secid = Tsecid and sedolseq2.cntrycd = Tcntrycd and sedolseq2.rcntrycd = Trcntrycd;
   END
   ELSE
   BEGIN   
       SET cnt = 1;
   END
   SET Ssecid =  Tsecid;
   SET  Scntrycd = Tcntrycd;
   FETCH MyCursor INTO Tsecid, Tcntrycd, Trcntrycd, Tseqnum
UNTIL done END REPEAT;

CLOSE MyCursor;

END;