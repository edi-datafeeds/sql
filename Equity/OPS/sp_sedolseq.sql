USE wca
GO
CREATE PROCEDURE sp_sedolseq2

AS

DECLARE MyCursor CURSOR LOCAL FORWARD_ONLY FOR
  SELECT secid,cntrycd,rcntrycd,seqnum FROM sedolseq2 ORDER BY secid,cntrycd,rcntrycd

OPEN MyCursor
DECLARE @Ssecid int
DECLARE @Scntrycd char(2)
DECLARE @Tsecid int
DECLARE @Tseqnum int
DECLARE @Tcntrycd char(2)
DECLARE @Trcntrycd char(2)
DECLARE @cnt int
SET @cnt = 1
FETCH NEXT FROM MyCursor INTO @Tsecid, @Tcntrycd, @Trcntrycd, @Tseqnum
WHILE @@FETCH_STATUS = 0
BEGIN   
   IF @Ssecid = @Tsecid and @Scntrycd =@Tcntrycd
   BEGIN 
      SET @cnt = @cnt + 1
      UPDATE sedolseq2 SET sedolseq2.seqnum= @cnt
      WHERE sedolseq2.secid = @Tsecid and sedolseq2.cntrycd = @Tcntrycd and sedolseq2.rcntrycd = @Trcntrycd
   END
   ELSE
   BEGIN   
       SET @cnt = 1
   END
   SET @Ssecid =  @Tsecid
   SET  @Scntrycd = @Tcntrycd  
   FETCH NEXT FROM MyCursor INTO @Tsecid, @Tcntrycd, @Trcntrycd, @Tseqnum
END
CLOSE MyCursor
DEALLOCATE MyCursor