print ""
go
print "STEP-1 insert new rts records for subrights flagging"
go
print ""
go
use wca
insert into subrights
(EventID, Changed)
select 
rts.RdID,
rts.Acttime
from rts
where
rts.acttime = (select max(acttime) from tbl_Opslog)
and 
((rts.starttrade is not null or rts.endtrade is not null) and (rts.starttrade > getdate()-1 or rts.endtrade > getdate()-1))
and not exists
(select subrights.eventid  from subrights
where rts.RdID = subrights.EventID)
go