/*
Script for Assigned subrights.
Use in Subrights program.
*/
use wca
select
cast(wca.dbo.Subrights.Acttime as VARCHAR(19)) as Acttime1,
wca.dbo.Subrights.Subrightsflag,
wca.dbo.Subrights.eventid,
wca.dbo.scmst.isin,
wca.dbo.issur.cntryofincorp,
wca.dbo.scmst.primaryexchgcd,
wca.dbo.issur.issuername,
wca.dbo.exchg.exchgname,
wca.dbo.rts.rtsnotes,
wca.dbo.Subrights.username
from wca.dbo.Subrights
left outer join wca.dbo.rts on wca.dbo.Subrights.eventid = wca.dbo.rts.rdid
left outer join wca.dbo.rd on wca.dbo.rts.rdid = wca.dbo.rd.rdid
left outer join wca.dbo.scmst on wca.dbo.rd.secid = wca.dbo.scmst.secid
left outer join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid
left outer join wca.dbo.exchg on wca.dbo.scmst.primaryexchgcd = wca.dbo.exchg.exchgcd
where wca.dbo.Subrights.Subrightsflag is not null
and wca.dbo.Subrights.Subrightsflag <> ''
and wca.dbo.Subrights.Subrightsflag <> 'U'
order by wca.dbo.Subrights.Acttime desc
