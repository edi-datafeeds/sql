CREATE PROCEDURE sp_sedolseq1()

BEGIN

DECLARE Ssecid int;
DECLARE Scntrycd char(2);
DECLARE Tsecid int;
DECLARE Tseqnum int;
DECLARE Tcntrycd char(2);
DECLARE Trcntrycd char(2);
DECLARE cnt int;

-- Declare the cursor
DECLARE mycursor CURSOR
FOR
  SELECT secid,cntrycd,rcntrycd,seqnum FROM sedolseq1 ORDER BY secid,cntrycd,rcntrycd;

-- Declare continue handler
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;

OPEN MyCursor;

SET cnt = 1;

FETCH MyCursor INTO Tsecid, Tcntrycd, Trcntrycd, Tseqnum;

REPEAT
   IF Ssecid = Tsecid and Scntrycd =Tcntrycd
   BEGIN 
      SET cnt = cnt + 1;
      UPDATE sedolseq1 SET sedolseq1.seqnum= cnt
      WHERE sedolseq1.secid = Tsecid and sedolseq1.cntrycd = Tcntrycd and sedolseq1.rcntrycd = Trcntrycd;
   END
   ELSE
   BEGIN   
       SET cnt = 1;
   END
   SET Ssecid =  Tsecid;
   SET  Scntrycd = Tcntrycd;
   FETCH MyCursor INTO Tsecid, Tcntrycd, Trcntrycd, Tseqnum
UNTIL done END REPEAT;

CLOSE MyCursor;

END;