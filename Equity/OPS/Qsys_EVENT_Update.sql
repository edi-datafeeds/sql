
print " Updating qsys.dbo.EVENTAnnouncement, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '01'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Cre' as Datetype,
created,
'','','','',''
from evi_Announcement
WHERE '01'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTArrangement, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '02'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when exdate is null then 'ExDate' else '' end as MissField01,
'','','',''
from evi_Arrangement
WHERE '02'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTAssimilation, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '03'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Assimilationdate,
Assimilationdate,
case when Assimilationdate is null then 'ExDate' else '' end as MissField01,
'','','',''
from evi_Assimilation
WHERE '03'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTBankruptcy, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '04'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Cre' as Datetype,
Created,
'','','','',''
from evi_Bankruptcy
WHERE '04'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTBonus, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '05'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Ratio is null then 'Ratio' else '' end as MissField04,
''
from evi_Bonus
WHERE '05'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTBonus_Rights, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '06'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Ratio is null then 'Ratio' else '' end as MissField04,
''
from evi_Bonus_Rights
WHERE '06'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTBuy_Back, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '07'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'End' as Datetype,
EndDate,
case when enddate is null then 'EndDate' else '' end as MissField01,
'','','',''
from evi_Buy_back
WHERE '07'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTCall, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '08'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'' as Datetype,
DueDate,
case when duedate is null then 'DueDate' else '' end as MissField01,
case when topremium is null then 'ToPremium' else '' end as MissField02,
'','',''
from evi_Call
WHERE '08'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTCapital_Reduction, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '09'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Pay' as Datetype,
PayDate,
case when Paydate is null then 'Paydate' else '' end as MissField01,
case when EffectiveDate is null then 'EffectiveDate' else '' end as MissField02,
'','',''
from evi_Capital_Reduction
WHERE '09'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTCertificate_Exchange, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '10'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'End' as Datetype,
EndDate,
case when Enddate is null then 'Enddate' else '' end as MissField01,
'','','',''
from evi_Certificate_Exchange
WHERE '10'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTClass_Action, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '11'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Cre' as Datetype,
Created,
'','','','',''
from evi_Class_Action
WHERE '11'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTCompany_Meeting, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '12'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'AGM' as Datetype,
AGMDate,
case when AGMDate is null then 'AGMDate' else '' end as MissField01,
'','','',''
from evi_Company_Meeting
WHERE '12'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTConsolidation, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '13'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Ratio is null then 'Ratio' else '' end as MissField04,
''
from evi_Consolidation
WHERE '13'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTCurrency_Redenomination, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '14'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
EffectiveDate,
case when EffectiveDate is null then 'EffectiveDate' else '' end as MissField01,
case when NewCurrency is null then 'NewCurrency' else '' end as MissField02,
case when NewParvalue is null then 'NewParvalue' else '' end as MissField03,
'',''
from evi_Currency_Redenomination
WHERE '14'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTDemerger, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '15'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
'',''
from evi_Demerger
WHERE '15'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTDistribution, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '17'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
'',''
from evi_Distribution
WHERE '17'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTDivestment, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '18'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
'',''
from evi_Divestment
WHERE '18'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTDividend, please wait..."
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '19'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
'','','',''
from evi_Dividend
WHERE '19'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTDividend_Reinvestment_Plan, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '20'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
'','','',''
from evi_Dividend_Reinvestment_Plan
WHERE '20'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTEntitlement, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '21'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Ratio is null then 'Ratio' else '' end as MissField04,
''
from evi_Entitlement
WHERE '21'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTFinancial_Year_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '22'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Cre' as Datetype,
Created,
'','','','',''
from evi_Financial_Year_Change
WHERE '22'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTFranking, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '23'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
'','','',''
from evi_Franking
WHERE '23'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTIncorporation_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '24'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
EffectiveDate,
case when EffectiveDate is null then 'EffectiveDate' else '' end as MissField01,
case when NewCountry is null then 'NewCountry' else '' end as MissField02,
'','',''
from evi_Incorporation_Change
WHERE '24'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go



print " Updating qsys.dbo.EVENTInternational_Code_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '25'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
Effectivedate,
case when Effectivedate is null then 'Effectivedate' else '' end as MissField01,
'','','',''
from evi_International_Code_Change
WHERE '25'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTIssuer_Name_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '26'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
NameChangeDate,
case when NameChangeDate is null then 'NameChangeDate' else '' end as MissField01,
case when IssNewName = '' then 'IssNewName' else '' end as MissField02,
'','',''
from evi_Issuer_Name_Change
WHERE '26'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTLiquidation, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '27'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Liq' as Datetype,
LiquidationDate,
case when LiquidationDate is null then 'LiquidationDate' else '' end as MissField01,
case when Rate is null then 'Rate' else '' end as MissField02,
case when Currency is null then 'Currency' else '' end as MissField03,
'',''
from evi_Liquidation
WHERE '27'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTListing_Status_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '28'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
EffectiveDate,
case when EffectiveDate is null then 'EffectiveDate' else '' end as MissField01,
'','','',''
from evi_Listing_Status_Change
WHERE '28'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTLocal_Code_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '29'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
Effectivedate,
case when Effectivedate is null then 'EffectiveDate' else '' end as MissField01,
'','','',''
from evi_Local_Code_Change
WHERE '29'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTLot_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '30'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
EffectiveDate,
case when EffectiveDate is null then 'EffectiveDate' else '' end as MissField01,
'','','',''
from evi_Lot_Change
WHERE '30'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTMerger, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '31'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Effectivedate is null then 'Effectivedate' else '' end as MissField04,
''
from evi_Merger
WHERE '31'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTNew_Listing, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '32'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Cre' as Datetype,
Created,
case when ListDate is null then 'ListDate' else '' end as MissField01,
'','','',''
from evi_New_Listing
WHERE '32'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTParvalue_Redenomination, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '33'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
EffectiveDate,
case when EffectiveDate is null then 'EffectiveDate' else '' end as MissField01,
case when NewParvalue is null then 'NewParvalue' else '' end as MissField02,
'','',''
from evi_Parvalue_Redenomination
WHERE '33'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTPreference_Conversion, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '34'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Cre' as Datetype,
Created,
'','','','',''
from evi_Preference_Conversion
WHERE '34'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTPreferential_Offer, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '36'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Ratio is null then 'Ratio' else '' end as MissField04,
''
from evi_Preferential_Offer
WHERE '36'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTPurchase_Offer, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '37'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
'','','',''
from evi_Purchase_Offer
WHERE '37'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTReturn_of_Capital, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '38'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Pay' as Datetype,
CSPYDate,
case when CSPYDate is null then 'CSPYDate' else '' end as MissField01,
case when Rate is null then 'Rate' else '' end as MissField02,
'','',''
from evi_Return_of_Capital
WHERE '38'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTRights, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '39'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Ratio is null then 'Ratio' else '' end as MissField04,
''
from evi_Rights
WHERE '39'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTSecurity_Description_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '40'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
DateofChange,
case when DateofChange is null then 'DateofChange' else '' end as MissField01,
case when NewName is null then 'NewName' else '' end as MissField02,
'','',''
from evi_Security_Description_Change
WHERE '40'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTSecurity_Reclassification, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '41'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
EffectiveDate,
case when EffectiveDate is null then 'EffectiveDate' else '' end as MissField01,
'','','',''
from evi_Security_Reclassification
WHERE '41'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTSecurity_Swap, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '42'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
'','','',''
from evi_Security_Swap
WHERE '42'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTSedol_Change, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '43'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Eff' as Datetype,
Effectivedate,
case when Effectivedate is null then 'EffectiveDate' else '' end as MissField01,
'','','',''
from evi_Sedol_Change
WHERE '43'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


print " Updating qsys.dbo.EVENTSubdivision, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '45'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Ex' as Datetype,
ExDate,
case when Exdate is null then 'Exdate' else '' end as MissField01,
case when Recdate is null then 'Recdate' else '' end as MissField02,
case when Paydate is null then 'Paydate' else '' end as MissField03,
case when Ratio is null then 'Ratio' else '' end as MissField04,
''
from evi_Subdivision
WHERE '45'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go

print " Updating qsys.dbo.EVENTTakeover, please wait..."
print ""
go
use wca2
INSERT INTO qsys.dbo.EVENT
select '46'+cast(caref as varchar(20)),
eventid, secid, exchgcd, levent, issid, created,
'Cre' as Datetype,
Created,
'','','','',''
from evi_Takeover
WHERE '46'+cast(caref as varchar(20)) 
Not IN (Select Distinct caref from qsys.dbo.EVENT)
and excountry = 'US'
and primaryex <> 'No'
go


