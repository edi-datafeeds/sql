
USE wca
GO
--drop PROCEDURE sp_serialseq
CREATE PROCEDURE sp_serialseq
AS

DECLARE MyCursor CURSOR LOCAL FORWARD_ONLY FOR
  SELECT eventcd, rdid, uniqueid FROM serialseq ORDER BY eventcd, rdid, uniqueid
 
OPEN MyCursor
DECLARE @Teventcd char(3)
DECLARE @Trdid int
DECLARE @Seventcd char(3)
DECLARE @Srdid int
DECLARE @Tuniqueid int
DECLARE @cnt int
SET @Seventcd = ''
SET @Srdid = 0
SET @cnt = 1
FETCH NEXT FROM MyCursor INTO @Teventcd, @Trdid, @Tuniqueid
WHILE @@FETCH_STATUS = 0
BEGIN  
   IF @Seventcd = @Teventcd and @Srdid = @Trdid
      BEGIN      
      SET @cnt = @cnt + 1
      UPDATE serialseq SET serialseq.seqnum= @cnt
      WHERE serialseq.eventcd=@Teventcd and serialseq.uniqueid = @Tuniqueid
      END
   ELSE
      BEGIN  
      SET @cnt = 1
      END
   SET @Srdid =  @Trdid
   SET @Seventcd =  @Teventcd
   
   FETCH NEXT FROM MyCursor INTO @Teventcd, @Trdid, @Tuniqueid
END
CLOSE MyCursor
DEALLOCATE MyCursor
