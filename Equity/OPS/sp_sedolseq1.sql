USE wca
GO
CREATE PROCEDURE sp_sedolseq1

AS

DECLARE MyCursor CURSOR LOCAL FORWARD_ONLY FOR
  SELECT secid,cntrycd,rcntrycd,curencd,seqnum FROM sedolseq1 ORDER BY secid,cntrycd,rcntrycd,curencd

OPEN MyCursor
DECLARE @Ssecid int
DECLARE @Scntrycd char(2)
DECLARE @Tsecid int
DECLARE @Tseqnum int
DECLARE @Tcntrycd char(2)
DECLARE @Trcntrycd char(2)
DECLARE @Tcurencd char(3)
DECLARE @cnt int
SET @cnt = 1
FETCH NEXT FROM MyCursor INTO @Tsecid, @Tcntrycd, @Trcntrycd, @Tcurencd, @Tseqnum
WHILE @@FETCH_STATUS = 0
BEGIN   
   IF @Ssecid = @Tsecid and @Scntrycd =@Tcntrycd
   BEGIN 
      SET @cnt = @cnt + 1
      UPDATE sedolseq1 SET sedolseq1.seqnum= @cnt
      WHERE sedolseq1.secid = @Tsecid and sedolseq1.cntrycd = @Tcntrycd 
            and sedolseq1.rcntrycd = @Trcntrycd
            and sedolseq1.curencd = @Tcurencd
   END
   ELSE
   BEGIN   
       SET @cnt = 1
   END
   SET @Ssecid =  @Tsecid
   SET  @Scntrycd = @Tcntrycd  
   FETCH NEXT FROM MyCursor INTO @Tsecid, @Tcntrycd, @Trcntrycd, @Tcurencd, @Tseqnum
END
CLOSE MyCursor
DEALLOCATE MyCursor