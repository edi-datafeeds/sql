--filepath=o:\Datafeed\Equity\Extracts\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
SELECT * 
FROM V54F_620_Dividend
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 
AND (excountry = 'GB')
and created > '2009/05/01' and created < '2009/06/01'
ORDER BY CaRef
