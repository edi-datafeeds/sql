--filepath=o:\Datafeed\Equity\620i_nosedol\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620i_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\temp\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n


--# 2
use wca
select * FROM v50f_620NS_Company_Meeting
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and v50f_620NS_Company_Meeting.agmegm<>'BHM'
ORDER BY CaRef

--# 3
use wca
select * FROM v53f_620NS_Call
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 4
use wca
select * FROM v50f_620NS_Liquidation
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 5
use wca
select * FROM v51f_620NS_Certificate_Exchange
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 6
use wca
select * FROM v51f_620NS_International_Code_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 7
use wca
select * FROM v51f_620NS_Conversion_Terms
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 9
use wca
select * FROM v51f_620NS_Redemption_Terms
where 1=2
ORDER BY CaRef

--# 10
use wca
select * FROM v51f_620NS_Security_Reclassification
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 11
use wca
select * FROM v52f_620NS_Lot_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 13
use wca
select * FROM v53f_620NS_Buy_Back
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 14
use wca
select * FROM v53f_620NS_Capital_Reduction
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 15
use wca
select * FROM v53f_620NS_Takeover
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 16
use wca
select * FROM v54f_620NS_Arrangement
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 17
use wca
select * FROM v54f_620NS_Bonus
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 19
use wca
select * FROM v54f_620NS_Consolidation
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 20
use wca
select * FROM v54f_620NS_Demerger
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 21
use wca
select * FROM v54f_620NS_Distribution
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 22
use wca
select * FROM v54f_620NS_Divestment
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 23
use wca
select * FROM v54f_620NS_Entitlement
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 24
use wca
select * FROM v54f_620NS_Merger
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 25
use wca
select * FROM v54f_620NS_Preferential_Offer
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 26
use wca
select * FROM v54f_620NS_Purchase_Offer
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 27
use wca
select * FROM v54f_620NS_Rights
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 28
use wca
select * FROM v54f_620NS_Security_Swap
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 29
use wca
select * FROM v54f_620NS_Subdivision
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
or eventid=448427
ORDER BY CaRef

--# 30
use wca
select * FROM v50f_620NS_Bankruptcy 
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 31
use wca
select * FROM v50f_620NS_Financial_Year_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 32
use wca
select * FROM v50f_620NS_Incorporation_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 33
use wca
select * FROM v50f_620NS_Issuer_Name_change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 34
use wca
select * FROM v50f_620NS_Class_Action
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 35
use wca
select * FROM v51f_620NS_Security_Description_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 36
use wca
select * FROM v52f_620NS_Assimilation
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 37
use wca
select * FROM v52f_620NS_Listing_Status_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 38
use wca
select * FROM v52f_620NS_Local_Code_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 39
use wca
select * FROM v52f_620NS_New_Listing
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 40
use wca
select * FROM v50f_620NS_Announcement
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 41
use wca
select * FROM v51f_620NS_Parvalue_Redenomination
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 42
use wca
select * FROM v51f_620NS_Currency_Redenomination
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 43
use wca
select * FROM v53f_620NS_Return_of_Capital
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 44
use wca
SELECT * FROM v54f_620NS_Dividend
WHERE
changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 45
use wca
select * FROM v54f_620NS_Dividend_Reinvestment_Plan
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 46
use wca
select * FROM v54f_620NS_Franking
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef

--# 47
use wca
select * FROM v51f_620NS_Conversion_Terms_Change
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
and (SecStatus = 'X') 
ORDER BY CaRef

--# 48
use wca
select * FROM v54f_620NS_Bonus_Rights
where changed>(select max(acttime)-0.02 from wca.dbo.tbl_Opslog)
ORDER BY CaRef
