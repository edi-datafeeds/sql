--filepath=c:\Datafeed\Equity\620i_CarryQuote\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620i_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\Equity\620i_CarryQuote\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n


--# 5
use wca2
SELECT *
FROM t620i_International_Code_Change
ORDER BY eventid desc

--# 31
use wca2
SELECT *
FROM t620i_Issuer_Name_change
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620i_Security_Description_Change
ORDER BY CaRef

--# 35
use wca2
SELECT * FROM t620i_Listing_Status_Change
ORDER BY eventid desc

--# 36
use wca2
SELECT *
FROM t620i_Local_Code_Change
ORDER BY CaRef

--# 37
use wca2
SELECT * FROM t620i_New_Listing
ORDER BY eventid desc
