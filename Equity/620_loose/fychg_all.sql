--filepath=o:\Datafeed\Equity\620_loose\
--filenameprefix=2010_
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_FYCHG
--fileheadertext=EDI_EQUITY_620_FYCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_loose\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca
SELECT *
FROM v50f_620_Financial_Year_Change
WHERE
CHANGED > '2010/01/01' and CHANGED<'2011/01/01'
ORDER BY EventID desc, ExchgCD, Sedol
