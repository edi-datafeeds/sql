--filepath=o:\Datafeed\Equity\620_loose\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(Feeddate),112) from wca.dbo.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_INCHG
--fileheadertext=EDI_EQUITY_620_INCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620_loose\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1
use wca2
SELECT *
FROM t620_Incorporation_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
ORDER BY CaRef
