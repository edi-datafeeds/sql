--filepath=o:\Datafeed\Equity\620i\SCXi\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620i_SCXi_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\Equity\620\SCX\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n


--# 1
use wca
select * FROM v54f_SCX_Dividend
where changed>'2015/01/01'
ORDER BY CaRef


--# 2
use wca
select * FROM v50f_SCX_Company_Meeting
where changed>'2015/01/01'
and v50f_SCX_Company_Meeting.agmegm<>'BHM'
ORDER BY CaRef


--# 3
use wca
select * FROM v53f_SCX_Call
where changed>'2015/01/01'
ORDER BY CaRef


--# 4
use wca
select * FROM v50f_SCX_Liquidation
where changed>'2015/01/01'
ORDER BY CaRef


--# 5
use wca
select * FROM v51f_SCX_Certificate_Exchange
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 6
use wca
select * FROM v51f_SCX_International_Code_Change
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 7
use wca
select * FROM v51f_SCX_Conversion_Terms
where changed>'2015/01/01'
ORDER BY CaRef


--# 8
use wca
select * FROM v51f_SCX_Conversion_Terms_Change
where changed>'2015/01/01'
and (SecStatus = 'X') 
ORDER BY CaRef


--# 9
use wca
select * FROM v51f_SCX_Redemption_Terms
where changed>'2015/01/01'
ORDER BY CaRef


--# 10
use wca
select * FROM v51f_SCX_Security_Reclassification
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 13
use wca
select * FROM v53f_SCX_Buy_Back
where changed>'2015/01/01'
ORDER BY CaRef


--# 14
use wca
select * FROM v53f_SCX_Capital_Reduction
where changed>'2015/01/01'
ORDER BY CaRef


--# 15
use wca
select * FROM v53f_SCX_Takeover
where changed>'2015/01/01'
ORDER BY CaRef


--# 16
use wca
select * FROM v54f_SCX_Arrangement
where changed>'2015/01/01'
ORDER BY CaRef


--# 17
use wca
select * FROM v54f_SCX_Bonus
where changed>'2015/01/01'
ORDER BY CaRef


--# 18
use wca
select * FROM v54f_SCX_Bonus_Rights
where changed>'2015/01/01'
ORDER BY CaRef


--# 19
use wca
select * FROM v54f_SCX_Consolidation
where changed>'2015/01/01'
ORDER BY CaRef


--# 20
use wca
select * FROM v54f_SCX_Demerger
where changed>'2015/01/01'
ORDER BY CaRef


--# 21
use wca
select * FROM v54f_SCX_Distribution
where changed>'2015/01/01'
ORDER BY CaRef


--# 22
use wca
select * FROM v54f_SCX_Divestment
where changed>'2015/01/01'
ORDER BY CaRef


--# 23
use wca
select * FROM v54f_SCX_Entitlement
where changed>'2015/01/01'
ORDER BY CaRef


--# 24
use wca
select * FROM v54f_SCX_Merger
where changed>'2015/01/01'
ORDER BY CaRef


--# 25
use wca
select * FROM v54f_SCX_Preferential_Offer
where changed>'2015/01/01'
ORDER BY CaRef


--# 26
use wca
select * FROM v54f_SCX_Purchase_Offer
where changed>'2015/01/01'
ORDER BY CaRef


--# 27
use wca
select * FROM v54f_SCX_Rights
where changed>'2015/01/01'
ORDER BY CaRef


--# 28
use wca
select * FROM v54f_SCX_Security_Swap
where changed>'2015/01/01'
ORDER BY CaRef


--# 29
use wca
select * FROM v54f_SCX_Subdivision
where changed>'2015/01/01'
ORDER BY CaRef


--# 30
use wca
select * FROM v50f_SCX_Bankruptcy 
where changed>'2015/01/01'
ORDER BY CaRef


--# 31
use wca
select * FROM v50f_SCX_Financial_Year_Change
where changed>'2015/01/01'
ORDER BY CaRef


--# 32
use wca
select * FROM v50f_SCX_Incorporation_Change
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 33
use wca
select * FROM v50f_SCX_Issuer_Name_change
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 34
use wca
select * FROM v50f_SCX_Class_Action
where changed>'2015/01/01'
ORDER BY CaRef


--# 35
use wca
select * FROM v51f_SCX_Security_Description_Change
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 40
use wca
select * FROM v50f_SCX_Announcement
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef

--# 41
use wca
select * FROM v51f_SCX_Parvalue_Redenomination
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 42
use wca
select * FROM v51f_SCX_Currency_Redenomination
where changed>'2015/01/01'
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY CaRef


--# 43
use wca
select * FROM v53f_SCX_Return_of_Capital
where changed>'2015/01/01'
ORDER BY CaRef


