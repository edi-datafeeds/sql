print " Generating UDRNEW_DAILY, please wait..."
go
use udr
if exists (select * from sysobjects where name = 'UDRNEW')
	drop table UDRNEW

use udr
if exists (select * from sysobjects where name = 'UDRNEW')
	drop table UDRNEW
use wca
SELECT
drSCMST.Uscode + 'A' as UscodeKey,
drSCMST.Uscode,
ISSUR.Issuername,
CASE WHEN (DPRCP.DRtype = 'ADR' OR  DPRCP.levdesc = '144A' ) AND drusSEDOL.Sedol IS not NULL THEN drusSMF.Longdesc
     WHEN drgbSEDOL.Sedol IS not NULL THEN drgbSMF.Longdesc 
     WHEN drSEDOL.Sedol IS not NULL THEN drSMF.Longdesc 
     ELSE drSCMST.SecurityDesc END as Descrip,
CASE WHEN (DPRCP.DRtype = 'ADR' OR  DPRCP.levdesc = '144A' ) AND drusSEDOL.Sedol IS not NULL THEN drusSedol.Sedol
     WHEN drgbSEDOL.Sedol IS not NULL THEN drgbSedol.Sedol 
     ELSE drSEDOL.Sedol END as DRSedol,
drSCMST.Isin drIsin,
CASE WHEN (DPRCP.DRtype = 'ADR' OR  DPRCP.levdesc = '144A' ) AND drusSEDOL.Sedol IS not NULL THEN drusSMF.Statusflag + drusSMF.Eventcode
     WHEN drgbSEDOL.Sedol IS not NULL THEN drgbSMF.Statusflag + drgbSMF.Eventcode
     ELSE drSMF.Statusflag + drSMF.Eventcode END as DrTradstat,
priSCEXH.Localcode as Symbol,
drSCMST.SharesOutstanding as drSos,
DPRCP.DRtype,
STUFF(STR(DPRCP.drRatio), 1, 3, '') 
  + ':' + LTRIM(STR(DPRCP.usRatio)) AS RATIO,
CASE WHEN unSEDOL.Sedol IS not NULL THEN unSMF.Longdesc 
     WHEN ungbSEDOL.Sedol IS not NULL THEN ungbSMF.Longdesc 
     ELSE unSCMST.SecurityDesc END as UnDescrip,
CASE WHEN unSEDOL.Sedol IS not NULL THEN unSEDOL.Sedol 
     ELSE ungbSEDOL.Sedol END as unSedol,
unSCMST.Isin as UnIsin, 
' ' as unSmflocalcode,
unSCMST.SharesOutstanding as unSos,
' ' as unSmfInfosource,
CASE WHEN DPRCP.Spnflag = 'T' THEN 'S' WHEN DPRCP.Spnflag = 'F' THEN 'U' ELSE '' END as Sponsored,
CASE WHEN (DPRCP.OtherDepbank IS not NULL and rtrim(DPRCP.OtherDepbank)<> '')
     THEN rtrim(DPRCP.Depbank)+', '+rtrim(DPRCP.OtherDepbank) 
     ELSE rtrim(DPRCP.Depbank) END as Depbank, 
drSCMST.PrimaryExchgCD as Exchange,
CASE WHEN drSCMST.Statusflag = 'I' THEN 'I' ELSE '' END as Statusflag,
indus.Indusname,
priSCEXH.Listdate as DateListed,
ISSUR.IssID,
DPRCP.Levdesc,
ISSUR.CntryofIncorp,
DPRCP.AnnounceDate as Credate,
DPRCP.drRatio,
DPRCP.usRatio,
CASE WHEN unSEDOL.Sedol IS not NULL THEN unSMF.Statusflag + unSMF.Eventcode
     ELSE ungbSMF.Statusflag + ungbSMF.Eventcode END as unTradstat,
CASE WHEN (unSCMST.PrimaryExchgCD IS not NULL and ltrim(unSCMST.PrimaryExchgCD)<>'')THEN unSCMST.PrimaryExchgCD 
     ELSE '' END as unExchange,
CASE WHEN (DPRCP.DRtype = 'ADR' OR  DPRCP.levdesc = '144A' ) AND drusSEDOL.Sedol IS not NULL THEN drusSMF.Isin
     WHEN drgbSEDOL.Sedol IS not NULL THEN drgbSMF.Isin
     ELSE drSMF.Isin END as SmfIsin,
unSCMST.Isin as UnSmfIsin, 
DPRCP.Secid,
DPRCP.unSecid,
DPRCP.Acttime as DPRCPActtime,
priSCEXH.Acttime as priSCEXHActtime,
ISSUR.Acttime as ISSURActtime,
drSCMST.Acttime as drSCMSTActtime,
unSCMST.Acttime as unSCMSTActtime,
'2000/01/01' as drusSEDOLActtime,
'2000/01/01' as drSEDOLActtime,
'2000/01/01'  as unSEDOLActtime,
'2000/01/01' as ungbSEDOLActtime,
DPRCP.Actflag,
DPRCP.DPRCPNotes as Additinfo
INTO udr.dbo.UDRNEW
FROM DPRCP
LEFT OUTER JOIN SCMST as drSCMST ON DPRCP.SecID = drSCMST.SecID
LEFT OUTER JOIN SCMST as unSCMST ON DPRCP.UnSecID = unSCMST.SecID
LEFT OUTER JOIN ISSUR ON unSCMST.IssID = ISSUR.IssID
LEFT OUTER JOIN INDUS ON ISSUR.IndusID = INDUS.IndusID
LEFT OUTER JOIN SEDOLSEQ1 as drusSEDOL ON drSCMST.SecID = drusSEDOL.SecID
                     and 'US' = drusSEDOL.CntryCD
                       and 1 = drusSEDOL.seqnum
LEFT OUTER JOIN SEDOLSEQ1 as drSEDOL ON drSCMST.SecID = drSEDOL.SecID
                     and substring(drSCMST.PrimaryExchgCD,1,2) = drSEDOL.CntryCD
                       and 1 = drSEDOL.seqnum
LEFT OUTER JOIN SEDOLSEQ1 as drgbSEDOL ON drSCMST.SecID = drgbSEDOL.SecID
                     and 'GB' = drgbSEDOL.CntryCD
                       and 1 = drgbSEDOL.seqnum
LEFT OUTER JOIN SEDOLSEQ1 as unSEDOL ON unSCMST.SecID = unSEDOL.SecID
                     and substring(unSCMST.PrimaryExchgCD,1,2) = unSEDOL.CntryCD
                       and 1 = unSEDOL.seqnum
LEFT OUTER JOIN SEDOLSEQ1 as ungbSEDOL ON unSCMST.SecID = ungbSEDOL.SecID
                     and 'GB' = ungbSEDOL.CntryCD
                       and 1 = ungbSEDOL.seqnum
LEFT OUTER JOIN smf4.dbo.Security as drusSMF ON drusSedol.Sedol = drusSMF.Sedol
LEFT OUTER JOIN smf4.dbo.Security as drSMF ON drSedol.Sedol = drSMF.Sedol
LEFT OUTER JOIN smf4.dbo.Security as drgbSMF ON drgbSedol.Sedol = drgbSMF.Sedol
LEFT OUTER JOIN smf4.dbo.Security as unSMF ON unSedol.Sedol = unSMF.Sedol
LEFT OUTER JOIN smf4.dbo.Security as ungbSMF ON ungbSedol.Sedol = ungbSMF.Sedol
LEFT OUTER JOIN SCEXH as priSCEXH ON drSCMST.SecID = priSCEXH.SecID
                     and drSCMST.PrimaryExchgCD = priSCEXH.ExchgCD
where 
drSCMST.uscode is not null
AND ISSUR.CNTRYOFINCORP<>'US'
and ltrim(drSCMST.uscode) <> ''
and DPRCP.DRtype <> 'CDR'
and DPRCP.DRtype <> 'EDR'
and DPRCP.DRtype <> 'NVD'
and DPRCP.DRtype <> 'RDR'
and DPRCP.Actflag <> 'D'
GO
USE UDR
ALTER TABLE UDRNEW ALTER COLUMN  uscodekey char(10) NOT NULL
GO 
USE UDR
ALTER TABLE [DBO].[udrnew] WITH NOCHECK ADD 
 CONSTRAINT [pk_udrnew_uscodekey] PRIMARY KEY ([uscodekey])  ON [PRIMARY]
GO 
USE UDR
CREATE  INDEX [ix_udrnew_unsedol] ON [dbo].[udrnew]([unsedol]) ON [PRIMARY] 
GO 
USE UDR
CREATE  INDEX [ix_udrnew_drsedol] ON [dbo].[udrnew]([drsedol]) ON [PRIMARY] 
GO