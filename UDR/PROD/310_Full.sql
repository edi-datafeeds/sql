--FileNamePrefix=AL
--FileName=YYMMDD
--FileExtension=.DR1
--FieldSeparator=	
--Archive=ON
--ArchivePath=n:\UDR\
--DataDateFormat=YYYYMMDD
--DataTimeFormat=
--ForceTime
--FilePath=o:\datafeed\UDR\310\
--filenameprefix=AL
--filename=YYMMDD
--filenamealt=
--fileextension=.310
--suffix=
--fileheadertext=EDI_UDR_310_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\310\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
use udr

SELECT
Issuername,
IssID,
CntryofIncorp,
udr.dbo.udrnew.Uscode,
UDRNEW.SecID as DrSecID,
UDRNEW.UnSecID,
DrIsin, 
UnIsin, 
DrSedol,
UnSedol,
Descrip as DrDescrip,
UnDescrip,
DrType,
LevDesc,
Exchange DrExchange,
UnExchange,
Depbank, 
substring(UnExchange,1,2) as ExCountry,
DrSos,
UnSos,
Ratio, 
Sponsored,
case when Statusflag ='' or Statusflag='A' then 'Active' else 'Inactive' end as SecStatus,
listdate as DrListdate
FROM UDRNEW
left outer join wca.dbo.scexh on udrnew.secid=wca.dbo.scexh.secid and exchange=wca.dbo.scexh.exchgcd
where 
(Statusflag<>'I' or drScmstActtime > getdate() - 1)
/*and DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'*/
order by udrnew.uscode
