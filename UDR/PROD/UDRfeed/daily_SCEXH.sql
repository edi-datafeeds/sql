--filepath=o:\Datafeed\UDR\UDRfeed\Refdata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_UDR_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\UDRfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
exchg.CntryCD,
exchg.MIC
FROM dprcp
inner join scexh on dprcp.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
SCEXH.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

union

SELECT
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
exchg.CntryCD,
exchg.MIC
FROM dprcp
inner join scexh on dprcp.unsecid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
SCEXH.acttime > (select max(feeddate)-1 from wca.dbo.tbl_Opslog where seq = 3)
