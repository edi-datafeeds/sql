--filepath=o:\Datafeed\UDR\UDRfeed\Refdata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_UDR_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\UDRfeed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp
FROM dprcp
inner join scmst on dprcp.secid = scmst.secid
INNER JOIN issur ON SCMST.IssID = SCMST.IssID
where 
ISSUR.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)

