--filepath=o:\Datafeed\UDR\UDRfeed\Refdata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_SCMST
--fileheadertext=EDI_UDR_SCMST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\UDRfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.Regs144a,
scmst.SharesOutstanding
FROM dprcp
inner join scmst on dprcp.secid = scmst.secid
WHERE scmst.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and scmst.secid is not null

union

SELECT
upper('SCMST') as Tablename,
scmst.Actflag,
scmst.Acttime,
scmst.Announcedate,
scmst.SecID,
scmst.IssID,
scmst.SectyCD,
scmst.SecurityDesc,
scmst.Statusflag,
scmst.StatusReason,
scmst.PrimaryExchgCD,
scmst.CurenCD,
scmst.ParValue,
scmst.IssuePrice,
scmst.PaidUpValue,
scmst.Voting,
scmst.USCode,
scmst.ISIN,
scmst.Regs144a,
scmst.SharesOutstanding
FROM dprcp
inner join scmst on dprcp.unsecid = wca.dbo.scmst.secid
WHERE scmst.acttime > (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
and scmst.secid is not null
order by sectycd, isin 