--filepath=o:\Datafeed\UDR\UDRfeed\Refdata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_RD
--fileheadertext=EDI_UDR_RD_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\No_Cull_Feeds\UDRfeed\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('RD') as Tablename,
RD.Actflag,
RD.Acttime,
RD.Announcedate,
RD.RdID,
RD.SecID,
RD.RecDate as Fromdate,
RD.ToDate,
RD.RegistrationDate,
RD.SectyCD,
RD.RecCalc,
RD.RDNotes as Notes
FROM rd
inner join DPRCP on RD.SecID = DPRCP.SecID
left outer join EXDT on RD.RdID = Exdt.RdID
where
exdt.rdid is null
and rd.sectycd='DR'
and RD.acttime > (select max(feeddate) from tbl_Opslog where seq = 3)
