--filepath=o:\Datafeed\UDR\UDRfeed_Sample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_ISSUR
--fileheadertext=EDI_UDR_ISSUR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT distinct
upper('ISSUR') as TableName,
ISSUR.Actflag,
ISSUR.AnnounceDate,
ISSUR.Acttime,
ISSUR.IssID,
ISSUR.IssuerName,
ISSUR.CntryofIncorp
FROM dprcp
inner join scmst on dprcp.secid = scmst.secid
INNER JOIN issur ON SCMST.IssID = issur.IssID
where 
dprcp.secid in (select secid from portfolio.dbo.drsecid)
or dprcp.unsecid in (select newunsecid from portfolio.dbo.drsecid)
