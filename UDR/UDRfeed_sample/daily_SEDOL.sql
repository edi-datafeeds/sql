--filepath=o:\Datafeed\UDR\UDRfeed_Sample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SEDOL
--fileheadertext=EDI_UDR_SEDOL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use wca
select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM dprcp
inner join sedol on dprcp.secid = sedol.secid
where 
dprcp.secid in (select secid from portfolio.dbo.drsecid)

union

select
upper('SEDOL') as TableName,
SEDOL.Actflag,
SEDOL.AnnounceDate,
SEDOL.Acttime, 
SEDOL.SedolId,
SEDOL.SecID,
SEDOL.CntryCD,
SEDOL.Sedol,
SEDOL.Defunct,
SEDOL.RcntryCD
FROM dprcp
inner join sedol on dprcp.unsecid = sedol.secid
where 
dprcp.unsecid in (select newunsecid from portfolio.dbo.drsecid)
