--filepath=o:\Datafeed\UDR\UDRfeed_Sample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_SCEXH
--fileheadertext=EDI_UDR_SCEXH_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 
use WCA
SELECT
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
exchg.CntryCD,
exchg.MIC
FROM dprcp
inner join scexh on dprcp.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
dprcp.unsecid in (select newunsecid from portfolio.dbo.drsecid)

union

SELECT
upper('SCEXH') as TableName,
SCEXH.Actflag,
SCEXH.AnnounceDate,
SCEXH.Acttime,
SCEXH.ScExhID,
SCEXH.SecID,
SCEXH.ExchgCD,
case when scexh.ListStatus='S' OR scexh.ListStatus ='R' OR scexh.ListStatus ='D' then scexh.ListStatus else 'L' end as ListStatus,
SCEXH.Lot,
SCEXH.MinTrdgQty,
SCEXH.ListDate,
SCEXH.LocalCode,
exchg.CntryCD,
exchg.MIC
FROM dprcp
inner join scexh on dprcp.unsecid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
where 
dprcp.secid in (select secid from portfolio.dbo.drsecid)
