--filepath=o:\Datafeed\UDR\UDRfeed_Sample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_DPRCP
--fileheadertext=EDI_UDR_DPRCP_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('DPRCP') as Tablename,
DPRCP.Actflag,
DPRCP.Acttime,
DPRCP.Announcedate,
DPRCP.SecID,
DPRCP.UnSecID,
DPRCP.DRRatio,
DPRCP.USRatio,
DPRCP.SpnFlag,
DPRCP.DPRCPNotes,
DPRCP.DRtype,
DPRCP.DepBank,
DPRCP.LevDesc,
DPRCP.OtherDepBank
FROM dprcp
WHERE 
dprcp.secid in (select secid from portfolio.dbo.drsecid)
or dprcp.unsecid in (select newunsecid from portfolio.dbo.drsecid)
