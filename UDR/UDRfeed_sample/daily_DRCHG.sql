--filepath=o:\Datafeed\UDR\UDRfeed_Sample\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.txt
--suffix=_DRCHG
--fileheadertext=EDI_UDR_DRCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT
upper('DRCHG') as Tablename,
DRCHG.Actflag,
DRCHG.Acttime,
DRCHG.Announcedate,
DRCHG.SecID,
DRCHG.EffectiveDate,
DRCHG.OldDRRatio,
DRCHG.NewDRRatio,
DRCHG.OldUNRatio,
DRCHG.NewUNRatio,
DRCHG.DRCHGID,
DRCHG.OldUNSecID,
DRCHG.NewUNSecID,
DRCHG.Eventtype,
DRCHG.OldDepbank,
DRCHG.NewDepbank,
DRCHG.OldDRtype,
DRCHG.NewDRtype,
DRCHG.OldLevel,
DRCHG.NewLevel,
DRCHG.DrchgNotes
FROM DRCHG
inner join dprcp on drchg.secid = dprcp.secid
WHERE
dprcp.secid in (select secid from portfolio.dbo.drsecid)
or dprcp.unsecid in (select newunsecid from portfolio.dbo.drsecid)
