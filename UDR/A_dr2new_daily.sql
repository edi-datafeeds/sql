--filepath=o:\datafeed\udr\DB\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.DR2
--suffix=
--fileheadertext=
--fileheaderdate=n
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=n
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
use udr

SELECT 
UDRNEWDIV.DRorUN,
UDRNEWDIV.id, 
DRSedol = CASE DRorUN WHEN 'DR' THEN UDRNEWDIV.drsedol ELSE '' END,
UNSedol = CASE DRorUN WHEN 'UN' THEN UDRNEWDIV.unsedol ELSE '' END,
UDRNEWDIV.Exdate, 
UDRNEWDIV.Paydate, 
UDRNEWDIV.Recdate, 
UDRNEWDIV.Currcode,
UDRNEWDIV.Payrate, 
substring(UDRNEWDIV.Approxflag,1,1) as Approxflag,
UDRNEWDIV.Taxflag, 
UDRNEWDIV.Grossrate,
UDRNEWDIV.Netrate, 
UDRNEWDIV.Taxrate, 
UDRNEWDIV.DivType,
UDRNEWDIV.Depfee, 
UDRNEWDIV.DivPeriod, 
UDRNEWDIV.Finalflag, 
wca.dbo.DIV.DivNotes as Notes,
UDRNEWDIV.Acttime, 
UDRNEWDIV.Creation
from UDRNEWDIV
left outer join wca.dbo.div on UDRNEWDIV.ID = wca.dbo.DIV.divid
LEFT OUTER JOIN DBnoGO ON udrnewdiv.Uscode = DBnoGO.uscode
and DBnoGO.uscode IS NULL
