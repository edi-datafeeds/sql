--filepath=o:\Datafeed\udr\DB\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.DR3
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyymmdd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\DB\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
use wca
select 
'10'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.AnnounceDate as Created,
'10' as eventtypenum,
v10s_BKRP.FilingDate as Enddate1,
v10s_BKRP.NotificationDate as Enddate2,
'' as Enddate3,
v10s_BKRP.BkrpNotes as Notes
FROM v10s_BKRP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_BKRP.IssID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_BKRP.Acttime >=getdate()-600 and v10s_BKRP.Actflag <> 'D'

--# 2
use WCA

select 
'20'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.AnnounceDate as Created,
'20' as eventtypenum,
v10s_INCHG.InChgDate as Enddate1,
'' as EndDate2,
'' as EndDate3,
'' as Notes
FROM v10s_INCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_INCHG.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_INCHG.Acttime >=getdate()-600 
and v10s_INCHG.Actflag <> 'D'


--# 3
use WCA
select 
'30'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
v10s_CLACT.Acttime as Changed,
v10s_CLACT.AnnounceDate as Created,
'30' as eventtypenum,
v10s_CLACT.EffectiveDate as Enddate1,
'' as EndDate2,
'' as EndDate3,
lawstNotes as Notes
FROM v10s_CLACT
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_CLACT.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_CLACT.Acttime >=getdate()-600
and v10s_CLACT.Actflag <> 'D'

--# 4

use wca
select 
'40'+rtrim(cast(v10s_LIQ.eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) 
THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as Changed,
v10s_LIQ.AnnounceDate as Created,
'40' as eventtypenum,
MPAY.Paydate as Enddate1,
'' as EndDate2,
'' as EndDate3,
'' as Notes
FROM v10s_LIQ
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_LIQ.Acttime >=getdate()-600 
and v10s_LIQ.Actflag <> 'D'

--# 5
use WCA
select 
'50'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.AnnounceDate as Created,
'50' as eventtypenum,
v10s_LSTAT.EffectiveDate as Enddate1,
v10s_LSTAT.NotificationDate as Enddate2,
'' as Enddate3,
'' as Notes
FROM v10s_LSTAT
INNER JOIN v20c_EV_dSCEXH ON v10s_LSTAT.SecID = v20c_EV_dSCEXH.SecID AND v10s_LSTAT.ExchgCD = v20c_EV_dSCEXH.ExchgCD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LSTAT.SecID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_LSTAT.Acttime >=getdate()-600 
and v10s_LSTAT.Actflag <> 'D'

--# 6
use WCA
select
'60'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.AnnounceDate as Created,
'60' as eventtypenum,
v20c_EV_scexh.Listdate as Enddate1,
'' as EndDate2,
'' as EndDate3,
'' as Notes
FROM v10s_NLIST1
INNER JOIN v20c_EV_SCEXH ON v10s_NLIST1.ScexhID = v20c_EV_SCEXH.ScexhID
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCEXH.SecID = v20c_EV_SCMST.SecID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_NLIST1.Acttime >=getdate()-600
and v10s_NLIST1.Actflag <> 'D' 


--# 8
use WCA
select
'80'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
v10s_CTX.Acttime as Changed,
v10s_CTX.AnnounceDate as Created,
'80' as eventtypenum,
v10s_CTX.EndDate as Enddate1,
v10s_CTX.StartDate as EndDate2,
'' as EndDate3,
ctxNotes as Notes
FROM v10s_CTX
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CTX.SecID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_CTX.ResSecID = SCMST.SecID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_CTX.Acttime >=getdate()-600
and v20c_EV_SCEXH.secid is not null and v20c_EV_SCEXH.exchgcd <> '' and v20c_EV_SCEXH.exchgcd is not null


--# 29
use WCA 
select 
'29'+rtrim(cast(v10s_BB.eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_BB.Acttime) THEN MPAY.Acttime ELSE v10s_BB.Acttime END as [Changed],
v10s_BB.AnnounceDate as Created,
'29' as eventtypenum,
v10s_BB.EndDate as Enddate1,
v10s_BB.StartDate as EndDate2,
MPAY.PayDate as EndDate3,
bbNotes as Notes
FROM v10s_BB
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_BB.SecID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_BB.EventID = MPAY.EventID AND v10s_BB.SEvent = MPAY.SEvent
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_BB.Acttime >=getdate()-600
or MPAY.Acttime >=getdate()-600)
and v10s_BB.Actflag <> 'D'


--# 12
use WCA
select 
'12'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN RD.Acttime > v10s_CAPRD.Acttime THEN RD.Acttime ELSE v10s_CAPRD.Acttime END as [Changed],
v10s_CAPRD.AnnounceDate as Created,
'12' as eventtypenum,
v10s_CAPRD.PayDate as Enddate1,
v10s_CAPRD.EffectiveDate as EndDate2,
RD.RecDate as EndDate3,
caprdNotes as Notes
FROM v10s_CAPRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CAPRD.SecID
LEFT OUTER JOIN RD ON v10s_CAPRD.RdID = RD.RdID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_CAPRD.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600)
and v10s_CAPRD.Actflag <> 'D'


--# 13
use WCA
select 
'13'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN RD.Acttime > v10s_RCAP.Acttime THEN RD.Acttime ELSE v10s_RCAP.Acttime END as [Changed],
v10s_RCAP.AnnounceDate as Created,
'13' as eventtypenum,
v10s_RCAP.CSPYDate as Enddate1,
v10s_RCAP.EffectiveDate as EndDate2,
RD.RecDate as EndDate3,
rcapNotes as Notes
FROM v10s_RCAP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_RCAP.SecID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_RCAP.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600)
and v10s_RCAP.Actflag <> 'D'

--# 14
use WCA
select
'14'+rtrim(cast(v10s_TKOVR.eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_TKOVR.Acttime) THEN MPAY.Acttime ELSE v10s_TKOVR.Acttime END as [Changed],
v10s_TKOVR.AnnounceDate as Created,
'14' as eventtypenum,
v10s_TKOVR.CmAcqDate as Enddate1,
v10s_TKOVR.UnconditionalDate as EndDate2,
v10s_TKOVR.CloseDate as EndDate3,
TKOVRNotes as Notes
FROM v10s_TKOVR
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_TKOVR.SecID
LEFT OUTER JOIN v20c_EV_dSCEXH ON v20c_EV_SCMST.SecID = v20c_EV_dSCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID AND v10s_TKOVR.SEvent = MPAY.SEvent
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_TKOVR.Acttime >=getdate()-600
or MPAY.Acttime >=getdate()-600)
and v10s_TKOVR.Actflag <> 'D'

--# 15
use WCA
select 
'15'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v10s_ARR.AnnounceDate as Created,
'15' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
'' as Notes
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_ARR.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_ARR.Actflag <> 'D'

--# 16
use WCA
select 
'16'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
v10s_BON.AnnounceDate as Created,
'16' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
bonNotes as Notes
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_BON.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_BON.Actflag <> 'D'

--# 17
use WCA
select 
'17'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BR.Acttime) THEN PEXDT.Acttime ELSE v10s_BR.Acttime END as [Changed],
v10s_BR.AnnounceDate as Created,
'17' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
brNotes as Notes
FROM v10s_BR
INNER JOIN RD ON RD.RdID = v10s_BR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_BR.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_BR.Actflag <> 'D'

--# 18
use WCA
select 
'18'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
v10s_CONSD.AnnounceDate as Created,
'18' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_CONSD.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_CONSD.Actflag <> 'D'

--# 19
use WCA
select 
'19'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
v10s_DMRGR.AnnounceDate as Created,
'19' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
dmrgrNotes as Notes
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_DMRGR.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_DMRGR.Actflag <> 'D'

--# 20
use WCA

select 
'70'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIST.Acttime) THEN PEXDT.Acttime ELSE v10s_DIST.Acttime END as [Changed],
v10s_DIST.AnnounceDate as Created,
'70' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
distNotes as Notes
FROM v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_DIST.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_DIST.Actflag <> 'D'

--# 21
use WCA
select
'21'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v10s_DVST.AnnounceDate as Created,
'21' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
DVSTNotes as Notes
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_DVST.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_DVST.Actflag <> 'D'

--# 22
use WCA
select 
'22'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
v10s_ENT.AnnounceDate as Created,
'22' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
entNotes as Notes
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_ENT.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_ENT.Actflag <> 'D' 

--# 23
use WCA
select 
'23'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
v10s_SD.AnnounceDate as Created,
'23' as eventtypenum,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as EndDate1,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate2,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate3,
sdNotes as Notes
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_SD.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or SDCHG.Acttime >=getdate()-600
or ICC.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_SD.Actflag <> 'D'

--# 24
use WCA
select 
'24'+rtrim(cast(v10s_MRGR.eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v10s_MRGR.AnnounceDate as Created,
'24' as eventtypenum,
v10s_MRGR.AppointedDate as Enddate1,
v10s_MRGR.EffectiveDate as EndDate2,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate3,
mrgrterms as Notes
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID AND v10s_MRGR.SEvent = MPAY.SEvent
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_MRGR.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_MRGR.Actflag <> 'D'

--# 25
use WCA
select 
'25'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
RD.Acttime Changed,
v10s_RTS.AnnounceDate as Created,
'25' as eventtypenum,
v10s_RTS.EndTrade as Enddate1,
v10s_RTS.StartTrade as EndDate2,
v10s_RTS.SplitDate as EndDate3,
rtsNotes as Notes
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and v10s_RTS.Acttime >=getdate()-600
and v10s_RTS.Actflag <> 'D'

--# 26
use WCA
select 
'26'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
v10s_PRF.AnnounceDate as Created,
'26' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
prfNotes as Notes
FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_PRF.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_PRF.Actflag <> 'D'

--# 27
use WCA
select 
'27'+rtrim(cast(eventid as varchar(10))) as ID,
case when dr.uscode is not null then dr.uscode else un.uscode end as druscode,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PO.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PO.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PO.Acttime) THEN PEXDT.Acttime ELSE v10s_PO.Acttime END as [Changed],
v10s_PO.AnnounceDate as Created,
'27' as eventtypenum,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as EndDate1,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as EndDate2,
RD.RecDate as Enddate3,
poNotes as Notes
FROM v10s_PO
INNER JOIN RD ON RD.RdID = v10s_PO.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PO' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PO' = PEXDT.EventType
left outer join udr.dbo.udrnew as dr on v20c_EV_SCMST.secid = dr.secid
left outer join udr.dbo.udrnew as un on v20c_EV_SCMST.secid = un.unsecid
where
(dr.secid is not null or un.unsecid is not null)
and (v10s_PO.Acttime >=getdate()-600
or RD.Acttime >=getdate()-600
or EXDT.Acttime >=getdate()-600
or PEXDT.Acttime >=getdate()-600)
and v10s_PO.Actflag <> 'D'
