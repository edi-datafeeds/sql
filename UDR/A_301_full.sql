/*
FILE FORMAT OF DATA FEED 301_FULL.SQL

Fieldname		Datatype(Maxwidth)	Notes
Uscode,			Char(9)
Sedol,  		Char(7)
Exchange,		varchar(6)		DR exchange
Issuername,		varchar(70)
Descrip,		varchar(70)
Isin,			Char(12)
DrTradstat,		Char(3)
SmfIsin,		Char(12)
Ratio,			varchar(41)		DR : UN ratio
UnDescrip,		varchar(70)
unSedol,		Char(7)
UnSmfIsin, 		Char(12)
UnTradstat,		Char(3)
unExchcode,		Char(10)
Sponsored,		Char(1)			S(ponsored), U(nsponsored), ''(don't know)
Depbank,		Varchar(30)
Cinccode,	 	Char(2)			ISO Country of Incorporation
IndSector,		varchar(250)
Acttime,		Date(10)		DR last modified date
DateListed,		Date(10)		
*/


--filepath=o:\datafeed\UDR\301dup\
--filenameprefix=
--filename=yyyymmdd
--FileHeaderDate=
--filenamealt=
--fileextension=.301
--suffix=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--fieldseparator=
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n





/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */


--# 1
use udr
SELECT 
Uscode,
drSedol as Sedol,
Exchange,
Issuername,
Descrip,
drIsin as Isin,
DrTradstat,
SmfIsin,
Ratio, 
UnDescrip,
unSedol,
UnSmfIsin, 
unTradstat,
unExchange as UNExchcode,
Sponsored,
Depbank, 
CntryofIncorp as Cinccode,
Indusname as Indsector,
drSCMSTActtime as Acttime,
DateListed
FROM UDRNEW
where (Statusflag<>'I')
and DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'
order by udrnew.uscode, 
case when drusSEDOLActtime is not null THEN drusSEDOLActtime ELSE drSEDOLActtime END desc,
case when unSEDOLActtime is not null THEN unSEDOLActtime ELSE ungbSEDOLActtime END

