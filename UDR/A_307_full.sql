/*
FILE FORMAT OF DATA FEED 307_FULL.SQL

Fieldname		Datatype(Maxwidth)	Notes
UscodeKey,		Char(10)		Uscode with A on the end (legacy)
Uscode,			Char(9)
Issuername,		varchar(70)
Descrip,		varchar(70)
DRSedol,		Char(7)
drIsin,			Char(12)
drTradstat,		Char(3)			LSE's DR trading status
Symbol,			varchar(20)		DR US ticker
Ratio,			varchar(41)		DR : UN ratio
UnDescrip,		varchar(70)
unSedol,		Char(7)
UnIsin, 		Char(12)
unTradstat,		Char(3)			LSE's UN trading status
old01			null			redundant
Sponsored,		Char(1)			S(ponsored), U(nsponsored), ''(don't know)
Depbank,		Varchar(30)
Exchange,		varchar(6)		DR exchange
Levdesc,		Varchar(10)
CntryofIncorp,		Char(2)			ISO Country of Incorporation
old02			null			redundant
old03 etc. ...
old31			null			redundant
Acttime,		Date(10)		DR last modified date
*/



--filepath=o:\datafeed\UDR\307dup\
--filenameprefix=AL
--filename=yymmdd
--filenamealt=
--fileextension=.307
--suffix=
--fileheadertext=EDI_UDR_307_
--fileheaderdate=yyyymmdd
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--sevent=n
--shownulls=n




--# 1
use udr

SELECT 
UscodeKey,
udrnew.Uscode,
Issuername,
Descrip,
DRSedol,
drIsin,
DrTradstat,
Symbol,
Ratio, 
UnDescrip,
unSedol,
UnIsin, 
unTradstat,
'' as old01,
Sponsored,
Depbank, 
Exchange,
CntryofIncorp,
'' as old02,
'' as old03,
'' as old04,
'' as old05,
'' as old06,
'' as old07,
'' as old08,
'' as old09,
'' as old10,
'' as old11,
'' as old12,
'' as old13,
'' as old14,
'' as old15,
'' as old16,
'' as old17,
'' as old18,
'' as old19,
'' as old20,
'' as old21,
'' as old22,
'' as old23,
'' as old24,
'' as old25,
'' as old26,
'' as old27,
'' as old28,
'' as old29,
'' as old30,
'' as old31,
drSCMSTActtime as Acttime
FROM UDRNEW
left outer join factsetnogo on udrnew.uscode = factsetnogo.uscode
where 
factsetnogo.uscode is null
and DRtype <> 'CDR'
and DRtype <> 'EDR'
and DRtype <> 'NVD'
and DRtype <> 'RDR'
order by udrnew.uscode, 
case when drusSEDOLActtime is not null THEN drusSEDOLActtime ELSE drSEDOLActtime END desc,
case when unSEDOLActtime is not null THEN unSEDOLActtime ELSE ungbSEDOLActtime END


