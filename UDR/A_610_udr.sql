--filepath=o:\datafeed\UDR\610_udr\
--filenameprefix=
--filename=YYYYMMDD
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 2
--fileextension=.610
--suffix=
--fileheadertext=EDI_UDR_REORG_
--fileheaderdate=YYYYMMDD
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\UDR\610_udr\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n


--# 1
USE WCA
SELECT * 
FROM v50f_610_Company_Meeting 
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 2
USE WCA
SELECT *
FROM v53f_610_Call
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 3
USE WCA
SELECT * 
FROM v50f_610_Liquidation
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA
SELECT *
FROM v51f_610_Certificate_Exchange
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 5
USE WCA
SELECT * 
FROM v51f_610_International_Code_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 6
USE WCA
SELECT * 
FROM v51f_610_Preference_Conversion
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 7
use wca
SELECT * 
FROM v51f_610_Preference_Redemption
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 8
use wca
SELECT * 
FROM v51f_610_Security_Reclassification
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 9
use wca
SELECT * 
FROM v52f_610_Lot_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 10
use wca
SELECT * 
FROM v52f_610_Sedol_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 11
use wca
SELECT * 
FROM v53f_610_Buy_Back
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
SELECT * 
FROM v53f_610_Capital_Reduction
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 14
use wca
SELECT * 
FROM v53f_610_Takeover
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 15
use wca
SELECT * 
FROM v54f_610_Arrangement
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 16
use wca
SELECT * 
FROM v54f_610_Bonus
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 17
use wca
SELECT * 
FROM v54f_610_Bonus_Rights
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 18
use wca
SELECT * 
FROM v54f_610_Consolidation
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 19
use wca
SELECT * 
FROM v54f_610_Demerger
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
SELECT * 
FROM v54f_610_Distribution
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 21
use wca
SELECT * 
FROM v54f_610_Divestment
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 22
use wca
SELECT * 
FROM v54f_610_Entitlement
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 23
use wca
SELECT * 
FROM v54f_610_Merger
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 24
use wca
SELECT * 
FROM v54f_610_Preferential_Offer
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 25
use wca
SELECT * 
FROM v54f_610_Purchase_Offer
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 26
use wca
SELECT * 
FROM v54f_610_Rights 
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 27
use wca
SELECT * 
FROM v54f_610_Security_Swap 
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 28
use wca
SELECT *
FROM v54f_610_Subdivision
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 29
use wca
SELECT *
FROM v50f_610_Bankruptcy 
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 30
use wca
SELECT *
FROM v50f_610_Financial_Year_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 31
use wca
SELECT *
FROM v50f_610_Incorporation_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 32
use wca
SELECT *
FROM v50f_610_Issuer_Name_change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 33
use wca
SELECT *
FROM v50f_610_Lawsuit
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 34
use wca
SELECT *
FROM v51f_610_Security_Description_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 35
use wca
SELECT *
FROM v52f_610_Assimilation
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3))
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 36
use wca2
SELECT *
FROM t610_Listing_Status_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3))
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 37
use wca
SELECT *
FROM v52f_610_Local_Code_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3))
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 38
use wca
SELECT * 
FROM v52f_610_New_Listing
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3))
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 39
use wca
SELECT * 
FROM v50f_610_Announcement
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 40
use wca
SELECT * 
FROM v51f_610_Parvalue_Redenomination 
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 41
USE WCA
SELECT * 
FROM v51f_610_Currency_Redenomination 
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 42
USE WCA
SELECT * 
FROM v53f_610_Return_of_Capital 
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 43
USE WCA
SELECT * 
FROM v54f_610_Dividend
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 44
USE WCA
SELECT * 
FROM v54f_610_Dividend_Reinvestment_Plan
WHERE
1=2

--# 45
USE WCA
SELECT * 
FROM v54f_610_Franking
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol

--# 46
USE WCA
SELECT * 
FROM v51f_600_Depositary_Receipt_Change
WHERE (CHANGED >= (select max(feeddate) from wca.dbo.tbl_opslog where seq=3)) AND (ListingStatus <> 'De_listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (sedol in (select drsedol from udr.dbo.udrnew)
or sedol in (select unsedol from udr.dbo.udrnew))
ORDER BY EventID, ExchgCD, Sedol
