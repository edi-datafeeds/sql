print " Generating UDRNEWDIV_DAILY, please wait..."
go
use udr
if exists (select * from sysobjects where name = 'UDRNEWDIV')
	drop table UDRNEWDIV
use wca
SELECT
'DR' as DRorUN,
v54f_610_Dividend.EventID as ID,
udr.dbo.UDRNEW.Uscode,
udr.dbo.UDRNEW.drSedol,
udr.dbo.UDRNEW.unSedol,
v54f_610_Dividend.Exdate,
v54f_610_Dividend.Paydate,
v54f_610_Dividend.Recdate,
v54f_610_Dividend.CurenCD as currcode,
v54f_610_Dividend.GrossDividend as Payrate,
CASE WHEN v54f_610_Dividend.Approxflag='T' THEN 'Yes' ELSE 'No' END as Approxflag,
'GROSS' as Taxflag,
v54f_610_Dividend.GrossDividend as Grossrate,
v54f_610_Dividend.NetDividend as Netrate,
v54f_610_Dividend.Taxrate,
CASE WHEN v54f_610_Dividend.Divtype='C' THEN 'CASH' WHEN v54f_610_Dividend.Divtype='S' THEN 'STOCK' WHEN v54f_610_Dividend.Divtype='B' THEN 'BOTH' ELSE '' END as Divtype,
v54f_610_Dividend.Depfees as depfee,
v54f_610_Dividend.DivPeriodCD as DivPeriod,
v54f_610_Dividend.DivPeriodCD as Finalflag,
v54f_610_Dividend.Changed as Acttime,
v54f_610_Dividend.Created as Creation,
udr.dbo.UDRNEW.Issuername as Issuer
into udr.dbo.UDRNEWDIV
from v54f_610_Dividend
inner join udr.dbo.udrnew on v54f_610_Dividend.secid = udr.dbo.udrnew.secid
                 and udr.dbo.udrnew.secid is not null
where v54f_610_Dividend.changed between getdate()-600 and getdate()
and (v54f_610_Dividend.exchgcd = udr.dbo.udrnew.exchange or udr.dbo.udrnew.exchange='')
and v54f_610_Dividend.Divtype = 'C'

union

SELECT
'UN' as DRorUN,
v54f_610_Dividend.EventID as ID,
udr.dbo.UDRNEW.Uscode,
udr.dbo.UDRNEW.drSedol,
udr.dbo.UDRNEW.unSedol,
v54f_610_Dividend.Exdate,
v54f_610_Dividend.Paydate,
v54f_610_Dividend.Recdate,
v54f_610_Dividend.CurenCD as currcode,
v54f_610_Dividend.GrossDividend as Payrate,
CASE WHEN v54f_610_Dividend.Approxflag='T' THEN 'Yes' ELSE 'No' END as YNDesc,
'GROSS' as Taxflag,
v54f_610_Dividend.GrossDividend as Grossrate,
v54f_610_Dividend.NetDividend as Netrate,
v54f_610_Dividend.Taxrate,
CASE WHEN v54f_610_Dividend.Divtype='C' THEN 'CASH' WHEN v54f_610_Dividend.Divtype='S' THEN 'STOCK' WHEN v54f_610_Dividend.Divtype='B' THEN 'BOTH' ELSE '' END as Divtype,
v54f_610_Dividend.Depfees as depfee,
v54f_610_Dividend.DivPeriodCD as DivPeriod,
v54f_610_Dividend.DivPeriodCD as Finalflag,
v54f_610_Dividend.Changed as Acttime,
v54f_610_Dividend.Created as Creation,
udr.dbo.UDRNEW.Issuername as Issuer
from v54f_610_Dividend
inner join udr.dbo.udrnew on v54f_610_Dividend.secid = udr.dbo.udrnew.unsecid
                 and udr.dbo.udrnew.unsecid is not null
where v54f_610_Dividend.changed between getdate()-600 and getdate()
and (v54f_610_Dividend.exchgcd = udr.dbo.udrnew.unexchange or udr.dbo.udrnew.unexchange='')
and v54f_610_Dividend.Divtype = 'C'
GO
