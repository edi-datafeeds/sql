--filepath=o:\Datafeed\udr\620i_otcmarkets\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620i_OTCMARKETS_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\udr\620i_otcmarkets\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n


--# 1
use wca2
SELECT * 
FROM t620i_Company_Meeting 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 2
use wca2
SELECT *
FROM t620i_Call
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 3
use wca2
SELECT * 
FROM t620i_Liquidation
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 4
use wca2
SELECT *
FROM t620i_Certificate_Exchange
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 5
use wca2
SELECT * 
FROM t620i_International_Code_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 6
use wca2
SELECT * 
FROM t620i_Conversion_Terms
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 7
use wca2
SELECT * 
FROM t620i_Redemption_Terms
where
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 8
use wca2
SELECT * 
FROM t620i_Security_Reclassification
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 9
use wca2
SELECT * 
FROM t620i_Lot_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 10
use wca2
SELECT * 
FROM t620i_Sedol_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 11
use wca2
SELECT * 
FROM t620i_Buy_Back
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 12
use wca2
SELECT * 
FROM t620i_Capital_Reduction
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 13
use wca2
SELECT * 
FROM t620i_Takeover
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 14
use wca2
SELECT * 
FROM t620i_Arrangement
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 15
use wca2
SELECT * 
FROM t620i_Bonus
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 17
use wca2
SELECT * 
FROM t620i_Consolidation
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 18
use wca2
SELECT * 
FROM t620i_Demerger
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 19
use wca2
SELECT * 
FROM t620i_Distribution
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 20
use wca2
SELECT * 
FROM t620i_Divestment
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 21
use wca2
SELECT * 
FROM t620i_Entitlement
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 22
use wca2
SELECT * 
FROM t620i_Merger
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 23
use wca2
SELECT * 
FROM t620i_Preferential_Offer
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 24
use wca2
SELECT * 
FROM t620i_Purchase_Offer
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 25
use wca2
SELECT * 
FROM t620i_Rights 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 26
use wca2
SELECT * 
FROM t620i_Security_Swap 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 27
use wca2
SELECT *
FROM t620i_Subdivision
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 28
use wca2
SELECT *
FROM t620i_Bankruptcy 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 29
use wca2
SELECT *
FROM t620i_Financial_Year_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 30
use wca2
SELECT *
FROM t620i_Incorporation_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 31
use wca2
SELECT *
FROM t620i_Issuer_Name_change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 32
use wca2
SELECT *
FROM t620i_Class_Action
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 33
use wca2
SELECT *
FROM t620i_Security_Description_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 34
use wca2
SELECT *
FROM t620i_Assimilation
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 35
use wca2
SELECT *
FROM t620i_Listing_Status_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 36
use wca2
SELECT *
FROM t620i_Local_Code_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 37
use wca2
SELECT * 
FROM t620i_New_Listing
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 38
use wca2
SELECT * 
FROM t620i_Announcement
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 39
use wca2
SELECT * 
FROM t620i_Parvalue_Redenomination 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 40
use wca2
SELECT * 
FROM t620i_Currency_Redenomination 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 41
use wca2
SELECT * 
FROM t620i_Return_of_Capital 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef


--# 42
use wca2
SELECT * 
FROM t620i_Dividend
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 43
use wca2
SELECT * 
FROM t620i_Dividend_Reinvestment_Plan
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 44
use wca2
SELECT * 
FROM t620i_Franking
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 45
use wca2
SELECT * 
FROM t620i_Conversion_Terms_Change
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus = 'X') 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef

--# 46
use wca2
SELECT * 
FROM t620i_Bonus_Rights 
WHERE
secid in (select unsecid from portfolio.dbo.otcmarkets)
and (SecStatus <> 'I' OR Secstatus IS NULL) 
and (primaryexchgcd=exchgcd or exchgcd is null)
ORDER BY CaRef
