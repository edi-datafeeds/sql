--filepath=at I:\GBDATA\CURRENCY_ELECTION_QUERIES\Reports\
--filenameprefix=D2_Rate_
--filename=yyyymmdd
--filenamealt=
--fileextension=.csv
--suffix=
--fileheadertext=EDI_D2_Rate_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=PLEASE_REPORT_ANY_ISSUES_WITH_THIS_FILE_TO_"ServiceRequests@exchange-data.com"
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\GBDATA\Drip\CurrencyElections\Reports\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N




--# 1

select distinct
xdes.detail.issuername as IssuerName,
xdes.detail.isin as Isin,
xdes.detail.securitydesc as SecurityDescription,
-- xdes.detail.sedol as sedol,
xdes.options.actflag as actflag,
xdes.detail.recdate as Closedate,
xdes.detail.exdate as Exdate,
xdes.detail.cashpaydate as Paydate,
xdes.detail.fxdt as Calculdate,
xdes.detail.curelecflag as ConversionStatus,
xdes.options.optelectiondate as Acceptdate,
xdes.options.grossdividend as CashDistCallRate,
REPLACE (xdes.detail.EDINotes, ',', ' ') as Comment1,
xdes.detail.CAType as DivType,
DATE_FORMAT( DATE_ADD(now(), INTERVAL(1-DAYOFWEEK(now())) +1 DAY),'%Y-%m-%d') as StartDate,
DATE_FORMAT( DATE_ADD(now(), INTERVAL(1-DAYOFWEEK(now())) +1 DAY),'%Y-%m-%d') + INTERVAL 18 DAY as EndDate
from xdes.detail
left outer join xdes.options on xdes.detail.detail_id = xdes.options.detail_id
where


(xdes.options.acttime >= DATE_FORMAT(DATE_ADD(now(), INTERVAL(1-DAYOFWEEK(now())) +1 DAY),'%Y-%m-%d') 
and xdes.options.acttime <= DATE_FORMAT(DATE_ADD(now(), INTERVAL(1-DAYOFWEEK(now())) +1 DAY),'%Y-%m-%d') + INTERVAL 18 DAY)


-- (xdes.options.acttime >= '2014-03-24' and xdes.options.acttime <= '2014-04-11')
and (xdes.detail.CAType = 'do'
or xdes.detail.CAType = 'd2')
and xdes.options.optiontype = 'c'
order by xdes.detail.issuername