

select distinct
xdes.detail. catype as Drips,
wca.sedol.sedol as sedol,
xdes.detail.detail_id as DistribID,
xdes.options.optionid as ElementID,
xdes.detail.issuername as Issuername,
xdes.detail.isin as Isin,
xdes.detail.securitydesc as SecurityDescription,
xdes.options.curencd as Currcode,
xdes.detail.exdate as exdate,
xdes.detail.recdate as Recordate,
xdes.detail.cashpaydate as Paydate,
xdes.detail.quantity as ComissionCost,
xdes.detail.maxprice as StampDuty,
-- smf4.dbo.edicadetail.Redemdate as ProvExpirationDate ---  dummy field
xdes.options.optelectiondate as ExpirationDate,
xdes.detail.sfpdate as StockDistributionDate,
xdes.detail.snpdate as CrestDate,
xdes.options.grossdividend as Payrate,
xdes.options.dripprice as ReInvestmentPrice,
xdes.options.taxrate as Taxflag,
-- smf4.dbo.edicaoption.DivType as Comment,  dummy fild
-- smf4.dbo.edicaoption.comment as Restrictions,    dummu field
xdes.detail.edinotes as Notes,
xdes.options.optiontype as DivType,
-- smf4.dbo.edicaoption.resflag as typeflag,   dummy field
xdes.options.actflag as actflag,
wca.agncy.RegistrarName as Regname,
wca.agncy.Add1 as RegAdd,
wca.agncy.Add2 as RegTown,
wca.agncy.Add3 as RegCounty,
wca.agncy.Add4 as RegPostcode,
wca.agncy.CntryCD as RegCountry,
wca.agncy.Tel1 as RegTel,
wca.agncy.Fax1 as RegFax,
wca.agncy.email1 as RegEmail,
wca.agncy.Website as RegWebsite
from xdes.detail

left outer join xdes.options on detail.detail_id = options.detail_id
LEFT OUTER JOIN wca.cntrg ON xdes.detail.secid = wca.cntrg.secid
LEFT OUTER JOIN wca.agncy ON wca.cntrg.regid = wca.agncy.agncyid
left outer join wca.sedol on xdes.detail.secid = wca.sedol.secid
-- left outer join xdes.detail on wca.sedol.secid = xdes.detail.secid

where
(xdes.detail.catype = 'dv' or xdes.options.optiontype = 'R')
and xdes.options.acttime > '2014-03-13'
and wca.sedol.cntrycd = 'gb'































--Second query, for others options

use smf4
select distinct smf4.dbo.edicadetail.catype as Drips,
smf4.dbo.security.sedol as Sedol,
smf4.dbo.edicadetail.cadid as DistribID,
smf4.dbo.edicaoption.cadoid as ElementID,
smf4.dbo.issuer.issuername as Issuername,
smf4.dbo.security.isin as Isin,
smf4.dbo.security.longdesc as SecurityDescription,
smf4.dbo.edicaoption.optioncurr as Currcode,
smf4.dbo.edicadetail.exdate as Exdate,
smf4.dbo.edicadetail.closedate as RecDate,
smf4.dbo.edicadetail.paydate as Paydate,
smf4.dbo.edicadetail.quantity as ComissionCost,
smf4.dbo.edicadetail.ostaxrate as StampDuty,
smf4.dbo.edicadetail.Redemdate as ProvExpirationDate,
smf4.dbo.edicadetail.acceptdate as ExpirationDate,
smf4.dboedicadetail.sfpdate as StockDistributionDate,
smf4.dbo.edicadetail.snpdate as CrestDate,
smf4.dbo.edicaoption.cashdistcallrate as Payrate,
smf4.dbo.edicaoption.payrate as ReInvestmentPrice,
smf4.dbo.edicaoption.netoftaxind as Taxflag,
smf4.dbo.edicaoption.DivType as Comment,
smf4.dbo.edicaoption.comment as Restrictions,
smf4.dbo.edicaoption.comment as Notes,
smf4.dbo.edicaoption.ratetype as DivType,
smf4.dbo.edicaoption.resflag as typeflag,
smf4.dbo.edicaoption.actflag as actflag,
uksec.dbo.registrar.regname as Regname,
uksec.dbo.registrar.regadd as RegAdd,
uksec.dbo.registrar.regtown as RegTown,
uksec.dbo.registrar.regcounty as RegCounty,
uksec.dbo.registrar.regpostcode as RegPostcode,
uksec.dbo.registrar.regcountry as RegCountry,
uksec.dbo.registrar.regtel as RegTel,
uksec.dbo.registrar.regfax as RegFax,
uksec.dbo.registrar.regemail as RegEmail,
uksec.dbo.registrar.regwebsite as RegWebsite

from smf4.dbo.edicadetail

left outer join smf4.dbo.edicaoption on smf4.dbo.edicadetail.securityid = smf4.dbo.edicaoption.securityid
				and smf4.dbo.edicadetail.cadid = smf4.dbo.edicaoption.cadid
left outer join smf4.dbo.security on smf4.dbo.edicadetail.securityid = smf4.dbo.security.securityid
left outer join smf4.dbo.issuer on smf4.dbo.security.issuerid = smf4.dbo.issuer.issuerid
left outer join uksec.dbo.issaddress on smf4.dbo.issuer.issuerid=uksec.dbo.issaddress.issuerid
left outer join uksec.dbo.registrar on uksec.dbo.issaddress.regid=uksec.dbo.registrar.regid


where (smf4.dbo.edicadetail.cadid = 'enter DIstribID/CADID'
or smf4.dbo.edicadetail.cadid = 'enter DIstribID/CADID'
or smf4.dbo.edicadetail.cadid = 'enter DIstribID/CADID')

and smf4.dbo.edicaoption.optiontype <> 'dv'















