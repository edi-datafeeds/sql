--filepath=I:\GBDATA\SV_QUERIES\Reports\
--filenameprefix=Scrips_
--filename=yyyymmdd
--filenamealt=
--fileextension=.csv
--suffix=
--fileheadertext=EDI_Scrips_Report_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=PLEASE_REPORT_ANY_ISSUES_WITH_THIS_FILE_TO_"ServiceRequests@exchange-data.com"
--fieldseparator=,	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\GBDATA\Drip\Scrips\Reports\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1


select
'ELECTION' as missing,
xdes.detail.issuername as CompanyName,
xdes.detail.securitydesc as Longdesc,
-- xdes.options.modifiedby as Username,
-- xdes.options.created as Credate,
xdes.detail.isin as seccodeofdist,
-- xdes.detail.Divperiodcd as SPL,
-- xdes.detail.acceptdate as Acceptdate,
xdes.detail.recdate as Closedate,
xdes.detail.exdate as Exdate,
xdes.detail.cashpaydate as Paydate,
-- xdes.detail.CAType as DivType,
xdes.options.rationew as Numerat,
xdes.options.ratioold as Denomin,
xdes.options.CurcdPr as PrimaryOptionCurr
-- xdes.detail.EDINotes as Comment1
from xdes.detail
left outer join xdes.options on xdes.detail.detail_id = xdes.options.detail_id
where
xdes.detail.catype = 'SV'
and xdes.options.optiontype = 's'
and xdes.options.optelectiondate is null
and xdes.detail.exdate < date_add(now(), interval +14 day)
and xdes.options.created>'2014-01-01'
and (xdes.detail.actflag <>'D' and xdes.options.actflag<>'D' )
and detail.exchgcd = 'gblse'


--# 2

select distinct
'PRICE' as Missing,
xdes.detail.issuername as CompanyName,
xdes.detail.securitydesc as Longdesc,
-- xdes.options.modifiedby as Username,
-- xdes.options.created as Credate,
xdes.detail.isin as seccodeofdist,
-- xdes.detail.Divperiodcd as SPL,
-- xdes.detail.acceptdate as Acceptdate,
xdes.detail.recdate as Closedate,
xdes.detail.exdate as Exdate,
xdes.detail.cashpaydate as Paydate,
-- xdes.detail.CAType as DivType,
xdes.options.rationew as Numerat,
xdes.options.ratioold as Denomin,
xdes.options.CurcdPr as PrimaryOptionCurr
-- xdes.detail.EDINotes as Comment1
from xdes.detail
left outer join xdes.options on xdes.detail.detail_id = xdes.options.detail_id
where
xdes.detail.catype = 'SV'
and xdes.options.optiontype = 's'
and xdes.options.dripprice is null
and xdes.detail.cashpaydate < date_add(now(), interval -8 day)
and xdes.options.created>'2014-01-01'
and (xdes.detail.actflag <>'D' and xdes.options.actflag<>'D' )
and detail.exchgcd = 'gblse'
  





