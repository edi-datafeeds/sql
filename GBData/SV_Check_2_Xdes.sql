--filepath=I:\GBDATA\SV_QUERIES\Reports\
--filenameprefix=Scrips_
--filename=yyyymmdd
--filenamealt=
--fileextension=.csv
--suffix=
--fileheadertext=EDI_Scrips_Report_
--fileheaderdate=yyyymmdd
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=PLEASE_REPORT_ANY_ISSUES_WITH_THIS_FILE_TO_"ServiceRequests@exchange-data.com"
--fieldseparator=,	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\GBDATA\Drip\Scrips\Reports\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1


select distinct
xdes.detail.issuername as CompanyName,
xdes.detail.isin as ISIN,
xdes.detail.acttime as Actdate,
xdes.detail.recdate as Closedate,
xdes.detail.exdate as Exdate,
xdes.detail.cashpaydate as Paydate,
xdes.detail.calculationdate as SVAnnouncedDate,
xdes.options.optelectiondate as ElectionDate
from xdes.detail

left outer join xdes.options on xdes.detail.detail_id = xdes.options.detail_id

where 
xdes.detail.Acttime > date_add(now(), interval -1 day)
and (xdes.detail.catype = 'SV' or xdes.options.optiontype = 'S')
and xdes.options.Acttime > date_add(now(), interval -1 day)


order by xdes.detail.Acttime
  



