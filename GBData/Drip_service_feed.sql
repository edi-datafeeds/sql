--filepath=I:\GBDATA\UK_Drips\Drip_Service\Drip\ 
--filenameprefix=Drip_Service_
--filename=yyyymmdd
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_Drip_Service_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_END_OF_FILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\GBDATA\Drip\DripService\Drip\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1

select distinct
xdes.detail. catype as Drips,
wca.sedol.sedol as sedol,
xdes.detail.detail_id as DistribID,
xdes.options.optionid as ElementID,
xdes.detail.issuername as Issuername,
xdes.detail.isin as Isin,
xdes.detail.securitydesc as SecurityDescription,
xdes.options.curencd as Currcode,
xdes.detail.exdate as exdate,
xdes.detail.recdate as Recordate,
xdes.detail.cashpaydate as Paydate,
xdes.detail.quantity as ComissionCost,
xdes.detail.maxprice as StampDuty,
-- smf4.dbo.edicadetail.Redemdate as ProvExpirationDate ---  dummy field
xdes.options.optelectiondate as ExpirationDate,
xdes.detail.sfpdate as StockDistributionDate,
xdes.detail.snpdate as CrestDate,
xdes.options.grossdividend as Payrate,
xdes.options.dripprice as ReInvestmentPrice,
xdes.options.taxrate as Taxflag,
-- smf4.dbo.edicaoption.DivType as Comment,  dummy fild
-- smf4.dbo.edicaoption.comment as Restrictions,    dummu field
xdes.detail.edinotes as Notes,
xdes.options.optiontype as DivType,
-- smf4.dbo.edicaoption.resflag as typeflag,   dummy field
xdes.options.actflag as actflag,
wca.agncy.RegistrarName as Regname,
wca.agncy.Add1 as RegAdd,
wca.agncy.Add2 as RegTown,
wca.agncy.Add3 as RegCounty,
wca.agncy.Add4 as RegPostcode,
wca.agncy.CntryCD as RegCountry,
wca.agncy.Tel1 as RegTel,
wca.agncy.Fax1 as RegFax,
wca.agncy.email1 as RegEmail,
wca.agncy.Website as RegWebsite
from xdes.detail

left outer join xdes.options on detail.detail_id = options.detail_id
LEFT OUTER JOIN wca.cntrg ON xdes.detail.secid = wca.cntrg.secid
LEFT OUTER JOIN wca.agncy ON wca.cntrg.regid = wca.agncy.agncyid
left outer join wca.sedol on xdes.detail.secid = wca.sedol.secid
-- left outer join xdes.detail on wca.sedol.secid = xdes.detail.secid

where
(xdes.detail.catype = 'dv' or xdes.options.optiontype = 'R')
and xdes.options.acttime > date_add(now(), interval -2 day)
and wca.sedol.cntrycd = 'gb'
and xdes.detail.maxprice is not null
and xdes.detail.detail_id not in (
select distinct detail_id from xdes.options
where dripprice is not null)