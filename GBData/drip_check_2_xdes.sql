--filepath=I:\GBDATA\UK_Drips\Drip_Service\Reports\ 
--filenameprefix=DRIPcheck_
--filename=yyyymmdd
--filenamealt=
--fileextension=.csv
--suffix=
--fileheadertext=EDI_Drip_Service_Reports_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=PLEASE_REPORT_ANY_ISSUES_WITH_THIS_FILE_TO_"ServiceRequests@exchange-data.com"
--fieldseparator=,
--outputstyle=
--archive=y
--archivepath=N:\No_Cull_Feeds\GBDATA\Drip\DripService\Reports\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 1



select
xdes.detail.issuername as CompanyName,
xdes.detail.isin as ISIN,
xdes.detail.sedol as SEDOL,
-- xdes.detail.securitydesc as LONGDESC,
xdes.detail.acttime as Actdate,
xdes.detail.divperiodcd,
-- xdes.detail.Divperiodcd as SPL,
-- xdes.detail.acceptdate as Acceptdate,
xdes.detail.recdate as Closedate,
xdes.detail.exdate as Exdate,
xdes.detail.cashpaydate as Paydate,
xdes.detail.snpdate as Snpdate,
xdes.detail.sfpdate as Sfpdate
from xdes.detail

left outer join xdes.options on xdes.detail.detail_id = xdes.options.detail_id

where xdes.detail.acttime > date_add(now(), interval -1 day)
and (xdes.detail.catype = 'dv'
or xdes.options.optiontype = 'R')
or (xdes.detail.divperiodcd = 'sp')
and xdes.options.acttime > date_add(now(), interval -1 day)
order by xdes.detail.Acttime



