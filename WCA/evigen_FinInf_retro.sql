print ""
go

print " Generating evi_Announcement, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Announcement')
 drop table evi_Announcement
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_ANN.EventID as char(8))),len(v10s_ANN.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_ANN.Levent,
EventID,
v10s_ANN.AnnounceDate as Created,
v10s_ANN.Acttime as Changed,
v10s_ANN.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_ANN.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ANN.EventType) > 0) THEN '[' + v10s_ANN.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ANN.NotificationDate,
'M' as Choice,
v10s_ANN.AnnNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Announcement
FROM v10s_ANN
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_Ann.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ANN.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_ANN.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_ANN.actflag<>'Z'
go

print ""
go

print " Generating evi_Announcement ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Announcement ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Announcement] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Announcement] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Announcement] ON [dbo].[evi_Announcement]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Announcement] ON [dbo].[evi_Announcement]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Bankruptcy, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Bankruptcy')
 drop table evi_Bankruptcy
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_BKRP.EventID as char(8))),len(v10s_BKRP.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_BKRP.Levent,
v10s_BKRP.EventID,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_BKRP.NotificationDate,
v10s_BKRP.FilingDate,
'M' as Choice,
v10s_BKRP.BkrpNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Bankruptcy
FROM v10s_BKRP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_BKRP.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_BKRP.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_BKRP.actflag<>'Z'
go


print ""
go

print " Generating evi_Bankruptcy ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Bankruptcy ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Bankruptcy] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Bankruptcy] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Bankruptcy] ON [dbo].[evi_Bankruptcy]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Bankruptcy] ON [dbo].[evi_Bankruptcy]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Company_Meeting, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Company_Meeting')
 drop table evi_Company_Meeting
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_AGM.EventID as char(8))),len(v10s_AGM.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_AGM.Levent,
v10s_AGM.EventID,
v10s_AGM.AnnounceDate as Created,
v10s_AGM.Acttime as Changed,
v10s_AGM.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_AGM.AGMDate,
v10s_AGM.AGMEGM,
v10s_AGM.AGMNO,
v10s_AGM.FYEDate as FinYearEndDate,
v10s_AGM.AGMTime,
v10s_AGM.Add1 as Address1,
v10s_AGM.Add2 as Address2,
v10s_AGM.Add3 as Address3,
v10s_AGM.Add4 as Address4,
v10s_AGM.Add5 as Address5,
v10s_AGM.Add6 as Address6,
v10s_AGM.City,
CASE WHEN (v10s_AGM.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_AGM.CntryCD) > 0) THEN '[' + v10s_AGM.CntryCD +'] not found' ELSE Cntry.Country END as Country,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Company_Meeting
FROM v10s_AGM
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_AGM.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN CNTRY ON v10s_AGM.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_AGM.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_AGM.actflag<>'Z'
go

print ""
go

print " Generating evi_Company_Meeting ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Company_Meeting ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Company_Meeting] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Company_Meeting] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Company_Meeting] ON [dbo].[evi_Company_Meeting]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Company_Meeting] ON [dbo].[evi_Company_Meeting]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Financial_Year_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Financial_Year_Change')
 drop table evi_Financial_Year_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_FYCHG.EventID as char(8))),len(v10s_FYCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_FYCHG.Levent,
v10s_FYCHG.EventID,
v10s_FYCHG.AnnounceDate as Created,
v10s_FYCHG.Acttime as Changed,
v10s_FYCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_FYCHG.NotificationDate,
v10s_FYCHG.OldFYStartDate as OldFinYearStart,
v10s_FYCHG.OldFYEndDate as OldFinYearEnd,
v10s_FYCHG.NewFYStartDate as NewFinYearStart,
v10s_FYCHG.NewFYEndDate as NewFinYearEnd,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Financial_Year_Change
FROM v10s_FYCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_FYCHG.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_FYCHG.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_FYCHG.actflag<>'Z'
go

print ""
go

print " Generating evi_Financial_Year_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Financial_Year_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Financial_Year_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Financial_Year_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Financial_Year_Change] ON [dbo].[evi_Financial_Year_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Financial_Year_Change] ON [dbo].[evi_Financial_Year_Change]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Incorporation_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Incorporation_Change')
 drop table evi_Incorporation_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_INCHG.EventID as char(8))),len(v10s_INCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_INCHG.Levent,
v10s_INCHG.EventID,
v10s_INCHG.AnnounceDate as Created,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_INCHG.InChgDate as EffectiveDate,
CASE WHEN (v10s_INCHG.OldCntryCD is null) THEN OldCntry.Country WHEN (OldCntry.Country is null) and (LEN(v10s_INCHG.OldCntryCD) > 0) THEN '[' + v10s_INCHG.OldCntryCD +'] not found' ELSE OldCntry.Country END as OldCountry,
CASE WHEN (v10s_INCHG.NewCntryCD is null) THEN NewCntry.Country WHEN (NewCntry.Country is null) and (LEN(v10s_INCHG.NewCntryCD) > 0) THEN '[' + v10s_INCHG.NewCntryCD +'] not found' ELSE NewCntry.Country END as NewCountry,
CASE WHEN (v10s_INCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_INCHG.EventType) > 0) THEN '[' + v10s_INCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Incorporation_Change
FROM v10s_INCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_INCHG.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_INCHG.EventType = EVENT.EventType
LEFT OUTER JOIN CNTRY as oldCNTRY ON v10s_INCHG.OldCntryCD = oldCNTRY.CntryCD
LEFT OUTER JOIN CNTRY as newCNTRY ON v10s_INCHG.NewCntryCD = newCNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_INCHG.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_INCHG.actflag<>'Z'
go

print ""
go

print " Generating evi_Incorporation_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Incorporation_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Incorporation_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Incorporation_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Incorporation_Change] ON [dbo].[evi_Incorporation_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Incorporation_Change] ON [dbo].[evi_Incorporation_Change]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Issuer_Name_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Issuer_Name_Change')
 drop table evi_Issuer_Name_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_ISCHG.EventID as char(8))),len(v10s_ISCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_ISCHG.Levent,
v10s_ISCHG.EventID,
v10s_ISCHG.AnnounceDate as Created,
v10s_ISCHG.Acttime as Changed,
v10s_ISCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_ISCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ISCHG.EventType) > 0) THEN '[' + v10s_ISCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ISCHG.NameChangeDate,
v10s_ISCHG.IssOldName,
v10s_ISCHG.IssNewName,
v10s_ISCHG.LegalName,
'M' as Choice,
v10s_ISCHG.IschgNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Issuer_Name_Change
FROM v10s_ISCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_ISCHG.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ISCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_ISCHG.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_ISCHG.actflag<>'Z'
go

print ""
go

print " Generating evi_Issuer_Name_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Issuer_Name_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Issuer_Name_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Issuer_Name_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Issuer_Name_Change] ON [dbo].[evi_Issuer_Name_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Issuer_Name_Change] ON [dbo].[evi_Issuer_Name_Change]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Class_Action, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Class_Action')
 drop table evi_Class_Action
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_CLACT.EventID as char(8))),len(v10s_CLACT.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_CLACT.Levent,
v10s_CLACT.EventID,
v10s_CLACT.AnnounceDate as Created,
v10s_CLACT.Acttime as Changed,
v10s_CLACT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_CLACT.EffectiveDate,
'M' as Choice,
v10s_CLACT.LawstNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Class_Action
FROM v10s_CLACT
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_CLACT.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_CLACT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_CLACT.actflag<>'Z'
go

print ""
go

print " Generating evi_Class_Action ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Class_Action ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Class_Action] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Class_Action] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Class_Action] ON [dbo].[evi_Class_Action]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Class_Action] ON [dbo].[evi_Class_Action]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Liquidation, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Liquidation')
 drop table evi_Liquidation
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+substring('00000'+rtrim(cast(v10s_LIQ.EventID as char(8))),len(v10s_LIQ.EventID),6)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+substring('00000'+rtrim(cast(v10s_LIQ.EventID as char(8))),len(v10s_LIQ.EventID),6)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint)
end as CAref,
v10s_LIQ.Levent,
v10s_LIQ.EventID,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as [Changed],
v10s_LIQ.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
MPAY.Paytype,
MPAY.ActFlag as ActionMPAY,
MPAY.Paydate as LiquidationDate,
'Price' AS RateType,
MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_LIQ.Liquidator,
v10s_LIQ.LiqAdd1 as Address1,
v10s_LIQ.LiqAdd2 as Address2,
v10s_LIQ.LiqAdd3 as Address3,
v10s_LIQ.LiqAdd4 as Address4,
v10s_LIQ.LiqAdd5 as Address5,
v10s_LIQ.LiqAdd6 as Address6,
v10s_LIQ.LiqCity as City,
CASE WHEN (v10s_LIQ.LiqCntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_LIQ.LiqCntryCD) > 0) THEN '[' + v10s_LIQ.LiqCntryCD +'] not found' ELSE Cntry.Country END as Country,
v10s_LIQ.LiqTel as Telephone,
v10s_LIQ.LiqFax as Fax,
v10s_LIQ.LiqEmail as Email,
'M' as Choice,
v10s_LIQ.LiquidationTerms as Terms,
'n/a' as Ratio,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Liquidation
FROM v10s_LIQ
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_LIQ.IssID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN CNTRY ON v10s_LIQ.LiqCntryCD = CNTRY.CntryCD
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_LIQ.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_LIQ.actflag<>'Z'
go

print ""
go

print " Generating evi_Liquidation ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Liquidation ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Liquidation] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Liquidation] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Liquidation] ON [dbo].[evi_Liquidation]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Liquidation] ON [dbo].[evi_Liquidation]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_International_Code_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_International_Code_Change')
 drop table evi_International_Code_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ICC.EventID as char(16)))as bigint) as CAref,
v10s_ICC.Levent,
v10s_ICC.EventID,
v10s_ICC.AnnounceDate as Created,
v10s_ICC.Acttime as Changed,
v10s_ICC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_ICC.EffectiveDate,
v10s_ICC.OldISIN,
v10s_ICC.NewISIN,
v10s_ICC.OldUSCode,
v10s_ICC.NewUSCode,
CASE WHEN (v10s_ICC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ICC.EventType) > 0) THEN '[' + v10s_ICC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_International_Code_Change
FROM v10s_ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_ICC.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ICC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_ICC.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_ICC.actflag<>'Z'
go

print ""
go

print " Generating evi_International_Code_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_International_Code_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_International_Code_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_International_Code_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_International_Code_Change] ON [dbo].[evi_International_Code_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_International_Code_Change] ON [dbo].[evi_International_Code_Change]([issid]) ON [PRIMARY]
GO


print ""
go

print " Generating evi_Security_Description_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Security_Description_Change')
 drop table evi_Security_Description_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SCCHG.EventID as char(16)))as bigint) as CAref,
v10s_SCCHG.Levent,
v10s_SCCHG.EventID,
v10s_SCCHG.AnnounceDate as Created,
v10s_SCCHG.Acttime as Changed,
v10s_SCCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_SCCHG.DateofChange,
CASE WHEN (v10s_SCCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SCCHG.EventType) > 0) THEN '[' + v10s_SCCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SCCHG.SecOldName as OldName,
v10s_SCCHG.SecNewName as NewName,
'M' as Choice,
v10s_SCCHG.ScChgNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Security_Description_Change
FROM v10s_SCCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_SCCHG.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_SCCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_SCCHG.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_SCCHG.actflag<>'Z'
go

print ""
go

print " Generating evi_Security_Description_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Security_Description_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Security_Description_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Security_Description_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Security_Description_Change] ON [dbo].[evi_Security_Description_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Security_Description_Change] ON [dbo].[evi_Security_Description_Change]([issid]) ON [PRIMARY]
GO
print ""
go

print " Generating evi_Listing_Status_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Listing_Status_Change')
 drop table evi_Listing_Status_Change
use wca
select 
cast(cast(v20c_EV_dSCEXH.Seqnum as char(1))+v20c_EV_dSCEXH.EXCHGID+rtrim(cast(v10s_LSTAT.EventID as char(16)))as bigint) as CAref,
v10s_LSTAT.Levent,
v10s_LSTAT.EventID,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_dSCEXH.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_dSCEXH.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_dSCEXH.ExchgCD,
v20c_EV_dSCEXH.MIC,
v20c_EV_dSCEXH.ExCountry,
v20c_EV_dSCEXH.RegCountry,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_dSCEXH.Listdate,
CASE WHEN (v10s_LSTAT.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LSTAT.EventType) > 0) THEN '[' + v10s_LSTAT.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LSTAT.LStatStatus as ListStatChangedTo,
v10s_LSTAT.NotificationDate,
v10s_LSTAT.EffectiveDate,
'M' as Choice,
v10s_LSTAT.Reason,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Listing_Status_Change
FROM v10s_LSTAT
INNER JOIN v20c_EV_dSCEXH ON v10s_LSTAT.SecID = v20c_EV_dSCEXH.SecID AND v10s_LSTAT.ExchgCD = v20c_EV_dSCEXH.ExchgCD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LSTAT.SecID
LEFT OUTER JOIN EVENT ON v10s_LSTAT.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_LSTAT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_LSTAT.actflag<>'Z'
go

print ""
go

print " Generating evi_Listing_Status_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Listing_Status_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Listing_Status_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Listing_Status_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Listing_Status_Change] ON [dbo].[evi_Listing_Status_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Listing_Status_Change] ON [dbo].[evi_Listing_Status_Change]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Local_Code_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Local_Code_Change')
 drop table evi_Local_Code_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_LCC.EventID as char(16)))as bigint) as CAref,
v10s_LCC.Levent,
v10s_LCC.EventID,
v10s_LCC.AnnounceDate as Created,
v10s_LCC.Acttime as Changed,
v10s_LCC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_LCC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LCC.EventType) > 0) THEN '[' + v10s_LCC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LCC.EffectiveDate,
v10s_LCC.OldLocalCode,
v10s_LCC.NewLocalCode,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Local_Code_Change
FROM v10s_LCC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LCC.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_LCC.ExchgCD = v20c_EV_SCEXH.ExchgCD
LEFT OUTER JOIN EVENT ON v10s_LCC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_LCC.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_LCC.actflag<>'Z'
go

print ""
go

print " Generating evi_Local_Code_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Local_Code_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Local_Code_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Local_Code_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Local_Code_Change] ON [dbo].[evi_Local_Code_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Local_Code_Change] ON [dbo].[evi_Local_Code_Change]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Lot_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Lot_Change')
 drop table evi_Lot_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_LTCHG.EventID as char(16)))as bigint) as CAref,
v10s_LTCHG.Levent,
v10s_LTCHG.EventID,
v10s_LTCHG.AnnounceDate as Created,
v10s_LTCHG.Acttime as Changed,
v10s_LTCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_LTCHG.EffectiveDate,
v10s_LTCHG.OldLot as OldLotSize,
v10s_LTCHG.NewLot as NewLotSize,
v10s_LTCHG.OldMinTrdQty as OldMinTradingQuant,
v10s_LTCHG.NewMinTrdgQty as NewMinTradingQuant,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Lot_Change
FROM v10s_LTCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LTCHG.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_LTCHG.ExchgCD = v20c_EV_SCEXH.ExchgCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_LTCHG.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_LTCHG.actflag<>'Z'
go

print ""
go

print " Generating evi_Lot_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Lot_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Lot_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Lot_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Lot_Change] ON [dbo].[evi_Lot_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Lot_Change] ON [dbo].[evi_Lot_Change]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_New_Listing, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_New_Listing')
 drop table evi_New_Listing
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_NLIST1.EventID as char(16)))as bigint) as CAref,
v10s_NLIST1.Levent,
v10s_NLIST1.EventID,
v10s_NLIST1.AnnounceDate as Created,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into WCA2.dbo.evi_New_Listing
FROM v10s_NLIST1
INNER JOIN v20c_EV_SCEXH ON v10s_NLIST1.ScexhID = v20c_EV_SCEXH.ScexhID
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCEXH.SecID = v20c_EV_SCMST.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_NLIST1.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_NLIST1.actflag<>'Z'
go

print ""
go

print " Generating evi_New_Listing ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_New_Listing ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_New_Listing] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_New_Listing] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_New_Listing] ON [dbo].[evi_New_Listing]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_New_Listing] ON [dbo].[evi_New_Listing]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Sedol_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Sedol_Change')
 drop table evi_Sedol_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SDCHG.EventID as char(16)))as bigint) as CAref,
v10s_SDCHG.Levent,
v10s_SDCHG.EventID,
v10s_SDCHG.AnnounceDate as Created,
v10s_SDCHG.Acttime as Changed,
v10s_SDCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_SDCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SDCHG.EventType) > 0) THEN '[' + v10s_SDCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SDCHG.CntryCD as CntryofSedol,
v10s_SDCHG.EffectiveDate,
v10s_SDCHG.OldSEDOL,
v10s_SDCHG.NewSEDOL,
v10s_SDCHG.CntryCD as OldCountry,
v10s_SDCHG.NewCntryCD as NewCountry,
v10s_SDCHG.RCntryCD as OldRegCountry,
v10s_SDCHG.NewRCntryCD as NewRegCountry,
'M' as Choice,
v10s_SDCHG.SdChgNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Sedol_Change
FROM v10s_SDCHG
INNER JOIN v20c_EV_SCMST ON v10s_SDCHG.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_SDCHG.CntryCD = v20c_EV_SCEXH.ExCountry
LEFT OUTER JOIN EVENT ON v10s_SDCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_SDCHG.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_SDCHG.actflag<>'Z'
go

print ""
go

print " Generating evi_Sedol_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Sedol_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Sedol_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Sedol_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Sedol_Change] ON [dbo].[evi_Sedol_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Sedol_Change] ON [dbo].[evi_Sedol_Change]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Certificate_Exchange, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Certificate_Exchange')
 drop table evi_Certificate_Exchange
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CTX.EventID as char(16)))as bigint) as CAref,
v10s_CTX.Levent,
v10s_CTX.EventID,
v10s_CTX.AnnounceDate as Created,
v10s_CTX.Acttime as Changed,
v10s_CTX.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_CTX.StartDate,
v10s_CTX.EndDate,
CASE WHEN (v10s_CTX.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_CTX.EventType) > 0) THEN '[' + v10s_CTX.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_CTX.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_CTX.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_CTX.SectyCD) > 0) THEN '[' + v10s_CTX.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
v10s_CTX.CtXNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Certificate_Exchange
FROM v10s_CTX
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CTX.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_CTX.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_CTX.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_CTX.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN EVENT ON v10s_CTX.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where 
ExCountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_CTX.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_CTX.actflag<>'Z'
go

print ""
go

print " Generating evi_Certificate_Exchange ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Certificate_Exchange ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Certificate_Exchange] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Certificate_Exchange] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Certificate_Exchange] ON [dbo].[evi_Certificate_Exchange]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Certificate_Exchange] ON [dbo].[evi_Certificate_Exchange]([issid]) ON [PRIMARY]
GO


print ""
go

print " Generating evi_Currency_Redenomination, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Currency_Redenomination')
 drop table evi_Currency_Redenomination
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CURRD.EventID as char(16)))as bigint) as CAref,
v10s_CURRD.Levent,
v10s_CURRD.EventID,
v10s_CURRD.AnnounceDate as Created,
v10s_CURRD.Acttime as Changed,
v10s_CURRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_CURRD.EffectiveDate,
CASE WHEN (v10s_CURRD.OldCurenCD is null) THEN oldCUREN.Currency WHEN (oldCUREN.Currency is null) and (LEN(v10s_CURRD.OldCurenCD) > 0) THEN '[' + v10s_CURRD.OldCurenCD +'] not found' ELSE oldCUREN.Currency END as OldCurrency,
CASE WHEN (v10s_CURRD.NewCurenCD is null) THEN newCUREN.Currency WHEN (newCUREN.Currency is null) and (LEN(v10s_CURRD.NewCurenCD) > 0) THEN '[' + v10s_CURRD.NewCurenCD +'] not found' ELSE newCUREN.Currency END as NewCurrency,
v10s_CURRD.OldParValue,
v10s_CURRD.NewParValue,
CASE WHEN (v10s_CURRD.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_CURRD.EventType) > 0) THEN '[' + v10s_CURRD.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v10s_CURRD.CurRdNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Currency_Redenomination
FROM v10s_CURRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CURRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_CURRD.EventType = EVENT.EventType
LEFT OUTER JOIN CUREN as oldCUREN ON v10s_CURRD.OldCurenCD = oldCUREN.CurenCD
LEFT OUTER JOIN CUREN as newCUREN ON v10s_CURRD.NewCurenCD = newCUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_CURRD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_CURRD.actflag<>'Z'
go

print ""
go

print " Generating evi_Currency_Redenomination ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Currency_Redenomination ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Currency_Redenomination] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Currency_Redenomination] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Currency_Redenomination] ON [dbo].[evi_Currency_Redenomination]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Currency_Redenomination] ON [dbo].[evi_Currency_Redenomination]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Parvalue_Redenomination, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Parvalue_Redenomination')
 drop table evi_Parvalue_Redenomination
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PVRD.EventID as char(16)))as bigint) as CAref,
v10s_PVRD.Levent,
v10s_PVRD.EventID,
v10s_PVRD.AnnounceDate as Created,
v10s_PVRD.Acttime as Changed,
v10s_PVRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_PVRD.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_PVRD.EventType) > 0) THEN '[' + v10s_PVRD.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_PVRD.EffectiveDate,
CASE WHEN (v10s_PVRD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PVRD.CurenCD) > 0) THEN '[' + v10s_PVRD.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PVRD.OldParValue,
v10s_PVRD.NewParValue,
'M' as Choice,
v10s_PVRD.PvRdNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Parvalue_Redenomination
FROM v10s_PVRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_PVRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_PVRD.EventType = EVENT.EventType
LEFT OUTER JOIN CUREN ON v10s_PVRD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where 
ExCountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_PVRD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_PVRD.actflag<>'Z'
go

print ""
go

print " Generating evi_Parvalue_Redenomination ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Parvalue_Redenomination ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Parvalue_Redenomination] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Parvalue_Redenomination] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Parvalue_Redenomination] ON [dbo].[evi_Parvalue_Redenomination]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Parvalue_Redenomination] ON [dbo].[evi_Parvalue_Redenomination]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Preference_Conversion, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Preference_Conversion')
 drop table evi_Preference_Conversion
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PRFCN.EventID as char(16)))as bigint) as CAref,
v10s_PRFCN.Levent,
v10s_PRFCN.EventID,
v10s_PRFCN.AnnounceDate as Created,
v10s_PRFCN.Acttime as Changed,
v10s_PRFCN.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_PRFCN.RatioNew +':'+v10s_PRFCN.RatioOld as Ratio,
'Price' AS RateType,
v10s_PRFCN.Price as Rate,
CASE WHEN (v10s_PRFCN.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRFCN.CurenCD) > 0) THEN '[' + v10s_PRFCN.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRFCN.FromDate,
v10s_PRFCN.ToDate,
v10s_PRFCN.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_PRFCN.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_PRFCN.SectyCD) > 0) THEN '[' + v10s_PRFCN.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
CASE WHEN v10s_PRFCN.MandOptFlag = 'M' THEN 'M' WHEN v10s_PRFCN.MandOptFlag = 'V' THEN 'V'ELSE '' END as Choice,
v10s_PRFCN.ConvtNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Preference_Conversion
FROM v10s_PRFCN
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_PRFCN.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_PRFCN.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_PRFCN.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_PRFCN.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN CUREN ON v10s_PRFCN.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where 
ExCountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_PRFCN.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_PRFCN.actflag<>'Z'
go

print ""
go

print " Generating evi_Preference_Conversion ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Preference_Conversion ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Preference_Conversion] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Preference_Conversion] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Preference_Conversion] ON [dbo].[evi_Preference_Conversion]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Preference_Conversion] ON [dbo].[evi_Preference_Conversion]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Preference_Redemption, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Preference_Redemption')
 drop table evi_Preference_Redemption
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PRFRD.EventID as char(16)))as bigint) as CAref,
v10s_PRFRD.Levent,
v10s_PRFRD.EventID,
v10s_PRFRD.AnnounceDate as Created,
v10s_PRFRD.Acttime as Changed,
v10s_PRFRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
'Price' AS RateType,
v10s_PRFRD.RedemptionPrice as Rate,
CASE WHEN (v10s_PRFRD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRFRD.CurenCD) > 0) THEN '[' + v10s_PRFRD.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRFRD.RedemptionDate,
CASE WHEN v10s_PRFRD.PartFinal='P' THEN 'Part' WHEN v10s_PRFRD.PartFinal='F' THEN 'Final' ELSE '' END as PartFinal,
CASE WHEN v10s_PRFRD.MandOptFlag = 'M' THEN 'M' WHEN v10s_PRFRD.MandOptFlag = 'V' THEN 'V'ELSE '' END as Choice,
v10s_PRFRD.RedmtNotes as Notes,
'n/a' as Ratio,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Preference_Redemption
FROM v10s_PRFRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_PRFRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN CUREN ON v10s_PRFRD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where 
ExCountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_PRFRD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_PRFRD.actflag<>'Z'
go


print ""
go

print " Generating evi_Preference_Redemption ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Preference_Redemption ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Preference_Redemption] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Preference_Redemption] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Preference_Redemption] ON [dbo].[evi_Preference_Redemption]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Preference_Redemption] ON [dbo].[evi_Preference_Redemption]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Security_Reclassification, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Security_Reclassification')
 drop table evi_Security_Reclassification
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SECRC.EventID as char(16)))as bigint) as CAref,
v10s_SECRC.Levent,
v10s_SECRC.EventID,
v10s_SECRC.AnnounceDate as Created,
v10s_SECRC.Acttime as Changed,
v10s_SECRC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_SECRC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SECRC.EventType) > 0) THEN '[' + v10s_SECRC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SECRC.EffectiveDate,
CASE WHEN (v10s_SECRC.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_SECRC.SectyCD) > 0) THEN '[' + v10s_SECRC.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_SECRC.RatioNew +':'+v10s_SECRC.RatioOld as Ratio,
v10s_SECRC.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'M' as Choice,
v10s_SECRC.SecRcNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Security_Reclassification
FROM v10s_SECRC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_SECRC.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_SECRC.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_SECRC.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_SECRC.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN EVENT ON v10s_SECRC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where 
ExCountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_SECRC.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_SECRC.actflag<>'Z'
go

print ""
go

print " Generating evi_Security_Reclassification ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Security_Reclassification ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Security_Reclassification] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Security_Reclassification] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Security_Reclassification] ON [dbo].[evi_Security_Reclassification]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Security_Reclassification] ON [dbo].[evi_Security_Reclassification]([issid]) ON [PRIMARY]
GO



print ""
go

print " Generating evi_Assimilation, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Assimilation')
 drop table evi_Assimilation
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ASSM.EventID as char(16)))as bigint) as CAref,
v10s_ASSM.Levent,
v10s_ASSM.EventID,
v10s_ASSM.AnnounceDate as Created,
v10s_ASSM.Acttime as Changed,
v10s_ASSM.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_ASSM.AssimilationDate,
CASE WHEN (v10s_ASSM.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_ASSM.SectyCD) > 0) THEN '[' + v10s_ASSM.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_ASSM.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Assimilation
FROM v10s_ASSM
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_ASSM.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_ASSM.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_ASSM.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_ASSM.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and v10s_ASSM.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
and v10s_ASSM.actflag<>'Z'
go

print ""
go

print " Generating evi_Assimilation ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Assimilation ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Assimilation] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Assimilation] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Assimilation] ON [dbo].[evi_Assimilation]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Assimilation] ON [dbo].[evi_Assimilation]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Buy_Back, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Buy_Back')
 drop table evi_Buy_Back
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BB.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BB.EventID as char(8))) as bigint)
end as CAref,
v10s_BB.Levent,
v10s_BB.EventID,
v10s_BB.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_BB.Acttime) and (MPAY.Acttime > RD.Acttime) THEN MPAY.Acttime WHEN RD.Acttime > v10s_BB.Acttime THEN RD.Acttime ELSE v10s_BB.Acttime END as [Changed],
v10s_BB.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
'MinMaxPrice' AS RateType,
MPAY.MinPrice +':'+MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
MPAY.MinQlyQty as MinQualifyingQuant,
MPAY.MaxQlyQty as MaxQualifyingQuant,
MPAY.PayDate,
MPAY.MinOfrQty as MinOfferQuant,
MPAY.MaxOfrqty as MaxOfferQuant,
MPAY.TndrStrkPrice as TenderStrikePrice,
MPAY.TndrStrkStep as TenderStrikeStep,
CASE WHEN (v10s_BB.OnOffFlag is null) THEN irOnOffMarket.Lookup WHEN (irOnOffMarket.Lookup is null) and (LEN(v10s_BB.OnOffFlag) > 0) THEN '[' + v10s_BB.OnOffFlag +'] not found' ELSE irOnOffMarket.Lookup END as OnOffMarket,
v10s_BB.StartDate,
v10s_BB.EndDate,
v10s_BB.MinAcpQty as MinAcceptanceQuant,
v10s_BB.MaxAcpQty as MAxAcceptanceQuant,
v10s_BB.BBMinPct as MinPercent,
v10s_BB.BBMaxPct as MaxPercent,
'V' as Choice,
RD.RDNotes,
v10s_BB.BBNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Buy_Back
FROM v10s_BB
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_BB.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_BB.RdID = RD.RdID
LEFT OUTER JOIN MPAY ON v10s_BB.EventID = MPAY.EventID AND v10s_BB.SEvent = MPAY.SEvent
LEFT OUTER JOIN irOnOffMarket ON v10s_BB.OnOffFlag = irOnOffMarket.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_BB.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or MPAY.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_BB.actflag<>'Z'
go

print ""
go

print " Generating evi_Buy_Back ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Buy_Back ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Buy_Back] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Buy_Back] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Buy_Back] ON [dbo].[evi_Buy_Back]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Buy_Back] ON [dbo].[evi_Buy_Back]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Call, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Call')
 drop table evi_Call
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CALL.EventID as char(16)))as bigint) as CAref,
v10s_CALL.Levent,
v10s_CALL.EventID,
v10s_CALL.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CALL.Acttime THEN RD.Acttime ELSE v10s_CALL.Acttime END as [Changed],
v10s_CALL.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
v10s_CALL.CallNumber,
CASE WHEN (v10s_CALL.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_CALL.CurenCD) > 0) THEN '[' + v10s_CALL.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_CALL.ToFaceValue,
v10s_CALL.ToPremium,
v10s_CALL.DueDate,
'V' as Choice,
RD.RDNotes,
v10s_CALL.CallNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Call
FROM v10s_CALL
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CALL.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_CALL.RdID = RD.RdID
LEFT OUTER JOIN CUREN ON v10s_CALL.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and ExCountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_CALL.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_CALL.actflag<>'Z'
go


print ""
go

print " Generating evi_Call ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Call ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Call] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Call] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Call] ON [dbo].[evi_Call]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Call] ON [dbo].[evi_Call]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Capital_Reduction, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Capital_Reduction')
 drop table evi_Capital_Reduction
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CAPRD.EventID as char(16)))as bigint) as CAref,
v10s_CAPRD.Levent,
v10s_CAPRD.EventID,
v10s_CAPRD.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_CAPRD.Acttime THEN RD.Acttime ELSE v10s_CAPRD.Acttime END as [Changed],
v10s_CAPRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN (v10s_CAPRD.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_CAPRD.Fractions) > 0) THEN '[' + v10s_CAPRD.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_CAPRD.EffectiveDate,
v10s_CAPRD.NewRatio +':'+v10s_CAPRD.OldRatio as Ratio,
v10s_CAPRD.PayDate,
v10s_CAPRD.OldParValue,
v10s_CAPRD.NewParValue,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
RD.RDNotes,
v10s_CAPRD.CapRdNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as RateType,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Capital_Reduction
FROM v10s_CAPRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CAPRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN irFRACTIONS ON v10s_CAPRD.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_CAPRD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_CAPRD.actflag<>'Z'
go

print ""
go

print " Generating evi_Capital_Reduction ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Capital_Reduction ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Capital_Reduction] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Capital_Reduction] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Capital_Reduction] ON [dbo].[evi_Capital_Reduction]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Capital_Reduction] ON [dbo].[evi_Capital_Reduction]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Return_of_Capital, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Return_of_Capital')
 drop table evi_Return_of_Capital
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_RCAP.EventID as char(16)))as bigint) as CAref,
v10s_RCAP.Levent,
v10s_RCAP.EventID,
v10s_RCAP.AnnounceDate as Created,
CASE WHEN RD.Acttime > v10s_RCAP.Acttime THEN RD.Acttime ELSE v10s_RCAP.Acttime END as [Changed],
v10s_RCAP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
'CashBack' AS RateType,
v10s_RCAP.CashBak as Rate,
CASE WHEN (v10s_RCAP.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_RCAP.CurenCD) > 0) THEN '[' + v10s_RCAP.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_RCAP.EffectiveDate,
v10s_RCAP.CSPYDate,
'M' as Choice,
RD.RDNotes,
v10s_RCAP.RCapNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratio
into WCA2.dbo.evi_Return_of_Capital
FROM v10s_RCAP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_RCAP.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
LEFT OUTER JOIN CUREN ON v10s_RCAP.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_RCAP.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_RCAP.actflag<>'Z'
go

print ""
go

print " Generating evi_Return_of_Capital ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Return_of_Capital ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Return_of_Capital] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Return_of_Capital] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Return_of_Capital] ON [dbo].[evi_Return_of_Capital]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Return_of_Capital] ON [dbo].[evi_Return_of_Capital]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Takeover, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Takeover')
 drop table evi_Takeover
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_dScexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_dScexh.EXCHGID+rtrim(cast(v10s_TKOVR.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_dScexh.Seqnum as char(1))+'11'+v20c_EV_dScexh.EXCHGID+rtrim(cast(v10s_TKOVR.EventID as char(8))) as bigint)
end as CAref,
v10s_TKOVR.Levent,
v10s_TKOVR.EventID,
v10s_TKOVR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_TKOVR.Acttime) and (MPAY.Acttime > RD.Acttime) THEN MPAY.Acttime WHEN RD.Acttime > v10s_TKOVR.Acttime THEN RD.Acttime ELSE v10s_TKOVR.Acttime END as [Changed],
v10s_TKOVR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_dScexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_dScexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_dScexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_dScexh.ExchgCD,
v20c_EV_dScexh.MIC,
v20c_EV_dScexh.ExCountry,
v20c_EV_dScexh.RegCountry,
v20c_EV_dScexh.Localcode,
v20c_EV_dScexh.ListStatus,
v20c_EV_dScexh.Listdate,
RD.RecDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
'MinPriceMaxPrice' AS RateType,
MPAY.MinPrice +':'+MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
MPAY.PayDate,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.MinQlyQty as MinQualifyingQuant,
MPAY.MaxQlyQty as MaxQualifyingQuant,
MPAY.MinOfrQty as MinOfferQuant,
MPAY.MaxOfrqty as MaxOfferQuant,
MPAY.TndrStrkPrice as TenderStrikePrice,
MPAY.TndrStrkStep as TenderStrikeStep,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
CASE WHEN (v10s_TKOVR.Hostile is null) THEN irHostile.Lookup WHEN (irHostile.Lookup is null) and (LEN(v10s_TKOVR.Hostile) > 0) THEN '[' + v10s_TKOVR.Hostile +'] not found' ELSE irHostile.Lookup END as TakeoverType,
CASE WHEN (v10s_TKOVR.TkovrStatus is null) THEN irTkovrStat.Lookup WHEN (irTkovrStat.Lookup is null) and (LEN(v10s_TKOVR.TkovrStatus) > 0) THEN '[' + v10s_TKOVR.TkovrStatus +'] not found' ELSE irTkovrStat.Lookup END as TakeoverStatus,
v10s_TKOVR.OfferorIssID,
v10s_TKOVR.OfferorName,
v10s_TKOVR.OpenDate,
v10s_TKOVR.CloseDate,
v10s_TKOVR.PreOfferQty,
v10s_TKOVR.PreOfferPercent,
v10s_TKOVR.TargetQuantity,
v10s_TKOVR.TargetPercent,
v10s_TKOVR.UnconditionalDate,
v10s_TKOVR.CmAcqDate as CompulsoryAcqDate,
v10s_TKOVR.MinAcpQty as MinAcceptanceQuant,
v10s_TKOVR.MaxAcpQty as MaxAcceptanceQuant,
CASE WHEN (v10s_TKOVR.CmAcqDate is null) THEN 'V' ELSE 'M' END as Choice,
RD.RDNotes,
v10s_TKOVR.TkovrNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Takeover
FROM v10s_TKOVR
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_TKOVR.SecID
left outer JOIN v20c_EV_dScexh ON v20c_EV_SCMST.SecID = v20c_EV_dScexh.SecID
LEFT OUTER JOIN RD ON v10s_TKOVR.RdID = RD.RdID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID AND v10s_TKOVR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_dScexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irHOSTILE ON v10s_TKOVR.Hostile = irHOSTILE.Code
LEFT OUTER JOIN irTKOVRSTAT ON v10s_TKOVR.TkovrStatus = irTKOVRSTAT.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_TKOVR.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or MPAY.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and (v10s_TKOVR.actflag<>'Z')

print ""
go

print " Generating evi_Takeover ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Takeover ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Takeover] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Takeover] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Takeover] ON [dbo].[evi_Takeover]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Takeover] ON [dbo].[evi_Takeover]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Arrangement, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Arrangement')
 drop table evi_Arrangement
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ARR.EventID as char(16)))as bigint) as CAref,
v10s_ARR.Levent,
v10s_ARR.EventID,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
v10s_ARR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
' ' as Choice,
RD.RDNotes,
v10s_ARR.ARRNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Arrangement
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_ARR.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_ARR.actflag<>'Z'
go

print ""
go

print " Generating evi_Arrangement ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Arrangement ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Arrangement] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Arrangement] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Arrangement] ON [dbo].[evi_Arrangement]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Arrangement] ON [dbo].[evi_Arrangement]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Bonus, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Bonus')
 drop table evi_Bonus
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BON.EventID as char(16)))as bigint) as CAref,
v10s_BON.Levent,
v10s_BON.EventID,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
v10s_BON.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_BON.RatioNew +':'+v10s_BON.RatioOld as Ratio,
CASE WHEN (v10s_BON.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_BON.Fractions) > 0) THEN '[' + v10s_BON.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_BON.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_BON.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_BON.SectyCD) > 0) THEN '[' + v10s_BON.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_BON.LapsedPremium,
'M' as Choice,
RD.RDNotes,
v10s_BON.BonNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Bonus
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_BON.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_BON.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_BON.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SECTY ON v10s_BON.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_BON.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_BON.actflag<>'Z'
go

print ""
go

print " Generating evi_Bonus ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Bonus ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Bonus] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Bonus] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Bonus] ON [dbo].[evi_Bonus]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Bonus] ON [dbo].[evi_Bonus]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Bonus_Rights, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Bonus_Rights')
 drop table evi_Bonus_Rights
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BR.EventID as char(16)))as bigint) as CAref,
v10s_BR.Levent,
v10s_BR.EventID,
v10s_BR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BR.Acttime) THEN PEXDT.Acttime ELSE v10s_BR.Acttime END as [Changed],
v10s_BR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_BR.RatioNew +':'+v10s_BR.RatioOld as Ratio,
'IssuePrice' AS RateType,
v10s_BR.IssuePrice as Rate,
CASE WHEN (v10s_BR.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_BR.CurenCD) > 0) THEN '[' + v10s_BR.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_BR.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_BR.SectyCD) > 0) THEN '[' + v10s_BR.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
CASE WHEN (v10s_BR.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_BR.Fractions) > 0) THEN '[' + v10s_BR.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_BR.StartSubscription,
v10s_BR.EndSubscription,
v10s_BR.SplitDate,
v10s_BR.StartTrade,
v10s_BR.EndTrade,
v10s_BR.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
v10s_BR.TraSecID as TradeableSecID,
v09b_SCMST.ISIN as TradeableIsin,
CASE WHEN v10s_BR.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
' ' as Choice,
RD.RDNotes,
v10s_BR.BRNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Bonus_Rights
FROM v10s_BR
INNER JOIN RD ON RD.RdID = v10s_BR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_BR.ResSecID = SCMST.SecID
LEFT OUTER JOIN v09b_SCMST ON v10s_BR.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_BR.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_BR.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SECTY ON v10s_BR.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN CUREN ON v10s_BR.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_BR.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_BR.actflag<>'Z'
go
print ""
go

print " Generating evi_Bonus_Rights ,please  wait ....."
GO 

use WCA2 
ALTER TABLE evi_Bonus_Rights ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Bonus_Rights] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Bonus_Rights] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Bonus_Rights] ON [dbo].[evi_Bonus_Rights]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Bonus_Rights] ON [dbo].[evi_Bonus_Rights]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Consolidation, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Consolidation')
 drop table evi_Consolidation
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CONSD.EventID as char(16)))as bigint) as CAref,
v10s_CONSD.Levent,
v10s_CONSD.EventID,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) THEN PEXDT.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
v10s_CONSD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_CONSD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_CONSD.CurenCD) > 0) THEN '[' + v10s_CONSD.CurenCD +'] not found' ELSE CUREN.Currency END as CONSDCurrency,
v10s_CONSD.OldParValue,
v10s_CONSD.NewParValue,
v10s_CONSD.NewRatio +':'+v10s_CONSD.OldRatio as Ratio,
CASE WHEN (v10s_CONSD.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_CONSD.Fractions) > 0) THEN '[' + v10s_CONSD.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
RD.RDNotes,
v10s_CONSD.ConsdNotes as Notes,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Consolidation
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN irFRACTIONS ON v10s_CONSD.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_CONSD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_CONSD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_CONSD.actflag<>'Z'
go

print ""
go

print " Generating evi_Consolidation ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Consolidation ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Consolidation] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Consolidation] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Consolidation] ON [dbo].[evi_Consolidation]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Consolidation] ON [dbo].[evi_Consolidation]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Demerger, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Demerger')
 drop table evi_Demerger
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DMRGR.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DMRGR.EventID as char(8))) as bigint)
end as CAref,
v10s_DMRGR.Levent,
v10s_DMRGR.EventID,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_DMRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime Or EXDT.Acttime is Null) and (MPAY.Acttime > PEXDT.Acttime Or PEXDT.Acttime is Null) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime Or EXDT.Acttime is Null) and (RD.Acttime > PEXDT.Acttime Or PEXDT.Acttime is Null) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime Or PEXDT.Acttime is Null) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
v10s_DMRGR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_DMRGR.EffectiveDate,
'M' as Choice,
RD.RDNotes,
v10s_DMRGR.DMrgrNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as RateType,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Demerger
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DMRGR.EventID = MPAY.EventID AND v10s_DMRGR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_DMRGR.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or MPAY.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_DMRGR.actflag<>'Z'
go

print ""
go

print " Generating evi_Demerger ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Demerger ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Demerger] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Demerger] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Demerger] ON [dbo].[evi_Demerger]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Demerger] ON [dbo].[evi_Demerger]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Distribution, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Distribution') drop table evi_Distribution
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIST.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIST.EventID as char(8))) as bigint)
end as CAref,
v10s_DIST.Levent,
v10s_DIST.EventID,
v10s_DIST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIST.Acttime) THEN PEXDT.Acttime ELSE v10s_DIST.Acttime END as [Changed],
v10s_DIST.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'M' as Choice,
RD.RDNotes,
v10s_DIST.DistNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Distribution
FROM v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DIST.EventID = MPAY.EventID AND v10s_DIST.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_DIST.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or MPAY.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_DIST.actflag<>'Z'
go

print ""
go

print " Generating evi_Distribution ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Distribution ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Distribution] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Distribution] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Distribution] ON [dbo].[evi_Distribution]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Distribution] ON [dbo].[evi_Distribution]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Divestment, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Divestment')
 drop table evi_Divestment
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DVST.EventID as char(16)))as bigint) as CAref,
v10s_DVST.Levent,
v10s_DVST.EventID,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
v10s_DVST.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_DVST.RatioNew +':'+v10s_DVST.RatioOld as Ratio,
'Min:MaxPrice' AS RateType,
v10s_DVST.MinPrice +':'+v10s_DVST.MaxPrice as Rate,
CASE WHEN (v10s_DVST.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_DVST.CurenCD) > 0) THEN '[' + v10s_DVST.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_DVST.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_DVST.Fractions) > 0) THEN '[' + v10s_DVST.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_DVST.StartSubscription,
v10s_DVST.EndSubscription,
v10s_DVST.TndrStrkPrice as TenderStrikePrice,
v10s_DVST.TndrPriceStep as TenderPriceStep,
v10s_DVST.MinQlyQty as MinQualifyingQuant,
v10s_DVST.MaxQlyQty as MaxQualifyingQuant,
v10s_DVST.MinAcpQty as MinAcceptanceQuant,
v10s_DVST.MaxAcpQty as MaxAcceptanceQuant,
v10s_DVST.TraSecID as TradeableSecID,
v09b_SCMST.Isin as TradeableIsin,
v10s_DVST.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_DVST.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_DVST.SectyCD) > 0) THEN '[' + v10s_DVST.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
RD.RDNotes,
v10s_DVST.DvstNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Divestment
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_DVST.ResSecID = SCMST.SecID
LEFT OUTER JOIN v09b_SCMST ON v10s_DVST.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_DVST.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_DVST.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON v10s_DVST.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_DVST.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_DVST.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_DVST.actflag<>'Z'
go

print ""
go

print " Generating evi_Divestment ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Divestment ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Divestment] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Divestment] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Divestment] ON [dbo].[evi_Divestment]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Divestment] ON [dbo].[evi_Divestment]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Entitlement, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Entitlement')
 drop table evi_Entitlement
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ENT.EventID as char(16)))as bigint) as CAref,
v10s_ENT.Levent,
v10s_ENT.EventID,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
v10s_ENT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_ENT.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_ENT.SectyCD) > 0) THEN '[' + v10s_ENT.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_ENT.RatioNew +':'+v10s_ENT.RatioOld as Ratio,
'IssuePrice' AS RateType,
v10s_ENT.EntIssuePrice as Rate,
CASE WHEN (v10s_ENT.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_ENT.CurenCD) > 0) THEN '[' + v10s_ENT.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_ENT.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_ENT.Fractions) > 0) THEN '[' + v10s_ENT.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_ENT.EntIssuePrice as IssuePrice,
v10s_ENT.StartSubscription,
v10s_ENT.EndSubscription,
v10s_ENT.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN v10s_ENT.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
'M' as Choice,
RD.RDNotes,
v10s_ENT.EntNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Entitlement
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_ENT.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_ENT.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_ENT.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_ENT.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_ENT.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_ENT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_ENT.actflag<>'Z'
go

print ""
go

print " Generating evi_Entitlement ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Entitlement ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Entitlement] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Entitlement] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Entitlement] ON [dbo].[evi_Entitlement]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Entitlement] ON [dbo].[evi_Entitlement]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Subdivision, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Subdivision')
 drop table evi_Subdivision
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SD.EventID as char(16)))as bigint) as CAref,
v10s_SD.Levent,
v10s_SD.EventID,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) THEN PEXDT.Acttime ELSE v10s_SD.Acttime END as [Changed],
v10s_SD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_SD.NewRatio +':'+v10s_SD.OldRatio as Ratio,
CASE WHEN (v10s_SD.OldCurenCD is null) THEN oldCUREN.Currency WHEN (oldCUREN.Currency is null) and (LEN(v10s_SD.OldCurenCD) > 0) THEN '[' + v10s_SD.OldCurenCD +'] not found' ELSE oldCUREN.Currency END as OldCurrency,
CASE WHEN (v10s_SD.NewCurenCD is null) THEN newCUREN.Currency WHEN (newCUREN.Currency is null) and (LEN(v10s_SD.NewCurenCD) > 0) THEN '[' + v10s_SD.NewCurenCD +'] not found' ELSE newCUREN.Currency END as NewCurrency,
v10s_SD.OldParValue,
v10s_SD.NewParValue,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
RD.RDNotes,
v10s_SD.SDNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Subdivision
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN CUREN as oldCUREN ON v10s_SD.OldCurenCD = oldCUREN.CurenCD
LEFT OUTER JOIN CUREN as newCUREN ON v10s_SD.NewCurenCD = newCUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_SD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_SD.actflag<>'Z'
go


print ""
go

print " Generating evi_Subdivision ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Subdivision ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Subdivision] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Subdivision] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Subdivision] ON [dbo].[evi_Subdivision]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Subdivision] ON [dbo].[evi_Subdivision]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Merger, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Merger')
 drop table evi_Merger
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_MRGR.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_MRGR.EventID as char(8))) as bigint)
end as CAref,
v10s_MRGR.Levent,
v10s_MRGR.EventID,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v10s_MRGR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.MinPrice,
v10s_MRGR.EffectiveDate,
v10s_MRGR.AppointedDate,
CASE WHEN (v10s_MRGR.MrgrStatus is null) THEN irMrgrStat.Lookup WHEN (irMrgrStat.Lookup is null) and (LEN(v10s_MRGR.MrgrStatus) > 0) THEN '[' + v10s_MRGR.MrgrStatus +'] not found' ELSE irMrgrStat.Lookup END as MergerStatus,
v10s_MRGR.Companies,
v10s_MRGR.ApprovalStatus,
'M' as Choice,
RD.RDNotes,
v10s_MRGR.MRGRTerms as Terms,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Merger
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID AND v10s_MRGR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irMRGRSTAT ON v10s_MRGR.MrgrStatus = irMRGRSTAT.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_MRGR.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or MPAY.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_MRGR.actflag<>'Z'
go

print ""
go

print " Generating evi_Merger ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Merger ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Merger] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Merger] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Merger] ON [dbo].[evi_Merger]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Merger] ON [dbo].[evi_Merger]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Rights, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Rights')
 drop table evi_Rights
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_RTS.EventID as char(16)))as bigint) as CAref,
v10s_RTS.Levent,
v10s_RTS.EventID,
v10s_RTS.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_RTS.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_RTS.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_RTS.Acttime) THEN PEXDT.Acttime ELSE v10s_RTS.Acttime END as [Changed],
v10s_RTS.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_RTS.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_RTS.SectyCD) > 0) THEN '[' + v10s_RTS.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_RTS.RatioNew +':'+v10s_RTS.RatioOld as Ratio,
'IssuePrice' as RateType,
v10s_RTS.IssuePrice as Rate,
CASE WHEN (v10s_RTS.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_RTS.CurenCD) > 0) THEN '[' + v10s_RTS.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_RTS.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_RTS.Fractions) > 0) THEN '[' + v10s_RTS.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_RTS.StartSubscription,
v10s_RTS.EndSubscription,
v10s_RTS.SplitDate,
v10s_RTS.StartTrade,
v10s_RTS.EndTrade,
v10s_RTS.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
v10s_RTS.TraSecID as TradeableSecID,
v09b_SCMST.ISIN as TradeableIsin,
v10s_RTS.LapsedPremium,
CASE WHEN v10s_RTS.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
'V' as Choice,
RD.RDNotes,
v10s_RTS.RTSNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Rights
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'RTS' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'RTS' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_RTS.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_RTS.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN v09b_SCMST ON v10s_RTS.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN irFRACTIONS ON v10s_RTS.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_RTS.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_RTS.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_RTS.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_RTS.actflag<>'Z'
go

print ""
go

print " Generating evi_Rights ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Rights ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Rights] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Rights] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Rights] ON [dbo].[evi_Rights]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Rights] ON [dbo].[evi_Rights]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Preferential_Offer, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Preferential_Offer')
 drop table evi_Preferential_Offer
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PRF.EventID as char(16)))as bigint) as CAref,
v10s_PRF.Levent,
v10s_PRF.EventID,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
v10s_PRF.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_PRF.OffereeIssID,
v10s_PRF.OffereeName,
CASE WHEN (v10s_PRF.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_PRF.SectyCD) > 0) THEN '[' + v10s_PRF.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
CASE WHEN (v10s_PRF.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_PRF.Fractions) > 0) THEN '[' + v10s_PRF.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_PRF.RatioNew +':'+v10s_PRF.RatioOld as Ratio,
'MinPrice:MaxPrice' AS RateType,
v10s_PRF.MinPrice +':'+v10s_PRF.MaxPrice as Rate,
CASE WHEN (v10s_PRF.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRF.CurenCD) > 0) THEN '[' + v10s_PRF.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRF.StartSubscription,
v10s_PRF.EndSubscription,
v10s_PRF.TndrStrkPrice,
v10s_PRF.TndrPriceStep,
v10s_PRF.MinQlyQty as MinQualifyingQuant,
v10s_PRF.MaxQlyQty as MaxQualifyingQuant,
v10s_PRF.MinAcpQty as MinAcceptanceQuant,
v10s_PRF.MaxAcpQty as MaxAcceptanceQuant,
v10s_PRF.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'V' as Choice,
RD.RDNotes,
v10s_PRF.PRFNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Preferential_Offer
FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_PRF.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_PRF.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_PRF.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_PRF.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_PRF.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_PRF.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_PRF.actflag<>'Z'
go

print ""
go

print " Generating evi_Preferential_Offer ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Preferential_Offer ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Preferential_Offer] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Preferential_Offer] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Preferential_Offer] ON [dbo].[evi_Preferential_Offer]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Preferential_Offer] ON [dbo].[evi_Preferential_Offer]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Purchase_Offer, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Purchase_Offer')
 drop table evi_Purchase_Offer
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PO.EventID as char(16)))as bigint) as CAref,
v10s_PO.Levent,
v10s_PO.EventID,
v10s_PO.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PO.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PO.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PO.Acttime) THEN PEXDT.Acttime ELSE v10s_PO.Acttime END as [Changed],
v10s_PO.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
'MinMaxPrice' AS RateType,
v10s_PO.MinPrice +':'+v10s_PO.MaxPrice as Rate,
CASE WHEN (v10s_PO.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PO.CurenCD) > 0) THEN '[' + v10s_PO.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PO.NegotiatedPrice,
v10s_PO.OfferOpens,
v10s_PO.OfferCloses,
v10s_PO.POMinPercent as MinPercent,
v10s_PO.POMaxPercent as MaxPercent,
v10s_PO.MinOfrQty as MinOfferQuant,
v10s_PO.MaxOfrqty as MaxOfferQuant,
v10s_PO.TndrStrkPrice as TenderStrikePrice,
v10s_PO.TndrPriceStep as TenderPriceStep,
v10s_PO.MinQlyQty as MinQualifyingQuant,
v10s_PO.MaxQlyQty as MaxQualifyingQuant,
v10s_PO.MinAcpQty as MinAcceptanceQuant,
v10s_PO.MaxAcpQty as MaxAcceptanceQuant,
'V' as Choice,
RD.RDNotes,
v10s_PO.PONotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratio
into WCA2.dbo.evi_Purchase_Offer
FROM v10s_PO
INNER JOIN RD ON RD.RdID = v10s_PO.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PO' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PO' = PEXDT.EventType
LEFT OUTER JOIN CUREN ON v10s_PO.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_PO.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_PO.actflag<>'Z'
go

print ""
go

print " Generating evi_Purchase_Offer ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Purchase_Offer ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Purchase_Offer] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Purchase_Offer] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Purchase_Offer] ON [dbo].[evi_Purchase_Offer]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Purchase_Offer] ON [dbo].[evi_Purchase_Offer]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Dividend_Reinvestment_Plan, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Dividend_Reinvestment_Plan')
 drop table evi_Dividend_Reinvestment_Plan
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DRIP.EventID as char(16)))as bigint) as CAref,
v10s_DRIP.Levent,
v10s_DRIP.EventID,
v10s_DRIP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DRIP.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DRIP.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DRIP.Acttime) THEN PEXDT.Acttime ELSE v10s_DRIP.Acttime END as [Changed],
v10s_DRIP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
DRIP.DripPayDate,
CASE WHEN v10s_DRIP.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_DRIP.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_DRIP.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_DRIP.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_DRIP.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_DRIP.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_DRIP.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_DRIP.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_DRIP.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_DRIP.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_DRIP.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_DRIP.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_DRIP.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_DRIP.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_DRIP.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_DRIP.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN (DRIP.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(DRIP.CntryCD) > 0) THEN '[' + DRIP.CntryCD +'] not found' ELSE Cntry.Country END as Country,
DRIP.DripLastdate,
DRIP.DripReinvPrice,
'V' as Choice,
v10s_DRIP.DIVNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Dividend_Reinvestment_Plan
FROM v10s_DRIP
INNER JOIN DRIP ON v10s_DRIP.EventID = DRIP.DivID
INNER JOIN RD ON RD.RdID = v10s_DRIP.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID and v20c_EV_SCEXH.ExCountry = DRIP.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN CNTRY ON DRIP.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_DRIP.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or DRIP.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_DRIP.actflag<>'Z'
go

print ""
go

print " Generating evi_Dividend_Reinvestment_Plan ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Dividend_Reinvestment_Plan ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Dividend_Reinvestment_Plan] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Dividend_Reinvestment_Plan] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Dividend_Reinvestment_Plan] ON [dbo].[evi_Dividend_Reinvestment_Plan]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Dividend_Reinvestment_Plan] ON [dbo].[evi_Dividend_Reinvestment_Plan]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Franking, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Franking')
 drop table evi_Franking
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_FRANK.EventID as char(16)))as bigint) as CAref,
v10s_FRANK.Levent,
v10s_FRANK.EventID,
v10s_FRANK.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_FRANK.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_FRANK.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_FRANK.Acttime) THEN PEXDT.Acttime ELSE v10s_FRANK.Acttime END as [Changed],
v10s_FRANK.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_FRANK.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_FRANK.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_FRANK.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_FRANK.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_FRANK.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_FRANK.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_FRANK.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_FRANK.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_FRANK.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_FRANK.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_FRANK.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_FRANK.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_FRANK.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_FRANK.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_FRANK.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_FRANK.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN (FRANK.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(FRANK.CntryCD) > 0) THEN '[' + FRANK.CntryCD +'] not found' ELSE Cntry.Country END as Country,
CASE WHEN FRANK.Frankflag = 'F' THEN 'Fully Franked'
     WHEN FRANK.Frankflag = 'P' THEN 'Partially Franked'
     WHEN FRANK.Frankflag = 'U' THEN 'Unfranked'
ELSE '' END as Franking,
FRANK.FrankDiv as AmountFranked,
FRANK.UnfrankDiv as AmountUnfranked,
'M' as Choice,
v10s_FRANK.DIVNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Franking
FROM v10s_FRANK
INNER JOIN FRANK ON v10s_FRANK.EventID = FRANK.DivID
INNER JOIN RD ON RD.RdID = v10s_FRANK.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID and v20c_EV_SCEXH.ExCountry = FRANK.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN CNTRY ON FRANK.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_FRANK.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or FRANK.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_FRANK.actflag<>'Z'
go


print ""
go

print " Generating evi_Franking ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Franking ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Franking] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Franking] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Franking] ON [dbo].[evi_Franking]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Franking] ON [dbo].[evi_Franking]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Security_Swap, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Security_Swap')
 drop table evi_Security_Swap
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SCSWP.EventID as char(16)))as bigint) as CAref,
v10s_SCSWP.Levent,
v10s_SCSWP.EventID,
v10s_SCSWP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SCSWP.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SCSWP.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SCSWP.Acttime) THEN PEXDT.Acttime ELSE v10s_SCSWP.Acttime END as [Changed],
v10s_SCSWP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_SCSWP.NewRatio +':'+v10s_SCSWP.OldRatio as Ratio,
CASE WHEN (v10s_SCSWP.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_SCSWP.Fractions) > 0) THEN '[' + v10s_SCSWP.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_SCSWP.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_SCSWP.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_SCSWP.SectyCD) > 0) THEN '[' + v10s_SCSWP.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
RD.RDNotes,
v10s_SCSWP.SCSWPNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Security_Swap
FROM v10s_SCSWP
INNER JOIN RD ON RD.RdID = v10s_SCSWP.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SCSWP' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SCSWP' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_SCSWP.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_SCSWP.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_SCSWP.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON v10s_SCSWP.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_SCSWP.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_SCSWP.actflag<>'Z'
go

print ""
go

print " Generating evi_Security_Swap ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Security_Swap ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Security_Swap] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Security_Swap] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Security_Swap] ON [dbo].[evi_Security_Swap]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Security_Swap] ON [dbo].[evi_Security_Swap]([issid]) ON [PRIMARY]
GO

print ""
go

print " Generating evi_Odd_Lot_Offer, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Odd_Lot_Offer') drop table evi_Odd_Lot_Offer
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ODDLT.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ODDLT.EventID as char(8))) as bigint)
end as CAref,
v10s_ODDLT.Levent,
v10s_ODDLT.EventID,
v10s_ODDLT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ODDLT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ODDLT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ODDLT.Acttime) THEN PEXDT.Acttime ELSE v10s_ODDLT.Acttime END as [Changed],
v10s_ODDLT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'M' as Choice,
RD.RDNotes,
v10s_ODDLT.Startdate,
v10s_ODDLT.Enddate,
v10s_ODDLT.MinAcpQty,
v10s_ODDLT.MaxAcpQty,
v10s_ODDLT.BuyIn,
v10s_ODDLT.BuyInCurenCD,
v10s_ODDLT.BuyInPrice,
v10s_ODDLT.Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into WCA2.dbo.evi_Odd_Lot_Offer
FROM v10s_ODDLT
INNER JOIN RD ON RD.RdID = v10s_ODDLT.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ODDLT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ODDLT' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_ODDLT.EventID = MPAY.EventID AND v10s_ODDLT.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (v10s_ODDLT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or MPAY.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_ODDLT.actflag<>'Z'
go

print ""
go

print " Generating evi_Odd_Lot_Offer ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Odd_Lot_Offer ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Odd_Lot_Offer] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Odd_Lot_Offer] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Odd_Lot_Offer] ON [dbo].[evi_Odd_Lot_Offer]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Odd_Lot_Offer] ON [dbo].[evi_Odd_Lot_Offer]([issid]) ON [PRIMARY]
GO

print ""
go


print ""
go

print " Generating evi_Dividend, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2019/06/05 12:00:00'
if exists (select * from sysobjects where name = 'evi_Dividend')
 drop table evi_Dividend
use wca
select 
case when DIVPY.OptionID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(DIVPY.OptionID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIV.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'1'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIV.EventID as char(8))) as bigint)
end as CAref,
v10s_DIV.Levent,
v10s_DIV.EventID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (DIVPY.Acttime is not null) and (DIVPY.Acttime > v10s_DIV.Acttime) and (DIVPY.Acttime > RD.Acttime) and (DIVPY.Acttime > EXDT.Acttime) and (DIVPY.Acttime > PEXDT.Acttime) THEN DIVPY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
v10s_DIV.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ParValue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
v20c_EV_SCMST.StructCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.ExDate is not null THEN '' WHEN PEXDT.ExDate is not null THEN 'P' ELSE '' END as Pex,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN EXDT.PayDate is not null THEN '' WHEN PEXDT.PayDate is not null THEN 'P' ELSE '' END as Ppy,
v10s_DIV.FYEDate as FinYearEndDate,
CASE WHEN v10s_DIV.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_DIV.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_DIV.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_DIV.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_DIV.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_DIV.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_DIV.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_DIV.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_DIV.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_DIV.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_DIV.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_DIV.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_DIV.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_DIV.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN v10s_DIV.NilDividend= 'T' THEN 'Yes' ELSE '' END as NilDividend,
DIVPY.OptionID as OptionKey,
CASE WHEN (DIVPY.ActFlag is null) THEN irActionDIVPY.Lookup WHEN (irActionDIVPY.Lookup is null) and (LEN(DIVPY.Actflag) > 0) THEN '[' + DIVPY.Actflag +'] not found' ELSE irActionDIVPY.Lookup END as OptionRecordFlag,
CASE WHEN v10s_DIV.NilDividend= 'Y' THEN 'NilDividend'
     WHEN DIVPY.DivType= 'B' THEN 'Cash & Stock'
     WHEN DIVPY.DivType= 'S' THEN 'Stock'
     WHEN DIVPY.DivType= 'C' THEN 'Cash'
     ELSE 'Unspecified' END as DividendType,
DIVPY.GrossDividend,
DIVPY.NetDividend,
CASE WHEN (DIVPY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(DIVPY.CurenCD) > 0) THEN '[' + DIVPY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN 1=1 THEN ' ' ELSE ' ' END as DivInPercent,
CASE WHEN DIVPY.RecindCashDiv= 'T' THEN 'Yes' ELSE '' END as CashDivRecinded,
DIVPY.TaxRate,
CASE WHEN DIVPY.Approxflag= 'T' THEN 'Yes' ELSE '' END as ApproximateDividend,
DIVPY.USDRateToCurrency,
CASE WHEN (DIVPY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(DIVPY.Fractions) > 0) THEN '[' + DIVPY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
DIVPY.Coupon,
DIVPY.CouponID,
DIVPY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
DIVPY.RatioNew +':'+DIVPY.RatioOld as Ratio,
EXDT.Paydate2 as StockPayDate,
'M' as Choice,
RD.RDNotes,
v10s_DIV.DIVNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Dividend
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
LEFT OUTER JOIN SCMST ON DIVPY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON DIVPY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON DIVPY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irACTION as irACTIONDIVPY ON DIVPY.Actflag = irACTIONDIVPY.Code
LEFT OUTER JOIN CUREN ON DIVPY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
Excountry<>'CA' and EXCountry<>'US'
and (v20c_EV_SCMST.SectyCd = 'EQS' Or v20c_EV_SCMST.SectyCd = 'PRF' Or v20c_EV_SCMST.SectyCd = 'DR')
and (DIVPY.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or v10s_DIV.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or RD.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or EXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00'
or PEXDT.Acttime BETWEEN @Startdate AND  '2019/06/05 16:30:00')
and v10s_DIV.actflag<>'Z'
go

print ""
go

print " Generating evi_Dividend ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Dividend ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Dividend] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Dividend] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Dividend] ON [dbo].[evi_Dividend]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Dividend] ON [dbo].[evi_Dividend]([issid]) ON [PRIMARY]
GO


