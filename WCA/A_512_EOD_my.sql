--filepath=o:\Datafeed\WCA\512_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.512
--suffix=
--fileheadertext=Records = NNNNNN Security Data
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\512_EOD\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
SELECT
wca.SCEXH.ScexhID as SecID,
DATE_FORMAT(current_timestamp() , '%d/%m/%Y %h:%i:%s') AS scmsttime,
wca.SCMST.Actflag,
substring(wca.SCEXH.ExchgCD,1,2) as ExCountry,
wca.SCEXH.ExchgCD,
wca.SCMST.IssID,  
wca.SCEXH.Localcode,  
wca.SCMST.Isin,  
wca.SCMST.SecurityDesc,
wca.SCMST.SectyCD as Typecode,
wca.sedolseq1.Sedol,  
wca.SCMST.USCode,
wca.SCMST.AnnounceDate as Sourcedate,
wca.SCMST.AnnounceDate as Creation,
wca.SCMST.IssID as Globissid,
'' as trailingTAB
FROM wca.scmst
LEFT OUTER JOIN wca.scexh ON wca.scmst.SecID = wca.scexh.SecID
LEFT OUTER JOIN wca.sedolseq1 ON wca.scmst.SecID = wca.sedolseq1.SecID
                   and substring(wca.SCEXH.ExchgCD,1,2) = wca.sedolseq1.cntryCD

where (wca.scmst.sectyCD='EQS' or wca.scmst.sectyCD='DR' or wca.scmst.sectyCD='PRF')
and (wca.scmst.acttime >= (select max(feeddate) from wca.tbl_Opslog where seq = 3)
or wca.scexh.acttime >= (select max(feeddate) from wca.tbl_Opslog where seq = 3))
and wca.scexh.scexhid is not null
and wca.sedolseq1.seqnum = 1
order by scexhid, sedolseq1.sedol desc