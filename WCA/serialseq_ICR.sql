use wca
if exists (select * from sysobjects where name = 'serialseq')
	drop table serialseq
go
select distinct
etab.sevent as eventcd,
etab.eventid as rdid,
etab.bonid as uniqueid,
1 as seqnum
into serialseq
from v10s_BON as etab
go

USE WCA
ALTER TABLE serialseq ALTER COLUMN eventcd char(3) NOT NULL
go
USE WCA
ALTER TABLE serialseq ALTER COLUMN rdid int NOT NULL
go
USE WCA
ALTER TABLE serialseq ALTER COLUMN uniqueid int NOT NULL
go

ALTER TABLE [DBO].[serialseq] WITH NOCHECK ADD
	
	CONSTRAINT [PK_serialseq] PRIMARY KEY 
	
	(
		[eventcd],[rdid],[uniqueid]
	)  ON [PRIMARY] 

go 

insert into serialseq 
select
etab.sevent as eventcd,
etab.eventid as rdid,
etab.prfid as uniqueid,
1 as seqnum
from v10s_PRF as etab
go

insert into serialseq 
select
etab.sevent as eventcd,
etab.eventid as rdid,
etab.rtsid as uniqueid,
1 as seqnum
from v10s_RTS as etab
go


EXEC sp_serialseq
go
