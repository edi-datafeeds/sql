--filepath=O:\Datafeed\WCA\Weekly_LSTAT\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_LSTAT_Weekly
--fileheadertext=WEEKLY_EDI_LSTAT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Bespoke\mbendi\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
SELECT 
upper('LSTAT') as TableName,
wca.dbo.SCMST.SectyCD,
wca.dbo.lstat.AnnounceDate as Created,
wca.dbo.lstat.Acttime as Changed,
wca.dbo.lstat.LstatID,
wca.dbo.SCMST.SecID,
wca.dbo.SCMST.ISIN,
wca.dbo.issur.Issuername,
wca.dbo.lstat.ExchgCD,
wca.dbo.exchg.MIC,
wca.dbo.scexh.Localcode,
wca.dbo.lstat.NotificationDate,
wca.dbo.lstat.EffectiveDate,
wca.dbo.lstat.LStatStatus,
wca.dbo.lstat.Eventtype,
wca.dbo.lstat.Reason
FROM wca.dbo.lstat
INNER JOIN wca.dbo.SCMST ON wca.dbo.lstat.SecID = wca.dbo.SCMST.SecID
INNER JOIN wca.dbo.issur ON wca.dbo.scmst.issid = wca.dbo.issur.issid
INNER JOIN wca.dbo.exchg ON wca.dbo.lstat.exchgcd = wca.dbo.exchg.exchgcd
INNER JOIN wca.dbo.scexh ON wca.dbo.lstat.exchgcd = wca.dbo.scexh.exchgcd and wca.dbo.SCMST.secid=wca.dbo.SCexh.secid
WHERE (wca.dbo.lstat.Acttime>=getdate()-7 and wca.dbo.lstat.Acttime<getdate()+0)
AND (wca.dbo.SCMST.SectyCD = 'EQS'
OR wca.dbo.SCMST.SectyCD = 'CDR'
OR wca.dbo.SCMST.SectyCD = 'DR'
OR wca.dbo.SCMST.SectyCD = 'ETF')
ORDER BY wca.dbo.lstat.Acttime desc, wca.dbo.lstat.ExchgCD asc