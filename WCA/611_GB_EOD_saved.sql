--filepath=o:\Datafeed\wca\611_GB_PID\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.611
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\611_GB\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
select
'Dividend' as dummy,
'EventID' as dummy,
'IssID' as dummy,
'SecID' as dummy,
'Created' as dummy,
'Changed' as dummy,
'Action' as dummy,
'CntryofIncorp' as dummy,
'IssuerName' as dummy,
'SecurityDesc' as dummy,
'ParValue' as dummy,
'PVCurrency' as dummy,
'ISIN' as dummy,
'USCode' as dummy,
'SecStatus' as dummy,
'PrimaryEx' as dummy,
'Exchange' as dummy,
'ExchgCD' as dummy,
'MIC' as dummy,
'ExCountry' as dummy,
'RegCountry' as dummy,
'Sedol' as dummy,
'LocalCode' as dummy,
'ListingStatus' as dummy,
'ListDate' as dummy,
'Lot' as dummy,
'MinTrdgQty' as dummy,
'Recdate' as dummy,
'ExDate' as dummy,
'PayDate' as dummy,
'PayDate2' as dummy,
'FYEDate' as dummy,
'DivPeriodCD' as dummy,
'Tbaflag' as dummy,
'Cashopt' as dummy,
'StockOpt' as dummy,
'OptionID' as dummy,
'ActionDIVPY' as dummy,
'Divtype' as dummy,
'CurenCD' as dummy,
'NilDividend' as dummy,
'Approxflag' as dummy,
'DivInPercent' as dummy,
'GrossDividend' as dummy,
'NetDividend' as dummy,
'TaxRate' as dummy,
'RecindCashDiv' as dummy,
'USDRateToCurrency' as dummy,
'ResSecType' as dummy,
'Fraction' as dummy,
'RecindStockDiv' as dummy,
'Depfees' as dummy,
'Coupon' as dummy,
'CouponID' as dummy,
'ResSecID' as dummy,
'ResIsin' as dummy,
'RatioNew' as dummy,
'RatioOld' as dummy,
'Group2GrossDiv' as dummy,
'Group2NetDiv' as dummy,
'RDNotes' as dummy,
'DIVNotes'

--# 2
use wca
SELECT * 
FROM v54f_611_Dividend_GB_PID
WHERE
changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol

--# 3
use wca
SELECT * 
FROM v54f_611_Dividend_GB_NONPID
WHERE
changed>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3)
and (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol
