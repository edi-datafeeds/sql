--filepath=o:\datafeed\wca\514_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.514
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\514_EOD\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n





--# 1
USE WCA

SELECT 
Issuername as Issname,
Isin,
Sedol,
Uscode,
Localcode,
Scexhid as SecID,
EventId as Divseqnum,
'1' as Divelement,
Changed as Acttime,
case when nildividend = 'T' and nildividend is not null then 'O'
     else DIVPYActflag 
     end as actflag,
ExCountry as Cntycode,
ExchgCD as Exchcode,
case when DivPeriodCD = 'SPL' then 'Special'
     when Divtype = 'C' then 'Cash'
     when Divtype = 'S' then 'Stock'
     else 'Cash'
     end as Divtype,
case when DivPeriodCD = 'SPL' then 'Special'
     when DivPeriodCD = 'ANL' then 'Annual'
     when DivPeriodCD = 'SMA' then 'Semi-Annual'
     when DivPeriodCD = 'QTR' then 'Quarterly'
     when DivPeriodCD = 'MNT' then 'Monthly'
     when DivPeriodCD = 'QTR' then 'Quarterly'
     else 'Unknown'
     end as DivPeriod,
case when DivPeriodCD = 'FNL' then 'Final'
     when DivPeriodCD = 'ANL' then 'Final'
     when DivPeriodCD = 'SPL' then 'N/A'
     else 'Interim'
     end as Finalflag,
case when divtype = 'C' then CurenCD
     when divtype_2 = 'C' then CurenCD_2
else '' end as Currcode,
'No' as Foreignflag,
case when divtype = 'C' then Taxrate
     when divtype_2 = 'C' then Taxrate_2
else '' end as Taxrate,
Exdate,
'No' as Exflag,
Recdate,
'No' as Recflag,
Paydate,
'No' as Payflag,
case when Tbaflag = 'T' then 'Yes'
     else 'No'
     end as Tbaflag,
case when divtype = 'C' then NetDividend
     when divtype_2 = 'C' then NetDividend_2
else '' end as Netrate,
case when divtype = 'C' then GrossDividend
     when divtype_2 = 'C' then GrossDividend_2
else '' end as Grossrate,
'' as Stockrate,
case when divtype = 'S' then RatioNew
     when divtype_2 = 'S' then RatioNew_2
else '' end as Numerator,
case when divtype = 'S' then RatioOld
     when divtype_2 = 'S' then RatioOld_2
else '' end as Denominator,
case when Approxflag = 'T' then 'Yes'
     else 'No'
     end as Approxflag,
Created as InfoDate,
Created as CreationDate,
'' as Notes,
case when divtype = 'C' and divtype_2 = 'S' then 'Yes'
     when divtype = 'S' and divtype_2 = 'C' then 'Yes'
     else 'No' end as Optionflag,
'' as Other1
from v54f_618_FlatDividend
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 1)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
AND (Divtype<>'B')
and scexhid is not null