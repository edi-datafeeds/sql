use wca
if exists (select * from sysobjects where name = 'sedolseq3')
	drop table sedolseq3
go
select distinct
SEDOL.secid,
SEDOL.sedol,
SEDOL.cntrycd,
SEDOL.rcntrycd,
SEDOL.curencd,
SEDOL.sedolid,
1 as seqnum,
case when sedol.defunct='F' then SEDOL.Acttime else '2000/01/01 15:00:00' end as SedolActtime,
case when sedol.defunct='T' then SEDOL.Acttime else null end as DefunctActtime,
case when sedol.source<>'STKCON' then lse.opol when lse.opol='XSHG' then 'XSSC' else 'SHSC' end as opol
into sedolseq3
from SEDOL
inner join smf4.dbo.security as lse on sedol.sedol = lse.sedol
where (SEDOL.actflag<>'D' or SEDOL.source='STKCON')
and SEDOL.cntrycd<>'ZZ'
and SEDOL.cntrycd<>''
and SEDOL.cntrycd is not null
go

USE WCA
ALTER TABLE sedolseq3 ALTER COLUMN  secid int NOT NULL
go
USE WCA
ALTER TABLE sedolseq3 ALTER COLUMN  sedol char(7) NOT NULL
go

ALTER TABLE [DBO].[sedolseq3] WITH NOCHECK ADD
	
	CONSTRAINT [PK_sedolseq3] PRIMARY KEY 
	
	(
		[secid],[sedol]
	)  ON [PRIMARY] 

go 

use WCA
CREATE  INDEX [ix_sedolseq3] ON [dbo].[sedolseq3]([secid],[cntrycd]) ON [PRIMARY] 
go

EXEC sp_sedolseq3

go
