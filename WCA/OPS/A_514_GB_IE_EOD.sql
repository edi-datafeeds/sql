--filepath=o:\datafeed\wca\514_GB_IE_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.514
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\514_GB_IE_EOD\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 0
SELECT 
'Issname' AS f3,
'Isin' AS f3,
'Sedol' AS f3,
'Uscode' AS f3,
'Localcode' AS f3,
'SecID' AS f3,
'Divseqnum' AS f3,
'Divelement' AS f3,
'Acttime' AS f3,
'actflag' AS f3,
'Cntycode' AS f3,
'Exchcode' AS f3,
'Divtype' AS f3,
'DivPeriod' AS f3,
'Finalflag' AS f3,
'Currcode' AS f3,
'Taxrate' AS f3,
'Exdate' AS f3,
'Exflag' AS f3,
'Recdate' AS f3,
'Recflag' AS f3,
'Paydate' AS f3,
'Payflag' AS f3,
'Tbaflag' AS f3,
'Netrate' AS f3,
'Grossrate' AS f3,
'Numerator' AS f3,
'Denominator' AS f3,
'Approxflag' AS f3,
'InfoDate' AS f3,
'CreationDate' AS f3,
'Notes' AS f3,
'Optionflag' AS f3,
'Other1' AS f3;


--# 1
USE WCA

SELECT 
Issuername as Issname,
v54f_618_FlatDividend.Isin,
Sedol,
v54f_618_FlatDividend.Uscode,
Localcode,
Scexhid as SecID,
EventId as Divseqnum,
'1' as Divelement,
Changed as Acttime,
case when nildividend = 'T' and nildividend is not null then 'O'
     else DIVPYActflag 
     end as actflag,
ExCountry as Cntycode,
ExchgCD as Exchcode,
case when DivPeriodCD = 'SPL' then 'Special'
     when Divtype = 'C' then 'Cash'
     when Divtype = 'S' then 'Stock'
     else 'Cash'
     end as Divtype,
case when DivPeriodCD = 'SPL' then 'Special'
     when DivPeriodCD = 'ANL' then 'Annual'
     when DivPeriodCD = 'SMA' then 'Semi-Annual'
     when DivPeriodCD = 'QTR' then 'Quarterly'
     when DivPeriodCD = 'MNT' then 'Monthly'
     when DivPeriodCD = 'QTR' then 'Quarterly'
     else 'Unknown'
     end as DivPeriod,
case when DivPeriodCD = 'FNL' then 'Final'
     when DivPeriodCD = 'ANL' then 'Final'
     when DivPeriodCD = 'SPL' then 'N/A'
     else 'Interim'
     end as Finalflag,
case when divtype = 'C' then v54f_618_FlatDividend.CurenCD
     when divtype_2 = 'C' then v54f_618_FlatDividend.CurenCD_2
else '' end as Currcode,
'No' as Foreignflag,
case when divtype = 'C' then Taxrate
     when divtype_2 = 'C' then Taxrate_2
else '' end as Taxrate,
Exdate,
'No' as Exflag,
Recdate,
'No' as Recflag,
Paydate,
'No' as Payflag,
case when Tbaflag = 'T' then 'Yes'
     else 'No'
     end as Tbaflag,
case when divtype = 'C' then NetDividend
     when divtype_2 = 'C' then NetDividend_2
else '' end as Netrate,
case when divtype = 'C' then GrossDividend
     when divtype_2 = 'C' then GrossDividend_2
else '' end as Grossrate,
'' as Stockrate,
case when divtype = 'S' then RatioNew
     when divtype_2 = 'S' then RatioNew_2
else '' end as Numerator,
case when divtype = 'S' then RatioOld
     when divtype_2 = 'S' then RatioOld_2
else '' end as Denominator,
case when Approxflag = 'T' then 'Yes'
     else 'No'
     end as Approxflag,
Created as InfoDate,
Created as CreationDate,
'' as Notes,
case when divtype = 'C' and divtype_2 = 'S' then 'Yes'
     when divtype = 'S' and divtype_2 = 'C' then 'Yes'
     else 'No' end as Optionflag,
'' as Other1

from v54f_618_FlatDividend
WHERE CHANGED >= (select max(acttime) from tbl_Opslog where seq = 1)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
AND (Divtype<>'B')
and scexhid is not null
AND (ExCountry = 'GB'
  or ExCountry = 'IE')
