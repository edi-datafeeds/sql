--filepath=n:\wca\hist\
--filenameprefix=2016_2017_Quadhist_
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT *  
FROM v50f_620_Company_Meeting 
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
SELECT * 
FROM v53f_620_Call
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
SELECT *  
FROM v50f_620_Liquidation
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 4
USE WCA
SELECT * 
FROM v51f_620_Certificate_Exchange
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
SELECT *  
FROM v51f_620_International_Code_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
SELECT *  
FROM v51f_620_Conversion_Terms
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
SELECT *  
FROM v51f_620_Redemption_Terms
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
SELECT *  
FROM v51f_620_Security_Reclassification
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
SELECT *  
FROM v52f_620_Lot_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
SELECT *  
FROM v52f_620_Sedol_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca
SELECT *  
FROM v53f_620_Buy_Back
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
SELECT *  
FROM v53f_620_Capital_Reduction
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 13
use wca
SELECT *  
FROM v53f_620_Takeover
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 14
use wca
SELECT *  
FROM v54f_620_Arrangement
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 15
use wca
SELECT *  
FROM v54f_620_Bonus
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
SELECT *  
FROM v54f_620_Bonus_Rights
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
SELECT *  
FROM v54f_620_Consolidation
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
SELECT *  
FROM v54f_620_Demerger
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 19
use wca
SELECT *  
FROM v54f_620_Distribution
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
SELECT *  
FROM v54f_620_Divestment
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 21
use wca
SELECT *  
FROM v54f_620_Entitlement
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
SELECT *  
FROM v54f_620_Merger
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 23
use wca
SELECT *  
FROM v54f_620_Preferential_Offer
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 24
use wca
SELECT *  
FROM v54f_620_Purchase_Offer
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
SELECT *  
FROM v54f_620_Rights 
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
SELECT *  
FROM v54f_620_Security_Swap 
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
SELECT * 
FROM v54f_620_Subdivision
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
SELECT * 
FROM v50f_620_Bankruptcy 
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
SELECT * 
FROM v50f_620_Financial_Year_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
SELECT * 
FROM v50f_620_Incorporation_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
SELECT * 
FROM v50f_620_Issuer_Name_change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
SELECT * 
FROM v50f_620_Class_Action
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
SELECT * 
FROM v51f_620_Security_Description_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
SELECT * 
FROM v52f_620_Assimilation
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
SELECT * 
FROM v52f_620_Listing_Status_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
SELECT * 
FROM v52f_620_Local_Code_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
SELECT *  
FROM v52f_620_New_Listing
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
SELECT *  
FROM v50f_620_Announcement
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
SELECT *  
FROM v51f_620_Parvalue_Redenomination 
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
USE WCA
SELECT *  
FROM v51f_620_Currency_Redenomination 
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
SELECT *  
FROM v53f_620_Return_of_Capital 
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
SELECT *  
FROM v54f_620_Dividend
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
SELECT *  
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 44
USE WCA
SELECT *  
FROM v54f_620_Franking
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol

--# 45
use wca
SELECT * 
FROM v51f_620_Conversion_Terms_Change
WHERE (CHANGED>= '2016-01-01' and CHANGED<'2018-01-01')
and (excountry='CL'
or excountry='CN'
or excountry='CO'
or excountry='ID'
or excountry='MX'
or excountry='MY'
or excountry='PL'
or excountry='SA'
or excountry='TH'
or excountry='TR')

ORDER BY EventID desc, ExchgCD, Sedol
