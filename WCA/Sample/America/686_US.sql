--filepath=o:\prodman\salesfeeds\wca\686\america
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.686
--suffix=_US_SampleData
--fileheadertext=EDI_WCA_686_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\datafeed\wca\686\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--#
select distinct
'DIV' as EventCD,
etab.DivID as EventID,
wca.divpy.OptionID,
'1' as SerialID,
wca.scexh.ScexhID,
etab.ActFlag,
etab.Acttime as Changed,
etab.AnnounceDate as Created,
wca.scmst.SecID,
wca.scmst.IssID,
wca.scmst.isin as Isin,
wca.scmst.uscode as Uscode,
wca.issur.IssuerName,
wca.issur.CntryofIncorp,
wca.scmst.SectyCD,
wca.scmst.SecurityDesc,
wca.scmst.ParValue,
wca.scmst.CurenCD as PVCurrency,
wca.scmst.StatusFlag,
wca.scmst.PrimaryExchgCD,
substring(wca.scexh.ExchgCD,1,2) as ExchgCntry,
wca.scexh.ExchgCD,
wca.exchg.Mic,
wca.mktsg.Mic as Micseg,
wca.scexh.LocalCode,
wca.scexh.ListStatus,
'ExDate' as Date1Type,
CASE WHEN wca.exdt.ExDate IS NOT NULL then wca.exdt.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN wca.exdt.PayDate IS NOT NULL then wca.exdt.PayDate ELSE pexdt.paydate END as Date3,
'Paydate2' as Date4Type,
CASE WHEN wca.exdt.PayDate2 IS NOT NULL then wca.exdt.PayDate2 ELSE pexdt.paydate2 END as Date3,
'FYEDate' as Date5Type,
etab.FYEDate Date5,
'PeriodEndDate' as Date6Type,
etab.PeriodEndDate as Date6,
'OptElectionDate' as Date7Type,
wca.divpy.OptElectionDate as Date7,
'ToDate' as Date8Type,
rd.ToDate as Date8,
'RegistrationDate' as Date9Type,
rd.RegistrationDate as Date9,
'DeclarationDate' as Date10Type,
etab.DeclarationDate as Date10,
'' as Date11Type,
null as Date11,
'' as Date12Type,
null as Date12,
wca.divpy.Divtype as Paytype,
wca.divpy.DefaultOpt as DefaultOpt,
wca.divpy.ResSecID as OutturnSecID,
resscmst.Isin as OutturnIsin,
wca.divpy.RatioNew,
wca.divpy.RatioOld,
wca.divpy.Fractions as Fractions,
wca.divpy.CurenCD as Currency,
'GrossDividend' as Rate1Type,
wca.divpy.GrossDividend as Rate1,
'NetDividend' as Rate2Type,
wca.divpy.NetDividend as  Rate2,
'Marker' as Field1Name,
etab.Marker as Field1,
'Frequency' as Field2Name,
etab.Frequency as Field2,
'Tbaflag' as Field3Name,
etab.Tbaflag as Field3,
'NilDividend' as Field4Name,
etab.NilDividend as Field4,
'DivRescind' as Field5Name,
etab.DivRescind as Field5,
'RecindCashDiv' as Field6Name,
wca.divpy.RecindCashDiv as Field6,
'RecindStockDiv' as Field7Name,
wca.divpy.RecindStockDiv as Field7,
'Approxflag' as Field8Name,
wca.divpy.Approxflag as Field8,
'TaxRate' as Field9Name,
wca.divpy.TaxRate as Field9,
'Depfees' as Field10Name,
wca.divpy.Depfees as Field10,
'Coupon' as Field11Name,
wca.divpy.Coupon as Field11,
'Priority' as Field12Name,
wca.rdprt.Priority as Field12
from wca.div_my as etab
inner join wca.rd on etab.rdid=wca.rd.rdid
left outer join wca.rdprt on etab.rdid=wca.rdprt.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
left outer join wca.divpy on wca.etab.divid = wca.divpy.divid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
left outer join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scexh.exchgcd = wca.exdt.exchgcd and 'div' = wca.exdt.eventtype
left outer join wca.exdt as pexdt on wca.rd.rdid = pexdt.rdid and wca.scmst.primaryexchgcd = pexdt.exchgcd and 'div' = pexdt.eventtype
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.scmst as resscmst on wca.divpy.ressecid = resscmst.secid
WHERE
wca.exchg.cntrycd = 'US'
and etab.actflag<>'D'
order by etab.acttime desc
limit 500
