--filenameprefix=620
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_QA_SampleData
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
USE WCA
select top 20 *  
FROM v50f_620_Company_Meeting 
left outer join continent on excountry = continent.cntrycd 
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
select top 20 * 
FROM v53f_620_Call 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
select top 20 *  
FROM v50f_620_Liquidation 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA
select top 20 * 
FROM v51f_620_Certificate_Exchange 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
select top 20 *  
FROM v51f_620_International_Code_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
select top 20 *  
FROM v51f_620_Conversion_Terms
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
select top 20 *  
FROM v51f_620_Redemption_Terms
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
select top 20 *  
FROM v51f_620_Security_Reclassification
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
select top 20 *  
FROM v52f_620_Lot_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
select top 20 *  
FROM v52f_620_Sedol_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 11
use wca
select top 20 *  
FROM v53f_620_Buy_Back
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 12
use wca
select top 20 *  
FROM v53f_620_Capital_Reduction
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 13
use wca
select top 20 *  
FROM v53f_620_Takeover
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 14
use wca
select top 20 *  
FROM v54f_620_Arrangement
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 15
use wca
select top 20 *  
FROM v54f_620_Bonus
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
select top 20 *  
FROM v54f_620_Bonus_Rights
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
select top 20 *  
FROM v54f_620_Consolidation
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
select top 20 *  
FROM v54f_620_Demerger
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 19
use wca
select top 20 *  
FROM v54f_620_Distribution
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 20
use wca
select top 20 *  
FROM v54f_620_Divestment
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 21
use wca
select top 20 *  
FROM v54f_620_Entitlement
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
select top 20 *  
FROM v54f_620_Merger
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol, OptionID, SerialID

--# 23
use wca
select top 20 *  
FROM v54f_620_Preferential_Offer
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 24
use wca
select top 20 *  
FROM v54f_620_Purchase_Offer
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
select top 20 *  
FROM v54f_620_Rights 
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
select top 20 *  
FROM v54f_620_Security_Swap 
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
select top 20 * 
FROM v54f_620_Subdivision
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
select top 20 * 
FROM v50f_620_Bankruptcy 
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
select top 20 * 
FROM v50f_620_Financial_Year_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
select top 20 * 
FROM v50f_620_Incorporation_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
select top 20 * 
FROM v50f_620_Issuer_Name_change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
select top 20 * 
FROM v50f_620_Class_Action
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
select top 20 * 
FROM v51f_620_Security_Description_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
select top 20 * 
FROM v52f_620_Assimilation
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
select top 20 * 
FROM v52f_620_Listing_Status_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
select top 20 * 
FROM v52f_620_Local_Code_Change
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
select top 20 *  
FROM v52f_620_New_Listing
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
select top 20 *  
FROM v50f_620_Announcement
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
select top 20 *  
FROM v51f_620_Parvalue_Redenomination 
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
USE WCA
select top 20 *  
FROM v51f_620_Currency_Redenomination 
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
select top 20 *  
FROM v53f_620_Return_of_Capital 
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
select top 20 *  
FROM v54f_620_Dividend_Reinvestment_Plan
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
select top 20 *  
FROM v54f_620_Franking
 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'

ORDER BY EventID desc, ExchgCD, Sedol

--# 44
use wca
select top 20 * 
FROM v51f_620_Conversion_Terms_Change 
left outer join continent on excountry = continent.cntrycd
WHERE CHANGED >= '2005/01/01'
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and excountry='QA'
ORDER BY EventID desc, ExchgCD, Sedol
