--filenameprefix=620
--filename=
--filenamealt=
--fileextension=.txt
--suffix=_Topix70_Secid_SampleData
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--zerorowchk=n

--# 1
USE WCA
select * 
FROM v50f_620_Company_Meeting
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 2
USE WCA
select *
FROM v53f_620_Call
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 3
USE WCA
select *
FROM v50f_620_Liquidation 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 4
USE WCA
select *
FROM v51f_620_Certificate_Exchange 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 5
USE WCA
select *  
FROM v51f_620_International_Code_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 6
USE WCA
select *  
FROM v51f_620_Conversion_Terms
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 7
use wca
select *  
FROM v51f_620_Redemption_Terms
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 8
use wca
select *  
FROM v51f_620_Security_Reclassification
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 9
use wca
select *
FROM v52f_620_Lot_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 10
use wca
select *
FROM v52f_620_Sedol_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 75
use wca
select *  
FROM v53f_620_Buy_Back
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 12
use wca
select *  
FROM v53f_620_Capital_Reduction
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 13
use wca
select *  
FROM v53f_620_Takeover
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 14
use wca
select *  
FROM v54f_620_Arrangement
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 15
use wca
select *  
FROM v54f_620_Bonus
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 16
use wca
select *  
FROM v54f_620_Bonus_Rights
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
select *  
FROM v54f_620_Consolidation
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 18
use wca
select *  
FROM v54f_620_Demerger
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 19
use wca
select *  
FROM v54f_620_Distribution
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 20
use wca
select *  
FROM v54f_620_Divestment
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 21
use wca
select *  
FROM v54f_620_Entitlement
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 22
use wca
select *  
FROM v54f_620_Merger
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 23
use wca
select *  
FROM v54f_620_Preferential_Offer
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 24
use wca
select *  
FROM v54f_620_Purchase_Offer
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
select *  
FROM v54f_620_Rights 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 26
use wca
select *  
FROM v54f_620_Security_Swap 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 27
use wca
select * 
FROM v54f_620_Subdivision
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 28
use wca
select * 
FROM v50f_620_Bankruptcy 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 29
use wca
select * 
FROM v50f_620_Financial_Year_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 30
use wca
select * 
FROM v50f_620_Incorporation_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 31
use wca
select * 
FROM v50f_620_Issuer_Name_change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 32
use wca
select * 
FROM v50f_620_Class_Action
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 33
use wca
select * 
FROM v51f_620_Security_Description_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 34
use wca
select * 
FROM v52f_620_Assimilation
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 35
use wca
select * 
FROM v52f_620_Listing_Status_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 36
use wca
select * 
FROM v52f_620_Local_Code_Change
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 37
use wca
select *  
FROM v52f_620_New_Listing
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 38
use wca
select *  
FROM v50f_620_Announcement
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 39
use wca
select *  
FROM v51f_620_Parvalue_Redenomination 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 40
USE WCA
select *  
FROM v51f_620_Currency_Redenomination 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 41
USE WCA
select *  
FROM v53f_620_Return_of_Capital 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 42
USE WCA
select *  
FROM v54f_620_Dividend_Reinvestment_Plan
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 43
USE WCA
select *  
FROM v54f_620_Franking
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol

--# 44
use wca
select * 
FROM v51f_620_Conversion_Terms_Change 
WHERE 
CHANGED <=getdate()-93
and secid in (select secid from icon.dbo.ixcon where ixnamid=75 and onindex='T')
AND (SecStatus <> 'I' OR Secstatus IS NULL)
and exchgcd in (select exchgcd from icon.dbo.ixnam where ixnamid=75)
ORDER BY EventID desc, ExchgCD, Sedol
