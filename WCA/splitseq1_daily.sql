use client
if exists (select * from sysobjects where name = 'splitseq1')
	drop table splitseq1
go
use wca
select
rd.secid,
scexh.exchgcd,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as SplitDate,
maintab.newratio,
maintab.oldratio,
1 as seqnum,
maintab.rdid as eventid,
'SD' as eventtype
into client.dbo.splitseq1
from sd as maintab
inner join rd on maintab.rdid=rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on rd.secid = scexh.secid
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'sd' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'sd' = pexdt.eventtype
where (maintab.actflag<>'D')
and (exdt.exdate is not null or pexdt.exdate is not null)
and maintab.newratio is not null and  maintab.newratio<>''
and maintab.oldratio is not null and  maintab.oldratio<>''
union
select
rd.secid,
scexh.exchgcd,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as SplitDate,
maintab.newratio,
maintab.oldratio,
1 as seqnum,
maintab.rdid as eventid,
'CONSD' as eventtype
from consd as maintab
inner join rd on maintab.rdid=rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on rd.secid = scexh.secid
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'CONSD' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'CONSD' = pexdt.eventtype
where (maintab.actflag<>'D')
and (exdt.exdate is not null or pexdt.exdate is not null)
and maintab.newratio is not null and  maintab.newratio<>''
and maintab.oldratio is not null and  maintab.oldratio<>''
union
select
maintab.secid,
scexh.exchgcd,
maintab.effectivedate as SplitDate,
maintab.newratio,
maintab.oldratio,
1 as seqnum,
maintab.caprdid as eventid,
'CAPRD' as eventtype
from CAPRD as maintab
inner join scmst on maintab.secid = scmst.secid
inner join scexh on maintab.secid = scexh.secid
where (maintab.actflag<>'D')
and maintab.effectivedate is not null
and maintab.newratio<>''
and maintab.oldratio<>''
and maintab.newratio<>maintab.oldratio

go

USE client
ALTER TABLE splitseq1 ALTER COLUMN  eventid int NOT NULL
go
USE client
ALTER TABLE splitseq1 ALTER COLUMN  exchgcd char(7) NOT NULL
go

ALTER TABLE [DBO].[splitseq1] WITH NOCHECK ADD
	
	CONSTRAINT [PK_splitseq1] PRIMARY KEY 
	
	(
		[exchgcd],[eventid],[eventtype]
	)  ON [PRIMARY] 

go 

use client
CREATE  INDEX [ix_splitseq1] ON [dbo].[splitseq1]([exchgcd],[eventid],[eventtype]) ON [PRIMARY] 
go

use wca
EXEC sp_splitseq1

go

