print " Generating ev_Dividend, please wait..."
go


use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Dividend')
 drop table ev_Dividend
use wca
select 
case when DIVPY.OptionID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(DIVPY.OptionID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIV.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'1'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIV.EventID as char(8))) as bigint)
end as CAref,
v10s_DIV.Levent,
v10s_DIV.EventID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (DIVPY.Acttime is not null) and (DIVPY.Acttime > v10s_DIV.Acttime) and (DIVPY.Acttime > RD.Acttime) and (DIVPY.Acttime > EXDT.Acttime) and (DIVPY.Acttime > PEXDT.Acttime) THEN DIVPY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
v10s_DIV.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ParValue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.ExDate is not null THEN '' WHEN PEXDT.ExDate is not null THEN 'P' ELSE '' END as Pex,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN EXDT.PayDate is not null THEN '' WHEN PEXDT.PayDate is not null THEN 'P' ELSE '' END as Ppy,
v10s_DIV.FYEDate as FinYearEndDate,
CASE WHEN v10s_DIV.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_DIV.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_DIV.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_DIV.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_DIV.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_DIV.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_DIV.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_DIV.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_DIV.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_DIV.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_DIV.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_DIV.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_DIV.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_DIV.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN v10s_DIV.NilDividend= 'T' THEN 'Yes' ELSE '' END as NilDividend,
DIVPY.OptionID as OptionKey,
CASE WHEN (DIVPY.ActFlag is null) THEN irActionDIVPY.Lookup WHEN (irActionDIVPY.Lookup is null) and (LEN(DIVPY.Actflag) > 0) THEN '[' + DIVPY.Actflag +'] not found' ELSE irActionDIVPY.Lookup END as OptionRecordFlag,
CASE WHEN v10s_DIV.NilDividend= 'Y' THEN 'NilDividend'
     WHEN DIVPY.DivType= 'B' THEN 'Cash & Stock'
     WHEN DIVPY.DivType= 'S' THEN 'Stock'
     WHEN DIVPY.DivType= 'C' THEN 'Cash'
     ELSE 'Unspecified' END as DividendType,
DIVPY.GrossDividend,
DIVPY.NetDividend,
' ' as DivInPercent,
CASE WHEN DIVPY.RecindCashDiv= 'T' THEN 'Yes' ELSE '' END as CashDivRecinded,
DIVPY.TaxRate,
CASE WHEN DIVPY.Approxflag= 'T' THEN 'Yes' ELSE '' END as ApproximateDividend,
DIVPY.USDRateToCurrency,
CASE WHEN (DIVPY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(DIVPY.Fractions) > 0) THEN '[' + DIVPY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
DIVPY.Coupon,
DIVPY.CouponID,
DIVPY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
DIVPY.RatioNew +':'+DIVPY.RatioOld as Ratio,
EXDT.Paydate2 as StockPayDate,
'M' as Choice,
RD.RDNotes,
v10s_DIV.DIVNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Dividend
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
LEFT OUTER JOIN SCMST ON DIVPY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON DIVPY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON DIVPY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irACTION as irACTIONDIVPY ON DIVPY.Actflag = irACTIONDIVPY.Code
LEFT OUTER JOIN CUREN ON DIVPY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where (DIVPY.Acttime = @Startdate
or v10s_DIV.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)
And (SectyGrp.secgrpid = 1 Or  SectyGrp.secgrpid = 2)
go

print " Generating ev_Announcement, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Announcement')
 drop table ev_Announcement
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_ANN.EventID as char(8))),len(v10s_ANN.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_ANN.Levent,
EventID,
v10s_ANN.AnnounceDate as Created,
v10s_ANN.Acttime as Changed,
v10s_ANN.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_ANN.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ANN.EventType) > 0) THEN '[' + v10s_ANN.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ANN.NotificationDate,
'M' as Choice,
v10s_ANN.AnnNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Announcement
FROM v10s_ANN
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_Ann.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ANN.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_ANN.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Bankruptcy, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Bankruptcy')
 drop table ev_Bankruptcy
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_BKRP.EventID as char(8))),len(v10s_BKRP.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_BKRP.Levent,
v10s_BKRP.EventID,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_BKRP.NotificationDate,
v10s_BKRP.FilingDate,
'M' as Choice,
v10s_BKRP.BkrpNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Bankruptcy
FROM v10s_BKRP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_BKRP.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_BKRP.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go


print " Generating ev_Company_Meeting, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Company_Meeting')
 drop table ev_Company_Meeting
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_AGM.EventID as char(8))),len(v10s_AGM.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_AGM.Levent,
v10s_AGM.EventID,
v10s_AGM.AnnounceDate as Created,
v10s_AGM.Acttime as Changed,
v10s_AGM.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_AGM.AGMDate,
v10s_AGM.AGMEGM,
v10s_AGM.AGMNO,
v10s_AGM.FYEDate as FinYearEndDate,
v10s_AGM.AGMTime,
v10s_AGM.Add1 as Address1,
v10s_AGM.Add2 as Address2,
v10s_AGM.Add3 as Address3,
v10s_AGM.Add4 as Address4,
v10s_AGM.Add5 as Address5,
v10s_AGM.Add6 as Address6,
v10s_AGM.City,
CASE WHEN (v10s_AGM.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_AGM.CntryCD) > 0) THEN '[' + v10s_AGM.CntryCD +'] not found' ELSE Cntry.Country END as Country,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Company_Meeting
FROM v10s_AGM
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_AGM.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN CNTRY ON v10s_AGM.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_AGM.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Financial_Year_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Financial_Year_Change')
 drop table ev_Financial_Year_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_FYCHG.EventID as char(8))),len(v10s_FYCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_FYCHG.Levent,
v10s_FYCHG.EventID,
v10s_FYCHG.AnnounceDate as Created,
v10s_FYCHG.Acttime as Changed,
v10s_FYCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_FYCHG.NotificationDate,
v10s_FYCHG.OldFYStartDate as OldFinYearStart,
v10s_FYCHG.OldFYEndDate as OldFinYearEnd,
v10s_FYCHG.NewFYStartDate as NewFinYearStart,
v10s_FYCHG.NewFYEndDate as NewFinYearEnd,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Financial_Year_Change
FROM v10s_FYCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_FYCHG.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_FYCHG.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Incorporation_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Incorporation_Change')
 drop table ev_Incorporation_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_INCHG.EventID as char(8))),len(v10s_INCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_INCHG.Levent,
v10s_INCHG.EventID,
v10s_INCHG.AnnounceDate as Created,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_INCHG.InChgDate as EffectiveDate,
CASE WHEN (v10s_INCHG.OldCntryCD is null) THEN OldCntry.Country WHEN (OldCntry.Country is null) and (LEN(v10s_INCHG.OldCntryCD) > 0) THEN '[' + v10s_INCHG.OldCntryCD +'] not found' ELSE OldCntry.Country END as OldCountry,
CASE WHEN (v10s_INCHG.NewCntryCD is null) THEN NewCntry.Country WHEN (NewCntry.Country is null) and (LEN(v10s_INCHG.NewCntryCD) > 0) THEN '[' + v10s_INCHG.NewCntryCD +'] not found' ELSE NewCntry.Country END as NewCountry,
CASE WHEN (v10s_INCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_INCHG.EventType) > 0) THEN '[' + v10s_INCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.ev_Incorporation_Change
FROM v10s_INCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_INCHG.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_INCHG.EventType = EVENT.EventType
LEFT OUTER JOIN CNTRY as oldCNTRY ON v10s_INCHG.OldCntryCD = oldCNTRY.CntryCD
LEFT OUTER JOIN CNTRY as newCNTRY ON v10s_INCHG.NewCntryCD = newCNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_INCHG.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Issuer_Name_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Issuer_Name_Change')
 drop table ev_Issuer_Name_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_ISCHG.EventID as char(8))),len(v10s_ISCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_ISCHG.Levent,
v10s_ISCHG.EventID,
v10s_ISCHG.AnnounceDate as Created,
v10s_ISCHG.Acttime as Changed,
v10s_ISCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_ISCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ISCHG.EventType) > 0) THEN '[' + v10s_ISCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ISCHG.NameChangeDate,
v10s_ISCHG.IssOldName,
v10s_ISCHG.IssNewName,
v10s_ISCHG.LegalName,
'M' as Choice,
v10s_ISCHG.IschgNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.ev_Issuer_Name_Change
FROM v10s_ISCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_ISCHG.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ISCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_ISCHG.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Class_Action, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Class_Action')
 drop table ev_Class_Action
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_CLACT.EventID as char(8))),len(v10s_CLACT.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_CLACT.Levent,
v10s_CLACT.EventID,
v10s_CLACT.AnnounceDate as Created,
v10s_CLACT.Acttime as Changed,
v10s_CLACT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_CLACT.EffectiveDate,
'M' as Choice,
v10s_CLACT.LawstNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.ev_Class_Action
FROM v10s_CLACT
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_CLACT.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_CLACT.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Liquidation, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Liquidation')
 drop table ev_Liquidation
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+substring('00000'+rtrim(cast(v10s_LIQ.EventID as char(8))),len(v10s_LIQ.EventID),6)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+substring('00000'+rtrim(cast(v10s_LIQ.EventID as char(8))),len(v10s_LIQ.EventID),6)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint)
end as CAref,
v10s_LIQ.Levent,
v10s_LIQ.EventID,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as [Changed],
v10s_LIQ.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
MPAY.Paytype,
MPAY.ActFlag as ActionMPAY,
MPAY.Paydate as LiquidationDate,
'Price' AS RateType,
MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_LIQ.Liquidator,
v10s_LIQ.LiqAdd1 as Address1,
v10s_LIQ.LiqAdd2 as Address2,
v10s_LIQ.LiqAdd3 as Address3,
v10s_LIQ.LiqAdd4 as Address4,
v10s_LIQ.LiqAdd5 as Address5,
v10s_LIQ.LiqAdd6 as Address6,
v10s_LIQ.LiqCity as City,
CASE WHEN (v10s_LIQ.LiqCntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_LIQ.LiqCntryCD) > 0) THEN '[' + v10s_LIQ.LiqCntryCD +'] not found' ELSE Cntry.Country END as Country,
v10s_LIQ.LiqTel as Telephone,
v10s_LIQ.LiqFax as Fax,
v10s_LIQ.LiqEmail as Email,
'M' as Choice,
v10s_LIQ.LiquidationTerms as Terms,
'n/a' as Ratio,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Liquidation
FROM v10s_LIQ
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN CNTRY ON v10s_LIQ.LiqCntryCD = CNTRY.CntryCD
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
where
v10s_LIQ.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_International_Code_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_International_Code_Change')
 drop table ev_International_Code_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ICC.EventID as char(16)))as bigint) as CAref,
v10s_ICC.Levent,
v10s_ICC.EventID,
v10s_ICC.AnnounceDate as Created,
v10s_ICC.Acttime as Changed,
v10s_ICC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_ICC.EffectiveDate,
v10s_ICC.OldISIN,
v10s_ICC.NewISIN,
v10s_ICC.OldUSCode,
v10s_ICC.NewUSCode,
CASE WHEN (v10s_ICC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ICC.EventType) > 0) THEN '[' + v10s_ICC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.ev_International_Code_Change
FROM v10s_ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_ICC.SecID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ICC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_ICC.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go


print " Generating ev_Security_Description_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Security_Description_Change')
 drop table ev_Security_Description_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SCCHG.EventID as char(16)))as bigint) as CAref,
v10s_SCCHG.Levent,
v10s_SCCHG.EventID,
v10s_SCCHG.AnnounceDate as Created,
v10s_SCCHG.Acttime as Changed,
v10s_SCCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_SCCHG.DateofChange,
CASE WHEN (v10s_SCCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SCCHG.EventType) > 0) THEN '[' + v10s_SCCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SCCHG.SecOldName as OldName,
v10s_SCCHG.SecNewName as NewName,
'M' as Choice,
v10s_SCCHG.ScChgNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.ev_Security_Description_Change
FROM v10s_SCCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_SCCHG.SecID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_SCCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_SCCHG.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go


print " Generating ev_Listing_Status_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Listing_Status_Change')
 drop table ev_Listing_Status_Change
use wca
select 
cast(cast(v20c_EV_dSCEXH.Seqnum as char(1))+v20c_EV_dSCEXH.EXCHGID+rtrim(cast(v10s_LSTAT.EventID as char(16)))as bigint) as CAref,
v10s_LSTAT.Levent,
v10s_LSTAT.EventID,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_dSCEXH.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_dSCEXH.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_dSCEXH.ExchgCD,
v20c_EV_dSCEXH.MIC,
v20c_EV_dSCEXH.ExCountry,
v20c_EV_dSCEXH.RegCountry,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_dSCEXH.Listdate,
CASE WHEN (v10s_LSTAT.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LSTAT.EventType) > 0) THEN '[' + v10s_LSTAT.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LSTAT.LStatStatus as ListStatChangedTo,
v10s_LSTAT.NotificationDate,
v10s_LSTAT.EffectiveDate,
'M' as Choice,
v10s_LSTAT.Reason,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Listing_Status_Change
FROM v10s_LSTAT
INNER JOIN v20c_EV_dSCEXH ON v10s_LSTAT.SecID = v20c_EV_dSCEXH.SecID AND v10s_LSTAT.ExchgCD = v20c_EV_dSCEXH.ExchgCD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LSTAT.SecID
LEFT OUTER JOIN EVENT ON v10s_LSTAT.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_LSTAT.Acttime = @Startdate AND v20c_EV_dSCEXH.secid is not null
go

print " Generating ev_Local_Code_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Local_Code_Change')
 drop table ev_Local_Code_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_LCC.EventID as char(16)))as bigint) as CAref,
v10s_LCC.Levent,
v10s_LCC.EventID,
v10s_LCC.AnnounceDate as Created,
v10s_LCC.Acttime as Changed,
v10s_LCC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_LCC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LCC.EventType) > 0) THEN '[' + v10s_LCC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LCC.EffectiveDate,
v10s_LCC.OldLocalCode,
v10s_LCC.NewLocalCode,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.ev_Local_Code_Change
FROM v10s_LCC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LCC.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_LCC.ExchgCD = v20c_EV_SCEXH.ExchgCD
LEFT OUTER JOIN EVENT ON v10s_LCC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_LCC.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Lot_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Lot_Change')
 drop table ev_Lot_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_LTCHG.EventID as char(16)))as bigint) as CAref,
v10s_LTCHG.Levent,
v10s_LTCHG.EventID,
v10s_LTCHG.AnnounceDate as Created,
v10s_LTCHG.Acttime as Changed,
v10s_LTCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_LTCHG.EffectiveDate,
v10s_LTCHG.OldLot as OldLotSize,
v10s_LTCHG.NewLot as NewLotSize,
v10s_LTCHG.OldMinTrdQty as OldMinTradingQuant,
v10s_LTCHG.NewMinTrdgQty as NewMinTradingQuant,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.ev_Lot_Change
FROM v10s_LTCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LTCHG.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_LTCHG.ExchgCD = v20c_EV_SCEXH.ExchgCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_LTCHG.Acttime = @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_New_Listing, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_New_Listing')
 drop table ev_New_Listing
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_NLIST1.EventID as char(16)))as bigint) as CAref,
v10s_NLIST1.Levent,
v10s_NLIST1.EventID,
v10s_NLIST1.AnnounceDate as Created,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_New_Listing
FROM v10s_NLIST1
INNER JOIN v20c_EV_SCEXH ON v10s_NLIST1.ScexhID = v20c_EV_SCEXH.ScexhID
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCEXH.SecID = v20c_EV_SCMST.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
left outer join scmst on v20c_EV_SCEXH.SecID = scmst.secid
left outer JOIN SCEXH ON v10s_NLIST1.ScexhID = SCEXH.ScexhID
LEFT OUTER JOIN sedolseq1 ON SCEXH.SecID = sedolseq1.SecID AND v20c_EV_scexh.ExCountry = sedolseq1.CntryCD 
                                AND v20c_EV_SCEXH.SEQNUM = SEDOLSEQ1.SEQNUM
where
(v10s_NLIST1.Acttime >= @Startdate 
or scmst.Acttime >= @Startdate 
or scexh.Acttime >= @Startdate 
or sedolseq1.Acttime >= @Startdate)
and v10s_NLIST1.AnnounceDate>getdate()-90
AND v20c_EV_SCEXH.secid is not null
go


print " Generating ev_Sedol_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Sedol_Change')
 drop table ev_Sedol_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SDCHG.EventID as char(16)))as bigint) as CAref,
v10s_SDCHG.Levent,
v10s_SDCHG.EventID,
v10s_SDCHG.AnnounceDate as Created,
v10s_SDCHG.Acttime as Changed,
v10s_SDCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_SDCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SDCHG.EventType) > 0) THEN '[' + v10s_SDCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SDCHG.CntryCD as CntryofSedol,
v10s_SDCHG.EffectiveDate,
v10s_SDCHG.OldSEDOL,
v10s_SDCHG.NewSEDOL,
v10s_SDCHG.CntryCD as OldCountry,
v10s_SDCHG.NewCntryCD as NewCountry,
v10s_SDCHG.RCntryCD as OldRegCountry,
v10s_SDCHG.NewRCntryCD as NewRegCountry,
'M' as Choice,
v10s_SDCHG.SdChgNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Sedol_Change
FROM v10s_SDCHG
INNER JOIN v20c_EV_SCMST ON v10s_SDCHG.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_SDCHG.CntryCD = v20c_EV_SCEXH.ExCountry
LEFT OUTER JOIN EVENT ON v10s_SDCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_SDCHG.Acttime = @Startdate 
AND v20c_EV_SCEXH.secid is not null
go

print " Generating ev_Certificate_Exchange, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Certificate_Exchange')
 drop table ev_Certificate_Exchange
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CTX.EventID as char(16)))as bigint) as CAref,
v10s_CTX.Levent,
v10s_CTX.EventID,
v10s_CTX.AnnounceDate as Created,
v10s_CTX.Acttime as Changed,
v10s_CTX.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_CTX.StartDate,
v10s_CTX.EndDate,
CASE WHEN (v10s_CTX.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_CTX.EventType) > 0) THEN '[' + v10s_CTX.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_CTX.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_CTX.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_CTX.SectyCD) > 0) THEN '[' + v10s_CTX.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
v10s_CTX.CtXNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Certificate_Exchange
FROM v10s_CTX
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CTX.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_CTX.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_CTX.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_CTX.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN EVENT ON v10s_CTX.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_CTX.Acttime = @Startdate

go


print " Generating ev_Currency_Redenomination, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Currency_Redenomination')
 drop table ev_Currency_Redenomination
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CURRD.EventID as char(16)))as bigint) as CAref,
v10s_CURRD.Levent,
v10s_CURRD.EventID,
v10s_CURRD.AnnounceDate as Created,
v10s_CURRD.Acttime as Changed,
v10s_CURRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_CURRD.EffectiveDate,
CASE WHEN (v10s_CURRD.OldCurenCD is null) THEN oldCUREN.Currency WHEN (oldCUREN.Currency is null) and (LEN(v10s_CURRD.OldCurenCD) > 0) THEN '[' + v10s_CURRD.OldCurenCD +'] not found' ELSE oldCUREN.Currency END as OldCurrency,
CASE WHEN (v10s_CURRD.NewCurenCD is null) THEN newCUREN.Currency WHEN (newCUREN.Currency is null) and (LEN(v10s_CURRD.NewCurenCD) > 0) THEN '[' + v10s_CURRD.NewCurenCD +'] not found' ELSE newCUREN.Currency END as NewCurrency,
v10s_CURRD.OldParValue,
v10s_CURRD.NewParValue,
CASE WHEN (v10s_CURRD.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_CURRD.EventType) > 0) THEN '[' + v10s_CURRD.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v10s_CURRD.CurRdNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Currency_Redenomination
FROM v10s_CURRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CURRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_CURRD.EventType = EVENT.EventType
LEFT OUTER JOIN CUREN as oldCUREN ON v10s_CURRD.OldCurenCD = oldCUREN.CurenCD
LEFT OUTER JOIN CUREN as newCUREN ON v10s_CURRD.NewCurenCD = newCUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_CURRD.Acttime = @Startdate

go

print " Generating ev_Parvalue_Redenomination, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Parvalue_Redenomination')
 drop table ev_Parvalue_Redenomination
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PVRD.EventID as char(16)))as bigint) as CAref,
v10s_PVRD.Levent,
v10s_PVRD.EventID,
v10s_PVRD.AnnounceDate as Created,
v10s_PVRD.Acttime as Changed,
v10s_PVRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_PVRD.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_PVRD.EventType) > 0) THEN '[' + v10s_PVRD.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_PVRD.EffectiveDate,
CASE WHEN (v10s_PVRD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PVRD.CurenCD) > 0) THEN '[' + v10s_PVRD.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PVRD.OldParValue,
v10s_PVRD.NewParValue,
'M' as Choice,
v10s_PVRD.PvRdNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Parvalue_Redenomination
FROM v10s_PVRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_PVRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_PVRD.EventType = EVENT.EventType
LEFT OUTER JOIN CUREN ON v10s_PVRD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_PVRD.Acttime = @Startdate

go

print " Generating ev_Preference_Conversion, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Preference_Conversion')
 drop table ev_Preference_Conversion
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PRFCN.EventID as char(16)))as bigint) as CAref,
v10s_PRFCN.Levent,
v10s_PRFCN.EventID,
v10s_PRFCN.AnnounceDate as Created,
v10s_PRFCN.Acttime as Changed,
v10s_PRFCN.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_PRFCN.RatioNew +':'+v10s_PRFCN.RatioOld as Ratio,
'Price' AS RateType,
v10s_PRFCN.Price as Rate,
CASE WHEN (v10s_PRFCN.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRFCN.CurenCD) > 0) THEN '[' + v10s_PRFCN.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRFCN.FromDate,
v10s_PRFCN.ToDate,
v10s_PRFCN.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_PRFCN.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_PRFCN.SectyCD) > 0) THEN '[' + v10s_PRFCN.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
CASE WHEN v10s_PRFCN.MandOptFlag = 'M' THEN 'M' WHEN v10s_PRFCN.MandOptFlag = 'V' THEN 'V'ELSE '' END as Choice,
v10s_PRFCN.ConvtNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Preference_Conversion
FROM v10s_PRFCN
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_PRFCN.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_PRFCN.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_PRFCN.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_PRFCN.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN CUREN ON v10s_PRFCN.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_PRFCN.Acttime = @Startdate

go

print " Generating ev_Preference_Redemption, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Preference_Redemption')
 drop table ev_Preference_Redemption
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PRFRD.EventID as char(16)))as bigint) as CAref,
v10s_PRFRD.Levent,
v10s_PRFRD.EventID,
v10s_PRFRD.AnnounceDate as Created,
v10s_PRFRD.Acttime as Changed,
v10s_PRFRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
'Price' AS RateType,
v10s_PRFRD.RedemptionPrice as Rate,
CASE WHEN (v10s_PRFRD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRFRD.CurenCD) > 0) THEN '[' + v10s_PRFRD.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRFRD.RedemptionDate,
CASE WHEN v10s_PRFRD.PartFinal='P' THEN 'Part' WHEN v10s_PRFRD.PartFinal='F' THEN 'Final' ELSE '' END as PartFinal,
CASE WHEN v10s_PRFRD.MandOptFlag = 'M' THEN 'M' ELSE 'V' END as Choice,
v10s_PRFRD.RedmtNotes as Notes,
'n/a' as Ratio,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Preference_Redemption
FROM v10s_PRFRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_PRFRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN CUREN ON v10s_PRFRD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_PRFRD.Acttime = @Startdate

go

print " Generating ev_Security_Reclassification, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Security_Reclassification')
 drop table ev_Security_Reclassification
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SECRC.EventID as char(16)))as bigint) as CAref,
v10s_SECRC.Levent,
v10s_SECRC.EventID,
v10s_SECRC.AnnounceDate as Created,
v10s_SECRC.Acttime as Changed,
v10s_SECRC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_SECRC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SECRC.EventType) > 0) THEN '[' + v10s_SECRC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SECRC.EffectiveDate,
CASE WHEN (v10s_SECRC.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_SECRC.SectyCD) > 0) THEN '[' + v10s_SECRC.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_SECRC.RatioNew +':'+v10s_SECRC.RatioOld as Ratio,
v10s_SECRC.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'M' as Choice,
v10s_SECRC.SecRcNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Security_Reclassification
FROM v10s_SECRC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_SECRC.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_SECRC.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_SECRC.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_SECRC.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN EVENT ON v10s_SECRC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_SECRC.Acttime = @Startdate

go

print " Generating ev_Assimilation, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Assimilation')
 drop table ev_Assimilation
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ASSM.EventID as char(16)))as bigint) as CAref,
v10s_ASSM.Levent,
v10s_ASSM.EventID,
v10s_ASSM.AnnounceDate as Created,
v10s_ASSM.Acttime as Changed,
v10s_ASSM.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_ASSM.AssimilationDate,
CASE WHEN (v10s_ASSM.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_ASSM.SectyCD) > 0) THEN '[' + v10s_ASSM.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_ASSM.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Assimilation
FROM v10s_ASSM
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_ASSM.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_ASSM.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_ASSM.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_ASSM.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_ASSM.Acttime = @Startdate

go

print " Generating ev_Buy_Back, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Buy_Back')
 drop table ev_Buy_Back
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BB.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BB.EventID as char(8))) as bigint)
end as CAref,
v10s_BB.Levent,
v10s_BB.EventID,
v10s_BB.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_BB.Acttime) and (MPAY.Acttime > RD.Acttime) THEN MPAY.Acttime WHEN RD.Acttime > v10s_BB.Acttime THEN RD.Acttime ELSE v10s_BB.Acttime END as [Changed],
v10s_BB.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
'MinMaxPrice' AS RateType,
MPAY.MinPrice +':'+MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
MPAY.MinQlyQty as MinQualifyingQuant,
MPAY.MaxQlyQty as MaxQualifyingQuant,
MPAY.PayDate,
MPAY.MinOfrQty as MinOfferQuant,
MPAY.MaxOfrqty as MaxOfferQuant,
MPAY.TndrStrkPrice as TenderStrikePrice,
MPAY.TndrStrkStep as TenderStrikeStep,
CASE WHEN (v10s_BB.OnOffFlag is null) THEN irOnOffMarket.Lookup WHEN (irOnOffMarket.Lookup is null) and (LEN(v10s_BB.OnOffFlag) > 0) THEN '[' + v10s_BB.OnOffFlag +'] not found' ELSE irOnOffMarket.Lookup END as OnOffMarket,
v10s_BB.StartDate,
v10s_BB.EndDate,
v10s_BB.MinAcpQty as MinAcceptanceQuant,
v10s_BB.MaxAcpQty as MAxAcceptanceQuant,
v10s_BB.BBMinPct as MinPercent,
v10s_BB.BBMaxPct as MaxPercent,
'V' as Choice,
RD.RDNotes,
v10s_BB.BBNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Buy_Back
FROM v10s_BB
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_BB.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_BB.RdID = RD.RdID
LEFT OUTER JOIN MPAY ON v10s_BB.EventID = MPAY.EventID AND v10s_BB.SEvent = MPAY.SEvent
LEFT OUTER JOIN irOnOffMarket ON v10s_BB.OnOffFlag = irOnOffMarket.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_BB.Acttime = @Startdate
or RD.Acttime = @Startdate)

go

print " Generating ev_Call, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Call')
 drop table ev_Call
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CALL.EventID as char(16)))as bigint) as CAref,
v10s_CALL.Levent,
v10s_CALL.EventID,
v10s_CALL.AnnounceDate as Created,
CASE
WHEN (v10s_CALL.Acttime=@StartDate) THEN v10s_CALL.Acttime 
ELSE RD.Acttime END as [Changed],
v10s_CALL.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
v10s_CALL.CallNumber,
CASE WHEN (v10s_CALL.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_CALL.CurenCD) > 0) THEN '[' + v10s_CALL.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_CALL.ToFaceValue,
v10s_CALL.ToPremium,
v10s_CALL.DueDate,
'V' as Choice,
RD.RDNotes,
v10s_CALL.CallNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Call
FROM v10s_CALL
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CALL.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_CALL.RdID = RD.RdID
LEFT OUTER JOIN CUREN ON v10s_CALL.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_CALL.Acttime = @Startdate
or RD.Acttime = @Startdate)

go

print " Generating ev_Capital_Reduction, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Capital_Reduction')
 drop table ev_Capital_Reduction
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CAPRD.EventID as char(16)))as bigint) as CAref,
v10s_CAPRD.Levent,
v10s_CAPRD.EventID,
v10s_CAPRD.AnnounceDate as Created,
CASE
WHEN (v10s_CAPRD.Acttime=@StartDate) THEN v10s_CAPRD.Acttime 
ELSE RD.Acttime END as [Changed],
v10s_CAPRD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN (v10s_CAPRD.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_CAPRD.Fractions) > 0) THEN '[' + v10s_CAPRD.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_CAPRD.EffectiveDate,
v10s_CAPRD.NewRatio +':'+v10s_CAPRD.OldRatio as Ratio,
v10s_CAPRD.PayDate,
v10s_CAPRD.OldParValue,
v10s_CAPRD.NewParValue,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
RD.RDNotes,
v10s_CAPRD.CapRdNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as RateType,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Capital_Reduction
FROM v10s_CAPRD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_CAPRD.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN irFRACTIONS ON v10s_CAPRD.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_CAPRD.Acttime = @Startdate
or RD.Acttime = @Startdate)

go

print " Generating ev_Return_of_Capital, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Return_of_Capital')
 drop table ev_Return_of_Capital
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_RCAP.EventID as char(16)))as bigint) as CAref,
v10s_RCAP.Levent,
v10s_RCAP.EventID,
v10s_RCAP.AnnounceDate as Created,
CASE
WHEN (v10s_RCAP.Acttime=@StartDate) THEN v10s_RCAP.Acttime 
ELSE RD.Acttime END as [Changed],
v10s_RCAP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
'CashBack' AS RateType,
v10s_RCAP.CashBak as Rate,
CASE WHEN (v10s_RCAP.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_RCAP.CurenCD) > 0) THEN '[' + v10s_RCAP.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_RCAP.EffectiveDate,
v10s_RCAP.CSPYDate,
'M' as Choice,
RD.RDNotes,
v10s_RCAP.RCapNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratio
into wca2.dbo.ev_Return_of_Capital
FROM v10s_RCAP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_RCAP.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
LEFT OUTER JOIN CUREN ON v10s_RCAP.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_RCAP.Acttime = @Startdate
or RD.Acttime = @Startdate)

go

print " Generating ev_Takeover, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Takeover')
 drop table ev_Takeover
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_TKOVR.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_TKOVR.EventID as char(8))) as bigint)
end as CAref,
v10s_TKOVR.Levent,
v10s_TKOVR.EventID,
v10s_TKOVR.AnnounceDate as Created,
CASE
WHEN (v10s_TKOVR.Acttime=@StartDate) THEN v10s_TKOVR.Acttime 
WHEN (MPAY.Acttime=@StartDate) THEN MPAY.Acttime 
ELSE RD.Acttime END as [Changed],
v10s_TKOVR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
'MinPriceMaxPrice' AS RateType,
MPAY.MinPrice +':'+MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
MPAY.PayDate,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.MinQlyQty as MinQualifyingQuant,
MPAY.MaxQlyQty as MaxQualifyingQuant,
MPAY.MinOfrQty as MinOfferQuant,
MPAY.MaxOfrqty as MaxOfferQuant,
MPAY.TndrStrkPrice as TenderStrikePrice,
MPAY.TndrStrkStep as TenderStrikeStep,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
CASE WHEN (v10s_TKOVR.Hostile is null) THEN irHostile.Lookup WHEN (irHostile.Lookup is null) and (LEN(v10s_TKOVR.Hostile) > 0) THEN '[' + v10s_TKOVR.Hostile +'] not found' ELSE irHostile.Lookup END as TakeoverType,
CASE WHEN (v10s_TKOVR.TkovrStatus is null) THEN irTkovrStat.Lookup WHEN (irTkovrStat.Lookup is null) and (LEN(v10s_TKOVR.TkovrStatus) > 0) THEN '[' + v10s_TKOVR.TkovrStatus +'] not found' ELSE irTkovrStat.Lookup END as TakeoverStatus,
v10s_TKOVR.OfferorIssID,
v10s_TKOVR.OfferorName,
v10s_TKOVR.OpenDate,
v10s_TKOVR.CloseDate,
v10s_TKOVR.PreOfferQty,
v10s_TKOVR.PreOfferPercent,
v10s_TKOVR.TargetQuantity,
v10s_TKOVR.TargetPercent,
v10s_TKOVR.UnconditionalDate,
v10s_TKOVR.CmAcqDate as CompulsoryAcqDate,
v10s_TKOVR.MinAcpQty as MinAcceptanceQuant,
v10s_TKOVR.MaxAcpQty as MaxAcceptanceQuant,
'M' as Choice,
RD.RDNotes,
v10s_TKOVR.TkovrNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Takeover
FROM v10s_TKOVR
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_TKOVR.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_TKOVR.RdID = RD.RdID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID AND v10s_TKOVR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irHOSTILE ON v10s_TKOVR.Hostile = irHOSTILE.Code
LEFT OUTER JOIN irTKOVRSTAT ON v10s_TKOVR.TkovrStatus = irTKOVRSTAT.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_TKOVR.Acttime = @Startdate
or RD.Acttime = @Startdate
or MPAY.Acttime = @Startdate


print " Generating ev_Arrangement, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Arrangement')
 drop table ev_Arrangement
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ARR.EventID as char(16)))as bigint) as CAref,
v10s_ARR.Levent,
v10s_ARR.EventID,
v10s_ARR.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_ARR.Acttime=@StartDate) THEN v10s_ARR.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_ARR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
'M' as Choice,
RD.RDNotes,
v10s_ARR.ARRNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Arrangement
FROM v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_ARR.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Bonus, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Bonus')
 drop table ev_Bonus
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BON.EventID as char(16)))as bigint) as CAref,
v10s_BON.Levent,
v10s_BON.EventID,
v10s_BON.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_BON.Acttime=@StartDate) THEN v10s_BON.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_BON.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_BON.RatioNew +':'+v10s_BON.RatioOld as Ratio,
CASE WHEN (v10s_BON.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_BON.Fractions) > 0) THEN '[' + v10s_BON.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_BON.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_BON.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_BON.SectyCD) > 0) THEN '[' + v10s_BON.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_BON.LapsedPremium,
'M' as Choice,
RD.RDNotes,
v10s_BON.BonNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Bonus
FROM v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_BON.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_BON.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_BON.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SECTY ON v10s_BON.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_BON.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Bonus_Rights, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Bonus_Rights')
 drop table ev_Bonus_Rights
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_BR.EventID as char(16)))as bigint) as CAref,
v10s_BR.Levent,
v10s_BR.EventID,
v10s_BR.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_BR.Acttime=@StartDate) THEN v10s_BR.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_BR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_BR.RatioNew +':'+v10s_BR.RatioOld as Ratio,
'IssuePrice' AS RateType,
v10s_BR.IssuePrice as Rate,
CASE WHEN (v10s_BR.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_BR.CurenCD) > 0) THEN '[' + v10s_BR.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_BR.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_BR.SectyCD) > 0) THEN '[' + v10s_BR.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
CASE WHEN (v10s_BR.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_BR.Fractions) > 0) THEN '[' + v10s_BR.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_BR.StartSubscription,
v10s_BR.EndSubscription,
v10s_BR.SplitDate,
v10s_BR.StartTrade,
v10s_BR.EndTrade,
v10s_BR.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
v10s_BR.TraSecID as TradeableSecID,
v09b_SCMST.ISIN as TradeableIsin,
CASE WHEN v10s_BR.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
'M' as Choice,
RD.RDNotes,
v10s_BR.BRNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Bonus_Rights
FROM v10s_BR
INNER JOIN RD ON RD.RdID = v10s_BR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BR' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_BR.ResSecID = SCMST.SecID
LEFT OUTER JOIN v09b_SCMST ON v10s_BR.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_BR.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_BR.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SECTY ON v10s_BR.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN CUREN ON v10s_BR.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_BR.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Consolidation, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Consolidation')
 drop table ev_Consolidation
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_CONSD.EventID as char(16)))as bigint) as CAref,
v10s_CONSD.Levent,
v10s_CONSD.EventID,
v10s_CONSD.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_CONSD.Acttime=@StartDate) THEN v10s_CONSD.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_CONSD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_CONSD.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_CONSD.CurenCD) > 0) THEN '[' + v10s_CONSD.CurenCD +'] not found' ELSE CUREN.Currency END as CONSDCurrency,
v10s_CONSD.OldParValue,
v10s_CONSD.NewParValue,
v10s_CONSD.NewRatio +':'+v10s_CONSD.OldRatio as Ratio,
CASE WHEN (v10s_CONSD.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_CONSD.Fractions) > 0) THEN '[' + v10s_CONSD.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
RD.RDNotes,
v10s_CONSD.ConsdNotes as Notes,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Consolidation
FROM v10s_CONSD
INNER JOIN RD ON RD.RdID = v10s_CONSD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN irFRACTIONS ON v10s_CONSD.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_CONSD.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_CONSD.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Demerger, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Demerger')
 drop table ev_Demerger
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DMRGR.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DMRGR.EventID as char(8))) as bigint)
end as CAref,
v10s_DMRGR.Levent,
v10s_DMRGR.EventID,
v10s_DMRGR.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_DMRGR.Acttime=@StartDate) THEN v10s_DMRGR.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
WHEN (MPAY.Acttime=@StartDate) THEN MPAY.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_DMRGR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
v10s_DMRGR.EffectiveDate,
'M' as Choice,
RD.RDNotes,
v10s_DMRGR.DMrgrNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as RateType,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Demerger
FROM v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DMRGR.EventID = MPAY.EventID AND v10s_DMRGR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_DMRGR.Acttime = @Startdate
or RD.Acttime = @Startdate
or MPAY.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Distribution, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Distribution') drop table ev_Distribution
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIST.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIST.EventID as char(8))) as bigint)
end as CAref,
v10s_DIST.Levent,
v10s_DIST.EventID,
v10s_DIST.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_DIST.Acttime=@StartDate) THEN v10s_DIST.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
WHEN (MPAY.Acttime=@StartDate) THEN MPAY.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_DIST.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'M' as Choice,
RD.RDNotes,
v10s_DIST.DistNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Distribution
FROM v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DIST.EventID = MPAY.EventID AND v10s_DIST.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_DIST.Acttime = @Startdate
or RD.Acttime = @Startdate
or MPAY.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Divestment, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Divestment')
 drop table ev_Divestment
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DVST.EventID as char(16)))as bigint) as CAref,
v10s_DVST.Levent,
v10s_DVST.EventID,
v10s_DVST.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_DVST.Acttime=@StartDate) THEN v10s_DVST.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_DVST.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_DVST.RatioNew +':'+v10s_DVST.RatioOld as Ratio,
'Min:MaxPrice' AS RateType,
v10s_DVST.MinPrice +':'+v10s_DVST.MaxPrice as Rate,
CASE WHEN (v10s_DVST.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_DVST.CurenCD) > 0) THEN '[' + v10s_DVST.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_DVST.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_DVST.Fractions) > 0) THEN '[' + v10s_DVST.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_DVST.StartSubscription,
v10s_DVST.EndSubscription,
v10s_DVST.TndrStrkPrice as TenderStrikePrice,
v10s_DVST.TndrPriceStep as TenderPriceStep,
v10s_DVST.MinQlyQty as MinQualifyingQuant,
v10s_DVST.MaxQlyQty as MaxQualifyingQuant,
v10s_DVST.MinAcpQty as MinAcceptanceQuant,
v10s_DVST.MaxAcpQty as MaxAcceptanceQuant,
v10s_DVST.TraSecID as TradeableSecID,
v09b_SCMST.Isin as TradeableIsin,
v10s_DVST.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_DVST.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_DVST.SectyCD) > 0) THEN '[' + v10s_DVST.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
RD.RDNotes,
v10s_DVST.DvstNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Divestment
FROM v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_DVST.ResSecID = SCMST.SecID
LEFT OUTER JOIN v09b_SCMST ON v10s_DVST.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_DVST.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_DVST.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON v10s_DVST.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_DVST.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_DVST.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Entitlement, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Entitlement')
 drop table ev_Entitlement
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ENT.EventID as char(16)))as bigint) as CAref,
v10s_ENT.Levent,
v10s_ENT.EventID,
v10s_ENT.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_ENT.Acttime=@StartDate) THEN v10s_ENT.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_ENT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_ENT.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_ENT.SectyCD) > 0) THEN '[' + v10s_ENT.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_ENT.RatioNew +':'+v10s_ENT.RatioOld as Ratio,
'IssuePrice' AS RateType,
v10s_ENT.EntIssuePrice as Rate,
CASE WHEN (v10s_ENT.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_ENT.CurenCD) > 0) THEN '[' + v10s_ENT.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_ENT.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_ENT.Fractions) > 0) THEN '[' + v10s_ENT.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_ENT.EntIssuePrice as IssuePrice,
v10s_ENT.StartSubscription,
v10s_ENT.EndSubscription,
v10s_ENT.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN v10s_ENT.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
'M' as Choice,
RD.RDNotes,
v10s_ENT.EntNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Entitlement
FROM v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_ENT.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_ENT.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_ENT.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_ENT.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_ENT.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_ENT.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Subdivision, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Subdivision')
 drop table ev_Subdivision
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SD.EventID as char(16)))as bigint) as CAref,
v10s_SD.Levent,
v10s_SD.EventID,
v10s_SD.AnnounceDate as Created,
CASE
WHEN (RD.Acttime=@StartDate) THEN RD.Acttime 
WHEN (v10s_SD.Acttime=@StartDate) THEN v10s_SD.Acttime 
WHEN (PEXDT.Acttime=@StartDate) THEN PEXDT.Acttime 
WHEN (SDCHG.Acttime=@StartDate) THEN SDCHG.Acttime 
WHEN (LCC.Acttime=@StartDate) THEN LCC.Acttime 
WHEN (ICC.Acttime=@StartDate) THEN ICC.Acttime 
ELSE EXDT.Acttime END as [Changed],
v10s_SD.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
CASE WHEN (ICC.RelEventID is null OR ICC.OldISIN = '') THEN v20c_EV_SCMST.ISIN ELSE ICC.OldISIN END as Isin,
CASE WHEN (ICC.RelEventID is null OR ICC.OldUSCODE = '') THEN v20c_EV_SCMST.USCODE ELSE ICC.OldUSCODE END as UScode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_EV_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_EV_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_SD.NewRatio +':'+v10s_SD.OldRatio as Ratio,
CASE WHEN (v10s_SD.OldCurenCD is null) THEN oldCUREN.Currency WHEN (oldCUREN.Currency is null) and (LEN(v10s_SD.OldCurenCD) > 0) THEN '[' + v10s_SD.OldCurenCD +'] not found' ELSE oldCUREN.Currency END as OldCurrency,
CASE WHEN (v10s_SD.NewCurenCD is null) THEN newCUREN.Currency WHEN (newCUREN.Currency is null) and (LEN(v10s_SD.NewCurenCD) > 0) THEN '[' + v10s_SD.NewCurenCD +'] not found' ELSE newCUREN.Currency END as NewCurrency,
v10s_SD.OldParValue,
v10s_SD.NewParValue,
ICC.NewIsin,
ICC.NewUscode,
SDCHG.NewSedol,
LCC.NewLocalcode,
CASE WHEN (ICC.Acttime is not null) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime ELSE SDCHG.Acttime END as NewCodeDate,
'M' as Choice,
RD.RDNotes,
v10s_SD.SDNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Subdivision
FROM v10s_SD
INNER JOIN RD ON RD.RdID = v10s_SD.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_EV_SCEXH.ExCountry = SDCHG.CntryCD AND v20c_EV_SCEXH.RegCountry = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_EV_SCEXH.ExchgCD = LCC.ExchgCD
LEFT OUTER JOIN CUREN as oldCUREN ON v10s_SD.OldCurenCD = oldCUREN.CurenCD
LEFT OUTER JOIN CUREN as newCUREN ON v10s_SD.NewCurenCD = newCUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_SD.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or SDCHG.Acttime = @Startdate
or LCC.Acttime = @Startdate
or ICC.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Merger, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Merger')
 drop table ev_Merger
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_MRGR.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_MRGR.EventID as char(8))) as bigint)
end as CAref,
v10s_MRGR.Levent,
v10s_MRGR.EventID,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
v10s_MRGR.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (MPAY.ActFlag is null) THEN irActionMPAY.Lookup WHEN (irActionMPAY.Lookup is null) and (LEN(MPAY.Actflag) > 0) THEN '[' + MPAY.Actflag +'] not found' ELSE irActionMPAY.Lookup END as PayOptionRecordFlag,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
CASE WHEN (MPAY.Paytype is null) THEN irPaytype.Lookup+'['+MPAY.actflag+']' WHEN (irPaytype.Lookup is null) and (LEN(MPAY.Paytype) > 0) THEN '[' + MPAY.Paytype +'] not found'+'['+MPAY.actflag+']' ELSE irPaytype.Lookup+'['+MPAY.actflag+']' END as Paytype,
MPAY.RatioNew +':'+MPAY.RatioOld as Ratio,
CASE WHEN (MPAY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(MPAY.Fractions) > 0) THEN '[' + MPAY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
MPAY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (MPAY.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(MPAY.SectyCD) > 0) THEN '[' + MPAY.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
MPAY.MinPrice,
v10s_MRGR.EffectiveDate,
v10s_MRGR.AppointedDate,
CASE WHEN (v10s_MRGR.MrgrStatus is null) THEN irMrgrStat.Lookup WHEN (irMrgrStat.Lookup is null) and (LEN(v10s_MRGR.MrgrStatus) > 0) THEN '[' + v10s_MRGR.MrgrStatus +'] not found' ELSE irMrgrStat.Lookup END as MergerStatus,
v10s_MRGR.Companies,
v10s_MRGR.ApprovalStatus,
'M' as Choice,
RD.RDNotes,
v10s_MRGR.MRGRTerms as Terms,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Merger
FROM v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID AND v10s_MRGR.SEvent = MPAY.SEvent
LEFT OUTER JOIN SCMST ON MPAY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON MPAY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON MPAY.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON MPAY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irPAYTYPE ON MPAY.Paytype = irPAYTYPE.Code
LEFT OUTER JOIN irMRGRSTAT ON v10s_MRGR.MrgrStatus = irMRGRSTAT.Code
LEFT OUTER JOIN irACTION as irACTIONMPAY ON MPAY.Actflag = irACTIONMPAY.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_MRGR.Acttime = @Startdate
or RD.Acttime = @Startdate
or MPAY.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Rights, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Rights')
 drop table ev_Rights
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_RTS.EventID as char(16)))as bigint) as CAref,
v10s_RTS.Levent,
v10s_RTS.EventID,
v10s_RTS.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_RTS.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_RTS.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_RTS.Acttime) THEN PEXDT.Acttime ELSE v10s_RTS.Acttime END as [Changed],
v10s_RTS.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_RTS.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_RTS.SectyCD) > 0) THEN '[' + v10s_RTS.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
v10s_RTS.RatioNew +':'+v10s_RTS.RatioOld as Ratio,
'IssuePrice' as RateType,
v10s_RTS.IssuePrice as Rate,
CASE WHEN (v10s_RTS.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_RTS.CurenCD) > 0) THEN '[' + v10s_RTS.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN (v10s_RTS.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_RTS.Fractions) > 0) THEN '[' + v10s_RTS.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_RTS.StartSubscription,
v10s_RTS.EndSubscription,
v10s_RTS.SplitDate,
v10s_RTS.StartTrade,
v10s_RTS.EndTrade,
v10s_RTS.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
v10s_RTS.TraSecID as TradeableSecID,
v09b_SCMST.ISIN as TradeableIsin,
v10s_RTS.LapsedPremium,
CASE WHEN v10s_RTS.OverSubscription = 'T' THEN 'Yes' ELSE 'No' END as OverSubscription,
'V' as Choice,
RD.RDNotes,
v10s_RTS.RTSNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Rights
FROM v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'RTS' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'RTS' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_RTS.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_RTS.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN v09b_SCMST ON v10s_RTS.TraSecID = v09b_SCMST.SecID
LEFT OUTER JOIN irFRACTIONS ON v10s_RTS.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_RTS.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_RTS.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_RTS.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Preferential_Offer, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Preferential_Offer')
 drop table ev_Preferential_Offer
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PRF.EventID as char(16)))as bigint) as CAref,
v10s_PRF.Levent,
v10s_PRF.EventID,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
v10s_PRF.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_PRF.OffereeIssID,
v10s_PRF.OffereeName,
CASE WHEN (v10s_PRF.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_PRF.SectyCD) > 0) THEN '[' + v10s_PRF.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as OfferedSecType,
CASE WHEN (v10s_PRF.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_PRF.Fractions) > 0) THEN '[' + v10s_PRF.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_PRF.RatioNew +':'+v10s_PRF.RatioOld as Ratio,
'MinPrice:MaxPrice' AS RateType,
v10s_PRF.MinPrice +':'+v10s_PRF.MaxPrice as Rate,
CASE WHEN (v10s_PRF.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PRF.CurenCD) > 0) THEN '[' + v10s_PRF.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PRF.StartSubscription,
v10s_PRF.EndSubscription,
v10s_PRF.TndrStrkPrice,
v10s_PRF.TndrPriceStep,
v10s_PRF.MinQlyQty as MinQualifyingQuant,
v10s_PRF.MaxQlyQty as MaxQualifyingQuant,
v10s_PRF.MinAcpQty as MinAcceptanceQuant,
v10s_PRF.MaxAcpQty as MaxAcceptanceQuant,
v10s_PRF.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
'V' as Choice,
RD.RDNotes,
v10s_PRF.PRFNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Preferential_Offer
FROM v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_PRF.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_PRF.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON v10s_PRF.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN CUREN ON v10s_PRF.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SECTY ON v10s_PRF.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_PRF.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Purchase_Offer, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Purchase_Offer')
 drop table ev_Purchase_Offer
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_PO.EventID as char(16)))as bigint) as CAref,
v10s_PO.Levent,
v10s_PO.EventID,
v10s_PO.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PO.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PO.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PO.Acttime) THEN PEXDT.Acttime ELSE v10s_PO.Acttime END as [Changed],
v10s_PO.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
'MinMaxPrice' AS RateType,
v10s_PO.MinPrice +':'+v10s_PO.MaxPrice as Rate,
CASE WHEN (v10s_PO.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_PO.CurenCD) > 0) THEN '[' + v10s_PO.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_PO.NegotiatedPrice,
v10s_PO.OfferOpens,
v10s_PO.OfferCloses,
v10s_PO.POMinPercent as MinPercent,
v10s_PO.POMaxPercent as MaxPercent,
v10s_PO.MinOfrQty as MinOfferQuant,
v10s_PO.MaxOfrqty as MaxOfferQuant,
v10s_PO.TndrStrkPrice as TenderStrikePrice,
v10s_PO.TndrPriceStep as TenderPriceStep,
v10s_PO.MinQlyQty as MinQualifyingQuant,
v10s_PO.MaxQlyQty as MaxQualifyingQuant,
v10s_PO.MinAcpQty as MinAcceptanceQuant,
v10s_PO.MaxAcpQty as MaxAcceptanceQuant,
'V' as Choice,
RD.RDNotes,
v10s_PO.PONotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratio
into wca2.dbo.ev_Purchase_Offer
FROM v10s_PO
INNER JOIN RD ON RD.RdID = v10s_PO.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PO' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PO' = PEXDT.EventType
LEFT OUTER JOIN CUREN ON v10s_PO.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_PO.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Dividend_Reinvestment_Plan, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Dividend_Reinvestment_Plan')
 drop table ev_Dividend_Reinvestment_Plan
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DRIP.EventID as char(16)))as bigint) as CAref,
v10s_DRIP.Levent,
v10s_DRIP.EventID,
v10s_DRIP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DRIP.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DRIP.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DRIP.Acttime) THEN PEXDT.Acttime ELSE v10s_DRIP.Acttime END as [Changed],
v10s_DRIP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
DRIP.DripPayDate,
CASE WHEN v10s_DRIP.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_DRIP.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_DRIP.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_DRIP.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_DRIP.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_DRIP.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_DRIP.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_DRIP.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_DRIP.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_DRIP.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_DRIP.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_DRIP.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_DRIP.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_DRIP.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_DRIP.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_DRIP.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN (DRIP.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(DRIP.CntryCD) > 0) THEN '[' + DRIP.CntryCD +'] not found' ELSE Cntry.Country END as Country,
DRIP.DripLastdate,
DRIP.DripReinvPrice,
'V' as Choice,
v10s_DRIP.DIVNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Dividend_Reinvestment_Plan
FROM v10s_DRIP
INNER JOIN DRIP ON v10s_DRIP.EventID = DRIP.DivID
INNER JOIN RD ON RD.RdID = v10s_DRIP.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID and v20c_EV_SCEXH.ExCountry = DRIP.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN CNTRY ON DRIP.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_DRIP.Acttime = @Startdate
or DRIP.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Franking, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Franking')
 drop table ev_Franking
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_FRANK.EventID as char(16)))as bigint) as CAref,
v10s_FRANK.Levent,
v10s_FRANK.EventID,
v10s_FRANK.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_FRANK.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_FRANK.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_FRANK.Acttime) THEN PEXDT.Acttime ELSE v10s_FRANK.Acttime END as [Changed],
v10s_FRANK.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_FRANK.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_FRANK.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_FRANK.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_FRANK.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_FRANK.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_FRANK.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_FRANK.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_FRANK.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_FRANK.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_FRANK.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_FRANK.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_FRANK.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_FRANK.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_FRANK.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_FRANK.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DivPeriod,
CASE WHEN v10s_FRANK.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN (FRANK.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(FRANK.CntryCD) > 0) THEN '[' + FRANK.CntryCD +'] not found' ELSE Cntry.Country END as Country,
CASE WHEN FRANK.Frankflag = 'F' THEN 'Fully Franked'
     WHEN FRANK.Frankflag = 'P' THEN 'Partially Franked'
     WHEN FRANK.Frankflag = 'U' THEN 'Unfranked'
ELSE '' END as Franking,
FRANK.FrankDiv as AmountFranked,
FRANK.UnfrankDiv as AmountUnfranked,
'M' as Choice,
v10s_FRANK.DIVNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.ev_Franking
FROM v10s_FRANK
INNER JOIN FRANK ON v10s_FRANK.EventID = FRANK.DivID
INNER JOIN RD ON RD.RdID = v10s_FRANK.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID and v20c_EV_SCEXH.ExCountry = FRANK.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN CNTRY ON FRANK.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_FRANK.Acttime = @Startdate
or FRANK.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

print " Generating ev_Security_Swap, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select acttime from wca.dbo.tbl_Opslog where feeddate = '2009/11/13' and SEQ = 3)
if exists (select * from sysobjects where name = 'ev_Security_Swap')
 drop table ev_Security_Swap
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SCSWP.EventID as char(16)))as bigint) as CAref,
v10s_SCSWP.Levent,
v10s_SCSWP.EventID,
v10s_SCSWP.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SCSWP.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SCSWP.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SCSWP.Acttime) THEN PEXDT.Acttime ELSE v10s_SCSWP.Acttime END as [Changed],
v10s_SCSWP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_SCSWP.NewRatio +':'+v10s_SCSWP.OldRatio as Ratio,
CASE WHEN (v10s_SCSWP.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(v10s_SCSWP.Fractions) > 0) THEN '[' + v10s_SCSWP.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
v10s_SCSWP.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
CASE WHEN (v10s_SCSWP.SectyCD is null) THEN Secty.SecurityDescriptor WHEN (Secty.SecurityDescriptor is null) and (LEN(v10s_SCSWP.SectyCD) > 0) THEN '[' + v10s_SCSWP.SectyCD +'] not found' ELSE Secty.SecurityDescriptor END as ResSecType,
'M' as Choice,
RD.RDNotes,
v10s_SCSWP.SCSWPNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID,
'n/a' as Ratetype,
'n/a' as Rate,
'n/a' as Currency
into wca2.dbo.ev_Security_Swap
FROM v10s_SCSWP
INNER JOIN RD ON RD.RdID = v10s_SCSWP.EventID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SCSWP' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SCSWP' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_SCSWP.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_SCSWP.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN SECTY ON v10s_SCSWP.SectyCD = SECTY.SectyCD
LEFT OUTER JOIN irFRACTIONS ON v10s_SCSWP.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_SCSWP.Acttime = @Startdate
or RD.Acttime = @Startdate
or EXDT.Acttime = @Startdate
or PEXDT.Acttime = @Startdate)

go

