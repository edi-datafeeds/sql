
use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Dividend')
	drop table wca2.dbo.t620_Dividend
use wca
select *
into wca2.dbo.t620_Dividend
FROM v54f_620_Dividend
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Company_Meeting')
	drop table wca2.dbo.t620_Company_Meeting
use wca
select *
into wca2.dbo.t620_Company_Meeting
FROM v50f_620_Company_Meeting
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Call')
	drop table wca2.dbo.t620_Call
use wca
select *
into wca2.dbo.t620_Call
FROM v53f_620_Call
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Liquidation')
	drop table wca2.dbo.t620_Liquidation
use wca
select *
into wca2.dbo.t620_Liquidation
FROM v50f_620_Liquidation
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Certificate_Exchange')
	drop table wca2.dbo.t620_Certificate_Exchange
use wca
select *
into wca2.dbo.t620_Certificate_Exchange
FROM v51f_620_Certificate_Exchange
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_International_Code_Change')
	drop table wca2.dbo.t620_International_Code_Change
use wca
select *
into wca2.dbo.t620_International_Code_Change
FROM v51f_620_International_Code_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Conversion_Terms')
	drop table wca2.dbo.t620_Conversion_Terms
use wca
select *
into wca2.dbo.t620_Conversion_Terms
FROM v51f_620_Conversion_Terms
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Conversion_Terms_Change')
	drop table wca2.dbo.t620_Conversion_Terms_Change
use wca
select *
into wca2.dbo.t620_Conversion_Terms_Change
FROM v51f_620_Conversion_Terms_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Redemption_Terms')
	drop table wca2.dbo.t620_Redemption_Terms
use wca
select *
into wca2.dbo.t620_Redemption_Terms
FROM v51f_620_Redemption_Terms
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Security_Reclassification')
	drop table wca2.dbo.t620_Security_Reclassification
use wca
select *
into wca2.dbo.t620_Security_Reclassification
FROM v51f_620_Security_Reclassification
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Lot_Change')
	drop table wca2.dbo.t620_Lot_Change
use wca
select *
into wca2.dbo.t620_Lot_Change
FROM v52f_620_Lot_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Sedol_Change')
	drop table wca2.dbo.t620_Sedol_Change
use wca
select *
into wca2.dbo.t620_Sedol_Change
FROM v52f_620_Sedol_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Buy_Back')
	drop table wca2.dbo.t620_Buy_Back
use wca
select *
into wca2.dbo.t620_Buy_Back
FROM v53f_620_Buy_Back
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Capital_Reduction')
	drop table wca2.dbo.t620_Capital_Reduction
use wca
select *
into wca2.dbo.t620_Capital_Reduction
FROM v53f_620_Capital_Reduction
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Takeover')
	drop table wca2.dbo.t620_Takeover
use wca
select *
into wca2.dbo.t620_Takeover
FROM v53f_620_Takeover
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Arrangement')
	drop table wca2.dbo.t620_Arrangement
use wca
select *
into wca2.dbo.t620_Arrangement
FROM v54f_620_Arrangement
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Bonus')
	drop table wca2.dbo.t620_Bonus
use wca
select *
into wca2.dbo.t620_Bonus
FROM v54f_620_Bonus
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Bonus_Rights')
	drop table wca2.dbo.t620_Bonus_Rights
use wca
select *
into wca2.dbo.t620_Bonus_Rights
FROM v54f_620_Bonus_Rights
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Consolidation')
	drop table wca2.dbo.t620_Consolidation
use wca
select *
into wca2.dbo.t620_Consolidation
FROM v54f_620_Consolidation
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Demerger')
	drop table wca2.dbo.t620_Demerger
use wca
select *
into wca2.dbo.t620_Demerger
FROM v54f_620_Demerger
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Distribution')
	drop table wca2.dbo.t620_Distribution
use wca
select *
into wca2.dbo.t620_Distribution
FROM v54f_620_Distribution
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Divestment')
	drop table wca2.dbo.t620_Divestment
use wca
select *
into wca2.dbo.t620_Divestment
FROM v54f_620_Divestment
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Entitlement')
	drop table wca2.dbo.t620_Entitlement
use wca
select *
into wca2.dbo.t620_Entitlement
FROM v54f_620_Entitlement
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Merger')
	drop table wca2.dbo.t620_Merger
use wca
select *
into wca2.dbo.t620_Merger
FROM v54f_620_Merger
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Preferential_Offer')
	drop table wca2.dbo.t620_Preferential_Offer
use wca
select *
into wca2.dbo.t620_Preferential_Offer
FROM v54f_620_Preferential_Offer
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Purchase_Offer')
	drop table wca2.dbo.t620_Purchase_Offer
use wca
select *
into wca2.dbo.t620_Purchase_Offer
FROM v54f_620_Purchase_Offer
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Rights ')
	drop table wca2.dbo.t620_Rights 
use wca
select *
into wca2.dbo.t620_Rights 
FROM v54f_620_Rights
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)


go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Security_Swap')
	drop table wca2.dbo.t620_Security_Swap
use wca
select *
into wca2.dbo.t620_Security_Swap
FROM v54f_620_Security_Swap
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Subdivision')
	drop table wca2.dbo.t620_Subdivision
use wca
select *
into wca2.dbo.t620_Subdivision
FROM v54f_620_Subdivision
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Bankruptcy ')
	drop table wca2.dbo.t620_Bankruptcy 
use wca
select *
into wca2.dbo.t620_Bankruptcy 
FROM v50f_620_Bankruptcy 
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Financial_Year_Change')
	drop table wca2.dbo.t620_Financial_Year_Change
use wca
select *
into wca2.dbo.t620_Financial_Year_Change
FROM v50f_620_Financial_Year_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Incorporation_Change')
	drop table wca2.dbo.t620_Incorporation_Change
use wca
select *
into wca2.dbo.t620_Incorporation_Change
FROM v50f_620_Incorporation_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Issuer_Name_change')
	drop table wca2.dbo.t620_Issuer_Name_change
use wca
select *
into wca2.dbo.t620_Issuer_Name_change
FROM v50f_620_Issuer_Name_change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Class_Action')
	drop table wca2.dbo.t620_Class_Action
use wca
select *
into wca2.dbo.t620_Class_Action
FROM v50f_620_Class_Action
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Security_Description_Change')
	drop table wca2.dbo.t620_Security_Description_Change
use wca
select *
into wca2.dbo.t620_Security_Description_Change
FROM v51f_620_Security_Description_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Assimilation')
	drop table wca2.dbo.t620_Assimilation
use wca
select *
into wca2.dbo.t620_Assimilation
FROM v52f_620_Assimilation
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Listing_Status_Change')
	drop table wca2.dbo.t620_Listing_Status_Change
use wca
select *
into wca2.dbo.t620_Listing_Status_Change
FROM v52f_620_Listing_Status_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Local_Code_Change')
	drop table wca2.dbo.t620_Local_Code_Change
use wca
select *
into wca2.dbo.t620_Local_Code_Change
FROM v52f_620_Local_Code_Change
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_New_Listing')
	drop table wca2.dbo.t620_New_Listing
use wca
select *
into wca2.dbo.t620_New_Listing
FROM v52f_620_New_Listing
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Announcement')
	drop table wca2.dbo.t620_Announcement
use wca
select *
into wca2.dbo.t620_Announcement
FROM v50f_620_Announcement
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Parvalue_Redenomination')
	drop table wca2.dbo.t620_Parvalue_Redenomination
use wca
select *
into wca2.dbo.t620_Parvalue_Redenomination
FROM v51f_620_Parvalue_Redenomination
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Currency_Redenomination')
	drop table wca2.dbo.t620_Currency_Redenomination
use wca
select *
into wca2.dbo.t620_Currency_Redenomination
FROM v51f_620_Currency_Redenomination
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Return_of_Capital')
	drop table wca2.dbo.t620_Return_of_Capital
use wca
select *
into wca2.dbo.t620_Return_of_Capital
FROM v53f_620_Return_of_Capital
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Dividend_Reinvestment_Plan')
	drop table wca2.dbo.t620_Dividend_Reinvestment_Plan
use wca
select *
into wca2.dbo.t620_Dividend_Reinvestment_Plan
FROM v54f_620_Dividend_Reinvestment_Plan
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(feeddate) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't620_Franking')
	drop table wca2.dbo.t620_Franking
use wca
select *
into wca2.dbo.t620_Franking
FROM v54f_620_Franking
where changed >= @Startdate and changed <= (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)

go
