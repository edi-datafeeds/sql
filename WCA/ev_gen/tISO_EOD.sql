print " Generating tISO_SPLF, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_SPLF')
 drop table tISO_SPLF

use wca
Select 
v10s_SD.EventID,
v20c_ISO_SCMST.SecID,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime)
and (RD.Acttime > EXDT.Acttime or  EXDT.Acttime is null) 
and (RD.Acttime > PEXDT.Acttime or PEXDT.Acttime is null)
and (RD.Acttime > ICC.Acttime or ICC.Acttime is null)
and (RD.Acttime > SDCHG.Acttime or SDCHG.Acttime is null) 
THEN RD.Acttime
 WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime)
 and (EXDT.Acttime > PEXDT.Acttime) and (EXDT.Acttime > ICC.Acttime)
 and (EXDT.Acttime > SDCHG.Acttime) THEN EXDT.Acttime 
WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) 
and (PEXDT.Acttime > ICC.Acttime) and (PEXDT.Acttime > SDCHG.Acttime)
 THEN PEXDT.Acttime 
WHEN (ICC.Acttime is not null) and (ICC.Acttime > v10s_SD.Acttime)
 and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime 
WHEN (SDCHG.Acttime is not null) and (SDCHG.Acttime > v10s_SD.Acttime) THEN SDCHG.Acttime
 ELSE v10s_SD.Acttime END as [Changed],
CASE WHEN v10s_SD.ActFlag is null THEN '' ELSE v10s_SD.ActFlag END as ActFlag,
v20c_ISO_SCMST.CntryofIncorp,
v20c_ISO_SCMST.IssuerName,
v20c_ISO_SCMST.SecurityDesc,
v20c_ISO_SCMST.ParValue,
v20c_ISO_SCMST.PVCurrency,
CASE WHEN (v20c_ISO_SCMST.Isin IS NOT NULL AND v20c_ISO_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_ISO_SCMST.Uscode IS NOT NULL AND v20c_ISO_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_ISO_SCMST.Isin IS NOT NULL AND v20c_ISO_SCMST.Isin <> '') THEN v20c_ISO_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_ISO_SCMST.Uscode IS NOT NULL AND v20c_ISO_SCMST.Uscode <> '')THEN v20c_ISO_SCMST.Uscode ELSE '' END as SID,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN 'ISIN' WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN '/GB/' WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN '/US/' ELSE '' END as OldSIDname,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN ICC.Oldisin WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN SDCHG.Oldsedol WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN ICC.OldUscode ELSE '' END as OldSID,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN 'ISIN' WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN '/GB/' WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN '/US/' ELSE '' END as newSIDname,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN ICC.newisin WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN SDCHG.newsedol WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN ICC.newUscode ELSE '' END as NewSID,
v20c_ISO_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_ISO_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_ISO_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_SD.OldParValue is not null and v10s_SD.NewParValue is not null and v10s_SD.OldParValue <>'' and v10s_SD.NewParValue <> '' THEN v10s_SD.OldParValue ELSE '' END as OldParValue,
v10s_SD.NewParValue,
v10s_SD.OldRatio as RatioOld,
v10s_SD.NewRatio as RatioNew,
CASE WHEN v10s_SD.Fractions is null THEN '' ELSE v10s_SD.Fractions END as Fractions
into wca2.dbo.tISO_SPLF
from v10s_SD
INNER JOIN RD ON v10s_SD.EventID = RD.RdID
INNER JOIN v20c_ISO_SCMST ON RD.SecID = v20c_ISO_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_ISO_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_ISO_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_ISO_SCEXH.CntryCD = SDCHG.CntryCD AND v20c_ISO_SCEXH.RCntryCD = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_ISO_SCEXH.ExchgCD = LCC.ExchgCD
where (v10s_SD.Acttime >= @Startdate
or ICC.Acttime >= @Startdate
or SDCHG.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate)

go


print " Generating tISO_SPLR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_SPLR')
 drop table tISO_SPLR

use wca
Select 
v10s_CONSD.EventID,
v20c_ISO_SCMST.SecID,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) and (RD.Acttime > ICC.Acttime) and (RD.Acttime > SDCHG.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) and (EXDT.Acttime > ICC.Acttime) and (EXDT.Acttime > SDCHG.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) and (PEXDT.Acttime > ICC.Acttime) and (PEXDT.Acttime > SDCHG.Acttime) THEN PEXDT.Acttime WHEN (ICC.Acttime is not null) and (ICC.Acttime > v10s_CONSD.Acttime) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime WHEN (SDCHG.Acttime is not null) and (SDCHG.Acttime > v10s_CONSD.Acttime) THEN SDCHG.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
CASE WHEN v10s_CONSD.ActFlag is null THEN '' ELSE v10s_CONSD.ActFlag END as ActFlag,
v20c_ISO_SCMST.CntryofIncorp,
v20c_ISO_SCMST.IssuerName,
v20c_ISO_SCMST.SecurityDesc,
v20c_ISO_SCMST.ParValue,
v20c_ISO_SCMST.PVCurrency,
CASE WHEN (v20c_ISO_SCMST.Isin IS NOT NULL AND v20c_ISO_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_ISO_SCMST.Uscode IS NOT NULL AND v20c_ISO_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_ISO_SCMST.Isin IS NOT NULL AND v20c_ISO_SCMST.Isin <> '') THEN v20c_ISO_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_ISO_SCMST.Uscode IS NOT NULL AND v20c_ISO_SCMST.Uscode <> '')THEN v20c_ISO_SCMST.Uscode ELSE '' END as SID,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN 'ISIN' WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN '/GB/' WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN '/US/' ELSE '' END as OldSIDname,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN ICC.Oldisin WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN SDCHG.Oldsedol WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN ICC.OldUscode ELSE '' END as OldSID,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN 'ISIN' WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN '/GB/' WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN '/US/' ELSE '' END as newSIDname,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN ICC.newisin WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN SDCHG.newsedol WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN ICC.newUscode ELSE '' END as NewSID,
v20c_ISO_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_ISO_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_ISO_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_CONSD.OldParValue is not null and v10s_CONSD.NewParValue is not null and v10s_CONSD.OldParValue <>'' and v10s_CONSD.NewParValue <> '' THEN v10s_CONSD.OldParValue ELSE '' END as OldParValue,
v10s_CONSD.NewParValue,
v10s_CONSD.OldRatio as RatioOld,
v10s_CONSD.NewRatio as RatioNew,
CASE WHEN v10s_CONSD.Fractions is null THEN '' ELSE v10s_CONSD.Fractions END as Fractions
into wca2.dbo.tISO_SPLR
from v10s_CONSD
INNER JOIN RD ON v10s_CONSD.EventID = RD.RdID
INNER JOIN v20c_ISO_SCMST ON RD.SecID = v20c_ISO_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_ISO_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_ISO_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_ISO_SCEXH.CntryCD = SDCHG.CntryCD AND v20c_ISO_SCEXH.RCntryCD = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_ISO_SCEXH.ExchgCD = LCC.ExchgCD
where (v10s_CONSD.Acttime >= @Startdate
or ICC.Acttime >= @Startdate
or SDCHG.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate)
go
