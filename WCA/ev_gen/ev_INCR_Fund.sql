
print " Generating evi_Dividend_UnitTrust, please wait..."
go
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Dividend_UnitTrust')
 drop table evi_Dividend_UnitTrust
use wca
select 
case when DIVPY.OptionID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(DIVPY.OptionID as char(2)))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIV.EventID as char(8))) as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'1'+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DIV.EventID as char(8))) as bigint)
end as CAref,
v10s_DIV.Levent+'_UnitTrust'As Levent,
v10s_DIV.EventID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (DIVPY.Acttime is not null) 
and (DIVPY.Acttime > v10s_DIV.Acttime) 
and (DIVPY.Acttime > RD.Acttime) 
and (DIVPY.Acttime > EXDT.Acttime) 
and (DIVPY.Acttime > PEXDT.Acttime) THEN DIVPY.Acttime WHEN (RD.Acttime is not null) 
and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) 
and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) 
and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) 
and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
v10s_DIV.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.ParValue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
case when rtrim(v20c_EV_SCMST.StructCD)<>'' then rtrim(v20c_EV_SCMST.SectyCD) + '('+rtrim(v20c_EV_SCMST.StructCD)+')' else v20c_EV_SCMST.SectyCD END as SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
RD.RecDate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.ExDate is not null THEN '' WHEN PEXDT.ExDate is not null THEN 'P' ELSE '' END as Pex,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN EXDT.PayDate is not null THEN '' WHEN PEXDT.PayDate is not null THEN 'P' ELSE '' END as Ppy,
v10s_DIV.FYEDate as FinYearEndDate,
CASE WHEN v10s_DIV.DivPeriodCD= 'MNT' THEN 'Monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SMA' THEN 'Semi-Annual'
     WHEN v10s_DIV.DivPeriodCD= 'INS' THEN 'Installment'
     WHEN v10s_DIV.DivPeriodCD= 'INT' THEN 'Interim'
     WHEN v10s_DIV.DivPeriodCD= 'QTR' THEN 'Quarterly'
     WHEN v10s_DIV.DivPeriodCD= 'FNL' THEN 'Final'
     WHEN v10s_DIV.DivPeriodCD= 'ANL' THEN 'Annual'
     WHEN v10s_DIV.DivPeriodCD= 'REG' THEN 'Regular'
     WHEN v10s_DIV.DivPeriodCD= 'UN'  THEN 'Unspecified'
     WHEN v10s_DIV.DivPeriodCD= 'BIM' THEN 'Bi-monthly'
     WHEN v10s_DIV.DivPeriodCD= 'SPL' THEN 'Special'
     WHEN v10s_DIV.DivPeriodCD= 'TRM' THEN 'Trimesterly'
     WHEN v10s_DIV.DivPeriodCD= 'MEM' THEN 'Memorial'
     WHEN v10s_DIV.DivPeriodCD= 'SUP' THEN 'Supplemental'
     WHEN v10s_DIV.DivPeriodCD= 'ISC' THEN 'Interest on SGC'
     ELSE '' END as DividendFrequency,
CASE WHEN v10s_DIV.Tbaflag= 'T' THEN 'Yes' ELSE '' END as ToBeAnnounced,
CASE WHEN v10s_DIV.NilDividend= 'T' THEN 'Yes' ELSE '' END as NilDividend,
DIVPY.OptionID as OptionKey,
CASE WHEN (DIVPY.ActFlag is null) THEN irActionDIVPY.Lookup WHEN (irActionDIVPY.Lookup is null) and (LEN(DIVPY.Actflag) > 0) THEN '[' + DIVPY.Actflag +'] not found' ELSE irActionDIVPY.Lookup END as OptionRecordFlag,
CASE WHEN DIVPY.DivType= 'B' THEN 'Cash & Stock'
     WHEN DIVPY.DivType= 'S' THEN 'Stock'
     WHEN DIVPY.DivType= 'C' THEN 'Cash'
     WHEN DIVPY.NilDividend= 'Y' THEN 'NilDividend'
     ELSE 'Unspecified' END as DividendType,
DIVPY.GrossDividend,
DIVPY.NetDividend,
CASE WHEN rtrim(DIVPY.Equalisation) <> '' and DIVPY.Group2GrossDiv = '' then '0' ELSE DIVPY.Group2GrossDiv END as Group2GrossDiv,
CASE WHEN rtrim(DIVPY.Equalisation) <> '' and DIVPY.Group2NetDiv = '' then '0' ELSE DIVPY.Group2NetDiv END as Group2NetDiv,
CASE WHEN rtrim(DIVPY.Equalisation) <> '' or DIVPY.Equalisation is null then DIVPY.Equalisation when rtrim(DIVPY.Group2NetDiv) <> '' and rtrim(DIVPY.NetDividend) <> '' THEN cast(cast(rtrim(NetDividend) as decimal(20,9))-cast(rtrim(Group2NetDiv) as decimal(20,9)) as varchar(20)) ELSE '' END as Equalisation,
CASE WHEN (DIVPY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(DIVPY.CurenCD) > 0) THEN '[' + DIVPY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
CASE WHEN DIVPY.DivInPercent= 'T' THEN 'Yes' ELSE '' END as DivInPercent,
CASE WHEN DIVPY.RecindCashDiv= 'T' THEN 'Yes' ELSE '' END as CashDivRecinded,
DIVPY.TaxRate,
CASE WHEN DIVPY.Approxflag= 'T' THEN 'Yes' ELSE '' END as ApproximateDividend,
DIVPY.USDRateToCurrency,
CASE WHEN (DIVPY.Fractions is null) THEN irFractions.Lookup WHEN (irFractions.Lookup is null) and (LEN(DIVPY.Fractions) > 0) THEN '[' + DIVPY.Fractions +'] not found' ELSE irFractions.Lookup END as Fractions,
DIVPY.Coupon,
DIVPY.CouponID,
DIVPY.ResSecID as ResultantSecID,
Scmst.Isin as ResultantIsin,
sedolseq1.Sedol as ResultantSedol,
DIVPY.RatioNew +':'+DIVPY.RatioOld as Ratio,
EXDT.Paydate2 as StockPayDate,
'M' as Choice,
RD.RDNotes,
v10s_DIV.DIVNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Dividend_UnitTrust
FROM v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_EV_SCMST ON RD.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_EV_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER JOIN v10s_DIVPY AS DIVPY ON v10s_DIV.EventID = DIVPY.DivID
LEFT OUTER JOIN SCMST ON DIVPY.ResSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON DIVPY.ResSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irFRACTIONS ON DIVPY.Fractions = irFRACTIONS.Code
LEFT OUTER JOIN irACTION as irACTIONDIVPY ON DIVPY.Actflag = irACTIONDIVPY.Code
LEFT OUTER JOIN CUREN ON DIVPY.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where (DIVPY.Acttime >=  @Startdate
or v10s_DIV.Acttime >=  @Startdate
or RD.Acttime >=  @Startdate
or EXDT.Acttime >=  @Startdate
or PEXDT.Acttime >=  @Startdate)
and v20c_EV_SCEXH.secid is not null
And (v20c_EV_SCMST.sectycd = 'UT' Or v20c_EV_SCMST.sectycd = 'OIC')
