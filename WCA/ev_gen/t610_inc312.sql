print " Generating t610, please wait..."
go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Dividend')
	drop table t610_Dividend
use wca
select *
into t610_Dividend
FROM v54f_610_Dividend
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Company_Meeting')
	drop table t610_Company_Meeting
use wca
select *
into t610_Company_Meeting
FROM v50f_610_Company_Meeting
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Call')
	drop table t610_Call
use wca
select *
into t610_Call
FROM v53f_610_Call
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Liquidation')
	drop table t610_Liquidation
use wca
select *
into t610_Liquidation
FROM v50f_610_Liquidation
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Certificate_Exchange')
	drop table t610_Certificate_Exchange
use wca
select *
into t610_Certificate_Exchange
FROM v51f_610_Certificate_Exchange
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_International_Code_Change')
	drop table t610_International_Code_Change
use wca
select *
into t610_International_Code_Change
FROM v51f_610_International_Code_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Preference_Conversion')
	drop table t610_Preference_Conversion
use wca
select *
into t610_Preference_Conversion
FROM v51f_610_Preference_Conversion
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Preference_Redemption')
	drop table t610_Preference_Redemption
use wca
select *
into t610_Preference_Redemption
FROM v51f_610_Preference_Redemption
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Security_Reclassification')
	drop table t610_Security_Reclassification
use wca
select *
into t610_Security_Reclassification
FROM v51f_610_Security_Reclassification
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Lot_Change')
	drop table t610_Lot_Change
use wca
select *
into t610_Lot_Change
FROM v52f_610_Lot_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Sedol_Change')
	drop table t610_Sedol_Change
use wca
select *
into t610_Sedol_Change
FROM v52f_610_Sedol_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Buy_Back')
	drop table t610_Buy_Back
use wca
select *
into t610_Buy_Back
FROM v53f_610_Buy_Back
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Capital_Reduction')
	drop table t610_Capital_Reduction
use wca
select *
into t610_Capital_Reduction
FROM v53f_610_Capital_Reduction
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Takeover')
	drop table t610_Takeover
use wca
select *
into t610_Takeover
FROM v53f_610_Takeover
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Arrangement')
	drop table t610_Arrangement
use wca
select *
into t610_Arrangement
FROM v54f_610_Arrangement
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Bonus')
	drop table t610_Bonus
use wca
select *
into t610_Bonus
FROM v54f_610_Bonus
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Bonus_Rights')
	drop table t610_Bonus_Rights
use wca
select *
into t610_Bonus_Rights
FROM v54f_610_Bonus_Rights
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Consolidation')
	drop table t610_Consolidation
use wca
select *
into t610_Consolidation
FROM v54f_610_Consolidation
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Demerger')
	drop table t610_Demerger
use wca
select *
into t610_Demerger
FROM v54f_610_Demerger
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Distribution')
	drop table t610_Distribution
use wca
select *
into t610_Distribution
FROM v54f_610_Distribution
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Divestment')
	drop table t610_Divestment
use wca
select *
into t610_Divestment
FROM v54f_610_Divestment
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Entitlement')
	drop table t610_Entitlement
use wca
select *
into t610_Entitlement
FROM v54f_610_Entitlement
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Merger')
	drop table t610_Merger
use wca
select *
into t610_Merger
FROM v54f_610_Merger
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Preferential_Offer')
	drop table t610_Preferential_Offer
use wca
select *
into t610_Preferential_Offer
FROM v54f_610_Preferential_Offer
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Purchase_Offer')
	drop table t610_Purchase_Offer
use wca
select *
into t610_Purchase_Offer
FROM v54f_610_Purchase_Offer
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Rights ')
	drop table t610_Rights 
use wca
select *
into t610_Rights 
FROM v54f_610_Rights
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Security_Swap')
	drop table t610_Security_Swap
use wca
select *
into t610_Security_Swap
FROM v54f_610_Security_Swap
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Subdivision')
	drop table t610_Subdivision
use wca
select *
into t610_Subdivision
FROM v54f_610_Subdivision
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Bankruptcy ')
	drop table t610_Bankruptcy 
use wca
select *
into t610_Bankruptcy 
FROM v50f_610_Bankruptcy 
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Financial_Year_Change')
	drop table t610_Financial_Year_Change
use wca
select *
into t610_Financial_Year_Change
FROM v50f_610_Financial_Year_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Incorporation_Change')
	drop table t610_Incorporation_Change
use wca
select *
into t610_Incorporation_Change
FROM v50f_610_Incorporation_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Issuer_Name_change')
	drop table t610_Issuer_Name_change
use wca
select *
into t610_Issuer_Name_change
FROM v50f_610_Issuer_Name_change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Lawsuit')
	drop table t610_Lawsuit
use wca
select *
into t610_Lawsuit
FROM v50f_610_Lawsuit
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Security_Description_Change')
	drop table t610_Security_Description_Change
use wca
select *
into t610_Security_Description_Change
FROM v51f_610_Security_Description_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Assimilation')
	drop table t610_Assimilation
use wca
select *
into t610_Assimilation
FROM v52f_610_Assimilation
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Listing_Status_Change')
	drop table t610_Listing_Status_Change
use wca
select *
into t610_Listing_Status_Change
FROM v52f_610_Listing_Status_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Local_Code_Change')
	drop table t610_Local_Code_Change
use wca
select *
into t610_Local_Code_Change
FROM v52f_610_Local_Code_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_New_Listing')
	drop table t610_New_Listing
use wca
select *
into t610_New_Listing
FROM v52f_610_New_Listing
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Announcement')
	drop table t610_Announcement
use wca
select *
into t610_Announcement
FROM v50f_610_Announcement
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Parvalue_Redenomination')
	drop table t610_Parvalue_Redenomination
use wca
select *
into t610_Parvalue_Redenomination
FROM v51f_610_Parvalue_Redenomination
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Currency_Redenomination')
	drop table t610_Currency_Redenomination
use wca
select *
into t610_Currency_Redenomination
FROM v51f_610_Currency_Redenomination
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Return_of_Capital')
	drop table t610_Return_of_Capital
use wca
select *
into t610_Return_of_Capital
FROM v53f_610_Return_of_Capital
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Dividend_Reinvestment_Plan')
	drop table t610_Dividend_Reinvestment_Plan
use wca
select *
into t610_Dividend_Reinvestment_Plan
FROM v54f_610_Dividend_Reinvestment_Plan
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 3)
if exists (select * from sysobjects where name = 't610_Franking')
	drop table t610_Franking
use wca
select *
into t610_Franking
FROM v54f_610_Franking
where changed >= @Startdate

go
