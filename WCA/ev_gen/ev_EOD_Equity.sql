print " Generating evd_Dividend, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Dividend')
 drop table evd_Dividend

select *
into wca2.dbo.evd_Dividend
from wca2.dbo.evf_Dividend
where changed >= @Startdate-0.3
