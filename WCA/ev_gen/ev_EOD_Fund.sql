
print " Generating evd_Dividend_UnitTrust, please wait..."
go
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Dividend_UnitTrust')
 drop table evd_Dividend_UnitTrust

select *
into wca2.dbo.evd_Dividend_UnitTrust
from wca2.dbo.evf_Dividend_UnitTrust
where changed >= @Startdate-0.3
