
use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_DividendExtra')
	delete FROM wca2.dbo.t610_DividendExtra
use wca2
insert into t610_DividendExtra
select * FROM wca.dbo.v54f_610_DividendExtra
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Dividend')
	delete FROM wca2.dbo.t610_Dividend
use wca2

insert into t610_Dividend
select * FROM wca.dbo.v54f_610_Dividend
where (changed between '2011/01/18' and '2011/01/19')

go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists (select * FROM wca2.dbo.sysobjects where name = 't610_SG_reit_Dividend')
	delete FROM wca2.dbo.t610_SG_reit_Dividend
use wca2
insert into t610_SG_reit_Dividend
select wca.dbo.v54f_610_SG_reit_Dividend.* FROM wca.dbo.v54f_610_SG_reit_Dividend
inner join wca.dbo.scmst on wca.dbo.v54f_610_SG_reit_Dividend.secid = wca.dbo.scmst.secid
where 
wca.dbo.scmst.structcd = 'REIT'
and (changed between '2011/01/18' and '2011/01/19')

go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists (select * FROM wca2.dbo.sysobjects where name = 't610_SG_Dividend')
	delete FROM wca2.dbo.t610_SG_Dividend
use wca2
insert into t610_SG_Dividend
select wca.dbo.v54f_610_SG_Dividend.* FROM wca.dbo.v54f_610_SG_Dividend
inner join wca.dbo.scmst on wca.dbo.v54f_610_SG_Dividend.secid = wca.dbo.scmst.secid
where 
wca.dbo.scmst.structcd <> 'REIT'
and (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Company_Meeting')
	delete FROM wca2.dbo.t610_Company_Meeting
use wca2

insert into t610_Company_Meeting
select * FROM wca.dbo.v50f_610_Company_Meeting
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Call')
	delete FROM wca2.dbo.t610_Call
use wca2

insert into t610_Call
select * FROM wca.dbo.v53f_610_Call
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Liquidation')
	delete FROM wca2.dbo.t610_Liquidation
use wca2

insert into t610_Liquidation
select * FROM wca.dbo.v50f_610_Liquidation
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Certificate_Exchange')
	delete FROM wca2.dbo.t610_Certificate_Exchange
use wca2

insert into t610_Certificate_Exchange
select * FROM wca.dbo.v51f_610_Certificate_Exchange
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_International_Code_Change')
	delete FROM wca2.dbo.t610_International_Code_Change
use wca2

insert into t610_International_Code_Change
select * FROM wca.dbo.v51f_610_International_Code_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Preference_Conversion')
	delete FROM wca2.dbo.t610_Preference_Conversion
use wca2

insert into t610_Preference_Conversion
select * FROM wca.dbo.v51f_610_Preference_Conversion
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Preference_Redemption')
	delete FROM wca2.dbo.t610_Preference_Redemption
use wca2

insert into t610_Preference_Redemption
select * FROM wca.dbo.v51f_610_Preference_Redemption
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Security_Reclassification')
	delete FROM wca2.dbo.t610_Security_Reclassification
use wca2

insert into t610_Security_Reclassification
select * FROM wca.dbo.v51f_610_Security_Reclassification
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Lot_Change')
	delete FROM wca2.dbo.t610_Lot_Change
use wca2

insert into t610_Lot_Change
select * FROM wca.dbo.v52f_610_Lot_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Sedol_Change')
	delete FROM wca2.dbo.t610_Sedol_Change
use wca2

insert into t610_Sedol_Change
select * FROM wca.dbo.v52f_610_Sedol_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Buy_Back')
	delete FROM wca2.dbo.t610_Buy_Back
use wca2

insert into t610_Buy_Back
select * FROM wca.dbo.v53f_610_Buy_Back
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Capital_Reduction')
	delete FROM wca2.dbo.t610_Capital_Reduction
use wca2

insert into t610_Capital_Reduction
select * FROM wca.dbo.v53f_610_Capital_Reduction
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Takeover')
	delete FROM wca2.dbo.t610_Takeover
use wca2

insert into t610_Takeover
select * FROM wca.dbo.v53f_610_Takeover
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Arrangement')
	delete FROM wca2.dbo.t610_Arrangement
use wca2

insert into t610_Arrangement
select * FROM wca.dbo.v54f_610_Arrangement
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Bonus')
	delete FROM wca2.dbo.t610_Bonus
use wca2

insert into t610_Bonus
select * FROM wca.dbo.v54f_610_Bonus
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Bonus_Rights')
	delete FROM wca2.dbo.t610_Bonus_Rights
use wca2

insert into t610_Bonus_Rights
select * FROM wca.dbo.v54f_610_Bonus_Rights
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Consolidation')
	delete FROM wca2.dbo.t610_Consolidation
use wca2

insert into t610_Consolidation
select * FROM wca.dbo.v54f_610_Consolidation
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Demerger')
	delete FROM wca2.dbo.t610_Demerger
use wca2

insert into t610_Demerger
select * FROM wca.dbo.v54f_610_Demerger
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Distribution')
	delete FROM wca2.dbo.t610_Distribution
use wca2

insert into t610_Distribution
select * FROM wca.dbo.v54f_610_Distribution
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Divestment')
	delete FROM wca2.dbo.t610_Divestment
use wca2

insert into t610_Divestment
select * FROM wca.dbo.v54f_610_Divestment
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Entitlement')
	delete FROM wca2.dbo.t610_Entitlement
use wca2

insert into t610_Entitlement
select * FROM wca.dbo.v54f_610_Entitlement
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Merger')
	delete FROM wca2.dbo.t610_Merger
use wca2

insert into t610_Merger
select * FROM wca.dbo.v54f_610_Merger
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Preferential_Offer')
	delete FROM wca2.dbo.t610_Preferential_Offer
use wca2

insert into t610_Preferential_Offer
select * FROM wca.dbo.v54f_610_Preferential_Offer
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Purchase_Offer')
	delete FROM wca2.dbo.t610_Purchase_Offer
use wca2

insert into t610_Purchase_Offer
select * FROM wca.dbo.v54f_610_Purchase_Offer
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Rights ')
	delete FROM wca2.dbo.t610_Rights 
use wca2

insert into t610_Rights 
select * FROM wca.dbo.v54f_610_Rights
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Security_Swap')
	delete FROM wca2.dbo.t610_Security_Swap
use wca2

insert into t610_Security_Swap
select * FROM wca.dbo.v54f_610_Security_Swap
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Subdivision')
	delete FROM wca2.dbo.t610_Subdivision
use wca2

insert into t610_Subdivision
select * FROM wca.dbo.v54f_610_Subdivision
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Bankruptcy ')
	delete FROM wca2.dbo.t610_Bankruptcy 
use wca2

insert into t610_Bankruptcy 
select * FROM wca.dbo.v50f_610_Bankruptcy 
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Financial_Year_Change')
	delete FROM wca2.dbo.t610_Financial_Year_Change
use wca2

insert into t610_Financial_Year_Change
select * FROM wca.dbo.v50f_610_Financial_Year_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Incorporation_Change')
	delete FROM wca2.dbo.t610_Incorporation_Change
use wca2

insert into t610_Incorporation_Change
select * FROM wca.dbo.v50f_610_Incorporation_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Issuer_Name_change')
	delete FROM wca2.dbo.t610_Issuer_Name_change
use wca2

insert into t610_Issuer_Name_change
select * FROM wca.dbo.v50f_610_Issuer_Name_change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Lawsuit')
	delete FROM wca2.dbo.t610_Lawsuit
use wca2

insert into t610_Lawsuit
select * FROM wca.dbo.v50f_610_Lawsuit
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Security_Description_Change')
	delete FROM wca2.dbo.t610_Security_Description_Change
use wca2

insert into t610_Security_Description_Change
select * FROM wca.dbo.v51f_610_Security_Description_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Assimilation')
	delete FROM wca2.dbo.t610_Assimilation
use wca2

insert into t610_Assimilation
select * FROM wca.dbo.v52f_610_Assimilation
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Listing_Status_Change')
	delete FROM wca2.dbo.t610_Listing_Status_Change
use wca2

insert into t610_Listing_Status_Change
select * FROM wca.dbo.v52f_610_Listing_Status_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Local_Code_Change')
	delete FROM wca2.dbo.t610_Local_Code_Change
use wca2

insert into t610_Local_Code_Change
select * FROM wca.dbo.v52f_610_Local_Code_Change
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_New_Listing')
	delete FROM wca2.dbo.t610_New_Listing
use wca2

insert into t610_New_Listing
select * FROM wca.dbo.v52f_610_New_Listing
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Announcement')
	delete FROM wca2.dbo.t610_Announcement
use wca2

insert into t610_Announcement
select * FROM wca.dbo.v50f_610_Announcement
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Parvalue_Redenomination')
	delete FROM wca2.dbo.t610_Parvalue_Redenomination
use wca2

insert into t610_Parvalue_Redenomination
select * FROM wca.dbo.v51f_610_Parvalue_Redenomination
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Currency_Redenomination')
	delete FROM wca2.dbo.t610_Currency_Redenomination
use wca2

insert into t610_Currency_Redenomination
select * FROM wca.dbo.v51f_610_Currency_Redenomination
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Return_of_Capital')
	delete FROM wca2.dbo.t610_Return_of_Capital
use wca2

insert into t610_Return_of_Capital
select * FROM wca.dbo.v53f_610_Return_of_Capital
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Dividend_Reinvestment_Plan')
	delete FROM wca2.dbo.t610_Dividend_Reinvestment_Plan
use wca2

insert into t610_Dividend_Reinvestment_Plan
select * FROM wca.dbo.v54f_610_Dividend_Reinvestment_Plan
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't610_Franking')
	delete FROM wca2.dbo.t610_Franking
use wca2

insert into t610_Franking
select * FROM wca.dbo.v54f_610_Franking
where (changed between '2011/01/18' and '2011/01/19')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) FROM wca.dbo.tbl_Opslog where seq = 1)
if exists ( select * FROM wca2.dbo.sysobjects where name = 't612_xShares_Outstanding_Change')
	delete FROM wca2.dbo.t612_xShares_Outstanding_Change
use wca2

insert into t612_xShares_Outstanding_Change
select * FROM wca.dbo.v51f_612_xShares_Outstanding_Change
where (changed between '2011/01/18' and '2011/01/19')

go
