--filepath=o:\Datafeed\WCA\eviold\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select feeddate from wca.dbo.tbl_Opslog where feeddate = '2009/11/13'
--fileextension=.evi
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\WCA\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=select seq from wca.dbo.tbl_opslog where seq = 3
--sevent=n
--shownulls=n


--# 1
USE WCA2
SELECT * 
FROM ev_Company_Meeting 

--# 2
USE WCA2
SELECT *
FROM ev_Call

--# 3
USE WCA2
SELECT * 
FROM ev_Liquidation

--# 4
USE WCA2
SELECT *
FROM ev_Certificate_Exchange

--# 5
USE WCA2
SELECT * 
FROM ev_International_Code_Change

--# 6
USE WCA2
SELECT * 
FROM ev_Preference_Conversion

--# 7
USE WCA2
SELECT * 
FROM ev_Preference_Redemption

--# 8
USE WCA2
SELECT * 
FROM ev_Security_Reclassification

--# 9
USE WCA2
SELECT * 
FROM ev_Lot_Change

--# 10
USE WCA2
SELECT * 
FROM ev_Sedol_Change

--# 11
USE WCA2
SELECT * 
FROM ev_Buy_Back

--# 12
USE WCA2
SELECT * 
FROM ev_Capital_Reduction

--# 14
USE WCA2
SELECT * 
FROM ev_Takeover

--# 15
USE WCA2
SELECT * 
FROM ev_Arrangement

--# 16
USE WCA2
SELECT * 
FROM ev_Bonus

--# 17
USE WCA2
SELECT * 
FROM ev_Bonus_Rights

--# 18
USE WCA2
SELECT * 
FROM ev_Consolidation

--# 19
USE WCA2
SELECT * 
FROM ev_Demerger

--# 20
USE WCA2
SELECT * 
FROM ev_Distribution

--# 21
USE WCA2
SELECT * 
FROM ev_Divestment

--# 22
USE WCA2
SELECT * 
FROM ev_Entitlement

--# 23
USE WCA2
SELECT * 
FROM ev_Merger

--# 24
USE WCA2
SELECT * 
FROM ev_Preferential_Offer

--# 25
USE WCA2
SELECT * 
FROM ev_Purchase_Offer

--# 26
USE WCA2
SELECT * 
FROM ev_Rights 

--# 27
USE WCA2
SELECT * 
FROM ev_Security_Swap 

--# 28
USE WCA2
SELECT *
FROM ev_Subdivision

--# 29
USE WCA2
SELECT *
FROM ev_Bankruptcy 

--# 30
USE WCA2
SELECT *
FROM ev_Financial_Year_Change

--# 31
USE WCA2
SELECT *
FROM ev_Incorporation_Change

--# 32
USE WCA2
SELECT *
FROM ev_Issuer_Name_change

--# 33
USE WCA2
SELECT *
FROM ev_Class_Action

--# 34
USE WCA2
SELECT *
FROM ev_Security_Description_Change

--# 35
USE WCA2
SELECT *
FROM ev_Assimilation

--# 36
USE WCA2
SELECT *
FROM ev_Listing_Status_Change

--# 37
USE WCA2
SELECT *
FROM ev_Local_Code_Change

--# 38
USE WCA2
SELECT * 
FROM ev_New_Listing

--# 39
USE WCA2
SELECT * 
FROM ev_Announcement

--# 40
USE WCA2
SELECT * 
FROM ev_Parvalue_Redenomination 

--# 41
USE WCA2
SELECT * 
FROM ev_Currency_Redenomination 

--# 42
USE WCA2
SELECT * 
FROM ev_Return_of_Capital 

--# 43
USE WCA2
SELECT * 
FROM ev_Dividend

--# 44
USE WCA2
SELECT * 
FROM ev_Dividend_Reinvestment_Plan

--# 45
USE WCA2
SELECT * 
FROM ev_Franking
