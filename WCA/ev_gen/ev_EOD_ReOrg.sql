print " Generating evd_Certificate_Exchange, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Certificate_Exchange')
 drop table evd_Certificate_Exchange

select *
into wca2.dbo.evd_Certificate_Exchange
from wca2.dbo.evf_Certificate_Exchange
where changed >= @Startdate-0.3
go


print " Generating evd_Currency_Redenomination, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Currency_Redenomination')
 drop table evd_Currency_Redenomination

select *
into wca2.dbo.evd_Currency_Redenomination
from wca2.dbo.evf_Currency_Redenomination
where changed >= @Startdate-0.3
go

print " Generating evd_Parvalue_Redenomination, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Parvalue_Redenomination')
 drop table evd_Parvalue_Redenomination

select *
into wca2.dbo.evd_Parvalue_Redenomination
from wca2.dbo.evf_Parvalue_Redenomination
where changed >= @Startdate-0.3
go

print " Generating evd_Preference_Conversion, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Preference_Conversion')
 drop table evd_Preference_Conversion

select *
into wca2.dbo.evd_Preference_Conversion
from wca2.dbo.evf_Preference_Conversion
where changed >= @Startdate-0.3
go

print " Generating evd_Preference_Redemption, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Preference_Redemption')
 drop table evd_Preference_Redemption

select *
into wca2.dbo.evd_Preference_Redemption
from wca2.dbo.evf_Preference_Redemption
where changed >= @Startdate-0.3
go

print " Generating evd_Security_Reclassification, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Security_Reclassification')
 drop table evd_Security_Reclassification

select *
into wca2.dbo.evd_Security_Reclassification
from wca2.dbo.evf_Security_Reclassification
where changed >= @Startdate-0.3
go

print " Generating evd_Assimilation, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Assimilation')
 drop table evd_Assimilation

select *
into wca2.dbo.evd_Assimilation
from wca2.dbo.evf_Assimilation
where changed >= @Startdate-0.3
go

print " Generating evd_Buy_Back, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Buy_Back')
 drop table evd_Buy_Back

select *
into wca2.dbo.evd_Buy_Back
from wca2.dbo.evf_Buy_Back
where changed >= @Startdate-0.3
go

print " Generating evd_Call, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Call')
 drop table evd_Call

select *
into wca2.dbo.evd_Call
from wca2.dbo.evf_Call
where changed >= @Startdate-0.3
go

print " Generating evd_Capital_Reduction, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Capital_Reduction')
 drop table evd_Capital_Reduction

select *
into wca2.dbo.evd_Capital_Reduction
from wca2.dbo.evf_Capital_Reduction
where changed >= @Startdate-0.3
go

print " Generating evd_Return_of_Capital, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Return_of_Capital')
 drop table evd_Return_of_Capital

select *
into wca2.dbo.evd_Return_of_Capital
from wca2.dbo.evf_Return_of_Capital
where changed >= @Startdate-0.3
go

print " Generating evd_Takeover, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Takeover')
 drop table evd_Takeover

select *
into wca2.dbo.evd_Takeover
from wca2.dbo.evf_Takeover
where changed >= @Startdate-0.3

print " Generating evd_Arrangement, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Arrangement')
 drop table evd_Arrangement

select *
into wca2.dbo.evd_Arrangement
from wca2.dbo.evf_Arrangement
where changed >= @Startdate-0.3
go

print " Generating evd_Bonus, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Bonus')
 drop table evd_Bonus

select *
into wca2.dbo.evd_Bonus
from wca2.dbo.evf_Bonus
where changed >= @Startdate-0.3
go

print " Generating evd_Bonus_Rights, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Bonus_Rights')
 drop table evd_Bonus_Rights

select *
into wca2.dbo.evd_Bonus_Rights
from wca2.dbo.evf_Bonus_Rights
where changed >= @Startdate-0.3
go

print " Generating evd_Consolidation, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Consolidation')
 drop table evd_Consolidation

select *
into wca2.dbo.evd_Consolidation
from wca2.dbo.evf_Consolidation
where changed >= @Startdate-0.3
go

print " Generating evd_Demerger, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Demerger')
 drop table evd_Demerger

select *
into wca2.dbo.evd_Demerger
from wca2.dbo.evf_Demerger
where changed >= @Startdate-0.3
go

print " Generating evd_Distribution, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Distribution') drop table evd_Distribution

select *
into wca2.dbo.evd_Distribution
from wca2.dbo.evf_Distribution
where changed >= @Startdate-0.3
go

print " Generating evd_Divestment, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Divestment')
 drop table evd_Divestment

select *
into wca2.dbo.evd_Divestment
from wca2.dbo.evf_Divestment
where changed >= @Startdate-0.3
go

print " Generating evd_Entitlement, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Entitlement')
 drop table evd_Entitlement

select *
into wca2.dbo.evd_Entitlement
from wca2.dbo.evf_Entitlement
where changed >= @Startdate-0.3
go

print " Generating evd_Subdivision, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Subdivision')
 drop table evd_Subdivision

select *
into wca2.dbo.evd_Subdivision
from wca2.dbo.evf_Subdivision
where changed >= @Startdate-0.3
go

print " Generating evd_Merger, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Merger')
 drop table evd_Merger

select *
into wca2.dbo.evd_Merger
from wca2.dbo.evf_Merger
where changed >= @Startdate-0.3
go

print " Generating evd_Rights, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Rights')
 drop table evd_Rights

select *
into wca2.dbo.evd_Rights
from wca2.dbo.evf_Rights
where changed >= @Startdate-0.3
go

print " Generating evd_Preferential_Offer, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Preferential_Offer')
 drop table evd_Preferential_Offer

select *
into wca2.dbo.evd_Preferential_Offer
from wca2.dbo.evf_Preferential_Offer
where changed >= @Startdate-0.3
go

print " Generating evd_Purchase_Offer, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Purchase_Offer')
 drop table evd_Purchase_Offer

select *
into wca2.dbo.evd_Purchase_Offer
from wca2.dbo.evf_Purchase_Offer
where changed >= @Startdate-0.3
go

print " Generating evd_Dividend_Reinvestment_Plan, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Dividend_Reinvestment_Plan')
 drop table evd_Dividend_Reinvestment_Plan

select *
into wca2.dbo.evd_Dividend_Reinvestment_Plan
from wca2.dbo.evf_Dividend_Reinvestment_Plan
where changed >= @Startdate-0.3
go

print " Generating evd_Franking, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Franking')
 drop table evd_Franking

select *
into wca2.dbo.evd_Franking
from wca2.dbo.evf_Franking
where changed >= @Startdate-0.3
go

print " Generating evd_Security_Swap, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Security_Swap')
 drop table evd_Security_Swap

select *
into wca2.dbo.evd_Security_Swap
from wca2.dbo.evf_Security_Swap
where changed >= @Startdate-0.3
go

print " Generating evd_Depositary_Receipt_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Depositary_Receipt_Change')
 drop table evd_Depositary_Receipt_Change

select *
into wca2.dbo.evd_Depositary_Receipt_Change
from wca2.dbo.evf_Depositary_Receipt_Change
where changed >= @Startdate-0.3
go

print " Generating evd_Shares_Outstanding_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Shares_Outstanding_Change')
 drop table evd_Shares_Outstanding_Change

select *
into wca2.dbo.evd_Shares_Outstanding_Change
from wca2.dbo.evf_Shares_Outstanding_Change
where changed >= @Startdate-0.3
go

