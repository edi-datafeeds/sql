--filepath=o:\Datafeed\WCA\align\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.evf
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\WCA\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
USE WCA2
SELECT * 
FROM evf_Takeover

--# 42
USE WCA2
SELECT * 
FROM evf_Return_of_Capital 

--# 43
USE WCA2
SELECT * 
FROM evf_Dividend

--# 44
USE WCA2
SELECT * 
FROM evf_Dividend_Reinvestment_Plan

--# 45
USE WCA2
SELECT * 
FROM evf_Franking
