print " Generating evd_Warrant_Terms_Change, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Warrant_Terms_Change')
 drop table evd_Warrant_Terms_Change
select *
into wca2.dbo.evd_Warrant_Terms_Change
from wca2.dbo.evf_Warrant_Terms_Change
where changed >= @Startdate-0.3
go


print " Generating evd_Warrant_Terms_Change_Change, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Warrant_Terms_Change')
 drop table evd_Warrant_Terms_Change
select *
into wca2.dbo.evd_Warrant_Terms_Change
from wca2.dbo.evf_Warrant_Terms_Change
where changed >= @Startdate-0.3
go


print " Generating evd_Warrant_Exercise_Change, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Warrant_Exercise_Change_Change')
 drop table evd_Warrant_Exercise_Change_Change
select *
into wca2.dbo.evd_Warrant_Exercise_Change_Change
from wca2.dbo.evf_Warrant_Exercise_Change_Change
where changed >= @Startdate-0.3
go


print " Generating evd_Warrant_Exercise, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Warrant_Exercise_Change')
 drop table evd_Warrant_Exercise_Change
select *
into wca2.dbo.evd_Warrant_Exercise_Change
from wca2.dbo.evf_Warrant_Exercise_Change
where changed >= @Startdate-0.3
go



print " Generating evd_Covered_Warrant, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Covered_Warrant_Change')
 drop table evd_Covered_Warrant_Change
select *
into wca2.dbo.evd_Covered_Warrant_Change
from wca2.dbo.evf_Covered_Warrant_Change
where changed >= @Startdate-0.3
go



print " Generating evd_Basket_Constituent_Change, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Basket_Constituent_Change_Change')
 drop table evd_Basket_Constituent_Change_Change
select *
into wca2.dbo.evd_Basket_Constituent_Change_Change
from wca2.dbo.evf_Basket_Constituent_Change_Change
where changed >= @Startdate-0.3
go



print " Generating evd_Basket_Warrant, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Basket_Warrant_Change')
 drop table evd_Basket_Warrant_Change
select *
into wca2.dbo.evd_Basket_Warrant_Change
from wca2.dbo.evf_Basket_Warrant_Change
where changed >= @Startdate-0.3
go

