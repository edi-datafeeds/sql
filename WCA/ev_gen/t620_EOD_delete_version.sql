print " Generating t620, please wait..."
go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Dividend')
	delete from t620_Dividend
use wca2
insert into t620_Dividend
select * FROM wca.dbo.v54f_620_Dividend
where changed >= @Startdate


go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Company_Meeting')
	delete from t620_Company_Meeting
use wca2
insert into t620_Company_Meeting
select * FROM wca.dbo.v50f_620_Company_Meeting
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Call')
	delete from t620_Call
use wca2
insert into t620_Call
select * FROM wca.dbo.v53f_620_Call
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Liquidation')
	delete from t620_Liquidation
use wca2
insert into t620_Liquidation
select * FROM wca.dbo.v50f_620_Liquidation
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Certificate_Exchange')
	delete from t620_Certificate_Exchange
use wca2
insert into t620_Certificate_Exchange
select * FROM wca.dbo.v51f_620_Certificate_Exchange
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_International_Code_Change')
	delete from t620_International_Code_Change
use wca2
insert into t620_International_Code_Change
select * FROM wca.dbo.v51f_620_International_Code_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Conversion_Terms')
	delete from t620_Conversion_Terms
use wca2
insert into t620_Conversion_Terms
select * FROM wca.dbo.v51f_620_Conversion_Terms
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Conversion_Terms_Change')
	delete from t620_Conversion_Terms_Change
use wca2
insert into t620_Conversion_Terms_Change
select * FROM wca.dbo.v51f_620_Conversion_Terms_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Redemption_Terms')
	delete from t620_Redemption_Terms
use wca2
insert into t620_Redemption_Terms
select * FROM wca.dbo.v51f_620_Redemption_Terms
where changed >= @Startdate

go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Security_Reclassification')
	delete from t620_Security_Reclassification
use wca2
insert into t620_Security_Reclassification
select * FROM wca.dbo.v51f_620_Security_Reclassification
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Lot_Change')
	delete from t620_Lot_Change
use wca2
insert into t620_Lot_Change
select * FROM wca.dbo.v52f_620_Lot_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Sedol_Change')
	delete from t620_Sedol_Change
use wca2
insert into t620_Sedol_Change
select * FROM wca.dbo.v52f_620_Sedol_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Buy_Back')
	delete from t620_Buy_Back
use wca2
insert into t620_Buy_Back
select * FROM wca.dbo.v53f_620_Buy_Back
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Capital_Reduction')
	delete from t620_Capital_Reduction
use wca2
insert into t620_Capital_Reduction
select * FROM wca.dbo.v53f_620_Capital_Reduction
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Takeover')
	delete from t620_Takeover
use wca2
insert into t620_Takeover
select * FROM wca.dbo.v53f_620_Takeover
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Arrangement')
	delete from t620_Arrangement
use wca2
insert into t620_Arrangement
select * FROM wca.dbo.v54f_620_Arrangement
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Bonus')
	delete from t620_Bonus
use wca2
insert into t620_Bonus
select * FROM wca.dbo.v54f_620_Bonus
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Consolidation')
	delete from t620_Consolidation
use wca2
insert into t620_Consolidation
select * FROM wca.dbo.v54f_620_Consolidation
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Demerger')
	delete from t620_Demerger
use wca2
insert into t620_Demerger
select * FROM wca.dbo.v54f_620_Demerger
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Distribution')
	delete from t620_Distribution
use wca2
insert into t620_Distribution
select * FROM wca.dbo.v54f_620_Distribution
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Divestment')
	delete from t620_Divestment
use wca2
insert into t620_Divestment
select * FROM wca.dbo.v54f_620_Divestment
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Entitlement')
	delete from t620_Entitlement
use wca2
insert into t620_Entitlement
select * FROM wca.dbo.v54f_620_Entitlement
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Merger')
	delete from t620_Merger
use wca2
insert into t620_Merger
select * FROM wca.dbo.v54f_620_Merger
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Preferential_Offer')
	delete from t620_Preferential_Offer
use wca2
insert into t620_Preferential_Offer
select * FROM wca.dbo.v54f_620_Preferential_Offer
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Purchase_Offer')
	delete from t620_Purchase_Offer
use wca2
insert into t620_Purchase_Offer
select * FROM wca.dbo.v54f_620_Purchase_Offer
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Rights ')
	delete from t620_Rights 
use wca2
insert into t620_Rights 
select * FROM wca.dbo.v54f_620_Rights
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Bonus_Rights ')
	delete from t620_Bonus_Rights 
use wca2
insert into t620_Bonus_Rights 
select * FROM wca.dbo.v54f_620_Bonus_Rights
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Security_Swap')
	delete from t620_Security_Swap
use wca2
insert into t620_Security_Swap
select * FROM wca.dbo.v54f_620_Security_Swap
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Subdivision')
	delete from t620_Subdivision
use wca2
insert into t620_Subdivision
select * FROM wca.dbo.v54f_620_Subdivision
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Bankruptcy ')
	delete from t620_Bankruptcy 
use wca2
insert into t620_Bankruptcy 
select * FROM wca.dbo.v50f_620_Bankruptcy 
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Financial_Year_Change')
	delete from t620_Financial_Year_Change
use wca2
insert into t620_Financial_Year_Change
select * FROM wca.dbo.v50f_620_Financial_Year_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Incorporation_Change')
	delete from t620_Incorporation_Change
use wca2
insert into t620_Incorporation_Change
select * FROM wca.dbo.v50f_620_Incorporation_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Issuer_Name_change')
	delete from t620_Issuer_Name_change
use wca2
insert into t620_Issuer_Name_change
select * FROM wca.dbo.v50f_620_Issuer_Name_change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Class_Action')
	delete from t620_Class_Action
use wca2
insert into t620_Class_Action
select * FROM wca.dbo.v50f_620_Class_Action
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Security_Description_Change')
	delete from t620_Security_Description_Change
use wca2
insert into t620_Security_Description_Change
select * FROM wca.dbo.v51f_620_Security_Description_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Assimilation')
	delete from t620_Assimilation
use wca2
insert into t620_Assimilation
select * FROM wca.dbo.v52f_620_Assimilation
where changed >= @Startdate

go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Listing_Status_Change')
	delete from t620_Listing_Status_Change
use wca2
insert into t620_Listing_Status_Change
select * FROM wca.dbo.v52f_620_Listing_Status_Change
where changed >= @Startdate


go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Local_Code_Change')
	delete from t620_Local_Code_Change
use wca2
insert into t620_Local_Code_Change
select * FROM wca.dbo.v52f_620_Local_Code_Change
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_New_Listing')
	delete from t620_New_Listing
use wca2
insert into t620_New_Listing
select * FROM wca.dbo.v52f_620_New_Listing
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Announcement')
	delete from t620_Announcement
use wca2
insert into t620_Announcement
select * FROM wca.dbo.v50f_620_Announcement
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Parvalue_Redenomination')
	delete from t620_Parvalue_Redenomination
use wca2
insert into t620_Parvalue_Redenomination
select * FROM wca.dbo.v51f_620_Parvalue_Redenomination
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Currency_Redenomination')
	delete from t620_Currency_Redenomination
use wca2
insert into t620_Currency_Redenomination
select * FROM wca.dbo.v51f_620_Currency_Redenomination
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Return_of_Capital')
	delete from t620_Return_of_Capital
use wca2
insert into t620_Return_of_Capital
select * FROM wca.dbo.v53f_620_Return_of_Capital
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Dividend_Reinvestment_Plan')
	delete from t620_Dividend_Reinvestment_Plan
use wca2
insert into t620_Dividend_Reinvestment_Plan
select * FROM wca.dbo.v54f_620_Dividend_Reinvestment_Plan
where changed >= @Startdate

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't620_Franking')
	delete from t620_Franking
use wca2
insert into t620_Franking
select * FROM wca.dbo.v54f_620_Franking
where changed >= @Startdate

go
