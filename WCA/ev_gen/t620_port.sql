print " Generating t620, please wait..."
go


use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Dividend')
	drop table t620_Dividend
use wca
select *
into t620_Dividend
FROM v54f_620_Dividend
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'


go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Company_Meeting')
	drop table t620_Company_Meeting
use wca
select *
into t620_Company_Meeting
FROM v50f_620_Company_Meeting
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Call')
	drop table t620_Call
use wca
select *
into t620_Call
FROM v53f_620_Call
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Liquidation')
	drop table t620_Liquidation
use wca
select *
into t620_Liquidation
FROM v50f_620_Liquidation
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Certificate_Exchange')
	drop table t620_Certificate_Exchange
use wca
select *
into t620_Certificate_Exchange
FROM v51f_620_Certificate_Exchange
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_International_Code_Change')
	drop table t620_International_Code_Change
use wca
select *
into t620_International_Code_Change
FROM v51f_620_International_Code_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Conversion_Terms')
	drop table t620_Conversion_Terms
use wca
select *
into t620_Conversion_Terms
FROM v51f_620_Conversion_Terms
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Conversion_Terms_Change')
	drop table t620_Conversion_Terms_Change
use wca
select *
into t620_Conversion_Terms_Change
FROM v51f_620_Conversion_Terms_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Redemption_Terms')
	drop table t620_Redemption_Terms
use wca
select *
into t620_Redemption_Terms
FROM v51f_620_Redemption_Terms
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go


use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Security_Reclassification')
	drop table t620_Security_Reclassification
use wca
select *
into t620_Security_Reclassification
FROM v51f_620_Security_Reclassification
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Lot_Change')
	drop table t620_Lot_Change
use wca
select *
into t620_Lot_Change
FROM v52f_620_Lot_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Sedol_Change')
	drop table t620_Sedol_Change
use wca
select *
into t620_Sedol_Change
FROM v52f_620_Sedol_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Buy_Back')
	drop table t620_Buy_Back
use wca
select *
into t620_Buy_Back
FROM v53f_620_Buy_Back
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Capital_Reduction')
	drop table t620_Capital_Reduction
use wca
select *
into t620_Capital_Reduction
FROM v53f_620_Capital_Reduction
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Takeover')
	drop table t620_Takeover
use wca
select *
into t620_Takeover
FROM v53f_620_Takeover
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Arrangement')
	drop table t620_Arrangement
use wca
select *
into t620_Arrangement
FROM v54f_620_Arrangement
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Bonus')
	drop table t620_Bonus
use wca
select *
into t620_Bonus
FROM v54f_620_Bonus
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Bonus_Rights')
	drop table t620_Bonus_Rights
use wca
select *
into t620_Bonus_Rights
FROM v54f_620_Bonus_Rights
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Consolidation')
	drop table t620_Consolidation
use wca
select *
into t620_Consolidation
FROM v54f_620_Consolidation
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Demerger')
	drop table t620_Demerger
use wca
select *
into t620_Demerger
FROM v54f_620_Demerger
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Distribution')
	drop table t620_Distribution
use wca
select *
into t620_Distribution
FROM v54f_620_Distribution
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Divestment')
	drop table t620_Divestment
use wca
select *
into t620_Divestment
FROM v54f_620_Divestment
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Entitlement')
	drop table t620_Entitlement
use wca
select *
into t620_Entitlement
FROM v54f_620_Entitlement
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Merger')
	drop table t620_Merger
use wca
select *
into t620_Merger
FROM v54f_620_Merger
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Preferential_Offer')
	drop table t620_Preferential_Offer
use wca
select *
into t620_Preferential_Offer
FROM v54f_620_Preferential_Offer
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Purchase_Offer')
	drop table t620_Purchase_Offer
use wca
select *
into t620_Purchase_Offer
FROM v54f_620_Purchase_Offer
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Rights ')
	drop table t620_Rights 
use wca
select *
into t620_Rights 
FROM v54f_620_Rights
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Security_Swap')
	drop table t620_Security_Swap
use wca
select *
into t620_Security_Swap
FROM v54f_620_Security_Swap
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Subdivision')
	drop table t620_Subdivision
use wca
select *
into t620_Subdivision
FROM v54f_620_Subdivision
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Bankruptcy ')
	drop table t620_Bankruptcy 
use wca
select *
into t620_Bankruptcy 
FROM v50f_620_Bankruptcy 
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Financial_Year_Change')
	drop table t620_Financial_Year_Change
use wca
select *
into t620_Financial_Year_Change
FROM v50f_620_Financial_Year_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Incorporation_Change')
	drop table t620_Incorporation_Change
use wca
select *
into t620_Incorporation_Change
FROM v50f_620_Incorporation_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Issuer_Name_change')
	drop table t620_Issuer_Name_change
use wca
select *
into t620_Issuer_Name_change
FROM v50f_620_Issuer_Name_change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Class_Action')
	drop table t620_Class_Action
use wca
select *
into t620_Class_Action
FROM v50f_620_Class_Action
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Security_Description_Change')
	drop table t620_Security_Description_Change
use wca
select *
into t620_Security_Description_Change
FROM v51f_620_Security_Description_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Assimilation')
	drop table t620_Assimilation
use wca
select *
into t620_Assimilation
FROM v52f_620_Assimilation
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Listing_Status_Change')
	drop table t620_Listing_Status_Change
use wca
select *
into t620_Listing_Status_Change
FROM v52f_620_Listing_Status_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Local_Code_Change')
	drop table t620_Local_Code_Change
use wca
select *
into t620_Local_Code_Change
FROM v52f_620_Local_Code_Change
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_New_Listing')
	drop table t620_New_Listing
use wca
select *
into t620_New_Listing
FROM v52f_620_New_Listing
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Announcement')
	drop table t620_Announcement
use wca
select *
into t620_Announcement
FROM v50f_620_Announcement
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Parvalue_Redenomination')
	drop table t620_Parvalue_Redenomination
use wca
select *
into t620_Parvalue_Redenomination
FROM v51f_620_Parvalue_Redenomination
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Currency_Redenomination')
	drop table t620_Currency_Redenomination
use wca
select *
into t620_Currency_Redenomination
FROM v51f_620_Currency_Redenomination
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Return_of_Capital')
	drop table t620_Return_of_Capital
use wca
select *
into t620_Return_of_Capital
FROM v53f_620_Return_of_Capital
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Dividend_Reinvestment_Plan')
	drop table t620_Dividend_Reinvestment_Plan
use wca
select *
into t620_Dividend_Reinvestment_Plan
FROM v54f_620_Dividend_Reinvestment_Plan
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go

use wca
Declare @StartDate datetime
set @StartDate = '2002/01/01'
if exists (select * from sysobjects where name = 't620_Franking')
	drop table t620_Franking
use wca
select *
into t620_Franking
FROM v54f_620_Franking
where (sedol in (select col001 from wca.dbo.citi_sedol)) and created > '2006/01/01'

go
