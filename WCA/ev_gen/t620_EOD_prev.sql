use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Dividend')
	drop table tt620_Dividend
use wca2
select *
into tt620_Dividend
FROM wca.dbo.v54f_620_Dividend
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Company_Meeting')
	drop table tt620_Company_Meeting
use wca2
select *
into tt620_Company_Meeting
FROM wca.dbo.v50f_620_Company_Meeting
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Call')
	drop table tt620_Call
use wca2
select *
into tt620_Call
FROM wca.dbo.v53f_620_Call
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Liquidation')
	drop table tt620_Liquidation
use wca2
select *
into tt620_Liquidation
FROM wca.dbo.v50f_620_Liquidation
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Certificate_Exchange')
	drop table tt620_Certificate_Exchange
use wca2
select *
into tt620_Certificate_Exchange
FROM wca.dbo.v51f_620_Certificate_Exchange
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_International_Code_Change')
	drop table tt620_International_Code_Change
use wca2
select *
into tt620_International_Code_Change
FROM wca.dbo.v51f_620_International_Code_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Conversion_Terms')
	drop table tt620_Conversion_Terms
use wca2
select *
into tt620_Conversion_Terms
FROM wca.dbo.v51f_620_Conversion_Terms
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Conversion_Terms_Change')
	drop table tt620_Conversion_Terms_Change
use wca2
select *
into tt620_Conversion_Terms_Change
FROM wca.dbo.v51f_620_Conversion_Terms_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Redemption_Terms')
	drop table tt620_Redemption_Terms
use wca2
select *
into tt620_Redemption_Terms
FROM wca.dbo.v51f_620_Redemption_Terms
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Security_Reclassification')
	drop table tt620_Security_Reclassification
use wca2
select *
into tt620_Security_Reclassification
FROM wca.dbo.v51f_620_Security_Reclassification
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Lot_Change')
	drop table tt620_Lot_Change
use wca2
select *
into tt620_Lot_Change
FROM wca.dbo.v52f_620_Lot_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Sedol_Change')
	drop table tt620_Sedol_Change
use wca2
select *
into tt620_Sedol_Change
FROM wca.dbo.v52f_620_Sedol_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Buy_Back')
	drop table tt620_Buy_Back
use wca2
select *
into tt620_Buy_Back
FROM wca.dbo.v53f_620_Buy_Back
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Capital_Reduction')
	drop table tt620_Capital_Reduction
use wca2
select *
into tt620_Capital_Reduction
FROM wca.dbo.v53f_620_Capital_Reduction
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Takeover')
	drop table tt620_Takeover
use wca2
select *
into tt620_Takeover
FROM wca.dbo.v53f_620_Takeover
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Arrangement')
	drop table tt620_Arrangement
use wca2
select *
into tt620_Arrangement
FROM wca.dbo.v54f_620_Arrangement
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Bonus')
	drop table tt620_Bonus
use wca2
select *
into tt620_Bonus
FROM wca.dbo.v54f_620_Bonus
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Consolidation')
	drop table tt620_Consolidation
use wca2
select *
into tt620_Consolidation
FROM wca.dbo.v54f_620_Consolidation
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Demerger')
	drop table tt620_Demerger
use wca2
select *
into tt620_Demerger
FROM wca.dbo.v54f_620_Demerger
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Distribution')
	drop table tt620_Distribution
use wca2
select *
into tt620_Distribution
FROM wca.dbo.v54f_620_Distribution
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Divestment')
	drop table tt620_Divestment
use wca2
select *
into tt620_Divestment
FROM wca.dbo.v54f_620_Divestment
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Entitlement')
	drop table tt620_Entitlement
use wca2
select *
into tt620_Entitlement
FROM wca.dbo.v54f_620_Entitlement
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Merger')
	drop table tt620_Merger
use wca2
select *
into tt620_Merger
FROM wca.dbo.v54f_620_Merger
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Preferential_Offer')
	drop table tt620_Preferential_Offer
use wca2
select *
into tt620_Preferential_Offer
FROM wca.dbo.v54f_620_Preferential_Offer
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Purchase_Offer')
	drop table tt620_Purchase_Offer
use wca2
select *
into tt620_Purchase_Offer
FROM wca.dbo.v54f_620_Purchase_Offer
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Rights ')
	drop table tt620_Rights 
use wca2
select *
into tt620_Rights 
FROM wca.dbo.v54f_620_Rights
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Bonus_Rights ')
	drop table tt620_Bonus_Rights 
use wca2
select *
into tt620_Bonus_Rights 
FROM wca.dbo.v54f_620_Bonus_Rights
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Security_Swap')
	drop table tt620_Security_Swap
use wca2
select *
into tt620_Security_Swap
FROM wca.dbo.v54f_620_Security_Swap
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Subdivision')
	drop table tt620_Subdivision
use wca2
select *
into tt620_Subdivision
FROM wca.dbo.v54f_620_Subdivision
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Bankruptcy ')
	drop table tt620_Bankruptcy 
use wca2
select *
into tt620_Bankruptcy 
FROM wca.dbo.v50f_620_Bankruptcy 
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Financial_Year_Change')
	drop table tt620_Financial_Year_Change
use wca2
select *
into tt620_Financial_Year_Change
FROM wca.dbo.v50f_620_Financial_Year_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Incorporation_Change')
	drop table tt620_Incorporation_Change
use wca2
select *
into tt620_Incorporation_Change
FROM wca.dbo.v50f_620_Incorporation_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Issuer_Name_change')
	drop table tt620_Issuer_Name_change
use wca2
select *
into tt620_Issuer_Name_change
FROM wca.dbo.v50f_620_Issuer_Name_change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Class_Action')
	drop table tt620_Class_Action
use wca2
select *
into tt620_Class_Action
FROM wca.dbo.v50f_620_Class_Action
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Security_Description_Change')
	drop table tt620_Security_Description_Change
use wca2
select *
into tt620_Security_Description_Change
FROM wca.dbo.v51f_620_Security_Description_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Assimilation')
	drop table tt620_Assimilation
use wca2
select *
into tt620_Assimilation
FROM wca.dbo.v52f_620_Assimilation
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Listing_Status_Change')
	drop table tt620_Listing_Status_Change
use wca2
select *
into tt620_Listing_Status_Change
FROM wca.dbo.v52f_620_Listing_Status_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Local_Code_Change')
	drop table tt620_Local_Code_Change
use wca2
select *
into tt620_Local_Code_Change
FROM wca.dbo.v52f_620_Local_Code_Change
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_New_Listing')
	drop table tt620_New_Listing
use wca2
select *
into tt620_New_Listing
FROM wca.dbo.v52f_620_New_Listing
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Announcement')
	drop table tt620_Announcement
use wca2
select *
into tt620_Announcement
FROM wca.dbo.v50f_620_Announcement
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Parvalue_Redenomination')
	drop table tt620_Parvalue_Redenomination
use wca2
select *
into tt620_Parvalue_Redenomination
FROM wca.dbo.v51f_620_Parvalue_Redenomination
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Currency_Redenomination')
	drop table tt620_Currency_Redenomination
use wca2
select *
into tt620_Currency_Redenomination
FROM wca.dbo.v51f_620_Currency_Redenomination
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Return_of_Capital')
	drop table tt620_Return_of_Capital
use wca2
select *
into tt620_Return_of_Capital
FROM wca.dbo.v53f_620_Return_of_Capital
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Dividend_Reinvestment_Plan')
	drop table tt620_Dividend_Reinvestment_Plan
use wca2
select *
into tt620_Dividend_Reinvestment_Plan
FROM wca.dbo.v54f_620_Dividend_Reinvestment_Plan
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tt620_Franking')
	drop table tt620_Franking
use wca2
select *
into tt620_Franking
FROM wca.dbo.v54f_620_Franking
where
changed between '2010/04/21' and '2010/04/22'
and substring(excountry,1,2)='US'
go
