print ""
go

print " Generating evf_Interest_Payment, please wait..."
go

use WcaArch
Declare @StartDate datetime
Declare @EndDate datetime
set @StartDate = '2005/01/01'
set @EndDate = getDate()+1
if exists (select * from sysobjects where name = 'evf_Interest_Payment')
	drop table evf_Interest_Payment

use wca
select 
cast(cast(INTPY.OptionID as char(1))+'1'+cast(v20c_EV_SCEXH.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_INT.EventID as char(16))) as bigint) as CAref,
v10s_INT.Levent,  
v10s_INT.EventID,  
v10s_INT.AnnounceDate as 'Created',  
CASE WHEN (INTPY.Acttime is not null) and (INTPY.Acttime > v10s_INT.Acttime) and (INTPY.Acttime > RD.Acttime) THEN INTPY.Acttime  WHEN RD.Acttime > v10s_INT.Acttime THEN RD.Acttime  ELSE v10s_INT.Acttime END as [Changed],
v10s_INT.ActFlag,
v20c_EVde_SCMST.CntryofIncorp,
v20c_EVde_SCMST.IssuerName,
v20c_EVde_SCMST.SecurityDesc,
v20c_EVde_SCMST.ParValue,
v20c_EVde_SCMST.PVCurrency,
v20c_EVde_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EVde_SCMST.USCode,
v20c_EVde_SCMST.StatusFlag,
v20c_EVde_SCMST.SectyCD,SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_SCEXH.ExchgCD IS NULL OR v20c_EVde_SCMST.PrimaryExchgCD IS NULL OR v20c_EVde_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVde_SCMST.PrimaryExchgCD=v20c_EV_SCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_SCEXH.ExchgCD,
v20c_EV_SCEXH.MIC,
v20c_EV_SCEXH.ExCountry,
v20c_EV_SCEXH.RegCountry,
v20c_EV_SCEXH.Localcode,
v20c_EV_SCEXH.ListStatus,
v20c_EV_SCEXH.Listdate,
RD.Recdate,
EXDT.ExDate,
EXDT.PayDate,
v10s_INT.InterestFromDate,
v10s_INT.InterestToDate, 
v10s_INT.Days, 
INTPY.OptionID as OptionKey,
CASE WHEN (INTPY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(INTPY.CurenCD) > 0) THEN '[' + INTPY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
INTPY.IntRate,
INTPY.GrossInterest,
INTPY.NetInterest,
INTPY.DomesticTaxRate, 
INTPY.NonResidentTaxRate,
INTPY.RescindInterest,
INTPY.AgencyFees,
INTPY.CouponNo, 
INTPY.CouponID,
INTPY.bParValue,
INTPY.DefaultOpt,
INTPY.OptElectionDate,
'M' as Choice,
v10s_INT.INTNotes as Notes,
v20c_EVde_SCMST.IssID,
v20c_EVde_SCMST.SecID
into WcaArch.dbo.evf_Interest_Payment
FROM v10s_INT
INNER JOIN RD ON RD.RdID = v10s_INT.RdID
INNER JOIN v20c_EVde_SCMST ON RD.SecID = v20c_EVde_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVde_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_EV_SCEXH.ExchgCD = EXDT.ExchgCD AND 'INT' = EXDT.EventType
LEFT OUTER JOIN INTPY ON v10s_INT.EventID = INTPY.RDID
LEFT OUTER JOIN SectyGrp ON v20c_EVde_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN CUREN ON INTPY.CurenCD = CUREN.CurenCD
where (INTPY.Acttime BETWEEN @Startdate AND  '2007/01/01'
or v10s_INT.Acttime BETWEEN @Startdate AND  '2007/01/01'
or RD.Acttime BETWEEN @Startdate AND  '2007/01/01')
and intpy.optionID is not null
and (v10s_INT.Acttime > getdate()-50 or v10s_INT.actflag<>'D')
go

print ""
go

print " Generating evf_Interest_Payment ,please  wait ....."
GO 
use WcaArch 
ALTER TABLE evf_Interest_Payment ALTER COLUMN  caref bigint NOT NULL
GO 
use WcaArch 
ALTER TABLE [DBO].[evf_Interest_Payment] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evf_Interest_Payment] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WcaArch 
CREATE  INDEX [ix_secid_evf_Interest_Payment] ON [dbo].[evf_Interest_Payment]([secid]) ON [PRIMARY] 
GO 
use WcaArch 
CREATE  INDEX [ix_issid_evf_Interest_Payment] ON [dbo].[evf_Interest_Payment]([issid]) ON [PRIMARY]
GO
