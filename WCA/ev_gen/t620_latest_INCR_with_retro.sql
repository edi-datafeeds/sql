use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Dividend')
	drop table t620i_Dividend
use wca
select *
into wca2.dbo.t620i_Dividend
FROM v54f_620_Dividend
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Company_Meeting')
	drop table t620i_Company_Meeting
use wca
select *
into wca2.dbo.t620i_Company_Meeting
FROM v50f_620_Company_Meeting
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Call')
	drop table t620i_Call
use wca
select *
into wca2.dbo.t620i_Call
FROM v53f_620_Call
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Liquidation')
	drop table t620i_Liquidation
use wca
select *
into wca2.dbo.t620i_Liquidation
FROM v50f_620_Liquidation
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Certificate_Exchange')
	drop table t620i_Certificate_Exchange
use wca
select *
into wca2.dbo.t620i_Certificate_Exchange
FROM v51f_620_Certificate_Exchange
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_International_Code_Change')
	drop table t620i_International_Code_Change
use wca
select *
into wca2.dbo.t620i_International_Code_Change
FROM v51f_620_International_Code_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))
and not (RelatedEvent = 'CORR' and OldUscode <> '' and NewUscode = '')

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Conversion')
	drop table t620i_Conversion
use wca
select *
into wca2.dbo.t620i_Conversion
FROM v51f_620_Conversion_terms
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Preference_Redemption')
	drop table t620i_Preference_Redemption
use wca
select *
into wca2.dbo.t620i_Preference_Redemption
FROM v51f_620_Redemption_terms
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Security_Reclassification')
	drop table t620i_Security_Reclassification
use wca
select *
into wca2.dbo.t620i_Security_Reclassification
FROM v51f_620_Security_Reclassification
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Lot_Change')
	drop table t620i_Lot_Change
use wca
select *
into wca2.dbo.t620i_Lot_Change
FROM v52f_620_Lot_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Sedol_Change')
	drop table t620i_Sedol_Change
use wca
select *
into wca2.dbo.t620i_Sedol_Change
FROM v52f_620_Sedol_Change
where changed > @startdate
and (oldsedol<>newsedol
or oldexcountry<>newexcountry
or oldregcountry<>newregcountry)

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Buy_Back')
	drop table t620i_Buy_Back
use wca
select *
into wca2.dbo.t620i_Buy_Back
FROM v53f_620_Buy_Back
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Capital_Reduction')
	drop table t620i_Capital_Reduction
use wca
select *
into wca2.dbo.t620i_Capital_Reduction
FROM v53f_620_Capital_Reduction
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Takeover')
	drop table t620i_Takeover
use wca
select *
into wca2.dbo.t620i_Takeover
FROM v53f_620_Takeover
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Arrangement')
	drop table t620i_Arrangement
use wca
select *
into wca2.dbo.t620i_Arrangement
FROM v54f_620_Arrangement
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Bonus')
	drop table t620i_Bonus
use wca
select *
into wca2.dbo.t620i_Bonus
FROM v54f_620_Bonus
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Consolidation')
	drop table t620i_Consolidation
use wca
select *
into wca2.dbo.t620i_Consolidation
FROM v54f_620_Consolidation
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Demerger')
	drop table t620i_Demerger
use wca
select *
into wca2.dbo.t620i_Demerger
FROM v54f_620_Demerger
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Distribution')
	drop table t620i_Distribution
use wca
select *
into wca2.dbo.t620i_Distribution
FROM v54f_620_Distribution
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Divestment')
	drop table t620i_Divestment
use wca
select *
into wca2.dbo.t620i_Divestment
FROM v54f_620_Divestment
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Entitlement')
	drop table t620i_Entitlement
use wca
select *
into wca2.dbo.t620i_Entitlement
FROM v54f_620_Entitlement
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Merger')
	drop table t620i_Merger
use wca
select *
into wca2.dbo.t620i_Merger
FROM v54f_620_Merger
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Preferential_Offer')
	drop table t620i_Preferential_Offer
use wca
select *
into wca2.dbo.t620i_Preferential_Offer
FROM v54f_620_Preferential_Offer
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Purchase_Offer')
	drop table t620i_Purchase_Offer
use wca
select *
into wca2.dbo.t620i_Purchase_Offer
FROM v54f_620_Purchase_Offer
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Rights ')
	drop table t620i_Rights 
use wca
select *
into wca2.dbo.t620i_Rights 
FROM v54f_620_Rights
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Security_Swap')
	drop table t620i_Security_Swap
use wca
select *
into wca2.dbo.t620i_Security_Swap
FROM v54f_620_Security_Swap
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Subdivision')
	drop table t620i_Subdivision
use wca
select *
into wca2.dbo.t620i_Subdivision
FROM v54f_620_Subdivision
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Bankruptcy ')
	drop table t620i_Bankruptcy 
use wca
select *
into wca2.dbo.t620i_Bankruptcy 
FROM v50f_620_Bankruptcy 
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Financial_Year_Change')
	drop table t620i_Financial_Year_Change
use wca
select *
into wca2.dbo.t620i_Financial_Year_Change
FROM v50f_620_Financial_Year_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Incorporation_Change')
	drop table t620i_Incorporation_Change
use wca
select *
into wca2.dbo.t620i_Incorporation_Change
FROM v50f_620_Incorporation_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Issuer_Name_change')
	drop table t620i_Issuer_Name_change
use wca
select *
into wca2.dbo.t620i_Issuer_Name_change
FROM v50f_620_Issuer_Name_change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Class_Action')
	drop table t620i_Class_Action
use wca
select *
into wca2.dbo.t620i_Class_Action
FROM v50f_620_Class_Action
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Security_Description_Change')
	drop table t620i_Security_Description_Change
use wca
select *
into wca2.dbo.t620i_Security_Description_Change
FROM v51f_620_Security_Description_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Assimilation')
	drop table t620i_Assimilation
use wca
select *
into wca2.dbo.t620i_Assimilation
FROM v52f_620_Assimilation
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Listing_Status_Change')
	drop table t620i_Listing_Status_Change
use wca
select *
into wca2.dbo.t620i_Listing_Status_Change
FROM v52f_620_Listing_Status_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Local_Code_Change')
	drop table t620i_Local_Code_Change
use wca
select *
into wca2.dbo.t620i_Local_Code_Change
FROM v52f_620_Local_Code_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_New_Listing')
	drop table t620i_New_Listing
use wca
select *
into wca2.dbo.t620i_New_Listing
FROM v52f_620_New_Listing
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Announcement')
	drop table t620i_Announcement
use wca
select *
into wca2.dbo.t620i_Announcement
FROM v50f_620_Announcement
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Parvalue_Redenomination')
	drop table t620i_Parvalue_Redenomination
use wca
select *
into wca2.dbo.t620i_Parvalue_Redenomination
FROM v51f_620_Parvalue_Redenomination
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Currency_Redenomination')
	drop table t620i_Currency_Redenomination
use wca
select *
into wca2.dbo.t620i_Currency_Redenomination
FROM v51f_620_Currency_Redenomination
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Return_of_Capital')
	drop table t620i_Return_of_Capital
use wca
select *
into wca2.dbo.t620i_Return_of_Capital
FROM v53f_620_Return_of_Capital
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Dividend_Reinvestment_Plan')
	drop table t620i_Dividend_Reinvestment_Plan
use wca
select *
into wca2.dbo.t620i_Dividend_Reinvestment_Plan
FROM v54f_620_Dividend_Reinvestment_Plan
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Franking')
	drop table t620i_Franking
use wca
select *
into wca2.dbo.t620i_Franking
FROM v54f_620_Franking
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Conversion_Terms_Change')
	drop table t620i_Conversion_Terms_Change
use wca
select *
into wca2.dbo.t620i_Conversion_Terms_Change
FROM v51f_620_Conversion_Terms_Change
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go

use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Bonus_Rights')
	drop table t620i_Bonus_Rights
use wca
select *
into wca2.dbo.t620i_Bonus_Rights
FROM v54f_620_Bonus_Rights
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))

go


use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'tSCXi_Dividend')
	delete from tSCXi_Dividend
use wca2
insert into tSCXi_Dividend
select * FROM wca.dbo.v54f_SCX_Dividend
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))
go
use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
insert into tSCXi_Dividend
select * FROM wca.dbo.v54f_SCX_Interest_Dividend
where (changed >= @Startdate or (changed>'2019/06/12 11:00:00' and changed<'2019/06/12 16:00:00'))
go

