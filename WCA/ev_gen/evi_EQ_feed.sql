--filepath=o:\Datafeed\WCA\evi_eq\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.evi
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--ArchivePath=n:\WCA\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq from wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n


--# 1
USE WCA2
SELECT * 
FROM evi_Company_Meeting 

--# 2
USE WCA2
SELECT *
FROM evi_Call

--# 3
USE WCA2
SELECT * 
FROM evi_Liquidation

--# 4
USE WCA2
SELECT *
FROM evi_Certificate_Exchange

--# 5
USE WCA2
SELECT * 
FROM evi_International_Code_Change

--# 6
USE WCA2
SELECT * 
FROM evi_Preference_Conversion

--# 7
USE WCA2
SELECT * 
FROM evi_Preference_Redemption

--# 8
USE WCA2
SELECT * 
FROM evi_Security_Reclassification

--# 9
USE WCA2
SELECT * 
FROM evi_Lot_Change

--# 10
USE WCA2
SELECT * 
FROM evi_Sedol_Change

--# 11
USE WCA2
SELECT * 
FROM evi_Buy_Back

--# 12
USE WCA2
SELECT * 
FROM evi_Capital_Reduction

--# 14
USE WCA2
SELECT * 
FROM evi_Takeover

--# 15
USE WCA2
SELECT * 
FROM evi_Arrangement

--# 16
USE WCA2
SELECT * 
FROM evi_Bonus

--# 17
USE WCA2
SELECT * 
FROM evi_Bonus_Rights

--# 18
USE WCA2
SELECT * 
FROM evi_Consolidation

--# 19
USE WCA2
SELECT * 
FROM evi_Demerger

--# 20
USE WCA2
SELECT * 
FROM evi_Distribution

--# 21
USE WCA2
SELECT * 
FROM evi_Divestment

--# 22
USE WCA2
SELECT * 
FROM evi_Entitlement

--# 23
USE WCA2
SELECT * 
FROM evi_Merger

--# 24
USE WCA2
SELECT * 
FROM evi_Preferential_Offer

--# 25
USE WCA2
SELECT * 
FROM evi_Purchase_Offer

--# 26
USE WCA2
SELECT * 
FROM evi_Rights 

--# 27
USE WCA2
SELECT * 
FROM evi_Security_Swap 

--# 28
USE WCA2
SELECT *
FROM evi_Subdivision

--# 29
USE WCA2
SELECT *
FROM evi_Bankruptcy 

--# 30
USE WCA2
SELECT *
FROM evi_Financial_Year_Change

--# 31
USE WCA2
SELECT *
FROM evi_Incorporation_Change

--# 32
USE WCA2
SELECT *
FROM evi_Issuer_Name_change

--# 33
USE WCA2
SELECT *
FROM evi_Class_Action

--# 34
USE WCA2
SELECT *
FROM evi_Security_Description_Change

--# 35
USE WCA2
SELECT *
FROM evi_Assimilation

--# 36
USE WCA2
SELECT *
FROM evi_Listing_Status_Change

--# 37
USE WCA2
SELECT *
FROM evi_Local_Code_Change

--# 38
USE WCA2
SELECT * 
FROM evi_New_Listing

--# 39
USE WCA2
SELECT * 
FROM evi_Announcement

--# 40
USE WCA2
SELECT * 
FROM evi_Parvalue_Redenomination 

--# 41
USE WCA2
SELECT * 
FROM evi_Currency_Redenomination 

--# 42
USE WCA2
SELECT * 
FROM evi_Return_of_Capital 

--# 43
USE WCA2
SELECT * 
FROM evi_Dividend

--# 44
USE WCA2
SELECT * 
FROM evi_Dividend_Reinvestment_Plan

--# 45
USE WCA2
SELECT * 
FROM evi_Franking
