print ""
go

print " Generating evf_Depositary_Receipt_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = '2006/01/01'
if exists (select * from sysobjects where name = 'evf_Depositary_Receipt_Change')
 drop table evf_Depositary_Receipt_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_DRCHG.EventID as char(16)))as bigint) as CAref,
v10s_DRCHG.Levent,
v10s_DRCHG.EventID,
v10s_DRCHG.AnnounceDate as Created,
v10s_DRCHG.Acttime as Changed,
v10s_DRCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_DRCHG.EffectiveDate,
v10s_DRCHG.OldDRratio +':'+v10s_DRCHG.OldUNratio as OldRatio,
v10s_DRCHG.NewDRratio +':'+v10s_DRCHG.NewUNratio as NewRatio,
v10s_DRCHG.OldUNSecID,
v10s_DRCHG.NewUNSecID,
CASE WHEN (v10s_DRCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_DRCHG.EventType) > 0) THEN '[' + v10s_DRCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_DRCHG.OldDepbank,
v10s_DRCHG.NewDepBank,
v10s_DRCHG.OldDRtype,
v10s_DRCHG.NewDRtype,
v10s_DRCHG.OldLevel,
v10s_DRCHG.NewLevel,
v10s_DRCHG.DRCHGNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evf_Depositary_Receipt_Change
FROM v10s_DRCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_DRCHG.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_DRCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_DRCHG.Acttime BETWEEN @Startdate AND  '2099/01/01'
and (v10s_DRCHG.actflag<>'D')
go

print ""
go

print " Generating evf_Depositary_Receipt_Change ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evf_Depositary_Receipt_Change ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evf_Depositary_Receipt_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evf_Depositary_Receipt_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
CREATE  INDEX [ix_secid_evf_Depositary_Receipt_Change] ON [dbo].[evf_Depositary_Receipt_Change]([secid]) ON [PRIMARY] 
GO 
use WCA2 
CREATE  INDEX [ix_issid_evf_Depositary_Receipt_Change] ON [dbo].[evf_Depositary_Receipt_Change]([issid]) ON [PRIMARY]
GO
