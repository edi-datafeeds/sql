use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'tSCXi_Dividend')
	delete from tSCXi_Dividend
use wca2
insert into tSCXi_Dividend
select * FROM wca.dbo.v54f_SCX_Dividend
where changed >= @Startdate
go
use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
insert into tSCXi_Dividend
select * FROM wca.dbo.v54f_SCX_Interest_Dividend
where changed >= @Startdate
go

