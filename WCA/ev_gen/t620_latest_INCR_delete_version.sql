print " Generating wca2.dbo.t620i, please wait..."
go


use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Dividend')
	delete from wca2.dbo.t620i_Dividend
use wca
insert into wca2.dbo.t620i_Dividend
select * from v54f_620_Dividend
where changed >= @Startdate

go


use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Company_Meeting')
	delete from wca2.dbo.t620i_Company_Meeting
use wca
insert into wca2.dbo.t620i_Company_Meeting
select * from v50f_620_Company_Meeting
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Call')
	delete from wca2.dbo.t620i_Call
use wca
insert into wca2.dbo.t620i_Call
select * from v53f_620_Call
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Liquidation')
	delete from wca2.dbo.t620i_Liquidation
use wca
insert into wca2.dbo.t620i_Liquidation
select * from v50f_620_Liquidation
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Certificate_Exchange')
	delete from wca2.dbo.t620i_Certificate_Exchange
use wca
insert into wca2.dbo.t620i_Certificate_Exchange
select * from v51f_620_Certificate_Exchange
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_International_Code_Change')
	delete from wca2.dbo.t620i_International_Code_Change
use wca
insert into wca2.dbo.t620i_International_Code_Change
select * from v51f_620_International_Code_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Conversion_Terms')
	delete from wca2.dbo.t620i_Conversion_Terms
use wca
insert into wca2.dbo.t620i_Conversion_Terms
select * from v51f_620_Conversion_Terms
where changed >= @Startdate

go
use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Conversion_Terms_Change')
	delete from wca2.dbo.t620i_Conversion_Terms_Change
use wca
insert into wca2.dbo.t620i_Conversion_Terms_Change
select * from v51f_620_Conversion_Terms_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Redemption_Terms')
	delete from wca2.dbo.t620i_Redemption_Terms
use wca
insert into wca2.dbo.t620i_Redemption_Terms
select * from v51f_620_Redemption_Terms
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Security_Reclassification')
	delete from wca2.dbo.t620i_Security_Reclassification
use wca
insert into wca2.dbo.t620i_Security_Reclassification
select * from v51f_620_Security_Reclassification
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Lot_Change')
	delete from wca2.dbo.t620i_Lot_Change
use wca
insert into wca2.dbo.t620i_Lot_Change
select * from v52f_620_Lot_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Sedol_Change')
	delete from wca2.dbo.t620i_Sedol_Change
use wca
insert into wca2.dbo.t620i_Sedol_Change
select * from v52f_620_Sedol_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Buy_Back')
	delete from wca2.dbo.t620i_Buy_Back
use wca
insert into wca2.dbo.t620i_Buy_Back
select * from v53f_620_Buy_Back
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Capital_Reduction')
	delete from wca2.dbo.t620i_Capital_Reduction
use wca
insert into wca2.dbo.t620i_Capital_Reduction
select * from v53f_620_Capital_Reduction
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Takeover')
	delete from wca2.dbo.t620i_Takeover
use wca
insert into wca2.dbo.t620i_Takeover
select * from v53f_620_Takeover
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Arrangement')
	delete from wca2.dbo.t620i_Arrangement
use wca
insert into wca2.dbo.t620i_Arrangement
select * from v54f_620_Arrangement
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Bonus')
	delete from wca2.dbo.t620i_Bonus
use wca
insert into wca2.dbo.t620i_Bonus
select * from v54f_620_Bonus
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Bonus_Rights')
	delete from wca2.dbo.t620i_Bonus_Rights
use wca
insert into wca2.dbo.t620i_Bonus_Rights
select * from v54f_620_Bonus_Rights
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Consolidation')
	delete from wca2.dbo.t620i_Consolidation
use wca
insert into wca2.dbo.t620i_Consolidation
select * from v54f_620_Consolidation
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Demerger')
	delete from wca2.dbo.t620i_Demerger
use wca
insert into wca2.dbo.t620i_Demerger
select * from v54f_620_Demerger
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Distribution')
	delete from wca2.dbo.t620i_Distribution
use wca
insert into wca2.dbo.t620i_Distribution
select * from v54f_620_Distribution
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Divestment')
	delete from wca2.dbo.t620i_Divestment
use wca
insert into wca2.dbo.t620i_Divestment
select * from v54f_620_Divestment
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Entitlement')
	delete from wca2.dbo.t620i_Entitlement
use wca
insert into wca2.dbo.t620i_Entitlement
select * from v54f_620_Entitlement
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Merger')
	delete from wca2.dbo.t620i_Merger
use wca
insert into wca2.dbo.t620i_Merger
select * from v54f_620_Merger
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Preferential_Offer')
	delete from wca2.dbo.t620i_Preferential_Offer
use wca
insert into wca2.dbo.t620i_Preferential_Offer
select * from v54f_620_Preferential_Offer
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Purchase_Offer')
	delete from wca2.dbo.t620i_Purchase_Offer
use wca
insert into wca2.dbo.t620i_Purchase_Offer
select * from v54f_620_Purchase_Offer
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Rights ')
	delete from wca2.dbo.t620i_Rights 
use wca
insert into wca2.dbo.t620i_Rights 
select * from v54f_620_Rights
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Security_Swap')
	delete from wca2.dbo.t620i_Security_Swap
use wca
insert into wca2.dbo.t620i_Security_Swap
select * from v54f_620_Security_Swap
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Subdivision')
	delete from wca2.dbo.t620i_Subdivision
use wca
insert into wca2.dbo.t620i_Subdivision
select * from v54f_620_Subdivision
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Bankruptcy ')
	delete from wca2.dbo.t620i_Bankruptcy 
use wca
insert into wca2.dbo.t620i_Bankruptcy 
select * from v50f_620_Bankruptcy 
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Financial_Year_Change')
	delete from wca2.dbo.t620i_Financial_Year_Change
use wca
insert into wca2.dbo.t620i_Financial_Year_Change
select * from v50f_620_Financial_Year_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Incorporation_Change')
	delete from wca2.dbo.t620i_Incorporation_Change
use wca
insert into wca2.dbo.t620i_Incorporation_Change
select * from v50f_620_Incorporation_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Issuer_Name_change')
	delete from wca2.dbo.t620i_Issuer_Name_change
use wca
insert into wca2.dbo.t620i_Issuer_Name_change
select * from v50f_620_Issuer_Name_change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Class_Action')
	delete from wca2.dbo.t620i_Class_Action
use wca
insert into wca2.dbo.t620i_Class_Action
select * from v50f_620_Class_Action
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Security_Description_Change')
	delete from wca2.dbo.t620i_Security_Description_Change
use wca
insert into wca2.dbo.t620i_Security_Description_Change
select * from v51f_620_Security_Description_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Assimilation')
	delete from wca2.dbo.t620i_Assimilation
use wca
insert into wca2.dbo.t620i_Assimilation
select * from v52f_620_Assimilation
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Listing_Status_Change')
	delete from wca2.dbo.t620i_Listing_Status_Change
use wca
insert into wca2.dbo.t620i_Listing_Status_Change
select * from v52f_620_Listing_Status_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Local_Code_Change')
	delete from wca2.dbo.t620i_Local_Code_Change
use wca
insert into wca2.dbo.t620i_Local_Code_Change
select * from v52f_620_Local_Code_Change
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_New_Listing')
	delete from wca2.dbo.t620i_New_Listing
use wca
insert into wca2.dbo.t620i_New_Listing
select * from v52f_620_New_Listing
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Announcement')
	delete from wca2.dbo.t620i_Announcement
use wca
insert into wca2.dbo.t620i_Announcement
select * from v50f_620_Announcement
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Parvalue_Redenomination')
	delete from wca2.dbo.t620i_Parvalue_Redenomination
use wca
insert into wca2.dbo.t620i_Parvalue_Redenomination
select * from v51f_620_Parvalue_Redenomination
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Currency_Redenomination')
	delete from wca2.dbo.t620i_Currency_Redenomination
use wca
insert into wca2.dbo.t620i_Currency_Redenomination
select * from v51f_620_Currency_Redenomination
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Return_of_Capital')
	delete from wca2.dbo.t620i_Return_of_Capital
use wca
insert into wca2.dbo.t620i_Return_of_Capital
select * from v53f_620_Return_of_Capital
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Dividend_Reinvestment_Plan')
	delete from wca2.dbo.t620i_Dividend_Reinvestment_Plan
use wca
insert into wca2.dbo.t620i_Dividend_Reinvestment_Plan
select * from v54f_620_Dividend_Reinvestment_Plan
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Franking')
	delete from wca2.dbo.t620i_Franking
use wca
insert into wca2.dbo.t620i_Franking
select * from v54f_620_Franking
where changed >= @Startdate

go

use wca
Declare @StartDate datetime
use wca2
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 't620i_Bonus_Rights')
	delete from wca2.dbo.t620i_Bonus_Rights
use wca
insert into wca2.dbo.t620i_Bonus_Rights
select * from v54f_620_Bonus_Rights
where changed >= @Startdate

go
