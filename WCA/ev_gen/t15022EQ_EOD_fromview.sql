print " Generating t50f_ISO_BRUP, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't50f_ISO_BRUP')
 drop table t50f_ISO_BRUP

use wca
select 
into wca2.dbo.t50f_ISO_BRUP
from v10s_BKRP

where v10s_BKRP.Acttime >= @Startdate
go


print " Generating t50f_ISO_CLSA, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't50f_ISO_CLSA')
 drop table t50f_ISO_CLSA

use wca
Select 
into wca2.dbo.t50f_ISO_CLSA
from v10s_LAWST
where v10s_LAWST.Acttime >= @Startdate
go


print " Generating t50f_ISO_NAME, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't50f_ISO_NAME')
 drop table t50f_ISO_NAME

use wca
Select 
into wca2.dbo.t50f_ISO_NAME
from v10s_ISCHG
where v10s_ISCHG.Acttime >= @Startdate
go


print " Generating t54f_ISO_EXWA, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_EXWA')
 drop table t54f_ISO_EXWA

use wca
Select 
into wca2.dbo.t54f_ISO_EXWA
from v10s_WARTM
where (v10s_WAREX.Acttime >= @Startdate
or v10s_WAREX.Acttime >= @Startdate)

go


print " Generating t50f_ISO_LIQU, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't50f_ISO_LIQU')
 drop table t50f_ISO_LIQU

use wca
Select 
into wca2.dbo.t50f_ISO_LIQU
from v10s_LIQ
where (MPAY.Acttime >= @Startdate
or v10s_LIQ.Acttime >= @Startdate)
go


print " Generating t54f_ISO_D, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_D')
 drop table t54f_ISO_D

use wca
Select 
into wca2.dbo.t54f_ISO_D
from v10s_DIV
where changed >= @Startdate

go


print " Generating t54f_ISO_CONV, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_CONV')
 drop table t54f_ISO_CONV

use wca
Select 
into wca2.dbo.t54f_ISO_CONV
from v10s_CONV
where v10s_CONV.Acttime >= @Startdate

go


print " Generating t54f_ISO_EXOF, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_EXOF')
 drop table t54f_ISO_EXOF

use wca
Select 
into wca2.dbo.t54f_ISO_EXOF
from v10s_CTX
where v10s_CTX.Acttime >= @Startdate
go


print " Generating t54f_ISO_BIDS, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_BIDS')
 drop table t54f_ISO_BIDS

use wca
Select 
into wca2.dbo.t54f_ISO_BIDS
from v20c_610_SCMST
where (MPAY.Acttime >= @Startdate
or v10s_BB.Acttime >= @Startdate)
go


print " Generating t54f_ISO_ACTV, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_ACTV')
 drop table t54f_ISO_ACTV

use wca
Select 
into wca2.dbo.t54f_ISO_ACTV
from v10s_LSTAT
where v10s_LSTAT.Acttime >= @Startdate
go


print " Generating t54f_ISO_PARI, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_PARI')
 drop table t54f_ISO_PARI

use wca
Select 
into wca2.dbo.t54f_ISO_PARI
from v10s_ASSM
where v10s_ASSM.Acttime >= @Startdate
go


print " Generating t54f_ISO_DECR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_DECR')
 drop table t54f_ISO_DECR

use wca
Select 
into wca2.dbo.t54f_ISO_DECR
from v20c_610_SCMST
where (v10s_CAPRD.Acttime >= @Startdate
go


print " Generating t54f_ISO_MRGR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_MRGR')
 drop table t54f_ISO_MRGR

use wca
Select 
into wca2.dbo.t54f_ISO_MRGR
from v10s_MRGR
where (MPAY.Acttime >= @Startdate
go

print " Generating t54f_ISO_INCR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_INCR')
 drop table t54f_ISO_INCR

use wca
Select 
into wca2.dbo.t54f_ISO_INCR
from v20c_610_SCMST
where v10s_RCAP.Acttime >= @Startdate
go


print " Generating t54f_ISO_PPMT, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_PPMT')
 drop table t54f_ISO_PPMT

use wca
Select 
into wca2.dbo.t54f_ISO_PPMT
from v20c_610_SCMST
where V10s_CALL.Acttime >= @Startdate
go


print " Generating t54f_ISO_TEND_TKOVR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_TEND_TKOVR')
 drop table t54f_ISO_TEND_TKOVR

use wca
Select 
into wca2.dbo.t54f_ISO_TEND_TKOVR
from v10s_Tkovr
where (MPAY.Acttime >= @Startdate
go


print " Generating t54f_ISO_BONU, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_BONU')
 drop table t54f_ISO_BONU

use wca
Select 
into wca2.dbo.t54f_ISO_BONU
from v10s_BON
where (EXDT.Acttime >= @Startdate
go


print " Generating t54f_ISO_DVOP, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_DVOP')
 drop table t54f_ISO_DVOP

use wca
Select 
into wca2.dbo.t54f_ISO_DVOP
from v10s_DIV
where (DP1.Acttime >= @Startdate
go


print " Generating t54f_ISO_RHTSn, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_RHTSn')
 drop table t54f_ISO_RHTSn

use wca
Select 
into wca2.dbo.t54f_ISO_RHTSn
from v10s_ENT
where (EXDT.Acttime >= @Startdate


go


print " Generating t54f_ISO_RHTSt, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_RHTSt')
 drop table t54f_ISO_RHTSt

use wca
Select 
into wca2.dbo.t54f_ISO_RHTSt
from v10s_RTS
where (EXDT.Acttime >= @Startdate
go


print " Generating t54f_ISO_OTHR_ARR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_OTHR_ARR')
 drop table t54f_ISO_OTHR_ARR

use wca
Select 
into wca2.dbo.t54f_ISO_OTHR_ARR
from v10s_ARR
where (EXDT.Acttime >= @Startdate
go


print " Generating t54f_ISO_OTHR_DVST, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_OTHR_DVST')
 drop table t54f_ISO_OTHR_DVST

use wca
Select 
into wca2.dbo.t54f_ISO_OTHR_DVST
from v10s_DVST
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_DVST.Acttime >= @Startdate)
go


print " Generating t54f_ISO_PRIO, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_PRIO')
 drop table t54f_ISO_PRIO

use wca
Select 
into wca2.dbo.t54f_ISO_PRIO
from v10s_PRF
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_PRF.Acttime >= @Startdate)
go

print " Generating t54f_ISO_SOFF_DIST, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_SOFF_DIST')
 drop table t54f_ISO_SOFF_DIST

use wca
Select 
into wca2.dbo.t54f_ISO_SOFF_DIST
from v10s_DIST
where (MPAY.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_DIST.Acttime >= @Startdate)
go


print " Generating t54f_ISO_SOFF_DMRGR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_SOFF_DMRGR')
 drop table t54f_ISO_SOFF_DMRGR

use wca
Select 
into wca2.dbo.t54f_ISO_SOFF_DMRGR
from v10s_DMRGR
where (MPAY.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_DMRGR.Acttime >= @Startdate)
go


print " Generating t54f_ISO_SPLF, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_SPLF')
 drop table t54f_ISO_SPLF

use wca
Select 
into wca2.dbo.t54f_ISO_SPLF
from v10s_SD
where (v10s_SD.Acttime >= @Startdate
or ICC.Acttime >= @Startdate
or SDCHG.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate)

go


print " Generating t54f_ISO_SPLR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_SPLR')
 drop table t54f_ISO_SPLR

use wca
Select 
where (v10s_CONSD.Acttime >= @Startdate
go

print " Generating t54f_ISO_TEND_PO, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 't54f_ISO_TEND_PO')
 drop table t54f_ISO_TEND_PO

use wca
Select 
into wca2.dbo.t54f_ISO_TEND_PO
from v10s_PO
where
(v10s_PO.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate)
