
print " "
go
print " Generating evi_Basket_Constituent_Change, please wait..."
go

use WCA2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Basket_Constituent_Change')
 drop table evi_Basket_Constituent_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(EventID as char(8))),len(EventID),7) as bigint) as CAref,
v10s_BSKCC.Levent,
v10s_BSKCC.EventID,
v10s_BSKCC.AnnounceDate as Created,
v10s_BSKCC.Acttime as Changed,
v10s_BSKCC.ActFlag,
v20c_EVwa_SCMST.CntryofIncorp,
v20c_EVwa_SCMST.IssuerName,
v20c_EVwa_SCMST.SecurityDesc,
v20c_EVwa_SCMST.Parvalue,
v20c_EVwa_SCMST.PVCurrency,
v20c_EVwa_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EVwa_SCMST.USCode,
v20c_EVwa_SCMST.StatusFlag,
v20c_EVwa_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVwa_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_BSKCC.EffectiveDate,
v10s_BSKCC.OshinBskt,
v10s_BSKCC.NshinBskt,
CASE WHEN (v10s_BSKCC.RelEventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_BSKCC.RelEventType) > 0) THEN '[' + v10s_BSKCC.RelEventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_BSKCC.UnSecID as UnderlyingSecID,
Scmst.ISIN as UnderlyingIsin,
sedolseq1.Sedol as UnderlyingSedol,
v10s_BSKCC.Notes,
' ' as Choice,
v20c_EVwa_SCMST.IssID,
v20c_EVwa_SCMST.SecID
into WCA2.dbo.evi_Basket_Constituent_Change
FROM v10s_BSKCC
INNER JOIN v20c_EVwa_SCMST ON v20c_EVwa_SCMST.SecID = v10s_BSKCC.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVwa_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EVwa_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN SCMST ON v10s_BSKCC.UnSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_BSKCC.UnSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN EVENT ON v10s_BSKCC.RelEventType = EVENT.EventType
where
v10s_BSKCC.Acttime BETWEEN @Startdate AND  '2099/01/01' 
and v10s_BSKCC.actflag<>'Z'
go


print " "
go
Print " Generating evi_Basket_Constituent_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Basket_Constituent_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Basket_Constituent_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Basket_Constituent_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Basket_Constituent_Change] ON [dbo].[evi_Basket_Constituent_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Basket_Constituent_Change] ON [dbo].[evi_Basket_Constituent_Change]([issid]) ON [PRIMARY]
GO


print " "
go
print " Generating evi_Basket_Warrant, please wait..."
go


use WCA2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Basket_Warrant')
 drop table evi_Basket_Warrant
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_BSKWT.EventID as char(8))),len(v10s_BSKWT.EventID),7)+substring('000000'+rtrim(cast(BSKWC.UnSecID as char(8))),len(BSKWC.UnSecID),7) as bigint) as CAref,
v10s_BSKWT.Levent,
v10s_BSKWT.EventID,
v10s_BSKWT.AnnounceDate as Created,
CASE WHEN (BSKWC.Acttime is not null) and (BSKWC.Acttime > v10s_BSKWT.Acttime) THEN BSKWC.Acttime ELSE v10s_BSKWT.Acttime END as Changed,
v10s_BSKWT.ActFlag,
v20c_EVwa_SCMST.CntryofIncorp,
v20c_EVwa_SCMST.IssuerName,
v20c_EVwa_SCMST.SecurityDesc,
v20c_EVwa_SCMST.Parvalue,
v20c_EVwa_SCMST.PVCurrency,
v20c_EVwa_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EVwa_SCMST.USCode,
v20c_EVwa_SCMST.StatusFlag,
v20c_EVwa_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVwa_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_BSKWT.CallPut = 'C') THEN 'Call' WHEN (v10s_BSKWT.CallPut = 'P') THEN 'Put' ELSE '' END as CallPut,
CASE WHEN (v10s_BSKWT.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_BSKWT.CurenCD) > 0) THEN '[' + v10s_BSKWT.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_BSKWT.ExpirationDate,
v10s_BSKWT.StrikePrice,
v10s_BSKWT.WarRatio,
v10s_BSKWT.UnRatio,
CASE WHEN (v10s_BSKWT.ExerciseStyle is null) THEN irEXSTYLE.Lookup WHEN (irEXSTYLE.Lookup is null) and (LEN(v10s_BSKWT.ExerciseStyle) > 0) THEN '[' + v10s_BSKWT.ExerciseStyle +'] not found' ELSE irEXSTYLE.Lookup END as ExerciseStyle,
CASE WHEN (v10s_BSKWT.CashStock = 'C') THEN 'Cash' WHEN (v10s_BSKWT.CashStock = 'S') THEN 'Stock' ELSE '' END as CashStock,
v10s_BSKWT.IssDate as IssueDate,
BSKWC.UnSecID as UnderlyingSecID,
Scmst.ISIN as UnderlyingIsin,
sedolseq1.Sedol as UnderlyingSedol,
BSKWC.ShinBskt,
v10s_BSKWT.Notes,
' ' as Choice,
v20c_EVwa_SCMST.IssID,
v20c_EVwa_SCMST.SecID
into WCA2.dbo.evi_Basket_Warrant
FROM v10s_BSKWT
INNER JOIN v20c_EVwa_SCMST ON v20c_EVwa_SCMST.SecID = v10s_BSKWT.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVwa_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN BSKWC ON v10s_BSKWT.EventID = BSKWC.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EVwa_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN SCMST ON BSKWC.UnSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON BSKWC.UnSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN CUREN ON v10s_BSKWT.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN irEXSTYLE ON v10s_BSKWT.ExerciseStyle = irEXSTYLE.Code
where
(v10s_BSKWT.Acttime BETWEEN @Startdate AND  '2099/01/01' or BSKWC.Acttime BETWEEN @Startdate AND  '2099/01/01')
and v10s_BSKWT.actflag<>'Z'


go

print " "
go
Print " Generating evi_Basket_Warrant ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evi_Basket_Warrant ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evi_Basket_Warrant] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Basket_Warrant] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
--CREATE  INDEX [ix_secid_evi_Basket_Warrant] ON [dbo].[evi_Basket_Warrant]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
--CREATE  INDEX [ix_issid_evi_Basket_Warrant] ON [dbo].[evi_Basket_Warrant]([issid]) ON [PRIMARY]
GO

