print " "
go
print " Generating evf_Warrant_Terms, please wait..."
go

use WCA2 Declare @StartDate datetime
set @StartDate = '2007/01/01'
if exists (select * from sysobjects where name = 'evf_Warrant_Terms')
 drop table evf_Warrant_Terms

use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(EventID as char(8))),len(EventID),7)+rtrim(cast(v20c_EVwa_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_WARTM.Levent,
EventID,
v10s_WARTM.AnnounceDate as Created,
v10s_WARTM.Acttime as Changed,
v10s_WARTM.ActFlag,
v20c_EVwa_SCMST.CntryofIncorp,
v20c_EVwa_SCMST.IssuerName,
v20c_EVwa_SCMST.SecurityDesc,
v20c_EVwa_SCMST.Parvalue,
v20c_EVwa_SCMST.PVCurrency,
v20c_EVwa_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EVwa_SCMST.USCode,
v20c_EVwa_SCMST.StatusFlag,
v20c_EVwa_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVwa_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_WARTM.IssueDate,
v10s_WARTM.ExpirationDate,
v10s_WARTM.RedemptionDate,
CASE WHEN (v10s_WARTM.ExerciseStyle is null) THEN irEXSTYLE.Lookup WHEN (irEXSTYLE.Lookup is null) and (LEN(v10s_WARTM.ExerciseStyle) > 0) THEN '[' + v10s_WARTM.ExerciseStyle +'] not found' ELSE irEXSTYLE.Lookup END as ExerciseStyle,
v10s_WARTM.wtExerSecID as ExerciseSecID,
Scmst.ISIN as ExerciseIsin,
sedolseq1.sedol as ExerciseSedol,
v10s_WARTM.WartmNotes as Notes,
v20c_EVwa_SCMST.IssID,
v20c_EVwa_SCMST.SecID
into WCA2.dbo.evf_Warrant_Terms
FROM v10s_WARTM
INNER JOIN v20c_EVwa_SCMST ON v20c_EVwa_SCMST.SecID = v10s_WARTM.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVwa_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_WARTM.wtExerSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_WARTM.wtExerSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN irEXSTYLE ON v10s_WARTM.ExerciseStyle = irEXSTYLE.Code
LEFT OUTER JOIN SectyGrp ON v20c_EVwa_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_WARTM.Acttime BETWEEN @Startdate AND  '2099/01/01' 
and v10s_WARTM.actflag<>'D'
go

print " "
go
Print " Generating evf_Warrant_Terms ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evf_Warrant_Terms ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evf_Warrant_Terms] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evf_Warrant_Terms] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
CREATE  INDEX [ix_secid_evf_Warrant_Terms] ON [dbo].[evf_Warrant_Terms]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
CREATE  INDEX [ix_issid_evf_Warrant_Terms] ON [dbo].[evf_Warrant_Terms]([issid]) ON [PRIMARY]
GO



print " "
go
print " Generating evf_Warrant_Terms_Change, please wait..."
go

use WCA2 Declare @StartDate datetime
set @StartDate = '2007/01/01'
if exists (select * from sysobjects where name = 'evf_Warrant_Terms_Change')
 drop table evf_Warrant_Terms_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(EventID as char(8))),len(EventID),7) as bigint) as CAref,
v10s_WTCHG.Levent,
v10s_WTCHG.EventID,
v10s_WTCHG.AnnounceDate as Created,
v10s_WTCHG.Acttime as Changed,
v10s_WTCHG.ActFlag,
v20c_EVwa_SCMST.CntryofIncorp,
v20c_EVwa_SCMST.IssuerName,
v20c_EVwa_SCMST.SecurityDesc,
v20c_EVwa_SCMST.Parvalue,
v20c_EVwa_SCMST.PVCurrency,
v20c_EVwa_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EVwa_SCMST.USCode,
v20c_EVwa_SCMST.StatusFlag,
v20c_EVwa_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVwa_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_WTCHG.RelEventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_WTCHG.RelEventType) > 0) THEN '[' + v10s_WTCHG.RelEventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_WTCHG.NotificationDate,
v10s_WTCHG.OldExpirationDate,
v10s_WTCHG.NewExpirationDate,
v10s_WTCHG.OldRedemptionDate,
v10s_WTCHG.NewRedemptionDate,
v10s_WTCHG.OldExerSecID as OldExerciseSecID,
Scmst.ISIN as OldExerciseIsin,
sedolold.Sedol as OldExerciseSedol,
v10s_WTCHG.NewExerSecID as NewExerciseSecID,
v09b_Scmst.ISIN as NewExerciseIsin,
sedolnew.Sedol as NewExerciseSedol,
' ' as Choice,
v10s_WTCHG.WTCHGNotes as Notes,
v20c_EVwa_SCMST.IssID,
v20c_EVwa_SCMST.SecID
into WCA2.dbo.evf_Warrant_Terms_Change
FROM v10s_WTCHG
INNER JOIN v20c_EVwa_SCMST ON v20c_EVwa_SCMST.SecID = v10s_WTCHG.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVwa_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EVwa_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN v09b_SCMST ON v10s_WTCHG.NewExerSecID = v09b_SCMST.SecID
LEFT OUTER JOIN SCMST ON v10s_WTCHG.OldExerSecID = SCMST.SecID
LEFT OUTER JOIN EVENT ON v10s_WTCHG.RelEventType = EVENT.EventType
LEFT OUTER JOIN sedolseq1 as sedolold ON v10s_WTCHG.OldExerSecID = sedolold.SecID
                   and 1 = sedolold.seqnum
                   and v20c_EV_scexh.ExCountry = sedolold.CntryCD
LEFT OUTER JOIN sedolseq1 as sedolnew ON v10s_WTCHG.NewExerSecID = sedolnew.SecID
                   and 1 = sedolnew.seqnum
                   and v20c_EV_scexh.ExCountry = sedolnew.CntryCD

where
v10s_WTCHG.Acttime BETWEEN @Startdate AND  '2099/01/01' 
and v10s_WTCHG.actflag<>'D'
go

print " "
go
Print " Generating evf_Warrant_Terms_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evf_Warrant_Terms_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evf_Warrant_Terms_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evf_Warrant_Terms_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
CREATE  INDEX [ix_secid_evf_Warrant_Terms_Change] ON [dbo].[evf_Warrant_Terms_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
CREATE  INDEX [ix_issid_evf_Warrant_Terms_Change] ON [dbo].[evf_Warrant_Terms_Change]([issid]) ON [PRIMARY]
GO



print " "
go
print " Generating evf_Warrant_Exercise_Change, please wait..."
go

use WCA2 Declare @StartDate datetime
set @StartDate = '2007/01/01'
if exists (select * from sysobjects where name = 'evf_Warrant_Exercise_Change')
 drop table evf_Warrant_Exercise_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(EventID as char(8))),len(EventID),7) as bigint) as CAref,
v10s_WXCHG.Levent,
v10s_WXCHG.EventID,
v10s_WXCHG.AnnounceDate as Created,
v10s_WXCHG.Acttime as Changed,
v10s_WXCHG.ActFlag,
v20c_EVwa_SCMST.CntryofIncorp,
v20c_EVwa_SCMST.IssuerName,
v20c_EVwa_SCMST.SecurityDesc,
v20c_EVwa_SCMST.Parvalue,
v20c_EVwa_SCMST.PVCurrency,
v20c_EVwa_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EVwa_SCMST.USCode,
v20c_EVwa_SCMST.StatusFlag,
v20c_EVwa_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVwa_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_WXCHG.RelEventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_WXCHG.RelEventType) > 0) THEN '[' + v10s_WXCHG.RelEventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_WXCHG.NotificationDate,
v10s_WXCHG.OldFromDate,
v10s_WXCHG.NewFromDate,
v10s_WXCHG.OldToDate,
v10s_WXCHG.NewToDate,
v10s_WXCHG.ExerRatioOld as ExerciseRatioOld,
v10s_WXCHG.ExerRatioNew as ExerciseRatioNew,
v10s_WXCHG.WarRatioOld as WarrantRatioOld,
v10s_WXCHG.WarRatioNew as WarrantRatioNew,
CASE WHEN (v10s_WXCHG.OldCurenCD is null) THEN oldCUREN.Currency WHEN (oldCUREN.Currency is null) and (LEN(v10s_WXCHG.OldCurenCD) > 0) THEN '[' + v10s_WXCHG.OldCurenCD +'] not found' ELSE oldCUREN.Currency END as OldCurrency,
CASE WHEN (v10s_WXCHG.NewCurenCD is null) THEN newCUREN.Currency WHEN (newCUREN.Currency is null) and (LEN(v10s_WXCHG.NewCurenCD) > 0) THEN '[' + v10s_WXCHG.NewCurenCD +'] not found' ELSE newCUREN.Currency END as NewCurrency,
v10s_WXCHG.OldStrikePrice,
v10s_WXCHG.NewStrikePrice,
v10s_WXCHG.OldPricePerShare,
v10s_WXCHG.NewPricePerShare,
v10s_WXCHG.OldExerSecID as OldExerciseSecID,
Scmst.ISIN as OldExerciseIsin,
sedolold.Sedol as OldExerciseSedol,
v10s_WXCHG.NewExerSecID as NewExerciseSecID,
v09b_Scmst.ISIN as NewExerciseIsin,
sedolnew.Sedol as NewExerciseSedol,
' ' as Choice,
v20c_EVwa_SCMST.IssID,
v20c_EVwa_SCMST.SecID
into WCA2.dbo.evf_Warrant_Exercise_Change
FROM v10s_WXCHG
INNER JOIN v20c_EVwa_SCMST ON v20c_EVwa_SCMST.SecID = v10s_WXCHG.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVwa_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EVwa_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN v09b_SCMST ON v10s_WXCHG.NewExerSecID = v09b_SCMST.SecID
LEFT OUTER JOIN SCMST ON v10s_WXCHG.OldExerSecID = SCMST.SecID
LEFT OUTER JOIN EVENT ON v10s_WXCHG.RelEventType = EVENT.EventType
LEFT OUTER JOIN sedolseq1 as sedolold ON v10s_WXCHG.OldExerSecID = sedolold.SecID
                   and 1 = sedolold.seqnum
                   and v20c_EV_scexh.ExCountry = sedolold.CntryCD
LEFT OUTER JOIN sedolseq1 as sedolnew ON v10s_WXCHG.NewExerSecID = sedolnew.SecID
                   and 1 = sedolnew.seqnum
                   and v20c_EV_scexh.ExCountry = sedolnew.CntryCD
LEFT OUTER JOIN CUREN as oldCUREN ON v10s_WXCHG.OldCurenCD = oldCUREN.CurenCD
LEFT OUTER JOIN CUREN as newCUREN ON v10s_WXCHG.NewCurenCD = newCUREN.CurenCD
where
v10s_WXCHG.Acttime BETWEEN @Startdate AND  '2099/01/01' 
and v10s_WXCHG.actflag<>'D'
go

print " "
go
Print " Generating evf_Warrant_Exercise_Change ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evf_Warrant_Exercise_Change ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evf_Warrant_Exercise_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evf_Warrant_Exercise_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
CREATE  INDEX [ix_secid_evf_Warrant_Exercise_Change] ON [dbo].[evf_Warrant_Exercise_Change]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
CREATE  INDEX [ix_issid_evf_Warrant_Exercise_Change] ON [dbo].[evf_Warrant_Exercise_Change]([issid]) ON [PRIMARY]
GO



print " "
go
print " Generating evf_Warrant_Exercise, please wait..."
go

use WCA2 Declare @StartDate datetime
set @StartDate = '2007/01/01'
if exists (select * from sysobjects where name = 'evf_Warrant_Exercise')
 drop table evf_Warrant_Exercise
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(EventID as char(8))),len(EventID),7) as bigint) as CAref,
v10s_WAREX.Levent,
v10s_WAREX.EventID,
v10s_WAREX.AnnounceDate as Created,
v10s_WAREX.Acttime as Changed,
v10s_WAREX.ActFlag,
v20c_EVwa_SCMST.CntryofIncorp,
v20c_EVwa_SCMST.IssuerName,
v20c_EVwa_SCMST.SecurityDesc,
v20c_EVwa_SCMST.Parvalue,
v20c_EVwa_SCMST.PVCurrency,
v20c_EVwa_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EVwa_SCMST.USCode,
v20c_EVwa_SCMST.StatusFlag,
v20c_EVwa_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVwa_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_WAREX.Fromdate,
v10s_WAREX.Todate,
v10s_WAREX.Rationew,
v10s_WAREX.Ratioold,
CASE WHEN (v10s_WAREX.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_WAREX.CurenCD) > 0) THEN '[' + v10s_WAREX.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_WAREX.CurenCD,
v10s_WAREX.StrikePrice,
v10s_WAREX.PricePerShare,
v10s_WAREX.ExerSecID as ExerciseSecID,
Scmst.ISIN as ExerciseIsin,
sedolseq1.Sedol as ExerciseSedol,
' ' as Choice,
v20c_EVwa_SCMST.IssID,
v20c_EVwa_SCMST.SecID
into WCA2.dbo.evf_Warrant_Exercise
FROM v10s_WAREX
INNER JOIN v20c_EVwa_SCMST ON v20c_EVwa_SCMST.SecID = v10s_WAREX.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVwa_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EVwa_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN SCMST ON v10s_WAREX.ExerSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_WAREX.ExerSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN CUREN ON v10s_WAREX.CurenCD = CUREN.CurenCD
where
v10s_WAREX.Acttime BETWEEN @Startdate AND  '2099/01/01' 
and v10s_WAREX.actflag<>'D'
go

print " "
go
Print " Generating evf_Warrant_Exercise ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evf_Warrant_Exercise ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evf_Warrant_Exercise] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evf_Warrant_Exercise] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
CREATE  INDEX [ix_secid_evf_Warrant_Exercise] ON [dbo].[evf_Warrant_Exercise]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
CREATE  INDEX [ix_issid_evf_Warrant_Exercise] ON [dbo].[evf_Warrant_Exercise]([issid]) ON [PRIMARY]
GO




print " "
go
print " Generating evf_Covered_Warrant, please wait..."
go

use WCA2 Declare @StartDate datetime
set @StartDate = '2007/01/01'
if exists (select * from sysobjects where name = 'evf_Covered_Warrant')
 drop table evf_Covered_Warrant
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(EventID as char(8))),len(EventID),7) as bigint) as CAref,
v10s_COWAR.Levent,
v10s_COWAR.EventID,
v10s_COWAR.AnnounceDate as Created,
v10s_COWAR.Acttime as Changed,
v10s_COWAR.ActFlag,
v20c_EVwa_SCMST.CntryofIncorp,
v20c_EVwa_SCMST.IssuerName,
v20c_EVwa_SCMST.SecurityDesc,
v20c_EVwa_SCMST.Parvalue,
v20c_EVwa_SCMST.PVCurrency,
v20c_EVwa_SCMST.ISIN,v20c_EV_scexh.Sedol,
v20c_EVwa_SCMST.USCode,
v20c_EVwa_SCMST.StatusFlag,
v20c_EVwa_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD IS NULL OR v20c_EVwa_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EVwa_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_COWAR.UnSecID as UnderlyingSecID,
Scmst.ISIN as UnderlyingIsin,
sedolseq1.Sedol as UnderlyingSedol,
CASE WHEN (v10s_COWAR.CallPut = 'C') THEN 'Call' WHEN (v10s_COWAR.CallPut = 'P') THEN 'Put' ELSE '' END as CallPut,
v10s_COWAR.IssueDate,
v10s_COWAR.ExpirationDate,
CASE WHEN (v10s_COWAR.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(v10s_COWAR.CurenCD) > 0) THEN '[' + v10s_COWAR.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_COWAR.StrikePrice,
v10s_COWAR.UnRatio,
v10s_COWAR.WarrantRatio,
CASE WHEN (v10s_COWAR.ExerciseStyle is null) THEN irEXSTYLE.Lookup WHEN (irEXSTYLE.Lookup is null) and (LEN(v10s_COWAR.ExerciseStyle) > 0) THEN '[' + v10s_COWAR.ExerciseStyle +'] not found' ELSE irEXSTYLE.Lookup END as ExerciseStyle,
CASE WHEN (v10s_COWAR.CashStock = 'C') THEN 'Cash' WHEN (v10s_COWAR.CashStock = 'S') THEN 'Stock' ELSE '' END as CashStock,
v10s_COWAR.COWARNotes as Notes,
' ' as Choice,
v20c_EVwa_SCMST.IssID,
v20c_EVwa_SCMST.SecID
into WCA2.dbo.evf_Covered_Warrant
FROM v10s_COWAR
INNER JOIN v20c_EVwa_SCMST ON v20c_EVwa_SCMST.SecID = v10s_COWAR.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EVwa_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EVwa_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN SCMST ON v10s_COWAR.UnSecID = SCMST.SecID
LEFT OUTER JOIN sedolseq1 ON v10s_COWAR.UnSecID = sedolseq1.SecID
                   and 1 = sedolseq1.seqnum
                   and v20c_EV_scexh.ExCountry = sedolseq1.CntryCD
LEFT OUTER JOIN CUREN ON v10s_COWAR.CurenCD = CUREN.CurenCD
LEFT OUTER JOIN irEXSTYLE ON v10s_COWAR.ExerciseStyle = irEXSTYLE.Code
where
v10s_COWAR.Acttime BETWEEN @Startdate AND  '2099/01/01' 
and v10s_COWAR.actflag<>'D'
go

print " "
go
Print " Generating evf_Covered_Warrant ,please  wait ....."
GO 
USE WCA2 
ALTER TABLE evf_Covered_Warrant ALTER COLUMN  caref bigint NOT NULL
GO 
USE WCA2 
ALTER TABLE [DBO].[evf_Covered_Warrant] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evf_Covered_Warrant] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
USE WCA2 
CREATE  INDEX [ix_secid_evf_Covered_Warrant] ON [dbo].[evf_Covered_Warrant]([secid]) ON [PRIMARY] 
GO 
USE WCA2 
CREATE  INDEX [ix_issid_evf_Covered_Warrant] ON [dbo].[evf_Covered_Warrant]([issid]) ON [PRIMARY]
GO


