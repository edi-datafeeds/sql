print " Generating evd_Announcement, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Announcement')
 drop table evd_Announcement

select *
into wca2.dbo.evd_Announcement
FROM wca2.dbo.evf_Announcement
where changed >= @Startdate-0.3
go

print " Generating evd_Bankruptcy, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Bankruptcy')
 drop table evd_Bankruptcy

select *
into wca2.dbo.evd_Bankruptcy
FROM wca2.dbo.evf_Bankruptcy
where changed >= @Startdate-0.3
go


print " Generating evd_Company_Meeting, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Company_Meeting')
 drop table evd_Company_Meeting

select *
into wca2.dbo.evd_Company_Meeting
from wca2.dbo.evf_Company_Meeting
where changed >= @Startdate-0.3
go

print " Generating evd_Financial_Year_Change, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Financial_Year_Change')
 drop table evd_Financial_Year_Change

select *
into wca2.dbo.evd_Financial_Year_Change
from wca2.dbo.evf_Financial_Year_Change
where changed >= @Startdate-0.3
go

print " Generating evd_Incorporation_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Incorporation_Change')
 drop table evd_Incorporation_Change

select *
into wca2.dbo.evd_Incorporation_Change
from wca2.dbo.evf_Incorporation_Change
where changed >= @Startdate-0.3
go

print " Generating evd_Issuer_Name_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Issuer_Name_Change')
 drop table evd_Issuer_Name_Change

select *
into wca2.dbo.evd_Issuer_Name_Change
from wca2.dbo.evf_Issuer_Name_Change
where changed >= @Startdate-0.3
go

print " Generating evd_Class_Action, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Class_Action')
 drop table evd_Class_Action

select *
into wca2.dbo.evd_Class_Action
from wca2.dbo.evf_Class_Action
where changed >= @Startdate-0.3
go

print " Generating evd_Liquidation, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Liquidation')
 drop table evd_Liquidation

select *
into wca2.dbo.evd_Liquidation
from wca2.dbo.evf_Liquidation
where changed >= @Startdate-0.3
go

print " Generating evd_International_Code_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_International_Code_Change')
 drop table evd_International_Code_Change

select *
into wca2.dbo.evd_International_Code_Change
from wca2.dbo.evf_International_Code_Change
where changed >= @Startdate-0.3
go


print " Generating evd_Security_Description_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Security_Description_Change')
 drop table evd_Security_Description_Change

select *
into wca2.dbo.evd_Security_Description_Change
from wca2.dbo.evf_Security_Description_Change
where changed >= @Startdate-0.3
go


print " Generating evd_Listing_Status_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Listing_Status_Change')
 drop table evd_Listing_Status_Change

select *
into wca2.dbo.evd_Listing_Status_Change
from wca2.dbo.evf_Listing_Status_Change
where changed >= @Startdate-0.3
go

print " Generating evd_Local_Code_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Local_Code_Change')
 drop table evd_Local_Code_Change

select *
into wca2.dbo.evd_Local_Code_Change
from wca2.dbo.evf_Local_Code_Change
where changed >= @Startdate-0.3
go

print " Generating evd_Lot_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Lot_Change')
 drop table evd_Lot_Change

select *
into wca2.dbo.evd_Lot_Change
from wca2.dbo.evf_Lot_Change
where changed >= @Startdate-0.3
go

print " Generating evd_New_Listing, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_New_Listing')
 drop table evd_New_Listing

select *
into wca2.dbo.evd_New_Listing
from wca2.dbo.evf_New_Listing
where changed >= @Startdate-0.3
go


print " Generating evd_Sedol_Change, please wait..."
go


use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'evd_Sedol_Change')
 drop table evd_Sedol_Change

select *
into wca2.dbo.evd_Sedol_Change
from wca2.dbo.evf_Sedol_Change
where changed >= @Startdate-0.3
go
