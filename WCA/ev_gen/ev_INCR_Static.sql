print " Generating evi_Announcement, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Announcement')
 drop table evi_Announcement
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_ANN.EventID as char(8))),len(v10s_ANN.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_ANN.Levent,
EventID,
v10s_ANN.AnnounceDate as Created,
v10s_ANN.Acttime as Changed,
v10s_ANN.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_ANN.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ANN.EventType) > 0) THEN '[' + v10s_ANN.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ANN.NotificationDate,
'M' as Choice,
v10s_ANN.AnnNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Announcement
FROM v10s_ANN
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_Ann.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ANN.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_ANN.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_Bankruptcy, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Bankruptcy')
 drop table evi_Bankruptcy
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_BKRP.EventID as char(8))),len(v10s_BKRP.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_BKRP.Levent,
v10s_BKRP.EventID,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
v10s_BKRP.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,

v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_BKRP.NotificationDate,
v10s_BKRP.FilingDate,
'M' as Choice,
v10s_BKRP.BkrpNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Bankruptcy
FROM v10s_BKRP
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_BKRP.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_BKRP.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go


print " Generating evi_Company_Meeting, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Company_Meeting')
 drop table evi_Company_Meeting
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_AGM.EventID as char(8))),len(v10s_AGM.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_AGM.Levent,
v10s_AGM.EventID,
v10s_AGM.AnnounceDate as Created,
v10s_AGM.Acttime as Changed,
v10s_AGM.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_AGM.AGMDate,
v10s_AGM.AGMEGM,
v10s_AGM.AGMNO,
v10s_AGM.FYEDate as FinYearEndDate,
v10s_AGM.AGMTime,
v10s_AGM.Add1 as Address1,
v10s_AGM.Add2 as Address2,
v10s_AGM.Add3 as Address3,
v10s_AGM.Add4 as Address4,
v10s_AGM.Add5 as Address5,
v10s_AGM.Add6 as Address6,
v10s_AGM.City,
CASE WHEN (v10s_AGM.CntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_AGM.CntryCD) > 0) THEN '[' + v10s_AGM.CntryCD +'] not found' ELSE Cntry.Country END as Country,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Company_Meeting
FROM v10s_AGM
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_AGM.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN CNTRY ON v10s_AGM.CntryCD = CNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_AGM.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_Financial_Year_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Financial_Year_Change')
 drop table evi_Financial_Year_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_FYCHG.EventID as char(8))),len(v10s_FYCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_FYCHG.Levent,
v10s_FYCHG.EventID,
v10s_FYCHG.AnnounceDate as Created,
v10s_FYCHG.Acttime as Changed,
v10s_FYCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_FYCHG.NotificationDate,
v10s_FYCHG.OldFYStartDate as OldFinYearStart,
v10s_FYCHG.OldFYEndDate as OldFinYearEnd,
v10s_FYCHG.NewFYStartDate as NewFinYearStart,
v10s_FYCHG.NewFYEndDate as NewFinYearEnd,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Financial_Year_Change
FROM v10s_FYCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_FYCHG.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_FYCHG.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_Incorporation_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Incorporation_Change')
 drop table evi_Incorporation_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_INCHG.EventID as char(8))),len(v10s_INCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_INCHG.Levent,
v10s_INCHG.EventID,
v10s_INCHG.AnnounceDate as Created,
v10s_INCHG.Acttime as Changed,
v10s_INCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_INCHG.InChgDate as EffectiveDate,
CASE WHEN (v10s_INCHG.OldCntryCD is null) THEN OldCntry.Country WHEN (OldCntry.Country is null) and (LEN(v10s_INCHG.OldCntryCD) > 0) THEN '[' + v10s_INCHG.OldCntryCD +'] not found' ELSE OldCntry.Country END as OldCountry,
CASE WHEN (v10s_INCHG.NewCntryCD is null) THEN NewCntry.Country WHEN (NewCntry.Country is null) and (LEN(v10s_INCHG.NewCntryCD) > 0) THEN '[' + v10s_INCHG.NewCntryCD +'] not found' ELSE NewCntry.Country END as NewCountry,
CASE WHEN (v10s_INCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_INCHG.EventType) > 0) THEN '[' + v10s_INCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.evi_Incorporation_Change
FROM v10s_INCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_INCHG.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_INCHG.EventType = EVENT.EventType
LEFT OUTER JOIN CNTRY as oldCNTRY ON v10s_INCHG.OldCntryCD = oldCNTRY.CntryCD
LEFT OUTER JOIN CNTRY as newCNTRY ON v10s_INCHG.NewCntryCD = newCNTRY.CntryCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_INCHG.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_Issuer_Name_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Issuer_Name_Change')
 drop table evi_Issuer_Name_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_ISCHG.EventID as char(8))),len(v10s_ISCHG.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_ISCHG.Levent,
v10s_ISCHG.EventID,
v10s_ISCHG.AnnounceDate as Created,
v10s_ISCHG.Acttime as Changed,
v10s_ISCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_ISCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ISCHG.EventType) > 0) THEN '[' + v10s_ISCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_ISCHG.NameChangeDate,
v10s_ISCHG.IssOldName,
v10s_ISCHG.IssNewName,
v10s_ISCHG.LegalName,
'M' as Choice,
v10s_ISCHG.IschgNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.evi_Issuer_Name_Change
FROM v10s_ISCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_ISCHG.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ISCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_ISCHG.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_Class_Action, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Class_Action')
 drop table evi_Class_Action
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+substring('000000'+rtrim(cast(v10s_CLACT.EventID as char(8))),len(v10s_CLACT.EventID),7)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint) as CAref,
v10s_CLACT.Levent,
v10s_CLACT.EventID,
v10s_CLACT.AnnounceDate as Created,
v10s_CLACT.Acttime as Changed,
v10s_CLACT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_CLACT.EffectiveDate,
'M' as Choice,
v10s_CLACT.LawstNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.evi_Class_Action
FROM v10s_CLACT
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_CLACT.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_CLACT.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_Liquidation, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Liquidation')
 drop table evi_Liquidation
use wca
select 
case when MPAY.OptionID is not null and MPAY.SerialID is not null 
then cast(cast(v20c_EV_scexh.Seqnum as char(1))+rtrim(cast(MPAY.OptionID as char(2)))+rtrim(cast(MPAY.SerialID as char(2)))+v20c_EV_SCEXH.EXCHGID+substring('00000'+rtrim(cast(v10s_LIQ.EventID as char(8))),len(v10s_LIQ.EventID),6)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'11'+v20c_EV_SCEXH.EXCHGID+substring('00000'+rtrim(cast(v10s_LIQ.EventID as char(8))),len(v10s_LIQ.EventID),6)+rtrim(cast(v20c_EV_SCMST.SecID as char(8)))as bigint)
end as CAref,
v10s_LIQ.Levent,
v10s_LIQ.EventID,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as [Changed],
v10s_LIQ.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
rtrim(cast(MPAY.OptionID as char(2)))+'\'+rtrim(cast(MPAY.SerialID as char(2))) as OptionKey,
MPAY.Paytype,
MPAY.ActFlag as ActionMPAY,
MPAY.Paydate as LiquidationDate,
'Price' AS RateType,
MPAY.MaxPrice as Rate,
CASE WHEN (MPAY.CurenCD is null) THEN CUREN.Currency WHEN (CUREN.Currency is null) and (LEN(MPAY.CurenCD) > 0) THEN '[' + MPAY.CurenCD +'] not found' ELSE CUREN.Currency END as Currency,
v10s_LIQ.Liquidator,
v10s_LIQ.LiqAdd1 as Address1,
v10s_LIQ.LiqAdd2 as Address2,
v10s_LIQ.LiqAdd3 as Address3,
v10s_LIQ.LiqAdd4 as Address4,
v10s_LIQ.LiqAdd5 as Address5,
v10s_LIQ.LiqAdd6 as Address6,
v10s_LIQ.LiqCity as City,
CASE WHEN (v10s_LIQ.LiqCntryCD is null) THEN Cntry.Country WHEN (Cntry.Country is null) and (LEN(v10s_LIQ.LiqCntryCD) > 0) THEN '[' + v10s_LIQ.LiqCntryCD +'] not found' ELSE Cntry.Country END as Country,
v10s_LIQ.LiqTel as Telephone,
v10s_LIQ.LiqFax as Fax,
v10s_LIQ.LiqEmail as Email,
'M' as Choice,
v10s_LIQ.LiquidationTerms as Terms,
'n/a' as Ratio,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Liquidation
FROM v10s_LIQ
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.IssID = v10s_LIQ.IssID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID AND v10s_LIQ.SEvent = MPAY.SEvent
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
LEFT OUTER JOIN CNTRY ON v10s_LIQ.LiqCntryCD = CNTRY.CntryCD
LEFT OUTER JOIN CUREN ON MPAY.CurenCD = CUREN.CurenCD
where
v10s_LIQ.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_International_Code_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_International_Code_Change')
 drop table evi_International_Code_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_ICC.EventID as char(16)))as bigint) as CAref,
v10s_ICC.Levent,
v10s_ICC.EventID,
v10s_ICC.AnnounceDate as Created,
v10s_ICC.Acttime as Changed,
v10s_ICC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_ICC.EffectiveDate,
v10s_ICC.OldISIN,
v10s_ICC.NewISIN,
v10s_ICC.OldUSCode,
v10s_ICC.NewUSCode,
CASE WHEN (v10s_ICC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_ICC.EventType) > 0) THEN '[' + v10s_ICC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.evi_International_Code_Change
FROM v10s_ICC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_ICC.SecID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_ICC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_ICC.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go


print " Generating evi_Security_Description_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Security_Description_Change')
 drop table evi_Security_Description_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SCCHG.EventID as char(16)))as bigint) as CAref,
v10s_SCCHG.Levent,
v10s_SCCHG.EventID,
v10s_SCCHG.AnnounceDate as Created,
v10s_SCCHG.Acttime as Changed,
v10s_SCCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_SCCHG.DateofChange,
CASE WHEN (v10s_SCCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SCCHG.EventType) > 0) THEN '[' + v10s_SCCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SCCHG.SecOldName as OldName,
v10s_SCCHG.SecNewName as NewName,
'M' as Choice,
v10s_SCCHG.ScChgNotes as Notes,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.evi_Security_Description_Change
FROM v10s_SCCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_SCCHG.SecID
LEFT OUTER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN EVENT ON v10s_SCCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_SCCHG.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go


print " Generating evi_Listing_Status_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Listing_Status_Change')
 drop table evi_Listing_Status_Change
use wca
select 
cast(cast(v20c_EV_dSCEXH.Seqnum as char(1))+v20c_EV_dSCEXH.EXCHGID+rtrim(cast(v10s_LSTAT.EventID as char(16)))as bigint) as CAref,
v10s_LSTAT.Levent,
v10s_LSTAT.EventID,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
v10s_LSTAT.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,v20c_EV_dSCEXH.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_dSCEXH.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_dSCEXH.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_dSCEXH.ExchgCD,
v20c_EV_dSCEXH.MIC,
v20c_EV_dSCEXH.ExCountry,
v20c_EV_dSCEXH.RegCountry,
v20c_EV_dSCEXH.Localcode,
v20c_EV_dSCEXH.ListStatus,
v20c_EV_dSCEXH.Listdate,
CASE WHEN (v10s_LSTAT.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LSTAT.EventType) > 0) THEN '[' + v10s_LSTAT.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LSTAT.LStatStatus as ListStatChangedTo,
v10s_LSTAT.NotificationDate,
v10s_LSTAT.EffectiveDate,
'M' as Choice,
v10s_LSTAT.Reason,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Listing_Status_Change
FROM v10s_LSTAT
INNER JOIN v20c_EV_dSCEXH ON v10s_LSTAT.SecID = v20c_EV_dSCEXH.SecID AND v10s_LSTAT.ExchgCD = v20c_EV_dSCEXH.ExchgCD
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LSTAT.SecID
LEFT OUTER JOIN EVENT ON v10s_LSTAT.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_LSTAT.Acttime >= @Startdate AND v20c_EV_dSCEXH.secid is not null
go

print " Generating evi_Local_Code_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Local_Code_Change')
 drop table evi_Local_Code_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_LCC.EventID as char(16)))as bigint) as CAref,
v10s_LCC.Levent,
v10s_LCC.EventID,
v10s_LCC.AnnounceDate as Created,
v10s_LCC.Acttime as Changed,
v10s_LCC.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_LCC.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_LCC.EventType) > 0) THEN '[' + v10s_LCC.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_LCC.EffectiveDate,
v10s_LCC.OldLocalCode,
v10s_LCC.NewLocalCode,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.evi_Local_Code_Change
FROM v10s_LCC
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LCC.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_LCC.ExchgCD = v20c_EV_SCEXH.ExchgCD
LEFT OUTER JOIN EVENT ON v10s_LCC.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_LCC.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_Lot_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Lot_Change')
 drop table evi_Lot_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_LTCHG.EventID as char(16)))as bigint) as CAref,
v10s_LTCHG.Levent,
v10s_LTCHG.EventID,
v10s_LTCHG.AnnounceDate as Created,
v10s_LTCHG.Acttime as Changed,
v10s_LTCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_LTCHG.EffectiveDate,
v10s_LTCHG.OldLot as OldLotSize,
v10s_LTCHG.NewLot as NewLotSize,
v10s_LTCHG.OldMinTrdQty as OldMinTradingQuant,
v10s_LTCHG.NewMinTrdgQty as NewMinTradingQuant,
'M' as Choice,
v20c_EV_SCMST.IssID,

v20c_EV_SCMST.SecID
into wca2.dbo.evi_Lot_Change
FROM v10s_LTCHG
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCMST.SecID = v10s_LTCHG.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_LTCHG.ExchgCD = v20c_EV_SCEXH.ExchgCD
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_LTCHG.Acttime >= @Startdate AND v20c_EV_SCEXH.secid is not null
go

print " Generating evi_New_Listing, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_New_Listing')
 drop table evi_New_Listing
use wca
select DISTINCT
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_NLIST1.EventID as char(16)))as bigint) as CAref,
v10s_NLIST1.Levent,
v10s_NLIST1.EventID,
v10s_NLIST1.AnnounceDate as Created,
v10s_NLIST1.Acttime as Changed,
v10s_NLIST1.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
'M' as Choice,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_New_Listing
FROM v10s_NLIST1
INNER JOIN v20c_EV_SCEXH ON v10s_NLIST1.ScexhID = v20c_EV_SCEXH.ScexhID
INNER JOIN v20c_EV_SCMST ON v20c_EV_SCEXH.SecID = v20c_EV_SCMST.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
left outer join scmst on v20c_EV_SCEXH.SecID = scmst.secid
left outer JOIN SCEXH ON v10s_NLIST1.ScexhID = SCEXH.ScexhID
LEFT OUTER JOIN sedolseq1 ON SCEXH.SecID = sedolseq1.SecID AND v20c_EV_scexh.ExCountry = sedolseq1.CntryCD 
where
(v10s_NLIST1.Acttime >= @Startdate 
or scmst.Acttime >= @Startdate 
or scexh.Acttime >= @Startdate 
or sedolseq1.Acttime >= @Startdate)
and v10s_NLIST1.AnnounceDate>getdate()-90
AND v20c_EV_SCEXH.secid is not null
go


print " Generating evi_Sedol_Change, please wait..."
go

use wca
use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Sedol_Change')
 drop table evi_Sedol_Change
use wca
select 
cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SDCHG.EventID as char(16)))as bigint) as CAref,
v10s_SDCHG.Levent,
v10s_SDCHG.EventID,
v10s_SDCHG.AnnounceDate as Created,
v10s_SDCHG.Acttime as Changed,
v10s_SDCHG.ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
CASE WHEN (v10s_SDCHG.EventType is null) THEN Event.EventName WHEN (Event.EventName is null) and (LEN(v10s_SDCHG.EventType) > 0) THEN '[' + v10s_SDCHG.EventType +'] not found' ELSE Event.EventName END as RelatedEvent,
v10s_SDCHG.CntryCD as CntryofSedol,
v10s_SDCHG.EffectiveDate,
v10s_SDCHG.OldSEDOL,
v10s_SDCHG.NewSEDOL,
v10s_SDCHG.CntryCD as OldCountry,
v10s_SDCHG.NewCntryCD as NewCountry,
v10s_SDCHG.RCntryCD as OldRegCountry,
v10s_SDCHG.NewRCntryCD as NewRegCountry,
'M' as Choice,
v10s_SDCHG.SdChgNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into wca2.dbo.evi_Sedol_Change
FROM v10s_SDCHG
INNER JOIN v20c_EV_SCMST ON v10s_SDCHG.SecID = v20c_EV_SCMST.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID AND v10s_SDCHG.CntryCD = v20c_EV_SCEXH.ExCountry
LEFT OUTER JOIN EVENT ON v10s_SDCHG.EventType = EVENT.EventType
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
v10s_SDCHG.Acttime >= @Startdate 
AND v20c_EV_SCEXH.secid is not null
go
