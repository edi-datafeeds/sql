print " Generating tISO_BRUP, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_BRUP')
 drop table tISO_BRUP

use wca
select 
v10s_BKRP.EventID,
v20c_610_SCMST.SecID,
v10s_BKRP.AnnounceDate as Created,
v10s_BKRP.Acttime as Changed,
CASE WHEN v10s_BKRP.ActFlag is null THEN '' ELSE v10s_BKRP.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN v10s_BKRP.NotificationDate is null THEN '' ELSE v10s_BKRP.NotificationDate END as NotificationDate,
CASE WHEN v10s_BKRP.FilingDate is null THEN '' ELSE v10s_BKRP.FilingDate END as FilingDate
into wca2.dbo.tISO_BRUP
from v10s_BKRP
INNER JOIN v20c_610_SCMST ON v20c_610_SCMST.IssID = v10s_BKRP.IssID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID

where v10s_BKRP.Acttime >= @Startdate
go


print " Generating tISO_CLSA, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_CLSA')
 drop table tISO_CLSA

use wca
Select 
v10s_LAWST.EventID,
v20c_610_SCMST.SecID,
v10s_LAWST.AnnounceDate as Created,
v10s_LAWST.Acttime as Changed,
CASE WHEN v10s_LAWST.ActFlag is null THEN '' ELSE v10s_LAWST.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN v10s_LAWST.EffectiveDate is null THEN '' ELSE v10s_LAWST.EffectiveDate END as EffectiveDate
into wca2.dbo.tISO_CLSA
from v10s_LAWST
INNER JOIN v20c_610_SCMST ON v20c_610_SCMST.IssID = v10s_LAWST.IssID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
where v10s_LAWST.Acttime >= @Startdate
go


print " Generating tISO_NAME, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_NAME')
 drop table tISO_NAME

use wca
Select 
v10s_ISCHG.EventID,
v20c_610_SCMST.SecID,
v10s_ISCHG.AnnounceDate as Created,
v10s_ISCHG.Acttime as Changed,
CASE WHEN v10s_ISCHG.ActFlag is null THEN '' ELSE v10s_ISCHG.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN v10s_ISCHG.NameChangeDate is null THEN '' ELSE v10s_ISCHG.NameChangeDate END as NameChangeDate,
CASE WHEN v10s_ISCHG.IssOldName is null THEN '' ELSE v10s_ISCHG.IssOldName END as IssOldName,
CASE WHEN v10s_ISCHG.IssNewName is null THEN '' ELSE v10s_ISCHG.IssNewName END as IssNewName,
CASE WHEN v10s_ISCHG.EventType IS NULL THEN '' ELSE v10s_ISCHG.EventType END as EventType
into wca2.dbo.tISO_NAME
from v10s_ISCHG
INNER JOIN v20c_610_SCMST ON v20c_610_SCMST.IssID = v10s_ISCHG.IssID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
where v10s_ISCHG.Acttime >= @Startdate
go


print " Generating tISO_EXWA, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_EXWA')
 drop table tISO_EXWA

use wca
Select 
v10s_WAREX.EventID,
v20c_610_SCMST.SecID,
v10s_WARTM.AnnounceDate as Created,
CASE WHEN (v10s_WAREX.Acttime is not null) and (v10s_WAREX.Acttime > v10s_WARTM.Acttime) THEN v10s_WAREX.Acttime ELSE v10s_WARTM.Acttime END as [Changed],
v10s_WAREX.Actflag as Actflag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN (v10s_WARTM.IssueDate IS NULL) THEN '' ELSE v10s_WARTM.IssueDate END as IssueDate,
CASE WHEN (v10s_WARTM.ExpirationDate IS NULL) THEN '' ELSE v10s_WARTM.ExpirationDate END as ExpirationDate,
CASE WHEN (v10s_WARTM.RedemptionDate IS NULL) THEN '' ELSE v10s_WARTM.RedemptionDate END as RedemptionDate,
CASE WHEN (v10s_WARTM.ExerciseStyle IS NULL) THEN '' ELSE v10s_WARTM.ExerciseStyle END as ExerciseStyle,
CASE WHEN (v10s_WAREX.WarexID IS NULL) THEN '' ELSE v10s_WAREX.WarexID END as WarexID,
CASE WHEN (v10s_WAREX.Fromdate IS NULL) THEN '' ELSE v10s_WAREX.Fromdate END as Fromdate,
CASE WHEN (v10s_WAREX.Todate IS NULL) THEN '' ELSE v10s_WAREX.Todate END as Todate,
CASE WHEN (v10s_WAREX.Rationew IS NULL) THEN '' ELSE v10s_WAREX.Rationew END as Rationew,
CASE WHEN (v10s_WAREX.Ratioold IS NULL) THEN '' ELSE v10s_WAREX.Ratioold END as Ratioold,
CASE WHEN (v10s_WAREX.CurenCD IS NULL) THEN '' ELSE v10s_WAREX.CurenCD END as CurenCD,
CASE WHEN (v10s_WAREX.StrikePrice IS NULL) THEN '' ELSE v10s_WAREX.StrikePrice END as StrikePrice,
CASE WHEN (v10s_WAREX.PricePerShare IS NULL) THEN '' ELSE v10s_WAREX.PricePerShare END as PricePerShare,
CASE WHEN v10s_WAREX.ExerSecId is null THEN '' ELSE v10s_WAREX.ExerSecId END as ExerSecId,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as exerSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as exerSID,
' ' as Paydate
into wca2.dbo.tISO_EXWA
from v10s_WARTM
INNER JOIN v20c_610_SCMST ON v10s_WARTM.EventID = v20c_610_SCMST.SecID
LEFT OUTER JOIN v10s_WAREX ON v10s_WARTM.SecID = v10s_WAREX.SecID 
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_WAREX.ExerSecID = SCMST.SecID
where (v10s_WAREX.Acttime >= @Startdate
or v10s_WAREX.Acttime >= @Startdate)

go


print " Generating tISO_LIQU, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_LIQU')
 drop table tISO_LIQU

use wca
Select 
v10s_LIQ.EventID,
v20c_610_SCMST.SecID,
v10s_LIQ.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_LIQ.Acttime) THEN MPAY.Acttime ELSE v10s_LIQ.Acttime END as [Changed],
CASE WHEN v10s_LIQ.ActFlag is null THEN '' ELSE v10s_LIQ.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN v10s_LIQ.RdDate is null THEN '' ELSE v10s_LIQ.RdDate END as RdDate,
CASE WHEN MPAY.RatioNew is null THEN '' ELSE MPAY.RatioNew END as RatioNew_1,
CASE WHEN MPAY.RatioOld is null THEN '' ELSE MPAY.RatioOld END as RatioOld_1,
CASE WHEN MPAY.MinPrice is null THEN '' ELSE MPAY.MinPrice END as MinPrice_1,
CASE WHEN MPAY.MaxPrice is null THEN '' ELSE MPAY.MaxPrice END as MaxPrice_1,
CASE WHEN MPAY.CurenCD is null THEN '' ELSE MPAY.CurenCD END as CurenCD_1,
CASE WHEN MPAY.Paydate is null THEN '' ELSE MPAY.Paydate END as Paydate_1,
CASE WHEN MPAY.SectyCD is null THEN '' ELSE MPAY.SectyCD END as SectyCD_1,
CASE WHEN MPAY.Paytype is null THEN '' ELSE MPAY.Paytype END as Paytype_1,
CASE WHEN MPAY.Fractions is null THEN '' ELSE MPAY.Fractions END as Fractions_1,
CASE WHEN MPAY.ActFlag is null THEN '' ELSE MPAY.ActFlag END as ActFlag_1,
CASE WHEN MPAY.ResSecId is null THEN '' ELSE MPAY.ResSecId END as ResSecId_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN 'ISIN' WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN SC1.Isin WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN SC1.Uscode ELSE '' END as resSID_1
into wca2.dbo.tISO_LIQU
from v10s_LIQ
INNER JOIN v20c_610_SCMST ON v10s_LIQ.IssID = v20c_610_SCMST.IssID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_LIQ.EventID = MPAY.EventID and 'LIQ' = MPAY.SEvent
LEFT OUTER JOIN SCMST as SC1 ON MPAY.ResSecID = SC1.SecID
where (MPAY.Acttime >= @Startdate
or v10s_LIQ.Acttime >= @Startdate)
go


print " Generating tISO_D, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_D')
 drop table tISO_D

use wca
Select 
v10s_DIV.EventID,
v20c_610_SCMST.SecID,
v10s_DIV.RdID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (DP1.Acttime is not null) and (DP1.Acttime > v10s_DIV.Acttime) and (DP1.Acttime > RD.Acttime) and (DP1.Acttime > EXDT.Acttime) and (DP1.Acttime > PEXDT.Acttime) THEN DP1.Acttime WHEN (DPDEL.Acttime is not null) and (DPDEL.Acttime > v10s_DIV.Acttime) and (DPDEL.Acttime > RD.Acttime) and (DPDEL.Acttime > EXDT.Acttime) and (DPDEL.Acttime > PEXDT.Acttime) THEN DPDEL.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
CASE WHEN v10s_DIV.ActFlag is null THEN '' ELSE v10s_DIV.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
v20c_610_SCMST.Isin,
v20c_610_SCMST.Uscode,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
EXDT.Paydate2,
CASE WHEN v10s_DIV.FYEDate is null THEN '' ELSE v10s_DIV.FYEDate END as FYEDate,
CASE WHEN v10s_DIV.DivPeriodCD is null THEN '' ELSE v10s_DIV.DivPeriodCD END as DivPeriodCD,
CASE WHEN v10s_DIV.Tbaflag is null THEN '' ELSE v10s_DIV.Tbaflag END as Tbaflag,
CASE WHEN v10s_DIV.NilDividend is null THEN '' ELSE v10s_DIV.NilDividend END as NilDividend,
CASE WHEN DRIP.CntryCD is null THEN '' ELSE DRIP.CntryCD END as DripCntryCD,
CASE WHEN DRIP.Acttime is null THEN '' ELSE DRIP.Acttime END as DripCHANGED,
CASE WHEN DRIP.DripLastdate is null THEN '' ELSE DRIP.DripLastdate END as DripLastdate,
CASE WHEN DRIP.DripReinvPrice is null THEN '' ELSE DRIP.DripReinvPrice END as DripReinvPrice,
CASE WHEN DRIP.DripPaydate is null THEN '' ELSE DRIP.DripPaydate END as DripPaydate,
CASE WHEN DRIP.Crestdate is null THEN '' ELSE DRIP.Crestdate END as DripCrestdate,
CASE WHEN FRANK.CntryCD is null THEN '' ELSE FRANK.CntryCD END as FrankCntryCD,
CASE WHEN FRANK.Acttime is null THEN '' ELSE FRANK.Acttime END as FrankCHANGED,
CASE WHEN FRANK.FrankFlag is null THEN '' ELSE FRANK.FrankFlag END as FrankFlag,
CASE WHEN FRANK.FrankDiv is null THEN '' ELSE FRANK.FrankDiv END as FrankDiv,
CASE WHEN FRANK.UnFrankDiv is null THEN '' ELSE FRANK.UnFrankDiv END as UnFrankDiv,
CASE WHEN DP1.OptionID is not null THEN DP1.OptionID ELSE DPDEL.OptionID END as OpID_1,
CASE WHEN DP1.OptionID is not null THEN DP1.ActFlag ELSE DPDEL.ActFlag END as ActFlag_1,
CASE WHEN DP1.OptionID is not null THEN DP1.Divtype ELSE DPDEL.Divtype END as Divtype_1,
CASE WHEN DP1.OptionID is not null THEN DP1.CurenCD ELSE DPDEL.CurenCD END as CurenCD_1,
CASE WHEN DP1.OptionID is not null THEN DP1.Approxflag ELSE DPDEL.Approxflag END as Approxflag_1,
CASE WHEN DP1.OptionID is not null THEN DP1.DivInPercent ELSE DPDEL.DivInPercent END as DivInPercent_1,
CASE WHEN DP1.OptionID is not null THEN DP1.GrossDividend ELSE DPDEL.GrossDividend END as GrossDividend_1,
CASE WHEN DP1.OptionID is not null THEN DP1.NetDividend ELSE DPDEL.NetDividend END as NetDividend_1,
CASE WHEN DP1.OptionID is not null THEN DP1.TaxRate ELSE DPDEL.TaxRate END as TaxRate_1,
CASE WHEN DP1.OptionID is not null THEN DP1.RecindCashDiv ELSE DPDEL.RecindCashDiv END as RecindCashDiv_1,
CASE WHEN DP1.OptionID is not null THEN DP1.USDRateToCurrency ELSE DPDEL.USDRateToCurrency END as USDRateToCurrency_1,
CASE WHEN DP1.OptionID is not null THEN DP1.SectyCD ELSE DPDEL.SectyCD END as SectyCD_1,
CASE WHEN DP1.OptionID is not null THEN DP1.Fractions ELSE DPDEL.Fractions END as Fractions_1,
CASE WHEN DP1.OptionID is not null THEN DP1.RecindStockDiv ELSE DPDEL.RecindStockDiv END as RecindStockDiv_1,
CASE WHEN DP1.OptionID is not null THEN DP1.Depfees ELSE DPDEL.Depfees END as Depfees_1,
CASE WHEN DP1.OptionID is not null THEN DP1.Coupon ELSE DPDEL.Coupon END as Coupon_1,
CASE WHEN DP1.OptionID is not null THEN DP1.CouponID ELSE DPDEL.CouponID END as CouponID_1,
CASE WHEN DP1.OptionID is not null THEN DP1.ResSecId ELSE DPDEL.ResSecId END as ResSecId_1,
CASE WHEN DP1.OptionID is not null and SC1.Isin IS NOT NULL AND SC1.Isin <> '' THEN 'ISIN' WHEN SC1.Uscode IS NOT NULL AND SC1.Uscode <> '' THEN '/US/' ELSE '' END as resSIDname_1,
CASE WHEN DP1.OptionID is not null and SC1.Isin IS NOT NULL AND SC1.Isin <> '' THEN SC1.Isin WHEN SC1.Uscode IS NOT NULL AND SC1.Uscode <> '' THEN SC1.Uscode ELSE '' END as resSID_1,
CASE WHEN DP1.OptionID is not null THEN DP1.RatioNew ELSE DPDEL.RatioNew END as RatioNew_1,
CASE WHEN DP1.OptionID is not null THEN DP1.RatioOld ELSE DPDEL.RatioOld END as RatioOld_1,
CASE WHEN DP2.OptionID is null THEN '' ELSE DP2.OptionID END as OpID_2,
CASE WHEN DP3.OptionID is null THEN '' ELSE DP3.OptionID END as OpID_3,
CASE WHEN DP4.OptionID is null THEN '' ELSE DP4.OptionID END as OpID_4,
CASE WHEN DP5.OptionID is null THEN '' ELSE DP5.OptionID END as OpID_5,
CASE WHEN DP6.OptionID is null THEN '' ELSE DP6.OptionID END as OpID_6
into wca2.dbo.tISO_D
from v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN DRIP ON v10s_DIV.EventID = DRIP.DivID and v20c_ISO_SCEXH.CntryCD = DRIP.CntryCD
LEFT OUTER JOIN FRANK ON v10s_DIV.EventID = FRANK.DivID and v20c_ISO_SCEXH.CntryCD = FRANK.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER join v10s_divpy as DP1 ON v10s_DIV.EventID = DP1.DivID and DP1.OptionID=1
LEFT OUTER join v10s_divpy as DP2 ON v10s_DIV.EventID = DP2.DivID and DP2.OptionID=2
LEFT OUTER join v10s_divpy as DP3 ON v10s_DIV.EventID = DP3.DivID and DP3.OptionID=3
LEFT OUTER join v10s_divpy as DP4 ON v10s_DIV.EventID = DP4.DivID and DP4.OptionID=4
LEFT OUTER join v10s_divpy as DP5 ON v10s_DIV.EventID = DP5.DivID and DP5.OptionID=5
LEFT OUTER join v10s_divpy as DP6 ON v10s_DIV.EventID = DP6.DivID and DP6.OptionID=6
LEFT OUTER join v10s_divpy as DPDEL ON v10s_DIV.EventID = DPDEL.DivID and DPDEL.OptionID=2
LEFT OUTER JOIN SCMST as SC1 ON DP1.ResSecID = SC1.SecID

where (DP1.Acttime >= @Startdate
or DPDEL.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or DRIP.Acttime >= @Startdate
or FRANK.Acttime >= @Startdate
or v10s_DIV.Acttime >= @Startdate)

go


print " Generating tISO_CONV, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_CONV')
 drop table tISO_CONV

use wca
Select 
v10s_CONV.EventID,
v20c_610_SCMST.SecID,
v10s_CONV.AnnounceDate as Created,
v10s_CONV.Acttime as Changed,
CASE WHEN v10s_CONV.ActFlag is null THEN '' ELSE v10s_CONV.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN (v10s_CONV.Fractions IS NULL) THEN '' ELSE v10s_CONV.Fractions END as Fractions,
CASE WHEN (v10s_CONV.ToDate IS NULL) THEN '' ELSE v10s_CONV.ToDate END as ToDate,
CASE WHEN (v10s_CONV.FromDate IS NULL) THEN '' ELSE v10s_CONV.FromDate END as FromDate,
CASE WHEN (v10s_CONV.RatioNew IS NULL) THEN '' ELSE v10s_CONV.RatioNew END as RatioNew,
CASE WHEN (v10s_CONV.RatioOld IS NULL) THEN '' ELSE v10s_CONV.RatioOld END as RatioOld,
CASE WHEN (v10s_CONV.CurenCD IS NULL) THEN '' ELSE v10s_CONV.CurenCD END as CurenCD,
CASE WHEN (v10s_CONV.MandOptFlag IS NULL) THEN '' ELSE v10s_CONV.MandOptFlag END as MandOptFlag,
CASE WHEN (v10s_CONV.PfrCnPrice IS NULL) THEN '' ELSE v10s_CONV.PfrCnPrice END as PfrCnPrice,
CASE WHEN v10s_CONV.ResSecId is null THEN '' ELSE v10s_CONV.ResSecId END as ResSecId,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID,
' ' as Paydate
into wca2.dbo.tISO_CONV
from v10s_CONV
INNER JOIN v20c_610_SCMST ON v10s_CONV.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_CONV.ResSecID = SCMST.SecID
where v10s_CONV.Acttime >= @Startdate

go


print " Generating tISO_EXOF, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_EXOF')
 drop table tISO_EXOF

use wca
Select 
v10s_CTX.EventID,
v20c_610_SCMST.SecID,
v10s_CTX.AnnounceDate as Created,
v10s_CTX.Acttime as Changed,
CASE WHEN v10s_CTX.ActFlag is null THEN '' ELSE v10s_CTX.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN (v10s_CTX.StartDate IS NULL) THEN '' ELSE v10s_CTX.StartDate END as StartDate,
CASE WHEN (v10s_CTX.EndDate IS NULL) THEN '' ELSE v10s_CTX.EndDate END as EndDate,
CASE WHEN v10s_CTX.ResSecId is null THEN '' ELSE v10s_CTX.ResSecId END as ResSecId,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID,
' ' as Paydate,
CASE WHEN v10s_CTX.EventType IS NULL THEN '' ELSE v10s_CTX.EventType END as EventType
into wca2.dbo.tISO_EXOF
from v10s_CTX
INNER JOIN v20c_610_SCMST ON v10s_CTX.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_CTX.ResSecID = SCMST.SecID
where v10s_CTX.Acttime >= @Startdate
go


print " Generating tISO_BIDS, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_BIDS')
 drop table tISO_BIDS

use wca
Select 
v10s_BB.EventID,
v20c_610_SCMST.SecID,
v10s_BB.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_BB.Acttime) THEN MPAY.Acttime ELSE v10s_BB.Acttime END as [Changed],
CASE WHEN v10s_BB.ActFlag is null THEN '' ELSE v10s_BB.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN MPAY.CurenCD is null THEN '' ELSE MPAY.CurenCD END as CurenCD,
CASE WHEN MPAY.Paydate is null THEN '' ELSE MPAY.Paydate END as Paydate,
CASE WHEN MPAY.MinPrice is null THEN '' ELSE MPAY.MinPrice END as MinPrice,
CASE WHEN MPAY.MaxPrice is null THEN '' ELSE MPAY.MaxPrice END as MaxPrice,
CASE WHEN MPAY.MinQlyQty is null THEN '' ELSE MPAY.MinQlyQty END as MinQlyQty,
CASE WHEN MPAY.MaxQlyQty is null THEN '' ELSE MPAY.MaxQlyQty END as MaxQlyQty,
CASE WHEN MPAY.MinOfrQty is null THEN '' ELSE MPAY.MinOfrQty END as MinOfrQty,
CASE WHEN MPAY.MaxOfrqty is null THEN '' ELSE MPAY.MaxOfrqty END as MaxOfrqty,
CASE WHEN MPAY.TndrStrkPrice is null THEN '' ELSE MPAY.TndrStrkPrice END as TndrStrkPrice,
CASE WHEN v10s_BB.OnOffFlag is null THEN '' ELSE v10s_BB.OnOffFlag END as OnOffFlag,
CASE WHEN v10s_BB.StartDate is null THEN '' ELSE v10s_BB.StartDate END as StartDate,
CASE WHEN v10s_BB.EndDate is null THEN '' ELSE v10s_BB.EndDate END as EndDate,
CASE WHEN v10s_BB.MinAcpQty is null THEN '' ELSE v10s_BB.MinAcpQty END as MinAcpQty,
CASE WHEN v10s_BB.MaxAcpQty is null THEN '' ELSE v10s_BB.MaxAcpQty END as MaxAcpQty,
CASE WHEN v10s_BB.BBMinPct is null THEN '' ELSE v10s_BB.BBMinPct END as BBMinPct,
CASE WHEN v10s_BB.BBMaxPct is null THEN '' ELSE v10s_BB.BBMaxPct END as BBMaxPct
into wca2.dbo.tISO_BIDS
from v20c_610_SCMST
INNER JOIN v10s_BB ON v10s_BB.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN MPAY ON v10s_BB.EventID = MPAY.EventID AND v10s_BB.SEvent = MPAY.SEvent
where (MPAY.Acttime >= @Startdate
or v10s_BB.Acttime >= @Startdate)
go


print " Generating tISO_ACTV, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_ACTV')
 drop table tISO_ACTV

use wca
Select 
v10s_LSTAT.EventID,
v20c_610_SCMST.SecID,
v10s_LSTAT.AnnounceDate as Created,
v10s_LSTAT.Acttime as Changed,
CASE WHEN v10s_LSTAT.ActFlag is null THEN '' ELSE v10s_LSTAT.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN (v10s_LSTAT.LStatStatus IS NULL) THEN '' ELSE v10s_LSTAT.LStatStatus END as LStatStatus,
CASE WHEN (v10s_LSTAT.NotificationDate IS NULL) THEN '' ELSE v10s_LSTAT.NotificationDate END as NotificationDate,
CASE WHEN (v10s_LSTAT.EffectiveDate IS NULL) THEN '' ELSE v10s_LSTAT.EffectiveDate END as EffectiveDate,
CASE WHEN (v10s_LSTAT.EventType IS NULL) THEN '' ELSE v10s_LSTAT.EventType END as EventType
into wca2.dbo.tISO_ACTV
from v10s_LSTAT
INNER JOIN v20c_610_SCMST ON v10s_LSTAT.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
             and v10s_LSTAT.ExchgCD = v20c_ISO_SCEXH.ExchgCD
where v10s_LSTAT.Acttime >= @Startdate
go


print " Generating tISO_PARI, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_PARI')
 drop table tISO_PARI

use wca
Select 
v10s_ASSM.EventID,
v20c_610_SCMST.SecID,
v10s_ASSM.AnnounceDate as Created,
v10s_ASSM.Acttime as Changed,
CASE WHEN v10s_ASSM.ActFlag is null THEN '' ELSE v10s_ASSM.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN v10s_ASSM.AssimilationDate is null THEN '' ELSE v10s_ASSM.AssimilationDate END as AssimilationDate,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID
into wca2.dbo.tISO_PARI
from v10s_ASSM
INNER JOIN v20c_610_SCMST ON v10s_ASSM.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN SCMST ON v10s_ASSM.ResSecID = SCMST.SecID
where v10s_ASSM.Acttime >= @Startdate
go


print " Generating tISO_DECR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_DECR')
 drop table tISO_DECR

use wca
Select 
v10s_CAPRD.EventID,
v20c_610_SCMST.SecID,
v10s_CAPRD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CAPRD.Acttime) and (RD.Acttime > ICC.Acttime) and (RD.Acttime > SDCHG.Acttime) THEN RD.Acttime WHEN (ICC.Acttime is not null) and (ICC.Acttime > v10s_CAPRD.Acttime) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime WHEN (SDCHG.Acttime is not null) and (SDCHG.Acttime > v10s_CAPRD.Acttime) THEN SDCHG.Acttime ELSE v10s_CAPRD.Acttime END as [Changed],
CASE WHEN v10s_CAPRD.ActFlag is null THEN '' ELSE v10s_CAPRD.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN 'ISIN' WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN '/GB/' WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN '/US/' ELSE '' END as OldSIDname,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN ICC.Oldisin WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN SDCHG.Oldsedol WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN ICC.OldUscode ELSE '' END as OldSID,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN 'ISIN' WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN '/GB/' WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN '/US/' ELSE '' END as newSIDname,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN ICC.newisin WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN SDCHG.newsedol WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN ICC.newUscode ELSE '' END as NewSID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_ISO_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_ISO_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
v10s_CAPRD.RdID,
RD.Recdate,
CASE WHEN v10s_CAPRD.Fractions is null THEN '' ELSE v10s_CAPRD.Fractions END as Fractions,
CASE WHEN v10s_CAPRD.EffectiveDate is null THEN '' ELSE v10s_CAPRD.EffectiveDate END as EffectiveDate,
CASE WHEN v10s_CAPRD.OldParValue is null THEN '' ELSE v10s_CAPRD.OldParValue END as OldParValue,
CASE WHEN v10s_CAPRD.NewParValue is null THEN '' ELSE v10s_CAPRD.NewParValue END as NewParValue,
CASE WHEN v10s_CAPRD.NewRatio is null THEN '' ELSE v10s_CAPRD.NewRatio END as NewRatio,
CASE WHEN v10s_CAPRD.OldRatio is null THEN '' ELSE v10s_CAPRD.OldRatio END as OldRatio,
CASE WHEN v10s_CAPRD.Paydate is null THEN '' ELSE v10s_CAPRD.Paydate END as Paydate
into wca2.dbo.tISO_DECR
from v20c_610_SCMST
INNER JOIN v10s_CAPRD ON v10s_CAPRD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_CAPRD.RdID = RD.RdID
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_ISO_SCEXH.CntryCD = SDCHG.CntryCD AND v20c_ISO_SCEXH.RCntryCD = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_ISO_SCEXH.ExchgCD = LCC.ExchgCD
where (v10s_CAPRD.Acttime >= @Startdate
or ICC.Acttime >= @Startdate
or SDCHG.Acttime >= @Startdate
or RD.Acttime >= @Startdate)
go


print " Generating tISO_MRGR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_MRGR')
 drop table tISO_MRGR

use wca
Select 
v10s_MRGR.EventID,
v20c_610_SCMST.SecID,
v10s_MRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_MRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_MRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_MRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_MRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_MRGR.Acttime END as [Changed],
CASE WHEN v10s_MRGR.ActFlag is null THEN '' ELSE v10s_MRGR.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
RD.RDID,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_MRGR.ApprovalStatus is null THEN '' ELSE v10s_MRGR.ApprovalStatus END as ApprovalStatus,
CASE WHEN v10s_MRGR.Companies is null THEN '' ELSE v10s_MRGR.Companies END as Companies,
CASE WHEN v10s_MRGR.MrgrStatus is null THEN '' ELSE v10s_MRGR.MrgrStatus END as MrgrStatus,
CASE WHEN v10s_MRGR.AppointedDate is null THEN '' ELSE v10s_MRGR.AppointedDate END as AppointedDate,
CASE WHEN v10s_MRGR.EffectiveDate is null THEN '' ELSE v10s_MRGR.EffectiveDate END as EffectiveDate,
MPAY.OptionID,
MPAY.SerialID,
CASE WHEN MPAY.ActFlag IS NULL THEN '' ELSE MPAY.ActFlag END as Actflag_1,
CASE WHEN MPAY.RatioNew IS NULL THEN '' ELSE MPAY.RatioNew END as RatioNew_1,
CASE WHEN MPAY.RatioOld IS NULL THEN '' ELSE MPAY.RatioOld END as RatioOld_1,
CASE WHEN MPAY.MinOfrQty IS NULL THEN '' ELSE MPAY.MinOfrQty END as MinOfrQty_1,
CASE WHEN MPAY.MaxOfrQty IS NULL THEN '' ELSE MPAY.MaxOfrQty END as MaxOfrQty_1,
CASE WHEN MPAY.MinQlyQty IS NULL THEN '' ELSE MPAY.MinQlyQty END as MinQlyQty_1,
CASE WHEN MPAY.MaxQlyQty IS NULL THEN '' ELSE MPAY.MaxQlyQty END as MaxQlyQty_1,
CASE WHEN MPAY.Paydate IS NULL THEN '' ELSE MPAY.Paydate END as Paydate_1,
CASE WHEN MPAY.CurenCD IS NULL THEN '' ELSE MPAY.CurenCD END as CurenCD_1,
CASE WHEN MPAY.MinPrice IS NULL THEN '' ELSE MPAY.MinPrice END as MinPrice_1,
CASE WHEN MPAY.MaxPrice IS NULL THEN '' ELSE MPAY.MaxPrice END as MaxPrice_1,
CASE WHEN MPAY.TndrStrkPrice IS NULL THEN '' ELSE MPAY.TndrStrkPrice END as TndrStrkPrice_1,
CASE WHEN MPAY.TndrStrkStep IS NULL THEN '' ELSE MPAY.TndrStrkStep END as TndrStrkStep_1,
CASE WHEN MPAY.PayType IS NULL THEN '' ELSE MPAY.PayType END as PayType_1,
CASE WHEN MPAY.Fractions IS NULL THEN '' ELSE MPAY.Fractions END as Fractions_1,
CASE WHEN MPAY.SectyCD IS NULL THEN '' ELSE MPAY.SectyCD END as SectyCD_1,
CASE WHEN MPAY.ResSecId is null THEN '' ELSE MPAY.ResSecId END as ResSecId_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN 'ISIN' WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN SC1.Isin WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN SC1.Uscode ELSE '' END as resSID_1
into wca2.dbo.tISO_MRGR
from v10s_MRGR
INNER JOIN RD ON RD.RdID = v10s_MRGR.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'MRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'MRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_MRGR.EventID = MPAY.EventID and 'MRGR' = MPAY.SEvent
LEFT OUTER JOIN SCMST as SC1 ON MPAY.ResSecID = SC1.SecID
where (MPAY.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_MRGR.Acttime >= @Startdate)
go

print " Generating tISO_INCR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_INCR')
 drop table tISO_INCR

use wca
Select 
v10s_RCAP.EventID,
v20c_610_SCMST.SecID,
v10s_RCAP.AnnounceDate as Created,
V10s_RCAP.Acttime as Changed,
CASE WHEN v10s_RCAP.ActFlag is null THEN '' ELSE v10s_RCAP.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
v10s_RCAP.RdID,
RD.Recdate,
CASE WHEN v10s_RCAP.EffectiveDate is null THEN '' ELSE v10s_RCAP.EffectiveDate END as EffectiveDate,
CASE WHEN v10s_RCAP.CurenCD is null THEN '' ELSE v10s_RCAP.CurenCD END as CurenCD,
CASE WHEN v10s_RCAP.CashBak is null THEN '' ELSE v10s_RCAP.CashBak END as CashBak,
CASE WHEN v10s_RCAP.CSPYDate is null THEN '' ELSE v10s_RCAP.CSPYDate END as CSPYDate
into wca2.dbo.tISO_INCR
from v20c_610_SCMST
INNER JOIN v10s_RCAP ON v10s_RCAP.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_RCAP.RdID = RD.RdID
where v10s_RCAP.Acttime >= @Startdate+1
go


print " Generating tISO_PPMT, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_PPMT')
 drop table tISO_PPMT

use wca
Select 
v10s_CALL.EventID,
v20c_610_SCMST.SecID,
v10s_CALL.AnnounceDate as Created,
V10s_CALL.Acttime as Changed,
CASE WHEN v10s_CALL.ActFlag is null THEN '' ELSE v10s_CALL.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN v10s_CALL.CurenCD is null THEN '' ELSE v10s_CALL.CurenCD END as CurenCD,
CASE WHEN v10s_CALL.CallNumber is null THEN '' ELSE v10s_CALL.CallNumber END as CallNumber,
CASE WHEN v10s_CALL.ToFacevalue is null THEN '' ELSE v10s_CALL.ToFacevalue END as ToFacevalue,
CASE WHEN v10s_CALL.ToPremium is null THEN '' ELSE v10s_CALL.ToPremium END as ToPremium,
CASE WHEN v10s_CALL.DueDate is null THEN '' ELSE v10s_CALL.DueDate END as DueDate
into wca2.dbo.tISO_PPMT
from v20c_610_SCMST
INNER JOIN v10s_CALL ON v10s_CALL.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
where V10s_CALL.Acttime >= @Startdate
go


print " Generating tISO_TEND_TKOVR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_TEND_TKOVR')
 drop table tISO_TEND_TKOVR

use wca
Select 
v10s_TKOVR.EventID,
v20c_610_SCMST.SecID,
v10s_TKOVR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_TKOVR.Acttime) and (MPAY.Acttime > RD.Acttime) THEN MPAY.Acttime WHEN RD.Acttime > v10s_TKOVR.Acttime THEN RD.Acttime ELSE v10s_TKOVR.Acttime END as [Changed],
CASE WHEN v10s_TKOVR.ActFlag is null THEN '' ELSE v10s_TKOVR.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
RD.RDID,
MPAY.OptionID,
MPAY.SerialID,
CASE WHEN MPAY.ActFlag IS NULL THEN '' ELSE MPAY.ActFlag END as Actflag_1,
CASE WHEN MPAY.RatioNew IS NULL THEN '' ELSE MPAY.RatioNew END as RatioNew_1,
CASE WHEN MPAY.RatioOld IS NULL THEN '' ELSE MPAY.RatioOld END as RatioOld_1,
CASE WHEN MPAY.MinOfrQty IS NULL THEN '' ELSE MPAY.MinOfrQty END as MinOfrQty_1,
CASE WHEN MPAY.MaxOfrQty IS NULL THEN '' ELSE MPAY.MaxOfrQty END as MaxOfrQty_1,
CASE WHEN MPAY.MinQlyQty IS NULL THEN '' ELSE MPAY.MinQlyQty END as MinQlyQty_1,
CASE WHEN MPAY.MaxQlyQty IS NULL THEN '' ELSE MPAY.MaxQlyQty END as MaxQlyQty_1,
CASE WHEN MPAY.Paydate IS NULL THEN '' ELSE MPAY.Paydate END as Paydate_1,
CASE WHEN MPAY.CurenCD IS NULL THEN '' ELSE MPAY.CurenCD END as CurenCD_1,
CASE WHEN MPAY.MinPrice IS NULL THEN '' ELSE MPAY.MinPrice END as MinPrice_1,
CASE WHEN MPAY.MaxPrice IS NULL THEN '' ELSE MPAY.MaxPrice END as MaxPrice_1,
CASE WHEN MPAY.TndrStrkPrice IS NULL THEN '' ELSE MPAY.TndrStrkPrice END as TndrStrkPrice_1,
CASE WHEN MPAY.TndrStrkStep IS NULL THEN '' ELSE MPAY.TndrStrkStep END as TndrStrkStep_1,
CASE WHEN MPAY.PayType IS NULL THEN '' ELSE MPAY.PayType END as PayType_1,
CASE WHEN MPAY.Fractions IS NULL THEN '' ELSE MPAY.Fractions END as Fractions_1,
CASE WHEN MPAY.SectyCD IS NULL THEN '' ELSE MPAY.SectyCD END as SectyCD_1,
CASE WHEN MPAY.ResSecId is null THEN '' ELSE MPAY.ResSecId END as ResSecId_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN 'ISIN' WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN SC1.Isin WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN SC1.Uscode ELSE '' END as resSID_1,
CASE WHEN (v10s_TKOVR.Hostile IS NULL) THEN '' ELSE v10s_TKOVR.Hostile END as Hostile,
CASE WHEN (v10s_TKOVR.TkovrStatus IS NULL) THEN '' ELSE v10s_TKOVR.TkovrStatus END as TkovrStatus,
CASE WHEN (v10s_TKOVR.OfferorIssID IS NULL) THEN '' ELSE v10s_TKOVR.OfferorIssID END as OfferorIssID,
CASE WHEN (v10s_TKOVR.OfferorName IS NULL) THEN '' ELSE v10s_TKOVR.OfferorName END as OfferorName,
CASE WHEN (v10s_TKOVR.OpenDate IS NULL) THEN '' ELSE v10s_TKOVR.OpenDate END as OpenDate,
CASE WHEN (v10s_TKOVR.CloseDate IS NULL) THEN '' ELSE v10s_TKOVR.CloseDate END as CloseDate,
CASE WHEN (v10s_TKOVR.PreOfferQty IS NULL) THEN '' ELSE v10s_TKOVR.PreOfferQty END as PreOfferQty,
CASE WHEN (v10s_TKOVR.PreOfferPercent IS NULL) THEN '' ELSE v10s_TKOVR.PreOfferPercent END as PreOfferPercent,
CASE WHEN (v10s_TKOVR.TargetQuantity IS NULL) THEN '' ELSE v10s_TKOVR.TargetQuantity END as TargetQuantity,
CASE WHEN (v10s_TKOVR.TargetPercent IS NULL) THEN '' ELSE v10s_TKOVR.TargetPercent END as TargetPercent,
CASE WHEN (v10s_TKOVR.UnconditionalDate IS NULL) THEN '' ELSE v10s_TKOVR.UnconditionalDate END as UnconditionalDate,
CASE WHEN (v10s_TKOVR.CmAcqDate IS NULL) THEN '' ELSE v10s_TKOVR.CmAcqDate END as CmAcqDate,
CASE WHEN (v10s_TKOVR.MinAcpQty IS NULL) THEN '' ELSE v10s_TKOVR.MinAcpQty END as MinAcpQty,
CASE WHEN (v10s_TKOVR.MaxAcpQty IS NULL) THEN '' ELSE v10s_TKOVR.MaxAcpQty END as MaxAcpQty
into wca2.dbo.tISO_TEND_TKOVR
from v10s_Tkovr
INNER JOIN v20c_610_SCMST ON v10s_Tkovr.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN RD ON v10s_TKOVR.RdID = RD.RdID
LEFT OUTER JOIN MPAY ON v10s_TKOVR.EventID = MPAY.EventID and 'TKOVR' = MPAY.SEvent
LEFT OUTER JOIN SCMST as SC1 ON MPAY.ResSecID = SC1.SecID
where (MPAY.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_Tkovr.Acttime >= @Startdate)
go


print " Generating tISO_BONU, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_BONU')
 drop table tISO_BONU

use wca
Select 
v10s_BON.EventID,
v20c_610_SCMST.SecID,
v10s_BON.RdID,
v10s_BON.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_BON.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_BON.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_BON.Acttime) THEN PEXDT.Acttime ELSE v10s_BON.Acttime END as [Changed],
CASE WHEN v10s_BON.ActFlag is null THEN '' ELSE v10s_BON.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
v10s_BON.RatioNew,
v10s_BON.RatioOld,
CASE WHEN v10s_BON.Fractions is null THEN '' ELSE v10s_BON.Fractions END as Fractions,
CASE WHEN v10s_BON.ResSecId is null THEN '' ELSE v10s_BON.ResSecId END as ResSecId,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID,
CASE WHEN v10s_BON.SectyCD is null THEN '' ELSE v10s_BON.SectyCD END as SectyCD,
v10s_BON.LapsedPremium
into wca2.dbo.tISO_BONU
from v10s_BON
INNER JOIN RD ON RD.RdID = v10s_BON.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'BON' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'BON' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_BON.ResSecID = SCMST.SecID
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_BON.Acttime >= @Startdate)
go


print " Generating tISO_DVOP, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_DVOP')
 drop table tISO_DVOP

use wca
Select 
v10s_DIV.EventID,
v20c_610_SCMST.SecID,
v10s_DIV.RdID,
v10s_DIV.AnnounceDate as Created,
CASE WHEN (DP1.Acttime is not null) and (DP1.Acttime > v10s_DIV.Acttime) and (DP1.Acttime > RD.Acttime) and (DP1.Acttime > EXDT.Acttime) and (DP1.Acttime > PEXDT.Acttime) THEN DP1.Acttime WHEN (DP2.Acttime is not null) and (DP2.Acttime > v10s_DIV.Acttime) and (DP2.Acttime > RD.Acttime) and (DP2.Acttime > EXDT.Acttime) and (DP2.Acttime > PEXDT.Acttime) THEN DP2.Acttime WHEN (DP3.Acttime is not null) and (DP3.Acttime > v10s_DIV.Acttime) and (DP3.Acttime > RD.Acttime) and (DP3.Acttime > EXDT.Acttime) and (DP3.Acttime > PEXDT.Acttime) THEN DP3.Acttime WHEN (DP4.Acttime is not null) and (DP4.Acttime > v10s_DIV.Acttime) and (DP4.Acttime > RD.Acttime) and (DP4.Acttime > EXDT.Acttime) and (DP4.Acttime > PEXDT.Acttime) THEN DP4.Acttime WHEN (DP5.Acttime is not null) and (DP5.Acttime > v10s_DIV.Acttime) and (DP5.Acttime > RD.Acttime) and (DP5.Acttime > EXDT.Acttime) and (DP5.Acttime > PEXDT.Acttime) THEN DP5.Acttime WHEN (DP6.Acttime is not null) and (DP6.Acttime > v10s_DIV.Acttime) and (DP6.Acttime > RD.Acttime) and (DP6.Acttime > EXDT.Acttime) and (DP6.Acttime > PEXDT.Acttime) THEN DP6.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIV.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIV.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIV.Acttime) THEN PEXDT.Acttime ELSE v10s_DIV.Acttime END as [Changed],
CASE WHEN v10s_DIV.ActFlag is null THEN '' ELSE v10s_DIV.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
EXDT.Paydate2,
v10s_DIV.FYEDate,
v10s_DIV.DivPeriodCD,
v10s_DIV.Tbaflag,
v10s_DIV.NilDividend,
CASE WHEN DRIP.CntryCD is null THEN '' ELSE DRIP.CntryCD END as DripCntryCD,
CASE WHEN DRIP.Acttime is null THEN '' ELSE DRIP.Acttime END as DripCHANGED,
DRIP.DripLastdate,
DRIP.DripReinvPrice,
DRIP.DripPaydate,
DRIP.Crestdate,
CASE WHEN DP1.OptionID is null THEN '' ELSE DP1.OptionID END as OpID_1,
CASE WHEN DP1.ActFlag is null THEN '' ELSE DP1.ActFlag END as ActFlag_1,
CASE WHEN DP1.Divtype is null THEN '' ELSE DP1.Divtype END as Divtype_1,
CASE WHEN DP1.CurenCD is null THEN '' ELSE DP1.CurenCD END as CurenCD_1,
CASE WHEN DP1.Approxflag is null THEN '' ELSE DP1.Approxflag END as Approxflag_1,
CASE WHEN DP1.DivInPercent is null THEN '' ELSE DP1.DivInPercent END as DivInPercent_1,
CASE WHEN DP1.GrossDividend is null THEN '' ELSE DP1.GrossDividend END as GrossDividend_1,
CASE WHEN DP1.NetDividend is null THEN '' ELSE DP1.NetDividend END as NetDividend_1,
CASE WHEN DP1.TaxRate is null THEN '' ELSE DP1.TaxRate END as TaxRate_1,
CASE WHEN DP1.RecindCashDiv is null THEN '' ELSE DP1.RecindCashDiv END as RecindCashDiv_1,
CASE WHEN DP1.USDRateToCurrency is null THEN '' ELSE DP1.USDRateToCurrency END as USDRateToCurrency_1,
CASE WHEN DP1.SectyCD is null THEN '' ELSE DP1.SectyCD END as SectyCD_1,
CASE WHEN DP1.Fractions is null THEN '' ELSE DP1.Fractions END as Fractions_1,
CASE WHEN DP1.RecindStockDiv is null THEN '' ELSE DP1.RecindStockDiv END as RecindStockDiv_1,
CASE WHEN DP1.Depfees is null THEN '' ELSE DP1.Depfees END as Depfees_1,
CASE WHEN DP1.Coupon is null THEN '' ELSE DP1.Coupon END as Coupon_1,
CASE WHEN DP1.CouponID is null THEN '' ELSE DP1.CouponID END as CouponID_1,
CASE WHEN DP1.ResSecId is null THEN '' ELSE DP1.ResSecId END as ResSecId_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN 'ISIN' WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN SC1.Isin WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN SC1.Uscode ELSE '' END as resSID_1,
CASE WHEN DP1.RatioNew is null THEN '' ELSE DP1.RatioNew END as RatioNew_1,
CASE WHEN DP1.RatioOld is null THEN '' ELSE DP1.RatioOld END as RatioOld_1,
CASE WHEN DP2.OptionID is null THEN '' ELSE DP2.OptionID END as OpID_2,
CASE WHEN DP2.ActFlag is null THEN '' ELSE DP2.ActFlag END as ActFlag_2,
CASE WHEN DP2.Divtype is null THEN '' ELSE DP2.Divtype END as Divtype_2,
CASE WHEN DP2.CurenCD is null THEN '' ELSE DP2.CurenCD END as CurenCD_2,
CASE WHEN DP2.Approxflag is null THEN '' ELSE DP2.Approxflag END as Approxflag_2,
CASE WHEN DP2.DivInPercent is null THEN '' ELSE DP2.DivInPercent END as DivInPercent_2,
CASE WHEN DP2.GrossDividend is null THEN '' ELSE DP2.GrossDividend END as GrossDividend_2,
CASE WHEN DP2.NetDividend is null THEN '' ELSE DP2.NetDividend END as NetDividend_2,
CASE WHEN DP2.TaxRate is null THEN '' ELSE DP2.TaxRate END as TaxRate_2,
CASE WHEN DP2.RecindCashDiv is null THEN '' ELSE DP2.RecindCashDiv END as RecindCashDiv_2,
CASE WHEN DP2.USDRateToCurrency is null THEN '' ELSE DP2.USDRateToCurrency END as USDRateToCurrency_2,
CASE WHEN DP2.SectyCD is null THEN '' ELSE DP2.SectyCD END as SectyCD_2,
CASE WHEN DP2.Fractions is null THEN '' ELSE DP2.Fractions END as Fractions_2,
CASE WHEN DP2.RecindStockDiv is null THEN '' ELSE DP2.RecindStockDiv END as RecindStockDiv_2,
CASE WHEN DP2.Depfees is null THEN '' ELSE DP2.Depfees END as Depfees_2,
CASE WHEN DP2.Coupon is null THEN '' ELSE DP2.Coupon END as Coupon_2,
CASE WHEN DP2.CouponID is null THEN '' ELSE DP2.CouponID END as CouponID_2,
CASE WHEN DP2.ResSecId is null THEN '' ELSE DP2.ResSecId END as ResSecId_2,
CASE WHEN (SC2.Isin IS NOT NULL AND SC2.Isin <> '') THEN 'ISIN' WHEN (SC2.Uscode IS NOT NULL AND SC2.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_2,
CASE WHEN (SC2.Isin IS NOT NULL AND SC2.Isin <> '') THEN SC2.Isin WHEN (SC2.Uscode IS NOT NULL AND SC2.Uscode <> '')THEN SC2.Uscode ELSE '' END as resSID_2,
CASE WHEN DP2.RatioNew is null THEN '' ELSE DP2.RatioNew END as RatioNew_2,
CASE WHEN DP2.RatioOld is null THEN '' ELSE DP2.RatioOld END as RatioOld_2,
CASE WHEN DP3.OptionID is null THEN '' ELSE DP3.OptionID END as OpID_3,
CASE WHEN DP3.ActFlag is null THEN '' ELSE DP3.ActFlag END as ActFlag_3,
CASE WHEN DP3.Divtype is null THEN '' ELSE DP3.Divtype END as Divtype_3,
CASE WHEN DP3.CurenCD is null THEN '' ELSE DP3.CurenCD END as CurenCD_3,
CASE WHEN DP3.Approxflag is null THEN '' ELSE DP3.Approxflag END as Approxflag_3,
CASE WHEN DP3.DivInPercent is null THEN '' ELSE DP3.DivInPercent END as DivInPercent_3,
CASE WHEN DP3.GrossDividend is null THEN '' ELSE DP3.GrossDividend END as GrossDividend_3,
CASE WHEN DP3.NetDividend is null THEN '' ELSE DP3.NetDividend END as NetDividend_3,
CASE WHEN DP3.TaxRate is null THEN '' ELSE DP3.TaxRate END as TaxRate_3,
CASE WHEN DP3.RecindCashDiv is null THEN '' ELSE DP3.RecindCashDiv END as RecindCashDiv_3,
CASE WHEN DP3.USDRateToCurrency is null THEN '' ELSE DP3.USDRateToCurrency END as USDRateToCurrency_3,
CASE WHEN DP3.SectyCD is null THEN '' ELSE DP3.SectyCD END as SectyCD_3,
CASE WHEN DP3.Fractions is null THEN '' ELSE DP3.Fractions END as Fractions_3,
CASE WHEN DP3.RecindStockDiv is null THEN '' ELSE DP3.RecindStockDiv END as RecindStockDiv_3,
CASE WHEN DP3.Depfees is null THEN '' ELSE DP3.Depfees END as Depfees_3,
CASE WHEN DP3.Coupon is null THEN '' ELSE DP3.Coupon END as Coupon_3,
CASE WHEN DP3.CouponID is null THEN '' ELSE DP3.CouponID END as CouponID_3,
CASE WHEN DP3.ResSecId is null THEN '' ELSE DP3.ResSecId END as ResSecId_3,
CASE WHEN (SC3.Isin IS NOT NULL AND SC3.Isin <> '') THEN 'ISIN' WHEN (SC3.Uscode IS NOT NULL AND SC3.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_3,
CASE WHEN (SC3.Isin IS NOT NULL AND SC3.Isin <> '') THEN SC3.Isin WHEN (SC3.Uscode IS NOT NULL AND SC3.Uscode <> '')THEN SC3.Uscode ELSE '' END as resSID_3,
CASE WHEN DP3.RatioNew is null THEN '' ELSE DP3.RatioNew END as RatioNew_3,
CASE WHEN DP3.RatioOld is null THEN '' ELSE DP3.RatioOld END as RatioOld_3,
CASE WHEN DP4.OptionID is null THEN '' ELSE DP4.OptionID END as OpID_4,
CASE WHEN DP4.ActFlag is null THEN '' ELSE DP4.ActFlag END as ActFlag_4,
CASE WHEN DP4.Divtype is null THEN '' ELSE DP4.Divtype END as Divtype_4,
CASE WHEN DP4.CurenCD is null THEN '' ELSE DP4.CurenCD END as CurenCD_4,
CASE WHEN DP4.Approxflag is null THEN '' ELSE DP4.Approxflag END as Approxflag_4,
CASE WHEN DP4.DivInPercent is null THEN '' ELSE DP4.DivInPercent END as DivInPercent_4,
CASE WHEN DP4.GrossDividend is null THEN '' ELSE DP4.GrossDividend END as GrossDividend_4,
CASE WHEN DP4.NetDividend is null THEN '' ELSE DP4.NetDividend END as NetDividend_4,
CASE WHEN DP4.TaxRate is null THEN '' ELSE DP4.TaxRate END as TaxRate_4,
CASE WHEN DP4.RecindCashDiv is null THEN '' ELSE DP4.RecindCashDiv END as RecindCashDiv_4,
CASE WHEN DP4.USDRateToCurrency is null THEN '' ELSE DP4.USDRateToCurrency END as USDRateToCurrency_4,
CASE WHEN DP4.SectyCD is null THEN '' ELSE DP4.SectyCD END as SectyCD_4,
CASE WHEN DP4.Fractions is null THEN '' ELSE DP4.Fractions END as Fractions_4,
CASE WHEN DP4.RecindStockDiv is null THEN '' ELSE DP4.RecindStockDiv END as RecindStockDiv_4,
CASE WHEN DP4.Depfees is null THEN '' ELSE DP4.Depfees END as Depfees_4,
CASE WHEN DP4.Coupon is null THEN '' ELSE DP4.Coupon END as Coupon_4,
CASE WHEN DP4.CouponID is null THEN '' ELSE DP4.CouponID END as CouponID_4,
CASE WHEN DP4.ResSecId is null THEN '' ELSE DP4.ResSecId END as ResSecId_4,
CASE WHEN (SC4.Isin IS NOT NULL AND SC4.Isin <> '') THEN 'ISIN' WHEN (SC4.Uscode IS NOT NULL AND SC4.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_4,
CASE WHEN (SC4.Isin IS NOT NULL AND SC4.Isin <> '') THEN SC4.Isin WHEN (SC4.Uscode IS NOT NULL AND SC4.Uscode <> '')THEN SC4.Uscode ELSE '' END as resSID_4,
CASE WHEN DP4.RatioNew is null THEN '' ELSE DP4.RatioNew END as RatioNew_4,
CASE WHEN DP4.RatioOld is null THEN '' ELSE DP4.RatioOld END as RatioOld_4,
CASE WHEN DP5.OptionID is null THEN '' ELSE DP5.OptionID END as OpID_5,
CASE WHEN DP5.ActFlag is null THEN '' ELSE DP5.ActFlag END as ActFlag_5,
CASE WHEN DP5.Divtype is null THEN '' ELSE DP5.Divtype END as Divtype_5,
CASE WHEN DP5.CurenCD is null THEN '' ELSE DP5.CurenCD END as CurenCD_5,
CASE WHEN DP5.Approxflag is null THEN '' ELSE DP5.Approxflag END as Approxflag_5,
CASE WHEN DP5.DivInPercent is null THEN '' ELSE DP5.DivInPercent END as DivInPercent_5,
CASE WHEN DP5.GrossDividend is null THEN '' ELSE DP5.GrossDividend END as GrossDividend_5,
CASE WHEN DP5.NetDividend is null THEN '' ELSE DP5.NetDividend END as NetDividend_5,
CASE WHEN DP5.TaxRate is null THEN '' ELSE DP5.TaxRate END as TaxRate_5,
CASE WHEN DP5.RecindCashDiv is null THEN '' ELSE DP5.RecindCashDiv END as RecindCashDiv_5,
CASE WHEN DP5.USDRateToCurrency is null THEN '' ELSE DP5.USDRateToCurrency END as USDRateToCurrency_5,
CASE WHEN DP5.SectyCD is null THEN '' ELSE DP5.SectyCD END as SectyCD_5,
CASE WHEN DP5.Fractions is null THEN '' ELSE DP5.Fractions END as Fractions_5,
CASE WHEN DP5.RecindStockDiv is null THEN '' ELSE DP5.RecindStockDiv END as RecindStockDiv_5,
CASE WHEN DP5.Depfees is null THEN '' ELSE DP5.Depfees END as Depfees_5,
CASE WHEN DP5.Coupon is null THEN '' ELSE DP5.Coupon END as Coupon_5,
CASE WHEN DP5.CouponID is null THEN '' ELSE DP5.CouponID END as CouponID_5,
CASE WHEN DP5.ResSecId is null THEN '' ELSE DP5.ResSecId END as ResSecId_5,
CASE WHEN (SC5.Isin IS NOT NULL AND SC5.Isin <> '') THEN 'ISIN' WHEN (SC5.Uscode IS NOT NULL AND SC5.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_5,
CASE WHEN (SC5.Isin IS NOT NULL AND SC5.Isin <> '') THEN SC5.Isin WHEN (SC5.Uscode IS NOT NULL AND SC5.Uscode <> '')THEN SC5.Uscode ELSE '' END as resSID_5,
CASE WHEN DP5.RatioNew is null THEN '' ELSE DP5.RatioNew END as RatioNew_5,
CASE WHEN DP5.RatioOld is null THEN '' ELSE DP5.RatioOld END as RatioOld_5,
CASE WHEN DP6.OptionID is null THEN '' ELSE DP6.OptionID END as OpID_6,
CASE WHEN DP6.ActFlag is null THEN '' ELSE DP6.ActFlag END as ActFlag_6,
CASE WHEN DP6.Divtype is null THEN '' ELSE DP6.Divtype END as Divtype_6,
CASE WHEN DP6.CurenCD is null THEN '' ELSE DP6.CurenCD END as CurenCD_6,
CASE WHEN DP6.Approxflag is null THEN '' ELSE DP6.Approxflag END as Approxflag_6,
CASE WHEN DP6.DivInPercent is null THEN '' ELSE DP6.DivInPercent END as DivInPercent_6,
CASE WHEN DP6.GrossDividend is null THEN '' ELSE DP6.GrossDividend END as GrossDividend_6,
CASE WHEN DP6.NetDividend is null THEN '' ELSE DP6.NetDividend END as NetDividend_6,
CASE WHEN DP6.TaxRate is null THEN '' ELSE DP6.TaxRate END as TaxRate_6,
CASE WHEN DP6.RecindCashDiv is null THEN '' ELSE DP6.RecindCashDiv END as RecindCashDiv_6,
CASE WHEN DP6.USDRateToCurrency is null THEN '' ELSE DP6.USDRateToCurrency END as USDRateToCurrency_6,
CASE WHEN DP6.SectyCD is null THEN '' ELSE DP6.SectyCD END as SectyCD_6,
CASE WHEN DP6.Fractions is null THEN '' ELSE DP6.Fractions END as Fractions_6,
CASE WHEN DP6.RecindStockDiv is null THEN '' ELSE DP6.RecindStockDiv END as RecindStockDiv_6,
CASE WHEN DP6.Depfees is null THEN '' ELSE DP6.Depfees END as Depfees_6,
CASE WHEN DP6.Coupon is null THEN '' ELSE DP6.Coupon END as Coupon_6,
CASE WHEN DP6.CouponID is null THEN '' ELSE DP6.CouponID END as CouponID_6,
CASE WHEN DP6.ResSecId is null THEN '' ELSE DP6.ResSecId END as ResSecId_6,
CASE WHEN (SC6.Isin IS NOT NULL AND SC6.Isin <> '') THEN 'ISIN' WHEN (SC6.Uscode IS NOT NULL AND SC6.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_6,
CASE WHEN (SC6.Isin IS NOT NULL AND SC6.Isin <> '') THEN SC6.Isin WHEN (SC6.Uscode IS NOT NULL AND SC6.Uscode <> '')THEN SC6.Uscode ELSE '' END as resSID_6,
CASE WHEN DP6.RatioNew is null THEN '' ELSE DP6.RatioNew END as RatioNew_6,
CASE WHEN DP6.RatioOld is null THEN '' ELSE DP6.RatioOld END as RatioOld_6
into wca2.dbo.tISO_DVOP
from v10s_DIV
INNER JOIN RD ON RD.RdID = v10s_DIV.RdID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN DRIP ON v10s_DIV.EventID = DRIP.DivID and v20c_ISO_SCEXH.CntryCD = DRIP.CntryCD
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIV' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
LEFT OUTER join v10s_divpy as DP1 ON v10s_DIV.EventID = DP1.DivID and DP1.OptionID=1
LEFT OUTER join v10s_divpy as DP2 ON v10s_DIV.EventID = DP2.DivID and DP2.OptionID=2
LEFT OUTER join v10s_divpy as DP3 ON v10s_DIV.EventID = DP3.DivID and DP3.OptionID=3
LEFT OUTER join v10s_divpy as DP4 ON v10s_DIV.EventID = DP4.DivID and DP4.OptionID=4
LEFT OUTER join v10s_divpy as DP5 ON v10s_DIV.EventID = DP5.DivID and DP5.OptionID=5
LEFT OUTER join v10s_divpy as DP6 ON v10s_DIV.EventID = DP6.DivID and DP6.OptionID=6
LEFT OUTER JOIN SCMST as SC1 ON DP1.ResSecID = SC1.SecID
LEFT OUTER JOIN SCMST as SC2 ON DP2.ResSecID = SC2.SecID
LEFT OUTER JOIN SCMST as SC3 ON DP3.ResSecID = SC3.SecID
LEFT OUTER JOIN SCMST as SC4 ON DP4.ResSecID = SC4.SecID
LEFT OUTER JOIN SCMST as SC5 ON DP5.ResSecID = SC5.SecID
LEFT OUTER JOIN SCMST as SC6 ON DP6.ResSecID = SC6.SecID
where (DP1.Acttime >= @Startdate
or DP2.Acttime >= @Startdate
or DP3.Acttime >= @Startdate
or DP4.Acttime >= @Startdate
or DP5.Acttime >= @Startdate
or DP6.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_DIV.Acttime >= @Startdate)
go


print " Generating tISO_RHTSn, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_RHTSn')
 drop table tISO_RHTSn

use wca
Select 
v10s_ENT.EventID,
v20c_610_SCMST.SecID,
v10s_ENT.RdID,
v10s_ENT.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ENT.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ENT.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ENT.Acttime) THEN PEXDT.Acttime ELSE v10s_ENT.Acttime END as [Changed],
CASE WHEN v10s_ENT.ActFlag is null THEN '' ELSE v10s_ENT.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN RD.Recdate is null THEN '' ELSE RD.Recdate END as Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_ENT.RatioNew is null THEN '' ELSE v10s_ENT.RatioNew END as RatioNew,
CASE WHEN v10s_ENT.RatioOld is null THEN '' ELSE v10s_ENT.RatioOld END as RatioOld,
CASE WHEN v10s_ENT.Fractions is null THEN '' ELSE v10s_ENT.Fractions END as Fractions,
CASE WHEN v10s_ENT.ResSecId is null THEN '' ELSE v10s_ENT.ResSecId END as ResSecId,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID,
CASE WHEN v10s_ENT.SectyCD is null THEN '' ELSE v10s_ENT.SectyCD END as SectyCD,
CASE WHEN v10s_ENT.CurenCD is null THEN '' ELSE v10s_ENT.CurenCD END as CurenCD,
CASE WHEN v10s_ENT.ENTIssuePrice is null THEN '' ELSE v10s_ENT.ENTIssuePrice END as ENTIssuePrice,
CASE WHEN v10s_ENT.Startsubscription is null THEN '' ELSE v10s_ENT.StartSubscription END as StartSubscription,
CASE WHEN v10s_ENT.EndSubscription is null THEN '' ELSE v10s_ENT.EndSubscription END as EndSubscription,
CASE WHEN v10s_ENT.OverSubscription is null THEN '' ELSE v10s_ENT.OverSubscription END as OverSubscription
into wca2.dbo.tISO_RHTSn
from v10s_ENT
INNER JOIN RD ON RD.RdID = v10s_ENT.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ENT' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ENT' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_ENT.ResSecID = SCMST.SecID
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_ENT.Acttime >= @Startdate)


go


print " Generating tISO_RHTSt, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_RHTSt')
 drop table tISO_RHTSt

use wca
Select 
v10s_RTS.EventID,
v20c_610_SCMST.SecID,
v10s_RTS.RdID,
v10s_RTS.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_RTS.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_RTS.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_RTS.Acttime) THEN PEXDT.Acttime ELSE v10s_RTS.Acttime END as [Changed],
CASE WHEN v10s_RTS.ActFlag is null THEN '' ELSE v10s_RTS.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
CASE WHEN RD.Recdate is null THEN '' ELSE RD.Recdate END as Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_RTS.RatioNew is null THEN '' ELSE v10s_RTS.RatioNew END as RatioNew,
CASE WHEN v10s_RTS.RatioOld is null THEN '' ELSE v10s_RTS.RatioOld END as RatioOld,
CASE WHEN v10s_RTS.Fractions is null THEN '' ELSE v10s_RTS.Fractions END as Fractions,
CASE WHEN v10s_RTS.ResSecId is null THEN '' ELSE v10s_RTS.ResSecId END as ResSecId,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID,
CASE WHEN (TraSCMST.Isin IS NOT NULL AND TraSCMST.Isin <> '') THEN 'ISIN' WHEN (TraSCMST.Uscode IS NOT NULL AND TraSCMST.Uscode <> '')THEN '/US/' ELSE '' END as traSIDname,
CASE WHEN (TraSCMST.Isin IS NOT NULL AND TraSCMST.Isin <> '') THEN TraSCMST.Isin WHEN (TraSCMST.Uscode IS NOT NULL AND TraSCMST.Uscode <> '')THEN TraSCMST.Uscode ELSE '' END as traSID,
CASE WHEN v10s_RTS.SectyCD is null THEN '' ELSE v10s_RTS.SectyCD END as SectyCD,
CASE WHEN v10s_RTS.CurenCD is null THEN '' ELSE v10s_RTS.CurenCD END as CurenCD,
CASE WHEN v10s_RTS.IssuePrice is null THEN '' ELSE v10s_RTS.IssuePrice END as IssuePrice,
CASE WHEN v10s_RTS.StartSubscription is null THEN '' ELSE v10s_RTS.StartSubscription END as StartSubscription,
CASE WHEN v10s_RTS.EndSubscription is null THEN '' ELSE v10s_RTS.EndSubscription END as EndSubscription,
CASE WHEN v10s_RTS.SplitDate is null THEN '' ELSE v10s_RTS.SplitDate END as SplitDate,
CASE WHEN v10s_RTS.StartTrade is null THEN '' ELSE v10s_RTS.StartTrade END as StartTrade,
CASE WHEN v10s_RTS.EndTrade is null THEN '' ELSE v10s_RTS.EndTrade END as EndTrade,
CASE WHEN v10s_RTS.LapsedPremium is null THEN '' ELSE v10s_RTS.LapsedPremium END as LapsedPremium,
CASE WHEN v10s_RTS.OverSubscription is null THEN '' ELSE v10s_RTS.OverSubscription END as OverSubscription,
CASE WHEN ISSUR.Issuername is null THEN '' ELSE ISSUR.Issuername END as tIssuername,
CASE WHEN TraSCMST.SecurityDesc is null THEN '' ELSE TraSCMST.SecurityDesc END as tSecurityDesc,
CASE WHEN sedolseq1.SEDOL is null THEN '' ELSE sedolseq1.SEDOL END as tSedol,
CASE WHEN SCEXH.Localcode is null THEN '' ELSE SCEXH.Localcode END as tLocalcode
into wca2.dbo.tISO_RHTSt
from v10s_RTS
INNER JOIN RD ON RD.RdID = v10s_RTS.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'RTS' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'RTS' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_RTS.ResSecID = SCMST.SecID
LEFT OUTER JOIN SCMST as TraSCMST ON v10s_RTS.TraSecID = TraSCMST.SecID
LEFT OUTER JOIN SCEXH ON v10s_RTS.TraSecID = SCEXH.SecID
                        and v20c_ISO_SCEXH.ExchgCD = SCEXH.ExchgCD
LEFT OUTER JOIN sedolseq1 ON v10s_RTS.TraSecID = sedolseq1.SecID
                        and v20c_ISO_SCEXH.CntryCD = sedolseq1.CntryCD
                        and '1' = sedolseq1.Seqnum
LEFT OUTER JOIN ISSUR ON TraSCMST.IssID = ISSUR.IssID
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_RTS.Acttime >= @Startdate)
go


print " Generating tISO_OTHR_ARR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_OTHR_ARR')
 drop table tISO_OTHR_ARR

use wca
Select 
v10s_ARR.EventID,
v20c_610_SCMST.SecID,
v10s_ARR.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_ARR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_ARR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_ARR.Acttime) THEN PEXDT.Acttime ELSE v10s_ARR.Acttime END as [Changed],
CASE WHEN v10s_ARR.ActFlag is null THEN '' ELSE v10s_ARR.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
RD.RDID,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate
into wca2.dbo.tISO_OTHR_ARR
from v10s_ARR
INNER JOIN RD ON RD.RdID = v10s_ARR.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'ARR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'ARR' = PEXDT.EventType
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_ARR.Acttime >= @Startdate)
go


print " Generating tISO_OTHR_DVST, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_OTHR_DVST')
 drop table tISO_OTHR_DVST

use wca
Select 
v10s_DVST.EventID,
v20c_610_SCMST.SecID,
v10s_DVST.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DVST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DVST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DVST.Acttime) THEN PEXDT.Acttime ELSE v10s_DVST.Acttime END as [Changed],
CASE WHEN v10s_DVST.ActFlag is null THEN '' ELSE v10s_DVST.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
RD.RDID,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_DVST.CurenCD IS NULL) THEN '' ELSE v10s_DVST.CurenCD END as CurenCD,
CASE WHEN (v10s_DVST.RatioNew IS NULL) THEN '' ELSE v10s_DVST.RatioNew END as RatioNew,
CASE WHEN (v10s_DVST.RatioOld IS NULL) THEN '' ELSE v10s_DVST.RatioOld END as RatioOld,
CASE WHEN (v10s_DVST.Fractions IS NULL) THEN '' ELSE v10s_DVST.Fractions END as Fractions,
CASE WHEN (v10s_DVST.StartSubscription IS NULL) THEN '' ELSE v10s_DVST.StartSubscription END as StartSubscription,
CASE WHEN (v10s_DVST.EndSubscription IS NULL) THEN '' ELSE v10s_DVST.EndSubscription END as EndSubscription,
CASE WHEN (v10s_DVST.TndrStrkPrice IS NULL) THEN '' ELSE v10s_DVST.TndrStrkPrice END as TndrStrkPrice,
CASE WHEN (v10s_DVST.TndrPriceStep IS NULL) THEN '' ELSE v10s_DVST.TndrPriceStep END as TndrPriceStep,
CASE WHEN (v10s_DVST.MinPrice IS NULL) THEN '' ELSE v10s_DVST.MinPrice END as MinPrice,
CASE WHEN (v10s_DVST.MaxPrice IS NULL) THEN '' ELSE v10s_DVST.MaxPrice END as MaxPrice,
CASE WHEN (v10s_DVST.MinQlyQty IS NULL) THEN '' ELSE v10s_DVST.MinQlyQty END as MinQlyQty,
CASE WHEN (v10s_DVST.MaxQlyQty IS NULL) THEN '' ELSE v10s_DVST.MaxQlyQty END as MaxQlyQty,
CASE WHEN (v10s_DVST.MinAcpQty IS NULL) THEN '' ELSE v10s_DVST.MinAcpQty END as MinAcpQty,
CASE WHEN (v10s_DVST.MaxAcpQty IS NULL) THEN '' ELSE v10s_DVST.MaxAcpQty END as MaxAcpQty,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID,
CASE WHEN (TraSCMST.Isin IS NOT NULL AND TraSCMST.Isin <> '') THEN 'ISIN' WHEN (TraSCMST.Uscode IS NOT NULL AND TraSCMST.Uscode <> '')THEN '/US/' ELSE '' END as traSIDname,
CASE WHEN (TraSCMST.Isin IS NOT NULL AND TraSCMST.Isin <> '') THEN TraSCMST.Isin WHEN (TraSCMST.Uscode IS NOT NULL AND TraSCMST.Uscode <> '')THEN TraSCMST.Uscode ELSE '' END as traSID
into wca2.dbo.tISO_OTHR_DVST
from v10s_DVST
INNER JOIN RD ON RD.RdID = v10s_DVST.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DVST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DVST' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_DVST.ResSecID = SCMST.SecID
LEFT OUTER JOIN SCMST as TraSCMST ON v10s_DVST.TraSecID = TraSCMST.SecID
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_DVST.Acttime >= @Startdate)
go


print " Generating tISO_PRIO, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_PRIO')
 drop table tISO_PRIO

use wca
Select 
v10s_PRF.EventID,
v20c_610_SCMST.SecID,
v10s_PRF.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PRF.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PRF.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PRF.Acttime) THEN PEXDT.Acttime ELSE v10s_PRF.Acttime END as [Changed],
CASE WHEN v10s_PRF.ActFlag is null THEN '' ELSE v10s_PRF.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN EXDT.PayDate2 is not null THEN EXDT.PayDate2 ELSE PEXDT.PayDate2 END as PayDate2,
CASE WHEN v10s_PRF.OffereeName is null THEN '' ELSE v10s_PRF.OffereeName END as OffereeName,
CASE WHEN v10s_PRF.CurenCD is null THEN '' ELSE v10s_PRF.CurenCD END as CurenCD,
CASE WHEN v10s_PRF.StartSubscription is null THEN '' ELSE v10s_PRF.StartSubscription END as StartSubscription,
CASE WHEN v10s_PRF.EndSubscription is null THEN '' ELSE v10s_PRF.EndSubscription END as EndSubscription,
CASE WHEN v10s_PRF.MinPrice is null THEN '' ELSE v10s_PRF.MinPrice END as MinPrice,
CASE WHEN v10s_PRF.MaxPrice is null THEN '' ELSE v10s_PRF.MaxPrice END as MaxPrice,
CASE WHEN v10s_PRF.SectyCD is null THEN '' ELSE v10s_PRF.SectyCD END as SectyCD,
CASE WHEN v10s_PRF.Fractions is null THEN '' ELSE v10s_PRF.Fractions END as Fractions,
CASE WHEN v10s_PRF.ResSecId is null THEN '' ELSE v10s_PRF.ResSecId END as ResSecId,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN 'ISIN' WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname,
CASE WHEN (SCMST.Isin IS NOT NULL AND SCMST.Isin <> '') THEN SCMST.Isin WHEN (SCMST.Uscode IS NOT NULL AND SCMST.Uscode <> '')THEN SCMST.Uscode ELSE '' END as resSID,
CASE WHEN v10s_PRF.RatioNew is null THEN '' ELSE v10s_PRF.RatioNew END as RatioNew,
CASE WHEN v10s_PRF.RatioOld is null THEN '' ELSE v10s_PRF.RatioOld END as RatioOld
into wca2.dbo.tISO_PRIO
from v10s_PRF
INNER JOIN RD ON RD.RdID = v10s_PRF.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PRF' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PRF' = PEXDT.EventType
LEFT OUTER JOIN SCMST ON v10s_PRF.ResSecID = SCMST.SecID
where (EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_PRF.Acttime >= @Startdate)
go

print " Generating tISO_SOFF_DIST, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_SOFF_DIST')
 drop table tISO_SOFF_DIST

use wca
Select 
v10s_DIST.EventID,
v20c_610_SCMST.SecID,
v10s_DIST.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_DIST.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DIST.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DIST.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DIST.Acttime) THEN PEXDT.Acttime ELSE v10s_DIST.Acttime END as [Changed],
CASE WHEN v10s_DIST.ActFlag is null THEN '' ELSE v10s_DIST.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
RD.RDID,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
MPAY.OptionID,
MPAY.SerialID,
CASE WHEN MPAY.ActFlag IS NULL THEN '' ELSE MPAY.ActFlag END as Actflag_1,
CASE WHEN MPAY.RatioNew IS NULL THEN '' ELSE MPAY.RatioNew END as RatioNew_1,
CASE WHEN MPAY.RatioOld IS NULL THEN '' ELSE MPAY.RatioOld END as RatioOld_1,
CASE WHEN MPAY.MinOfrQty IS NULL THEN '' ELSE MPAY.MinOfrQty END as MinOfrQty_1,
CASE WHEN MPAY.MaxOfrQty IS NULL THEN '' ELSE MPAY.MaxOfrQty END as MaxOfrQty_1,
CASE WHEN MPAY.MinQlyQty IS NULL THEN '' ELSE MPAY.MinQlyQty END as MinQlyQty_1,
CASE WHEN MPAY.MaxQlyQty IS NULL THEN '' ELSE MPAY.MaxQlyQty END as MaxQlyQty_1,
CASE WHEN MPAY.Paydate IS NULL THEN '' ELSE MPAY.Paydate END as Paydate_1,
CASE WHEN MPAY.CurenCD IS NULL THEN '' ELSE MPAY.CurenCD END as CurenCD_1,
CASE WHEN MPAY.MinPrice IS NULL THEN '' ELSE MPAY.MinPrice END as MinPrice_1,
CASE WHEN MPAY.MaxPrice IS NULL THEN '' ELSE MPAY.MaxPrice END as MaxPrice_1,
CASE WHEN MPAY.TndrStrkPrice IS NULL THEN '' ELSE MPAY.TndrStrkPrice END as TndrStrkPrice_1,
CASE WHEN MPAY.TndrStrkStep IS NULL THEN '' ELSE MPAY.TndrStrkStep END as TndrStrkStep_1,
CASE WHEN MPAY.PayType IS NULL THEN '' ELSE MPAY.PayType END as PayType_1,
CASE WHEN MPAY.Fractions IS NULL THEN '' ELSE MPAY.Fractions END as Fractions_1,
CASE WHEN MPAY.SectyCD IS NULL THEN '' ELSE MPAY.SectyCD END as SectyCD_1,
CASE WHEN MPAY.ResSecId is null THEN '' ELSE MPAY.ResSecId END as ResSecId_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN 'ISIN' WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN SC1.Isin WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN SC1.Uscode ELSE '' END as resSID_1
into wca2.dbo.tISO_SOFF_DIST
from v10s_DIST
INNER JOIN RD ON RD.RdID = v10s_DIST.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DIST' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIST' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DIST.EventID = MPAY.EventID and 'DIST' = MPAY.SEvent
LEFT OUTER JOIN SCMST as SC1 ON MPAY.ResSecID = SC1.SecID
where (MPAY.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_DIST.Acttime >= @Startdate)
go


print " Generating tISO_SOFF_DMRGR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_SOFF_DMRGR')
 drop table tISO_SOFF_DMRGR

use wca
Select 
v10s_DMRGR.EventID,
v20c_610_SCMST.SecID,
v10s_DMRGR.AnnounceDate as Created,
CASE WHEN (MPAY.Acttime is not null) and (MPAY.Acttime > v10s_DMRGR.Acttime) and (MPAY.Acttime > RD.Acttime) and (MPAY.Acttime > EXDT.Acttime) and (MPAY.Acttime > PEXDT.Acttime) THEN MPAY.Acttime WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_DMRGR.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_DMRGR.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_DMRGR.Acttime) THEN PEXDT.Acttime ELSE v10s_DMRGR.Acttime END as [Changed],
CASE WHEN v10s_DMRGR.ActFlag is null THEN '' ELSE v10s_DMRGR.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
RD.RDID,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_DMRGR.EffectiveDate is null THEN '' ELSE v10s_DMRGR.EffectiveDate END as EffectiveDate,
MPAY.OptionID,
MPAY.SerialID,
CASE WHEN MPAY.ActFlag IS NULL THEN '' ELSE MPAY.ActFlag END as Actflag_1,
CASE WHEN MPAY.RatioNew IS NULL THEN '' ELSE MPAY.RatioNew END as RatioNew_1,
CASE WHEN MPAY.RatioOld IS NULL THEN '' ELSE MPAY.RatioOld END as RatioOld_1,
CASE WHEN MPAY.MinOfrQty IS NULL THEN '' ELSE MPAY.MinOfrQty END as MinOfrQty_1,
CASE WHEN MPAY.MaxOfrQty IS NULL THEN '' ELSE MPAY.MaxOfrQty END as MaxOfrQty_1,
CASE WHEN MPAY.MinQlyQty IS NULL THEN '' ELSE MPAY.MinQlyQty END as MinQlyQty_1,
CASE WHEN MPAY.MaxQlyQty IS NULL THEN '' ELSE MPAY.MaxQlyQty END as MaxQlyQty_1,
CASE WHEN MPAY.Paydate IS NULL THEN '' ELSE MPAY.Paydate END as Paydate_1,
CASE WHEN MPAY.CurenCD IS NULL THEN '' ELSE MPAY.CurenCD END as CurenCD_1,
CASE WHEN MPAY.MinPrice IS NULL THEN '' ELSE MPAY.MinPrice END as MinPrice_1,
CASE WHEN MPAY.MaxPrice IS NULL THEN '' ELSE MPAY.MaxPrice END as MaxPrice_1,
CASE WHEN MPAY.TndrStrkPrice IS NULL THEN '' ELSE MPAY.TndrStrkPrice END as TndrStrkPrice_1,
CASE WHEN MPAY.TndrStrkStep IS NULL THEN '' ELSE MPAY.TndrStrkStep END as TndrStrkStep_1,
CASE WHEN MPAY.PayType IS NULL THEN '' ELSE MPAY.PayType END as PayType_1,
CASE WHEN MPAY.Fractions IS NULL THEN '' ELSE MPAY.Fractions END as Fractions_1,
CASE WHEN MPAY.SectyCD IS NULL THEN '' ELSE MPAY.SectyCD END as SectyCD_1,
CASE WHEN MPAY.ResSecId is null THEN '' ELSE MPAY.ResSecId END as ResSecId_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN 'ISIN' WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN '/US/' ELSE '' END as resSIDname_1,
CASE WHEN (SC1.Isin IS NOT NULL AND SC1.Isin <> '') THEN SC1.Isin WHEN (SC1.Uscode IS NOT NULL AND SC1.Uscode <> '')THEN SC1.Uscode ELSE '' END as resSID_1
into wca2.dbo.tISO_SOFF_DMRGR
from v10s_DMRGR
INNER JOIN RD ON RD.RdID = v10s_DMRGR.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'DMRGR' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'DMRGR' = PEXDT.EventType
LEFT OUTER JOIN MPAY ON v10s_DMRGR.EventID = MPAY.EventID and 'DMRGR' = MPAY.SEvent
LEFT OUTER JOIN SCMST as SC1 ON MPAY.ResSecID = SC1.SecID
where (MPAY.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate
or v10s_DMRGR.Acttime >= @Startdate)
go


print " Generating tISO_SPLF, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_SPLF')
 drop table tISO_SPLF

use wca
Select 
v10s_SD.EventID,
v20c_610_SCMST.SecID,
v10s_SD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_SD.Acttime)
and (RD.Acttime > EXDT.Acttime or  EXDT.Acttime is null) 
and (RD.Acttime > PEXDT.Acttime or PEXDT.Acttime is null)
and (RD.Acttime > ICC.Acttime or ICC.Acttime is null)
and (RD.Acttime > SDCHG.Acttime or SDCHG.Acttime is null) 
THEN RD.Acttime
 WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_SD.Acttime)
 and (EXDT.Acttime > PEXDT.Acttime) and (EXDT.Acttime > ICC.Acttime)
 and (EXDT.Acttime > SDCHG.Acttime) THEN EXDT.Acttime 
WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_SD.Acttime) 
and (PEXDT.Acttime > ICC.Acttime) and (PEXDT.Acttime > SDCHG.Acttime)
 THEN PEXDT.Acttime 
WHEN (ICC.Acttime is not null) and (ICC.Acttime > v10s_SD.Acttime)
 and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime 
WHEN (SDCHG.Acttime is not null) and (SDCHG.Acttime > v10s_SD.Acttime) THEN SDCHG.Acttime
 ELSE v10s_SD.Acttime END as [Changed],
CASE WHEN v10s_SD.ActFlag is null THEN '' ELSE v10s_SD.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN 'ISIN' WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN '/GB/' WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN '/US/' ELSE '' END as OldSIDname,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN ICC.Oldisin WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN SDCHG.Oldsedol WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN ICC.OldUscode ELSE '' END as OldSID,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN 'ISIN' WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN '/GB/' WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN '/US/' ELSE '' END as newSIDname,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN ICC.newisin WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN SDCHG.newsedol WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN ICC.newUscode ELSE '' END as NewSID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_ISO_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_ISO_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_SD.OldParValue is not null and v10s_SD.NewParValue is not null and v10s_SD.OldParValue <>'' and v10s_SD.NewParValue <> '' THEN v10s_SD.OldParValue ELSE '' END as OldParValue,
v10s_SD.NewParValue,
v10s_SD.OldRatio as RatioOld,
v10s_SD.NewRatio as RatioNew,
CASE WHEN v10s_SD.Fractions is null THEN '' ELSE v10s_SD.Fractions END as Fractions
into wca2.dbo.tISO_SPLF
from v10s_SD
INNER JOIN RD ON v10s_SD.EventID = RD.RdID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'SD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'SD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_ISO_SCEXH.CntryCD = SDCHG.CntryCD AND v20c_ISO_SCEXH.RCntryCD = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_ISO_SCEXH.ExchgCD = LCC.ExchgCD
where (v10s_SD.Acttime >= @Startdate
or ICC.Acttime >= @Startdate
or SDCHG.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate)

go


print " Generating tISO_SPLR, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_SPLR')
 drop table tISO_SPLR

use wca
Select 
v10s_CONSD.EventID,
v20c_610_SCMST.SecID,
v10s_CONSD.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_CONSD.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) and (RD.Acttime > ICC.Acttime) and (RD.Acttime > SDCHG.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_CONSD.Acttime) and (EXDT.Acttime > PEXDT.Acttime) and (EXDT.Acttime > ICC.Acttime) and (EXDT.Acttime > SDCHG.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_CONSD.Acttime) and (PEXDT.Acttime > ICC.Acttime) and (PEXDT.Acttime > SDCHG.Acttime) THEN PEXDT.Acttime WHEN (ICC.Acttime is not null) and (ICC.Acttime > v10s_CONSD.Acttime) and (ICC.Acttime > SDCHG.Acttime) THEN ICC.Acttime WHEN (SDCHG.Acttime is not null) and (SDCHG.Acttime > v10s_CONSD.Acttime) THEN SDCHG.Acttime ELSE v10s_CONSD.Acttime END as [Changed],
CASE WHEN v10s_CONSD.ActFlag is null THEN '' ELSE v10s_CONSD.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN 'ISIN' WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN '/GB/' WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN '/US/' ELSE '' END as OldSIDname,
CASE WHEN (ICC.Oldisin IS NOT NULL AND ICC.Oldisin <> '') THEN ICC.Oldisin WHEN (SDCHG.Oldsedol IS NOT NULL AND SDCHG.Oldsedol <> '')THEN SDCHG.Oldsedol WHEN (ICC.OldUscode IS NOT NULL AND ICC.OldUscode <> '')THEN ICC.OldUscode ELSE '' END as OldSID,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN 'ISIN' WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN '/GB/' WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN '/US/' ELSE '' END as newSIDname,
CASE WHEN (ICC.newisin IS NOT NULL AND ICC.newisin <> '') THEN ICC.newisin WHEN (SDCHG.newsedol IS NOT NULL AND SDCHG.newsedol <> '')THEN SDCHG.newsedol WHEN (ICC.newUscode IS NOT NULL AND ICC.newUscode <> '')THEN ICC.newUscode ELSE '' END as NewSID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (SDCHG.RelEventID is null OR SDCHG.OldSEDOL = '') THEN v20c_ISO_SCEXH.Sedol ELSE SDCHG.OldSEDOL END as Sedol,
CASE WHEN (LCC.RelEventID is null OR LCC.OldLocalcode = '') THEN v20c_ISO_SCEXH.Localcode ELSE LCC.OldLocalcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN v10s_CONSD.OldParValue is not null and v10s_CONSD.NewParValue is not null and v10s_CONSD.OldParValue <>'' and v10s_CONSD.NewParValue <> '' THEN v10s_CONSD.OldParValue ELSE '' END as OldParValue,
v10s_CONSD.NewParValue,
v10s_CONSD.OldRatio as RatioOld,
v10s_CONSD.NewRatio as RatioNew,
CASE WHEN v10s_CONSD.Fractions is null THEN '' ELSE v10s_CONSD.Fractions END as Fractions
into wca2.dbo.tISO_SPLR
from v10s_CONSD
INNER JOIN RD ON v10s_CONSD.EventID = RD.RdID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'CONSD' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'CONSD' = PEXDT.EventType
LEFT OUTER JOIN ICC ON EventID = ICC.RelEventID AND SEvent = ICC.EventType
LEFT OUTER JOIN SDCHG ON EventID = SDCHG.RelEventID AND SEvent = SDCHG.EventType
       AND v20c_ISO_SCEXH.CntryCD = SDCHG.CntryCD AND v20c_ISO_SCEXH.RCntryCD = SDCHG.RcntryCD
LEFT OUTER JOIN LCC ON EventID = LCC.RelEventID AND SEvent = LCC.EventType
       AND v20c_ISO_SCEXH.ExchgCD = LCC.ExchgCD
where (v10s_CONSD.Acttime >= @Startdate
or ICC.Acttime >= @Startdate
or SDCHG.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate)
go

print " Generating tISO_TEND_PO, please wait..."
go

use wca2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog where seq = 1)
if exists (select * from sysobjects where name = 'tISO_TEND_PO')
 drop table tISO_TEND_PO

use wca
Select 
v10s_PO.EventID,
v20c_610_SCMST.SecID,
v10s_PO.AnnounceDate as Created,
CASE WHEN (RD.Acttime is not null) and (RD.Acttime > v10s_PO.Acttime) and (RD.Acttime > EXDT.Acttime) and (RD.Acttime > PEXDT.Acttime) THEN RD.Acttime WHEN (EXDT.Acttime is not null) and (EXDT.Acttime > v10s_PO.Acttime) and (EXDT.Acttime > PEXDT.Acttime) THEN EXDT.Acttime WHEN (PEXDT.Acttime is not null) and (PEXDT.Acttime > v10s_PO.Acttime) THEN PEXDT.Acttime ELSE v10s_PO.Acttime END as [Changed],
CASE WHEN v10s_PO.ActFlag is null THEN '' ELSE v10s_PO.ActFlag END as ActFlag,
v20c_610_SCMST.CntryofIncorp,
v20c_610_SCMST.IssuerName,
v20c_610_SCMST.SecurityDesc,
v20c_610_SCMST.ParValue,
v20c_610_SCMST.PVCurrency,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN 'ISIN' WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN '/GB/' WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN '/US/' ELSE '' END as SIDname,
CASE WHEN (v20c_610_SCMST.Isin IS NOT NULL AND v20c_610_SCMST.Isin <> '') THEN v20c_610_SCMST.Isin WHEN (v20c_ISO_SCEXH.Sedol IS NOT NULL AND v20c_ISO_SCEXH.Sedol <> '')THEN v20c_ISO_SCEXH.Sedol WHEN (v20c_610_SCMST.Uscode IS NOT NULL AND v20c_610_SCMST.Uscode <> '')THEN v20c_610_SCMST.Uscode ELSE '' END as SID,
v20c_610_SCMST.SecStatus,
v20c_ISO_SCEXH.ExchgCD,
CASE WHEN (v20c_ISO_SCEXH.MIC IS NOT NULL AND v20c_ISO_SCEXH.MIC <> '') THEN v20c_ISO_SCEXH.MIC WHEN (v20c_ISO_SCEXH.ExchgCD IS NOT NULL AND v20c_ISO_SCEXH.ExchgCD <> '') THEN v20c_ISO_SCEXH.ExchgCD ELSE '' END as MCD,
v20c_ISO_SCEXH.CntryCD,
v20c_ISO_SCEXH.RCntryCD,
CASE WHEN (v20c_ISO_SCEXH.Sedol IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Sedol END as Sedol,
CASE WHEN (v20c_ISO_SCEXH.Localcode IS NULL) THEN '' ELSE v20c_ISO_SCEXH.Localcode END as Localcode,
v20c_ISO_SCEXH.ListStatus,
v20c_ISO_SCEXH.Seqnum,
v20c_ISO_SCEXH.EXCHGID,
RD.Recdate,
RD.RDID,
CASE WHEN EXDT.ExDate is not null THEN EXDT.ExDate ELSE PEXDT.ExDate END as ExDate,
CASE WHEN EXDT.PayDate is not null THEN EXDT.PayDate ELSE PEXDT.PayDate END as PayDate,
CASE WHEN (v10s_PO.CurenCD IS NULL) THEN '' ELSE v10s_PO.CurenCD END as CurenCD,
CASE WHEN (v10s_PO.NegotiatedPrice IS NULL) THEN '' ELSE v10s_PO.NegotiatedPrice END as NegotiatedPrice,
CASE WHEN (v10s_PO.OfferOpens IS NULL) THEN '' ELSE v10s_PO.OfferOpens END as OfferOpens,
CASE WHEN (v10s_PO.OfferCloses IS NULL) THEN '' ELSE v10s_PO.OfferCloses END as OfferCloses,
CASE WHEN (v10s_PO.POMinPercent IS NULL) THEN '' ELSE v10s_PO.POMinPercent END as POMinPercent,
CASE WHEN (v10s_PO.POMaxPercent IS NULL) THEN '' ELSE v10s_PO.POMaxPercent END as POMaxPercent,
CASE WHEN (v10s_PO.MinOfrQty IS NULL) THEN '' ELSE v10s_PO.MinOfrQty END as MinOfrQty,
CASE WHEN (v10s_PO.MaxOfrqty IS NULL) THEN '' ELSE v10s_PO.MaxOfrqty END as MaxOfrqty,
CASE WHEN (v10s_PO.TndrStrkPrice IS NULL) THEN '' ELSE v10s_PO.TndrStrkPrice END as TndrStrkPrice,
CASE WHEN (v10s_PO.TndrPriceStep IS NULL) THEN '' ELSE v10s_PO.TndrPriceStep END as TndrPriceStep,
CASE WHEN (v10s_PO.MinPrice IS NULL) THEN '' ELSE v10s_PO.MinPrice END as MinPrice,
CASE WHEN (v10s_PO.MaxPrice IS NULL) THEN '' ELSE v10s_PO.MaxPrice END as MaxPrice,
CASE WHEN (v10s_PO.MinQlyQty IS NULL) THEN '' ELSE v10s_PO.MinQlyQty END as MinQlyQty,
CASE WHEN (v10s_PO.MaxQlyQty IS NULL) THEN '' ELSE v10s_PO.MaxQlyQty END as MaxQlyQty,
CASE WHEN (v10s_PO.MinAcpQty IS NULL) THEN '' ELSE v10s_PO.MinAcpQty END as MinAcpQty,
CASE WHEN (v10s_PO.MaxAcpQty IS NULL) THEN '' ELSE v10s_PO.MaxAcpQty END as MaxAcpQty,
CASE WHEN (v10s_PO.SealedBid IS NULL) THEN '' ELSE v10s_PO.SealedBid END as SealedBid
into wca2.dbo.tISO_TEND_PO
from v10s_PO
INNER JOIN RD ON RD.RdID = v10s_PO.EventID
INNER JOIN v20c_610_SCMST ON RD.SecID = v20c_610_SCMST.SecID
INNER JOIN v20c_ISO_SCEXH ON v20c_610_SCMST.SecID = v20c_ISO_SCEXH.SecID
LEFT OUTER JOIN EXDT ON RD.RdID = EXDT.RdID AND v20c_ISO_SCEXH.ExchgCD = EXDT.ExchgCD AND 'PO' = EXDT.EventType
LEFT OUTER JOIN EXDT as PEXDT ON RD.RdID = PEXDT.RdID AND v20c_610_SCMST.PrimaryExchgCD = PEXDT.ExchgCD AND 'PO' = PEXDT.EventType
where
(v10s_PO.Acttime >= @Startdate
or EXDT.Acttime >= @Startdate
or PEXDT.Acttime >= @Startdate
or RD.Acttime >= @Startdate)
