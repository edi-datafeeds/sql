
print " Generating evi_Shares_Outstanding_Change, please wait..."
go

use wca
use WCA2 Declare @StartDate datetime
set @StartDate = (select max(acttime) from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'evi_Shares_Outstanding_Change')
 drop table evi_Shares_Outstanding_Change
use wca
select 
CASE WHEN v10s_SHOCH.EventID is not null 
THEN cast(cast(v20c_EV_scexh.Seqnum as char(1))+v20c_EV_SCEXH.EXCHGID+rtrim(cast(v10s_SHOCH.EventID as char(16)))as bigint)
else cast(cast(v20c_EV_scexh.Seqnum as char(1))+'9999'+rtrim(cast(v20c_EV_SCMST.SecID as char(16)))as bigint)
END as CAref,
'Shares_Outstanding_Change' as Levent,
CASE WHEN v10s_SHOCH.EventID is not null THEN v10s_SHOCH.EventID ELSE v20c_EV_SCMST.SecID END as EventID,
CASE WHEN v10s_SHOCH.EventID is not null THEN v10s_SHOCH.AnnounceDate ELSE SCMST.AnnounceDate END as Created,
CASE WHEN v10s_SHOCH.EventID is not null THEN v10s_SHOCH.Acttime ELSE SCMST.Acttime END as Changed,
CASE WHEN v10s_SHOCH.EventID is not null THEN v10s_SHOCH.ActFlag ELSE SCMST.ActFlag END as ActFlag,
v20c_EV_SCMST.CntryofIncorp,
v20c_EV_SCMST.IssuerName,
v20c_EV_SCMST.SecurityDesc,
v20c_EV_SCMST.Parvalue,
v20c_EV_SCMST.PVCurrency,
v20c_EV_SCMST.ISIN,
v20c_EV_scexh.Sedol,
v20c_EV_SCMST.USCode,
v20c_EV_SCMST.StatusFlag,
v20c_EV_SCMST.SectyCD,
SectyGrp.SecGrpID,
CASE WHEN (v20c_EV_scexh.ExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD IS NULL OR v20c_EV_SCMST.PrimaryExchgCD='') THEN '' WHEN (v20c_EV_SCMST.PrimaryExchgCD=v20c_EV_scexh.ExchgCD) THEN 'Yes' ELSE 'No' END as PrimaryEx,
v20c_EV_scexh.ExchgCD,
v20c_EV_scexh.MIC,
v20c_EV_scexh.ExCountry,
v20c_EV_scexh.RegCountry,
v20c_EV_scexh.Localcode,
v20c_EV_scexh.ListStatus,
v20c_EV_scexh.Listdate,
v10s_SHOCH.EffectiveDate,
CASE WHEN v10s_SHOCH.EventID is not null THEN v10s_SHOCH.OldSos ELSE null END as OldSharesoutstanding,
CASE WHEN v10s_SHOCH.EventID is not null THEN v10s_SHOCH.NewSos ELSE SCMST.Sharesoutstanding END as NewSharesOutstanding,
'M' as Choice,
v10s_SHOCH.SHOCHNotes as Notes,
v20c_EV_SCMST.IssID,
v20c_EV_SCMST.SecID
into WCA2.dbo.evi_Shares_Outstanding_Change
FROM v20c_EV_SCMST
LEFT OUTER JOIN v10s_SHOCH ON v20c_EV_SCMST.SecID = v10s_SHOCH.SecID
inner JOIN scmst ON v20c_EV_SCMST.SecID = scmst.SecID
INNER JOIN v20c_EV_SCEXH ON v20c_EV_SCMST.SecID = v20c_EV_SCEXH.SecID
LEFT OUTER JOIN SectyGrp ON v20c_EV_SCMST.SectyCD = SectyGrp.SectyCD
where
(v10s_SHOCH.Acttime BETWEEN @Startdate AND  '2099/01/01' or v10s_SHOCH.acttime is null)
and scmst.sharesoutstanding <> ''
and scmst.actflag<>'Z'
go


print ""
go

print " Generating evi_Shares_Outstanding_Change ,please  wait ....."
GO 
use WCA2 
ALTER TABLE evi_Shares_Outstanding_Change ALTER COLUMN  caref bigint NOT NULL
GO 
use WCA2 
ALTER TABLE [DBO].[evi_Shares_Outstanding_Change] WITH NOCHECK ADD 
 CONSTRAINT [pk_caref_evi_Shares_Outstanding_Change] PRIMARY KEY ([caref])  ON [PRIMARY]
GO 
use WCA2 
--CREATE  INDEX [ix_secid_evi_Shares_Outstanding_Change] ON [dbo].[evi_Shares_Outstanding_Change]([secid]) ON [PRIMARY] 
GO 
use WCA2 
--CREATE  INDEX [ix_issid_evi_Shares_Outstanding_Change] ON [dbo].[evi_Shares_Outstanding_Change]([issid]) ON [PRIMARY]
GO
use WCA2 
--CREATE  INDEX [ix_sedol_evi_Shares_Outstanding_Change] ON [dbo].[evi_Shares_Outstanding_Change]([sedol]) ON [PRIMARY]
GO
use WCA2 
--CREATE  INDEX [ix_isin_evi_Shares_Outstanding_Change] ON [dbo].[evi_Shares_Outstanding_Change]([isin]) ON [PRIMARY]
GO

