use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
if exists (select * from sysobjects where name = 'tSCXi_Dividend')
	delete from tSCXi_Dividend
use wca2
insert into tSCXi_Dividend
select * FROM wca.dbo.v54f_SCX_Dividend
where changed >= @Startdate
or (changed > '2019/06/06 17:00:00' and changed < '2019/06/06 18:00:00')
or (changed > '2019/06/10 12:00:00' and changed < '2019/06/10 22:00:00')
or (changed > '2019/06/12 02:00:00' and changed < '2019/06/12 12:00:00')
or (changed > '2019/06/18 12:00:00' and changed < '2019/06/18 22:00:00')
go
use wca2
Declare @StartDate datetime
set @StartDate = (select max(acttime)-0.05 from wca.dbo.tbl_Opslog)
insert into tSCXi_Dividend
select * FROM wca.dbo.v54f_SCX_Interest_Dividend
where changed >= @Startdate
or (changed > '2019/06/06 17:00:00' and changed < '2019/06/06 18:00:00')
or (changed > '2019/06/10 12:00:00' and changed < '2019/06/10 22:00:00')
or (changed > '2019/06/12 02:00:00' and changed < '2019/06/12 12:00:00')
or (changed > '2019/06/18 12:00:00' and changed < '2019/06/18 22:00:00')
go