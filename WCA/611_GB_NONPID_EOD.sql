--filepath=o:\Datafeed\wca\611_GB_PID\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.v54f_610_Dividend_GB_PID.changed),112) from wca2.dbo.v54f_610_Dividend_GB_PID
--fileextension=.611
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\611_GB_PID\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
USE WCA
SELECT * 
FROM v54f_611_Dividend_GB_PID
WHERE
changed>(select max(feeddate)-100 from wca.dbo.tbl_opslog where seq=3)
and (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)

SELECT * 
FROM v54f_611_Dividend_GB_NONPID
WHERE
changed>(select max(feeddate)-100 from wca.dbo.tbl_opslog where seq=3)
and (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol