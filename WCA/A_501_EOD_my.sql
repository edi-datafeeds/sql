--filepath=o:\Datafeed\WCA\501_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.501
--suffix=
--fileheadertext=records = NNNNNNN EXCHANGE ISSUER DATA
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\501_EOD\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n




--# 1
SELECT 
wca.ISSUR.IssID,
DATE_FORMAT(wca.ISSUR.Acttime , '%d/%m/%Y %h:%i:%s') AS ISSURtime,
ISSUR.Actflag,  
'' as exchcode,
wca.ISSUR.Issuername,
'' as ExCountry,
wca.ISSUR.IndusID as Induscode,
wca.ISSUR.AnnounceDate as Creation,
'' as trailingTAB

FROM wca.issur

where wca.issur.acttime >= (select max(feeddate) from wca.tbl_Opslog where seq = 3)