--filepath=o:\Datafeed\WCA\evi_eq\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.evi
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Bespoke\FinInf\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq from wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n

--# 1
USE WCA2
SELECT * 
FROM evi_Company_Meeting_nosedol
where
 caref is not null and caref <> '' and len(caref)<20

--# 2
USE WCA2
SELECT *
FROM evi_Call_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 3
USE WCA2
SELECT * 
FROM evi_Liquidation_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 4
USE WCA2
SELECT *
FROM evi_Certificate_Exchange_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 5
USE WCA2
SELECT * 
FROM evi_International_Code_Change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 6
USE WCA2
SELECT * 
FROM evi_Preference_Conversion_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 7
USE WCA2
SELECT * 
FROM evi_Preference_Redemption_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 8
USE WCA2
SELECT * 
FROM evi_Security_Reclassification_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 9
USE WCA2
SELECT * 
FROM evi_Lot_Change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 11
USE WCA2
SELECT * 
FROM evi_Buy_Back_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 12
USE WCA2
SELECT * 
FROM evi_Capital_Reduction_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 14
USE WCA2
SELECT * 
FROM evi_Takeover_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 15
USE WCA2
SELECT * 
FROM evi_Arrangement_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 16
USE WCA2
SELECT * 
FROM evi_Bonus_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 17
USE WCA2
SELECT * 
FROM evi_Bonus_Rights_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 18
USE WCA2
SELECT * 
FROM evi_Consolidation_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 19
USE WCA2
SELECT * 
FROM evi_Demerger_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 20
USE WCA2
SELECT * 
FROM evi_Distribution_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 21
USE WCA2
SELECT * 
FROM evi_Divestment_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 22
USE WCA2
SELECT * 
FROM evi_Entitlement_nosedol
where
 caref is not null and caref <> '' and len(caref)<20

--# 23
USE WCA2
SELECT * 
FROM evi_Merger_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 24
USE WCA2
SELECT * 
FROM evi_Preferential_Offer_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 25
USE WCA2
SELECT * 
FROM evi_Purchase_Offer_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 26
USE WCA2
SELECT * 
FROM evi_Rights_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 27
USE WCA2
SELECT * 
FROM evi_Security_Swap_nosedol
where
 caref is not null and caref <> '' and len(caref)<20

--# 28
USE WCA2
SELECT *
FROM evi_Subdivision_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 29
USE WCA2
SELECT *
FROM evi_Bankruptcy_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 30
USE WCA2
SELECT *
FROM evi_Financial_Year_Change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 31
USE WCA2
SELECT *
FROM evi_Incorporation_Change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 32
USE WCA2
SELECT *
FROM evi_Issuer_Name_change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 33
USE WCA2
SELECT *
FROM evi_Class_Action_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 34
USE WCA2
SELECT *
FROM evi_Security_Description_Change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 35
USE WCA2
SELECT *
FROM evi_Assimilation_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 36
USE WCA2
SELECT *
FROM evi_Listing_Status_Change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 37
USE WCA2
SELECT *
FROM evi_Local_Code_Change_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 38
USE WCA2
SELECT * 
FROM evi_New_Listing_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 39
USE WCA2
SELECT * 
FROM evi_Announcement_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 40
USE WCA2
SELECT * 
FROM evi_Parvalue_Redenomination_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 41
USE WCA2
SELECT * 
FROM evi_Currency_Redenomination_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 42
USE WCA2
SELECT * 
FROM evi_Return_of_Capital_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 43
USE WCA2
SELECT * 
FROM evi_Dividend_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 44
USE WCA2
SELECT * 
FROM evi_Dividend_Reinvestment_Plan_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
--# 45
USE WCA2
SELECT * 
FROM evi_Franking_nosedol
where
 caref is not null and caref <> '' and len(caref)<20
