--filepath=o:\Datafeed\WCA\evi_eq\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.evi
--suffix=
--fileheadertext=
--fileheaderdate=
--datadateformat=yyyy/mm/dd hh:mm:ss
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Bespoke\FinInf\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq from wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n

--# 1
USE WCA2
SELECT * 
FROM evi_Company_Meeting 
where
 caref is not null and caref <> '' and len(caref)<20

--# 2
USE WCA2
SELECT *
FROM evi_Call
where
 caref is not null and caref <> '' and len(caref)<20
--# 3
USE WCA2
SELECT * 
FROM evi_Liquidation
where
 caref is not null and caref <> '' and len(caref)<20
--# 4
USE WCA2
SELECT *
FROM evi_Certificate_Exchange
where
 caref is not null and caref <> '' and len(caref)<20
--# 5
USE WCA2
SELECT * 
FROM evi_International_Code_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 6
USE WCA2
SELECT * 
FROM evi_Preference_Conversion
where
 caref is not null and caref <> '' and len(caref)<20
--# 7
USE WCA2
SELECT * 
FROM evi_Preference_Redemption
where
 caref is not null and caref <> '' and len(caref)<20
--# 8
USE WCA2
SELECT * 
FROM evi_Security_Reclassification
where
 caref is not null and caref <> '' and len(caref)<20
--# 9
USE WCA2
SELECT * 
FROM evi_Lot_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 10
USE WCA2
SELECT * 
FROM evi_Sedol_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 11
USE WCA2
SELECT * 
FROM evi_Buy_Back
where
 caref is not null and caref <> '' and len(caref)<20
--# 12
USE WCA2
SELECT * 
FROM evi_Capital_Reduction
where
 caref is not null and caref <> '' and len(caref)<20
--# 14
USE WCA2
SELECT * 
FROM evi_Takeover
where
 caref is not null and caref <> '' and len(caref)<20
--# 15
USE WCA2
SELECT * 
FROM evi_Arrangement
where
 caref is not null and caref <> '' and len(caref)<20
--# 16
USE WCA2
SELECT * 
FROM evi_Bonus
where
 caref is not null and caref <> '' and len(caref)<20
--# 17
USE WCA2
SELECT * 
FROM evi_Bonus_Rights
where
 caref is not null and caref <> '' and len(caref)<20
--# 18
USE WCA2
SELECT * 
FROM evi_Consolidation
where
 caref is not null and caref <> '' and len(caref)<20
--# 19
USE WCA2
SELECT * 
FROM evi_Demerger
where
 caref is not null and caref <> '' and len(caref)<20
--# 20
USE WCA2
SELECT * 
FROM evi_Distribution
where
 caref is not null and caref <> '' and len(caref)<20
--# 21
USE WCA2
SELECT * 
FROM evi_Divestment
where
 caref is not null and caref <> '' and len(caref)<20
--# 22
USE WCA2
SELECT * 
FROM evi_Entitlement
where
 caref is not null and caref <> '' and len(caref)<20

--# 23
USE WCA2
SELECT * 
FROM evi_Merger
where
 caref is not null and caref <> '' and len(caref)<20
--# 24
USE WCA2
SELECT * 
FROM evi_Preferential_Offer
where
 caref is not null and caref <> '' and len(caref)<20
--# 25
USE WCA2
SELECT * 
FROM evi_Purchase_Offer
where
 caref is not null and caref <> '' and len(caref)<20
--# 26
USE WCA2
SELECT * 
FROM evi_Rights 
where
 caref is not null and caref <> '' and len(caref)<20
--# 27
USE WCA2
SELECT * 
FROM evi_Security_Swap 
where
 caref is not null and caref <> '' and len(caref)<20

--# 28
USE WCA2
SELECT *
FROM evi_Subdivision
where
 caref is not null and caref <> '' and len(caref)<20
--# 29
USE WCA2
SELECT *
FROM evi_Bankruptcy 
where
 caref is not null and caref <> '' and len(caref)<20
--# 30
USE WCA2
SELECT *
FROM evi_Financial_Year_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 31
USE WCA2
SELECT *
FROM evi_Incorporation_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 32
USE WCA2
SELECT *
FROM evi_Issuer_Name_change
where
 caref is not null and caref <> '' and len(caref)<20
--# 33
USE WCA2
SELECT *
FROM evi_Class_Action
where
 caref is not null and caref <> '' and len(caref)<20
--# 34
USE WCA2
SELECT *
FROM evi_Security_Description_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 35
USE WCA2
SELECT *
FROM evi_Assimilation
where
 caref is not null and caref <> '' and len(caref)<20
--# 36
USE WCA2
SELECT *
FROM evi_Listing_Status_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 37
USE WCA2
SELECT *
FROM evi_Local_Code_Change
where
 caref is not null and caref <> '' and len(caref)<20
--# 38
USE WCA2
SELECT * 
FROM evi_New_Listing
where
 caref is not null and caref <> '' and len(caref)<20
--# 39
USE WCA2
SELECT * 
FROM evi_Announcement
where
 caref is not null and caref <> '' and len(caref)<20
--# 40
USE WCA2
SELECT * 
FROM evi_Parvalue_Redenomination 
where
 caref is not null and caref <> '' and len(caref)<20
--# 41
USE WCA2
SELECT * 
FROM evi_Currency_Redenomination 
where
 caref is not null and caref <> '' and len(caref)<20
--# 42
USE WCA2
SELECT * 
FROM evi_Return_of_Capital 
where
 caref is not null and caref <> '' and len(caref)<20
--# 43
USE WCA2
SELECT * 
FROM evi_Dividend
where
 caref is not null and caref <> '' and len(caref)<20
--# 44
USE WCA2
SELECT * 
FROM evi_Dividend_Reinvestment_Plan
where
 caref is not null and caref <> '' and len(caref)<20
--# 45
USE WCA2
SELECT * 
FROM evi_Franking
where
 caref is not null and caref <> '' and len(caref)<20
