--filepath=o:\Datafeed\WCA\505_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.505
--suffix=
--fileheadertext=Records = NNNNNN World Dividend Data
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\505_EOD\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
USE WCA

SELECT 
Scexhid as SecID,
EventId as Divseqnum,
'1' as Divelement,
convert(varchar, Changed,103)+' '+CONVERT( varchar , Changed,8) as Changed,
case when nildividend = 'T' and nildividend is not null then 'O'
     else DIVPYActflag 
     end as actflag,
ExCountry as Cntycode,
ExchgCD as Exchcode,
case when DivPeriodCD = 'SPL' then 'Special'
     when Divtype = 'C' then 'Cash'
     when Divtype = 'S' then 'Stock'
     else 'Cash'
     end as Divtype,
case when DivPeriodCD = 'SPL' then 'Special'
     when DivPeriodCD = 'ANL' then 'Annual'
     when DivPeriodCD = 'SMA' then 'Semi-Annual'
     when DivPeriodCD = 'QTR' then 'Quarterly'
     when DivPeriodCD = 'MNT' then 'Monthly'
     when DivPeriodCD = 'QTR' then 'Quarterly'
     else 'Unknown'
     end as DivPeriod,
case when DivPeriodCD = 'FNL' then 'Final'
     when DivPeriodCD = 'ANL' then 'Final'
     when DivPeriodCD = 'SPL' then 'N/A'
     else 'Interim'
     end as Finalflag,
case when divtype = 'C' then CurenCD
     when divtype_2 = 'C' then CurenCD_2
else '' end as Currcode,
'No' as Foreignflag,
case when divtype = 'C' then Taxrate
     when divtype_2 = 'C' then Taxrate_2
else '' end as Taxrate,
Exdate,
'No' as Exflag,
Recdate,
'No' as Recflag,
Paydate,
'No' as Payflag,
case when Tbaflag = 'T' then 'Yes'
     else 'No'
     end as Tbaflag,
case when divtype = 'C' then NetDividend
     when divtype_2 = 'C' then NetDividend_2
else '' end as Netrate,
case when divtype = 'C' then GrossDividend
     when divtype_2 = 'C' then GrossDividend_2
else '' end as Grossrate,
'' as Stockrate,
case when divtype = 'S' then RatioNew
     when divtype_2 = 'S' then RatioNew_2
else '' end as Numerator,
case when divtype = 'S' then RatioOld
     when divtype_2 = 'S' then RatioOld_2
else '' end as Denominator,
case when Approxflag = 'T' then 'Yes'
     else 'No'
     end as Approxflag,
Created as InfoDate,
Created as CreationDate,
'' as Notes,
case when divtype = 'C' and divtype_2 = 'S' then 'Yes'
     when divtype = 'S' and divtype_2 = 'C' then 'Yes'
     else 'No' end as Optionflag,
'' as Other1
from v54f_618_FlatDividend

WHERE changed >= (select max(feeddate) from tbl_Opslog where seq = 3)
and ((divtype = 'C' and curencd <> '' and curencd is not null) OR (divtype_2 = 'C' and curencd_2 <> '' and curencd_2 is not null) or (divtype = 'S' and divtype_2 <> 'C'))
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
and scexhid is not null
