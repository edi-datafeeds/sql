--filepath=o:\upload\acc\157\feed\
--filenameprefix=Global_
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_GLOBAL_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\157\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 5
USE WCA2
SELECT * 
FROM t610_International_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 




--# 10
USE WCA2
SELECT * 
FROM t610_Sedol_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 




--# 35
USE WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 




--# 36
USE WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
and (SecStatus <> 'I' OR Secstatus IS NULL) 



--# 37
USE WCA2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'I' OR Secstatus IS NULL) 


