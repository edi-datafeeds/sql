--filepath=o:\datafeed\wca\600_full\
--filenameprefix=
--filename=ALYYMMDD
--filenamealt=
--fileextension=.600
--suffix=
--fileheadertext=EDI_WCA_600_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\600\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n






--# 1
use WCA
SELECT 
issur.Issuername,
issur.CntryofIncorp,
scmst.IssID,
scmst.SecID,
scmst.Statusflag,
scmst.PrimaryExchgCD,
scmst.Securitydesc,
scmst.CurenCD as ParValueCurrency,
scmst.Parvalue,
scmst.SectyCD,
scmst.Uscode,
scmst.Isin,
Sedol.Sedol,
Sedol.Defunct,
scexh.exchgcd,
scexh.localcode,
scexh.liststatus,
cntry.curenCD as DomicileCurrency
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN cntry ON issur.CntryofIncorp = cntry.cntryCD
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                      AND exchg.cntryCD = Sedol.CntryCD
where scmst.statusflag <> 'I'
and scmst.actflag <> 'D'
and scexh.actflag<>'D'

scmst.sectycd<>'CW'
and (scexh.acttime>=(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 1)
or scmst.acttime>=(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 1)
or issur.acttime>=(select max(wca.dbo.tbl_opslog.Feeddate) from wca.dbo.tbl_opslog where seq = 1))
