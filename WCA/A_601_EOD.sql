--filepath=o:\datafeed\wca\601\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.601
--suffix=
--fileheadertext=EDI_WCA_601_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\601\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n





use WCA



--# 1
USE WCA
SELECT 
issur.Issuername,
scmst.Statusflag,
scmst.Securitydesc,
scmst.Isin,
scexh.exchgcd,
scexh.localcode,
scexh.liststatus,
scexh.actflag,
scexh.acttime,
scexh.source


FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID

WHERE scmst.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or scexh.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
or issur.acttime>= (select max(acttime) from tbl_Opslog where seq = 1)
