use wca
if exists (select * from sysobjects where name = 'figiseq')
	drop table figiseq
go
select distinct
BBE.SecID,
BBE.ExchgCD,
BBE.CurenCD,
BBE.BbgExhID,
BBE.BbeID,
1 as Seqnum,
BBE.BbgExhTk,
BBE.Acttime
into figiseq
from BBE
where 
BBE.actflag<>'D'
and BBE.ExchgCD<>''
and BBE.BbgExhID<>''
go

USE WCA
ALTER TABLE figiseq ALTER COLUMN BbeID int NOT NULL
go
USE WCA
ALTER TABLE figiseq ALTER COLUMN SecID int NOT NULL
go
USE WCA
ALTER TABLE figiseq ALTER COLUMN ExchgCD char(6) NOT NULL
go
USE WCA
ALTER TABLE figiseq ALTER COLUMN CurenCD char(3) NOT NULL
go

ALTER TABLE [DBO].[figiseq] WITH NOCHECK ADD
	
	CONSTRAINT [PK_figiseq] PRIMARY KEY 
	
	(
		[BbeID]
	)  ON [PRIMARY] 

go 

use WCA
CREATE  INDEX [ix_figiseq] ON [dbo].[figiseq]([SecID],[ExchgCD],[BbeID] ) ON [PRIMARY] 
go

EXEC sp_figiseq

go
