--filepath=o:\datafeed\wca\621_dup\
--filenameprefix=EDI_
--filename=621
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_621_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=;
--outputstyle=
--archive=y
--archivepath=O:\WCA\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select distinct
scmst.issid,
issuername,
securitydesc,
scmst.isin,
sedol.sedol,
case when sectyCD = 'EQS' then '1 Equity'
     when sectyCD = 'PRF' then '2 Preference'
     when sectyCD = 'DR' then '3 Depositary Receipt'
     when sectyCD = 'BND' then '4 Debt'
else '5 Other'
end as SecurityType,
case when primaryexchgcd = 'GBLSE' then smf4.dbo.market.TIDM
else pscexh.localcode end as PrimaryLocalcode,
smf4.dbo.market.TIDM as LSETicker,
smf4.dbo.Security.Longdesc as LSESecurityDesc
from scmst
left outer join scexh on scmst.secid = scexh.secid
left outer join scexh as pscexh on scmst.primaryexchgcd = pscexh.exchgcd
                      and scmst.secid = pscexh.secid
left outer join sedol on scmst.secid = sedol.secid
left outer join issur on scmst.issid = issur.issid
left outer join smf4.dbo.security on sedol.sedol = smf4.dbo.security.sedol
left outer join smf4.dbo.market on smf4.dbo.security.securityid = smf4.dbo.market.securityid
                           and smf4.dbo.security.opol = smf4.dbo.market.mic
where (opol = 'AIMX' or opol = 'XLON')
and (scexh.exchgcd = 'GBLSE' or scexh.exchgcd is null)
and (scexh.actflag <> 'D' or scexh.actflag is null)
and scexh.liststatus <> 'D'
and scmst.actflag <> 'D'
and (scmst.statusflag <> 'I' or scmst.statusflag is null)
and scmst.actflag <> 'D'
and smf4.dbo.market.tidm is not null
and (smf4.dbo.market.statusflag <> 'D' and smf4.dbo.market.statusflag is not null)
order by scmst.issid, securitytype, primarylocalcode desc;

