--filepath=o:\prodman\salesfeeds\wca\620\sampledata\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.620
--suffix=_2
--fileheadertext=EDI_REORG_620_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

/* DATE Date formating */
/* DataDateFormat is the date format for ONLY the date part of a date field */
/* DataTimeFormat is the Time format for ONLY the Time part of a date field */
/* If you want NO TIMES in the outputed feed then leave DateTimeForm blank */
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 27
use wca
SELECT * 
FROM v54f_620_Subdivision
WHERE (CHANGED>= '2014-01-23' and CHANGED<'2014-07-29')
ORDER BY EventID desc, ExchgCD, Sedol

--# 25
use wca
SELECT *  
FROM v54f_620_Rights 
WHERE (CHANGED>= '2014-01-23' and CHANGED<'2014-07-29')
ORDER BY EventID desc, ExchgCD, Sedol

--# 15
use wca
SELECT *  
FROM v54f_620_Bonus
WHERE (CHANGED>= '2014-01-23' and CHANGED<'2014-07-29')
ORDER BY EventID desc, ExchgCD, Sedol

--# 21
use wca
SELECT *  
FROM v54f_620_Entitlement
WHERE (CHANGED>= '2014-01-23' and CHANGED<'2014-07-29')
ORDER BY EventID desc, ExchgCD, Sedol

--# 17
use wca
SELECT *  
FROM v54f_620_Consolidation
WHERE (CHANGED>= '2014-01-23' and CHANGED<'2014-07-29')
ORDER BY EventID desc, ExchgCD, Sedol


