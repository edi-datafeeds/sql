--FilePath=o:\datafeed\wca\618i_Not_US_CA\
--filename=yyyymmdd
--filenamealt=
--fileextension=.618
--suffix=
--FileHeaderTEXT=EDI_FLATDIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\618i_Not_US_CA\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog where seq=2 order by acttime desc
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime


--# 1
USE WCA
SELECT v54f_618_FlatDivFidNew.* from v54f_618_FlatDivFidNew
WHERE CHANGED > '2014/05/01 12:00:00' and CHANGED < '2014/05/01 17:00:00'
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
AND ExCountry <> 'US'
AND ExCountry <> 'CA'
and sedol is not null
and sedol <> ''
and (paydate>'2010/04/01' or paydate is null)
and (Excountry <> 'JP' or GrossDividend<>'' or NetDividend<>'')
and (Excountry <> 'JP' or optionid is not null)

