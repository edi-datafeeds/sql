--filepath=o:\Datafeed\wca\602\hist\
--filenameprefix=
--filename=hist
--filenamealt=
--fileextension=.602
--suffix=
--fileheadertext=EDI_STATIC_602_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=n
--archivepath=n:\wca\602\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT 
issur.Issuername,
issur.CntryofIncorp,
scmst.IssID,
scmst.SecID,
scmst.Statusflag,
scmst.PrimaryExchgCD,
scmst.Securitydesc,
scmst.CurenCD as ParValueCurrency,
scmst.Parvalue,
scmst.SectyCD,
scmst.Uscode,
scmst.Isin,
scmst.StructCD,
Sedol.Sedol,
Sedol.RCntryCD as RegCountry,
Sedol.Defunct,
scexh.exchgcd,
scexh.localcode,
scexh.liststatus,
scexh.Lot,
scexh.MinTrdgQty
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN sectygrp ON scmst.sectycd = sectygrp.sectycd
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                      AND exchg.cntryCD = Sedol.CntryCD
where
issur.actflag <> 'D' 
AND scmst.actflag <> 'D'
AND sedol.actflag <> 'D'
AND sectygrp.secgrpid <3