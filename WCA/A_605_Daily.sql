--filepath=o:\datafeed\wca\605\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.605
--suffix=
--fileheadertext=
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\605\
--fieldheaders=
--filetidy=y
--fwoffsets=
--sevent=n
--shownulls=n



--# 1
use WCA
SELECT 
issur.Issuername,
scmst.Statusflag,
scmst.Isin,
Sedol.Sedol

FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID

where (scmst.acttime>= (select max(acttime) from tbl_Opslog where seq = 3)
or sedol.acttime>= (select max(acttime) from tbl_Opslog where seq = 3)
or issur.acttime>= (select max(acttime) from tbl_Opslog where seq = 3))
and scmst.isin <> ''
and sedol.sedol <> ''
and sedol.defunct = 'F'
and scmst.isin is not null
and sedol.sedol is not null
