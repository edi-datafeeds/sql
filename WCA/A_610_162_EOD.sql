--filepath=o:\upload\acc\162\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.610
--suffix=
--fileheadertext=EDI_REORG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\162\feed\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
USE WCA2
SELECT * 
FROM t610_Company_Meeting 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 2
USE WCA2
SELECT *
FROM t610_Call
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 3
USE WCA2
SELECT * 
FROM t610_Liquidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 4
USE WCA2
SELECT *
FROM t610_Certificate_Exchange
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 5
USE WCA2
SELECT * 
FROM t610_International_Code_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 6
USE WCA2
SELECT * 
FROM t610_Preference_Conversion
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 7
USE WCA2
SELECT * 
FROM t610_Preference_Redemption
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 8
USE WCA2
SELECT * 
FROM t610_Security_Reclassification
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 9
USE WCA2
SELECT * 
FROM t610_Lot_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 10
USE WCA2
SELECT * 
FROM t610_Sedol_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 11
USE WCA2
SELECT * 
FROM t610_Buy_Back
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 12
USE WCA2
SELECT * 
FROM t610_Capital_Reduction
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 13
USE WCA2
SELECT * 
FROM t610_Takeover
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 14
USE WCA2
SELECT * 
FROM t610_Arrangement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 15
USE WCA2
SELECT * 
FROM t610_Bonus
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 16
USE WCA2
SELECT * 
FROM t610_Bonus_Rights
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 17
USE WCA2
SELECT * 
FROM t610_Consolidation
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 18
USE WCA2
SELECT * 
FROM t610_Demerger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 19
USE WCA2
SELECT * 
FROM t610_Distribution
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 20
USE WCA2
SELECT * 
FROM t610_Divestment
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 21
USE WCA2
SELECT * 
FROM t610_Entitlement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 22
USE WCA2
SELECT * 
FROM t610_Merger
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol, OptionID, SerialID

--# 23
USE WCA2
SELECT * 
FROM t610_Preferential_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 24
USE WCA2
SELECT * 
FROM t610_Purchase_Offer
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 25
USE WCA2
SELECT * 
FROM t610_Rights 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 26
USE WCA2
SELECT * 
FROM t610_Security_Swap 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 27
USE WCA2
SELECT *
FROM t610_Subdivision
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 28
USE WCA2
SELECT *
FROM t610_Bankruptcy 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 29
USE WCA2
SELECT *
FROM t610_Financial_Year_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 30
USE WCA2
SELECT *
FROM t610_Incorporation_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 31
USE WCA2
SELECT *
FROM t610_Issuer_Name_change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 32
USE WCA2
SELECT *
FROM t610_Lawsuit
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 33
USE WCA2
SELECT *
FROM t610_Security_Description_Change
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 34
USE WCA2
SELECT *
FROM t610_Assimilation
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 35
USE WCA2
SELECT *
FROM t610_Listing_Status_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 36
USE WCA2
SELECT *
FROM t610_Local_Code_Change
WHERE
(RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 37
USE WCA2
SELECT * 
FROM t610_New_Listing
WHERE
(SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 38
USE WCA2
SELECT * 
FROM t610_Announcement
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 39
USE WCA2
SELECT * 
FROM t610_Parvalue_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 40
USE WCA2
SELECT * 
FROM t610_Currency_Redenomination 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 41
USE WCA2
SELECT * 
FROM t610_Return_of_Capital 
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol


--# 42
USE WCA2
SELECT * 
FROM t610_Dividend
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 43
USE WCA2
SELECT * 
FROM t610_Dividend_Reinvestment_Plan
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 44
USE WCA2
SELECT * 
FROM t610_Franking
WHERE
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
and (sedol in (select code from portfolio.dbo.fsedol where accid=162 and actflag='U'))

ORDER BY EventID, ExchgCD, Sedol

--# 51
USE WCA
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Company_Meeting on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 52
USE WCA
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v53f_610_Call on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 53
USE WCA
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Liquidation on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 54
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_Certificate_Exchange on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 55
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_International_Code_Change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 56
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_Preference_Conversion on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 57
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_Preference_Redemption on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 58
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_Security_Reclassification on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 59
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v52f_610_Lot_Change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 60
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v52f_610_Sedol_Change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 61
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v53f_610_Buy_Back on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 62
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v53f_610_Capital_Reduction on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 63
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v53f_610_Takeover on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 64
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Arrangement on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 65
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Bonus on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 66
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Bonus_Rights on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 67
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Consolidation on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 68
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Demerger on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 69
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Distribution on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 70
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Divestment on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 71
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Entitlement on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 72
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Merger on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 73
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Preferential_Offer on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 74
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Purchase_Offer on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 75
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Rights on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 76
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Security_Swap on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 77
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Subdivision on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 


--# 78
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Bankruptcy on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 79
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Financial_Year_Change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 80
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Incorporation_Change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 81
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Issuer_Name_change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 82
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Lawsuit on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 83
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_Security_Description_Change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 84
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v52f_610_Assimilation on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 85
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v52f_610_Listing_Status_Change on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 86
USE wca2
SELECT *
FROM t610_Local_Code_Change
WHERE (CHANGED > getdate()+1)

--# 87
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v52f_610_New_Listing on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 88
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v50f_610_Announcement on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 89
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_Parvalue_Redenomination on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 90
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v51f_610_Currency_Redenomination on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
AND (RelatedEvent <> 'Clean' or RelatedEvent is null)
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 91
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v53f_610_Return_of_Capital on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 92
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Dividend on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 93
USE wca
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Dividend_Reinvestment_Plan on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 

--# 94
USE WCA
SELECT * 
FROM portfolio.dbo.fsedol
left outer join v54f_610_Franking on portfolio.dbo.fsedol.code = sedol
WHERE portfolio.dbo.fsedol.accid=162 
and portfolio.dbo.fsedol.actflag='I'
and (CHANGED >= getdate()-183)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL) 
