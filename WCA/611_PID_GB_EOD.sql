--filepath=o:\Datafeed\wca\611_PID_GB\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.611
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\611_PID_GB\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n



--# 1
USE WCA
SELECT * 
FROM v54f_611_Dividend_PID
WHERE 
Dividend='PID'
and (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
and exchgCD = 'GBLSE'
ORDER BY EventID, ExchgCD, Sedol
