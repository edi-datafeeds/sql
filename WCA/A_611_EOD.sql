--filepath=o:\Datafeed\WCA\611_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca2.dbo.t610_dividend.changed),112) from wca2.dbo.t610_dividend
--fileextension=.611
--suffix=
--fileheadertext=EDI_DIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\611_EOD\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n




Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = getDate()-0.8
set @EndDate = getDate()+1



/* Use script Var and Sets*/
--UseIt @StartDate
--UseIt @EndDate

--# 1
USE WCA2
SELECT * 
FROM t610_Dividend
WHERE 
(ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
ORDER BY EventID, ExchgCD, Sedol
