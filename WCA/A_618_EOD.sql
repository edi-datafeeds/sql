--filepath=o:\datafeed\wca\618\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=
--fileextension=.618
--suffix=
--fileheadertext=EDI_FlatDividend_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\618\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n

--# 1
USE WCA
SELECT * from v54f_618_FlatDividend
WHERE 
CHANGED >= (select max(feeddate) from tbl_Opslog where seq = 3)
AND (ListingStatus <> 'De-listed' or ListingStatus is null)
AND (SecStatus <> 'Inactive' OR Secstatus IS NULL)
