use wca
if exists (select * from sysobjects where name = 'serialseq')
	drop table serialseq
go
select distinct
etab.sevent as eventcd,
etab.eventid as rdid,
etab.bonid as eventid,
1 as seqnum
into serialseq
from v10s_BON as etab
where (etab.actflag<>'D')
go

USE WCA
ALTER TABLE serialseq ALTER COLUMN eventcd char(3) NOT NULL
go
USE WCA
ALTER TABLE serialseq ALTER COLUMN rdid int NOT NULL
go
USE WCA
ALTER TABLE serialseq ALTER COLUMN eventid int NOT NULL
go

ALTER TABLE [DBO].[serialseq] WITH NOCHECK ADD
	
	CONSTRAINT [PK_serialseq] PRIMARY KEY 
	
	(
		[eventcd],[rdid],[eventid]
	)  ON [PRIMARY] 

go 

insert into serialseq 
select
etab.sevent as eventcd,
etab.eventid as rdid,
etab.prfid as eventid,
1 as seqnum
from v10s_PRF as etab
where (etab.actflag<>'D')
go

insert into serialseq 
select
etab.sevent as eventcd,
etab.eventid as rdid,
etab.rtsid as eventid,
1 as seqnum
from v10s_RTS as etab
where (etab.actflag<>'D')
go


EXEC sp_serialseq

go
