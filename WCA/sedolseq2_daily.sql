use wca
if exists (select * from sysobjects where name = 'sedolseq2')
	drop table sedolseq2
go
select distinct
SEDOL.secid,
SEDOL.sedol,
SEDOL.cntrycd,
SEDOL.rcntrycd,
SEDOL.curencd,
SEDOL.sedolid,
1 as seqnum,
case when sedol.defunct='F' then SEDOL.Acttime else '2000/01/01 15:00:00' end as SedolActtime
into sedolseq2
from SEDOL
where SEDOL.actflag<>'D'
and SEDOL.sedol<>''
and SEDOL.sedol is not null
and SEDOL.cntrycd<>'ZZ'
and SEDOL.cntrycd<>''
and SEDOL.cntrycd is not null
go

USE WCA
ALTER TABLE sedolseq2 ALTER COLUMN  secid int NOT NULL
go
USE WCA
ALTER TABLE sedolseq2 ALTER COLUMN  sedol char(7) NOT NULL
go

ALTER TABLE [DBO].[sedolseq2] WITH NOCHECK ADD
	
	CONSTRAINT [PK_sedolseq2] PRIMARY KEY 
	
	(
		[secid],[sedol]
	)  ON [PRIMARY] 

go 

use WCA
CREATE  INDEX [ix_sedolseq2] ON [dbo].[sedolseq2]([secid],[cntrycd]) ON [PRIMARY] 
go

EXEC sp_sedolseq2

go
