--filepath=o:\Datafeed\wca\602\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.602
--suffix=
--fileheadertext=EDI_STATIC_602_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\wca\602\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=
--sevent=n
--shownulls=n


--# 1
use WCA
SELECT 
issur.Issuername,
issur.CntryofIncorp,
scmst.IssID,
scmst.SecID,
scmst.Statusflag,
scmst.PrimaryExchgCD,
scmst.Securitydesc,
scmst.CurenCD as ParValueCurrency,
scmst.Parvalue,
scmst.SectyCD,
scmst.Uscode,
scmst.Isin,
scmst.StructCD,
Sedol.Sedol,
Sedol.RCntryCD as RegCountry,
Sedol.Defunct,
scexh.exchgcd,
scexh.localcode,
scexh.liststatus,
scexh.Lot,
scexh.MinTrdgQty
FROM scmst
LEFT OUTER JOIN issur ON scmst.IssID = issur.IssID
LEFT OUTER JOIN sectygrp ON scmst.sectycd = sectygrp.sectycd
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN exchg ON scexh.ExchgCD = exchg.ExchgCD
LEFT OUTER JOIN sedol ON scmst.SecID = sedol.SecID
                      AND exchg.cntryCD = Sedol.CntryCD
where
(issur.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
or scmst.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
or scexh.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
or sedol.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
AND sectygrp.secgrpid <3