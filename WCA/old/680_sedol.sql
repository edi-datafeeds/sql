--filepath=o:\upload\acc\213\feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(tbl_opslog.Feeddate),112) from tbl_opslog where seq = 3
--fileextension=.680
--suffix=
--fileheadertext=EDI_WCA_680_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\upload\acc\213\feed\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 0
use wca
select
'EventCD' as f1,
'EventID' as f2,
'OptionID' as f5,
'SerialID' as f6,
'Sedol' as f3,
'SecID' as f2,
'Isin' as f3,
'Uscode' as f3,
'Market' as f3,
'Created' as f7,
'Changed' as f8,
'Actflag' as f9,
'Date1type' as f14,
'Date1' as f15,
'Date2type' as f16,
'Date2' as f17,
'Date3type' as f18,
'Date3' as f19,
'Date4type' as f18,
'Date4' as f19,
'Date5type' as f18,
'Date5' as f19,
'Date6type' as f18,
'Date6' as f19,
'Date7type' as f18,
'Date7' as f19,
'Date8type' as f18,
'Date8' as f19,
'Paytype' as f22,
'DefaultOpt' as f23,
'OutturnSecID' as f23,
'OutturnIsin' as f24,
'RatioOld' as f25,
'RatioNew' as f26,
'Fractions' as f27,
'Currency' as f28,
'Rate1type' as f29,
'Rate1' as f30,
'Rate2type' as f31,
'Rate2' as f32,
'Field1name' as f33,
'Field1' as f34,
'Field2name' as f35,
'Field2' as f36,
'Field3name' as f37,
'Field3' as f38,
'Field4name' as f40,
'Field4' as f41,
'Field5name' as f42,
'Field5' as f43,
'Field6name' as f42,
'Field6' as f43


--#
use wca
select distinct
'ARR' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Paytype,
'S' as Paytype,
'' as DefaultOpt,
case when (icc.releventid is null or icc.oldisin = '') then null else scmst.secid end as OutturnSecID,
case when (icc.releventid is null or icc.oldisin = '') then null else icc.newisin end as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from ARR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
left outer join icc on etab.rdid = icc.releventid and 'arr' = icc.eventtype
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'arr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'arr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'ASSM' as EventCD,
etab.AssmID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'AssimilationDate' as Date1Type,
etab.AssimilationDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Paytype,
'' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from ASSM as etab
inner join scexh on etab.secid = scexh.secid and etab.exchgcd = scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on ressecid = resscmst.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.assimilationdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.assimilationdate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.assimilationdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.assimilationdate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'BB' as EventCD,
etab.BbID as EventID,
case when mpay.OptionID is null then 1 else mpay.OptionID end as OptionID,
case when mpay.SerialID is null then 1 else mpay.SerialID end as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'Startdate' as Date1Type,
etab.Startdate as Date1,
'Enddate' as Date2Type,
etab.Enddate as Date2,
'Recdate' as Date3Type,
rd.Recdate as Date3,
'Paydate' as Date4Type,
mpay.Paydate as Date4,
'Withdrawalfromdate' as Date5Type,
mpay.Withdrawalfromdate as Date5,
'Withdrawaltodate' as Date6Type,
mpay.Withdrawaltodate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
case when mpay.Paytype<>'' then mpay.Paytype
     when mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when mpay.curencd<>'' then 'C'
     when mpay.rationew<>'' then 'S'
     else '' end as Paytype,
'' as DefaultOpt,
case when mpay.defaultopt='T' then 'Y' else '' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
mpay.RatioNew,
mpay.RatioOld,
mpay.Fractions,
mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
mpay.Maxprice Rate2,
'OnOffFlag' as Field1Name,
etab.OnOffFlag as Field1,
'WithdrawalRights' as Field2Name,
mpay.WithdrawalRights as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from BB as etab
inner join scmst on etab.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join mpay on etab.bbid = mpay.eventid and 'bb'=mpay.sEvent
left outer join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join rd on etab.rdid = rd.rdid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.enddate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.enddate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.enddate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.enddate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'




--#
use wca
select distinct
'BKRP' as EventCD,
etab.BkrpID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'NotificationDate' as Date1Type,
etab.NotificationDate Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Paytype,
'' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from BKRP as etab
inner join scmst on etab.issid = scmst.issid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.notificationdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.notificationdate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.notificationdate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.notificationdate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'BON' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'S' as Paytype,
'' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
etab.RatioNew,
etab.RatioOld,
etab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from bon as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'bon' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'bon' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on ressecid = resscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'CAPRD' as EventCD,
etab.CaprdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
case when (icc.releventid is null or icc.oldisin = '') then scmst.isin else icc.newisin end as Isin,
case when (icc.releventid is null or icc.olduscode = '') then scmst.uscode else icc.newuscode end as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'EffectiveDate' as Date1Type,
etab.EffectiveDate as Date1,
'RecDate' as Date2Type,
RD.RecDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'S' as Paytype,
'' as DefaultOpt,
case when (icc.releventid is null or icc.oldisin = '') then null else scmst.secid end as OutturnSecID,
case when (icc.releventid is null or icc.oldisin = '') then null else icc.newisin end as OutturnIsin,
etab.NewRatio as RatioNew,
etab.OldRatio as RatioOld,
etab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
etab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
etab.NewParvalue as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
FROM CAPRD as etab
INNER JOIN SCMST ON SCMST.SecID = etab.SecID
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
LEFT OUTER JOIN RD ON etab.RdID = RD.RdID
LEFT OUTER JOIN ICC ON etab.CaprdID = ICC.RelEventID AND 'CAPRD' = ICC.EventType
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.effectivedate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.effectivedate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'


--#
use wca
select distinct
'CONSD' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
case when (icc.releventid is null or icc.oldisin = '') then scmst.isin else icc.newisin end as Isin,
case when (icc.releventid is null or icc.olduscode = '') then scmst.uscode else icc.newuscode end as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'S' as Paytype,
'' as DefaultOpt,
case when (icc.releventid is null or icc.oldisin = '') then null else scmst.secid end as OutturnSecID,
case when (icc.releventid is null or icc.oldisin = '') then null else icc.newisin end as OutturnIsin,
NewRatio as RatioNew,
OldRatio as RatioOld,
Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
OldParvalue as Field1,
'NewParvalue' as Field2Name,
NewParvalue as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from consd as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
left outer join icc on etab.rdid = icc.releventid and 'consd' = icc.eventtype
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'consd' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'consd' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'


--#
use wca
select distinct
'CTX' as EventCD,
etab.CtxID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'StartDate' as Date1Type,
etab.StartDate Date1,
'EndDate' as Date2Type,
EndDate as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Paytype,
'' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'RelEvent' as Field1Name,
Eventtype as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from CTX as etab
inner join scmst on etab.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on ressecid = resscmst.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.enddate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.enddate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.enddate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.enddate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'DIST' as EventCD,
etab.RdID as EventID,
case when mpay.OptionID is null then 1 else mpay.OptionID end as OptionID,
case when mpay.SerialID is null then 1 else mpay.SerialID end as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'Withdrawalfromdate' as Date4Type,
mpay.Withdrawalfromdate as Date4,
'Withdrawaltodate' as Date5Type,
mpay.Withdrawaltodate as Date5,
'OptElectionDate' as Date6Type,
mpay.OptElectionDate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
case when mpay.Paytype<>'' then mpay.Paytype
     when mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when mpay.curencd<>'' then 'C'
     when mpay.rationew<>'' then 'S'
     else '' end as Paytype,
'' as DefaultOpt,
case when mpay.defaultopt='T' then 'Y' else '' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
mpay.RatioNew,
mpay.RatioOld,
mpay.Fractions,
mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
mpay.Maxprice Rate2,
'WithdrawalRights' as Field1Name,
mpay.WithdrawalRights as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from DIST as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'dist' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'dist' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join mpay on etab.rdid = mpay.eventid and 'dist'=mpay.sEvent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'




--#
use wca
select distinct
'DMRGR' as EventCD,
etab.RdID as EventID,
case when mpay.OptionID is null then 1 else mpay.OptionID end as OptionID,
case when mpay.SerialID is null then 1 else mpay.SerialID end as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'Withdrawalfromdate' as Date4Type,
mpay.Withdrawalfromdate as Date4,
'Withdrawaltodate' as Date5Type,
mpay.Withdrawaltodate as Date5,
'OptElectionDate' as Date6Type,
mpay.OptElectionDate as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
case when mpay.Paytype<>'' then mpay.Paytype
     when mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when mpay.curencd<>'' then 'C'
     when mpay.rationew<>'' then 'S'
     else '' end as Paytype,
'' as DefaultOpt,
case when mpay.defaultopt='T' then 'Y' else '' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
mpay.RatioNew,
mpay.RatioOld,
mpay.Fractions,
mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
mpay.Maxprice Rate2,
'WithdrawalRights' as Field1Name,
mpay.WithdrawalRights as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from DMRGR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'dmrgr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'dmrgr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join mpay on etab.rdid = mpay.eventid and 'dmrgr'=mpay.sEvent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'DVST' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
etab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
etab.EndSubscription as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
case when etab.rationew<>'' and etab.curencd<>'' then 'B'
     when etab.curencd<>'' then 'C'
     when etab.rationew<>'' then 'S'
     else '' end as Paytype,
'' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
etab.RatioNew,
etab.RatioOld,
etab.Fractions,
etab.CurenCD as Currency,
'MinPrice' as Rate1Type,
etab.Minprice Rate1,
'MaxPrice' as Rate2Type,
etab.MaxPrice Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from DVST as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'dvst' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'dvst' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on ressecid = resscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'ENT' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
etab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
etab.EndSubscription as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'S' as Paytype,
'' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
etab.RatioNew,
etab.RatioOld,
etab.Fractions,
etab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
etab.EntIssuePrice Rate1,
'' as Rate2Type,
'' Rate2,
'OverSubscription' as Field1Name,
OverSubscription as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from ENT as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'ent' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'ent' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on ressecid = resscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'LIQ' as EventCD,
etab.LiqID as EventID,
case when mpay.OptionID is null then 1 else mpay.OptionID end as OptionID,
case when mpay.SerialID is null then 1 else mpay.SerialID end as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'Recdate' as Date1Type,
etab.Rddate as Date1,
'Paydate' as Date2Type,
mpay.paydate as Date2,
'OptElectionDate' as Date3Type,
mpay.OptElectionDate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
case when mpay.Paytype<>'' then mpay.Paytype
     when mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when mpay.curencd<>'' then 'C'
     when mpay.rationew<>'' then 'S'
     else '' end as Paytype,
'' as DefaultOpt,
case when mpay.defaultopt='T' then 'Y' else '' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
mpay.RatioNew,
mpay.RatioOld,
mpay.Fractions,
mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
mpay.Maxprice Rate2,
'Liquidator' as Field1Name,
etab.Liquidator as Field1,
'LiqCntryCD' as Field2Name,
LiqCntryCD as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from LIQ as etab
inner join scmst on etab.issid = scmst.issid
inner join sedol on scmst.secid = sedol.secid
left outer join mpay on etab.liqid = mpay.eventid and 'liq'=mpay.sEvent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.RdDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.RdDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.RdDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.RdDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'MRGR' as EventCD,
etab.RdID as EventID,
case when mpay.OptionID is null then 1 else mpay.OptionID end as OptionID,
case when mpay.SerialID is null then 1 else mpay.SerialID end as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'AppointedDate' as Date4Type,
etab.AppointedDate as Date4,
'EffectiveDate' as Date5Type,
etab.EffectiveDate as Date5,
'WithdrawalFromdate' as Date6Type,
mpay.WithdrawalFromdate as Date6,
'WithdrawalTodate' as Date7Type,
mpay.WithdrawalTodate as Date7,
'OptElectionDate' as Date8Type,
mpay.OptElectionDate as Date8,
case when mpay.Paytype<>'' then mpay.Paytype
     when mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when mpay.curencd<>'' then 'C'
     when mpay.rationew<>'' then 'S'
     else '' end as Paytype,
case when mpay.defaultopt='T' then 'Y' else '' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
mpay.RatioNew,
mpay.RatioOld,
mpay.Fractions,
mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
mpay.Maxprice Rate2,
'WithdrawalRights' as Field1Name,
mpay.WithdrawalRights as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from MRGR as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'mrgr' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'mrgr' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join mpay on etab.rdid = mpay.eventid and 'mrgr'=mpay.sEvent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'PO' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'OfferCloses' as Date4Type,
etab.OfferCloses as Date4,
'OfferOpens' as Date5Type,
etab.OfferOpens as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'C' as Paytype,
'' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
etab.CurenCD as Currency,
'MaxPrice' as Rate1Type,
etab.MaxPrice Rate1,
'MinPrice' as Rate2Type,
MinPrice Rate2,
'TndrStrkPrice' as Field1Name,
TndrStrkPrice as Field1,
'NegotiatedPrice' as Field2Name,
NegotiatedPrice as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from PO as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'rts' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'rts' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (Offercloses>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or Offercloses is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (Offercloses>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (Offercloses is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'


--#
use wca
select distinct
'PRF' as EventCD,
etab.RdID as EventID,
sedol.sedol as Sedol,
'1' as OptionID,
'1' as SerialID,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
etab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
etab.EndSubscription as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
case when etab.rationew<>'' and etab.curencd<>'' then 'B'
     when etab.curencd<>'' then 'C'
     when etab.rationew<>'' then 'S'
     else '' end as Paytype,
'' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
etab.RatioNew,
etab.RatioOld,
etab.Fractions,
etab.CurenCD as Currency,
'MaxPrice' as Rate1Type,
etab.MaxPrice Rate1,
'MinPrice' as Rate2Type,
MinPrice Rate2,
'TndrStrkPrice' as Field1Name,
TndrStrkPrice as Field1,
'OffereeName' as Field2Name,
OffereeName as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from PRF as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'prf' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'prf' = pexdt.eventtype
left outer join scmst as resscmst on ressecid = resscmst.secid
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'RCAP' as EventCD,
etab.RcapID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'EffectiveDate' as Date1Type,
etab.EffectiveDate as Date1,
'RecDate' as Date2Type,
RD.RecDate as Date2,
'PayDate' as Date3Type,
etab.Cspydate as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'C' as Paytype,
'' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
etab.CurenCD as Currency,
'CashBak' as Rate1Type,
etab.CashBak Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
FROM RCAP as etab
INNER JOIN SCMST ON SCMST.SecID = etab.SecID
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
LEFT OUTER JOIN RD ON etab.RdID = RD.RdID
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.effectivedate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.effectivedate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'




--#
use wca
select distinct
'REDEM' as EventCD,
etab.RedemID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'RedemDate' as Date1Type,
RedemDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'' as Paytype,
'C' as Paytype,
'' as DefaultOpt,
null as OutturnSecID,
'' as OutturnIsin,
'' as RatioNew,
'' as RatioOld,
'' as Fractions,
etab.CurenCD as Currency,
'RedemPrice' as Rate1Type,
etab.RedemPrice Rate1,
'' as Rate2Type,
'' Rate2,
'MandOptFlag' as Field1Name,
etab.MandOptFlag as Field1,
'PartFinal' as Field2Name,
etab.PartFinal as Field2,
'Redemtype' as Field3Name,
etab.Redemtype as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from REDEM as etab
inner join scmst on etab.secid = scmst.secid
inner join sedol on scmst.secid = sedol.secid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (RedemDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or RedemDate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (RedemDate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (RedemDate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'


--#
use wca
select distinct
'RTS' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'StartSubscription' as Date4Type,
etab.StartSubscription as Date4,
'EndSubscription' as Date5Type,
etab.EndSubscription as Date5,
'StartTrade' as Date6Type,
etab.StartTrade as Date6,
'EndTrade' as Date7Type,
etab.EndTrade as Date7,
'Splitdate' as Date8Type,
Splitdate as Date8,
'S' as Paytype,
'' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
etab.RatioNew,
etab.RatioOld,
etab.Fractions,
etab.CurenCD as Currency,
'IssuePrice' as Rate1Type,
etab.IssuePrice Rate1,
'LapsedPremium' as Rate2Type,
LapsedPremium Rate2,
'TradingSecID' as Field1Name,
etab.TraSecID as Field1,
'TradingIsin' as Field2Name,
trascmst.Isin as Field2,
'PPSecID' as Field3Name,
etab.PPSecID as Field3,
'PPIsin' as Field4Name,
PPscmst.Isin as Field4,
'OverSubscription' as Field5Name,
OverSubscription as Field5,
'' as Field6Name,
'' as Field6
from RTS as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'rts' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'rts' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on ressecid = resscmst.secid
left outer join scmst as trascmst on trasecid = trascmst.secid
left outer join scmst as ppscmst on ppsecid = ppscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'SD' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
case when (icc.releventid is null or icc.oldisin = '') then scmst.isin else icc.newisin end as Isin,
case when (icc.releventid is null or icc.olduscode = '') then scmst.uscode else icc.newuscode end as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'S' as Paytype,
'' as DefaultOpt,
case when (icc.releventid is null or icc.oldisin = '') then null else scmst.secid end as OutturnSecID,
case when (icc.releventid is null or icc.oldisin = '') then null else icc.newisin end as OutturnIsin,
etab.NewRatio as RatioNew,
etab.OldRatio as RatioOld,
etab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'OldParvalue' as Field1Name,
etab.OldParvalue as Field1,
'NewParvalue' as Field2Name,
etab.NewParvalue as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from SD as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
left outer join icc on etab.rdid = icc.releventid and 'sd' = icc.eventtype
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'sd' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'sd' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'


--#
use wca
select distinct
'SECRC' as EventCD,
etab.SecrcID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'EffectiveDate' as Date1Type,
etab.EffectiveDate as Date1,
'' as Date2Type,
null as Date2,
'' as Date3Type,
null as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'S' as Paytype,
'' as DefaultOpt,
resscmst.secid as OutturnSecID,
resscmst.isin as OutturnIsin,
etab.RatioNew,
etab.RatioOld,
'' as Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'Relevent' as Field1Name,
etab.Eventtype as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
FROM SECRC as etab
INNER JOIN SCMST ON SCMST.SecID = etab.SecID
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer JOIN SCMST as resscmst ON etab.ressecID = resscmst.SecID
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.effectivedate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.effectivedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.effectivedate is null and etab.announcedate>getdate()-92)) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'SCSWP' as EventCD,
etab.RdID as EventID,
'1' as OptionID,
'1' as SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'ExDate' as Date1Type,
CASE WHEN EXDT.ExDate IS NOT NULL then EXDT.ExDate ELSE pexdt.exdate END as Date1,
'Recdate' as Date2Type,
rd.Recdate as Date2,
'Paydate' as Date3Type,
CASE WHEN EXDT.PayDate IS NOT NULL then EXDT.PayDate ELSE pexdt.paydate END as Date3,
'' as Date4Type,
null as Date4,
'' as Date5Type,
null as Date5,
'' as Date6Type,
null as Date6,
'' as Date7Type,
null as Date7,
'' as Date8Type,
null as Date8,
'S' as Paytype,
'' as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
etab.NewRatio as RatioNew,
etab.OldRatio as RatioOld,
etab.Fractions,
'' as Currency,
'' as Rate1Type,
'' Rate1,
'' as Rate2Type,
'' Rate2,
'' as Field1Name,
'' as Field1,
'' as Field2Name,
'' as Field2,
'' as Field3Name,
'' as Field3,
'' as Field4Name,
'' as Field4,
'' as Field5Name,
'' as Field5,
'' as Field6Name,
'' as Field6
from SCSWP as etab
inner join rd on etab.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'scswp' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'dvst' = pexdt.eventtype
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on ressecid = resscmst.secid
WHERE
(
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
           or (select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end)>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or ((select case when exdt.exdate is null then pexdt.exdate else exdt.exdate end) is null and etab.announcedate>getdate()-92)) )
)
)
and sedol.actflag<>'D'
and etab.actflag<>'D'



--#
use wca
select distinct
'TKOVR' as EventCD,
etab.TkovrID as EventID,
mpay.OptionID,
mpay.SerialID,
sedol.sedol as Sedol,
scmst.SecID,
scmst.isin as Isin,
scmst.uscode as Uscode,
'' as Market,
etab.AnnounceDate as Created,
etab.Acttime as Changed,
etab.ActFlag,
'Opendate' as Date1Type,
etab.opendate as Date1,
'Closedate' as Date2Type,
etab.closedate as Date2,
'Recdate' as Date3Type,
rd.Recdate as Date3,
'Cmacqdate' as Date4Type,
etab.Cmacqdate as Date4,
'Paydate' as Date5Type,
mpay.Paydate as Date5,
'Optelectiondate' as Date6Type,
mpay.Optelectiondate as Date6,
'Withdrawalfromdate' as Date7Type,
mpay.Withdrawalfromdate as Date7,
'Withdrawaltodate' as Date8Type,
mpay.Withdrawaltodate as Date8,
case when mpay.Paytype<>'' then mpay.Paytype
     when mpay.rationew<>'' and mpay.curencd<>'' then 'B'
     when mpay.curencd<>'' then 'C'
     when mpay.rationew<>'' then 'S'
     else '' end as Paytype,
case when mpay.defaultopt='T' then 'Y' else '' end as DefaultOpt,
resscmst.SecID as OutturnSecID,
resscmst.isin as OutturnIsin,
mpay.RatioNew,
mpay.RatioOld,
mpay.Fractions,
mpay.CurenCD as Currency,
'Minprice' as Rate1Type,
mpay.Minprice Rate1,
'Maxprice' as Rate2Type,
mpay.Maxprice Rate2,
'OfferorName' as Field1Name,
etab.OfferorName as Field1,
'TkovrStatus' as Field2Name,
etab.TkovrStatus as Field2,
'MiniTkovr' as Field3Name,
etab.MiniTkovr as Field3,
'WithdrawalRights' as Field4Name,
mpay.WithdrawalRights as Field4,
'Hostile' as Field5Name,
etab.Hostile as Field5,
'' as Field6Name,
'' as Field6
from TKOVR as etab
inner join scmst on etab.secid = scmst.secid
inner join scexh on scmst.secid = scexh.secid
left outer join mpay on etab.tkovrid = mpay.eventid and 'tkovr'=mpay.sEvent
left outer join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd = sedol.cntrycd
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join rd on etab.rdid = rd.rdid
WHERE
(
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='U')
    and (etab.closedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) or etab.closedate is null)
    and etab.acttime>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) )
  or
  ( sedol in (select code from client.dbo.pfsedol where accid=213 and actflag='I')
    and (etab.closedate>(select max(feeddate) from wca.dbo.tbl_opslog where seq=3) 
         or (etab.closedate is null and etab.announcedate>getdate()-183 and etab.tkovrstatus<>'L')) )
)
and sedol.actflag<>'D'
and etab.actflag<>'D'
