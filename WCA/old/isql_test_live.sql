use wca
if exists (select * from sysobjects where name = 'sedolseq1')
	drop table sedolseq1
go
select distinct
SEDOL.secid,
SEDOL.sedol,
SEDOL.cntrycd,
SEDOL.rcntrycd,
SEDOL.curencd,
1 as seqnum
into sedolseq1
from SEDOL
where SEDOL.actflag<>'D'
and SEDOL.defunct='F'
and SEDOL.sedol<>''
and SEDOL.sedol is not null
and SEDOL.cntrycd<>'ZZ'
and SEDOL.cntrycd<>''
and SEDOL.cntrycd is not null
go

USE WCA
ALTER TABLE sedolseq1 ALTER COLUMN  secid int NOT NULL
go
USE WCA
ALTER TABLE sedolseq1 ALTER COLUMN  sedol char(7) NOT NULL
go

ALTER TABLE [DBO].[sedolseq1] WITH NOCHECK ADD
	
	CONSTRAINT [PK_sedolseq1] PRIMARY KEY 
	
	(
		[secid],[sedol]
	)  ON [PRIMARY] 

go 

use WCA
CREATE  INDEX [ix_sedolseq1] ON [dbo].[sedolseq1]([secid],[cntrycd]) ON [PRIMARY] 
go

EXEC sp_sedolseq1

go