use wca
if exists (select * from sysobjects where name = 'sedolseq')
	drop table sedolseq
go
select distinct
SEDOL.secid,
SEDOL.sedol,
SEDOL.cntrycd,
SEDOL.rcntrycd,
1 as seqnum
into sedolseq
from SEDOL
where SEDOL.actflag<>'D'
and SEDOL.defunct='F'
and SEDOL.sedol<>''
and SEDOL.sedol is not null
and SEDOL.cntrycd<>'ZZ'
and SEDOL.cntrycd<>''
and SEDOL.cntrycd is not null
go

USE WCA
ALTER TABLE sedolseq ALTER COLUMN  secid int NOT NULL
go
USE WCA
ALTER TABLE sedolseq ALTER COLUMN  sedol char(7) NOT NULL
go

ALTER TABLE [DBO].[sedolseq] WITH NOCHECK ADD
	
	CONSTRAINT [PK_sedolseq] PRIMARY KEY 
	
	(
		[secid],[sedol]
	)  ON [PRIMARY] 

go 

use WCA
CREATE  INDEX [ix_sedolseq] ON [dbo].[sedolseq]([secid],[cntrycd]) ON [PRIMARY] 
go

