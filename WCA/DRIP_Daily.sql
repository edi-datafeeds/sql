--FilePath=o:\DataFeed\WCA\DRIPS\
--filenameprefix=
--FileName=YYYYMMDD
--filenamealt=
--FileExtension=.TXT
--suffix=_DRIP
--fileheadertext=
--fileheaderdate=
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime
--forcetime=n
--filefootertext=
--FieldSeparator=	
--Archive=y
--ArchivePath=n:\WCA\DRIPS\
--FIELDHEADERS=on
--FileTidy=N
--fwoffsets=
--incremental=
--sevent=
--shownulls=


--# 1

use wca2
select 
tISO_D.Changed,
tISO_D.EXCHGID+cast(tISO_D.Seqnum as char(1))+ cast(tISO_D.EventID as char(16)) as CAref,
tISO_D.Actflag_1,
tISO_D.MCD,
tISO_D.Issuername,
tISO_D.Securitydesc,
tISO_D.Sedol,
tISO_D.Localcode,
tISO_D.Exdate,
tISO_D.Recdate,
tISO_D.DivPeriodCD,
tISO_D.CurenCD_1,
tISO_D.Netdividend_1,
tISO_D.Grossdividend_1,
tISO_D.Taxrate_1,
tISO_D.Paydate,
tISO_D.Fractions_1,
tISO_D.DripReinvPrice,
tISO_D.DripLastDate,
tISO_D.DripPaydate,
tISO_D.DripCrestdate,
tISO_D.DripCntryCD
From tISO_D
WHERE 
not (tISO_D.DripCntryCD = '')
