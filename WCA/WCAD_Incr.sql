--filepath=o:\Datafeed\EDIUK\wcad\
--filenameprefix=
--filename=yyyymmdd
--fileextension=.txt
--suffix=
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\EDIUK\WCAD\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--filefootertext=
--ZEROROWCHK=N

--# 1
use WCA
select 
EventID,
Optionid as OpID,
Created,
v54f_620_dividend.Actflag as Flag,
Issuername,
SecurityDesc,
case when Isin is not null and isin <>'' THEN Isin else Sedol end as DIVcode,
Recdate as Closedate,
PEXDT.ExDate as PrimaryEx,
PEXDT.PayDate as PrimaryPay,
v54f_620_dividend.OptElectionDate,
Divtype as RType,
CurenCD as OpCurr,
Grossdividend as GrossRate,
Taxrate as OSTax,
RatioOld as Denomin,
RatioNew as Numerat,
marker,
divperiodcd
from v54f_620_dividend
LEFT OUTER JOIN EXDT as PEXDT ON v54f_620_dividend.RdID = PEXDT.RdID AND v54f_620_dividend.PrimaryExchgCD = PEXDT.ExchgCD AND 'DIV' = PEXDT.EventType
where
(CntryofIncorp<>'GB' or sectycd='DR')
and v54f_620_dividend.ExchgCD = 'GBLSE'
and CHANGED =(select max(tbl_opslog.acttime) from tbl_opslog)
and ((optionid = 1 and divtype <> 'C') or (optionid <> 1))
and divtype <> 'T'
order by eventid, optionid