--filepath=o:\Datafeed\WCA\512_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.512
--suffix=
--fileheadertext=Records = NNNNNN Security Data
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\512_EOD\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n




--# 1
use wca
SELECT
SCEXH.ScexhID as SecID,
convert(varchar, getdate(),103)+' '+CONVERT ( varchar , getdate(),8) as scmsttime,
SCMST.Actflag,
substring(SCEXH.ExchgCD,1,2) as ExCountry,
SCEXH.ExchgCD,
SCMST.IssID,  
SCEXH.Localcode,  
SCMST.Isin,  
SCMST.SecurityDesc,
SCMST.SectyCD as Typecode,
sedolseq1.Sedol,  
SCMST.USCode,
SCMST.AnnounceDate as Sourcedate,
SCMST.AnnounceDate as Creation,
SCMST.IssID as Globissid,
'' as trailingTAB
FROM scmst
LEFT OUTER JOIN scexh ON scmst.SecID = scexh.SecID
LEFT OUTER JOIN sedolseq1 ON scmst.SecID = sedolseq1.SecID
                   and substring(SCEXH.ExchgCD,1,2) = sedolseq1.cntryCD

where (scmst.sectyCD='EQS' or scmst.sectyCD='DR' or scmst.sectyCD='PRF')
and (scmst.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)
or scexh.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3))
and scexh.scexhid is not null
and sedolseq1.seqnum = 1
order by scexhid, sedolseq1.sedol desc