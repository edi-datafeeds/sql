use wca2
if exists (select * from sysobjects where name = 'compseq')
	drop table compseq
go
use wca
select distinct
BBC.SecID,
BBC.CntryCD,
BBC.CurenCD,
BBC.BbgCompID,
BBC.BbcID,
1 as Seqnum,
BBC.BbgCompTk,
BBC.Acttime
into wca2.dbo.compseq
from BBC
where 
BBC.actflag<>'D'
and BBC.CntryCD<>''
and BBC.BbgCompID<>''
go

USE WCA2
ALTER TABLE compseq ALTER COLUMN BbcID int NOT NULL
go
use WCA2
ALTER TABLE compseq ALTER COLUMN SecID int NOT NULL
go
use WCA2
ALTER TABLE compseq ALTER COLUMN CntryCD char(6) NOT NULL
go
use WCA2
ALTER TABLE compseq ALTER COLUMN CurenCD char(3) NOT NULL
go

use WCA2
ALTER TABLE [DBO].[compseq] WITH NOCHECK ADD
	
	CONSTRAINT [PK_compseq] PRIMARY KEY 
	
	(
		[BbcID]
	)  ON [PRIMARY] 

go 

use WCA2
CREATE  INDEX [ix_compseq] ON [dbo].[compseq]([SecID],[CntryCD],[BbcID] ) ON [PRIMARY] 
go

use wca
EXEC sp_compseq

go
