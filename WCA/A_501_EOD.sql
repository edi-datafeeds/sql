--filepath=o:\Datafeed\WCA\501_EOD\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.501
--suffix=
--fileheadertext=records = NNNNNNN EXCHANGE ISSUER DATA
--fileheaderdate=
--datadateformat=dd/mm/yyyy
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=n:\WCA\501_EOD\
--fieldheaders=n
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n




--# 1
use wca
SELECT 
ISSUR.IssID,
convert(varchar, ISSUR.Acttime,103)+' '+CONVERT ( varchar , ISSUR.Acttime,8) as ISSURtime,
ISSUR.Actflag,  
'' as exchcode,
ISSUR.Issuername,
'' as ExCountry,
ISSUR.IndusID as Induscode,
ISSUR.AnnounceDate as Creation,
'' as trailingTAB

FROM issur

where issur.acttime >= (select max(feeddate) from tbl_Opslog where seq = 3)