--filepath=O:\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select convert(varchar(10),max(wca.dbo.tbl_opslog.Feeddate),112) from wca.dbo.tbl_opslog where seq = 3
--fileextension=.txt
--suffix=_NLIST_Weekly
--fileheadertext=WEEKLY_EDI_NLIST_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\Bespoke\mbendi\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n


--# 1
use wca
SELECT 
upper('NLIST') as TableName,
wca.dbo.SCMST.SectyCD,
wca.dbo.nlist.AnnounceDate as Created,
wca.dbo.nlist.Acttime as Changed,
wca.dbo.nlist.scexhid as nlistID,
wca.dbo.SCMST.SecID,
wca.dbo.SCMST.ISIN,
wca.dbo.issur.Issuername,
wca.dbo.scexh.ExchgCD,
wca.dbo.exchg.MIC,
wca.dbo.scexh.Localcode
FROM wca.dbo.nlist
INNER JOIN wca.dbo.scexh ON wca.dbo.nlist.scexhid = wca.dbo.scexh.scexhid
INNER JOIN wca.dbo.SCMST ON wca.dbo.scexh.SecID = wca.dbo.SCMST.SecID
INNER JOIN wca.dbo.issur ON wca.dbo.scmst.issid = wca.dbo.issur.issid
INNER JOIN wca.dbo.exchg ON wca.dbo.scexh.exchgcd = wca.dbo.exchg.exchgcd
WHERE (wca.dbo.nlist.Acttime>=getdate()-7 and wca.dbo.nlist.Acttime<getdate()+0)
AND (wca.dbo.SCMST.SectyCD = 'EQS'
OR wca.dbo.SCMST.SectyCD = 'CDR'
OR wca.dbo.SCMST.SectyCD = 'DR'
OR wca.dbo.SCMST.SectyCD = 'ETF')
ORDER BY wca.dbo.nlist.Acttime desc, wca.dbo.scexh.ExchgCD asc

