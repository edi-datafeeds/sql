--filepath=o:\datafeed\wca\621_FR\
--filenameprefix=EDI_FR_
--filename=621
--filenamealt=
--fileextension=.txt
--suffix=
--fileheadertext=EDI_621_FR_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=
--fieldseparator=;
--outputstyle=
--archive=y
--archivepath=O:\WCA\
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
use wca
select distinct
scmst.issid,
issuername,
securitydesc,
scmst.isin,
sedol.sedol,
case when sectyCD = 'EQS' then '1 Equity'
     when sectyCD = 'PRF' then '2 Preference'
     when sectyCD = 'DR' then '3 Depositary Receipt'
     when sectyCD = 'BND' then '4 Debt'
else '5 Other'
end as SecurityType,
scexh.localcode,
substring(exchgcd,1,2) as ExchgCntryCD,
CntryofIncorp
from scmst
left outer join scexh on scmst.secid = scexh.secid
left outer join sedol on scmst.secid = sedol.secid and substring(scexh.exchgcd,1,2)=sedol.cntrycd
left outer join issur on scmst.issid = issur.issid
where 
scexh.exchgcd = 'FRPEN'
and scmst.sectycd<>'CW'
and scexh.actflag <> 'D'
and scexh.liststatus <> 'D'
and scmst.actflag <> 'D'
and (scmst.statusflag <> 'I' or scmst.statusflag is null)
and scmst.actflag <> 'D'
order by scmst.issid, securitytype, Localcode desc

