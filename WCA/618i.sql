--FilePath=c:\datafeed\wca\618i\
--filename=yyyymmdd
--filenamealt=
--fileextension=.618
--suffix=
--FileHeaderTEXT=EDI_FLATDIVIDEND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\WCA\618i_global
--fieldheaders=y
--filetidy=y
--fwoffsets=
--incremental=select top 1 seq FROM wca.dbo.tbl_opslog order by acttime desc
--sevent=n
--shownulls=n
--DataDateFormat=YYYY/MM/DD
--DataTimeFormat=
--ForceTime

--# 1
USE WCA
SELECT v54f_618_FlatDivFidDsub.* from v54f_618_FlatDivFidDsub
WHERE CHANGED >= (select max(acttime) from tbl_Opslog)
ORDER BY EventID, ExchgCD, Sedol
